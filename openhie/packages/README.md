# Everwell HUB

## Components

#

### Everwell Internal Components
* **Integrated Notification Service (INS)** - INS handles the patient and staff notifications. it supports SMS, Emails, IVR, and push notifications.
* **Integrated Adherence Management (IAM)** - IAM integrates many adherence technologies and binds them onto the IAM platform. The platform features primarily include adherence management agnostic of the technology used to measure.
* **Dispensation Service** - Dispensation Service is a standalone service including management of patient dispensations and refills as well as managing the medications for each deployment.
* **Episode Service** - Episode Service is used to track and manage episode across various diseases throughout their treatment journey, from diagnosis to outcome.
* **Data Gateway** - Data Gateway is used as API Gateway for communication between the internal services and communication between Everwell and third-party clients.

#

### Third party open source Components
* [Postgres](https://www.postgresql.org/) - Postgres is a free and open-source relational database management system. it is used by all the services, each service will create a separate DB.
* [Redis](https://redis.io/) - Redis is an in-memory data structure store, used as a distributed, in-memory key–value database. it is used by IAM, Dispensation, DataGateway, and Episode service.
* [RabbitMQ](https://www.rabbitmq.com/) - RabbitMQ is an open-source message-broker software. it is used by DataGateway, IAM, INS, and Episode Service. 
* [Elastic Search](https://www.elastic.co/elasticsearch) - Elasticsearch is a distributed, free and open search and analytics engine. it is used by the Episode Service.

## Installation Steps

#

### Docker
* clone Everwell OpenHIE repo
  ```bash
  git clone https://gitlab.com/everwell/hub-foss.git
  cd hub-foss/openhie/packages/
  ```
* Download goinstant binary
  ```bash
  wget -O goinstant https://github.com/openhie/instant/releases/download/0.0.12/goinstant-linux
  sudo mv goinstant /usr/local/bin/goinstant
  chmod +x /usr/local/bin/goinstant
  ```
* Start the applications. 
  ```bash
  goinstant docker up everwell \
    -c="${PWD}/everwell" \
    --instant-version="latest"
  ```
* Stop the running applications
  ```bash
  goinstant docker down everwell \
    -c="${PWD}/everwell" \
    --instant-version="latest"
  ```
* Delete the applicaitons including data
  ```bash
  goinstant docker destroy everwell \
    -c="${PWD}/everwell" \
    --instant-version="latest"
  ```
  > in the above commands we used ${PWD} for the current working directory. if running from some different directory update the path of the package accordingly.

### Kubernetes
#
> at the time of writing this document go instant binary doesn't support Kubernetes. we get the following output if we try to use goinstant.
  ```log
  Kubernetes not supported for now :(
  ```
* clone Everwell OpenHIE repo
  ```bash
  git clone https://gitlab.com/everwell/hub-foss.git
  cd hub-foss/openhie/packages/
  ```
* Start the applications. 
  ```bash
  ${PWD}/everwell/kubernetes/main/k8s.sh up
  ```

* Stop the running applications
  ```bash
  ${PWD}/everwell/kubernetes/main/k8s.sh down
  ```

* Delete the applicaitons including data
  ```bash
  ${PWD}/everwell/kubernetes/main/k8s.sh destroy
  ```
  > in the above commands we used ${PWD} for the current working directory. if running from some different directory update the path of the package accordingly.

## Environment Variables

| Name | Description |
| -- | -- |
| `POSTGRESQL_APPLICATION_USER`          | User name of the Postgres DB user, used by the applications to connect to DB. this user will be automatically created for the new DB.                                |
| `POSTGRESQL_APPLICATION_USER_PASSWORD` | Password of the Postgres DB user, used by the applications to connect to DB                                                                                          |
| `POSTGRESQL_PASSWORD`                  | password of the Postgres root(postgres) user. this user will be automatically created for the new DB.                                                                |
| `RABBITMQ_USERNAME`                    | User name of RabbitMQ user, used by the applications to connect to RabbitMQ. this user will be automatically created for the new RabbitMQ instance.                  |
| `RABBITMQ_PASSWORD`                    | Password of RabbitMQ user, used by the applications to connect to RabbitMQ. this user will be automatically created for the new RabbitMQ instance.                   |
| `REDIS_PASSWORD`                       | Password of RabbitMQ, used by the applications to connect to RabbitMQ. this user will be automatically created a new Redis instance.                                 |
| `JWT_SECRET`                           | Secret used by the applications to sign tokens                                                                                                                       |
| `ELASTIC_USERNAME`                     | User name of the ElasticSearch user, used by the applications to connect to ElasticSearch. this user will be automatically created for a new ElasticSearch instance. |
| `ELASTIC_PASSWORD`                     | Password of the ElasticSearch user, used by the applications to connect to ElasticSearch                                                                             |
| `DEFAULT_USERNAME`                 | The default username for the Everwell services, if the SERVICE_USERNAME variable is not set for a service then this value will be used as default value |
| `DEFAULT_PASSWORD`                 | The default password for the Everwell services, if the SERVICE_EVERWELL variable is not set for a service then this value will be used as default value                            |
| `DATAGATEWAY_USERNAME`                 | The Username of the new Datagateway client, required for Bearer token authentication. the client will be created automatically on startup. either this or the DEFAULT_USERNAME variable can be used to set username                            |
| `DATAGATEWAY_PASSWORD`                 | The password of the new Datagateway client, is required for Bearer token authentication. the client will be created automatically on startup. either this or the DEFAULT_PASSWORD variable can be used to set username                         |
| `EPISODE_USERNAME`                     | The Username of the new Episode Service client, required for Bearer token authentication. the client will be created automatically on startup. either this or the DEFAULT_USERNAME variable can be used to set username                        |
| `EPISODE_PASSWORD`                     | The password of the new Episode Service client, required for Bearer token authentication. the client will be created automatically on startup. either this or the DEFAULT_PASSWORD variable can be used to set username                            |
| `INS_USERNAME`                         | The Username of the new INS client, required for Bearer token authentication. the client will be created automatically on startup. either this or the DEFAULT_USERNAME variable can be used to set username                                        |
| `INS_PASSWORD`                         | The password of the new INS client, required for Bearer token authentication. the client will be created automatically on startup. either this or the DEFAULT_PASSWORD variable can be used to set username                                                              |
| `IAM_USERNAME`                         | The Username of the new IAM client, required for Bearer token authentication. the client will be created automatically on startup. either this or the DEFAULT_USERNAME variable can be used to set username                                        |
| `IAM_PASSWORD`                         | The password of the new IAM client, required for Bearer token authentication. the client will be created automatically on startup. either this or the DEFAULT_PASSWORD variable can be used to set username                                                              |

## Verifying Installation

to verify the setup you can open the following URLs in the browser, if it shows the status: UP that means the application is running

Example of a success response
```json
{
  "status": "UP",
  "components": {
    "db": {
      "status": "UP",
      "details": {
        "database": "PostgreSQL",
        "result": 1,
        "validationQuery": "SELECT 1"
      }
    }
  }
}
```

| Application | Health URL |
| -- | -- |
| INS | [http://localhost:9100/actuator/health](http://localhost:9100/actuator/health) | 
| IAM | [http://localhost:9090/actuator/health](http://localhost:9090/actuator/health) | 
| Dispensation Service | [http://localhost:9080/actuator/health](http://localhost:9080/actuator/health) | 
| Episode Service | [http://localhost:8787/actuator/health](http://localhost:8787/actuator/health) | 
| Data Gateway | [http://localhost:8080/actuator/health](http://localhost:8080/actuator/health) | 
