version: "3.3"
services:
  setup:
    image: docker.elastic.co/elasticsearch/elasticsearch:8.6.1
    volumes:
      - certs:/usr/share/elasticsearch/config/certs
    user: "0"
    command: >
      bash -c '
        if [ ! -f config/certs/ca.zip ]; then
          echo "Creating CA";
          bin/elasticsearch-certutil ca --silent --pem -out config/certs/ca.zip;
          unzip config/certs/ca.zip -d config/certs;
        fi;
        if [ ! -f config/certs/certs.zip ]; then
          echo "Creating certs";
          echo -ne \
          "instances:\n"\
          "  - name: elastic-search\n"\
          "    dns:\n"\
          "      - elastic-search\n"\
          "      - localhost\n"\
          "    ip:\n"\
          "      - 127.0.0.1\n"\
          > config/certs/instances.yml;
          bin/elasticsearch-certutil cert --silent --pem -out config/certs/certs.zip --in config/certs/instances.yml --ca-cert config/certs/ca/ca.crt --ca-key config/certs/ca/ca.key;
          unzip config/certs/certs.zip -d config/certs;
        fi;
        echo "Setting file permissions"
        chown -R root:root config/certs;
        find . -type d -exec chmod 750 \{\} \;;
        find . -type f -exec chmod 640 \{\} \;;
        echo "All done!";
        ls -l config/certs
      '
    healthcheck:
      test: ["CMD-SHELL", "[ -f config/certs/elastic-search/elastic-search.crt ]"]
      interval: 1s
      timeout: 5s
      retries: 120
  elastic-search:
    depends_on:
      setup:
        condition: service_completed_successfully
    image: docker.elastic.co/elasticsearch/elasticsearch:8.6.1
    volumes:
      - certs:/usr/share/elasticsearch/config/certs
      - esdata:/usr/share/elasticsearch/data
    environment:
      - node.name=elastic-search
      - cluster.name=docker-cluster
      - cluster.initial_master_nodes=elastic-search
      - ELASTIC_PASSWORD=${ELASTIC_PASSWORD}
      - bootstrap.memory_lock=true
      - xpack.security.enabled=true
      - xpack.security.http.ssl.enabled=false
      - xpack.security.http.ssl.key=certs/elastic-search/elastic-search.key
      - xpack.security.http.ssl.certificate=certs/elastic-search/elastic-search.crt
      - xpack.security.http.ssl.certificate_authorities=certs/ca/ca.crt
      - xpack.security.transport.ssl.enabled=true
      - xpack.security.transport.ssl.key=certs/elastic-search/elastic-search.key
      - xpack.security.transport.ssl.certificate=certs/elastic-search/elastic-search.crt
      - xpack.security.transport.ssl.certificate_authorities=certs/ca/ca.crt
      - xpack.security.transport.ssl.verification_mode=certificate
      - xpack.license.self_generated.type=basic
      - node.store.allow_mmap=false
      - bootstrap.memory_lock=false
    mem_limit: 2g
    ulimits:
      memlock:
        soft: -1
        hard: -1
    healthcheck:
      test:
        [
          "CMD-SHELL",
          "curl -s --cacert config/certs/ca/ca.crt http://localhost:9200 | grep -q 'missing authentication credentials'",
        ]
      interval: 10s
      timeout: 10s
      retries: 120

  elastic-mappings:
    image: alpine/curl
    restart: "no"
    depends_on:
      elastic-search:
        condition: service_healthy
    environment:
      - ELASTIC_USERNAME=${ELASTIC_USERNAME}
      - ELASTIC_PASSWORD=${ELASTIC_PASSWORD}
    volumes:
      - elastic_scripts:/scripts
    command: /bin/sh /scripts/change-mappings.sh
    healthcheck:
      test:
        [
          "CMD-SHELL",
          "curl -s --cacert GET http://localhost:9200/_cluster/health -u elastic:EedS_8jId8TaNpMrnmwV",
        ]
      interval: 10s
      timeout: 10s
      retries: 120

volumes:
  certs:
    driver: local
  esdata:
    driver: local