version: "3.3"
services:
  data-gateway:
    image: registry.gitlab.com/everwell/hub-foss/data-gateway:latest-master
    restart: unless-stopped
    ports:
      - "8080:8080"
    depends_on:
      iam-backend:
        condition: service_healthy
      ins:
        condition: service_healthy
      user-service:
        condition: service_healthy
      redis:
        condition: service_healthy
      postgres:
        condition: service_healthy
      rabbitmq:
        condition: service_healthy
    environment:
      - JWT_SECRET=${JWT_SECRET}
      - POSTGRESQL_APPLICATION_USER=${POSTGRESQL_APPLICATION_USER}
      - POSTGRESQL_APPLICATION_USER_PASSWORD=${POSTGRESQL_APPLICATION_USER_PASSWORD}
      - POSTGRESQL_DB=datagateway
      - POSTGRESQL_HOST=postgres
      - POSTGRESQL_PORT=5432
      - RABBITMQ_HOST=rabbitmq
      - RABBITMQ_PORT:5672
      - RABBITMQ_USERNAME=${RABBITMQ_USERNAME}
      - RABBITMQ_PASSWORD=${RABBITMQ_PASSWORD}
      - REDIS_HOST=redis
      - REDIS_PORT=6379
      - REDIS_PASSWORD=${REDIS_PASSWORD}
    healthcheck:
      test: ["CMD", "curl", "-f", "http://localhost:8080/actuator/health"]
      interval: 30s
      timeout: 10s
      retries: 10
      # start_period: 40s
  dispensation-service:
    image: registry.gitlab.com/everwell/hub-foss/dispensation-service:latest-master
    restart: unless-stopped
    ports:
      - "9080:9080"
    depends_on:
      redis:
        condition: service_healthy
      postgres:
        condition: service_healthy
      rabbitmq:
        condition: service_healthy
      elastic-search:
        condition: service_healthy
    environment:
      - POSTGRESQL_APPLICATION_USER=${POSTGRESQL_APPLICATION_USER}
      - POSTGRESQL_APPLICATION_USER_PASSWORD=${POSTGRESQL_APPLICATION_USER_PASSWORD}
      - POSTGRESQL_DB=dispensation
      - POSTGRESQL_HOST=postgres
      - POSTGRESQL_PORT=5432
      - RABBITMQ_HOST=rabbitmq
      - RABBITMQ_PORT=5672
      - RABBITMQ_USERNAME=${RABBITMQ_USERNAME}
      - RABBITMQ_PASSWORD=${RABBITMQ_PASSWORD}
      - REDIS_HOST=redis
      - REDIS_PORT=6379
      - REDIS_PASSWORD=${REDIS_PASSWORD}
      - ELASTIC_HOST=elastic-search
      - ELASTIC_USERNAME=${ELASTIC_USERNAME}
      - ELASTIC_PASSWORD=${ELASTIC_PASSWORD}
      - ELASTIC_PROTOCOL=http
      - ELASTIC_PORT=9200
    healthcheck:
      test: ["CMD", "curl", "-f", "http://localhost:9080/actuator/health"]
      interval: 30s
      timeout: 10s
      retries: 10

  iam-backend:
    image: registry.gitlab.com/everwell/hub-foss/iam-backend:latest-master
    restart: unless-stopped
    ports:
      - "9090:9090"
    depends_on:
      redis:
        condition: service_healthy
      postgres:
        condition: service_healthy
      rabbitmq:
        condition: service_healthy
    environment:
      - POSTGRESQL_APPLICATION_USER=${POSTGRESQL_APPLICATION_USER}
      - POSTGRESQL_APPLICATION_USER_PASSWORD=${POSTGRESQL_APPLICATION_USER_PASSWORD}
      - POSTGRESQL_DB=adherence
      - POSTGRESQL_HOST=postgres
      - POSTGRESQL_PORT=5432
      - RABBITMQ_HOST=rabbitmq
      - RABBITMQ_PORT=5672
      - RABBITMQ_USERNAME=${RABBITMQ_USERNAME}
      - RABBITMQ_PASSWORD=${RABBITMQ_PASSWORD}
      - REDIS_HOST=redis
      - REDIS_PORT=6379
      - REDIS_PASSWORD=${REDIS_PASSWORD}
      - SENTRY_VERSION=0.1.0
    healthcheck:
      test: ["CMD", "curl", "-f", "http://localhost:9090/actuator/health"]
      interval: 30s
      timeout: 10s
      retries: 10
      # start_period: 40s

  ins:
    image: registry.gitlab.com/everwell/hub-foss/ins:latest-master
    restart: unless-stopped
    ports:
      - "9100:9100"
    depends_on:
      postgres:
        condition: service_healthy
      rabbitmq:
        condition: service_healthy
    environment:
      - KEYSTORE_PASSWORD=
      - POSTGRESQL_APPLICATION_USER=${POSTGRESQL_APPLICATION_USER}
      - POSTGRESQL_APPLICATION_USER_PASSWORD=${POSTGRESQL_APPLICATION_USER_PASSWORD}
      - POSTGRESQL_DB=ins
      - POSTGRESQL_HOST=postgres
      - POSTGRESQL_PORT=5432
      - RABBITMQ_HOST=rabbitmq
      - RABBITMQ_PORT=5672
      - RABBITMQ_USERNAME=${RABBITMQ_USERNAME}
      - RABBITMQ_PASSWORD=${RABBITMQ_PASSWORD}
    healthcheck:
      test: ["CMD", "curl", "-f", "http://localhost:9100/actuator/health"]
      interval: 30s
      timeout: 10s
      retries: 10
      # start_period: 40s
  episode-service-init:
    image: alpine:3
    restart: "no"
    depends_on:
      iam-backend:
        condition: service_healthy
      ins:
        condition: service_healthy
      data-gateway:
        condition: service_healthy
      postgres:
        condition: service_healthy
    environment:
      - DATAGATEWAY_PASSWORD=${DATAGATEWAY_PASSWORD}
      - DATAGATEWAY_USERNAME=${DATAGATEWAY_USERNAME}
      - EPISODE_CLIENT_ID=${EPISODE_CLIENT_ID}
      - EPISODE_PASSWORD=${EPISODE_PASSWORD}
      - EPISODE_USERNAME=${EPISODE_USERNAME}
      # iam client id same as episode client id
      - IAM_CLIENT_ID=${EPISODE_CLIENT_ID}
      - IAM_PASSWORD=${IAM_PASSWORD}
      - IAM_USERNAME=${IAM_USERNAME}
      # ins client id same as episode client id
      - INS_CLIENT_ID=${EPISODE_CLIENT_ID}
      - INS_PASSWORD=${INS_PASSWORD}
      - INS_USERNAME=${INS_USERNAME}
      - POSTGRESQL_HOST=postgres
      - POSTGRESQL_PORT=5432
      - POSTGRESQL_APPLICATION_USER=${POSTGRESQL_APPLICATION_USER}
      - POSTGRESQL_APPLICATION_USER_PASSWORD=${POSTGRESQL_APPLICATION_USER_PASSWORD}
    volumes:
      - episode_scripts:/scripts
    command: /bin/sh /scripts/register-clients.sh

  episode-service:
    image: registry.gitlab.com/everwell/hub-foss/episode-service:latest-master
    restart: unless-stopped
    ports:
      - "8787:8787"
    depends_on:
      episode-service-init:
        condition: service_completed_successfully
      elastic-search:
        condition: service_healthy
      iam-backend:
        condition: service_healthy
      ins:
        condition: service_healthy
      data-gateway:
        condition: service_healthy
      postgres:
        condition: service_healthy
      rabbitmq:
        condition: service_healthy
      redis:
        condition: service_healthy
    environment:
      - POSTGRESQL_HOST=postgres
      - POSTGRESQL_PORT=5432
      - POSTGRESQL_DB=episode
      - POSTGRESQL_APPLICATION_USER=${POSTGRESQL_APPLICATION_USER}
      - POSTGRESQL_APPLICATION_USER_PASSWORD=${POSTGRESQL_APPLICATION_USER_PASSWORD}
      - RABBITMQ_HOST=rabbitmq
      - RABBITMQ_PORT=5672
      - RABBITMQ_USERNAME=${RABBITMQ_USERNAME}
      - RABBITMQ_PASSWORD=${RABBITMQ_PASSWORD}
      - REDIS_HOST=redis
      - REDIS_PORT=6379
      - REDIS_PASSWORD=${REDIS_PASSWORD}
      - DATAGATEWAY_USERNAME=${DATAGATEWAY_USERNAME}
      - DATAGATEWAY_PASSWORD=${DATAGATEWAY_PASSWORD}
      - EPISODE_CLIENT_ID=${EPISODE_CLIENT_ID}
      - ELASTIC_HOST=elastic-search
      - ELASTIC_USERNAME=${ELASTIC_USERNAME}
      - ELASTIC_PASSWORD=${ELASTIC_PASSWORD}
      - ELASTIC_PROTOCOL=http
      - ELASTIC_PORT=9200
      - JWT_SECRET=${JWT_SECRET}
    healthcheck:
      test: ["CMD", "curl", "-f", "http://localhost:8787/actuator/health"]
      interval: 30s
      timeout: 10s
      retries: 10
      start_period: 40s

  user-service:
    image: registry.gitlab.com/everwell/hub-foss/user-service:latest-master
    restart: unless-stopped
    ports:
      - "9099:9099"
    depends_on:
      postgres:
        condition: service_healthy
      rabbitmq:
        condition: service_healthy
    environment:
      - POSTGRESQL_HOST=postgres
      - POSTGRESQL_PORT=5432
      - POSTGRESQL_DB=userservice
      - POSTGRESQL_APPLICATION_USER=${POSTGRESQL_APPLICATION_USER}
      - POSTGRESQL_APPLICATION_USER_PASSWORD=${POSTGRESQL_APPLICATION_USER_PASSWORD}
      - RABBITMQ_HOST=rabbitmq
      - RABBITMQ_PORT=5672
      - RABBITMQ_USERNAME=${RABBITMQ_USERNAME}
      - RABBITMQ_PASSWORD=${RABBITMQ_PASSWORD}
      - REDIS_HOST=redis
      - REDIS_PORT=6379
      - REDIS_PASSWORD=${REDIS_PASSWORD}
      - SENTRY_VERSION=0.1.0
    healthcheck:
      test: [ "CMD", "curl", "-f", "http://localhost:9099/actuator/health" ]
      interval: 30s
      timeout: 10s
      retries: 10
      start_period: 40s

  redis:
    image: bitnami/redis:6.2.13-debian-11-r25
    restart: unless-stopped
    volumes:
      - redis-data:/bitnami/redis/data
    environment:
      - ALLOW_EMPTY_PASSWORD=no
      - REDIS_PASSWORD=${REDIS_PASSWORD}
    healthcheck:
      test: ["CMD", "/bin/bash", "-c", 'REDISCLI_AUTH="$REDIS_PASSWORD" redis-cli -h localhost ping | grep PONG']
      interval: 30s
      timeout: 10s
      retries: 10
  postgres:
    image: bitnami/postgresql:11.21.0-debian-11-r0
    restart: unless-stopped
    volumes:
      - postgres_config:/docker-entrypoint-initdb.d/
      - postgres-data:/bitnami/postgresql
    environment:
      # postgres user password
      - POSTGRESQL_PASSWORD=${POSTGRESQL_PASSWORD}
      - POSTGRESQL_APPLICATION_USER=${POSTGRESQL_APPLICATION_USER}
      - POSTGRESQL_APPLICATION_USER_PASSWORD=${POSTGRESQL_APPLICATION_USER_PASSWORD}
    healthcheck:
      test: ["CMD", "/bin/bash", "-c", 'exec pg_isready -U "${POSTGRESQL_APPLICATION_USER}" -d "dbname=postgres" -h 127.0.0.1 -p 5432']
      interval: 30s
      timeout: 10s
      retries: 10
      # start_period: 30s

  rabbitmq:
    image: rabbitmq:3.9
    restart: unless-stopped
    volumes:
      - rabbitmq-data:/bitnami  
    environment:
      - RABBITMQ_DEFAULT_USER=${RABBITMQ_USERNAME}
      - RABBITMQ_DEFAULT_PASS=${RABBITMQ_PASSWORD}
    healthcheck:
      test: ["CMD", "/bin/bash", "-ec", "rabbitmq-diagnostics -q ping"]
      interval: 30s
      timeout: 10s
      retries: 10
      # start_period: 30s

volumes:
  redis-data: {}
  postgres-data: {}
  rabbitmq-data: {}
  postgres_config:
    external: true
    name: ${NAMESPACE:-everwell}_postgres_config
  episode_scripts:
    external: true
    name: ${NAMESPACE:-everwell}_episode_scripts
  elastic_scripts:
    external: true
    name: ${NAMESPACE:-everwell}_elastic_scripts
