init_default_values() {
    export DEFAULT_USERNAME=${DATAGATEWAY_USERNAME:-INTERNAL_CLIENT}
    export DEFAULT_PASSWORD=${DATAGATEWAY_PASSWORD:-WMfP5YbwCZcxJY@Mqy-r}
    export NAMESPACE=${NAMESPACE:-everwell}
    export POSTGRESQL_APPLICATION_USER=${POSTGRESQL_APPLICATION_USER:-everwell}
    export POSTGRESQL_APPLICATION_USER_PASSWORD=${POSTGRESQL_APPLICATION_USER_PASSWORD:-TX_UFEBptLH8P@yDBTJu}
    export POSTGRESQL_PASSWORD=${POSTGRESQL_PASSWORD:-BCxrxSRcSLxV63ntsby4}
    export RABBITMQ_USERNAME=${RABBITMQ_USERNAME:-user}
    export RABBITMQ_PASSWORD=${RABBITMQ_PASSWORD:-NTZSxK37vWPTEGa-7HVt}
    export REDIS_PASSWORD=${REDIS_PASSWORD:-bCuSNGAnYBvjXeSeTZ4y}
    export JWT_SECRET=${JWT_SECRET:-tDYp44Fe9UwmMbkI5J9x}
    export SENTRY_VERSION=${SENTRY_VERSION:-0.1.0}
    export ELASTIC_USERNAME=${ELASTIC_USERNAME:-elastic}
    export ELASTIC_PASSWORD=${ELASTIC_PASSWORD:-EedS_8jId8TaNpMrnmwV}
    export DATAGATEWAY_USERNAME=${DATAGATEWAY_USERNAME:-${DEFAULT_USERNAME}}
    export DATAGATEWAY_PASSWORD=${DATAGATEWAY_PASSWORD:-${DEFAULT_PASSWORD}}
    export EPISODE_USERNAME=${EPISODE_USERNAME:-${DEFAULT_USERNAME}}
    export EPISODE_PASSWORD=${EPISODE_PASSWORD:-${DEFAULT_PASSWORD}}
    export EPISODE_CLIENT_ID=${EPISODE_CLIENT_ID:-1}
    export INS_USERNAME=${INS_USERNAME:-${DEFAULT_USERNAME}}
    export INS_PASSWORD=${INS_PASSWORD:-${DEFAULT_PASSWORD}}
    export IAM_USERNAME=${IAM_USERNAME:-${DEFAULT_USERNAME}}
    export IAM_PASSWORD=${IAM_PASSWORD:-${DEFAULT_PASSWORD}}
}

kubectl get ns ${NAMESPACE} || kubectl create ns ${NAMESPACE}
k8sMainRootFilePath=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
init_default_values

create_secret_files() {
# Data Gateway
cat <<EOF > ${k8sMainRootFilePath}/data-gateway/.env.secret-jwt-secret
secret=${JWT_SECRET}
EOF

cat <<EOF > ${k8sMainRootFilePath}/postgres/.env.secret-pgsql-appuser
password=${POSTGRESQL_APPLICATION_USER_PASSWORD}
username=${POSTGRESQL_APPLICATION_USER}
EOF

cat <<EOF > ${k8sMainRootFilePath}/postgres/.env.secret-postgres
postgres-password=${POSTGRESQL_PASSWORD}
EOF

# RabbitMQ
cat <<EOF > ${k8sMainRootFilePath}/rabbitmq/.env.rabbitmq
rabbitmq-password=${RABBITMQ_PASSWORD}
rabbitmq-username=${RABBITMQ_USERNAME}
EOF

# Postgres
cat <<EOF > ${k8sMainRootFilePath}/postgres/.env.secret-pgsql-appuser
password=${POSTGRESQL_APPLICATION_USER_PASSWORD}
username=${POSTGRESQL_APPLICATION_USER}
EOF

cat <<EOF > ${k8sMainRootFilePath}/postgres/.env.secret-postgres
postgres-password=${POSTGRESQL_PASSWORD}
EOF

# Redis
cat <<EOF > ${k8sMainRootFilePath}/redis/.env.secret-redis
redis-password=${REDIS_PASSWORD}
EOF

# elastic search
cat <<EOF > ${k8sMainRootFilePath}/elastic-search/.env.secret-elasticsearch
elastic-password=${ELASTIC_PASSWORD}
elastic-user=${ELASTIC_USERNAME}
EOF

# IAM Backend
cat <<EOF > ${k8sMainRootFilePath}/iam-backend/.env.secret-jwt-secret
secret=${JWT_SECRET}
EOF

# episode
cat <<EOF > ${k8sMainRootFilePath}/episode-service/.env.secret-episode-client
username=${EPISODE_USERNAME}
password=${EPISODE_PASSWORD}
id=${EPISODE_CLIENT_ID}
EOF

cat <<EOF > ${k8sMainRootFilePath}/episode-service/.env.secret-ins-client
username=${INS_USERNAME}
password=${INS_PASSWORD}
id=${EPISODE_CLIENT_ID}
EOF

cat <<EOF > ${k8sMainRootFilePath}/episode-service/.env.secret-iam-client
username=${IAM_USERNAME}
password=${IAM_PASSWORD}
id=${EPISODE_CLIENT_ID}
EOF


cat <<EOF > ${k8sMainRootFilePath}/episode-service/.env.secret-datagateway-client
username=${DATAGATEWAY_USERNAME}
password=${DATAGATEWAY_PASSWORD}
EOF

cat <<EOF > ${k8sMainRootFilePath}/episode-service/.env.secret-jwt-secret
secret=${JWT_SECRET}
EOF
}

cleanup() {
    rm ${k8sMainRootFilePath}/data-gateway/.env.secret-jwt-secret

    rm ${k8sMainRootFilePath}/postgres/.env.secret-pgsql-appuser
    rm ${k8sMainRootFilePath}/postgres/.env.secret-postgres

    rm ${k8sMainRootFilePath}/rabbitmq/.env.rabbitmq

    rm ${k8sMainRootFilePath}/redis/.env.secret-redis

    rm ${k8sMainRootFilePath}/elastic-search/.env.secret-elasticsearch

    rm ${k8sMainRootFilePath}/iam-backend/.env.secret-jwt-secret

    rm ${k8sMainRootFilePath}/episode-service/.env.secret-episode-client
    rm ${k8sMainRootFilePath}/episode-service/.env.secret-ins-client
    rm ${k8sMainRootFilePath}/episode-service/.env.secret-iam-client
    rm ${k8sMainRootFilePath}/episode-service/.env.secret-datagateway-client
    rm ${k8sMainRootFilePath}/episode-service/.env.secret-jwt-secret
}

exitCode=0
if [ "$1" == "init" ]; then
    create_secret_files
    kubectl apply -n ${NAMESPACE} -k ${k8sMainRootFilePath}
    exitCode=$?
    cleanup
elif [ "$1" == "up" ]; then
    create_secret_files
    kubectl apply -n ${NAMESPACE} -k ${k8sMainRootFilePath}
    exitCode=$?
    cleanup
elif [ "$1" == "down" ]; then
    create_secret_files
    kubectl delete deployments.apps,statefulsets.apps -n ${NAMESPACE} -l package=everwell
    exitCode=$?
    cleanup
elif [ "$1" == "destroy" ]; then
    create_secret_files
    kubectl delete -n ${NAMESPACE} -k ${k8sMainRootFilePath}
    exitCode=$?
    kubectl delete persistentvolumeclaims -n ${NAMESPACE} -l package=everwell || true
    kubectl delete configmaps -n ${NAMESPACE} -l package=everwell || true
    cleanup
else
    echo "Valid options are: up, down, or destroy"
    exitCode=1
fi

exit $exitCode