package com.everwell.userservice.mapper;


import java.sql.ResultSet;
import java.sql.SQLException;

import com.everwell.userservice.models.dtos.vitals.VitalsTimeData;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

@Component
public class VitalsTimeDataRowMapper implements RowMapper<VitalsTimeData> {

    @Override
    public VitalsTimeData mapRow(final ResultSet resultSet, final int rowNum) throws SQLException {
        VitalsTimeData vitalsTimeData = new VitalsTimeData();
        vitalsTimeData.setTime(resultSet.getTimestamp(1).toString());
        vitalsTimeData.setUserId(resultSet.getLong(2));
        vitalsTimeData.setVitalId(resultSet.getLong(3));
        vitalsTimeData.setValue(String.valueOf(resultSet.getObject(4)));
        return vitalsTimeData;
    }
}