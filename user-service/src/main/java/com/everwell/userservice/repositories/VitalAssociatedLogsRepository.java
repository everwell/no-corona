package com.everwell.userservice.repositories;

import com.everwell.userservice.models.db.timescale.VitalAssociatedLogs;
import com.everwell.userservice.models.dtos.vitals.VitalsTimeData;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;

@Repository
public interface VitalAssociatedLogsRepository {
    void saveAll(List<VitalAssociatedLogs> vitalAssociatedLogsList);

    List<VitalsTimeData> fetchLogs(List<Long> userIdList, List<Long> vitalIdList, Timestamp startTimestamp, Timestamp endTimestamp);
}
