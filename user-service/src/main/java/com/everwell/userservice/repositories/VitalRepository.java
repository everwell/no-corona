package com.everwell.userservice.repositories;

import com.everwell.userservice.models.db.Vital;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VitalRepository  extends JpaRepository<Vital, Long> {
}
