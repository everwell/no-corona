package com.everwell.userservice.repositories;


import com.everwell.userservice.models.dtos.vitals.VitalsAggregatedTimeData;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;

@Repository
public interface VitalAggregateRepository {
    List<VitalsAggregatedTimeData> fetchData(List<Long> userIdList, List<Long> vitalIdList, Timestamp startTimestamp, Timestamp endTimestamp);
}
