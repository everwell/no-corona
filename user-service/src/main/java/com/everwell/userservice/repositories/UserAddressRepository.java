package com.everwell.userservice.repositories;


import com.everwell.userservice.models.db.UserAddress;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface UserAddressRepository extends JpaRepository<UserAddress, Long>
{
    UserAddress findByUserId(Long userId);
}