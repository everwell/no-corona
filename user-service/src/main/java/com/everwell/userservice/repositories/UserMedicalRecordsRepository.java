package com.everwell.userservice.repositories;

import com.everwell.userservice.models.db.UserMedicalRecords;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.transaction.annotation.Transactional;


public interface UserMedicalRecordsRepository extends JpaRepository<UserMedicalRecords, Long> {

   @Modifying
   @Transactional
   void deleteAllByConsentRequestId(String consentRequestId);
}
