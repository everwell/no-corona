package com.everwell.userservice.utils;

import com.everwell.userservice.constants.Constants;
import com.everwell.userservice.exceptions.ValidationException;
import com.everwell.userservice.models.dtos.Response;
import com.everwell.userservice.models.dtos.UserEmailRequest;
import com.everwell.userservice.models.dtos.UserMobileRequest;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.IOUtils;
import org.apache.commons.text.similarity.LevenshteinDistance;
import org.apache.logging.log4j.util.TriConsumer;
import org.springframework.util.ObjectUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.*;

public class Utils
{
    private static final ObjectMapper objMapper = new ObjectMapper();
    private static final Logger LOGGER = LoggerFactory.getLogger(Utils.class);

    public static <T> T convertObject(String from, Class<T> to) throws Exception
    {
        return objMapper.readValue(from, to);
    }

    //for generic classes
    public static <T> T convertObject (String json, TypeReference<T> typeReference) throws IOException {
        return objMapper.readValue(json, typeReference);
    }

    public static <T> T convertObject (Object from, Class<T> to) throws IOException {
        return objMapper.convertValue(from, to);
    }

    public static Date convertStringToDate(String date) throws ParseException
    {
        return convertStringToDate(date, "yyyy-MM-dd HH:mm:ss");
    }

    public static Date convertStringToDate(String date, String format) throws ParseException
    {
        return new SimpleDateFormat(format).parse(date);
    }

    public static String getFormattedDate(Date date, String format)
    {
        DateFormat dateFormat = new SimpleDateFormat(format);
        return dateFormat.format(date);
    }

    public static void ensureSinglePrimaryMobile(List<UserMobileRequest> mobileList)
    {
        boolean primarySelected = false;
        for(UserMobileRequest userMobileRequest: mobileList)
        {
            if(userMobileRequest.isPrimary())
            {
                if(!primarySelected)
                {
                    primarySelected = true;
                }
                else
                {
                    userMobileRequest.setPrimary(false);
                }
            }
        }

        if(!primarySelected && !mobileList.isEmpty())
        {
            mobileList.get(0).setPrimary(true);
        }
    }

    public static List<UserMobileRequest> removeDuplicateNumbers(List<UserMobileRequest> mobileList)
    {
        List<UserMobileRequest> uniqueList = new ArrayList<>();
        List<String> numberLookUp = new ArrayList<>();
        for(UserMobileRequest userMobileRequest: mobileList)
        {
            if(!numberLookUp.contains(userMobileRequest.getNumber()))
            {
                uniqueList.add(userMobileRequest);
                numberLookUp.add(userMobileRequest.getNumber());
            }
        }

        return uniqueList;
    }

    public static void ensureSinglePrimaryEmail(List<UserEmailRequest> emailList)
    {
        boolean primarySelected = false;
        for(UserEmailRequest userEmailRequest: emailList)
        {
            if(userEmailRequest.isPrimary())
            {
                if(!primarySelected)
                {
                    primarySelected = true;
                }
                else
                {
                    userEmailRequest.setPrimary(false);
                }
            }
        }

        if(!primarySelected && !emailList.isEmpty())
        {
            emailList.get(0).setPrimary(true);
        }
    }

    public static List<UserEmailRequest> removeDuplicateEmails(List<UserEmailRequest> emailList)
    {
        List<UserEmailRequest> uniqueList = new ArrayList<>();
        List<String> emailLookUp = new ArrayList<>();
        for(UserEmailRequest userEmailRequest: emailList)
        {
            if(!emailLookUp.contains(userEmailRequest.getEmailId()))
            {
                uniqueList.add(userEmailRequest);
                emailLookUp.add(userEmailRequest.getEmailId());
            }
        }

        return uniqueList;
    }

    public static String asJsonString(final Object obj) {
        try {
            return objMapper.writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static String readRequestBody(InputStream inputStream) throws IOException {
        StringWriter writer = null;
        if (inputStream != null) {
            writer = new StringWriter();
            IOUtils.copy(inputStream, writer, "UTF-8");
        }
        return writer != null ? writer.toString() : null;
    }


    public static <T> Boolean compareObjectsForValue(T oldObject, T newObject, Boolean objectValueChanged, TriConsumer<Field, Object, Object> function)  {
        try {
            for (Field field : oldObject.getClass().getDeclaredFields()) {
                field.setAccessible(true);
                Object oldObjectFieldValue = field.get(oldObject);
                Field newField = newObject.getClass().getDeclaredField(field.getName());
                newField.setAccessible(true);
                Object newObjectFieldValue = newField.get(newObject);
                if (newObjectFieldValue  == null ) {
                    function.accept(field, oldObjectFieldValue, null);
                } else if (ObjectUtils.isEmpty(newObjectFieldValue)){
                    function.accept(field, null, null);
                    objectValueChanged = objectValueChanged || (oldObjectFieldValue != null) ;
                } else if (!Objects.equals(oldObjectFieldValue, newObjectFieldValue)) {
                    objectValueChanged = true;
                }
            }
        } catch (Exception  ex) {
            throw new ValidationException("Error updating data");
        }
        return objectValueChanged;
    }

    public static Date getCurrentDate() {
        return formatDate(new Date());
    }

    public static Date formatDate(Date date) {
        return formatDate(date, "dd-MM-yyyy HH:mm:ss");
    }
    
    public static Date formatDate(Date date, String format) {
        try {
            date = convertStringToDate(getFormattedDate(date, format), format);
        } catch (ParseException e) {
            LOGGER.error("unable to convert record date " + date);
        }
        return date;
    }

    public static void writeCustomResponseToOutputStream(HttpServletResponse response, Exception e, HttpStatus httpStatus) throws IOException {
        writeCustomResponseToOutputStream(response, new Response(e.getMessage()), httpStatus);
    }

    public static void writeCustomResponseToOutputStream(HttpServletResponse response, Response CustomResponse, HttpStatus httpStatus) throws IOException {
        response.setContentType("application/json");
        response.setStatus(httpStatus.value());
        response.getOutputStream()
                .println(Utils.asJsonString(CustomResponse));
    }

    public static LocalDateTime convertStringToLocalDateTime(String date) {
        return convertStringToLocalDateTime(date, Constants.DATE_FORMAT);
    }

    public static LocalDateTime convertStringToLocalDateTime(String date, String format) {
        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
            return LocalDateTime.parse(date, formatter);
        } catch (DateTimeParseException ex) {
            throw new IllegalArgumentException("[convertStringToLocalDateTime] Unable to parse date: " + date + " format: " + format);
        }
    }

    public static List<Long> levenshteinMatchOnName(Map<Long,String> userNames, String actualName) {
        LevenshteinDistance levenshteinDistanceInstance = new LevenshteinDistance();
        int minDist = Integer.MAX_VALUE;
        List<Long> userIds = new ArrayList<>();
        for(Map.Entry<Long,String> entry : userNames.entrySet()) {
            int dist = levenshteinDistanceInstance.apply(entry.getValue().toLowerCase(),actualName.toLowerCase().replaceAll(" ",""));
            if(dist <= minDist) {
                if (dist != minDist) {
                    userIds = new ArrayList<>();
                }
                userIds.add(entry.getKey());
                minDist = dist;
            }
        }
        return userIds;
    }

    public static int getYearFromDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.get(Calendar.YEAR);
    }
}
