package com.everwell.userservice.controllers;


import com.everwell.userservice.models.db.Vital;
import com.everwell.userservice.models.dtos.Response;
import com.everwell.userservice.models.http.requests.VitalBulkRequest;
import com.everwell.userservice.models.http.requests.VitalRequest;
import com.everwell.userservice.models.http.responses.VitalResponse;
import com.everwell.userservice.services.VitalService;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.*;
import java.util.List;

import static com.everwell.userservice.constants.Constants.X_US_CLIENT_ID;

@RestController
public class VitalController {

    private static final Logger LOGGER = LoggerFactory.getLogger(VitalController.class);

    @Autowired
    private VitalService vitalService;

    @ApiOperation(
            value = "Get all Vitals Details",
            notes = "Used to get all the vitals details present on the system"
    )
    @GetMapping(value = "/v1/vitals")
    public ResponseEntity<Response<List<VitalResponse>>> getAllVitals(@RequestHeader(name = X_US_CLIENT_ID) Long clientId) {
        LOGGER.info("[getAllVitals] request received ");
        List<VitalResponse> vitalResponseList = vitalService.getAllVitalsResponse();
        Response<List<VitalResponse>> response = new Response<>(true, vitalResponseList);
        LOGGER.info("[getAllVitals] response generated : " + response);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation(
            value = "Get Vital Details by vital Id",
            notes = "Used to get vital details by Vital Id"
    )
    @GetMapping(value = "/v1/vitals/{id}")
    public ResponseEntity<Response<VitalResponse>> getVitalsById(@PathVariable("id") Long vitalId, @RequestHeader(name = X_US_CLIENT_ID) Long clientId) {
        LOGGER.info("[getVitalsById] request received for vitalId : " + vitalId);
        VitalResponse vitalResponse = vitalService.getVitalResponseById(vitalId);
        Response<VitalResponse> response = new Response<>(true, vitalResponse);
        LOGGER.info("[getVitalsById] response generated : " + response);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation(
            value = "Save Vitals Details",
            notes = "Used to save bulk vitals details"
    )
    @PostMapping(value = "/v1/vitals")
    public ResponseEntity<Response<List<Vital>>> saveVitals(@RequestBody VitalBulkRequest vitalBulkRequest, @RequestHeader(name = X_US_CLIENT_ID) Long clientId) {
        LOGGER.info("[saveVitals] request received : " + vitalBulkRequest);
        vitalBulkRequest.validate();
        List<Vital> vitalResponseList = vitalService.saveVitals(vitalBulkRequest);
        Response<List<Vital>> response = new Response<>(true, vitalResponseList);
        LOGGER.info("[saveVitals] response generated : " + response);
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @ApiOperation(
            value = "Update Vital Details by vital Id",
            notes = "Used to update vital details by Vital Id"
    )
    @PutMapping(value = "/v1/vitals/{id}")
    public ResponseEntity<Response<VitalResponse>> updateVitalsById(@PathVariable("id") Long vitalId, @RequestBody VitalRequest vitalRequest, @RequestHeader(name = X_US_CLIENT_ID) Long clientId) {
        LOGGER.info("[updateVitalsById] request received for : " + vitalId + " with body : " + vitalRequest);
        vitalRequest.validate();
        VitalResponse vitalResponse = vitalService.updateVitalsById(vitalId, vitalRequest);
        Response<VitalResponse> response = new Response<>(true, vitalResponse);
        LOGGER.info("[updateVitalsById] response generated : " + response);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation(
            value = "Delete Vital Details by vital Id",
            notes = "Used to delete vital details by Vital Id"
    )
    @DeleteMapping(value = "/v1/vitals/{id}")
    public ResponseEntity<Response<String>> deleteVitalsById(@PathVariable("id") Long vitalId) {
        LOGGER.info("[deleteVitalsById] request received for : " + vitalId);
        vitalService.deleteVitalsById(vitalId);
        return new ResponseEntity<>(new Response<>(true, "Vital deleted successfully"), HttpStatus.OK);
    }
}


