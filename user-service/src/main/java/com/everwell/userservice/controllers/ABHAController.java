package com.everwell.userservice.controllers;

import com.everwell.userservice.models.dtos.Response;
import com.everwell.userservice.models.dtos.UpdateUserRequest;
import com.everwell.userservice.models.http.requests.*;
import com.everwell.userservice.models.http.responses.*;
import com.everwell.userservice.services.ABHAService;
import com.everwell.userservice.services.UserService;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

import static com.everwell.userservice.constants.Constants.*;
import static com.everwell.userservice.constants.ABHAConstants.*;
import static com.everwell.userservice.enums.ABHARoute.*;

@RestController
public class ABHAController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ABHAController.class);

    @Autowired
    private ABHAService abhaService;

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/v1/abha/registration/aadhaar/generate-otp",  method = RequestMethod.POST)
    @ApiOperation(
            value = "Generate aadhaar OTP for ABHA generation",
            notes = "Step 1 of 5 in ABHA generation"
    )
    public ResponseEntity<Response<AadhaarOTPResponse>> generateOtpForAadhaar(@RequestBody AadhaarOTPRequest aadhaarOTPRequest) throws IOException {
        LOGGER.info("[GenerateOtpForAadhaar] request received : " + aadhaarOTPRequest);
        aadhaarOTPRequest.validate();
        aadhaarOTPRequest.decryptData();
        AadhaarOTPResponse aadhaarOTPResponse = abhaService.generateOtp(aadhaarOTPRequest, GENERATE_OTP_AADHAAR.getPath());
        Response<AadhaarOTPResponse> response = new Response<>(aadhaarOTPResponse, "Aadhaar OTP for ABHA creation generated successfully");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/v1/abha/registration/aadhaar/resend-otp",  method = RequestMethod.POST)
    @ApiOperation(
            value = "Resend aadhaar OTP for ABHA generation",
            notes = "Step 1 of 5 in ABHA generation"
    )
    public ResponseEntity<Response> resendOtpForAadhaar(@RequestBody ResendOTPRequest resendOTPRequest) throws IOException {
        LOGGER.info("[ResendOtpForAadhaar] request received : " + resendOTPRequest);
        resendOTPRequest.validate();
        abhaService.resendOtp(resendOTPRequest, RESEND_OTP_AADHAAR.getPath());
        Response response = new Response<>(null, "Resend aadhaar OTP for ABHA creation generated successfully");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/v1/abha/registration/aadhaar/verify-otp",  method = RequestMethod.POST)
    @ApiOperation(
            value = "Verify aadhaar/mobile OTP for ABHA generation",
            notes = "Step 2 and 4 of 5 in ABHA generation"
    )
    public ResponseEntity<Response<AadhaarOTPResponse>> verifyOtp(@RequestParam(value = "otpType", required = true) String otpType, @RequestBody OTPVerifyRequest otpVerifyRequest) throws IOException {
        LOGGER.info("[VerifyOtp] request received : " + otpVerifyRequest);
        otpVerifyRequest.validate();
        AadhaarOTPResponse aadhaarOTPResponse = abhaService.verifyOtp(otpVerifyRequest, otpType);
        Response<AadhaarOTPResponse> response = new Response<>(aadhaarOTPResponse, otpType + " OTP for ABHA creation verified successfully");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/v1/abha/registration/aadhaar/generate-mobile-otp",  method = RequestMethod.POST)
    @ApiOperation(
            value = "Generate mobile OTP for ABHA generation",
            notes = "Step 3 of 5 in ABHA generation"
    )
    public ResponseEntity<Response<AadhaarOTPResponse>> generateOtpForMobile(@RequestBody MobileOTPRequest mobileOTPRequest) throws IOException {
        LOGGER.info("[GenerateOtpForMobile] request received : " + mobileOTPRequest);
        mobileOTPRequest.validate();
        AadhaarOTPResponse aadhaarOTPResponse = abhaService.generateOtp(mobileOTPRequest, GENERATE_OTP_MOBILE.getPath());
        Response<AadhaarOTPResponse> response = new Response<>(aadhaarOTPResponse, "Mobile OTP for ABHA creation generated successfully");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/v1/abha/registration/aadhaar/create-abha",  method = RequestMethod.POST)
    @ApiOperation(
            value = "Create ABHA",
            notes = "Step 5 of 5 in ABHA generation"
    )
    public ResponseEntity<Response<ABHACreateResponse>> createAbha(@RequestHeader(name = X_US_CLIENT_ID) Long clientId, @RequestBody ABHACreateRequest abhaCreateRequest) throws Exception {
        LOGGER.info("[CreateAbha] request received : " + abhaCreateRequest);
        abhaCreateRequest.validate();
        ABHACreateResponse abhaCreateResponse = abhaService.createAbha(abhaCreateRequest);
        UpdateUserRequest updateUserRequest = abhaService.buildUpdateUserReq(abhaCreateRequest.getUserId(), abhaCreateRequest.getTxnId(), abhaCreateResponse);
        userService.update(updateUserRequest, clientId);
        Response<ABHACreateResponse> response = new Response<>(abhaCreateResponse, "ABHA created successfully");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/v1/abha/search/health-id",  method = RequestMethod.POST)
    @ApiOperation(
            value = "Search ABHA using health ID",
            notes = "Step 1 of 4 in existing ABHA linking"
    )
    public ResponseEntity<Response<ABHASearchResponse>> searchAbha(@RequestBody ABHASearchRequest abhaSearchRequest) throws IOException {
        LOGGER.info("[SearchAbha] request received : " + abhaSearchRequest);
        abhaSearchRequest.validate();
        ABHASearchResponse abhaSearchResponse = abhaService.searchAbha(abhaSearchRequest);
        Response<ABHASearchResponse> response = new Response<>(abhaSearchResponse, "ABHA found successfully");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/v1/abha/auth/generate-otp/health-id",  method = RequestMethod.POST)
    @ApiOperation(
            value = "Generate OTP using health ID (Number linked with aadhaar will be used)",
            notes = "Step 2 of 4 in existing ABHA linking"
    )
    public ResponseEntity<Response<AadhaarOTPResponse>> generateOtpForHealthId(@RequestBody HealthIdOTPRequest healthIdOTPRequest) throws IOException {
        LOGGER.info("[GenerateOtpForHealthId] request received : " + healthIdOTPRequest);
        healthIdOTPRequest.validate();
        AadhaarOTPResponse aadhaarOTPResponse = abhaService.generateOtp(healthIdOTPRequest, GENERATE_OTP_AUTH.getPath());
        Response<AadhaarOTPResponse> response = new Response<>(aadhaarOTPResponse, "Aadhaar OTP for given health ID generated successfully");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/v1/abha/auth/resend-otp",  method = RequestMethod.POST)
    @ApiOperation(
            value = "Resend aadhaar OTP",
            notes = "Step 2 of 4 in existing ABHA linking"
    )
    public ResponseEntity<Response> resendAuthOtp(@RequestBody ResendOTPRequest resendOTPRequest) throws IOException {
        LOGGER.info("[ResendAuthOtp] request received : " + resendOTPRequest);
        resendOTPRequest.validateForAuth();
        abhaService.resendOtp(resendOTPRequest, RESEND_OTP_AUTH.getPath());
        Response response = new Response<>(null, "Resend aadhaar OTP generated successfully");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/v1/abha/auth/confirm-with-aadhaar-otp",  method = RequestMethod.POST)
    @ApiOperation(
            value = "Confirm OTP sent to aadhaar number linked to health ID",
            notes = "Step 3 of 4 in existing ABHA linking"
    )
    public ResponseEntity<Response<ABHAAuthResponse>> confirmWithAadhaarOtp(@RequestHeader(name = X_US_CLIENT_ID) Long clientId, @RequestBody OTPVerifyRequest otpVerifyRequest) throws Exception {
        LOGGER.info("[confirmWithAadhaarOtp] request received : " + otpVerifyRequest);
        otpVerifyRequest.validateForUserUpdate();
        ABHAAuthResponse abhaAuthResponse = abhaService.verifyOtpForAuth(otpVerifyRequest);
        ABHAProfileResponse abhaProfileResponse = abhaService.getProfile(abhaAuthResponse.getToken());
        UpdateUserRequest updateUserRequest = abhaService.buildUpdateUserReq(otpVerifyRequest.getUserId(), otpVerifyRequest.getTxnId(), abhaProfileResponse);
//        userService.update(updateUserRequest, clientId);
        Response<ABHAAuthResponse> response = new Response<>(abhaAuthResponse, "Aadhaar OTP for given health ID verified successfully");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/v1/abha/account/profile",  method = RequestMethod.GET)
    @ApiOperation(
            value = "Get ABHA profile using token after authentication",
            notes = "Step 4 of 4 in existing ABHA linking"
    )
    public ResponseEntity<Response<ABHAProfileResponse>> getAbhaProfile(@RequestHeader(name = ABHA_X_TOKEN) String authToken) throws IOException {
        LOGGER.info("[GetAbhaProfile] request received : " + authToken);
        ABHAProfileResponse abhaProfileResponse = abhaService.getProfile(authToken);
        Response<ABHAProfileResponse> response = new Response<>(abhaProfileResponse, "ABHA profile retrieved successfully");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/v1/abha/account/image",  method = RequestMethod.GET)
    @ApiOperation(
            value = "Get ABHA images using token after authentication",
            notes = "Step 4 of 4 in existing ABHA linking"
    )
    public ResponseEntity<Response<byte[]>> getAbhaImage(@RequestParam(value = "imageName", required = true) String imageName, @RequestParam(name = "userId") Long userId) throws IOException {
        LOGGER.info("[GetPngCard] request received : " + userId);
        byte[] abhaImageBytes = abhaService.getAbhaImage(userId, imageName);
        Response<byte[]> response = new Response<>(abhaImageBytes, imageName + " ABHA PNG retrieved successfully");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    //// Login through mobile : V1 working /////

    @RequestMapping(value = "/v1/mobileEmail/login/generateMobileEmailOtp",  method = RequestMethod.POST)
    @ApiOperation(
            value = "Generate Mobile OTP for ABHA login",
            notes = "Step 1 for ABHA mobile login"
    )
    public ResponseEntity<Response<MobileEmailOTPResponse>> generateOtpForMobileEmailLogin(@RequestBody MobileEmailOTPRequest mobileEmailOTPRequest) throws IOException {
        LOGGER.info("Generate Mobile OTP request received : " + mobileEmailOTPRequest);
        mobileEmailOTPRequest.validate();
        MobileEmailOTPResponse aadhaarOTPResponse = abhaService.generateOtpForMobileEmailLogin(mobileEmailOTPRequest, GENERATE_OTP_FOR_MOBILE_EMAIL_LOGIN.getPath());
        Response<MobileEmailOTPResponse> response = new Response<>(aadhaarOTPResponse, "Mobile/Email OTP generated successfully");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/v1/mobileEmail/login/verify-otp",  method = RequestMethod.POST)
    @ApiOperation(
            value = "Verify mobile OTP for ABHA login",
            notes = "Step 2 for ABHA mobile login"
    )
    public ResponseEntity<Response<MobileEmailVerifyOTPResponse>> verifyOtpForMobileEmailLogin(@RequestBody MobileEmailVerifyOTPRequest mobileEmailVerifyOTPRequest) throws IOException {
        LOGGER.info("[VerifyOtp] request received : " + mobileEmailVerifyOTPRequest);
        mobileEmailVerifyOTPRequest.validate();
        MobileEmailVerifyOTPResponse veriftyOTPForMobileLoginResponse = abhaService.verifyOtpForMobileEmailLogin(mobileEmailVerifyOTPRequest, VERIFY_OTP_FOR_MOBILE_EMAIL_LOGIN.getPath());
        Response<MobileEmailVerifyOTPResponse> response = new Response<>(veriftyOTPForMobileLoginResponse, "OTP for Mobile/Email Login verified successfully");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/v1/mobileEmail/login/userAuthorizedToken",  method = RequestMethod.POST)
    @ApiOperation(
            value = "Get User Auth Token after mobile login",
            notes = "Step 3 for ABHA mobile login"
    )
    public ResponseEntity<Response<UserAuthorizedTokenResponse>> userAuthorizedTokenForMobileEmail(@RequestBody UserAuthorizedTokenForMobileEmailRequest userAuthorizedTokenForMobileEmailRequest) throws IOException {
        LOGGER.info("[userAuthorizedToken] request received : " + userAuthorizedTokenForMobileEmailRequest);
        userAuthorizedTokenForMobileEmailRequest.validate();
        UserAuthorizedTokenResponse userAuthorizedTokenResponse = abhaService.userAuthorizedTokenForMobileEmail(userAuthorizedTokenForMobileEmailRequest, AUTH_CONFIRM_OTP_FOR_MOBILE_EMAIL_LOGIN.getPath());
        Response<UserAuthorizedTokenResponse> response = new Response<>(userAuthorizedTokenResponse, "User Authorization Token fetched successfully");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/v1/share/profile",  method = RequestMethod.POST)
    @ApiOperation(
            value = "Share user profile with facility for OPD token",
            notes = "Step 1 for ABHA mobile login"
    )
    //// Login through mobile : V1 working /////


    public ResponseEntity<Response<ShareProfileResponse>> shareProfile(@RequestBody ShareProfileRequest shareProfileRequest) throws IOException {
        LOGGER.info("[userAuthorizedToken] request received : " + shareProfileRequest);
        shareProfileRequest.validate();
        ShareProfileResponse shareProfileResponse = abhaService.shareProfile(shareProfileRequest, SHARE_PROFILE.getPath());
        Response<ShareProfileResponse> response = new Response<>(shareProfileResponse, "User Profile share successfully with facility");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }


    @RequestMapping(value = "/v1/abha/provider/{providerId}",  method = RequestMethod.GET)
    @ApiOperation(
            value = "Get specific provider details",
            notes = "For fetching HIP details"
    )
    public ResponseEntity<Response<ABHAProviderResponse>> getProvider(@PathVariable(value = "providerId", required = true) String providerId,
                                                                      @RequestParam(name = "userId") Long userId) throws IOException {
        LOGGER.info("[getProvider] request received : " + providerId);
        ABHAProviderResponse abhaProviderResponse = abhaService.getProviderDetails(providerId,userId);
        Response<ABHAProviderResponse> response = new Response<>(abhaProviderResponse, "ABHA provider's details retrieved successfully");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }


    @RequestMapping(value = "/v1/abha/profile",  method = RequestMethod.GET)
    @ApiOperation(
            value = "Get ABHA profile using token after authentication",
            notes = "Step 4 of 4 in existing ABHA linking"
    )
    public ResponseEntity<Response<ABHAProfileResponse>> getAbhaAccountProfile(@RequestParam(name = "userId") Long userId) throws IOException {
        LOGGER.info("[GetAbhaProfile] request received : " + userId);
        ABHAProfileResponse abhaProfileResponse = abhaService.getAbhaProfile(userId);
        Response<ABHAProfileResponse> response = new Response<>(abhaProfileResponse, "ABHA profile retrieved successfully");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }


}
