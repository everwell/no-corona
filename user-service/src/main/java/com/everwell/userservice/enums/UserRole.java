package com.everwell.userservice.enums;

public enum UserRole {
    Caregiver,
    Dependant,
    Member;

    public static String getMutualRole(String role) {
        if (role.equalsIgnoreCase(UserRole.Caregiver.toString())) {
            role = UserRole.Dependant.toString();
        } else if (role.equalsIgnoreCase(UserRole.Dependant.toString())) {
            role = UserRole.Caregiver.toString();
        }
        return role;
    }
}
