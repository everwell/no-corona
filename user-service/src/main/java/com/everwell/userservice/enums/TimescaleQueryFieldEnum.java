package com.everwell.userservice.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum TimescaleQueryFieldEnum {
    START_TIMESTAMP("startTimestamp"),
    END_TIMESTAMP("endTimestamp"),
    USER_ID_LIST("userIds"),
    VITAL_ID_LIST("vitalIds");

    @Getter
    private final String fieldName;
}
