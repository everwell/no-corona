package com.everwell.userservice.enums;

public enum UserRelationEnum {
    Parent,
    Child,
    Others,
    Spouse,
    Sibling,
    Grandparent,
    Grandchild;

    public static String getMutualRelation(String relation) {
        if (relation.equalsIgnoreCase(UserRelationEnum.Parent.toString())) {
            relation = UserRelationEnum.Child.toString();
        } else if (relation.equalsIgnoreCase(UserRelationEnum.Child.toString())) {
            relation = UserRelationEnum.Parent.toString();
        } else if (relation.equalsIgnoreCase(UserRelationEnum.Grandchild.toString())) {
            relation = UserRelationEnum.Grandparent.toString();
        } else if (relation.equalsIgnoreCase(UserRelationEnum.Grandparent.toString())) {
            relation = UserRelationEnum.Grandchild.toString();
        }
        return relation;
    }
}
