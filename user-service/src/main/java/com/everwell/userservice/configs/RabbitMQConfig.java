package com.everwell.userservice.configs;

import com.everwell.userservice.constants.RMQConstants;
import com.everwell.userservice.exceptions.RMQErrorHandler;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import org.springframework.amqp.rabbit.connection.ConnectionFactory;

import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.util.ErrorHandler;

@Configuration
public class RabbitMQConfig
{
    @Bean
    Queue addUserQueue()
    {
        return new Queue(RMQConstants.ADD_USER_QUEUE);
    }

    @Bean
    Queue deleteUserQueue()
    {
        return new Queue(RMQConstants.DELETE_USER_QUEUE);
    }

    @Bean
    Queue updateUserQueue()
    {
        return new Queue(RMQConstants.UPDATE_USER_QUEUE);
    }

    @Bean
    Queue updateUserMobileQueue()
    {
        return new Queue(RMQConstants.UPDATE_USER_MOBILE_QUEUE);
    }

    @Bean
    Queue updateUserEmailQueue()
    {
        return new Queue(RMQConstants.UPDATE_USER_EMAIL_QUEUE);
    }

    @Bean
    Queue abdmAddUpdateConsentResponseQueue()
    {
        return new Queue(RMQConstants.ABDM_ADD_UPDATE_CONSENT_RESPONSE_QUEUE);
    }

    @Bean
    Queue abdmAddUpdateConsentConsumerQueue()
    {
        return new Queue(RMQConstants.ABDM_ADD_UPDATE_CONSENT_CONSUMER_QUEUE);
    }

    @Bean
    Queue abdmUpdateConsentDetailsQueue()
    {
        return new Queue(RMQConstants.ABDM_UPDATE_HIU_CONSENT_REQUEST_QUEUE);
    }

    @Bean
    DirectExchange userExchange()
    {
        return new DirectExchange(RMQConstants.USER_EXCHANGE);
    }

    @Bean
    DirectExchange dataGatewayExchange()
    {
        return new DirectExchange(RMQConstants.DATA_GATEWAY_EXCHANGE);
    }

    @Bean
    Binding addUserQueueBinding()
    {
        return BindingBuilder.bind(addUserQueue()).to(userExchange()).with(RMQConstants.ADD_USER_ROUTING_KEY);
    }

    @Bean
    Binding deleteUserQueueBinding()
    {
        return BindingBuilder.bind(deleteUserQueue()).to(userExchange()).with(RMQConstants.DELETE_USER_ROUTING_KEY);
    }

    @Bean
    Binding updateUserQueueBinding()
    {
        return BindingBuilder.bind(updateUserQueue()).to(userExchange()).with(RMQConstants.UPDATE_USER_ROUTING_KEY);
    }

    @Bean
    Binding updateUserMobileQueueBinding()
    {
        return BindingBuilder.bind(updateUserMobileQueue()).to(userExchange()).with(RMQConstants.UPDATE_USER_MOBILE_ROUTING_KEY);
    }

    @Bean
    Binding updateUserEmailQueueBinding()
    {
        return BindingBuilder.bind(updateUserEmailQueue()).to(userExchange()).with(RMQConstants.UPDATE_USER_EMAIL_ROUTING_KEY);
    }

    @Bean
    Binding abdmAddUpdateConsentResponseQueueBinding()
    {
        return BindingBuilder.bind(abdmAddUpdateConsentResponseQueue()).to(userExchange()).with(RMQConstants.ABDM_ADD_UPDATE_CONSENT_RESPONSE_ROUTING_KEY);
    }

    @Bean
    Binding abdmAddUpdateConsentConsumerQueueBinding()
    {
        return BindingBuilder.bind(abdmAddUpdateConsentConsumerQueue()).to(dataGatewayExchange()).with(RMQConstants.ABDM_ADD_UPDATE_CONSENT_CONSUMER_ROUTING_KEY);
    }

    @Bean
    Binding abdmUpdateConsentConsumerQueueBinding()
    {
        return BindingBuilder.bind(abdmUpdateConsentDetailsQueue()).to(userExchange()).with(RMQConstants.ABDM_ADD_CONSENT_REQUEST_ROUTING_KEY);
    }

    @Bean
    public MessageConverter jsonMessageConverter()
    {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public SimpleRabbitListenerContainerFactory rabbitListenerContainerFactory(ConnectionFactory connectionFactory)
    {
        SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
        factory.setConnectionFactory(connectionFactory);
        factory.setMessageConverter(jsonMessageConverter());
        factory.setErrorHandler(errorHandler());
        return factory;
    }

    @Bean
    public ErrorHandler errorHandler()
    {
        return new RMQErrorHandler();
    }
}
