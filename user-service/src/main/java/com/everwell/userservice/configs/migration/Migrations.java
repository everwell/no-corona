package com.everwell.userservice.configs.migration;

import org.flywaydb.core.Flyway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class Migrations {

    @Autowired
    private Flyway flyway;

    @PostConstruct
    @Order(1)
    public void runMigrations() {
        flyway.migrate();
    }
}
