package com.everwell.userservice.services;

import com.everwell.userservice.models.db.UserRelation;
import com.everwell.userservice.models.dtos.UserRelationDto;

import java.util.List;

public interface UserRelationService {

    List<UserRelation> getUserRelations(Long userId, String status);

    void addUserRelation(UserRelationDto addUserRelationDto);

    void deleteUserRelation(UserRelationDto addUserRelationDto);

    void updateUserRelation(UserRelationDto userRelationDto);
}
