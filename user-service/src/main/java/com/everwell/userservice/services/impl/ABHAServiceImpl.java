package com.everwell.userservice.services.impl;

import com.everwell.userservice.constants.Constants;
import com.everwell.userservice.exceptions.ValidationException;
import com.everwell.userservice.models.dtos.ExternalIdRequest;
import com.everwell.userservice.models.dtos.UpdateUserRequest;
import com.everwell.userservice.models.dtos.VerificationRequest;
import com.everwell.userservice.models.http.requests.*;
import com.everwell.userservice.models.http.responses.*;
import com.everwell.userservice.services.ABHAService;
import com.everwell.userservice.utils.CacheUtils;
import com.everwell.userservice.utils.Utils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static com.everwell.userservice.constants.ABHAConstants.*;
import static com.everwell.userservice.enums.ABHARoute.*;

@Service
public class ABHAServiceImpl extends AbhaRestTemplateServiceImpl implements ABHAService {

    @Value("${ABDM_API_ENDPOINT:}")
    String apiEndpoint;

    @Value("${NGP_ABDM_PHR_ID:}")
    String abdmPhrId;

    @Override
    public AadhaarOTPResponse verifyOtp(OTPVerifyRequest otpVerifyRequest, String otpType)  throws IOException {
        String url = apiEndpoint + (otpType.equals(Constants.MOBILE) ? VERIFY_OTP_MOBILE.getPath() : VERIFY_OTP_AADHAAR.getPath());
        return Utils.convertObject(postExchange(url, otpVerifyRequest, new ParameterizedTypeReference<AadhaarOTPResponse>(){}).getBody(), AadhaarOTPResponse.class);
    }

    @Override
    public ABHACreateResponse createAbha(ABHACreateRequest abhaCreateRequest) throws IOException {
        String url = apiEndpoint + CREATE_ABHA_PREVERIFIED.getPath();
        return Utils.convertObject(postExchange(url, abhaCreateRequest, new ParameterizedTypeReference<ABHACreateResponse>(){}).getBody(), ABHACreateResponse.class);
    }

    public UpdateUserRequest buildUpdateUserReq(Long userId, String txnId, Object abhaDetails) throws IOException {
        UpdateUserRequest updateUserRequest = new UpdateUserRequest();
        Map <String, Object> requestBody = Utils.convertObject(abhaDetails, Map.class);
        updateUserRequest.setUserId(userId);
        updateUserRequest.setFirstName(requestBody.get(Constants.FIRST_NAME).toString() + " " + requestBody.get(Constants.MIDDLE_NAME).toString());
        if (requestBody.get(Constants.LAST_NAME) != null && !requestBody.get(Constants.LAST_NAME).toString().isEmpty()) {
            updateUserRequest.setLastName(requestBody.get(Constants.LAST_NAME).toString());
        }
        updateUserRequest.setGender(requestBody.get(Constants.GENDER).toString());
        updateUserRequest.setCity(requestBody.get(Constants.DISTRICT_NAME).toString());
        updateUserRequest.setDateOfBirth(String.format("%s-%s-%s", requestBody.get(Constants.YEAR_OF_BIRTH).toString(), requestBody.get(Constants.MONTH_OF_BIRTH).toString(), requestBody.get(Constants.DAY_OF_BIRTH).toString()));
        if (requestBody.get(Constants.PINCODE) != null && requestBody.get(Constants.ADDRESS) != null) {
            updateUserRequest.setPincode(Integer.parseInt(requestBody.get(Constants.PINCODE).toString()));
            updateUserRequest.setAddress(requestBody.get(Constants.ADDRESS).toString());
        }
        List<VerificationRequest> verificationList = Arrays.asList(
                new VerificationRequest(Constants.AADHAR_OTP, Constants.PRIMARY_PHONE, requestBody.get(Constants.MOBILE).toString(), txnId)
        );
        updateUserRequest.setVerifications(verificationList);
        List<ExternalIdRequest> externalIdList = Arrays.asList(
                new ExternalIdRequest(Constants.HEALTH_ID_CAPS, requestBody.get(Constants.HEALTH_ID_NUMBER).toString()),
                new ExternalIdRequest(Constants.HEALTH_ADDRESS, null != requestBody.get(Constants.HEALTH_ID) ? requestBody.get(Constants.HEALTH_ID).toString() : null)
        );
        updateUserRequest.setExternalIds(externalIdList);
        return updateUserRequest;
    }

    @Override
    public ABHASearchResponse searchAbha(ABHASearchRequest abhaSearchRequest) throws IOException {
        String url = apiEndpoint + SEARCH_ABHA_HEALTH_ID.getPath();
        return Utils.convertObject(postExchange(url, abhaSearchRequest, new ParameterizedTypeReference<ABHASearchResponse>(){}).getBody(), ABHASearchResponse.class);
    }

    @Override
    public AadhaarOTPResponse generateOtp(Object requestObj, String urlPath) throws IOException {
        String url = apiEndpoint + urlPath;
        return Utils.convertObject(postExchange(url, requestObj, new ParameterizedTypeReference<AadhaarOTPResponse>(){}).getBody(), AadhaarOTPResponse.class);
    }

    public void resendOtp(ResendOTPRequest resendOTPRequest, String urlPath) throws IOException {
        String url = apiEndpoint + urlPath;
        postExchange(url, resendOTPRequest, new ParameterizedTypeReference<String>(){});
    }

    @Override
    public ABHAAuthResponse verifyOtpForAuth(OTPVerifyRequest otpVerifyRequest) throws IOException {
        String url = "";
        if(otpVerifyRequest.getAuthMode()!=null && otpVerifyRequest.getAuthMode().equalsIgnoreCase("MOBILE_OTP")){
            url = apiEndpoint + VERIFY_OTP_WITH_MOBILE.getPath();
        }else{
            url = apiEndpoint + VERIFY_OTP_AUTH.getPath();
        }
        ABHAAuthResponse abhaAuthResponse=  Utils.convertObject(postExchange(url, otpVerifyRequest, new ParameterizedTypeReference<ABHAAuthResponse>(){}).getBody(), ABHAAuthResponse.class);
        storeXAuthTokenInCache(abhaAuthResponse.getToken(),otpVerifyRequest.getUserId());
        return abhaAuthResponse;
    }

    @Override
    public ABHAProfileResponse getProfile(String authToken) throws IOException {
        String url = apiEndpoint + GET_ABHA_PROFILE.getPath();
        return Utils.convertObject(getExchange(url, authToken,  new ParameterizedTypeReference<ABHAProfileResponse>(){}).getBody(), ABHAProfileResponse.class);
    }

    @Override
    public <T> T getAbhaImage(Long userId, String imageName) throws IOException {
        String url = apiEndpoint;
        if (imageName.equals("card")) {
            url += GET_ABHA_CARD_IMAGE.getPath();
        } else if (imageName.equals("qr")) {
            url += GET_ABHA_QR_IMAGE.getPath();
        }
        String x_auth_token = getXAuthTokenFromCache(userId);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(ABHA_X_TOKEN,x_auth_token);
        return (T) getExchange(url, httpHeaders, new ParameterizedTypeReference<byte[]>(){}).getBody();
    }


    @Override
    public MobileEmailOTPResponse generateOtpForMobileEmailLogin(MobileEmailOTPRequest requestObj, String urlPath) throws IOException {
        MobileEmailOTPRequester mobileEmailOTPRequester = new MobileEmailOTPRequester();
        mobileEmailOTPRequester.setId(abdmPhrId);
        mobileEmailOTPRequester.setType(PHR);
        requestObj.setRequester(mobileEmailOTPRequester);
        String url = accessEndpoint + urlPath;
        MobileEmailOTPResponse mobileEmailOTPResponse = Utils.convertObject(postExchange(url, requestObj, new ParameterizedTypeReference<MobileEmailOTPResponse>(){}).getBody(), MobileEmailOTPResponse.class);
        CacheUtils.putIntoCache(DEFAULT_ABHA_TRANSACTION_SESSION_ID_CACHE_LABEL +requestObj.getUserId(), mobileEmailOTPResponse.getTransactionId(), DEFAULT_ABHA_TOKEN_CACHE_EXPIRATION_SECONDS_VALUE);
        return mobileEmailOTPResponse;
    }

    @Override
    public MobileEmailVerifyOTPResponse verifyOtpForMobileEmailLogin(MobileEmailVerifyOTPRequest requestObj, String urlPath) throws IOException {
        String transactionId = CacheUtils.getFromCache(DEFAULT_ABHA_TRANSACTION_SESSION_ID_CACHE_LABEL + requestObj.getUserId());
        if(transactionId==null){
            throw new ValidationException("TransactionId null or empty");
        }
        requestObj.setTransactionId(transactionId);
        requestObj.setRequesterId(abdmPhrId);
        String url = accessEndpoint + urlPath;
        MobileEmailVerifyOTPResponse mobileEmailVerifyOTPResponse =  Utils.convertObject(postExchange(url, requestObj, new ParameterizedTypeReference<MobileEmailVerifyOTPResponse>(){}).getBody(), MobileEmailVerifyOTPResponse.class);
        CacheUtils.putIntoCache(DEFAULT_ABHA_TRANSACTION_SESSION_ID_CACHE_LABEL +requestObj.getUserId(), mobileEmailVerifyOTPResponse.getTransactionId(), DEFAULT_ABHA_TOKEN_CACHE_EXPIRATION_SECONDS_VALUE);
        return mobileEmailVerifyOTPResponse;
    }

    @Override
    public UserAuthorizedTokenResponse userAuthorizedTokenForMobileEmail(UserAuthorizedTokenForMobileEmailRequest requestObj, String urlPath) throws IOException {
        String transactionId = CacheUtils.getFromCache(DEFAULT_ABHA_TRANSACTION_SESSION_ID_CACHE_LABEL + requestObj.getUserId());
        if(transactionId==null){
            throw new ValidationException("TransactionId null or empty");
        }
        requestObj.setTransactionId(transactionId);
        requestObj.setRequesterId(abdmPhrId);
        String url = accessEndpoint + urlPath;
        UserAuthorizedTokenResponse userAuthorizedTokenResponse =  Utils.convertObject(postExchange(url, requestObj, new ParameterizedTypeReference<UserAuthorizedTokenResponse>(){}).getBody(), UserAuthorizedTokenResponse.class);
        storeXAuthTokenInCache(userAuthorizedTokenResponse.getToken(),requestObj.getUserId());
        return userAuthorizedTokenResponse;
    }

    @Override
    public ShareProfileResponse shareProfile(ShareProfileRequest requestObj,String urlPath) throws IOException{
        String x_auth_token = getXAuthTokenFromCache(requestObj.getUserId());
        String url = accessEndpoint + urlPath;
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(ABHA_X_AUTH_TOKEN,x_auth_token);
        return Utils.convertObject(postExchange(url,requestObj,httpHeaders, new ParameterizedTypeReference<ShareProfileResponse>(){}).getBody(), ShareProfileResponse.class);
    }

    @Override
    public ABHAProviderResponse getProviderDetails(String providerId, Long userId) throws IOException {
        String x_auth_token = getXAuthTokenFromCache(userId);
        String url = accessEndpoint + PROVIDERS.getPath() + providerId;
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(ABHA_X_AUTH_TOKEN,x_auth_token);
        return Utils.convertObject(getExchange(url, httpHeaders, new ParameterizedTypeReference<ABHAProviderResponse>(){}).getBody(), ABHAProviderResponse.class);
    }


    @Override
    public ABHAProfileResponse getAbhaProfile(Long userId) throws IOException {
        String x_auth_token = getXAuthTokenFromCache(userId);
        String url = apiEndpoint + GET_ABHA_PROFILE.getPath();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(ABHA_X_TOKEN,x_auth_token);
        return Utils.convertObject(getExchange(url, httpHeaders, new ParameterizedTypeReference<ABHAProfileResponse>(){}).getBody(), ABHAProfileResponse.class);
    }

    private void storeXAuthTokenInCache(String x_auth_token,Long userId){
        if(!x_auth_token.contains("Bearer ")){
            x_auth_token="Bearer " + x_auth_token;
        }
        CacheUtils.putIntoCache(ABHA_X_AUTH_TOKEN+ "_" +userId, x_auth_token, DEFAULT_ABHA_TOKEN_CACHE_EXPIRATION_SECONDS_VALUE);
    }

    private String getXAuthTokenFromCache(Long userId){
        String x_auth_token = CacheUtils.getFromCache(ABHA_X_AUTH_TOKEN+ "_" + userId);
        if(x_auth_token==null){
            throw new ValidationException(" X-AUTH-TOKEN is null or empty");
        }
        return x_auth_token;
    }

}
