package com.everwell.userservice.services.impl;

import com.everwell.userservice.constants.Constants;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

import static com.everwell.userservice.enums.ABHARoute.GET_ACCESS_TOKEN;

@Service
public class AbhaRestTemplateServiceImpl extends RestTemplateServiceImpl{
    @Value("${ABDM_ACCESS_ENDPOINT:}")
    String accessEndpoint;

    @Value("${NGP_ABDM_CLIENT_ID:}")
    String clientId;

    @Value("${NGP_ABDM_CLIENT_SECRET:}")
    String clientSecret;

    private RestTemplate restTemplate;

    public AbhaRestTemplateServiceImpl()
    {
        restTemplate = new RestTemplate();
    }

    @Override
    public String generateAccessToken() {
        String url = accessEndpoint + GET_ACCESS_TOKEN.getPath();
        Map<String,String> object = new HashMap<>();
        object.put(Constants.CLIENT_ID, clientId);
        object.put(Constants.CLIENT_SECRET, clientSecret);
        ResponseEntity<Map> response =  restTemplate.postForEntity(url, object, Map.class);
        String token =  response.getBody() != null ? response.getBody().get(Constants.ACCESS_TOKEN).toString() : null;
        headers.remove(Constants.AUTHORIZATION);
        headers.add(Constants.AUTHORIZATION, Constants.BEARER + token);
        return token;
    }

}
