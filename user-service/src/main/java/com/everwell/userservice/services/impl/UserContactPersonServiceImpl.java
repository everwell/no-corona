package com.everwell.userservice.services.impl;

import com.everwell.userservice.exceptions.InternalServerErrorException;
import com.everwell.userservice.models.db.UserContactPerson;
import com.everwell.userservice.models.dtos.DetailsChangedDto;
import com.everwell.userservice.models.dtos.UserContactPersonRequest;
import com.everwell.userservice.repositories.UserContactPersonRepository;
import com.everwell.userservice.services.UserContactPersonService;
import com.everwell.userservice.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserContactPersonServiceImpl implements UserContactPersonService {

    @Autowired
    private UserContactPersonRepository userContactPersonRepository;


    @Override
    public UserContactPerson addUserContactPersonDetails(Long userId, UserContactPersonRequest userContactPersonRequest) {
        UserContactPerson userContactPerson = new UserContactPerson();
        if (null != userContactPersonRequest) {
            userContactPerson = new UserContactPerson(userId, userContactPersonRequest.getContactPersonAddress(), userContactPersonRequest.getContactPersonName(), userContactPersonRequest.getContactPersonPhone());
            userContactPersonRepository.save(userContactPerson);
        }
        return userContactPerson;
    }

    @Override
    public DetailsChangedDto<UserContactPerson> updateUserContactPersonDetails(Long userId, UserContactPersonRequest userUpdateContactPersonRequest){
        boolean detailsChanged = false;
        UserContactPerson userContactPersonUpdated = null;
        if (null != userUpdateContactPersonRequest) {
            UserContactPerson userContactPerson = userContactPersonRepository.findByUserId(userId);
            if (null != userContactPerson) {
                UserContactPerson newUserContactPerson = new UserContactPerson(userContactPerson.getId(), userContactPerson.getUserId(), userUpdateContactPersonRequest.getContactPersonAddress(),
                        userUpdateContactPersonRequest.getContactPersonName(), userUpdateContactPersonRequest.getContactPersonPhone());
                 detailsChanged = Utils.compareObjectsForValue(userContactPerson, newUserContactPerson, detailsChanged, (currField, oldVal, newVal) -> {
                    try {
                        currField.set(newUserContactPerson, oldVal);
                    } catch (IllegalAccessException e) {
                        throw new InternalServerErrorException("Error updating contact person details");
                    }
                });
                if (detailsChanged) {
                    userContactPersonUpdated = userContactPersonRepository.save(newUserContactPerson);
                }
            }
        }
        return new DetailsChangedDto<>(detailsChanged, userContactPersonUpdated);
    }

    @Override
    public UserContactPerson getUserContactPersonDetails(Long userId) {
        return userContactPersonRepository.findByUserId(userId);
    }
}
