package com.everwell.userservice.services.impl;

import com.everwell.userservice.exceptions.NotFoundException;
import com.everwell.userservice.exceptions.ValidationException;
import com.everwell.userservice.models.db.Client;
import com.everwell.userservice.models.http.requests.ClientRequest;
import com.everwell.userservice.models.http.responses.ClientResponse;
import com.everwell.userservice.repositories.ClientRepository;
import com.everwell.userservice.services.AuthenticationService;
import com.everwell.userservice.services.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;


@Service
public class ClientServiceImpl implements ClientService{

    @Autowired
    ClientRepository clientRepository;

    @Autowired
    private AuthenticationService authenticationService;

    @Override
    public ClientResponse registerClient(ClientRequest clientRequest) {
        Client client = clientRepository.findClientByName(clientRequest.getName());
        if (null != client) {
            throw new ValidationException("Client already exists");
        }
        String encodedPassword = new BCryptPasswordEncoder().encode(clientRequest.getPassword());;
        client = new Client(clientRequest.getName(), encodedPassword);
        clientRepository.save(client);
        return new ClientResponse(client);
    }

    @Override
    public ClientResponse getClientDetails(Long id) {
        Client client = getClientById(id);
        String secureClientToken = authenticationService.generateToken(client);
        return new ClientResponse(client, secureClientToken);
    }

    @Override
    public Client getClientById(Long id) {
        if(null == id) {
            throw new ValidationException("id cannot be null");
        }
        Client client = clientRepository.findById(id).orElse(null);
        if(null == client) {
            throw new NotFoundException("No client found with id:" + id);
        }
        return client;
    }

    @Override
    public Client getClientByToken() {
        return (Client) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }

}
