package com.everwell.userservice.services;

import com.everwell.userservice.models.db.Client;

public interface AuthenticationService {

    String generateToken(Client clientAuth);

    Boolean isValidToken(String token, Client client);

}
