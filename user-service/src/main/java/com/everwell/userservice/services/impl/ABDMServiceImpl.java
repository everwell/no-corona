package com.everwell.userservice.services.impl;

import com.everwell.userservice.constants.RMQConstants;
import com.everwell.userservice.enums.UserValidation;
import com.everwell.userservice.exceptions.NotFoundException;
import com.everwell.userservice.models.db.*;
import com.everwell.userservice.models.dtos.ReferenceIdRMQRequest;
import com.everwell.userservice.models.dtos.ReferenceIdRMQResponse;
import com.everwell.userservice.models.dtos.abdm.*;
import com.everwell.userservice.repositories.*;
import com.everwell.userservice.services.ABDMService;
import com.everwell.userservice.services.RabbitMQPublisherService;
import com.everwell.userservice.utils.Utils;
import com.fasterxml.jackson.core.type.TypeReference;
import io.sentry.Sentry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.everwell.userservice.constants.Constants;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.everwell.userservice.constants.Constants.*;
import static com.everwell.userservice.constants.RMQConstants.ABDM_ADD_CONSENT_REQUEST_ROUTING_KEY;

@Service
public class ABDMServiceImpl implements ABDMService
{
    @Autowired
    private ABDMConsentRepository abdmConsentRepository;

    @Autowired
    private RabbitMQPublisherService rabbitMQPublisherService;

    @Autowired
    private UserMedicalRecordsRepository userMedicalRecordsRepository;

    private static Logger LOGGER = LoggerFactory.getLogger(ABDMServiceImpl.class);

    public void addUpdateConsent(ReferenceIdRMQRequest rmqRequest, ABDMAddUpdateConsentRequest abdmAddUpdateConsentRequest, String replyTo)
    {
        String consentId = abdmAddUpdateConsentRequest.getNotification().getConsentId();
        String requestId = abdmAddUpdateConsentRequest.getRequestId();
        String acknowledgementStatus = "";
        boolean success = false;
        String error = null;
        try
        {
            ABDMConsent abdmConsentData = abdmConsentRepository.findConsentByConsentId(consentId);
            String json = Utils.asJsonString(abdmAddUpdateConsentRequest);
            Date now = new Date();
            ABDMConsent abdmConsent = abdmConsentData == null ? new ABDMConsent(consentId, now,HIP_CONSENT_TYPE) : abdmConsentData;
            abdmConsent.setData(json);
            abdmConsent.setUpdatedAt(now);
            abdmConsentRepository.save(abdmConsent);
            acknowledgementStatus = Constants.ABDM_CONSENT_SUCCESS_STATUS;
            success = true;

        }
        catch(Exception ex)
        {
            LOGGER.error("Error during Saving and Updating Consent Data " + ex);
            Sentry.capture(ex);
            acknowledgementStatus = Constants.ABDM_CONSENT_FAILURE_STATUS;
            error = ex.getMessage();
        }
        ABDMAddUpdateConsentResponse abdmAddUpdateConsentResponse = new ABDMAddUpdateConsentResponse(acknowledgementStatus, consentId, error, requestId);
        ReferenceIdRMQResponse rmqResponse = new ReferenceIdRMQResponse(rmqRequest.getReferenceId(), rmqRequest.getUserName(), Utils.asJsonString(abdmAddUpdateConsentResponse),success);
        rabbitMQPublisherService.send(rmqResponse, RMQConstants.USER_EXCHANGE, replyTo);
    }

    @Override
    public ABDMConsentResponse getByConsentId(String consentId) {
        ABDMConsent abdmConsent = abdmConsentRepository.findConsentByConsentId(consentId);
        ABDMConsentResponse abdmConsentResponse = new ABDMConsentResponse();
        try {
            ABDMAddUpdateConsentRequest consentRequest = Utils.convertObject(abdmConsent.getData(), new TypeReference<ABDMAddUpdateConsentRequest>(){});
             abdmConsentResponse = new ABDMConsentResponse(consentRequest.getNotification().getStatus());
            if(abdmConsentResponse.getStatus().equals(ABDM_CONSENT_GRANTED)) {
                ABDMConsentDetail abdmConsentDetail = consentRequest.getNotification().getConsentDetail();
                abdmConsentResponse.setEpisodeIds(abdmConsentDetail.getCareContexts().stream().map(ABDMConsentDetail.CareContext::getCareContextReference).collect(Collectors.toList()));
                abdmConsentResponse.setHiTypes(abdmConsentDetail.getHiTypes());
                abdmConsentResponse.setStartDate(abdmConsentDetail.getPermission().getDateRange().getFrom());
                abdmConsentResponse.setEndDate(abdmConsentDetail.getPermission().getDateRange().getTill());
                abdmConsentResponse.setDateEraseTime(abdmConsentDetail.getPermission().getDataEraseAt());
            }
        }
        catch (Exception e) {
            LOGGER.error("Exception during fetching Consent Data " + e);
            Sentry.capture(e);
            abdmConsentResponse.setStatus(ABDM_CONSENT_EXPIRED);
        }
        return abdmConsentResponse;
    }

    @Override
    public void updateHIUConsentDetails(UpdateConsentDto updateConsentDto) {
        Optional<ABDMConsent> abdmConsent = StringUtils.hasText(updateConsentDto.getConsentId()) ? abdmConsentRepository.findById(Long.parseLong(updateConsentDto.getConsentId())) : Optional.ofNullable(abdmConsentRepository.findConsentByConsentId(updateConsentDto.getConsentRequestId()));
        if(!abdmConsent.isPresent()) {
            throw new NotFoundException(UserValidation.CONSENT_ID_NOT_FOUND.getMessage());
        }
        Date now = new Date();
        ABDMConsent abdmConsentData = abdmConsent.get();
        if(null != updateConsentDto.getConsentRequestId()){
            abdmConsentData.setConsentId(updateConsentDto.getConsentRequestId());
        }
        if(null != updateConsentDto.getAbdmConsentNotification()) {
            ConsentNotificationDto consentNotificationDto = updateConsentDto.getAbdmConsentNotification();
            String json = Utils.asJsonString(consentNotificationDto);
            if (CONSENT_GRANTED.equals(consentNotificationDto.getNotification().getStatus())) {
                List<UserMedicalRecords> userMedicalRecordsList = consentNotificationDto.getNotification().getConsentArtefacts().stream().map(consentArtefact -> new UserMedicalRecords(consentArtefact.getId(), updateConsentDto.getConsentRequestId(),now)).collect(Collectors.toList());
                userMedicalRecordsRepository.saveAll(userMedicalRecordsList);
            } else if (CONSENT_EXPIRED.equals(consentNotificationDto.getNotification().getStatus())) {
                userMedicalRecordsRepository.deleteAllByConsentRequestId(updateConsentDto.getConsentRequestId());
            }
            abdmConsentData.setData(json);
        }
        abdmConsentData.setUpdatedAt(now);
        abdmConsentRepository.save(abdmConsentData);
    }

    @Override
    public void addHIUConsentDetails(AddConsentRequest addConsentRequest) {
        ABDMConsent abdmConsent = new ABDMConsent(addConsentRequest.getUserId(),HIU_CONSENT_TYPE);
        ABDMConsent addedConsent = abdmConsentRepository.save(abdmConsent);

        addConsentRequest.setConsentId(addedConsent.getId().toString());
        rabbitMQPublisherService.send(addConsentRequest, RMQConstants.DATA_GATEWAY_EXCHANGE, ABDM_ADD_CONSENT_REQUEST_ROUTING_KEY);
    }
}
