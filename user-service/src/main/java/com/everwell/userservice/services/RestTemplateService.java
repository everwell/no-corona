package com.everwell.userservice.services;

import com.everwell.userservice.models.dtos.Response;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Map;

public interface RestTemplateService {
    <T> ResponseEntity<Response<T>> getExchange(String url, String authToken, ParameterizedTypeReference<T> typeReference);

    <T> ResponseEntity<Response<T>> getExchange(String url, HttpHeaders extraHeaders,ParameterizedTypeReference<T> typeReference);

    <T> ResponseEntity<Response<T>> postExchange(String url, Object requestObj, ParameterizedTypeReference<T> typeReference);

    <T> ResponseEntity<Response<T>> postExchange(String url, Object requestObj,HttpHeaders extraHeaders, ParameterizedTypeReference<T> typeReference);

    <T> ResponseEntity<Response<T>> genericExchange(String url, HttpMethod method, ParameterizedTypeReference<T> parameterizedTypeReference, Object requestObject, String authToken, HttpHeaders extraHeaders);
}
