package com.everwell.userservice.models.dtos.vitals;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class VitalDto {
    private Long vitalId;

    private String vitalName;

    private String value;
}
