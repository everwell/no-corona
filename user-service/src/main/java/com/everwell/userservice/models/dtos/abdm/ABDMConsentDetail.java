package com.everwell.userservice.models.dtos.abdm;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ABDMConsentDetail {
    public String consentId;
    public String createdAt;
    public Purpose purpose;
    public Patient patient;
    public ConsentManager consentManager;
    public Hip hip;
    public List<String> hiTypes;
    public Permission permission;
    public List<CareContext> careContexts;

    @Getter
    @Setter
    public static class CareContext{
        public String patientReference;
        public String careContextReference;
    }

    @Getter
    @Setter
    public static class ConsentManager{
        public String id;
    }

    @Getter
    @Setter
    public static class DateRange{
        public String from;
        @JsonProperty("to")
        public String till;
    }

    @Getter
    @Setter
    public static class Frequency{
        public String unit;
        public int value;
        public int repeats;
    }

    @Getter
    @Setter
    public static class Hip{
        public String id;
        public Object name;
    }

    @Getter
    @Setter
    public static class Patient{
        public String id;
    }

    @Getter
    @Setter
    public static class Permission{
        public String accessMode;
        public DateRange dateRange;
        public String dataEraseAt;
        public Frequency frequency;
    }

    @Getter
    @Setter
    public static class Purpose{
        public String text;
        public String code;
        public Object refUri;
    }
}
