package com.everwell.userservice.models.dtos.abdm;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
public class ABDMAddUpdateConsentRequest
{
    public ABDMConsentNotification notification;
    public String requestId;
    public String timestamp;

    @Getter
    @Setter
    public static class ABDMConsentNotification {
        public ABDMConsentDetail consentDetail;
        public String status;
        public String signature;
        public String consentId;
        public boolean grantAcknowledgement;
    }
}
