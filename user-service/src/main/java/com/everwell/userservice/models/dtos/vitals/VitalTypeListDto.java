package com.everwell.userservice.models.dtos.vitals;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class VitalTypeListDto {
    private List<Long> associatedVitalIdList;

    private List<Long> singleValuedVitalIdList;

    private List<Long> vitalIdList;
}
