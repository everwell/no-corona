package com.everwell.userservice.models.http.requests;

import lombok.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class HipDetails{
    private String hipId;
    private String code;
}
