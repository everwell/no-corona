package com.everwell.userservice.models.db;


import com.everwell.userservice.enums.UserRelationStatus;
import com.everwell.userservice.models.dtos.UserRelationDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "user_relation")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UserRelation {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @Column(name = "primary_user_id")
    private Long primaryUserId;

    @Column(name = "secondary_user_id")
    private Long secondaryUserId;

    @Column
    private Integer status;

    @Column
    private String relation;

    @Column
    private String role;

    @Column
    private String nickname;

    @Column(name = "emergency_contact")
    private boolean emergencyContact;

    public UserRelation(UserRelationDto userRelationDto) {
        this.primaryUserId = userRelationDto.getPrimaryUserId();
        this.secondaryUserId = userRelationDto.getSecondaryUserId();
        this.status = UserRelationStatus.getStatusValue(userRelationDto.getStatus());
        this.relation = userRelationDto.getRelation();
        this.role = userRelationDto.getRole();
        this.nickname = userRelationDto.getNickname();
        this.emergencyContact = userRelationDto.isEmergencyContact();
    }
}
