package com.everwell.userservice.models.dtos;


import lombok.*;

import java.util.Date;

@AllArgsConstructor
@Getter
public class UserCompleteDto {

    private Long userMobileId;

    private Date userMobileCreatedAt;

    private Boolean userMobileIsPrimary;

    private Boolean userMobileIsVerified;

    private String userMobileNumber;

    private Date userMobileStoppedAt;

    private Date userMobileUpdatedAt;

    private Long userMobileUserId;

    private String userMobileOwner;

    private Long userEmailIdentityId;

    private Date userEmailCreatedAt;

    private String userEmailId;

    private Boolean userEmailIsPrimary;

    private Boolean userEmailIsVerified;

    private Date userEmailStoppedAt;

    private Date userEmailUpdatedAt;

    private Long userEmailUserId;

    private Long userAddressId;

    private String area;

    private String landmark;

    private String taluka;

    private String town;

    private String ward;

    private Long userAddressUserId;

    private Long userContactPersonId;

    private String address;

    private String name;

    private String phone;

    private  Long userContactPersonUserId;

}
