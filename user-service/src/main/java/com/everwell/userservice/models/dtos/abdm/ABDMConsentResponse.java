package com.everwell.userservice.models.dtos.abdm;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ABDMConsentResponse {
    public String status;
    public List<String> episodeIds;
    public List<String> hiTypes;
    public String startDate;
    public String endDate;
    public String dateEraseTime;

    public ABDMConsentResponse(String status){
        this.status = status;
    }
}
