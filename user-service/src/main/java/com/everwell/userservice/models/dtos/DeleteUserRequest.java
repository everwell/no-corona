package com.everwell.userservice.models.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown=true)
@Getter
@ToString
public class DeleteUserRequest
{
    private Long userId;
    private Long deletedBy;
    private String deletedByType;
}
