package com.everwell.userservice.models.dtos;

import com.everwell.userservice.exceptions.ValidationException;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.util.StringUtils;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserEmailRequest
{
    private String emailId;
    private boolean primary;

    public void validate()
    {
        if(StringUtils.isEmpty(emailId))
        {
            throw new ValidationException("Email ID cannot be empty");
        }
    }
}
