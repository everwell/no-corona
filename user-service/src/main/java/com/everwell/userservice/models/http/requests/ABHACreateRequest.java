package com.everwell.userservice.models.http.requests;

import com.everwell.userservice.exceptions.ValidationException;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import org.apache.commons.lang3.StringUtils;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ABHACreateRequest {
    private String email;
    private String firstName;
    private String healthId;
    private String lastName;
    private String middleName;
    private String password;
    private String profilePhoto;
    private String txnId;
    private Long userId;
    private String entityType;
    private Long entityId;

    public void validate() {
        if(StringUtils.isEmpty(txnId)) {
            throw new ValidationException("txnId cannot be null or empty");
        } else if (null == userId) {
            throw new ValidationException("userId cannot be null or empty");
        }
    }
}
