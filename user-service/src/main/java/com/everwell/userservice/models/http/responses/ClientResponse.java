package com.everwell.userservice.models.http.responses;

import com.everwell.userservice.models.db.Client;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.ToString;

@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
public class ClientResponse {
    private Long id;

    private String name;

    private String password;

    private String authToken;

    public ClientResponse(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public ClientResponse(Client client) {
        this(client.getId(), client.getName());
    }

    public ClientResponse(Long id, String name, String authToken) {
        this(id, name);
        this.authToken = authToken;
    }

    public ClientResponse(Client client, String authToken) {
        this(client.getId(), client.getName(), authToken);
    }
}