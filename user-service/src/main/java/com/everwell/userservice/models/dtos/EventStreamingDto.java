package com.everwell.userservice.models.dtos;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class EventStreamingDto<T>
{
    @JsonAlias("EventName")
    String eventName;
    @JsonAlias("Field")
    T field;
}

