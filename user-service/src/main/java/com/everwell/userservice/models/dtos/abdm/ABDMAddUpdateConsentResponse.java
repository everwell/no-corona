package com.everwell.userservice.models.dtos.abdm;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.UUID;
import com.everwell.userservice.constants.Constants;


@Getter
@Setter
@NoArgsConstructor
public class ABDMAddUpdateConsentResponse
{
    public String requestId;
    public String timestamp;
    public ABDMConsentAcknowledgement acknowledgement;
    public ABDMError error;
    public ABDMConsentResp resp;

    public ABDMAddUpdateConsentResponse(String acknowledgementStatus, String consentId, String error, String requestId)
    {
        DateTimeFormatter formatter =  DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSSSS");
        LocalDateTime utcNow =  LocalDateTime.now(ZoneOffset.UTC);
        this.requestId = UUID.randomUUID().toString();
        this.timestamp = formatter.format(utcNow);
        this.acknowledgement = new ABDMConsentAcknowledgement(acknowledgementStatus, consentId);
        this.error = error == null ? null : new ABDMError(Constants.ABDM_EXCEPTION_CODE, error);
        this.resp = new ABDMConsentResp(requestId);
    }

    @Getter
    @Setter
    public static class ABDMConsentAcknowledgement {
        public String status;
        public String consentId;

        public ABDMConsentAcknowledgement(String status, String consentId)
        {
            this.status = status;
            this.consentId = consentId;
        }
    }

    @Getter
    @Setter
    public static class ABDMError {
        public int code;
        public String message;

        public ABDMError(int code, String message)
        {
            this.code = code;
            this.message = message;
        }
    }

    @Getter
    @Setter
    public static class ABDMConsentResp {
        public String requestId;

        public ABDMConsentResp(String requestId)
        {
            this.requestId = requestId;
        }
    }
}
