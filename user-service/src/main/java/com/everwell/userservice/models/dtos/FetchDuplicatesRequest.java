package com.everwell.userservice.models.dtos;

import com.everwell.userservice.enums.DeduplicationScheme;
import com.everwell.userservice.exceptions.ValidationException;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown=true)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "scheme", include = JsonTypeInfo.As.EXISTING_PROPERTY)
@JsonSubTypes({
        @JsonSubTypes.Type(value = FetchDuplicatesRequestForPrimaryPhoneAndGender.class, name = "PRIMARYPHONE_GENDER")
})
public class FetchDuplicatesRequest
{
    private int offset;
    private int count;
    private DeduplicationScheme scheme;
    private Set<String> requiredFields = new HashSet<>();

    public void validate()
    {
        if(null == scheme)
        {
            throw new ValidationException("Provide valid deduplication scheme");
        }

        if(offset < 0)
        {
            throw new ValidationException("Invalid offset");
        }
    }

    public FetchDuplicatesRequest(DeduplicationScheme scheme)
    {
        this.scheme = scheme;
    }
}
