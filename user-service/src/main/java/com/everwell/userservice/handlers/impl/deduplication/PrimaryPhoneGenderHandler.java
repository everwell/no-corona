package com.everwell.userservice.handlers.impl.deduplication;

import com.everwell.userservice.constants.Constants;
import com.everwell.userservice.enums.DeduplicationScheme;
import com.everwell.userservice.handlers.DeduplicationHandler;
import com.everwell.userservice.models.db.User;
import com.everwell.userservice.models.db.UserMobile;
import com.everwell.userservice.models.dtos.FetchDuplicatesRequest;
import com.everwell.userservice.models.dtos.FetchDuplicatesRequestForPrimaryPhoneAndGender;
import com.everwell.userservice.models.dtos.UserResponse;
import com.everwell.userservice.repositories.UserMobileRepository;
import com.everwell.userservice.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class PrimaryPhoneGenderHandler extends DeduplicationHandler
{
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserMobileRepository userMobileRepository;

    public DeduplicationScheme getDeduplicationScheme()
    {
        return DeduplicationScheme.PRIMARYPHONE_GENDER;
    }

    public List<String> getDefaultRequiredFields()
    {
        return Arrays.asList(
                Constants.ID,
                Constants.FIRST_NAME,
                Constants.LAST_NAME,
                Constants.GENDER,
                Constants.MOBILE_LIST,
                Constants.PRIMARY_PHONE_NUMBER
        );
    }

    public List<User> fetchDuplicates(FetchDuplicatesRequest fetchDuplicatesRequest, Long clientId)
    {
        FetchDuplicatesRequestForPrimaryPhoneAndGender specificDuplicatesRequest = (FetchDuplicatesRequestForPrimaryPhoneAndGender) fetchDuplicatesRequest;
        String primaryPhoneNumber = specificDuplicatesRequest.getPrimaryPhoneNumber();
        String gender = specificDuplicatesRequest.getGender();

        List<UserMobile> userMobiles = userMobileRepository.findAllByNumberAndIsPrimaryTrue(primaryPhoneNumber);

        List<Long> userIds = userMobiles
                .stream()
                .map(UserMobile::getUserId).
                collect(Collectors.toList());

        return userRepository.findAllByIdInAndIsDeletedFalseAndGenderAndClientId(userIds, gender, clientId);
    }
}
