package com.everwell.userservice.constants;

public class ABHAConstants {

    public static final String ABHA_X_TOKEN = "X-Token";
    public static final String ABHA_X_AUTH_TOKEN = "X-AUTH-TOKEN";
    public static final String ABHA_T_TOKEN = "T-Token";
    public static final String PHR = "PHR";
    public static final String DEFAULT_ABHA_TRANSACTION_SESSION_ID_CACHE_LABEL ="DEFAULT_ABHA_TRANSACTION_SESSION_ID_";
    public static final Long DEFAULT_ABHA_TOKEN_CACHE_EXPIRATION_SECONDS_VALUE = 1200L; // 20 seconds

}
