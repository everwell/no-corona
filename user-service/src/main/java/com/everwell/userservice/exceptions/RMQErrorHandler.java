package com.everwell.userservice.exceptions;

import com.everwell.userservice.utils.Utils;
import io.sentry.Sentry;
import io.sentry.event.EventBuilder;
import io.sentry.event.interfaces.ExceptionInterface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.AmqpRejectAndDontRequeueException;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.support.ListenerExecutionFailedException;
import org.springframework.util.ErrorHandler;

public class RMQErrorHandler implements ErrorHandler
{
    private static Logger LOGGER = LoggerFactory.getLogger(RMQErrorHandler.class);

    @Override
    public void handleError(Throwable t)
    {
        if (t instanceof ListenerExecutionFailedException) {
            Message message = ((ListenerExecutionFailedException) t).getFailedMessage();
            String consumerQueue = message.getMessageProperties().getConsumerQueue();
            String body = Utils.asJsonString(new String(message.getBody()));
            EventBuilder builder = new EventBuilder()
                    .withSentryInterface(new ExceptionInterface(t))
                    .withExtra("Queue", consumerQueue)
                    .withExtra("Message Payload", body);
            Sentry.capture(builder);
        }
        else
        {
            Sentry.capture(t);
        }
        LOGGER.error("Exception at consumer", t);

        throw new AmqpRejectAndDontRequeueException("Exception at consumer", t);
    }
}
