package tests.unit.mappers;

import com.everwell.userservice.mapper.VitalsAggregatedTimeDataRowMapper;
import com.everwell.userservice.models.dtos.vitals.VitalsAggregatedTimeData;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import tests.BaseTest;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

public class VitalsAggregatedTimeDataRowMapperTest extends BaseTest {

    @Spy
    @InjectMocks
    private VitalsAggregatedTimeDataRowMapper vitalsAggregatedTimeDataRowMapper;

    @Mock
    ResultSet resultSet;

    @Test
    public void mapRowTest() throws SQLException {
        Mockito.when(resultSet.getTimestamp(1)).thenReturn(Timestamp.valueOf("2022-11-29 00:00:00"));
        Mockito.when(resultSet.getLong(2)).thenReturn(1L);
        Mockito.when(resultSet.getLong(3)).thenReturn(2L);
        Mockito.when(resultSet.getFloat(4)).thenReturn(100f);
        Mockito.when(resultSet.getFloat(5)).thenReturn(10f);

        VitalsAggregatedTimeData vitalsAggregatedTimeData = vitalsAggregatedTimeDataRowMapper.mapRow(resultSet, 0);

        Assert.assertEquals(1L, vitalsAggregatedTimeData.getUserId().longValue());
        Assert.assertEquals(2L, vitalsAggregatedTimeData.getVitalId().longValue());
        Assert.assertEquals(100L, vitalsAggregatedTimeData.getTotal().longValue());
        Assert.assertEquals(10L, vitalsAggregatedTimeData.getAverage().longValue());
    }
}
