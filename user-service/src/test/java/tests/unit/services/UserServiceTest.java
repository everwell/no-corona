package tests.unit.services;


import com.everwell.userservice.enums.DeduplicationScheme;
import com.everwell.userservice.enums.UserRelationEnum;
import com.everwell.userservice.enums.UserRelationStatus;
import com.everwell.userservice.exceptions.NotFoundException;
import com.everwell.userservice.exceptions.ValidationException;
import com.everwell.userservice.handlers.DeduplicationHandler;
import com.everwell.userservice.handlers.DeduplicationHandlerMap;
import com.everwell.userservice.models.db.*;
import com.everwell.userservice.models.dtos.*;
import com.everwell.userservice.repositories.*;
import com.everwell.userservice.services.ClientService;
import com.everwell.userservice.services.RabbitMQPublisherService;
import com.everwell.userservice.services.UserRelationService;
import com.everwell.userservice.services.UserService;
import com.everwell.userservice.utils.Utils;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

import org.springframework.boot.test.mock.mockito.MockBean;
import tests.BaseTest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;


public class UserServiceTest extends BaseTest
{
    @MockBean
    private UserRepository userRepository;

    @MockBean
    private UserMobileRepository userMobileRepository;

    @MockBean
    private UserEmailRepository userEmailRepository;

    @MockBean
    private VerificationRepository verificationRepository;

    @MockBean
    private ExternalIdRepository externalIdRepository;

    @Autowired
    private UserService userService;

    @MockBean
    DeduplicationHandlerMap deduplicationHandlerMap;

    @Mock
    DeduplicationHandler deduplicationHandler;

    @MockBean
    private RabbitMQPublisherService rabbitMQPublisherService;

    @MockBean
    private UserAddressRepository userAddressRepository;

    @MockBean
    private UserContactPersonRepository userContactPersonRepository;

    @MockBean
    private UserRelationService userRelationService;

    @MockBean
    private ClientService clientService;

    @Test
    public void testGetUserById() throws Exception
    {
        User user = new User();
        user.setDateOfBirth(new Date());
        List<UserCompleteDto> userCompleteDtoList = new ArrayList<>();
        userCompleteDtoList.add(new UserCompleteDto(1L, Utils.getCurrentDate(), true, true, "1234567890", null, Utils.getCurrentDate(), 1L, null, 1L, Utils.getCurrentDate(), "abc@gmail.com",
                true, false, null, Utils.getCurrentDate(), 1L, 1L, "area", "landmark",
                "taluka", "town", "ward", 1L, 1L, "address", "name", "phone", 1L));

        when(userRepository.findByIdAndIsDeletedAndClientId(anyLong(), anyBoolean(), any())).thenReturn(user);
        when(userRepository.getAllUserDetailByUserId(any(), any())).thenReturn(userCompleteDtoList);
        when(verificationRepository.findAllByUserIdAndStoppedAtNull(anyLong())).thenReturn(new ArrayList<>());
        when(externalIdRepository.findAllByUserId(anyLong())).thenReturn(new ArrayList<>());

        UserResponse userResponse = userService.get(1L, 1L);
        Assert.assertNotNull(userResponse);
        verify(userRepository, Mockito.times(1)).getAllUserDetailByUserId(any(), any());
    }
    @Test
    public void testGetUserByIdWithMobileOwners() throws Exception
    {
        User user = new User();
        user.setDateOfBirth(new Date());
        List<UserCompleteDto> userCompleteDtoList = new ArrayList<>();
        userCompleteDtoList.add(new UserCompleteDto(1L, Utils.getCurrentDate(), true, true, "1234567890", null, Utils.getCurrentDate(), 1L, "userMobileOwner", 1L, Utils.getCurrentDate(), "abc@gmail.com",
                true, false, null, Utils.getCurrentDate(), 1L, 1L, "area", "landmark",
                "taluka", "town", "ward", 1L, 1L, "address", "name", "phone", 1L));

        when(userRepository.findByIdAndIsDeletedAndClientId(anyLong(), anyBoolean(), any())).thenReturn(user);
        when(userRepository.getAllUserDetailByUserId(any(), any())).thenReturn(userCompleteDtoList);
        when(verificationRepository.findAllByUserIdAndStoppedAtNull(anyLong())).thenReturn(new ArrayList<>());
        when(externalIdRepository.findAllByUserId(anyLong())).thenReturn(new ArrayList<>());

        UserResponse userResponse = userService.get(1L, 1L);
        Assert.assertNotNull(userResponse);
        verify(userRepository, Mockito.times(1)).getAllUserDetailByUserId(any(), any());
        Assert.assertEquals("1234567890", userResponse.getMobileList().get(0).getNumber());
        Assert.assertEquals("userMobileOwner", userResponse.getMobileList().get(0).getOwner());

    }
    @Test
    public void testGetUserByIdWithDuplicateIdResponseFromJoin() throws Exception
    {
        User user = new User();
        user.setDateOfBirth(new Date());
        List<UserCompleteDto> userCompleteDtoList = new ArrayList<>();
        userCompleteDtoList.add(new UserCompleteDto(1L, Utils.getCurrentDate(), true, true, "1234567890", null, Utils.getCurrentDate(), 1L, "userMobileOwner", 1L, Utils.getCurrentDate(), "abc@gmail.com",
                true, false, null, Utils.getCurrentDate(), 1L, 1L, "area", "landmark",
                "taluka", "town", "ward", 1L, 1L, "address", "name", "phone", 1L));

        userCompleteDtoList.add(new UserCompleteDto(1L, Utils.getCurrentDate(), true, true, "1234567890", null, Utils.getCurrentDate(), 1L,"userMobileOwner",  1L, Utils.getCurrentDate(), "abc@gmail.com",
                true, false, null, Utils.getCurrentDate(), 1L, 1L, "area", "landmark",
                "taluka", "town", "ward", 1L, 1L, "address", "name", "phone", 1L));

        when(userRepository.findByIdAndIsDeletedAndClientId(anyLong(), anyBoolean(), any())).thenReturn(user);
        when(userRepository.getAllUserDetailByUserId(any(), any())).thenReturn(userCompleteDtoList);
        when(verificationRepository.findAllByUserIdAndStoppedAtNull(anyLong())).thenReturn(new ArrayList<>());
        when(externalIdRepository.findAllByUserId(anyLong())).thenReturn(new ArrayList<>());

        UserResponse userResponse = userService.get(1L, 1L);
        Assert.assertEquals(1L, userResponse.getMobileList().size());
        Assert.assertEquals(1L, userResponse.getEmailList().size());
        Assert.assertNotNull(userResponse);
        verify(userRepository, Mockito.times(1)).getAllUserDetailByUserId(any(), any());
    }

    @Test
    public void testGetUserByIdWithoutMobileEmailAddressAndContactInformation() throws Exception
    {
        User user = new User();
        user.setDateOfBirth(new Date());
        List<UserCompleteDto> userCompleteDtoList = new ArrayList<>();
        userCompleteDtoList.add(new UserCompleteDto(null, null, true, true, null, null, null, null, null, null, null, null,
                null, null, null, null, null, null, null, null,
                null, null, null, null, null, null, null, null, null));

        when(userRepository.findByIdAndIsDeletedAndClientId(anyLong(), anyBoolean(), any())).thenReturn(user);
        when(userRepository.getAllUserDetailByUserId(any(), any())).thenReturn(userCompleteDtoList);
        when(verificationRepository.findAllByUserIdAndStoppedAtNull(anyLong())).thenReturn(new ArrayList<>());
        when(externalIdRepository.findAllByUserId(anyLong())).thenReturn(new ArrayList<>());

        UserResponse userResponse = userService.get(1L, 1L);
        Assert.assertNotNull(userResponse);
        Assert.assertEquals(0L, userResponse.getMobileList().size());
        Assert.assertEquals(0L, userResponse.getEmailList().size());
        verify(userRepository, Mockito.times(1)).getAllUserDetailByUserId(any(), any());
    }

    @Test
    public void testGetUserByIdWithoutEmailAddressAndContactInformation() throws Exception
    {
        User user = new User();
        user.setDateOfBirth(new Date());
        List<UserCompleteDto> userCompleteDtoList = new ArrayList<>();
        userCompleteDtoList.add(new UserCompleteDto(1L, Utils.getCurrentDate(), true, true, "1234567890", null, Utils.getCurrentDate(), 1L, null, null, null, null,
                null, null, null, null, null, null, null, null,
                null, null, null, null, null, null, null, null, null));

        when(userRepository.findByIdAndIsDeletedAndClientId(anyLong(), anyBoolean(), any())).thenReturn(user);
        when(userRepository.getAllUserDetailByUserId(any(), any())).thenReturn(userCompleteDtoList);
        when(verificationRepository.findAllByUserIdAndStoppedAtNull(anyLong())).thenReturn(new ArrayList<>());
        when(externalIdRepository.findAllByUserId(anyLong())).thenReturn(new ArrayList<>());

        UserResponse userResponse = userService.get(1L, 1L);
        Assert.assertNotNull(userResponse);
        Assert.assertEquals(1L, userResponse.getMobileList().size());
        Assert.assertEquals(0L, userResponse.getEmailList().size());
        verify(userRepository, Mockito.times(1)).getAllUserDetailByUserId(any(), any());
    }

    @Test
    public void testGetUserByIdWithVerifications() throws Exception
    {
        User user = new User();
        user.setId(1L);
        Date now = new Date();

        when(userRepository.findByIdAndIsDeletedAndClientId(anyLong(), anyBoolean(), any())).thenReturn(user);
        when(userMobileRepository.findAllByUserIdAndStoppedAtNull(anyLong())).thenReturn(new ArrayList<>());
        when(userEmailRepository.findAllByUserIdAndStoppedAtNull(anyLong())).thenReturn(new ArrayList<>());
        when(verificationRepository.findAllByUserIdAndStoppedAtNull(anyLong())).thenReturn(Collections.singletonList(new Verification(1L, 1L, "AADHAR_OTP", "PRIMARYPHONE", "9876543210", "1234-5678", now, null)));
        when(userAddressRepository.findByUserId(anyLong())).thenReturn(new UserAddress());
        when(userContactPersonRepository.findByUserId(anyLong())).thenReturn(new UserContactPerson());

        UserResponse userResponse = userService.get(1L, 1L);
        Assert.assertNotNull(userResponse);
        Assert.assertEquals(1, userResponse.getVerifications().size());
    }

    @Test
    public void testGetUserByIdWithExternalIds() throws Exception
    {
        User user = new User();
        user.setId(1L);
        Date now = new Date();

        when(userRepository.findByIdAndIsDeletedAndClientId(anyLong(), anyBoolean(), any())).thenReturn(user);
        when(userMobileRepository.findAllByUserIdAndStoppedAtNull(anyLong())).thenReturn(new ArrayList<>());
        when(userEmailRepository.findAllByUserIdAndStoppedAtNull(anyLong())).thenReturn(new ArrayList<>());
        when(externalIdRepository.findAllByUserId(anyLong())).thenReturn(Collections.singletonList(new ExternalId(1L, 1L, "HEALTH_ID", "12345678", now)));
        when(userAddressRepository.findByUserId(anyLong())).thenReturn(new UserAddress());
        when(userContactPersonRepository.findByUserId(anyLong())).thenReturn(new UserContactPerson());

        UserResponse userResponse = userService.get(1L, 1L);
        Assert.assertNotNull(userResponse);
        Assert.assertEquals(1, userResponse.getExternalIds().size());
    }

    @Test
    public void testGetUserById_NotFound() throws Exception
    {
        when(userRepository.findByIdAndIsDeletedAndClientId(anyLong(), anyBoolean(), any())).thenReturn(null);

        UserResponse userResponse = userService.get(1L, 1L);
        Assert.assertNull(userResponse);
    }


    @Test
    public void testGetByPrimaryPhone() throws Exception
    {
        Date now = new Date();

        User user = new User();
        user.setId(1L);

        when(userMobileRepository.findAllByNumberAndIsPrimaryTrue(anyString())).thenReturn(Collections.singletonList(new UserMobile("9988799887", 1L, true, now, now, false, "patient")));
        when(userRepository.findAllByIdInAndIsDeletedAndClientId(anyList(), anyBoolean(), anyLong())).thenReturn(Collections.singletonList(user));
        when(userMobileRepository.findAllByUserIdInAndStoppedAtNull(anyList())).thenReturn(Arrays.asList(
            new UserMobile("9988799887", 1L, true, now, now, false, "patient"),
            new UserMobile("9988799886", 1L, false, now, now, false, "family")
        ));
        when(userEmailRepository.findAllByUserIdInAndStoppedAtNull(anyList())).thenReturn(Arrays.asList(
            new UserEmail("abcd@gmail.com", 1L, true, now, now),
            new UserEmail("abcd@gmail.com", 1L, false, now, now)
        ));

        List<UserResponse> users = userService.getByPrimaryPhone("9988799887", 1L);
        Assert.assertEquals(1, users.size());
    }

    @Test(expected = ValidationException.class)
    public void testGetByPrimaryPhone_SpecialCharacters() throws Exception
    {
        userService.getByPrimaryPhone("@#9988799887", 1L);
    }

    @Test(expected = ValidationException.class)
    public void testGetByPrimaryPhone_Alphabets() throws Exception
    {
        userService.getByPrimaryPhone("99887asdf99887", 1L);
    }

    @Test(expected = NotFoundException.class)
    public void testDeleteUser_NotFound()
    {
        long clientId = 1L;
        when(userRepository.findByIdAndIsDeletedAndClientId(anyLong(), anyBoolean(), anyLong())).thenReturn(null);

        DeleteUserRequest deleteUserRequest = new DeleteUserRequest(1L, 1L, "USER_ACCESS");

        userService.delete(deleteUserRequest, 1L);
    }

    @Test
    public void testDeleteUser_Success()
    {
        Date now = new Date();
        User user = new User();
        user.setDateOfBirth(new Date());
        when(userRepository.findByIdAndIsDeletedAndClientId(anyLong(), anyBoolean(), anyLong())).thenReturn(user);
        when(userMobileRepository.findAllByUserIdAndStoppedAtNull(anyLong())).thenReturn(Collections.singletonList(new UserMobile("9988799887", 1L, false, now, now, false, null)));
        when(userEmailRepository.findAllByUserIdAndStoppedAtNull(anyLong())).thenReturn(Collections.singletonList(new UserEmail("abcd@gmail.com", 1L, false, now, now)));

        when(userRepository.save(any())).thenReturn(new User());
        when(userMobileRepository.saveAll(anyList())).thenReturn(new ArrayList<>());
        when(userEmailRepository.saveAll(anyList())).thenReturn(new ArrayList<>());

        DeleteUserRequest deleteUserRequest = new DeleteUserRequest(1L, 1L, "USER_ACCESS");

        userService.delete(deleteUserRequest, 1L);

        verify(userRepository, Mockito.times(1)).save(any());
        verify(userMobileRepository, Mockito.times(1)).saveAll(anyList());
        verify(userEmailRepository, Mockito.times(1)).saveAll(anyList());
    }

    @Test(expected = ValidationException.class)
    public void testCreateUser_ValidationErrorName() throws Exception
    {
        AddUserRequest addUserRequest = new AddUserRequest();
        userService.create(addUserRequest, 1L, true);
    }

    @Test(expected = ValidationException.class)
    public void testCreateUser_ValidationErrorDateOfBirth() throws Exception
    {
        AddUserRequest addUserRequest = new AddUserRequest();
        addUserRequest.setFirstName("ABCD");
        addUserRequest.setLastName("EFGH");
        userService.create(addUserRequest, 1L, true);
    }

    @Test(expected = ValidationException.class)
    public void testCreateUser_ValidationErrorAddress() throws Exception
    {
        AddUserRequest addUserRequest = new AddUserRequest();
        addUserRequest.setFirstName("ABCD");
        addUserRequest.setLastName("EFGH");
        addUserRequest.setDateOfBirth(Utils.getFormattedDate(new Date(), "yyyy-MM-dd"));
        userService.create(addUserRequest, 1L, true);
    }

    @Test(expected = ValidationException.class)
    public void testCreateUser_ValidationErrorCity() throws Exception
    {
        AddUserRequest addUserRequest = new AddUserRequest();
        addUserRequest.setFirstName("ABCD");
        addUserRequest.setLastName("EFGH");
        addUserRequest.setDateOfBirth(Utils.getFormattedDate(new Date(), "yyyy-MM-dd"));
        addUserRequest.setAddress("abcd");
        userService.create(addUserRequest, 1L,true);
    }

    @Test(expected = ValidationException.class)
    public void testCreateUser_ValidationErrorNoMobile() throws Exception
    {
        AddUserRequest addUserRequest = new AddUserRequest();
        addUserRequest.setFirstName("ABCD");
        addUserRequest.setLastName("EFGH");
        addUserRequest.setDateOfBirth(Utils.getFormattedDate(new Date(), "yyyy-MM-dd"));
        addUserRequest.setAddress("abcd");
        addUserRequest.setCity("abc");
        userService.create(addUserRequest, 1L, true);
    }

    @Test(expected = ValidationException.class)
    public void testCreateUser_ValidationErrorMobileNull() throws Exception
    {
        AddUserRequest addUserRequest = new AddUserRequest();
        addUserRequest.setFirstName("ABCD");
        addUserRequest.setLastName("EFGH");
        addUserRequest.setDateOfBirth(Utils.getFormattedDate(new Date(), "yyyy-MM-dd"));
        addUserRequest.setAddress("abcd");
        addUserRequest.setCity("abc");
        addUserRequest.setMobileList(null);
        userService.create(addUserRequest, 1L, true);
    }

    @Test
    public void testCreateUser_Success() throws Exception
    {
        AddUserRequest addUserRequest = new AddUserRequest();
        addUserRequest.setFirstName("ABCD");
        addUserRequest.setLastName("EFGH");
        addUserRequest.setDateOfBirth(Utils.getFormattedDate(new Date(), "yyyy-MM-dd"));
        addUserRequest.setAddress("abcd");
        addUserRequest.setCity("abc");
        addUserRequest.setGender("Male");

        addUserRequest.setMobileList(Collections.singletonList(new UserMobileRequest("9988799887", true, false, null)));
        addUserRequest.setEmailList(Collections.singletonList(new UserEmailRequest("john.doe@gmail.com", true)));

        when(userRepository.save(any())).thenReturn(new User());

        when(userMobileRepository.saveAll(anyList())).thenReturn(new ArrayList<>());
        when(userEmailRepository.saveAll(anyList())).thenReturn(new ArrayList<>());
        doNothing().when(rabbitMQPublisherService).send(any(), anyString(), anyString());

        userService.create(addUserRequest, 1L, true);

        verify(userRepository, Mockito.times(1)).save(any());
        verify(userMobileRepository, Mockito.times(1)).saveAll(anyList());
        verify(userEmailRepository, Mockito.times(1)).saveAll(anyList());
        verify(rabbitMQPublisherService, Mockito.times(1)).send(any(), anyString(), anyString());
    }

    @Test
    public void testCreateUserMultipleMobileNumbers_Success() throws Exception
    {
        long userId = 1L;
        String mobileNumber1 = "9988799887";
        String mobileNumber2 = "9121231231";
        String mobileOwner1 = "patient";

        AddUserRequest addUserRequest = new AddUserRequest();
        addUserRequest.setFirstName("ABCD");
        addUserRequest.setLastName("EFGH");
        addUserRequest.setDateOfBirth(Utils.getFormattedDate(new Date(), "yyyy-MM-dd"));
        addUserRequest.setAddress("abcd");
        addUserRequest.setCity("abc");
        addUserRequest.setGender("Male");

        List<UserMobileRequest> mobileList = new ArrayList<>();
        UserMobileRequest userMobileRequest1 = new UserMobileRequest(mobileNumber1, true, false, mobileOwner1);
        UserMobileRequest userMobileRequest2 = new UserMobileRequest(mobileNumber2, false, false, null);
        mobileList.add(userMobileRequest1);
        mobileList.add(userMobileRequest2);

        addUserRequest.setMobileList(mobileList);
        addUserRequest.setEmailList(Collections.singletonList(new UserEmailRequest("john.doe@gmail.com", true)));

        List<UserMobile> userMobiles = new ArrayList<>();
        UserMobile userMobile1 = new UserMobile(mobileNumber1, userId, true, null, null, false, mobileOwner1);
        UserMobile userMobile2 = new UserMobile(mobileNumber2, userId, false, null, null, false, null);
        userMobiles.add(userMobile1);
        userMobiles.add(userMobile2);

        when(userRepository.save(any())).thenReturn(new User());

        when(userMobileRepository.saveAll(anyList())).thenReturn(userMobiles);
        when(userEmailRepository.saveAll(anyList())).thenReturn(new ArrayList<>());
        doNothing().when(rabbitMQPublisherService).send(any(), anyString(), anyString());

        UserResponse userResponse = userService.create(addUserRequest, 1L, true);
        verify(userRepository, Mockito.times(1)).save(any());
        verify(userMobileRepository, Mockito.times(1)).saveAll(anyList());
        verify(userEmailRepository, Mockito.times(1)).saveAll(anyList());
        verify(rabbitMQPublisherService, Mockito.times(1)).send(any(), anyString(), anyString());
        Assert.assertEquals(mobileNumber1, userResponse.getMobileList().get(0).getNumber());
        Assert.assertEquals(mobileOwner1, userResponse.getMobileList().get(0).getOwner());
        Assert.assertEquals(mobileNumber2, userResponse.getMobileList().get(1).getNumber());
        Assert.assertEquals(null, userResponse.getMobileList().get(1).getOwner());

    }

    @Test
    public void testCreateUser_SuccessWithoutEmail() throws Exception
    {
        AddUserRequest addUserRequest = new AddUserRequest();
        addUserRequest.setFirstName("ABCD");
        addUserRequest.setLastName("EFGH");
        addUserRequest.setDateOfBirth(Utils.getFormattedDate(new Date(), "yyyy-MM-dd"));
        addUserRequest.setAddress("abcd");
        addUserRequest.setCity("abc");
        addUserRequest.setGender("Male");

        addUserRequest.setMobileList(Collections.singletonList(new UserMobileRequest("9988799887", true, false, null)));
        addUserRequest.setEmailList(new ArrayList<>());

        when(userRepository.save(any())).thenReturn(new User());
        when(userMobileRepository.saveAll(anyList())).thenReturn(new ArrayList<>());
        doNothing().when(rabbitMQPublisherService).send(any(), anyString(), anyString());

        userService.create(addUserRequest, 1L, true);

        verify(userRepository, Mockito.times(1)).save(any());
        verify(userMobileRepository, Mockito.times(1)).saveAll(anyList());
        verify(userEmailRepository, Mockito.times(0)).saveAll(anyList());
        verify(rabbitMQPublisherService, Mockito.times(1)).send(any(), anyString(), anyString());
    }

    @Test
    public void testCreateUser_SuccessNullEmails() throws Exception
    {
        AddUserRequest addUserRequest = new AddUserRequest();
        addUserRequest.setFirstName("ABCD");
        addUserRequest.setLastName("EFGH");
        addUserRequest.setDateOfBirth(Utils.getFormattedDate(new Date(), "yyyy-MM-dd"));
        addUserRequest.setAddress("abcd");
        addUserRequest.setCity("abc");
        addUserRequest.setGender("Male");

        addUserRequest.setMobileList(Collections.singletonList(new UserMobileRequest("9988799887", true, false, null)));
        addUserRequest.setEmailList(null);

        when(userRepository.save(any())).thenReturn(new User());
        when(userMobileRepository.saveAll(anyList())).thenReturn(new ArrayList<>());
        doNothing().when(rabbitMQPublisherService).send(any(), anyString(), anyString());

        userService.create(addUserRequest, 1L, true);

        verify(userRepository, Mockito.times(1)).save(any());
        verify(userMobileRepository, Mockito.times(1)).saveAll(anyList());
        verify(userEmailRepository, Mockito.times(0)).saveAll(anyList());
        verify(rabbitMQPublisherService, Mockito.times(1)).send(any(), anyString(), anyString());
    }

    @Test(expected = NotFoundException.class)
    public void testUpdateUser_NotFound() throws Exception
    {
        when(userRepository.findByIdAndIsDeletedAndClientId(anyLong(), anyBoolean(), anyLong())).thenReturn(null);

        userService.update(new UpdateUserRequest(), 1L);
    }

    @Test
    public void testUpdateUser_EmptyRequest() throws Exception
    {
        User user = new User();
        user.setDateOfBirth(new Date());

        when(userRepository.findByIdAndIsDeletedAndClientId(anyLong(), anyBoolean(), anyLong())).thenReturn(user);

        UpdateUserRequest updateUserRequest = new UpdateUserRequest();
        updateUserRequest.setUserId(1L);
        userService.update(updateUserRequest, 1L);

        verify(userRepository, Mockito.times(0)).save(any());
    }

    @Test
    public void testUpdateUser_Success() throws Exception
    {
        User user = new User();
        user.setDateOfBirth(new Date());
        user.setFirstName("abcd");
        user.setLastName("efgh");
        user.setGender("Male");
        user.setLanguage("Kannada");
        user.setAddress("abcd");
        user.setFatherName("abc");
        user.setPincode(123456);
        user.setCity("abcd");
        user.setMaritalStatus("Single");
        user.setSocioEconomicStatus("BPL");
        user.setKeyPopulation("abcd");
        user.setHeight(100);
        user.setWeight(100);
        user.setOccupation("Staff");

        when(userRepository.findByIdAndIsDeletedAndClientId(anyLong(), anyBoolean(), anyLong())).thenReturn(user);

        when(userRepository.save(any())).thenReturn(new User());

        UpdateUserRequest updateUserRequest = new UpdateUserRequest();
        updateUserRequest.setUserId(1L);
        updateUserRequest.setDateOfBirth("2020-01-01");
        updateUserRequest.setFirstName("efgh");
        updateUserRequest.setLastName("abcd");
        updateUserRequest.setGender("Female");
        updateUserRequest.setLanguage("Hindi");
        updateUserRequest.setAddress("efgh");
        updateUserRequest.setFatherName("efg");
        updateUserRequest.setPincode(123457);
        updateUserRequest.setCity("efgh");
        updateUserRequest.setMaritalStatus("Married");
        updateUserRequest.setSocioEconomicStatus("APL");
        updateUserRequest.setKeyPopulation("efgh");
        updateUserRequest.setHeight(200);
        updateUserRequest.setWeight(200);
        updateUserRequest.setOccupation("Not Staff");

        userService.update(updateUserRequest, 1L);

        verify(userRepository, Mockito.times(1)).save(any());
    }

    @Test
    public void testUpdateUser_HeightWeightEmpty() throws Exception
    {
        User user = new User();
        user.setId(1L);

        when(userRepository.findByIdAndIsDeletedAndClientId(anyLong(), anyBoolean(), anyLong())).thenReturn(user);

        when(userRepository.save(any())).thenReturn(new User());

        UpdateUserRequest updateUserRequest = new UpdateUserRequest();
        updateUserRequest.setUserId(1L);
        updateUserRequest.setHeight(100);
        updateUserRequest.setWeight(200);

        userService.update(updateUserRequest, 1L);

        verify(userRepository, Mockito.times(1)).save(any());
    }

    @Test
    public void testUpdateUser_Unchanged() throws Exception
    {
        Date now = new Date();
        User user = new User();
        user.setDateOfBirth(now);
        user.setFirstName("abcd");
        user.setLastName("efgh");
        user.setGender("Male");
        user.setLanguage("Kannada");
        user.setAddress("abcd");
        user.setFatherName("abc");
        user.setPincode(123456);
        user.setCity("abcd");
        user.setMaritalStatus("Single");
        user.setSocioEconomicStatus("BPL");
        user.setKeyPopulation("abcd");
        user.setHeight(100);
        user.setWeight(100);

        when(userRepository.findByIdAndIsDeletedAndClientId(anyLong(), anyBoolean(), anyLong())).thenReturn(user);

        UpdateUserRequest updateUserRequest = new UpdateUserRequest();
        updateUserRequest.setUserId(1L);
        updateUserRequest.setDateOfBirth(Utils.getFormattedDate(now, "yyyy-MM-dd"));
        updateUserRequest.setFirstName("abcd");
        updateUserRequest.setLastName("efgh");
        updateUserRequest.setGender("Male");
        updateUserRequest.setLanguage("Kannada");
        updateUserRequest.setAddress("abcd");
        updateUserRequest.setFatherName("abc");
        updateUserRequest.setPincode(123456);
        updateUserRequest.setCity("abcd");
        updateUserRequest.setMaritalStatus("Single");
        updateUserRequest.setSocioEconomicStatus("BPL");
        updateUserRequest.setKeyPopulation("abcd");
        updateUserRequest.setHeight(100);
        updateUserRequest.setWeight(100);

        userService.update(updateUserRequest, 1L);

        verify(userRepository, Mockito.times(0)).save(any());
    }

    @Test
    public void testUpdateUser_Verifications() throws Exception
    {
        Long userId = 1L;
        Long clientId = 1L;
        Date now = new Date();

        Verification verification = new Verification(1L, userId, "AADHAR_OTP", "PRIMARYPHONE", "9876543210", "1234-5678", now, null);

        User user = new User();
        user.setId(userId);

        when(userRepository.findByIdAndIsDeletedAndClientId(userId, false, clientId)).thenReturn(user);

        UpdateUserRequest updateUserRequest = new UpdateUserRequest();
        updateUserRequest.setUserId(1L);

        updateUserRequest.setVerifications(Collections.singletonList(new VerificationRequest(verification.getType(), verification.getEntityType(), verification.getEntityValue(), verification.getTransactionId())));

        userService.update(updateUserRequest, clientId);

        verify(userRepository, Mockito.times(0)).save(any());
        verify(verificationRepository, Mockito.times(1)).saveAll(any());
    }

    @Test
    public void testUpdateUser_VerificationsEmpty() throws Exception
    {
        Long userId = 1L;
        Long clientId = 1L;
        Date now = new Date();

        User user = new User();
        user.setId(userId);

        when(userRepository.findByIdAndIsDeletedAndClientId(userId, false, clientId )).thenReturn(user);

        UpdateUserRequest updateUserRequest = new UpdateUserRequest();
        updateUserRequest.setUserId(1L);

        updateUserRequest.setVerifications(new ArrayList<>());

        userService.update(updateUserRequest, clientId );

        verify(userRepository, Mockito.times(0)).save(any());
        verify(verificationRepository, Mockito.times(0)).saveAll(any());
    }

    @Test
    public void testUpdateUser_MobileListEmpty() throws Exception
    {
        Long userId = 1L;
        Long clientId = 1L;
        Date now = new Date();

        User user = new User();
        user.setId(userId);

        when(userRepository.findByIdAndIsDeletedAndClientId(userId, false, clientId )).thenReturn(user);

        UpdateUserRequest updateUserRequest = new UpdateUserRequest();
        updateUserRequest.setUserId(1L);

        updateUserRequest.setMobileList(new ArrayList<>());

        userService.update(updateUserRequest, clientId );

        verify(userRepository, Mockito.times(0)).save(any());
        verify(userMobileRepository, Mockito.times(0)).saveAll(any());
    }

    @Test
    public void testUpdateUser_WithMobileList() throws Exception
    {
        Long userId = 1L;
        Long clientId = 1L;
        Date now = new Date();

        User user = new User();
        user.setId(userId);

        when(userRepository.findByIdAndIsDeletedAndClientId(userId, false, clientId )).thenReturn(user);

        UpdateUserRequest updateUserRequest = new UpdateUserRequest();
        updateUserRequest.setUserId(1L);
        List<UserMobileRequest> mobileList = new ArrayList<>();
        mobileList.add(new UserMobileRequest("9727832", true, true, "patient"));
        mobileList.add(new UserMobileRequest("91512121", true, true, null));
        updateUserRequest.setMobileList(mobileList);

        userService.update(updateUserRequest, clientId );

        verify(userRepository, Mockito.times(0)).save(any());
        verify(userMobileRepository, Mockito.times(1)).saveAll(any());
    }

    @Test
    public void testUpdateUser_EmailListEmpty() throws Exception
    {
        Long userId = 1L;
        Long clientId = 1L;
        Date now = new Date();

        User user = new User();
        user.setId(userId);

        when(userRepository.findByIdAndIsDeletedAndClientId(userId, false, clientId )).thenReturn(user);

        UpdateUserRequest updateUserRequest = new UpdateUserRequest();
        updateUserRequest.setUserId(1L);

        updateUserRequest.setEmailList(new ArrayList<>());

        userService.update(updateUserRequest, clientId );

        verify(userRepository, Mockito.times(0)).save(any());
        verify(userEmailRepository, Mockito.times(0)).saveAll(any());
    }

    @Test
    public void testUpdateUser_WithEmailList() throws Exception
    {
        Long userId = 1L;
        Long clientId = 1L;
        Date now = new Date();

        User user = new User();
        user.setId(userId);

        when(userRepository.findByIdAndIsDeletedAndClientId(userId, false, clientId )).thenReturn(user);

        UpdateUserRequest updateUserRequest = new UpdateUserRequest();
        updateUserRequest.setUserId(1L);
        List<UserEmailRequest> emailList = new ArrayList<>();
        emailList.add(new UserEmailRequest("manan@gmail.com",true));
        updateUserRequest.setEmailList(emailList);

        userService.update(updateUserRequest, clientId );

        verify(userRepository, Mockito.times(0)).save(any());
        verify(userEmailRepository, Mockito.times(1)).saveAll(any());
    }

    @Test
    public void testUpdateUser_ExternalIds() throws Exception
    {
        Long userId = 1L;
        Long clientId = 1L;
        Date now = new Date();

        ExternalId externalId = new ExternalId(1L, userId, "HEALTH_ID", "123456789", now);

        User user = new User();
        user.setId(userId);

        when(userRepository.findByIdAndIsDeletedAndClientId(userId, false, clientId)).thenReturn(user);

        UpdateUserRequest updateUserRequest = new UpdateUserRequest();
        updateUserRequest.setUserId(1L);

        updateUserRequest.setExternalIds(Collections.singletonList(new ExternalIdRequest(externalId.getType(), externalId.getValue())));

        userService.update(updateUserRequest, clientId);

        verify(userRepository, Mockito.times(0)).save(any());
        verify(externalIdRepository, Mockito.times(1)).saveAll(any());
    }

    @Test
    public void testUpdateUser_ExternalIdsEmpty() throws Exception
    {
        Long userId = 1L;
        Long clientId = 1L;
        Date now = new Date();

        User user = new User();
        user.setId(userId);

        when(userRepository.findByIdAndIsDeletedAndClientId(userId, false, clientId)).thenReturn(user);

        UpdateUserRequest updateUserRequest = new UpdateUserRequest();
        updateUserRequest.setUserId(1L);

        updateUserRequest.setExternalIds(new ArrayList<>());

        userService.update(updateUserRequest, clientId);

        verify(userRepository, Mockito.times(0)).save(any());
        verify(externalIdRepository, Mockito.times(0)).saveAll(any());
    }

    @Test(expected = NotFoundException.class)
    public void testUpdateMobiles_NotFound()
    {
        when(userRepository.findByIdAndIsDeletedAndClientId(anyLong(), anyBoolean(), any())).thenReturn(null);
        UpdateMobileRequest updateMobileRequest = new UpdateMobileRequest(1L, Collections.singletonList(new UserMobileRequest("9988799887", true, false, null)));

        userService.updateMobiles(updateMobileRequest, 1L);
    }

    @Test
    public void testUpdateMobiles_Success()
    {
        long clientId = 1L;
        User user = new User();
        Date now = new Date();
        user.setDateOfBirth(now);
        List<UserMobile> userMobiles = new ArrayList<>();

        userMobiles.add(new UserMobile("9988799887", 1L, true, now, now, true, null));
        userMobiles.add(new UserMobile("9988699886", 1L, false, now, now, false, null));

        when(userRepository.findByIdAndIsDeletedAndClientId(anyLong(), anyBoolean(), any())).thenReturn(user);
        when(userMobileRepository.saveAll(anyList())).thenReturn(new ArrayList<>());
        when(userMobileRepository.findAllByUserIdAndStoppedAtNull(anyLong())).thenReturn(userMobiles);

        UpdateMobileRequest updateMobileRequest = new UpdateMobileRequest(1L, Arrays.asList(
            new UserMobileRequest("9988699886", false, false, null),
            new UserMobileRequest("9988899888", true, true, null)
        ));

        userService.updateMobiles(updateMobileRequest, clientId);

        verify(userMobileRepository, Mockito.times(1)).saveAll(anyList());
    }

    @Test
    public void testUpdateMobiles_Unchanged()
    {
        long clientId = 1L;
        User user = new User();
        Date now = new Date();
        user.setDateOfBirth(now);
        List<UserMobile> userMobiles = new ArrayList<>();

        userMobiles.add(new UserMobile("9988799887", 1L, true, now, now, false, null));
        userMobiles.add(new UserMobile("9988699886", 1L, false, now, now, false, null));

        when(userRepository.findByIdAndIsDeletedAndClientId(anyLong(), anyBoolean(), anyLong())).thenReturn(user);
        when(userMobileRepository.findAllByUserIdAndStoppedAtNull(anyLong())).thenReturn(userMobiles);

        UpdateMobileRequest updateMobileRequest = new UpdateMobileRequest(1L, Arrays.asList(
                new UserMobileRequest("9988799887", true, false, null),
                new UserMobileRequest("9988699886", false, false, null)
        ));

        userService.updateMobiles(updateMobileRequest,clientId);

        verify(userMobileRepository, Mockito.times(0)).saveAll(anyList());
    }

    @Test
    public void testUpdateMobileOwners_Success()
    {
        long clientId = 1L;
        User user = new User();
        Date now = new Date();
        user.setDateOfBirth(now);
        List<UserMobile> userMobiles = new ArrayList<>();

        userMobiles.add(new UserMobile("9988799887", 1L, true, now, now, false, null));
        userMobiles.add(new UserMobile("9988699886", 1L, false, now, now, false, null));

        when(userRepository.findByIdAndIsDeletedAndClientId(anyLong(), anyBoolean(), anyLong())).thenReturn(user);
        when(userMobileRepository.findAllByUserIdAndStoppedAtNull(anyLong())).thenReturn(userMobiles);

        UpdateMobileRequest updateMobileRequest = new UpdateMobileRequest(1L, Arrays.asList(
                new UserMobileRequest("9988799887", true, false, "Patient"),
                new UserMobileRequest("9988699886", false, false, "Family")
        ));

        userService.updateMobiles(updateMobileRequest,clientId);

        verify(userMobileRepository, Mockito.times(1)).saveAll(anyList());
    }

    @Test
    public void testUpdateMobiles_ChangePrimary()
    {
        long clientId = 1L;
        User user = new User();
        Date now = new Date();
        user.setDateOfBirth(now);
        List<UserMobile> userMobiles = new ArrayList<>();

        userMobiles.add(new UserMobile("9988799887", 1L, true, now, now, false, null));
        userMobiles.add(new UserMobile("9988699886", 1L, false, now, now, false, null));

        when(userRepository.findByIdAndIsDeletedAndClientId(anyLong(), anyBoolean(), anyLong())).thenReturn(user);
        when(userMobileRepository.findAllByUserIdAndStoppedAtNull(anyLong())).thenReturn(userMobiles);

        UpdateMobileRequest updateMobileRequest = new UpdateMobileRequest(1L, Arrays.asList(
                new UserMobileRequest("9988799887", false, false, null),
                new UserMobileRequest("9988699886", true, false, null)
        ));

        userService.updateMobiles(updateMobileRequest, clientId);

        verify(userMobileRepository, Mockito.times(1)).saveAll(anyList());
    }

    @Test(expected = NotFoundException.class)
    public void testUpdateEmails_NotFound()
    {
        when(userRepository.findByIdAndIsDeletedAndClientId(anyLong(), anyBoolean(), any())).thenReturn(null);
        UpdateEmailRequest updateEmailRequest = new UpdateEmailRequest(1L, Collections.singletonList(new UserEmailRequest("abcd@gmail.com", true)));

        userService.updateEmails(updateEmailRequest, 1L);
    }

    @Test
    public void testUpdateEmails_Success()
    {
        long clientId = 1L;
        User user = new User();
        Date now = new Date();
        user.setDateOfBirth(now);
        List<UserEmail> userEmails = new ArrayList<>();

        userEmails.add(new UserEmail("abcd@gmail.com", 1L, true, now, now));
        userEmails.add(new UserEmail("efgh@gmail.com", 1L, false, now, now));

        when(userRepository.findByIdAndIsDeletedAndClientId(anyLong(), anyBoolean(), anyLong())).thenReturn(user);
        when(userEmailRepository.saveAll(anyList())).thenReturn(new ArrayList<>());
        when(userEmailRepository.findAllByUserIdAndStoppedAtNull(anyLong())).thenReturn(userEmails);

        UpdateEmailRequest updateEmailRequest = new UpdateEmailRequest(1L, Arrays.asList(
                new UserEmailRequest("efgh@gmail.com", false),
                new UserEmailRequest("ijkl@gmail.com", true)
        ));

        userService.updateEmails(updateEmailRequest, clientId);

        verify(userEmailRepository, Mockito.times(1)).saveAll(anyList());
    }

    @Test
    public void testUpdateEmails_Unchanged()
    {
        long clientId = 1L;
        User user = new User();
        Date now = new Date();
        user.setDateOfBirth(now);
        List<UserEmail> userEmails = new ArrayList<>();

        userEmails.add(new UserEmail("abcd@gmail.com", 1L, true, now, now));
        userEmails.add(new UserEmail("efgh@gmail.com", 1L, false, now, now));

        when(userRepository.findByIdAndIsDeletedAndClientId(anyLong(), anyBoolean(), anyLong())).thenReturn(user);
        when(userEmailRepository.findAllByUserIdAndStoppedAtNull(anyLong())).thenReturn(userEmails);

        UpdateEmailRequest updateEmailRequest = new UpdateEmailRequest(1L, Arrays.asList(
                new UserEmailRequest("abcd@gmail.com", true),
                new UserEmailRequest("efgh@gmail.com", false)
        ));

        userService.updateEmails(updateEmailRequest, clientId);

        verify(userEmailRepository, Mockito.times(0)).saveAll(anyList());
    }

    @Test
    public void testUpdateEmails_ChangePrimary()
    {
        long clientId = 1L;
        User user = new User();
        Date now = new Date();
        user.setDateOfBirth(now);
        List<UserEmail> userEmails = new ArrayList<>();

        userEmails.add(new UserEmail("abcd@gmail.com", 1L, true, now, now));
        userEmails.add(new UserEmail("efgh@gmail.com", 1L, false, now, now));

        when(userRepository.findByIdAndIsDeletedAndClientId(anyLong(), anyBoolean(), anyLong())).thenReturn(user);
        when(userEmailRepository.findAllByUserIdAndStoppedAtNull(anyLong())).thenReturn(userEmails);

        UpdateEmailRequest updateEmailRequest = new UpdateEmailRequest(1L, Arrays.asList(
                new UserEmailRequest("abcd@gmail.com", false),
                new UserEmailRequest("efgh@gmail.com", true)
        ));

        userService.updateEmails(updateEmailRequest, clientId);

        verify(userEmailRepository, Mockito.times(1)).saveAll(anyList());
    }

    @Test
    public void testFetchDuplicates() throws Exception
    {
        FetchDuplicatesRequestForPrimaryPhoneAndGender request = new FetchDuplicatesRequestForPrimaryPhoneAndGender("9988799887", "Male");
        request.setOffset(0);
        request.setCount(10);
        request.setScheme(DeduplicationScheme.PRIMARYPHONE_GENDER);

        Date now = new Date();
        List<UserMobile> userMobiles = Collections.singletonList(
                new UserMobile("9988799887", 1L, true, now, now, false, null)
        );

        when(deduplicationHandlerMap.getHandler(any())).thenReturn(deduplicationHandler);

        User sampleUser = new User();
        sampleUser.setFirstName("John");
        sampleUser.setLastName("Doe");
        sampleUser.setId(1L);

        when(deduplicationHandler.fetchDuplicates(any(), any())).thenReturn(Collections.singletonList(sampleUser));

        when(userMobileRepository.findAllByUserIdInAndStoppedAtNull(anyList())).thenReturn(userMobiles);

        List<UserEmail> userEmails = Collections.singletonList(
                new UserEmail("abcd@gmail.com", 1L, true, now, now)
        );
        when(userEmailRepository.findAllByUserIdInAndStoppedAtNull(anyList())).thenReturn(userEmails);

        List<UserResponse> users = userService.fetchDuplicates(request, 1L);
        Assert.assertEquals(1, users.size());
    }

    @Test
    public void testFetchDuplicates_SharedPrimaryPhone() throws Exception
    {
        FetchDuplicatesRequestForPrimaryPhoneAndGender request = new FetchDuplicatesRequestForPrimaryPhoneAndGender("9988799887", "Male");
        request.setOffset(0);
        request.setCount(10);
        request.setScheme(DeduplicationScheme.PRIMARYPHONE_GENDER);

        Date now = new Date();
        List<UserMobile> userMobiles = Arrays.asList(
                new UserMobile("9988799887", 1L, true, now, now, false, null),
                new UserMobile("9988799887", 2L, true, now, now, false, null)
        );

        when(deduplicationHandlerMap.getHandler(any())).thenReturn(deduplicationHandler);

        User sampleUser = new User();
        sampleUser.setFirstName("John");
        sampleUser.setLastName("Doe");
        sampleUser.setId(1L);

        User sampleUser2 = new User();
        sampleUser2.setFirstName("Jesus");
        sampleUser2.setLastName("Christ");
        sampleUser2.setId(2L);

        when(deduplicationHandler.fetchDuplicates(any(), any())).thenReturn(Arrays.asList(
            sampleUser,
            sampleUser2
        ));

        when(userMobileRepository.findAllByUserIdInAndStoppedAtNull(anyList())).thenReturn(userMobiles);

        List<UserEmail> userEmails = Arrays.asList(
                new UserEmail("abcd@gmail.com", 1L, true, now, now),
                new UserEmail("efgh@gmail.com", 2L, true, now, now)
        );
        when(userEmailRepository.findAllByUserIdInAndStoppedAtNull(anyList())).thenReturn(userEmails);

        List<UserResponse> users = userService.fetchDuplicates(request, 1L);
        Assert.assertEquals(2, users.size());
    }

    @Test
    public void testFetchDuplicates_NoMatches() throws Exception
    {
        FetchDuplicatesRequestForPrimaryPhoneAndGender request = new FetchDuplicatesRequestForPrimaryPhoneAndGender("9988799887", "Male");
        request.setOffset(0);
        request.setCount(10);
        request.setScheme(DeduplicationScheme.PRIMARYPHONE_GENDER);

        Date now = new Date();
        List<UserMobile> userMobiles = Collections.singletonList(
                new UserMobile("9988799887", 1L, true, now, now, false, null)
        );

        when(deduplicationHandlerMap.getHandler(any())).thenReturn(deduplicationHandler);

        when(deduplicationHandler.fetchDuplicates(any(), any())).thenReturn(new ArrayList<>());

        List<UserResponse> users = userService.fetchDuplicates(request, 1L);
        Assert.assertEquals(0, users.size());

        verify(userMobileRepository, Mockito.times(0)).findAllByUserIdInAndStoppedAtNull(anyList());
        verify(userEmailRepository, Mockito.times(0)).findAllByUserIdInAndStoppedAtNull(anyList());
    }

    @Test
    public void testFetchDuplicatesMissingMobilesAndEmails() throws Exception
    {
        FetchDuplicatesRequestForPrimaryPhoneAndGender request = new FetchDuplicatesRequestForPrimaryPhoneAndGender("9988799887", "Male");
        request.setOffset(0);
        request.setCount(10);
        request.setScheme(DeduplicationScheme.PRIMARYPHONE_GENDER);

        Date now = new Date();

        when(deduplicationHandlerMap.getHandler(any())).thenReturn(deduplicationHandler);

        User sampleUser = new User();
        sampleUser.setFirstName("John");
        sampleUser.setLastName("Doe");
        sampleUser.setId(1L);

        when(deduplicationHandler.fetchDuplicates(any(), any())).thenReturn(Collections.singletonList(sampleUser));

        when(userMobileRepository.findAllByUserIdInAndStoppedAtNull(anyList())).thenReturn(new ArrayList<>());
        when(userEmailRepository.findAllByUserIdInAndStoppedAtNull(anyList())).thenReturn(new ArrayList<>());

        List<UserResponse> users = userService.fetchDuplicates(request, 1L);
        Assert.assertEquals(1, users.size());
    }

    @Test
    public void testGetUser() {
        Long userId = 1L;
        User user = new User();
        user.setId(userId);
        when(userRepository.findByIdAndIsDeletedAndClientId(anyLong(), anyBoolean(), anyLong())).thenReturn(user);

        User userReturned = userService.getUser(userId, 1L);

        Assert.assertNotNull(userReturned);
        Assert.assertEquals(userId, userReturned.getId());
        Mockito.verify(userRepository, Mockito.times(1)).findByIdAndIsDeletedAndClientId(anyLong(), anyBoolean(), anyLong());
    }

    @Test(expected = NotFoundException.class)
    public void testGetUserWithNonExistingId() {
        Long userId = 1L;
        when(userRepository.findByIdAndIsDeletedAndClientId(anyLong(), anyBoolean(), anyLong())).thenReturn(null);

        userService.getUser(userId, 1L);

        Mockito.verify(userRepository, Mockito.times(1)).findByIdAndIsDeletedAndClientId(anyLong(), anyBoolean(), anyLong());
    }

    @Test
    public void testValidateUserIdList() {
        Long clientId = 1L;
        List<Long> userIdList = new ArrayList<>();
        userIdList.add(1L);
        List<User> userList = new ArrayList<>();
        User user = new User();
        user.setId(1L);
        userList.add(user);
        when(userRepository.findAllByIdInAndIsDeletedAndClientId(anyList(), anyBoolean(), anyLong())).thenReturn(userList);

        userService.validateUserIdList(userIdList, clientId);

        Mockito.verify(userRepository, Mockito.times(1)).findAllByIdInAndIsDeletedAndClientId(anyList(), anyBoolean(), anyLong());
    }

    @Test(expected = NotFoundException.class)
    public void testGetUserListWithUsersNotFound() {
        Long clientId = 1L;
        List<Long> userIdList = new ArrayList<>();
        userIdList.add(1L);
        userIdList.add(2L);
        List<User> userList = new ArrayList<>();
        User user = new User();
        user.setId(1L);
        userList.add(user);
        when(userRepository.findAllByIdInAndIsDeletedAndClientId(anyList(), anyBoolean(), anyLong())).thenReturn(userList);

        userService.validateUserIdList(userIdList, clientId);

        Mockito.verify(userRepository, Mockito.times(1)).findAllByIdInAndIsDeletedAndClientId(anyList(), anyBoolean(), anyLong());
    }

    @Test
    public void testGetRelations() throws Exception {
        List<UserRelation> userRelations = Collections.singletonList(new UserRelation(1L, 123L, 321L, 1, "test", "test", "test", true));
        when(userRelationService.getUserRelations(eq(123L), eq(UserRelationStatus.ACCEPT.getName()))).thenReturn(userRelations);
        Date now = new Date();

        User user1 = new User();
        user1.setId(123L);
        User user2 = new User();
        user2.setId(321L);

        when(clientService.getClientByToken()).thenReturn(new Client(171L, "test", "test", now));
        when(userMobileRepository.findAllByNumberAndIsPrimaryTrue(anyString())).thenReturn(Collections.singletonList(new UserMobile("9988799887", 123L, true, now, now, false, null)));
        when(userRepository.findAllByIdInAndIsDeletedAndClientId(anyList(), anyBoolean(), anyLong())).thenReturn(Collections.singletonList(user2));
        when(userMobileRepository.findAllByUserIdInAndStoppedAtNull(anyList())).thenReturn(Arrays.asList(
                new UserMobile("9988799887", 123L, true, now, now, false, null),
                new UserMobile("9988799886", 321L, true, now, now, false, null)
        ));
        when(userEmailRepository.findAllByUserIdInAndStoppedAtNull(anyList())).thenReturn(Arrays.asList(
                new UserEmail("abcd@gmail.com", 123L, true, now, now),
                new UserEmail("abcd@gmail.com", 321L, true, now, now)
        ));

        List<UserResponse> repsonse = userService.getUserRelations(123L, "ACCEPT");
        Assert.assertEquals(1, repsonse.size());
    }

    @Test
    public void testAddRelation() {
        doNothing().when(userRelationService).addUserRelation(any());
        userService.addUserRelation(null);
        Mockito.verify(userRelationService, Mockito.times(1)).addUserRelation(any());
    }

    @Test
    public void testUpdateRelation() {
        doNothing().when(userRelationService).updateUserRelation(any());
        userService.updateUserRelation(null);
        Mockito.verify(userRelationService, Mockito.times(1)).updateUserRelation(any());
    }

    @Test
    public void testDeleteRelation() {
        doNothing().when(userRelationService).deleteUserRelation(any());
        userService.deleteUserRelation(null);
        Mockito.verify(userRelationService, Mockito.times(1)).deleteUserRelation(any());
    }

    @Test
    public void testGetRelationsPrimaryPhone() throws Exception {
        List<UserRelation> userRelations = Collections.singletonList(new UserRelation(1L, 123L, 321L, 1, "test", "test", "test", true));
        when(userRelationService.getUserRelations(eq(123L), eq(UserRelationStatus.ALL.getName()))).thenReturn(userRelations);
        Date now = new Date();

        User user2 = new User();
        user2.setId(321L);

        when(clientService.getClientByToken()).thenReturn(new Client(171L, "test", "test", now));
        when(userMobileRepository.findAllByNumberAndIsPrimaryTrue(anyString())).thenReturn(Collections.singletonList(new UserMobile("9988799887", 321L, true, now, now, false, null)));
        when(userRepository.findAllByIdInAndIsDeletedAndClientId(anyList(), anyBoolean(), anyLong())).thenReturn(Collections.singletonList(user2));
        when(userMobileRepository.findAllByUserIdInAndStoppedAtNull(anyList())).thenReturn(Collections.singletonList(
                new UserMobile("9988799887", 321L, true, now, now, false, null)
        ));
        List<UserResponse> repsonse = userService.getUserRelationsPrimaryPhone("9988799887", 123L);
        Assert.assertEquals(1, repsonse.size());
    }
    @Test
    public void searchTestByuserMobiles() throws Exception {
        FilterUserRequest filterUserRequest = new FilterUserRequest("abc@sbx","42-7175-8604-4553","9865763451","test","Male","1999");


        Date now = new Date();
        List<UserMobile> userMobiles = Arrays.asList(
                new UserMobile("9988799887", 1L, true, now, now, false, null),
                new UserMobile("9988799887", 2L, true, now, now, false, null)
        );

        when(userMobileRepository.findAllByNumber(any())).thenReturn(userMobiles);
        when(userRepository.findAllByIdInAndIsDeletedAndClientId(anyList(), anyBoolean(), anyLong())).thenReturn(Collections.singletonList(getUser()));
        UserResponse user = userService.search(filterUserRequest,1L);

        verify(userMobileRepository, Mockito.times(1)).findAllByNumber(any());
        verify(userRepository, Mockito.times(1)).findAllByIdInAndIsDeletedAndClientId(anyList(),anyBoolean(),any());
    }

    @Test
    public void searchTestByphrAddress() throws Exception {
        FilterUserRequest filterUserRequest = new FilterUserRequest("abc@sbx","42-7175-8604-4553","","test","Male","1999");


        Date now = new Date();
        List<ExternalId> externalIds = Collections.singletonList(new ExternalId(1L, 1L, "HEALTH_ID", "12345678", now));

        when(externalIdRepository.findAllByValueAndType(any(),any())).thenReturn(externalIds);
        when(userRepository.findAllByIdInAndIsDeletedAndClientId(anyList(), anyBoolean(), anyLong())).thenReturn(Collections.singletonList(getUser()));
        UserResponse user = userService.search(filterUserRequest,1L);

        verify(externalIdRepository,Mockito.times(1)).findAllByValueAndType(any(),any());
        verify(userRepository, Mockito.times(1)).findAllByIdInAndIsDeletedAndClientId(anyList(),anyBoolean(),any());
    }

    @Test
    public void searchTestByhealthId() throws Exception {
        FilterUserRequest filterUserRequest = new FilterUserRequest("","42-7175-8604-4553","","test","Male","1999");


        Date now = new Date();
        List<ExternalId> externalIds = Collections.singletonList(new ExternalId(1L, 1L, "HEALTH_ID", "12345678", now));

        when(externalIdRepository.findAllByValueAndType(any(),any())).thenReturn(externalIds);
        when(userRepository.findAllByIdInAndIsDeletedAndClientId(anyList(), anyBoolean(), anyLong())).thenReturn(Collections.singletonList(getUser()));
        UserResponse user = userService.search(filterUserRequest,1L);

        verify(externalIdRepository,Mockito.times(1)).findAllByValueAndType(any(),any());
        verify(userRepository, Mockito.times(1)).findAllByIdInAndIsDeletedAndClientId(anyList(),anyBoolean(),any());
    }

    public User getUser() throws ParseException {
        Date dob = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse("1999-10-07");

        User sampleUser = new User();
        sampleUser.setId(1L);
        sampleUser.setClientId(1L);
        sampleUser.setDeleted(false);
        sampleUser.setDateOfBirth(dob);
        sampleUser.setFirstName("test");
        sampleUser.setLastName("");
        sampleUser.setGender("Male");

        return sampleUser;
    }
}
