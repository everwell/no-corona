package tests.unit.services;

import com.everwell.userservice.models.db.UserAddress;
import com.everwell.userservice.models.dtos.DetailsChangedDto;
import com.everwell.userservice.models.dtos.UserAddressRequest;
import com.everwell.userservice.repositories.UserAddressRepository;
import com.everwell.userservice.services.impl.UserAddressServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

import tests.BaseTest;



public class UserAddressServiceTest  extends BaseTest {

    @Spy
    @InjectMocks
    private UserAddressServiceImpl userAddressService;

    @Mock
    private UserAddressRepository userAddressRepository;


    public UserAddressRequest getUserAddressRequestObject(UserAddress userAddress){
        return new UserAddressRequest(userAddress.getTaluka(), userAddress.getTown(), userAddress.getWard(), userAddress.getLandmark(), userAddress.getArea());
    }

    public UserAddress getUserAddressObject() {
        return new UserAddress(1L, 1L, "taluka1", "town1", "ward1", "landmark1", "area1");
    }

    @Test
    public void testAddUserAddressDetails() {
        Long userId = 1L;
        UserAddress userAddress = getUserAddressObject();
        UserAddressRequest userAddressRequest = getUserAddressRequestObject(userAddress);
        when(userAddressRepository.save(any())).thenReturn(userAddress);
        UserAddress userAddressCreated = userAddressService.addUserAddressDetails(userId, userAddressRequest);
        Assert.assertNotNull(userAddressCreated);
        Assert.assertEquals(userId, userAddressCreated.getUserId());
        verify(userAddressRepository, Mockito.times(1)).save(any());
    }

    @Test
    public void testAddUserAddressDetailsWithNullRequest() {
        Long userId = 1L;
        UserAddress userAddress = userAddressService.addUserAddressDetails(userId, null);
        Assert.assertNull(userAddress.getUserId());
        verify(userAddressRepository, Mockito.times(0)).save(any());
    }

    @Test
    public void testUpdateUserAddressDetailsWithNullRequest() {
        Long userId = 1L;
        DetailsChangedDto<UserAddress> detailsChanged = userAddressService.updateUserAddressDetails(userId, null);
        Assert.assertFalse(detailsChanged.getDetailsChanged());
    }

    @Test
    public void testUpdateUserAddressWithNullObject() {
        Long userId = 1L;
        UserAddress userAddress = getUserAddressObject();
        UserAddressRequest userAddressRequest = getUserAddressRequestObject(userAddress);
        when(userAddressRepository.findByUserId(any())).thenReturn(null);
        DetailsChangedDto<UserAddress> detailsChanged = userAddressService.updateUserAddressDetails(userId, userAddressRequest);
        Assert.assertEquals(false, detailsChanged.getDetailsChanged());
        verify(userAddressRepository, Mockito.times(0)).save(any());
    }

    @Test
    public void testUpdateUserAddressDetailsWithNoChange() {
        Long userId = 1L;
        UserAddress userAddress = getUserAddressObject();
        UserAddressRequest userAddressRequest = new UserAddressRequest();
        when(userAddressRepository.findByUserId(any())).thenReturn(userAddress);
        DetailsChangedDto<UserAddress> detailsChanged = userAddressService.updateUserAddressDetails(userId, userAddressRequest);
        Assert.assertEquals(false, detailsChanged.getDetailsChanged());
        verify(userAddressRepository, Mockito.times(0)).save(any());
    }

    @Test
    public void testUpdateUserAddressDetailsWithChange() {
        Long userId = 1L;
        UserAddress userAddress = getUserAddressObject();
        UserAddressRequest userAddressRequest = getUserAddressRequestObject(userAddress);
        userAddressRequest.setTown("town2");
        when(userAddressRepository.findByUserId(any())).thenReturn(userAddress);
        DetailsChangedDto<UserAddress> detailsChanged = userAddressService.updateUserAddressDetails(userId, userAddressRequest);
        Assert.assertEquals(true, detailsChanged.getDetailsChanged());
        verify(userAddressRepository, Mockito.times(1)).save(any());
    }

    @Test
    public void testUpdateUserAddressDetailsWithEmptyStringCaseOne() {
        Long userId = 1L;
        UserAddress userAddress = getUserAddressObject();
        UserAddressRequest userAddressRequest = getUserAddressRequestObject(userAddress);
        userAddressRequest.setTown("");
        when(userAddressRepository.findByUserId(any())).thenReturn(userAddress);
        DetailsChangedDto<UserAddress> detailsChanged = userAddressService.updateUserAddressDetails(userId, userAddressRequest);
        Assert.assertEquals(true, detailsChanged.getDetailsChanged());
        verify(userAddressRepository, Mockito.times(1)).save(any());
    }

    @Test
    public void testUpdateUserAddressDetailsWithEmptyStringCaseTwo() {
        Long userId = 1L;
        UserAddress userAddress = getUserAddressObject();
        UserAddressRequest userAddressRequest = getUserAddressRequestObject(userAddress);
        userAddress.setTown(null);
        userAddressRequest.setTown("");
        when(userAddressRepository.findByUserId(any())).thenReturn(userAddress);
        DetailsChangedDto<UserAddress> detailsChanged = userAddressService.updateUserAddressDetails(userId, userAddressRequest);
        Assert.assertEquals(false, detailsChanged.getDetailsChanged());
        verify(userAddressRepository, Mockito.times(0)).save(any());
    }

    @Test
    public void testGetUserAddressDetails() {
        Long userId = 1L;
        when(userAddressRepository.findByUserId(userId)).thenReturn(getUserAddressObject());
        UserAddress userAddress = userAddressService.getUserAddressDetails(userId);
        Assert.assertEquals(userId, userAddress.getUserId());
        verify(userAddressRepository, Mockito.times(1)).findByUserId(any());
    }

}
