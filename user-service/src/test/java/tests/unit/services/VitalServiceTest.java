package tests.unit.services;

import com.everwell.userservice.exceptions.NotFoundException;
import com.everwell.userservice.exceptions.ValidationException;
import com.everwell.userservice.models.db.Vital;
import com.everwell.userservice.models.http.requests.VitalBulkRequest;
import com.everwell.userservice.models.http.requests.VitalRequest;
import com.everwell.userservice.models.http.responses.VitalResponse;
import com.everwell.userservice.repositories.VitalRepository;
import com.everwell.userservice.services.impl.VitalServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import tests.BaseTest;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

public class VitalServiceTest extends BaseTest {

    @Spy
    @InjectMocks
    private VitalServiceImpl vitalService;

    @Mock
    private VitalRepository vitalRepository;

    @Test
    public void testGetAllVitalsResponse() {
        List<Vital> vitals = new ArrayList<>();
        vitals.add(new Vital(1L, "vital name", 10f, 10000f, 1f, 10000f, "uomTest", false));
        when(vitalRepository.findAll()).thenReturn(vitals);

        List<VitalResponse> vitalResponseList = vitalService.getAllVitalsResponse();

        Assert.assertEquals(vitalResponseList.size(), vitals.size());
        Mockito.verify(vitalRepository, Mockito.times(1)).findAll();
    }

    @Test
    public void testGetVitalResponseById() {
        Long vitalId = 1L;
        Vital vital = new Vital(vitalId,"vital name", 10f, 10000f, 1f, 10000f, "uomTest", false);
        when(vitalRepository.findById(eq(vitalId))).thenReturn(java.util.Optional.of(vital));

        VitalResponse vitalResponse = vitalService.getVitalResponseById(vitalId);

        Assert.assertEquals(vital.getId(), vitalResponse.getId());
        Assert.assertEquals(vital.getName(), vitalResponse.getName());
        Mockito.verify(vitalRepository, Mockito.times(1)).findById(any());
    }

    @Test(expected = NotFoundException.class)
    public void testGetVitalResponseByIdWithNullVital()
    {
        Long vitalId = 1L;
        when(vitalRepository.findById(eq(vitalId))).thenReturn(Optional.empty());

        vitalService.getVitalResponseById(vitalId);

        Mockito.verify(vitalRepository, Mockito.times(1)).findById(any());
    }

    @Test
    public void testSaveVitals() {
        List<Vital> vitals = new ArrayList<>();
        vitals.add(new Vital(1L, "vital name one", 10f, 10000f, 1f, 10000f, "uomTest", false));
        vitals.add(new Vital(2L, "vital name one", 10f, 10000f, 1f, 10000f, "uomTest", false));
        List<VitalRequest> vitalRequestList = new ArrayList<>();
        vitalRequestList.add(new VitalRequest("vital name two", 10f, 10000f, 1f, 10000f, "uomTest", false));
        vitalRequestList.add(new VitalRequest("vital name one", 10f, 10000f, 1f, 10000f, "uomTest", false));
        VitalBulkRequest vitalBulkRequest = new VitalBulkRequest(vitalRequestList);
        when(vitalRepository.findAll()).thenReturn(vitals);

        List<Vital> vitalsResponse = vitalService.saveVitals(vitalBulkRequest);

        Assert.assertEquals(vitalBulkRequest.getVitalsDetails().size(), vitalsResponse.size());
        Mockito.verify(vitalRepository, Mockito.times(1)).findAll();
        Mockito.verify(vitalRepository, Mockito.times(1)).saveAll(any());
    }

    @Test
    public void testSaveVitalsForSameVitalNameFirstTime() {
        List<VitalRequest> vitalRequestList = new ArrayList<>();
        vitalRequestList.add(new VitalRequest("vital name one", 10f, 10000f, 1f, 10000f, "uomTest", false));
        vitalRequestList.add(new VitalRequest("vital name one", 12f, 10000f, 1f, 10000f, "uomTest", false));
        VitalBulkRequest vitalBulkRequest = new VitalBulkRequest(vitalRequestList);
        when(vitalRepository.findAll()).thenReturn(new ArrayList<>());

        List<Vital> vitalsResponse = vitalService.saveVitals(vitalBulkRequest);

        Assert.assertEquals(1L, vitalsResponse.size());
        Assert.assertEquals("10.0", vitalsResponse.get(0).getLowValue().toString());
        Mockito.verify(vitalRepository, Mockito.times(1)).findAll();
        Mockito.verify(vitalRepository, Mockito.times(1)).saveAll(any());
    }

    @Test(expected = NotFoundException.class)
    public void testUpdateVitalsByIdForNonExistingId() {
        Long vitalId = 1L;
        VitalRequest vitalRequest = new VitalRequest("vital name two", 10f, 10000f, 1f, 10000f, "uomTest", false);
        when(vitalRepository.findById(eq(vitalId))).thenReturn(Optional.empty());

        VitalResponse vitalResponse = vitalService.updateVitalsById(vitalId, vitalRequest);

        Assert.assertEquals(vitalRequest.getName(), vitalResponse.getName());
        Mockito.verify(vitalRepository, Mockito.times(0)).save(any());
        Mockito.verify(vitalRepository, Mockito.times(1)).findById(any());
    }

    @Test
    public void testUpdateVitalsByIdForExistingId() {
        Long vitalId = 1L;
        VitalRequest vitalRequest = new VitalRequest("vital name two", 10f, 10000f, 1f, 10000f, "uomTest", false);
        Vital vital = new Vital(vitalId,"vital name", 10f, 10000f, 1f, 10000f, "uomTest", false);
        when(vitalRepository.findById(eq(vitalId))).thenReturn(java.util.Optional.of(vital));

        VitalResponse vitalResponse = vitalService.updateVitalsById(vitalId, vitalRequest);

        Assert.assertEquals(vitalRequest.getName(), vitalResponse.getName());
        Mockito.verify(vitalRepository, Mockito.times(1)).save(any());
        Mockito.verify(vitalRepository, Mockito.times(1)).findById(any());
    }

    @Test
    public void testDeleteVitalsById() {
        Long vitalId = 1L;
        Vital vital = new Vital(1L,"vital name", 10f, 10000f, 1f, 10000f, "uomTest", false);
        when(vitalRepository.findById(eq(vitalId))).thenReturn(java.util.Optional.of(vital));

        vitalService.deleteVitalsById(vitalId);

        Mockito.verify(vitalRepository, Mockito.times(1)).deleteById(any());
        Mockito.verify(vitalRepository, Mockito.times(1)).findById(any());
    }

    @Test(expected = NotFoundException.class)
    public void testDeleteVitalsByIdForInvalidId() {
        Long vitalId = 1L;
        when(vitalRepository.findById(eq(vitalId))).thenReturn(Optional.empty());

        vitalService.deleteVitalsById(vitalId);

        Mockito.verify(vitalRepository, Mockito.times(0)).deleteById(any());
        Mockito.verify(vitalRepository, Mockito.times(1)).findById(any());
    }

    @Test
    public void testProcessAndFetchVitalIdWithValidVitalId() {
        List<Vital> vitals = new ArrayList<>();
        Long vitalId = 1L;
        vitals.add(new Vital(vitalId, "vital name one", 10f, 10000f, 1f, 10000f, "uomTest", false));
        when(vitalRepository.findAll()).thenReturn(vitals);

        Long processedVitalId = vitalService.processAndFetchVitalId(vitalId, null);

        Assert.assertEquals(vitalId, processedVitalId);
        Mockito.verify(vitalRepository, Mockito.times(1)).findAll();
    }

    @Test(expected = ValidationException.class)
    public void testProcessAndFetchVitalIdWithInValidVitalId() {
        List<Vital> vitals = new ArrayList<>();
        Long vitalId = 2L;
        vitals.add(new Vital(1L, "vital name one", 10f, 10000f, 1f, 10000f, "uomTest", false));
        when(vitalRepository.findAll()).thenReturn(vitals);

        vitalService.processAndFetchVitalId(vitalId, null);

        Mockito.verify(vitalRepository, Mockito.times(1)).findAll();
    }

    @Test
    public void testProcessAndFetchVitalIdWithValidVitalName() {
        List<Vital> vitals = new ArrayList<>();
        Long vitalId = 1L;
        String vitalName = "vital name one";
        vitals.add(new Vital(vitalId, vitalName, 10f, 10000f, 1f, 10000f, "uomTest", false));
        when(vitalRepository.findAll()).thenReturn(vitals);

        Long processedVitalId = vitalService.processAndFetchVitalId(null, vitalName);

        Assert.assertEquals(vitalId, processedVitalId);
        Mockito.verify(vitalRepository, Mockito.times(1)).findAll();
    }

    @Test(expected = ValidationException.class)
    public void testProcessAndFetchVitalIdWithInValidVitalName() {
        List<Vital> vitals = new ArrayList<>();
        String vitalName = "vital name one";
        vitals.add(new Vital(1L, "vital name two", 10f, 10000f, 1f, 10000f, "uomTest", false));
        when(vitalRepository.findAll()).thenReturn(vitals);

        vitalService.processAndFetchVitalId(null, vitalName);

        Mockito.verify(vitalRepository, Mockito.times(1)).findAll();
    }

    @Test(expected = ValidationException.class)
    public void testProcessAndFetchVitalIdWithInValidVitalIdAndVitalName() {
        List<Vital> vitals = new ArrayList<>();
        vitals.add(new Vital(1L, "vital name one", 10f, 10000f, 1f, 10000f, "uomTest", false));
        when(vitalRepository.findAll()).thenReturn(vitals);

        vitalService.processAndFetchVitalId(null, null);
        Mockito.verify(vitalRepository, Mockito.times(1)).findAll();
    }

    @Test
    public void getAllVitals() {
        List<Vital> vitals = new ArrayList<>();
        vitals.add(new Vital(1L, "water", 10f, 10000f, 1f, 10000f, "uomTest", false));
        when(vitalRepository.findAll()).thenReturn(vitals);
        List<Vital> vitalsResult = vitalService.getAllVitals();

        Assert.assertEquals(vitals.size(), vitalsResult.size());
        Assert.assertEquals(vitals.get(0).getName(), vitalsResult.get(0).getName());
        Mockito.verify(vitalRepository, Mockito.times(1)).findAll();
    }
}
