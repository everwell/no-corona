package tests.unit.services;

import com.everwell.userservice.models.db.UserContactPerson;
import com.everwell.userservice.models.dtos.DetailsChangedDto;
import com.everwell.userservice.models.dtos.UserContactPersonRequest;
import com.everwell.userservice.repositories.UserContactPersonRepository;
import com.everwell.userservice.services.impl.UserContactPersonServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import tests.BaseTest;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class UserContactPersonServiceTest  extends BaseTest {

    @Spy
    @InjectMocks
    private UserContactPersonServiceImpl userContactPersonService;

    @Mock
    private UserContactPersonRepository userContactPersonRepository;


    public UserContactPersonRequest getUserContactPersonRequestObject(UserContactPerson userContactPerson){
        return new UserContactPersonRequest(userContactPerson.getAddress(), userContactPerson.getName(), userContactPerson.getPhone());
    }

    public UserContactPerson getUserContactPersonObject() {
        return new UserContactPerson(1L, 1L, "address1", "name1", "12345");
    }

    @Test
    public void testAddUserContactPersonDetails() {
        Long userId = 1L;
        UserContactPerson userContactPerson = getUserContactPersonObject();
        UserContactPersonRequest userContactPersonRequest = getUserContactPersonRequestObject(userContactPerson);
        when(userContactPersonRepository.save(any())).thenReturn(getUserContactPersonObject());
        UserContactPerson userContactPersonCreated = userContactPersonService.addUserContactPersonDetails(userId, userContactPersonRequest);
        Assert.assertNotNull(userContactPersonCreated);
        Assert.assertEquals(userId, userContactPersonCreated.getUserId());
        verify(userContactPersonRepository, Mockito.times(1)).save(any());
    }

    @Test
    public void testAddUserContactPersonDetailsWithNullRequest() {
        Long userId = 1L;
        UserContactPerson userContactPerson = userContactPersonService.addUserContactPersonDetails(userId, null);
        Assert.assertNull(userContactPerson.getUserId());
        verify(userContactPersonRepository, Mockito.times(0)).save(any());
    }

    @Test
    public void testAddYpdateContactPersonDetailsWithNullRequest() {
        Long userId = 1L;
        DetailsChangedDto<UserContactPerson> detailsChanged = userContactPersonService.updateUserContactPersonDetails(userId, null);
        Assert.assertFalse(detailsChanged.getDetailsChanged());
    }

    @Test
    public void testUpdateUserContactPersonWithNullObject() {
        Long userId = 1L;
        UserContactPerson userContactPerson = getUserContactPersonObject();
        UserContactPersonRequest userContactPersonRequest = getUserContactPersonRequestObject(userContactPerson);
        when(userContactPersonRepository.findByUserId(any())).thenReturn(null);
        DetailsChangedDto<UserContactPerson> detailsChanged = userContactPersonService.updateUserContactPersonDetails(userId, userContactPersonRequest);
        Assert.assertEquals(false, detailsChanged.getDetailsChanged());
        verify(userContactPersonRepository, Mockito.times(0)).save(any());
    }

    @Test
    public void testUpdateUserContactPersonDetailsWithNoChange() {
        Long userId = 1L;
        UserContactPerson userContactPerson = getUserContactPersonObject();
        when(userContactPersonRepository.findByUserId(any())).thenReturn(userContactPerson);
        DetailsChangedDto<UserContactPerson> detailsChanged = userContactPersonService.updateUserContactPersonDetails(userId, getUserContactPersonRequestObject(userContactPerson));
        Assert.assertEquals(false, detailsChanged.getDetailsChanged());
        verify(userContactPersonRepository, Mockito.times(0)).save(any());
    }

    @Test
    public void testUpdateUserContactPersonDetailsWithChanged() {
        Long userId = 1L;
        UserContactPerson userContactPerson = getUserContactPersonObject();
        UserContactPersonRequest userContactPersonRequest = getUserContactPersonRequestObject(userContactPerson);
        userContactPersonRequest.setContactPersonName("Updated Name");
        when(userContactPersonRepository.findByUserId(any())).thenReturn(userContactPerson);
        DetailsChangedDto<UserContactPerson> detailsChanged = userContactPersonService.updateUserContactPersonDetails(userId, userContactPersonRequest);
        Assert.assertEquals(true, detailsChanged.getDetailsChanged());
        verify(userContactPersonRepository, Mockito.times(1)).save(any());
    }

    @Test
    public void testUpdateUserContactPersonDetailsWithEmptyStringCaseOne() {
        Long userId = 1L;
        UserContactPerson userContactPerson = getUserContactPersonObject();
        UserContactPersonRequest userContactPersonRequest = getUserContactPersonRequestObject(userContactPerson);
        userContactPersonRequest.setContactPersonName("");
        when(userContactPersonRepository.findByUserId(any())).thenReturn(userContactPerson);
        DetailsChangedDto<UserContactPerson> detailsChanged = userContactPersonService.updateUserContactPersonDetails(userId, userContactPersonRequest);
        Assert.assertEquals(true, detailsChanged.getDetailsChanged());
        verify(userContactPersonRepository, Mockito.times(1)).save(any());
    }

    @Test
    public void testUpdateUserContactPersonDetailsWithEmptyStringCaseTwo() {
        Long userId = 1L;
        UserContactPerson userContactPerson = getUserContactPersonObject();
        UserContactPersonRequest userContactPersonRequest = getUserContactPersonRequestObject(userContactPerson);
        userContactPerson.setName(null);
        userContactPersonRequest.setContactPersonName("");
        when(userContactPersonRepository.findByUserId(any())).thenReturn(userContactPerson);
        DetailsChangedDto<UserContactPerson> detailsChanged = userContactPersonService.updateUserContactPersonDetails(userId, userContactPersonRequest);
        Assert.assertEquals(false, detailsChanged.getDetailsChanged());
        verify(userContactPersonRepository, Mockito.times(0)).save(any());
    }

    @Test
    public void testGetUserContactPersonDetails() {
        Long userId = 1L;
        when(userContactPersonRepository.findByUserId(userId)).thenReturn(getUserContactPersonObject());
        UserContactPerson userContactPerson = userContactPersonService.getUserContactPersonDetails(userId);
        Assert.assertEquals(userId, userContactPerson.getUserId());
        verify(userContactPersonRepository, Mockito.times(1)).findByUserId(any());
    }
}
