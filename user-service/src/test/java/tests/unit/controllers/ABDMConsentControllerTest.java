package tests.unit.controllers;

import com.everwell.userservice.controllers.ABDMConsentController;
import com.everwell.userservice.exceptions.CustomExceptionHandler;
import com.everwell.userservice.models.dtos.abdm.ABDMConsentResponse;
import com.everwell.userservice.models.dtos.abdm.AddConsentRequest;
import com.everwell.userservice.services.ABDMService;
import com.everwell.userservice.utils.Utils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import tests.BaseTestNoSpring;

import java.util.Date;

import static com.everwell.userservice.constants.Constants.X_US_CLIENT_ID;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class ABDMConsentControllerTest extends BaseTestNoSpring  {

    @InjectMocks
    private ABDMConsentController abdmConsentController;

    @Mock
    private ABDMService abdmService;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(abdmConsentController)
                .setControllerAdvice(new CustomExceptionHandler())
                .build();
    }

    @Test
    public void testGetConsentResponseById() throws Exception
    {
        String consentId = "1bcd-1egh";
        ABDMConsentResponse abdmConsentResponse = new ABDMConsentResponse();
        when(abdmService.getByConsentId(consentId)).thenReturn(abdmConsentResponse);
        String uri = "/v1/abdm-consent/" + consentId;
        mockMvc.perform(
                MockMvcRequestBuilders.get(uri)
                        .header(X_US_CLIENT_ID,Long.toString(1L))
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());
    }

    @Test
    public void testAddConsentRequest() throws Exception
    {
        AddConsentRequest addConsentRequest = new AddConsentRequest(1L,null,null);
        String uri = "/v1/abdm-consent";
        mockMvc.perform(
                MockMvcRequestBuilders.post(uri)
                        .header(X_US_CLIENT_ID,Long.toString(1L))
                        .content(Utils.asJsonString(addConsentRequest))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value(true));
    }

    @Test
    public void testAddConsentRequestNoUserId() throws Exception
    {
        AddConsentRequest addConsentRequest = new AddConsentRequest(null,null,null);
        String uri = "/v1/abdm-consent";
        mockMvc.perform(
                MockMvcRequestBuilders.post(uri)
                        .header(X_US_CLIENT_ID,Long.toString(1L))
                        .content(Utils.asJsonString(addConsentRequest))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("user id cannot be null"));
    }

}
