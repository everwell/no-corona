import RadioGroup from '@/app/shared/components/RadioGroup.vue'
import { mount,shallowMount,createLocalVue } from '@vue/test-utils'
import FormStore from '@/app/shared/store/modules/form'
import flushPromises from 'flush-promises'
import Vue from 'vue'
import Form from '@/app/shared/components/Form.vue'
import Vuex from 'vuex'

const localVue = createLocalVue()

localVue.use(Vuex)
window.EventBus = new Vue()
const getStore = () => {
  return new Vuex.Store({
  })
}

const getStoreWithActions = (actions) => {
  const formattedActions = {
    ...FormStore.actions,
    ...actions
  }

  return new Vuex.Store({
    modules: {
      Form: {
        state: FormStore.state,
        actions: formattedActions,
        mutations: FormStore.mutations,
        namespaced: true
      }
    }
  })
}

const formWithOneSelect = {
  "Success": true,
  "Data": {
      "Form": {
          "Name": "SingleInputForm1",
          "Title": "_dummy_input_form",
          "Parts": [
              {
                  "FormName": "SingleInputForm1",
                  "Name": "input_form_01",
                  "Title": "",
                  "TitleKey": "input_form_01",
                  "Order": 1,
                  "IsVisible": true,
                  "Id": 119,
                  "Type": "vertical-form-part",
                  "Rows": null,
                  "Columns": null,
                  "RowDataName": "input_form_01",
                  "AllowRowOpen": false,
                  "AllowRowDelete": false,
                  "ListItemTitle": null,
                  "ItemDescriptionField": null,
                  "IsRepeatable": false,
                  "RecordId": null
              }
          ],
          "PartOptions": null,
          "Fields": [
              {
                  "Value": null,
                  "PartName": "input_form_01",
                  "Placeholder": null,
                  "Component": "app-radio",
                  "IsDisabled": false,
                  "IsVisible": true,
                  "LabelKey": "Validate_input__Required_01",
                  "Label": null,
                  "Name": "Validate_input__Required_01",
                  "IsHierarchySelector": false,
                  "IsRequired": false,
                  "RemoteUrl": null,
                  "Type": null,
                  "AddOn": null,
                  "Options": null,
                  "HierarchyOptions": null,
                  "OptionsWithKeyValue": [
                    {
                        "Value": "Val1",
                        "Key": "Val1"
                    },
                    {
                        "Value": "Val2",
                        "Key": "Val2"
                    }
                ],
                  "AdditionalInfo": null,
                  "DefaultVisibilty": false,
                  "Order": 0,
                  "OptionsWithLabel": null,
                  "Validations": {
                      "Or": null,
                      "And": [
                      ]
                  },
                  "ResponseDataPath": null,
                  "OptionDisplayKeys": null,
                  "OptionValueKey": null,
                  "LoadImmediately": false,
                  "HierarchySelectionConfigs": null,
                  "DisabledDateConfig": null,
                  "RemoteUpdateConfig": null,
                  "RowNumber": 16,
                  "ColumnNumber": null,
                  "Id": 11407,
                  "ParentType": "PART",
                  "ParentId": 119,
                  "FieldOptions": "[{\"Value\" : \"Val1\", \"Key\" : \"Val1\"}, {\"Value\":\"Val2\", \"Key\":\"Val2\"}]",
                  "ValidationList": "Required",
                  "DefaultValue": null,
                  "Key": "input_form_01Validate_input__Required_01",
                  "HasInfo": false,
                  "InfoText": null,
                  "Config": null,
                  "ColumnWidth": 6,
                  "HasToggleButton": false
              }
          ],
          "Triggers": null,
          "TriggerConfigs": null,
          "ValueDependencies": [],
          "FilterDependencies": [],
          "VisibilityDependencies": [],
          "DateConstraintDependencies": [],
          "IsRequiredDependencies": [],
          "RemoteUpdateConfigs": [],
          "ValuePropertyDependencies": [],
          "CompoundValueDependencies": null,
          "SaveEndpoint": "/api/patients",
          "SaveText": null,
          "SaveTextKey": "_submit"
      },
      "ExistingData": {}
  },
  "Error": null
} 

const getNewForm = () => {
  return JSON.parse(JSON.stringify(formWithOneSelect))
}
const dummyValidation = {
  "Type": "Dummy",
  "Max": 0,
  "Min": 0,
  "IsBackendValidation": false,
  "ValidationUrl": null,
  "ShowServerError": false,
  "ErrorTargetField": null,
  "ValidationParams": null,
  "ErrorMessage": "Dummy validation message",
  "ErrorMessageKey": "error_dummy",
  "RequiredOnlyWhen": null,
  "Regex": null
}
const getDummyValidation = () => {
  return [JSON.parse(JSON.stringify(dummyValidation))]
}


describe('RadioGroup.vue', () => {
  it('renders props.label when passed', () => {
    const label = 'new label'
    const wrapper = shallowMount(RadioGroup, {
      store: getStore(),
      localVue,
      propsData: { label },
      mocks: {
        $t: () => {
          return {
            label: label
          }
        }
      }
    })
    expect(wrapper.text()).toMatch(label)
  })
})

describe('RadioGroup.vue', () => {
  it('renders correct number of props.optionsWithLabel when passed', () => {
    const allOptions =  [{ Value: 3, Key: 'Amharic' }, { Value: 4, Key: 'Luganda' }]
    const wrapper = shallowMount(RadioGroup, {
      store: getStore(),
      localVue,
      propsData: { allOptions },
      mocks: {
        $t: (key) => key
      }
    })
    const labels = wrapper.findAll('label')
    expect(labels.length.toString()).toMatch('3')
  })
})
describe('RadioGroup.vue', () => {
  it('correct radio button gets selected when user selects it', async () => {
    const allOptions = [{ Value: 3, Key: 'Amharic' }]
    const wrapper = mount(RadioGroup, {
      store: getStore(),
      localVue,
      propsData: { allOptions },
      mocks: {
        $t: (key) => key
      }
    })
    const radioInput = wrapper.find('input[type="radio"]')
    radioInput.element.checked = true
    expect(radioInput.element.checked).toBeTruthy()
  })
})
describe('RadioGroup.vue', () => {
  it('correct radio button value gets changed when user change it', async () => {
    const allOptions = [{ Value: 3, Key: 'Amharic' }, { Value: 2, Key: 'English' }]
    const wrapper = mount(RadioGroup, {
      store: getStore(),
      localVue,
      propsData: { allOptions },
      mocks: {
        $t: (key) => key
      }
    })
    await wrapper.findAll('.c-radio-button').at(1).trigger('click')
    expect(wrapper.findAll('input[type="radio"]').at(1).element.checked).toBe(true)
    await wrapper.findAll('.c-radio-button').at(0).trigger('click')
    expect(wrapper.findAll('input[type="radio"]').at(1).element.checked).toBe(false)
  })
})

describe('RadioGroup.vue', () => {
  it('check if validation is invoked on value change and error message displayed', async () => {
    const label = 'new_label'
    const formData = getNewForm()
    const dummyValidation = getDummyValidation()
    formData['Data']['Form']['Fields'][0]['Label'] = label
    formData['Data']['Form']['Fields'][0]['Validations']['And'] = dummyValidation
    const negativeDummy = (_) => false
    const wrapper = mount(Form, {
      store: getStoreWithActions({
        getFormData: async () => (
          Promise.resolve(
            formData
          )
        )
      }),
      localVue,
      propsData: { isEditing: true, name: 'DummyForm' },
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises() 
    await wrapper.findComponent(RadioGroup).setData({validationMapper: {Dummy:negativeDummy}})
    await wrapper.findComponent(RadioGroup).findAll('input[type="radio"]').at(0).setChecked()
    wrapper.findComponent(RadioGroup).vm.$emit('input','a')
    await wrapper.vm.$nextTick()
    expect(wrapper.findComponent(RadioGroup).find('.error-message').text()).toMatch(dummyValidation[0]['ErrorMessage'])
  })
})

describe('RadioGroup.vue', () => {
  it('check if validation is invoked on value change and error message not displayed if validation return true', async () => {
    const label = 'new_label'
    const formData = getNewForm()
    const dummyValidation = getDummyValidation()
    formData['Data']['Form']['Fields'][0]['Label'] = label
    formData['Data']['Form']['Fields'][0]['Validations']['And'] = dummyValidation
    const postiveDummy = (_) => true
    const wrapper = mount(Form, {
      store: getStoreWithActions ({
        getFormData: async () => (
          Promise.resolve(
            formData
          )
        )
      }),
      localVue,
      propsData: { isEditing: false, name: 'DummyForm' },
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises() 
    await wrapper.findComponent(RadioGroup).setData({validationMapper: {Dummy:postiveDummy}})
    await wrapper.findComponent(RadioGroup).findAll('input[type="radio"]').at(0).setChecked()
    wrapper.findComponent(RadioGroup).vm.$emit('input','a')
    await wrapper.vm.$nextTick()
    expect(wrapper.findComponent(RadioGroup).find('.error-message').exists()).toBeFalsy()
  })
})
