import { createLocalVue, mount } from '@vue/test-utils'
import TableFormPart from '@/app/shared/components/TableFormPart'
import Vuex from 'vuex'
import FormStore from '@/app/shared/store/modules/form'
import flushPromises from 'flush-promises'
import Vue from 'vue'
import { List } from 'immutable'
import { createMapOf, createMapListOf } from '@/app/shared/utils/Objects'

const formWithOneInput = {
  Success: true,
  Data: {
    Form: {
      Name: 'SingleInputForm1',
      Title: '_dummy_input_form',
      Parts: [
        {
          FormName: 'SingleInputForm1',
          Name: 'input_form_01',
          Title: '',
          TitleKey: 'input_form_01',
          Order: 1,
          IsVisible: true,
          Id: 119,
          Type: 'table-form-part',
          Rows: null,
          Columns: null,
          RowDataName: 'input_form_01',
          AllowRowOpen: false,
          AllowRowDelete: false,
          ListItemTitle: null,
          ItemDescriptionField: null,
          IsRepeatable: false,
          RecordId: null
        }
      ],
      PartOptions: null,
      Fields: [
        {
          Value: null,
          PartName: 'input_form_01',
          Placeholder: null,
          Component: 'app-select',
          IsDisabled: false,
          IsVisible: true,
          LabelKey: '_label',
          Label: 'Label',
          Name: 'Label0',
          IsHierarchySelector: false,
          IsRequired: false,
          RemoteUrl: null,
          Type: null,
          AddOn: null,
          Options: null,
          HierarchyOptions: null,
          OptionsWithLabel: null,
          AdditionalInfo: null,
          DefaultVisibilty: false,
          Order: 0,
          OptionsWithKeyValue: [
            {
              value: '1',
              id: '1'
            },
            {
              value: '2',
              id: '2'
            },
            {
              value: '3',
              id: '3'
            }
          ],
          Validations: {
            Or: null,
            And: [
              {
                Type: 'Required',
                Max: 0,
                Min: 0,
                IsBackendValidation: false,
                ValidationUrl: null,
                ShowServerError: false,
                ErrorTargetField: null,
                ValidationParams: null,
                ErrorMessage: 'This field is required',
                ErrorMessageKey: 'error_field_required',
                RequiredOnlyWhen: null,
                Regex: null
              }
            ]
          },
          ResponseDataPath: null,
          OptionDisplayKeys: null,
          OptionValueKey: null,
          LoadImmediately: false,
          HierarchySelectionConfigs: null,
          DisabledDateConfig: null,
          RemoteUpdateConfig: null,
          RowNumber: null,
          ColumnNumber: 1,
          Id: 11114,
          ParentType: 'PART',
          ParentId: 57,
          FieldOptions: null,
          ValidationList: 'OnlyAlphabetAndSpaceAllowed',
          DefaultValue: '',
          Key: 'input_form_01Label0',
          HasInfo: false,
          InfoText: null,
          Config: null,
          ColumnWidth: 1,
          RowWidth: 1,
          HasToggleButton: false
        },
        {
          Value: null,
          PartName: 'input_form_01',
          Placeholder: null,
          Component: 'app-select',
          IsDisabled: true,
          IsVisible: true,
          LabelKey: '_label',
          Label: 'Label',
          Name: 'Label1',
          IsHierarchySelector: false,
          IsRequired: false,
          RemoteUrl: null,
          Type: null,
          AddOn: null,
          Options: null,
          HierarchyOptions: null,
          OptionsWithKeyValue: [
            {
              value: '1',
              id: '1'
            },
            {
              value: '2',
              id: '2'
            },
            {
              value: '3',
              id: '3'
            }
          ],
          AdditionalInfo: null,
          DefaultVisibilty: false,
          Order: 0,
          OptionsWithLabel: null,
          Validations: {
            Or: null,
            And: [
              {
                Type: 'Required',
                Max: 0,
                Min: 0,
                IsBackendValidation: false,
                ValidationUrl: null,
                ShowServerError: false,
                ErrorTargetField: null,
                ValidationParams: null,
                ErrorMessage: 'This field is required',
                ErrorMessageKey: 'error_field_required',
                RequiredOnlyWhen: null,
                Regex: null
              }
            ]
          },
          ResponseDataPath: null,
          OptionDisplayKeys: null,
          OptionValueKey: null,
          LoadImmediately: false,
          HierarchySelectionConfigs: null,
          DisabledDateConfig: null,
          RemoteUpdateConfig: null,
          RowNumber: null,
          ColumnNumber: 2,
          Id: 11114,
          ParentType: 'PART',
          ParentId: 57,
          FieldOptions: null,
          ValidationList: 'OnlyAlphabetAndSpaceAllowed',
          DefaultValue: '',
          Key: 'input_form_01Label1',
          HasInfo: false,
          InfoText: null,
          Config: null,
          ColumnWidth: 1,
          RowWidth: 1,
          HasToggleButton: false
        },
        {
          Value: null,
          PartName: 'ProductList',
          Placeholder: null,
          Component: 'app-select',
          IsDisabled: false,
          IsVisible: true,
          LabelKey: '_label',
          Label: 'Label',
          Name: 'Label2',
          IsHierarchySelector: false,
          IsRequired: true,
          RemoteUrl: null,
          Type: null,
          AddOn: null,
          Options: null,
          HierarchyOptions: null,
          OptionsWithKeyValue: [
            {
              value: '1',
              id: '1'
            },
            {
              value: '2',
              id: '2'
            },
            {
              value: '3',
              id: '3'
            }
          ],
          AdditionalInfo: null,
          DefaultVisibilty: false,
          Order: 0,
          OptionsWithLabel: null,
          Validations: {
            Or: null,
            And: [
              {
                Type: 'Required',
                Max: 0,
                Min: 0,
                IsBackendValidation: false,
                ValidationUrl: null,
                ShowServerError: false,
                ErrorTargetField: null,
                ValidationParams: null,
                ErrorMessage: 'This field is required',
                ErrorMessageKey: 'error_field_required',
                RequiredOnlyWhen: null,
                Regex: null
              }
            ]
          },
          ResponseDataPath: null,
          OptionDisplayKeys: null,
          OptionValueKey: null,
          LoadImmediately: false,
          HierarchySelectionConfigs: null,
          DisabledDateConfig: null,
          RemoteUpdateConfig: null,
          RowNumber: null,
          ColumnNumber: 3,
          Id: 11114,
          ParentType: 'PART',
          ParentId: 57,
          FieldOptions: null,
          ValidationList: 'OnlyAlphabetAndSpaceAllowed',
          DefaultValue: '',
          Key: 'input_form_01Label2',
          HasInfo: false,
          InfoText: null,
          Config: null,
          ColumnWidth: 1,
          RowWidth: 1,
          HasToggleButton: false
        }
      ],
      Triggers: null,
      TriggerConfigs: null,
      ValueDependencies: [],
      FilterDependencies: [],
      VisibilityDependencies: [],
      DateConstraintDependencies: [],
      IsRequiredDependencies: [],
      RemoteUpdateConfigs: [],
      ValuePropertyDependencies: [],
      CompoundValueDependencies: null,
      SaveEndpoint: '/api/patients',
      SaveText: null,
      SaveTextKey: '_submit'
    },
    ExistingData: {}
  },
  Error: null
}

const localVue = createLocalVue()

localVue.use(Vuex)
window.EventBus = new Vue()

jest.mock('../../src/utils/toastUtils', () => {
  return {
    __esModule: true,
    defaultToast: jest.fn(),
    ToastType: {
      Success: 'Success',
      Error: 'Error',
      Warning: 'Warning',
      Neutal: 'Neutal'
    }
  }
})

const getStore = (actions, state) => {
  const formattedActions = {
    ...FormStore.actions,
    ...actions
  }
  const formattedState = {
    ...FormStore.state,
    ...state
  }
  return new Vuex.Store({
    modules: {
      Form: {
        state: formattedState,
        actions: formattedActions,
        mutations: FormStore.mutations,
        namespaced: true
      }
    }
  })
}

const getNewForm = () => {
  return JSON.parse(JSON.stringify(formWithOneInput))
}

const tableFormPartFieldLabels = [{
  Label0: null,
  Label1: null,
  Label2: 'Test'
}]

const localValue = {
  rows: tableFormPartFieldLabels
}

describe('Table Form Part Tests', () => {
  it('renders props.headingText when passed', async () => {
    const formData = getNewForm()
    const heading = formData.Data.Form.Parts[0].Name
    const wrapper = mount(TableFormPart, {
      store: getStore({}, {
        fieldsMappedByFormPartName: { get: (_) => List(formData.Data.Form.Fields), has: (_) => true },
        allFields: List(formData.Data.Form.Fields)
      }),
      localVue,
      propsData: { headingText: heading, name: 'ProductList', allowRowOpen: true },
      data () {
        return {
          localValue: localValue
        }
      },
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises()
    expect(wrapper.findComponent(TableFormPart).find('#table-heading').text()).toBe(heading)
  })

  it('call open row function', async () => {
    const formData = getNewForm()
    const heading = formData.Data.Form.Parts[0].Name
    const wrapper = mount(TableFormPart, {
      store: getStore({}, {
        fieldsMappedByFormPartName: createMapListOf(
          'PartName', formData.Data.Form.Fields),
        allFields: formData.Data.Form.Fields,
        fieldsMappedByName: createMapOf('Key', formData.Data.Form.Fields),
        formPartsMappedByName: createMapOf('Name', formData.Data.Form.Parts)
      }),
      localVue,
      propsData: { headingText: heading, name: 'input_form_01', allowRowOpen: true },
      data () {
        return {
          localValue: localValue
        }
      },
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises()
    await wrapper.findComponent(TableFormPart).find('#openRow').trigger('click')
  })

  it('call Add row function', async () => {
    const formData = getNewForm()
    const heading = formData.Data.Form.Parts[0].Name
    const wrapper = mount(TableFormPart, {
      store: getStore({}, {
        fieldsMappedByFormPartName: createMapListOf(
          'PartName', formData.Data.Form.Fields),
        allFields: formData.Data.Form.Fields,
        fieldsMappedByName: createMapOf('Key', formData.Data.Form.Fields),
        formPartsMappedByName: createMapOf('Name', formData.Data.Form.Parts)
      }),
      localVue,
      propsData: { headingText: heading, name: 'ProductList', allowRowOpen: true },
      data () {
        return {
          localValue: localValue
        }
      },
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises()
    await wrapper.findComponent(TableFormPart).find('#addRow').trigger('click')
  })

  it('call remove row function', async () => {
    const formData = getNewForm()
    const heading = formData.Data.Form.Parts[0].Name
    const wrapper = mount(TableFormPart, {
      store: getStore({}, {
        fieldsMappedByFormPartName: { get: (_) => List(formData.Data.Form.Fields), has: (_) => true },
        allFields: List(formData.Data.Form.Fields)
      }),
      localVue,
      propsData: { headingText: heading, name: 'ProductList', allowRowOpen: true },
      data () {
        return {
          localValue: localValue
        }
      },
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises()
    await wrapper.findComponent(TableFormPart).find('#removeRow').trigger('click')
  })

  it('call delete row function', async () => {
    const formData = getNewForm()
    const heading = formData.Data.Form.Parts[0].Name
    const wrapper = mount(TableFormPart, {
      store: getStore({}, {
        // fieldsMappedByFormPartName: { get: (_) => List(formData.Data.Form.Fields), has: (_) => true },
        // allFields: List(formData.Data.Form.Fields)
        fieldsMappedByFormPartName: createMapListOf(
          'PartName', formData.Data.Form.Fields),
        allFields: formData.Data.Form.Fields,
        fieldsMappedByName: createMapOf('Key', formData.Data.Form.Fields),
        formPartsMappedByName: createMapOf('Name', formData.Data.Form.Parts)
      }),
      localVue,
      propsData: { headingText: heading, name: 'ProductList', allowRowDelete: true },
      data () {
        return {
          localValue: localValue
        }
      },
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises()
    await wrapper.findComponent(TableFormPart).find('#deleteRow').trigger('click')
  })
  it('emit event test', async () => {
    const formData = getNewForm()
    const heading = formData.Data.Form.Parts[0].Name
    const wrapper = mount(TableFormPart, {
      store: getStore({}, {
        fieldsMappedByFormPartName: { get: (_) => List(formData.Data.Form.Fields), has: (_) => true },
        allFields: List(formData.Data.Form.Fields)
      }),
      localVue,
      propsData: { headingText: heading, name: 'ProductList', allowRowOpen: true },
      data () {
        return {
          localValue: localValue
        }
      },
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises()
    wrapper.vm.$emit('TABLE_ROW_OPENED')
    wrapper.vm.$emit('TABLE_ROW_OPENED', 'name')
    await wrapper.vm.$nextTick()
    expect(wrapper.emitted().TABLE_ROW_OPENED).toBeTruthy()
  })
})
