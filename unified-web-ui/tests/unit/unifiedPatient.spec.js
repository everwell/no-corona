import { mount, createLocalVue } from '@vue/test-utils'
import UnifiedPatient from '../../src/app/Pages/dashboard/UnifiedPatient/UnifiedPatient'
import Select from '@/app/shared/components/Select.vue'
import Vuex from 'vuex'
import { List, Map } from 'immutable'

const localVue = createLocalVue()
localVue.use(Vuex)
const $t = () => {}
const optionsData = {
  HierarchyOptions:
[[{ Id: 33710, Name: 'Uganda', Parent: null, Type: 'COUNTRY' }],
  [{ Key: 33719, Id: 33719, Name: 'betaa', Type: 'DISTRICT', Parent: 33710 },
    { Key: 266713, Id: 266713, Name: 'Bugiri', Type: 'DISTRICT', Parent: 33710 }],
  [{ Key: 266714, Id: 266714, Name: 'Bugiri HOSPITAL', Type: 'FACILITY', Parent: 266713 },
    { Key: 266917, Id: 266917, Name: 'C test', Type: 'FACILITY', Parent: 33719 },
    { Key: 266918, Id: 266918, Name: 'dd', Type: 'FACILITY', Parent: 33719 },
    { Key: 33720, Id: 33720, Name: 'Facility C1', Type: 'FACILITY', Parent: 33719 }],
  [{ Key: 33722, Id: 33722, Name: 'Test', Type: 'TU', Parent: 266917 }],
  [{ Key: 33725, Id: 33725, Name: 'Test PHI', Type: 'PHI', Parent: 33722 }]],
  StageOptions: [{ value: 'ON_TREATMENT', label: 'on_treatment', hasCalendar: 'True' },
    { value: 'OFF_TREATMENT', label: 'outcome_assigned', hasCalendar: 'False' }],
  TechnologyOptions: [{ Key: '99DOTS', Value: '_99dots', AdherenceTechOptions: '99DOTS' }],
  EarliestEnrollment: '2019-05-01T00:00:00'
}

const patientData = {
  TotalPatients: 1,
  EarliestEnrollment: '2019-05-01T00:00:00',
  Columns: {
    Id: 'patient_id_web',
    PatientName: 'patient_name',
    MonitoringMethod: 'adherence_technology',
    DigitalDoses: 'digitally_confirmed_adherence',
    DigitalManual: 'digital_manual_adherence',
    Phone: 'primary_phone_number'
  }
}

describe('Unified Patient page Tests', () => {
  let store
  let actions
  beforeEach(() => {
    actions = {
      loadOptions: jest.fn(),
      setCalendarView: jest.fn(),
      updateClientName: jest.fn(),
      getPatients: jest.fn(),
      setSelectedFilters: jest.fn(),
      setStage: jest.fn(),
      loadHierarchyLevelOptions: jest.fn(),
      setCurrentPage: jest.fn()
    }
    store = new Vuex.Store({
      modules: {
        UnifiedPatient: {
          namespaced: true,
          state: {
            client: null,
            deploymentCode: 'DEFAULT',
            isOptionsLoading: true,
            isPatientsLoading: false,
            isCalendar: false,
            firstHierarchies: [],
            secondHierarchies: [],
            thirdHierarchies: [],
            fourthHierarchies: [],
            fifthHierarchies: [],
            PHIPatientTypeOptions: List(),
            PHIPatientType: '',
            stageOptions: List(),
            patientTypes: List(),
            caseTypes: List(),
            techOptions: List(),
            dateOptions: List(),
            patients: Map(),
            columns: List(),
            calendarModel: Map(),
            pageSize: 20,
            selectedPage: 1,
            stage: '',
            numberPages: 0,
            totalPatients: 0,
            currentMonth: new Date().getMonth(),
            currentYear: new Date().getFullYear(),
            filters: null,
            earliestEnrollment: new Date('2017-05-10')
          },
          actions
        }
      }
    })
  })

  it('calls loadOptions on load', () => {
    process.env.VUE_APP_THEME = 'hub'
    const wrapper = mount(UnifiedPatient, { store, localVue, mocks: { $t } })
    wrapper.vm.$nextTick()
    expect(actions.loadOptions.mock.calls).toHaveLength(1)
    expect(actions.updateClientName.mock.calls).toHaveLength(1)
  })

  it('HUB filters load', () => {
    store.state.UnifiedPatient.client = 'NNDOTS'
    store.state.UnifiedPatient.isOptionsLoading = false
    store.state.UnifiedPatient.stage = ''
    const wrapper = mount(UnifiedPatient, { store, localVue, mocks: { $t } })
    expect(wrapper.find('.filter-container').exists()).toBe(true)
  })

  it('Filters with Patient List load', () => {
    store.state.UnifiedPatient.client = 'NNDOTS'
    store.state.UnifiedPatient.isOptionsLoading = false
    store.state.UnifiedPatient.stage = ''
    store.state.UnifiedPatient.isCalendar = false
    store.state.UnifiedPatient.patients = [{ Id: 314390, PatientName: 'sadawd ', TBNumber: '4353', Facility: 'DEMO_TU_2', MonitoringMethod: 'EvriMED(MERM)', Phone: '3232424234', EnrollmentDate: '04-01-2022', TBTreatmentStartDate: '04-01-2022', EndDate: '21-06-2022', TotalTreatmentDays: 168, MissedDoses: 16, DigitalDoses: '0%', DigitalManual: '0%', LastDosage: '04-01-2022', CurrentTags: '', TreatmentOutcome: null, AdherenceString: '6666666666666666', C_PatientTag: '', DeviceIMEI: '862927043270280', BatteryLevel: '0 %', AdherenceResults: [{ Item1: 'blank ', Item2: 'Saturday January 01-01-2022 00:00:00, 2022: No tags added' }, { Item1: 'blank ', Item2: 'Sunday January 02-01-2022 00:00:00, 2022: No tags added' }, { Item1: 'blank ', Item2: 'Monday January 03-01-2022 00:00:00, 2022: No tags added' }, { Item1: 'no_info ', Item2: 'Tuesday January 04-01-2022 00:00:00, 2022: No tags added' }, { Item1: 'no_info ', Item2: 'Wednesday January 05-01-2022 00:00:00, 2022: No tags added' }, { Item1: 'no_info ', Item2: 'Thursday January 06-01-2022 00:00:00, 2022: No tags added' }, { Item1: 'no_info ', Item2: 'Friday January 07-01-2022 00:00:00, 2022: No tags added' }, { Item1: 'no_info ', Item2: 'Saturday January 08-01-2022 00:00:00, 2022: No tags added' }, { Item1: 'no_info ', Item2: 'Sunday January 09-01-2022 00:00:00, 2022: No tags added' }, { Item1: 'no_info ', Item2: 'Monday January 10-01-2022 00:00:00, 2022: No tags added' }, { Item1: 'no_info ', Item2: 'Tuesday January 11-01-2022 00:00:00, 2022: No tags added' }, { Item1: 'no_info ', Item2: 'Wednesday January 12-01-2022 00:00:00, 2022: No tags added' }, { Item1: 'no_info ', Item2: 'Thursday January 13-01-2022 00:00:00, 2022: No tags added' }, { Item1: 'no_info ', Item2: 'Friday January 14-01-2022 00:00:00, 2022: No tags added' }, { Item1: 'no_info ', Item2: 'Saturday January 15-01-2022 00:00:00, 2022: No tags added' }, { Item1: 'no_info ', Item2: 'Sunday January 16-01-2022 00:00:00, 2022: No tags added' }, { Item1: 'no_info ', Item2: 'Monday January 17-01-2022 00:00:00, 2022: No tags added' }, { Item1: 'no_info ', Item2: 'Tuesday January 18-01-2022 00:00:00, 2022: No tags added' }, { Item1: 'no_info ', Item2: 'Wednesday January 19-01-2022 00:00:00, 2022: No tags added' }] }]
    const wrapper = mount(UnifiedPatient, { store, localVue, mocks: { $t } })
    expect(wrapper.find('.data-container').exists()).toBe(true)
  })

  it('Hierarchies Loading', async () => {
    store.state.UnifiedPatient.client = 'NNDOTS'
    store.state.UnifiedPatient.isOptionsLoading = false
    store.state.UnifiedPatient.stage = { Key: 'ON_TREATMENT', Value: 'on_treatment' }
    store.state.UnifiedPatient.isCalendar = false
    store.state.UnifiedPatient.firstHierarchies = optionsData.HierarchyOptions[0]
    store.state.UnifiedPatient.secondHierarchies = optionsData.HierarchyOptions[1]
    store.state.UnifiedPatient.thirdHierarchies = optionsData.HierarchyOptions[2]
    store.state.UnifiedPatient.fourthHierarchies = optionsData.HierarchyOptions[3]
    store.state.UnifiedPatient.fifthHierarchies = optionsData.HierarchyOptions[4]
    store.state.UnifiedPatient.stageOptions = optionsData.StageOptions
    store.state.UnifiedPatient.TechnologyOptions = optionsData.TechnologyOptions
    store.state.UnifiedPatient.EarliestEnrollment = new Date(optionsData.EarliestEnrollment)
    store.state.UnifiedPatient.totalPatients = 1
    store.state.UnifiedPatient.patients = [{ Id: 314390, PatientName: 'sadawd ', TBNumber: '4353', Facility: 'DEMO_TU_2', MonitoringMethod: 'EvriMED(MERM)', Phone: '3232424234', EnrollmentDate: '04-01-2022', TBTreatmentStartDate: '04-01-2022', EndDate: '21-06-2022', TotalTreatmentDays: 168, MissedDoses: 16, DigitalDoses: '0%', DigitalManual: '0%', LastDosage: '04-01-2022', CurrentTags: '', TreatmentOutcome: null, AdherenceString: '6666666666666666', C_PatientTag: '', DeviceIMEI: '862927043270280', BatteryLevel: '0 %', AdherenceResults: [{ Item1: 'blank ', Item2: 'Saturday January 01-01-2022 00:00:00, 2022: No tags added' }, { Item1: 'blank ', Item2: 'Sunday January 02-01-2022 00:00:00, 2022: No tags added' }, { Item1: 'blank ', Item2: 'Monday January 03-01-2022 00:00:00, 2022: No tags added' }, { Item1: 'no_info ', Item2: 'Tuesday January 04-01-2022 00:00:00, 2022: No tags added' }, { Item1: 'no_info ', Item2: 'Wednesday January 05-01-2022 00:00:00, 2022: No tags added' }, { Item1: 'no_info ', Item2: 'Thursday January 06-01-2022 00:00:00, 2022: No tags added' }, { Item1: 'no_info ', Item2: 'Friday January 07-01-2022 00:00:00, 2022: No tags added' }, { Item1: 'no_info ', Item2: 'Saturday January 08-01-2022 00:00:00, 2022: No tags added' }, { Item1: 'no_info ', Item2: 'Sunday January 09-01-2022 00:00:00, 2022: No tags added' }, { Item1: 'no_info ', Item2: 'Monday January 10-01-2022 00:00:00, 2022: No tags added' }, { Item1: 'no_info ', Item2: 'Tuesday January 11-01-2022 00:00:00, 2022: No tags added' }, { Item1: 'no_info ', Item2: 'Wednesday January 12-01-2022 00:00:00, 2022: No tags added' }, { Item1: 'no_info ', Item2: 'Thursday January 13-01-2022 00:00:00, 2022: No tags added' }, { Item1: 'no_info ', Item2: 'Friday January 14-01-2022 00:00:00, 2022: No tags added' }, { Item1: 'no_info ', Item2: 'Saturday January 15-01-2022 00:00:00, 2022: No tags added' }, { Item1: 'no_info ', Item2: 'Sunday January 16-01-2022 00:00:00, 2022: No tags added' }, { Item1: 'no_info ', Item2: 'Monday January 17-01-2022 00:00:00, 2022: No tags added' }, { Item1: 'no_info ', Item2: 'Tuesday January 18-01-2022 00:00:00, 2022: No tags added' }, { Item1: 'no_info ', Item2: 'Wednesday January 19-01-2022 00:00:00, 2022: No tags added' }] }]
    const wrapper = mount(UnifiedPatient, { store, localVue, mocks: { $t: key => key } })
    expect(wrapper.find('.data-container').exists()).toBe(true)
    expect(wrapper.find('.br').exists()).toBe(true)
    await wrapper.findComponent(Select).findAll('.dropdown ul li').at(0).trigger('click')
    expect(wrapper.find('input').element.value).toBe('betaa')
    await wrapper.setData({ secondHierarchy: [{ Id: 33719, Name: 'betaa', Type: 'DISTRICT', Parent: 33710 }] })
    await wrapper.findAllComponents(Select).at(1).findAll('.dropdown ul li').at(0).trigger('click')
    expect(wrapper.findComponent({ name: 'Select Third Hierarchy' }).exists()).toBe(true)
    await wrapper.setData({ thirdHierarchy: [{ Id: 266917, Name: 'C test', Type: 'FACILITY', Parent: 33719 }] })
    await wrapper.findAllComponents(Select).at(2).findAll('.dropdown ul li').at(0).trigger('click')
    expect(wrapper.findComponent({ name: 'Select Fourth Hierarchy' }).exists()).toBe(true)
  })

  it('table load', async () => {
    var columnList = Object.keys(patientData.Columns).map(function (key) {
      return { Key: key, Value: patientData.Columns[key], Sorting: true }
    })
    store.state.UnifiedPatient.client = 'NNDOTS'
    store.state.UnifiedPatient.isOptionsLoading = false
    store.state.UnifiedPatient.stage = { Key: 'ON_TREATMENT', Value: 'on_treatment' }
    store.state.UnifiedPatient.isCalendar = false
    store.state.UnifiedPatient.firstHierarchies = optionsData.HierarchyOptions[0]
    store.state.UnifiedPatient.secondHierarchies = optionsData.HierarchyOptions[1]
    store.state.UnifiedPatient.thirdHierarchies = optionsData.HierarchyOptions[2]
    store.state.UnifiedPatient.fourthHierarchies = optionsData.HierarchyOptions[3]
    store.state.UnifiedPatient.fifthHierarchies = optionsData.HierarchyOptions[4]
    store.state.UnifiedPatient.stageOptions = optionsData.StageOptions
    store.state.UnifiedPatient.TechnologyOptions = optionsData.TechnologyOptions
    store.state.UnifiedPatient.EarliestEnrollment = new Date(optionsData.EarliestEnrollment)
    store.state.UnifiedPatient.columns = columnList
    store.state.UnifiedPatient.isPatientsLoading = false
    store.state.UnifiedPatient.selectedPage = 1
    store.state.UnifiedPatient.totalPatients = optionsData.TotalPatients
    store.state.UnifiedPatient.numberPages = Math.ceil(optionsData.TotalPatients / 20)
    store.state.UnifiedPatient.patients = [{ Id: 314390, PatientName: 'sadawd ', TBNumber: '4353', Facility: 'DEMO_TU_2', MonitoringMethod: 'EvriMED(MERM)', Phone: '3232424234', EnrollmentDate: '04-01-2022', TBTreatmentStartDate: '04-01-2022', EndDate: '21-06-2022', TotalTreatmentDays: 168, MissedDoses: 16, DigitalDoses: '0%', DigitalManual: '0%', LastDosage: '04-01-2022', CurrentTags: '', TreatmentOutcome: null, AdherenceString: '6666666666666666', C_PatientTag: '', DeviceIMEI: '862927043270280', BatteryLevel: '0 %', AdherenceResults: [{ Item1: 'blank ', Item2: 'Saturday January 01-01-2022 00:00:00, 2022: No tags added' }, { Item1: 'blank ', Item2: 'Sunday January 02-01-2022 00:00:00, 2022: No tags added' }, { Item1: 'blank ', Item2: 'Monday January 03-01-2022 00:00:00, 2022: No tags added' }, { Item1: 'no_info ', Item2: 'Tuesday January 04-01-2022 00:00:00, 2022: No tags added' }, { Item1: 'no_info ', Item2: 'Wednesday January 05-01-2022 00:00:00, 2022: No tags added' }, { Item1: 'no_info ', Item2: 'Thursday January 06-01-2022 00:00:00, 2022: No tags added' }, { Item1: 'no_info ', Item2: 'Friday January 07-01-2022 00:00:00, 2022: No tags added' }, { Item1: 'no_info ', Item2: 'Saturday January 08-01-2022 00:00:00, 2022: No tags added' }, { Item1: 'no_info ', Item2: 'Sunday January 09-01-2022 00:00:00, 2022: No tags added' }, { Item1: 'no_info ', Item2: 'Monday January 10-01-2022 00:00:00, 2022: No tags added' }, { Item1: 'no_info ', Item2: 'Tuesday January 11-01-2022 00:00:00, 2022: No tags added' }, { Item1: 'no_info ', Item2: 'Wednesday January 12-01-2022 00:00:00, 2022: No tags added' }, { Item1: 'no_info ', Item2: 'Thursday January 13-01-2022 00:00:00, 2022: No tags added' }, { Item1: 'no_info ', Item2: 'Friday January 14-01-2022 00:00:00, 2022: No tags added' }, { Item1: 'no_info ', Item2: 'Saturday January 15-01-2022 00:00:00, 2022: No tags added' }, { Item1: 'no_info ', Item2: 'Sunday January 16-01-2022 00:00:00, 2022: No tags added' }, { Item1: 'no_info ', Item2: 'Monday January 17-01-2022 00:00:00, 2022: No tags added' }, { Item1: 'no_info ', Item2: 'Tuesday January 18-01-2022 00:00:00, 2022: No tags added' }, { Item1: 'no_info ', Item2: 'Wednesday January 19-01-2022 00:00:00, 2022: No tags added' }] }]

    const wrapper = mount(UnifiedPatient, { store, localVue, mocks: { $t: key => key } })
    expect(wrapper.find('.data-container').exists()).toBe(true)
    expect(wrapper.find('.br').exists()).toBe(true)
    await wrapper.findComponent(Select).findAll('.dropdown ul li').at(0).trigger('click')
    expect(wrapper.find('input').element.value).toBe('betaa')
    var date = new Date()
    date.setMonth(date.getMonth() - 3)
    await wrapper.setData({
      secondHierarchy: [{ Id: 33719, Name: 'betaa', Type: 'DISTRICT', Parent: 33710 }],
      thirdHierarchy: [{ Id: 266917, Name: 'C test', Type: 'FACILITY', Parent: 33719 }],
      fourthSelectedHierarchies: [{ Id: 33722, Name: 'Test', Type: 'TU', Parent: 266917 }],
      fifthSelectedHierarchies: [{ Id: 33725, Name: 'Test PHI', Type: 'PHI', Parent: 33722 }],
      dateStart: date.toISOString().split('T')[0],
      dateEnd: new Date().toISOString().split('T')[0],
      selectedTech: [{ Key: '99DOTS', Value: '_99dots' }]
    })
    await wrapper.findComponent({ name: 'Stage' }).findAll('.dropdown ul li').at(0).trigger('click')
    await wrapper.findAllComponents(Select).at(4).findAll('.dropdown ul li').at(0).trigger('click')
    expect(wrapper.findAll('input').at(13).element.value).toBe('patient_name + 4 more')
    expect(wrapper.find('.table_container').exists()).toBe(true)
  })
})
