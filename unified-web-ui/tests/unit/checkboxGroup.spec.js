import { shallowMount, mount, createLocalVue } from '@vue/test-utils'
import CheckboxGroup from '@/app/shared/components/CheckboxGroup.vue'
import Vue from 'vue'
import Vuex from 'vuex'
import FormStore from '@/app/shared/store/modules/form'
import flushPromises from 'flush-promises'
import Form from '@/app/shared/components/Form.vue'

const localVue = createLocalVue()
localVue.use(Vuex)
const getStore = () => {
  return new Vuex.Store({
  })
}

const getStoreWithActions = (actions) => {
  const formattedActions = {
    ...FormStore.actions,
    ...actions
  }

  return new Vuex.Store({
    modules: {
      Form: {
        state: FormStore.state,
        actions: formattedActions,
        mutations: FormStore.mutations,
        namespaced: true
      }
    }
  })
}

const formWithOneSelect = {
  "Success": true,
  "Data": {
      "Form": {
          "Name": "SingleInputForm1",
          "Title": "_dummy_input_form",
          "Parts": [
              {
                  "FormName": "SingleInputForm1",
                  "Name": "input_form_01",
                  "Title": "",
                  "TitleKey": "input_form_01",
                  "Order": 1,
                  "IsVisible": true,
                  "Id": 119,
                  "Type": "vertical-form-part",
                  "Rows": null,
                  "Columns": null,
                  "RowDataName": "input_form_01",
                  "AllowRowOpen": false,
                  "AllowRowDelete": false,
                  "ListItemTitle": null,
                  "ItemDescriptionField": null,
                  "IsRepeatable": false,
                  "RecordId": null
              }
          ],
          "PartOptions": null,
          "Fields": [
              {
                  "Value": null,
                  "PartName": "input_form_01",
                  "Placeholder": null,
                  "Component": "app-checkbox-group",
                  "IsDisabled": false,
                  "IsVisible": true,
                  "LabelKey": "Validate_input__Required_01",
                  "Label": null,
                  "Name": "Validate_input__Required_01",
                  "IsHierarchySelector": false,
                  "IsRequired": false,
                  "RemoteUrl": null,
                  "Type": null,
                  "AddOn": null,
                  "Options": null,
                  "HierarchyOptions": null,
                  "OptionsWithKeyValue": [{ Key: 3, Value: 'Amharic' }],
                  "AdditionalInfo": null,
                  "DefaultVisibilty": false,
                  "Order": 0,
                  "OptionsWithLabel": null,
                  "Validations": {
                      "Or": null,
                      "And": [
                      ]
                  },
                  "ResponseDataPath": null,
                  "OptionDisplayKeys": null,
                  "OptionValueKey": null,
                  "LoadImmediately": false,
                  "HierarchySelectionConfigs": null,
                  "DisabledDateConfig": null,
                  "RemoteUpdateConfig": null,
                  "RowNumber": 16,
                  "ColumnNumber": null,
                  "Id": 11407,
                  "ParentType": "PART",
                  "ParentId": 119,
                  "FieldOptions": null,
                  "ValidationList": "Required",
                  "DefaultValue": null,
                  "Key": "input_form_01Validate_input__Required_01",
                  "HasInfo": false,
                  "InfoText": null,
                  "Config": null,
                  "ColumnWidth": 6,
                  "HasToggleButton": false
              }
          ],
          "Triggers": null,
          "TriggerConfigs": null,
          "ValueDependencies": [],
          "FilterDependencies": [],
          "VisibilityDependencies": [],
          "DateConstraintDependencies": [],
          "IsRequiredDependencies": [],
          "RemoteUpdateConfigs": [],
          "ValuePropertyDependencies": [],
          "CompoundValueDependencies": null,
          "SaveEndpoint": "/api/patients",
          "SaveText": null,
          "SaveTextKey": "_submit"
      },
      "ExistingData": {}
  },
  "Error": null
} 

const getNewForm = () => {
  return JSON.parse(JSON.stringify(formWithOneSelect))
}
const dummyValidation = {
  "Type": "Dummy",
  "Max": 0,
  "Min": 0,
  "IsBackendValidation": false,
  "ValidationUrl": null,
  "ShowServerError": false,
  "ErrorTargetField": null,
  "ValidationParams": null,
  "ErrorMessage": "Dummy validation message",
  "ErrorMessageKey": "error_dummy",
  "RequiredOnlyWhen": null,
  "Regex": null
}
const getDummyValidation = () => {
  return [JSON.parse(JSON.stringify(dummyValidation))]
}

describe('CheckboxGroup.vue', () => {
  it('renders props.label when passed', async () => {
    const label = 'new label'
    const wrapper = shallowMount(CheckboxGroup, {
      store: getStore(),
      localVue,
      propsData: { label },
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises() 
    expect(wrapper.text()).toMatch(label)
  })

  it('renders correct number of props.allOptions when passed', async () => {
    const allOptions = [{ Key: 3, Value: 'Amharic' }, { Key: 4, Value: 'Luganda' }]
    const wrapper = shallowMount(CheckboxGroup, {
      store: getStore(),
      localVue,
      propsData: { allOptions, optionsWithKeyValue:allOptions },
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises() 
    const labels = wrapper.findAll('label')
    expect(labels.length.toString()).toMatch('3')
  })

  it('correct checkbox gets selected when user selects it', async () => {
    const allOptions = [{ Key: 3, Value: 'Amharic' }]
    const wrapper = mount(CheckboxGroup, {
      store: getStore(),
      localVue,
      propsData: { allOptions, optionsWithKeyValue:allOptions  },
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises() 
    const radioInput = wrapper.find('input[type="checkbox"]')
    radioInput.element.checked = true
    expect(radioInput.element.checked).toBeTruthy()
  })

  it('selects the correct checkbox by default', async () => {
    const allOptions = [{ Key: 3, Value: 'Amharic' }]
    const value = [3]
    const wrapper = mount(CheckboxGroup, {
      store: getStore(),
      localVue,
      propsData: { allOptions, value: value, optionsWithKeyValue:allOptions  },
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises() 
    await wrapper.vm.$nextTick()

    const radioInput = wrapper.find('input[type="checkbox"]')
    expect(radioInput.element.checked).toBeTruthy()
  })

  it('New value gets added when user checks new value', async () => {
    const allOptions = [{ Value: 3, Key: 'Amharic' }, { Value: 2, Key: 'English' }]
    const wrapper = mount(CheckboxGroup, {
      store: getStoreWithActions({
                getFormData: async () => (
                  Promise.resolve(
                    formData
                  )
                )
              }),
      localVue,
      propsData: { allOptions, optionsWithKeyValue:allOptions  },
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises() 
    await wrapper.findAll('.c-checkbox').at(1).trigger('click')
    await wrapper.vm.$nextTick()
    expect(wrapper.findAll('input[type="checkbox"]').at(1).element.checked).toBe(true)
    expect(wrapper.findAll('input[type="checkbox"]').at(0).element.checked).toBe(false)
    await wrapper.findAll('.c-checkbox').at(0).trigger('click')
    expect(wrapper.findAll('input[type="checkbox"]').at(1).element.checked).toBe(true)
    expect(wrapper.findAll('input[type="checkbox"]').at(0).element.checked).toBe(true)
  })
})

describe('CheckboxGroup.vue', () => {
  it('check if validation is invoked on value change and error message displayed', async () => {
    const label = 'new_label'
    const formData = getNewForm()
    const dummyValidation = getDummyValidation()
    formData['Data']['Form']['Fields'][0]['Label'] = label
    formData['Data']['Form']['Fields'][0]['Validations']['And'] = dummyValidation
    const negativeDummy = (_) => false
    const wrapper = mount(Form, {
      store: getStoreWithActions({
        getFormData: async () => (
          Promise.resolve(
            formData
          )
        )
      }),
      localVue,
      propsData: { isEditing: true, name: 'DummyForm' },
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises() 
    await wrapper.findComponent(CheckboxGroup).setData({validationMapper: {Dummy:negativeDummy}})
    await wrapper.findComponent(CheckboxGroup).findAll('input[type="checkbox"]').at(0).setChecked()
    await wrapper.findComponent(CheckboxGroup).vm.$emit('input','a')
    await wrapper.vm.$nextTick()
    expect(wrapper.findComponent(CheckboxGroup).find('.error-message').text()).toMatch(dummyValidation[0]['ErrorMessage'])
  })
})

describe('CheckboxGroup.vue', () => {
  it('check if validation is invoked on value change and error message not displayed if validation return true', async () => {
    const label = 'new_label'
    const formData = getNewForm()
    const dummyValidation = getDummyValidation()
    formData['Data']['Form']['Fields'][0]['Label'] = label
    formData['Data']['Form']['Fields'][0]['Validations']['And'] = dummyValidation
    const postiveDummy = (_) => true
    const wrapper = mount(Form, {
      store: getStoreWithActions ({
        getFormData: async () => (
          Promise.resolve(
            formData
          )
        )
      }),
      localVue,
      propsData: { isEditing: false, name: 'DummyForm' },
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises() 
    await wrapper.findComponent(CheckboxGroup).setData({validationMapper: {Dummy:postiveDummy}})
    await wrapper.findComponent(CheckboxGroup).findAll('input[type="checkbox"]').at(0).setChecked()
    await wrapper.findComponent(CheckboxGroup).vm.$emit('input','a')
    await wrapper.vm.$nextTick()
    expect(wrapper.findComponent(CheckboxGroup).find('.error-message').exists()).toBeFalsy()
  })
})