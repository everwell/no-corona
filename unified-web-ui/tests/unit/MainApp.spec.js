import App from '../../src/app/App'
import { shallowMount, createLocalVue } from '@vue/test-utils'
import Header from '../../src/app/shared/components/Header/Header.vue'
import Vuex from 'vuex'
import VueRouter from 'vue-router'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(VueRouter)

describe('App.vue', () => {
  let globalGetters
  let store
  let globalActions
  let globalData

  beforeEach(() => {
    globalGetters = {
      darkModeState: jest.fn(),
      accessibilityModeState: jest.fn(),
      isUserAuthenticated: jest.fn()
    }
    globalActions = {
      toggleDarkMode: jest.fn(),
      toggleAccessibilityMode: jest.fn()
    }
    globalData = {
      userAuthenticated: false
    }
    store = new Vuex.Store({
      getters: globalGetters,
      actions: globalActions,
      state: globalData
    })
  })

  it('main app to check child component visibility', () => {
    globalGetters.darkModeState.mockReturnValue(true)
    globalGetters.accessibilityModeState.mockReturnValue(false)
    const wrapper = shallowMount(App, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      }
    })
    expect(wrapper.findComponent(Header).isVisible()).toBe(true)
  })

  it('main app to check child component visibility', () => {
    globalGetters.darkModeState.mockReturnValue(true)
    globalGetters.accessibilityModeState.mockReturnValue(false)
    localStorage.setItem('theme', 'light-theme')
    const wrapper = shallowMount(App, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      }
    })
    expect(wrapper.findComponent(Header).isVisible()).toBe(true)
  })

  it('setting dark mode', () => {
    globalGetters.darkModeState.mockReturnValue(true)
    globalGetters.accessibilityModeState.mockReturnValue(false)
    localStorage.setItem('theme', 'dark-theme')
    const wrapper = shallowMount(App, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      }
    })
    expect(wrapper.findComponent(Header).isVisible()).toBe(true)
    expect(globalActions.toggleDarkMode).toBeCalled()
  })

  it('setting acc-mode', () => {
    globalGetters.darkModeState.mockReturnValue(true)
    globalGetters.accessibilityModeState.mockReturnValue(false)
    localStorage.setItem('theme', 'acc-mode')
    const wrapper = shallowMount(App, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      }
    })
    expect(wrapper.findComponent(Header).isVisible()).toBe(true)
    expect(globalActions.toggleAccessibilityMode).toBeCalled()
  })

  it('watch methods light theme', () => {
    globalGetters.darkModeState.mockReturnValue(true)
    globalGetters.accessibilityModeState.mockReturnValue(false)
    const wrapper = shallowMount(App, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      }
    })
    wrapper.vm.$options.watch.darkModeState(false)
    expect(localStorage.theme).toMatch('light-theme')
  })

  it('watch methods dark theme', () => {
    globalGetters.darkModeState.mockReturnValue(true)
    globalGetters.accessibilityModeState.mockReturnValue(false)
    const wrapper = shallowMount(App, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      }
    })
    wrapper.vm.$options.watch.darkModeState(true)
    expect(localStorage.theme).toMatch('dark-theme')
  })

  it('acc-mode', () => {
    globalGetters.darkModeState.mockReturnValue(true)
    globalGetters.accessibilityModeState.mockReturnValue(false)
    const wrapper = shallowMount(App, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      }
    })
    wrapper.vm.$options.watch.accessibilityModeState(true)
    expect(localStorage.theme).toMatch('acc-mode')
  })

  it('acc-mode off to light mode', () => {
    globalGetters.darkModeState.mockReturnValue(true)
    const wrapper = shallowMount(App, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      }
    })
    wrapper.vm.$options.watch.accessibilityModeState(false)
    expect(localStorage.theme).toMatch('dark-theme')
  })
})
