import { mount, createLocalVue } from '@vue/test-utils'
import StaffMapping from '../../src/app/Pages/dashboard/patient/components/StaffMapping'
import Vuex from 'vuex'
import Vue from 'vue'
import { Map, List } from 'immutable'
import flushPromises from 'flush-promises'
import Form from '@/app/shared/components/Form.vue'
import Button from '@/app/shared/components/Button.vue'

const localVue = createLocalVue()
localVue.use(Vuex)
window.EventBus = new Vue()

jest.mock('../../src/utils/toastUtils', () => {
  return {
    __esModule: true,
    defaultToast: jest.fn(),
    ToastType: {
      Success: 'Success',
      Error: 'Error',
      Warning: 'Warning',
      Neutal: 'Neutal'
    }
  }
})

jest.mock('../../src/utils/matomoTracking', () => {
  return {
    __esModule: true,
    initializeMatomo: jest.fn(),
    trackEvent: jest.fn()
  }
})

describe('Staff Mapping Page tests ', () => {
  let store
  let actions
  afterEach(() => {
    jest.clearAllMocks()
  })
  beforeEach(() => {
    actions = {
      getStaffMappingTab: jest.fn(),
      getFormData: jest.fn(),
      setIsEditing: jest.fn(),
      loadForm: jest.fn(),
      startSaving: jest.fn(),
      save: async () => (
        Promise.resolve({
          success: true,
          Error: null,
          Data: 'Success'
        })
      ),
      endSaving: jest.fn()
    }
    store = new Vuex.Store({
      modules: {
        StaffMapping: {
          namespaced: true,
          actions
        },
        Form: {
          namespaced: true,
          state: {
            isSaving: false,
            isLoading: false,
            isEditing: true,
            hasFormError: false,
            adherenceTechnology: { MERM: false, VOT: false },
            existingData: null,
            existingVModel: {},
            form: {},
            title: '',
            remoteUpdateConfigsMappedByField: Map(),
            remoteUpdateConfigs: [],
            hierarchyConfigs: Map(),
            valueDependencies: List(),
            formPartsMappedByName: Map(),
            fieldsMappedByFormPartName: Map(),
            fieldsMappedByNameOriginal: Map(),
            fieldsMappedByName: null,
            allFields: null,
            visibilityDependencies: List(),
            isRequiredDependencies: List(),
            filterDependencies: List(),
            visibilityDependenciesMappedByFormPartName: null,
            dateConstraintDependency: List(),
            formFieldsByFormPartName: {},
            saveEndpoint: '',
            saveText: '',
            saveUrlParams: '',
            getFormUrlParams: '',
            replaceExistingDataFromServer: true,
            recaptchaVerified: false,
            otp: '',
            editAccess: false,
            valuePropertyDependencies: List()
          },
          actions
        }

      }
    })
  })

  it('starting the staff mapping vue app with staff mapping', async () => {
    store.state.Form.isLoading = false
    var sampleResponse = { Success: true, Data: { EnableAdd: false, Permissions: null }, Error: null }
    actions.getStaffMappingTab.mockReturnValue(sampleResponse)
    const wrapper = mount(StaffMapping, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises()
    expect(wrapper.find('#addBtn').exists()).toBe(false)
    expect(wrapper.find('#updateBtn').exists()).toBe(true)
    expect(wrapper.find('#cancelBtn').exists()).toBe(false)
    expect(actions.getStaffMappingTab.mock.calls).toHaveLength(1)
  })

  it('On clicking on the add button, add form opens and then click on cancel button', async () => {
    store.state.Form.isLoading = false
    var sampleResponse = { Success: true, Data: { EnableAdd: true, Permissions: null }, Error: null }
    actions.getStaffMappingTab.mockReturnValue(sampleResponse)
    const wrapper = mount(StaffMapping, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises()
    await wrapper.find('#addBtn').trigger('click')
    await wrapper.vm.$nextTick()
    expect(wrapper.find('#cancelBtn').exists()).toBe(true)
    await wrapper.find('#cancelBtn').trigger('click')
    await wrapper.vm.$nextTick()
    expect(wrapper.find('#addBtn').exists()).toBe(true)
    expect(actions.getStaffMappingTab.mock.calls).toHaveLength(2)
  })

  it('On clicking on the add button, while loading is true', async () => {
    store.state.Form.isLoading = false
    var sampleResponse = { Success: true, Data: { EnableAdd: true, Permissions: null }, Error: null }
    actions.getStaffMappingTab.mockReturnValue(sampleResponse)
    const wrapper = mount(StaffMapping, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises()
    store.state.Form.isLoading = true
    await wrapper.find('#addBtn').trigger('click')
    await wrapper.vm.$nextTick()
    expect(wrapper.find('#cancelBtn').exists()).toBe(false)
    expect(actions.getStaffMappingTab.mock.calls).toHaveLength(1)
  })

  it('On clicking on the edit button, edit form opens and then click on cancel button', async () => {
    store.state.Form.isLoading = false
    var sampleResponse = { Success: true, Data: { EnableAdd: false, Permissions: null }, Error: null }
    actions.getStaffMappingTab.mockReturnValue(sampleResponse)
    const wrapper = mount(StaffMapping, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises()
    await wrapper.find('#updateBtn').trigger('click')
    await wrapper.vm.$nextTick()
    expect(wrapper.find('#cancelBtn').exists()).toBe(true)
    await wrapper.find('#cancelBtn').trigger('click')
    await wrapper.vm.$nextTick()
    expect(wrapper.find('#updateBtn').exists()).toBe(true)
    expect(actions.getStaffMappingTab.mock.calls).toHaveLength(2)
  })

  it('On clicking on the edit button, while loading is true', async () => {
    store.state.Form.isLoading = false
    var sampleResponse = { Success: true, Data: { EnableAdd: false, Permissions: null }, Error: null }
    actions.getStaffMappingTab.mockReturnValue(sampleResponse)
    const wrapper = mount(StaffMapping, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises()
    store.state.Form.isLoading = true
    await wrapper.find('#updateBtn').trigger('click')
    await wrapper.vm.$nextTick()
    expect(wrapper.find('#cancelBtn').exists()).toBe(false)
    expect(actions.getStaffMappingTab.mock.calls).toHaveLength(1)
  })

  it('On clicking on the add button, add form opens and then click on submit button', async () => {
    store.state.Form.isLoading = false
    var sampleResponse = { Success: true, Data: { EnableAdd: true, Permissions: null }, Error: null }
    actions.getStaffMappingTab.mockReturnValue(sampleResponse)
    const wrapper = mount(StaffMapping, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises()
    await wrapper.find('#addBtn').trigger('click')
    await wrapper.vm.$nextTick()
    expect(wrapper.find('#cancelBtn').exists()).toBe(true)
    expect(wrapper.findComponent(Form).exists()).toBe(true)
    await wrapper.findComponent(Form).findComponent(Button).trigger('click')
    await wrapper.vm.$nextTick()
    expect(actions.getStaffMappingTab.mock.calls).toHaveLength(2)
  })

  it('On clicking on the add button, add form opens and then click on cancel button while form is loading', async () => {
    store.state.Form.isLoading = false
    var sampleResponse = { Success: true, Data: { EnableAdd: true, Permissions: null }, Error: null }
    actions.getStaffMappingTab.mockReturnValue(sampleResponse)
    const wrapper = mount(StaffMapping, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises()
    await wrapper.find('#addBtn').trigger('click')
    await wrapper.vm.$nextTick()
    expect(wrapper.find('#cancelBtn').exists()).toBe(true)
    store.state.Form.isLoading = true
    await wrapper.find('#cancelBtn').trigger('click')
    await wrapper.vm.$nextTick()
    expect(wrapper.find('#addBtn').exists()).toBe(false)
    expect(actions.getStaffMappingTab.mock.calls).toHaveLength(1)
  })

  it('On clicking on the edit button, edit form opens and then click on cancel button while form is loading', async () => {
    store.state.Form.isLoading = false
    var sampleResponse = { Success: true, Data: { EnableAdd: false, Permissions: null }, Error: null }
    actions.getStaffMappingTab.mockReturnValue(sampleResponse)
    const wrapper = mount(StaffMapping, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises()
    await wrapper.find('#updateBtn').trigger('click')
    await wrapper.vm.$nextTick()
    expect(wrapper.find('#cancelBtn').exists()).toBe(true)
    store.state.Form.isLoading = true
    await wrapper.find('#cancelBtn').trigger('click')
    await wrapper.vm.$nextTick()
    expect(wrapper.find('#updateBtn').exists()).toBe(false)
    expect(actions.getStaffMappingTab.mock.calls).toHaveLength(1)
  })
})
