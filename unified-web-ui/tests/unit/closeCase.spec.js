import { mount, createLocalVue } from '@vue/test-utils'
import CloseCase from '../../src/app/Pages/dashboard/patient/components/CloseCase'
import Button from '../../src/app/shared/components/Button'
import Form from '../../src/app/shared/components/Form'
import VerticalFormPart from '../../src/app/shared/components/VerticalFormPart'
import Input from '../../src/app/shared/components/Input'
import RadioGroup from '../../src/app/shared/components/RadioGroup'
import DatePicker from '../../src/app/shared/components/DatePicker'
import Select from '../../src/app/shared/components/Select'
import TextArea from '../../src/app/shared/components/TextArea'
import Vuex from 'vuex'
import FormStore from '@/app/shared/store/modules/form'
import flushPromises from 'flush-promises'
import Vue from 'vue'

const localVue = createLocalVue()
localVue.use(Vuex)
window.EventBus = new Vue()

const mockTranslate = (key) => {
  let translatedKey = key
  const translations = {
    close_case: 'Close case',
    cancel: 'Cancel',
    patient_details_activity_reopen_case: 'Reopen Case',
    submit: 'Submit'
  }

  if (translations[key]) {
    translatedKey = translations[key]
  }

  return translatedKey
}

jest.mock('../../src/utils/toastUtils', () => {
  return {
    __esModule: true,
    defaultToast: jest.fn(),
    ToastType: {
      Success: 'Success',
      Error: 'Error',
      Warning: 'Warning',
      Neutal: 'Neutal'
    }
  }
})

jest.mock('../../src/utils/matomoTracking', () => {
  return {
    __esModule: true,
    initializeMatomo: jest.fn(),
    trackEvent: jest.fn()
  }
})

describe('Close case page Tests', () => {
  const reload = window.location.reload

  beforeAll(() => {
    Object.defineProperty(window, 'location', {
      value: { reload: jest.fn() }
    })
  })

  afterAll(() => {
    window.location.reload = reload
  })

  it('close case for on_treatment patient', async () => {
    const wrapper = mount(CloseCase, {
      store: new Vuex.Store({
        modules: {
          Patient: {
            namespaced: true,
            state: {
              patientId: 1234,
              stage: 'ON_TREATMENT',
              endDate: '20-01-2022'
            }
          }
        }
      }),
      localVue,
      mocks: {
        $t: mockTranslate
      }
    })

    expect(wrapper.findAllComponents(Button).length).toBe(1)
    expect(wrapper.findComponent(Button).text()).toBe('Close case')
    expect(wrapper.findAll('div.info p').length).toBe(1)
  })

  it('click on close case', async () => {
    const patientId = 1234
    const patientName = 'Name'

    const wrapper = mount(CloseCase, {
      store: new Vuex.Store({
        modules: {
          Patient: {
            namespaced: true,
            state: {
              patientId: 1234,
              stage: 'ON_TREATMENT',
              endDate: '20-01-2022'
            }
          },
          Form: {
            state: FormStore.state,
            actions: Object.assign(FormStore.actions, {
              getFormData: async () => (
                Promise.resolve(
                  JSON.parse('{"Success":true,"Data":{"Form":{"Name":"CloseCase","Title":"CloseCase","Parts":[{"FormName":"CloseCase","Name":"TreatmentInformation","Title":"","TitleKey":"","Order":1,"IsVisible":true,"Id":61,"Type":"vertical-form-part","Rows":null,"Columns":null,"RowDataName":"TreatmentInformation","AllowRowOpen":false,"AllowRowDelete":false,"ListItemTitle":null,"ItemDescriptionField":null,"IsRepeatable":false,"RecordId":null}],"PartOptions":null,"Fields":[{"Value":null,"PartName":"TreatmentInformation","Placeholder":null,"Component":"app-input-field","ComponentVariants":null,"IsDisabled":true,"IsVisible":true,"LabelKey":"PatientId","Label":"Patient Id","Name":"PatientId","IsHierarchySelector":false,"IsRequired":false,"RemoteUrl":null,"Type":null,"AddOn":null,"Options":null,"HierarchyOptions":null,"OptionsWithKeyValue":null,"AdditionalInfo":null,"DefaultVisibilty":false,"Order":0,"OptionsWithLabel":null,"Validations":{"Or":null,"And":[]},"ResponseDataPath":null,"OptionDisplayKeys":null,"OptionValueKey":null,"LoadImmediately":false,"HierarchySelectionConfigs":null,"DisabledDateConfig":null,"RemoteUpdateConfig":null,"RowNumber":1,"ColumnNumber":null,"Id":11153,"ParentType":"PART","ParentId":61,"FieldOptions":null,"ValidationList":null,"DefaultValue":null,"Key":"TreatmentInformationPatientId","HasInfo":false,"InfoText":null,"Config":null,"ColumnWidth":6,"HasToggleButton":false},{"Value":null,"PartName":"TreatmentInformation","Placeholder":null,"Component":"app-input-field","ComponentVariants":null,"IsDisabled":true,"IsVisible":true,"LabelKey":"patient_name","Label":"Patient Name","Name":"PatientName","IsHierarchySelector":false,"IsRequired":false,"RemoteUrl":null,"Type":null,"AddOn":null,"Options":null,"HierarchyOptions":null,"OptionsWithKeyValue":null,"AdditionalInfo":null,"DefaultVisibilty":false,"Order":0,"OptionsWithLabel":null,"Validations":{"Or":null,"And":[]},"ResponseDataPath":null,"OptionDisplayKeys":null,"OptionValueKey":null,"LoadImmediately":false,"HierarchySelectionConfigs":null,"DisabledDateConfig":null,"RemoteUpdateConfig":null,"RowNumber":2,"ColumnNumber":null,"Id":11154,"ParentType":"PART","ParentId":61,"FieldOptions":null,"ValidationList":null,"DefaultValue":null,"Key":"TreatmentInformationPatientName","HasInfo":false,"InfoText":null,"Config":null,"ColumnWidth":6,"HasToggleButton":false},{"Value":null,"PartName":"TreatmentInformation","Placeholder":null,"Component":"app-radio","ComponentVariants":null,"IsDisabled":false,"IsVisible":true,"LabelKey":"treatment_outcome","Label":"Treatment Outcome*","Name":"TreatmentOutcome","IsHierarchySelector":false,"IsRequired":true,"RemoteUrl":null,"Type":null,"AddOn":null,"Options":null,"HierarchyOptions":null,"OptionsWithKeyValue":[{"Key":"CURED","Value":"Cured"},{"Key":"TREATMENT_FAILURE","Value":"Treatment Failure"},{"Key":"TREATMENT_COMPLETE","Value":"Treatment Complete"},{"Key":"NOT_EVALUATED","Value":"Not Evaluated"},{"Key":"LOST_TO_FOLLOW_UP","Value":"Lost To Follow Up"},{"Key":"TREATMENT_REGIMEN_CHANGED","Value":"Treatment Regimen Changed"},{"Key":"OPTED_OUT","Value":"Opted Out"},{"Key":"TRANSFERRED_OUT","Value":"Transferred Out"},{"Key":"DIED","Value":"Died"},{"Key":"OTHER","Value":"Other"}],"AdditionalInfo":null,"DefaultVisibilty":false,"Order":0,"OptionsWithLabel":null,"Validations":{"Or":null,"And":[{"Type":"Required","Max":0,"Min":0,"IsBackendValidation":false,"ValidationUrl":null,"ShowServerError":false,"ErrorTargetField":null,"ValidationParams":null,"ErrorMessage":"This field is required","ErrorMessageKey":"error_field_required","RequiredOnlyWhen":null,"Regex":null}]},"ResponseDataPath":null,"OptionDisplayKeys":null,"OptionValueKey":null,"LoadImmediately":false,"HierarchySelectionConfigs":null,"DisabledDateConfig":null,"RemoteUpdateConfig":null,"RowNumber":3,"ColumnNumber":null,"Id":11155,"ParentType":"PART","ParentId":61,"FieldOptions":"[{\\"Key\\":\\"CURED\\", \\"Value\\":\\"_cured\\"}, {\\"Key\\":\\"TREATMENT_FAILURE\\", \\"Value\\":\\"_treatment_failure\\"}, {\\"Key\\":\\"TREATMENT_COMPLETE\\", \\"Value\\":\\"_treatment_complete\\"}, {\\"Key\\":\\"NOT_EVALUATED\\", \\"Value\\":\\"_not_evaluated\\"},{\\"Key\\":\\"LOST_TO_FOLLOW_UP\\", \\"Value\\":\\"_lost_to_follow_up\\"},{\\"Key\\":\\"TREATMENT_REGIMEN_CHANGED\\", \\"Value\\":\\"_treatment_regimen_changed\\"},{\\"Key\\":\\"OPTED_OUT\\", \\"Value\\":\\"_opted_out\\"},{\\"Key\\":\\"TRANSFERRED_OUT\\", \\"Value\\":\\"_transferred_out\\"},{\\"Key\\":\\"DIED\\", \\"Value\\":\\"_died\\"},{\\"Key\\":\\"OTHER\\", \\"Value\\":\\"_other\\"}]","ValidationList":null,"DefaultValue":null,"Key":"TreatmentInformationTreatmentOutcome","HasInfo":false,"InfoText":null,"Config":null,"ColumnWidth":6,"HasToggleButton":false},{"Value":null,"PartName":"TreatmentInformation","Placeholder":null,"Component":"app-textarea","ComponentVariants":null,"IsDisabled":false,"IsVisible":true,"LabelKey":"note_text","Label":"Add a Note","Name":"NoteText","IsHierarchySelector":false,"IsRequired":false,"RemoteUrl":null,"Type":null,"AddOn":null,"Options":null,"HierarchyOptions":null,"OptionsWithKeyValue":null,"AdditionalInfo":null,"DefaultVisibilty":false,"Order":0,"OptionsWithLabel":null,"Validations":{"Or":null,"And":[]},"ResponseDataPath":null,"OptionDisplayKeys":null,"OptionValueKey":null,"LoadImmediately":false,"HierarchySelectionConfigs":null,"DisabledDateConfig":null,"RemoteUpdateConfig":null,"RowNumber":4,"ColumnNumber":null,"Id":11156,"ParentType":"PART","ParentId":61,"FieldOptions":null,"ValidationList":null,"DefaultValue":null,"Key":"TreatmentInformationNoteText","HasInfo":false,"InfoText":null,"Config":null,"ColumnWidth":6,"HasToggleButton":false},{"Value":null,"PartName":"TreatmentInformation","Placeholder":null,"Component":"app-datepicker","ComponentVariants":null,"IsDisabled":false,"IsVisible":true,"LabelKey":"end_date","Label":"End Date","Name":"EndDate","IsHierarchySelector":false,"IsRequired":true,"RemoteUrl":null,"Type":null,"AddOn":null,"Options":null,"HierarchyOptions":null,"OptionsWithKeyValue":null,"AdditionalInfo":null,"DefaultVisibilty":false,"Order":0,"OptionsWithLabel":null,"Validations":{"Or":null,"And":[{"Type":"Required","Max":0,"Min":0,"IsBackendValidation":false,"ValidationUrl":null,"ShowServerError":false,"ErrorTargetField":null,"ValidationParams":null,"ErrorMessage":"This field is required","ErrorMessageKey":"error_field_required","RequiredOnlyWhen":null,"Regex":null}]},"ResponseDataPath":null,"OptionDisplayKeys":null,"OptionValueKey":null,"LoadImmediately":false,"HierarchySelectionConfigs":null,"DisabledDateConfig":{"To":"2022-03-01T00:00:00","From":"2022-03-21T11:16:02.0309179+05:30","Days":null,"DaysOfMonth":null,"Dates":null,"Ranges":null,"AddDaysFromToday":0,"SubtractDaysFromToday":180},"RemoteUpdateConfig":null,"RowNumber":6,"ColumnNumber":null,"Id":11158,"ParentType":"PART","ParentId":61,"FieldOptions":null,"ValidationList":null,"DefaultValue":null,"Key":"TreatmentInformationEndDate","HasInfo":false,"InfoText":null,"Config":null,"ColumnWidth":6,"HasToggleButton":false},{"Value":null,"PartName":"TreatmentInformation","Placeholder":null,"Component":"app-datepicker","ComponentVariants":null,"IsDisabled":false,"IsVisible":false,"LabelKey":"","Label":null,"Name":"StartDate","IsHierarchySelector":false,"IsRequired":false,"RemoteUrl":null,"Type":null,"AddOn":null,"Options":null,"HierarchyOptions":null,"OptionsWithKeyValue":null,"AdditionalInfo":null,"DefaultVisibilty":false,"Order":0,"OptionsWithLabel":null,"Validations":{"Or":null,"And":[]},"ResponseDataPath":null,"OptionDisplayKeys":null,"OptionValueKey":null,"LoadImmediately":false,"HierarchySelectionConfigs":null,"DisabledDateConfig":null,"RemoteUpdateConfig":null,"RowNumber":null,"ColumnNumber":null,"Id":11159,"ParentType":"PART","ParentId":61,"FieldOptions":null,"ValidationList":null,"DefaultValue":null,"Key":"TreatmentInformationStartDate","HasInfo":false,"InfoText":null,"Config":null,"ColumnWidth":6,"HasToggleButton":false}],"Triggers":null,"TriggerConfigs":null,"ValueDependencies":[],"FilterDependencies":[],"VisibilityDependencies":[],"DateConstraintDependencies":[{"Lookups":[{"FormPart":"TreatmentInformation","Field":"StartDate","ExpectedValue":null,"ExpectedValues":null,"IsObject":false,"ObjectKeyAndValues":null,"ObjectKeyAndValue":null,"AttributeName":null,"ConstraintName":"To","RestrictedValue":null,"ComparisonOperator":"EQ","AnyObjectKeyValue":false,"FilteredKeys":null,"LookupFormPart":"TreatmentInformation","LookupField":"StartDate"}],"FormPart":"TreatmentInformation","Field":"EndDate","FormPartId":null,"FieldId":null,"Type":"DATE_CONSTRAINT","IsForm":false,"PropertyAndValues":null}],"IsRequiredDependencies":[{"Lookups":[{"FormPart":"TreatmentInformation","Field":"TreatmentOutcome","ExpectedValue":null,"ExpectedValues":["OTHER"],"IsObject":false,"ObjectKeyAndValues":null,"ObjectKeyAndValue":null,"AttributeName":null,"ConstraintName":null,"RestrictedValue":null,"ComparisonOperator":"EQ","AnyObjectKeyValue":false,"FilteredKeys":null,"LookupFormPart":"TreatmentInformation","LookupField":"TreatmentOutcome"}],"FormPart":"TreatmentInformation","Field":"NoteText","FormPartId":null,"FieldId":null,"Type":"IS_REQUIRED","IsForm":false,"PropertyAndValues":null}],"RemoteUpdateConfigs":[],"ValuePropertyDependencies":[],"CompoundValueDependencies":null,"SaveEndpoint":"/API/Patients/CloseCase","SaveText":"Close case","SaveTextKey":"close_case"},"ExistingData":{"PatientId":' + patientId + ',"EndDate":null,"PatientName":"' + patientName + '","StartDate":"2022-03-01T00:00:00"}},"Error":null}')
                )
              )
            }),
            mutations: FormStore.mutations,
            namespaced: true
          }
        }
      }),
      localVue,
      mocks: {
        $t: mockTranslate
      }
    })

    await wrapper.findComponent(Button).trigger('click')

    await flushPromises()

    expect(wrapper.findAllComponents(Button).length).toBe(2)
    expect(wrapper.findAllComponents(Button).at(0).text()).toBe('Cancel')
    expect(wrapper.findAllComponents(Button).at(1).text()).toBe('Close case')

    expect(wrapper.findAllComponents(Form).length).toBe(1)
    expect(wrapper.findAllComponents(VerticalFormPart).length).toBe(1)

    expect(wrapper.findComponent(Form).findComponent(VerticalFormPart).findAllComponents(Input).length).toBe(2)
    expect(wrapper.findComponent(Form).findComponent(VerticalFormPart).findAllComponents(TextArea).length).toBe(1)
    expect(wrapper.findComponent(Form).findComponent(VerticalFormPart).findAllComponents(RadioGroup).length).toBe(1)
    expect(wrapper.findComponent(Form).findComponent(VerticalFormPart).findAllComponents(DatePicker).length).toBe(1)

    expect(wrapper.findComponent(Form).findComponent(VerticalFormPart).findAllComponents(Input).at(0).find('input').element.value).toBe(patientId.toString())
    expect(wrapper.findComponent(Form).findComponent(VerticalFormPart).findAllComponents(Input).at(1).find('input').element.value).toBe(patientName)
  })

  it('click on cancel close case', async () => {
    const patientId = 1234
    const patientName = 'Name'

    const wrapper = mount(CloseCase, {
      store: new Vuex.Store({
        modules: {
          Patient: {
            namespaced: true,
            state: {
              patientId: 1234,
              stage: 'ON_TREATMENT',
              endDate: '20-01-2022'
            }
          },
          Form: {
            state: FormStore.state,
            actions: Object.assign(FormStore.actions, {
              getFormData: async () => (
                Promise.resolve(
                  JSON.parse('{"Success":true,"Data":{"Form":{"Name":"CloseCase","Title":"CloseCase","Parts":[{"FormName":"CloseCase","Name":"TreatmentInformation","Title":"","TitleKey":"","Order":1,"IsVisible":true,"Id":61,"Type":"vertical-form-part","Rows":null,"Columns":null,"RowDataName":"TreatmentInformation","AllowRowOpen":false,"AllowRowDelete":false,"ListItemTitle":null,"ItemDescriptionField":null,"IsRepeatable":false,"RecordId":null}],"PartOptions":null,"Fields":[{"Value":null,"PartName":"TreatmentInformation","Placeholder":null,"Component":"app-input-field","ComponentVariants":null,"IsDisabled":true,"IsVisible":true,"LabelKey":"PatientId","Label":"Patient Id","Name":"PatientId","IsHierarchySelector":false,"IsRequired":false,"RemoteUrl":null,"Type":null,"AddOn":null,"Options":null,"HierarchyOptions":null,"OptionsWithKeyValue":null,"AdditionalInfo":null,"DefaultVisibilty":false,"Order":0,"OptionsWithLabel":null,"Validations":{"Or":null,"And":[]},"ResponseDataPath":null,"OptionDisplayKeys":null,"OptionValueKey":null,"LoadImmediately":false,"HierarchySelectionConfigs":null,"DisabledDateConfig":null,"RemoteUpdateConfig":null,"RowNumber":1,"ColumnNumber":null,"Id":11153,"ParentType":"PART","ParentId":61,"FieldOptions":null,"ValidationList":null,"DefaultValue":null,"Key":"TreatmentInformationPatientId","HasInfo":false,"InfoText":null,"Config":null,"ColumnWidth":6,"HasToggleButton":false},{"Value":null,"PartName":"TreatmentInformation","Placeholder":null,"Component":"app-input-field","ComponentVariants":null,"IsDisabled":true,"IsVisible":true,"LabelKey":"patient_name","Label":"Patient Name","Name":"PatientName","IsHierarchySelector":false,"IsRequired":false,"RemoteUrl":null,"Type":null,"AddOn":null,"Options":null,"HierarchyOptions":null,"OptionsWithKeyValue":null,"AdditionalInfo":null,"DefaultVisibilty":false,"Order":0,"OptionsWithLabel":null,"Validations":{"Or":null,"And":[]},"ResponseDataPath":null,"OptionDisplayKeys":null,"OptionValueKey":null,"LoadImmediately":false,"HierarchySelectionConfigs":null,"DisabledDateConfig":null,"RemoteUpdateConfig":null,"RowNumber":2,"ColumnNumber":null,"Id":11154,"ParentType":"PART","ParentId":61,"FieldOptions":null,"ValidationList":null,"DefaultValue":null,"Key":"TreatmentInformationPatientName","HasInfo":false,"InfoText":null,"Config":null,"ColumnWidth":6,"HasToggleButton":false},{"Value":null,"PartName":"TreatmentInformation","Placeholder":null,"Component":"app-radio","ComponentVariants":null,"IsDisabled":false,"IsVisible":true,"LabelKey":"treatment_outcome","Label":"Treatment Outcome*","Name":"TreatmentOutcome","IsHierarchySelector":false,"IsRequired":true,"RemoteUrl":null,"Type":null,"AddOn":null,"Options":null,"HierarchyOptions":null,"OptionsWithKeyValue":[{"Key":"CURED","Value":"Cured"},{"Key":"TREATMENT_FAILURE","Value":"Treatment Failure"},{"Key":"TREATMENT_COMPLETE","Value":"Treatment Complete"},{"Key":"NOT_EVALUATED","Value":"Not Evaluated"},{"Key":"LOST_TO_FOLLOW_UP","Value":"Lost To Follow Up"},{"Key":"TREATMENT_REGIMEN_CHANGED","Value":"Treatment Regimen Changed"},{"Key":"OPTED_OUT","Value":"Opted Out"},{"Key":"TRANSFERRED_OUT","Value":"Transferred Out"},{"Key":"DIED","Value":"Died"},{"Key":"OTHER","Value":"Other"}],"AdditionalInfo":null,"DefaultVisibilty":false,"Order":0,"OptionsWithLabel":null,"Validations":{"Or":null,"And":[{"Type":"Required","Max":0,"Min":0,"IsBackendValidation":false,"ValidationUrl":null,"ShowServerError":false,"ErrorTargetField":null,"ValidationParams":null,"ErrorMessage":"This field is required","ErrorMessageKey":"error_field_required","RequiredOnlyWhen":null,"Regex":null}]},"ResponseDataPath":null,"OptionDisplayKeys":null,"OptionValueKey":null,"LoadImmediately":false,"HierarchySelectionConfigs":null,"DisabledDateConfig":null,"RemoteUpdateConfig":null,"RowNumber":3,"ColumnNumber":null,"Id":11155,"ParentType":"PART","ParentId":61,"FieldOptions":"[{\\"Key\\":\\"CURED\\", \\"Value\\":\\"_cured\\"}, {\\"Key\\":\\"TREATMENT_FAILURE\\", \\"Value\\":\\"_treatment_failure\\"}, {\\"Key\\":\\"TREATMENT_COMPLETE\\", \\"Value\\":\\"_treatment_complete\\"}, {\\"Key\\":\\"NOT_EVALUATED\\", \\"Value\\":\\"_not_evaluated\\"},{\\"Key\\":\\"LOST_TO_FOLLOW_UP\\", \\"Value\\":\\"_lost_to_follow_up\\"},{\\"Key\\":\\"TREATMENT_REGIMEN_CHANGED\\", \\"Value\\":\\"_treatment_regimen_changed\\"},{\\"Key\\":\\"OPTED_OUT\\", \\"Value\\":\\"_opted_out\\"},{\\"Key\\":\\"TRANSFERRED_OUT\\", \\"Value\\":\\"_transferred_out\\"},{\\"Key\\":\\"DIED\\", \\"Value\\":\\"_died\\"},{\\"Key\\":\\"OTHER\\", \\"Value\\":\\"_other\\"}]","ValidationList":null,"DefaultValue":null,"Key":"TreatmentInformationTreatmentOutcome","HasInfo":false,"InfoText":null,"Config":null,"ColumnWidth":6,"HasToggleButton":false},{"Value":null,"PartName":"TreatmentInformation","Placeholder":null,"Component":"app-textarea","ComponentVariants":null,"IsDisabled":false,"IsVisible":true,"LabelKey":"note_text","Label":"Add a Note","Name":"NoteText","IsHierarchySelector":false,"IsRequired":false,"RemoteUrl":null,"Type":null,"AddOn":null,"Options":null,"HierarchyOptions":null,"OptionsWithKeyValue":null,"AdditionalInfo":null,"DefaultVisibilty":false,"Order":0,"OptionsWithLabel":null,"Validations":{"Or":null,"And":[]},"ResponseDataPath":null,"OptionDisplayKeys":null,"OptionValueKey":null,"LoadImmediately":false,"HierarchySelectionConfigs":null,"DisabledDateConfig":null,"RemoteUpdateConfig":null,"RowNumber":4,"ColumnNumber":null,"Id":11156,"ParentType":"PART","ParentId":61,"FieldOptions":null,"ValidationList":null,"DefaultValue":null,"Key":"TreatmentInformationNoteText","HasInfo":false,"InfoText":null,"Config":null,"ColumnWidth":6,"HasToggleButton":false},{"Value":null,"PartName":"TreatmentInformation","Placeholder":null,"Component":"app-datepicker","ComponentVariants":null,"IsDisabled":false,"IsVisible":true,"LabelKey":"end_date","Label":"End Date","Name":"EndDate","IsHierarchySelector":false,"IsRequired":true,"RemoteUrl":null,"Type":null,"AddOn":null,"Options":null,"HierarchyOptions":null,"OptionsWithKeyValue":null,"AdditionalInfo":null,"DefaultVisibilty":false,"Order":0,"OptionsWithLabel":null,"Validations":{"Or":null,"And":[{"Type":"Required","Max":0,"Min":0,"IsBackendValidation":false,"ValidationUrl":null,"ShowServerError":false,"ErrorTargetField":null,"ValidationParams":null,"ErrorMessage":"This field is required","ErrorMessageKey":"error_field_required","RequiredOnlyWhen":null,"Regex":null}]},"ResponseDataPath":null,"OptionDisplayKeys":null,"OptionValueKey":null,"LoadImmediately":false,"HierarchySelectionConfigs":null,"DisabledDateConfig":{"To":"2022-03-01T00:00:00","From":"2022-03-21T11:16:02.0309179+05:30","Days":null,"DaysOfMonth":null,"Dates":null,"Ranges":null,"AddDaysFromToday":0,"SubtractDaysFromToday":180},"RemoteUpdateConfig":null,"RowNumber":6,"ColumnNumber":null,"Id":11158,"ParentType":"PART","ParentId":61,"FieldOptions":null,"ValidationList":null,"DefaultValue":null,"Key":"TreatmentInformationEndDate","HasInfo":false,"InfoText":null,"Config":null,"ColumnWidth":6,"HasToggleButton":false},{"Value":null,"PartName":"TreatmentInformation","Placeholder":null,"Component":"app-datepicker","ComponentVariants":null,"IsDisabled":false,"IsVisible":false,"LabelKey":"","Label":null,"Name":"StartDate","IsHierarchySelector":false,"IsRequired":false,"RemoteUrl":null,"Type":null,"AddOn":null,"Options":null,"HierarchyOptions":null,"OptionsWithKeyValue":null,"AdditionalInfo":null,"DefaultVisibilty":false,"Order":0,"OptionsWithLabel":null,"Validations":{"Or":null,"And":[]},"ResponseDataPath":null,"OptionDisplayKeys":null,"OptionValueKey":null,"LoadImmediately":false,"HierarchySelectionConfigs":null,"DisabledDateConfig":null,"RemoteUpdateConfig":null,"RowNumber":null,"ColumnNumber":null,"Id":11159,"ParentType":"PART","ParentId":61,"FieldOptions":null,"ValidationList":null,"DefaultValue":null,"Key":"TreatmentInformationStartDate","HasInfo":false,"InfoText":null,"Config":null,"ColumnWidth":6,"HasToggleButton":false}],"Triggers":null,"TriggerConfigs":null,"ValueDependencies":[],"FilterDependencies":[],"VisibilityDependencies":[],"DateConstraintDependencies":[{"Lookups":[{"FormPart":"TreatmentInformation","Field":"StartDate","ExpectedValue":null,"ExpectedValues":null,"IsObject":false,"ObjectKeyAndValues":null,"ObjectKeyAndValue":null,"AttributeName":null,"ConstraintName":"To","RestrictedValue":null,"ComparisonOperator":"EQ","AnyObjectKeyValue":false,"FilteredKeys":null,"LookupFormPart":"TreatmentInformation","LookupField":"StartDate"}],"FormPart":"TreatmentInformation","Field":"EndDate","FormPartId":null,"FieldId":null,"Type":"DATE_CONSTRAINT","IsForm":false,"PropertyAndValues":null}],"IsRequiredDependencies":[{"Lookups":[{"FormPart":"TreatmentInformation","Field":"TreatmentOutcome","ExpectedValue":null,"ExpectedValues":["OTHER"],"IsObject":false,"ObjectKeyAndValues":null,"ObjectKeyAndValue":null,"AttributeName":null,"ConstraintName":null,"RestrictedValue":null,"ComparisonOperator":"EQ","AnyObjectKeyValue":false,"FilteredKeys":null,"LookupFormPart":"TreatmentInformation","LookupField":"TreatmentOutcome"}],"FormPart":"TreatmentInformation","Field":"NoteText","FormPartId":null,"FieldId":null,"Type":"IS_REQUIRED","IsForm":false,"PropertyAndValues":null}],"RemoteUpdateConfigs":[],"ValuePropertyDependencies":[],"CompoundValueDependencies":null,"SaveEndpoint":"/API/Patients/CloseCase","SaveText":"Close case","SaveTextKey":"close_case"},"ExistingData":{"PatientId":' + patientId + ',"EndDate":null,"PatientName":"' + patientName + '","StartDate":"2022-03-01T00:00:00"}},"Error":null}')
                )
              )
            }),
            mutations: FormStore.mutations,
            namespaced: true
          }
        }
      }),
      localVue,
      mocks: {
        $t: mockTranslate
      }
    })

    await wrapper.findComponent(Button).trigger('click')

    await flushPromises()

    await wrapper.findAllComponents(Button).at(0).trigger('click')

    expect(wrapper.findAllComponents(Button).length).toBe(1)
    expect(wrapper.findComponent(Button).text()).toBe('Close case')
    expect(wrapper.findAll('div.info p').length).toBe(1)
  })

  it('reopen case page load for on_treatment patient', async () => {
    const treatmentOutcome = 'Cured'
    const wrapper = mount(CloseCase, {
      store: new Vuex.Store({
        modules: {
          Patient: {
            namespaced: true,
            state: {
              patientId: 1234,
              stage: 'OFF_TREATMENT',
              endDate: '20-01-2022',
              treatmentOutcome
            }
          }
        }
      }),
      localVue,
      mocks: {
        $t: mockTranslate
      }
    })

    expect(wrapper.findAllComponents(Button).length).toBe(1)
    expect(wrapper.findComponent(Button).text()).toBe('Reopen Case')
    expect(wrapper.findAll('div.info p').length).toBe(2)
    expect(wrapper.findAll('div.info p').at(1).find('span').text()).toBe(treatmentOutcome)
  })

  it('click on reopen case', async () => {
    const treatmentOutcome = 'Cured'
    const availableMerms = [
      '123',
      '456',
      '789'
    ]

    const wrapper = mount(CloseCase, {
      store: new Vuex.Store({
        modules: {
          Patient: {
            namespaced: true,
            state: {
              patientId: 1234,
              stage: 'OFF_TREATMENT',
              endDate: '20-01-2022',
              treatmentOutcome,
              availableMerms,
              monitoringMethod: 'MERM'
            }
          }
        }
      }),
      localVue,
      mocks: {
        $t: mockTranslate
      }
    })

    await wrapper.findComponent(Button).trigger('click')

    await flushPromises()

    expect(wrapper.findAllComponents(Button).length).toBe(2)
    expect(wrapper.findAllComponents(Button).at(0).text()).toBe('Submit')
    expect(wrapper.findAllComponents(Button).at(1).text()).toBe('Cancel')

    expect(wrapper.findAll('form').length).toBe(1)

    expect(wrapper.findAllComponents(DatePicker).length).toBe(1)
    expect(wrapper.findAllComponents(Select).length).toBe(1)
    expect(wrapper.findComponent(Select).findAll('ul li').length).toBe(availableMerms.length)
    expect(wrapper.findAllComponents(TextArea).length).toBe(1)
  })

  it('click on cancel reopen case', async () => {
    const treatmentOutcome = 'Cured'
    const availableMerms = [
      '123',
      '456',
      '789'
    ]

    const wrapper = mount(CloseCase, {
      store: new Vuex.Store({
        modules: {
          Patient: {
            namespaced: true,
            state: {
              patientId: 1234,
              stage: 'OFF_TREATMENT',
              endDate: '20-01-2022',
              treatmentOutcome,
              availableMerms
            }
          }
        }
      }),
      localVue,
      mocks: {
        $t: mockTranslate
      }
    })

    await wrapper.findComponent(Button).trigger('click')

    await flushPromises()

    await wrapper.findAllComponents(Button).at(1).trigger('click')

    await flushPromises()

    expect(wrapper.findAllComponents(Button).length).toBe(1)
    expect(wrapper.findComponent(Button).text()).toBe('Reopen Case')
    expect(wrapper.findAll('div.info p').length).toBe(2)
    expect(wrapper.findAll('div.info p').at(1).find('span').text()).toBe(treatmentOutcome)
  })

  it('submit close case', async () => {
    const patientId = 1234
    const patientName = 'Name'

    const wrapper = mount(CloseCase, {
      store: new Vuex.Store({
        modules: {
          Patient: {
            namespaced: true,
            state: {
              patientId: 1234,
              stage: 'ON_TREATMENT',
              endDate: '20-01-2022'
            }
          },
          Form: {
            state: FormStore.state,
            actions: Object.assign(FormStore.actions, {
              getFormData: async () => (
                Promise.resolve(
                  JSON.parse('{"Success":true,"Data":{"Form":{"Name":"CloseCase","Title":"CloseCase","Parts":[{"FormName":"CloseCase","Name":"TreatmentInformation","Title":"","TitleKey":"","Order":1,"IsVisible":true,"Id":61,"Type":"vertical-form-part","Rows":null,"Columns":null,"RowDataName":"TreatmentInformation","AllowRowOpen":false,"AllowRowDelete":false,"ListItemTitle":null,"ItemDescriptionField":null,"IsRepeatable":false,"RecordId":null}],"PartOptions":null,"Fields":[{"Value":null,"PartName":"TreatmentInformation","Placeholder":null,"Component":"app-input-field","ComponentVariants":null,"IsDisabled":true,"IsVisible":true,"LabelKey":"PatientId","Label":"Patient Id","Name":"PatientId","IsHierarchySelector":false,"IsRequired":false,"RemoteUrl":null,"Type":null,"AddOn":null,"Options":null,"HierarchyOptions":null,"OptionsWithKeyValue":null,"AdditionalInfo":null,"DefaultVisibilty":false,"Order":0,"OptionsWithLabel":null,"Validations":{"Or":null,"And":[]},"ResponseDataPath":null,"OptionDisplayKeys":null,"OptionValueKey":null,"LoadImmediately":false,"HierarchySelectionConfigs":null,"DisabledDateConfig":null,"RemoteUpdateConfig":null,"RowNumber":1,"ColumnNumber":null,"Id":11153,"ParentType":"PART","ParentId":61,"FieldOptions":null,"ValidationList":null,"DefaultValue":null,"Key":"TreatmentInformationPatientId","HasInfo":false,"InfoText":null,"Config":null,"ColumnWidth":6,"HasToggleButton":false},{"Value":null,"PartName":"TreatmentInformation","Placeholder":null,"Component":"app-input-field","ComponentVariants":null,"IsDisabled":true,"IsVisible":true,"LabelKey":"patient_name","Label":"Patient Name","Name":"PatientName","IsHierarchySelector":false,"IsRequired":false,"RemoteUrl":null,"Type":null,"AddOn":null,"Options":null,"HierarchyOptions":null,"OptionsWithKeyValue":null,"AdditionalInfo":null,"DefaultVisibilty":false,"Order":0,"OptionsWithLabel":null,"Validations":{"Or":null,"And":[]},"ResponseDataPath":null,"OptionDisplayKeys":null,"OptionValueKey":null,"LoadImmediately":false,"HierarchySelectionConfigs":null,"DisabledDateConfig":null,"RemoteUpdateConfig":null,"RowNumber":2,"ColumnNumber":null,"Id":11154,"ParentType":"PART","ParentId":61,"FieldOptions":null,"ValidationList":null,"DefaultValue":null,"Key":"TreatmentInformationPatientName","HasInfo":false,"InfoText":null,"Config":null,"ColumnWidth":6,"HasToggleButton":false},{"Value":null,"PartName":"TreatmentInformation","Placeholder":null,"Component":"app-radio","ComponentVariants":null,"IsDisabled":false,"IsVisible":true,"LabelKey":"treatment_outcome","Label":"Treatment Outcome*","Name":"TreatmentOutcome","IsHierarchySelector":false,"IsRequired":true,"RemoteUrl":null,"Type":null,"AddOn":null,"Options":null,"HierarchyOptions":null,"OptionsWithKeyValue":[{"Key":"CURED","Value":"Cured"},{"Key":"TREATMENT_FAILURE","Value":"Treatment Failure"},{"Key":"TREATMENT_COMPLETE","Value":"Treatment Complete"},{"Key":"NOT_EVALUATED","Value":"Not Evaluated"},{"Key":"LOST_TO_FOLLOW_UP","Value":"Lost To Follow Up"},{"Key":"TREATMENT_REGIMEN_CHANGED","Value":"Treatment Regimen Changed"},{"Key":"OPTED_OUT","Value":"Opted Out"},{"Key":"TRANSFERRED_OUT","Value":"Transferred Out"},{"Key":"DIED","Value":"Died"},{"Key":"OTHER","Value":"Other"}],"AdditionalInfo":null,"DefaultVisibilty":false,"Order":0,"OptionsWithLabel":null,"Validations":{"Or":null,"And":[{"Type":"Required","Max":0,"Min":0,"IsBackendValidation":false,"ValidationUrl":null,"ShowServerError":false,"ErrorTargetField":null,"ValidationParams":null,"ErrorMessage":"This field is required","ErrorMessageKey":"error_field_required","RequiredOnlyWhen":null,"Regex":null}]},"ResponseDataPath":null,"OptionDisplayKeys":null,"OptionValueKey":null,"LoadImmediately":false,"HierarchySelectionConfigs":null,"DisabledDateConfig":null,"RemoteUpdateConfig":null,"RowNumber":3,"ColumnNumber":null,"Id":11155,"ParentType":"PART","ParentId":61,"FieldOptions":"[{\\"Key\\":\\"CURED\\", \\"Value\\":\\"_cured\\"}, {\\"Key\\":\\"TREATMENT_FAILURE\\", \\"Value\\":\\"_treatment_failure\\"}, {\\"Key\\":\\"TREATMENT_COMPLETE\\", \\"Value\\":\\"_treatment_complete\\"}, {\\"Key\\":\\"NOT_EVALUATED\\", \\"Value\\":\\"_not_evaluated\\"},{\\"Key\\":\\"LOST_TO_FOLLOW_UP\\", \\"Value\\":\\"_lost_to_follow_up\\"},{\\"Key\\":\\"TREATMENT_REGIMEN_CHANGED\\", \\"Value\\":\\"_treatment_regimen_changed\\"},{\\"Key\\":\\"OPTED_OUT\\", \\"Value\\":\\"_opted_out\\"},{\\"Key\\":\\"TRANSFERRED_OUT\\", \\"Value\\":\\"_transferred_out\\"},{\\"Key\\":\\"DIED\\", \\"Value\\":\\"_died\\"},{\\"Key\\":\\"OTHER\\", \\"Value\\":\\"_other\\"}]","ValidationList":null,"DefaultValue":null,"Key":"TreatmentInformationTreatmentOutcome","HasInfo":false,"InfoText":null,"Config":null,"ColumnWidth":6,"HasToggleButton":false},{"Value":null,"PartName":"TreatmentInformation","Placeholder":null,"Component":"app-textarea","ComponentVariants":null,"IsDisabled":false,"IsVisible":true,"LabelKey":"note_text","Label":"Add a Note","Name":"NoteText","IsHierarchySelector":false,"IsRequired":false,"RemoteUrl":null,"Type":null,"AddOn":null,"Options":null,"HierarchyOptions":null,"OptionsWithKeyValue":null,"AdditionalInfo":null,"DefaultVisibilty":false,"Order":0,"OptionsWithLabel":null,"Validations":{"Or":null,"And":[]},"ResponseDataPath":null,"OptionDisplayKeys":null,"OptionValueKey":null,"LoadImmediately":false,"HierarchySelectionConfigs":null,"DisabledDateConfig":null,"RemoteUpdateConfig":null,"RowNumber":4,"ColumnNumber":null,"Id":11156,"ParentType":"PART","ParentId":61,"FieldOptions":null,"ValidationList":null,"DefaultValue":null,"Key":"TreatmentInformationNoteText","HasInfo":false,"InfoText":null,"Config":null,"ColumnWidth":6,"HasToggleButton":false},{"Value":null,"PartName":"TreatmentInformation","Placeholder":null,"Component":"app-datepicker","ComponentVariants":null,"IsDisabled":false,"IsVisible":true,"LabelKey":"end_date","Label":"End Date","Name":"EndDate","IsHierarchySelector":false,"IsRequired":true,"RemoteUrl":null,"Type":null,"AddOn":null,"Options":null,"HierarchyOptions":null,"OptionsWithKeyValue":null,"AdditionalInfo":null,"DefaultVisibilty":false,"Order":0,"OptionsWithLabel":null,"Validations":{"Or":null,"And":[{"Type":"Required","Max":0,"Min":0,"IsBackendValidation":false,"ValidationUrl":null,"ShowServerError":false,"ErrorTargetField":null,"ValidationParams":null,"ErrorMessage":"This field is required","ErrorMessageKey":"error_field_required","RequiredOnlyWhen":null,"Regex":null}]},"ResponseDataPath":null,"OptionDisplayKeys":null,"OptionValueKey":null,"LoadImmediately":false,"HierarchySelectionConfigs":null,"DisabledDateConfig":{"To":"2022-03-01T00:00:00","From":"2022-03-21T11:16:02.0309179+05:30","Days":null,"DaysOfMonth":null,"Dates":null,"Ranges":null,"AddDaysFromToday":0,"SubtractDaysFromToday":180},"RemoteUpdateConfig":null,"RowNumber":6,"ColumnNumber":null,"Id":11158,"ParentType":"PART","ParentId":61,"FieldOptions":null,"ValidationList":null,"DefaultValue":null,"Key":"TreatmentInformationEndDate","HasInfo":false,"InfoText":null,"Config":null,"ColumnWidth":6,"HasToggleButton":false},{"Value":null,"PartName":"TreatmentInformation","Placeholder":null,"Component":"app-datepicker","ComponentVariants":null,"IsDisabled":false,"IsVisible":false,"LabelKey":"","Label":null,"Name":"StartDate","IsHierarchySelector":false,"IsRequired":false,"RemoteUrl":null,"Type":null,"AddOn":null,"Options":null,"HierarchyOptions":null,"OptionsWithKeyValue":null,"AdditionalInfo":null,"DefaultVisibilty":false,"Order":0,"OptionsWithLabel":null,"Validations":{"Or":null,"And":[]},"ResponseDataPath":null,"OptionDisplayKeys":null,"OptionValueKey":null,"LoadImmediately":false,"HierarchySelectionConfigs":null,"DisabledDateConfig":null,"RemoteUpdateConfig":null,"RowNumber":null,"ColumnNumber":null,"Id":11159,"ParentType":"PART","ParentId":61,"FieldOptions":null,"ValidationList":null,"DefaultValue":null,"Key":"TreatmentInformationStartDate","HasInfo":false,"InfoText":null,"Config":null,"ColumnWidth":6,"HasToggleButton":false}],"Triggers":null,"TriggerConfigs":null,"ValueDependencies":[],"FilterDependencies":[],"VisibilityDependencies":[],"DateConstraintDependencies":[{"Lookups":[{"FormPart":"TreatmentInformation","Field":"StartDate","ExpectedValue":null,"ExpectedValues":null,"IsObject":false,"ObjectKeyAndValues":null,"ObjectKeyAndValue":null,"AttributeName":null,"ConstraintName":"To","RestrictedValue":null,"ComparisonOperator":"EQ","AnyObjectKeyValue":false,"FilteredKeys":null,"LookupFormPart":"TreatmentInformation","LookupField":"StartDate"}],"FormPart":"TreatmentInformation","Field":"EndDate","FormPartId":null,"FieldId":null,"Type":"DATE_CONSTRAINT","IsForm":false,"PropertyAndValues":null}],"IsRequiredDependencies":[{"Lookups":[{"FormPart":"TreatmentInformation","Field":"TreatmentOutcome","ExpectedValue":null,"ExpectedValues":["OTHER"],"IsObject":false,"ObjectKeyAndValues":null,"ObjectKeyAndValue":null,"AttributeName":null,"ConstraintName":null,"RestrictedValue":null,"ComparisonOperator":"EQ","AnyObjectKeyValue":false,"FilteredKeys":null,"LookupFormPart":"TreatmentInformation","LookupField":"TreatmentOutcome"}],"FormPart":"TreatmentInformation","Field":"NoteText","FormPartId":null,"FieldId":null,"Type":"IS_REQUIRED","IsForm":false,"PropertyAndValues":null}],"RemoteUpdateConfigs":[],"ValuePropertyDependencies":[],"CompoundValueDependencies":null,"SaveEndpoint":"/API/Patients/CloseCase","SaveText":"Close case","SaveTextKey":"close_case"},"ExistingData":{"PatientId":' + patientId + ',"EndDate":null,"PatientName":"' + patientName + '","StartDate":"2022-03-01T00:00:00"}},"Error":null}')
                )
              ),
              save: async () => {
                return Promise.resolve({
                  Success: true,
                  Error: null,
                  Data: 'Success'
                })
              }
            }),
            mutations: FormStore.mutations,
            namespaced: true
          }
        }
      }),
      localVue,
      mocks: {
        $t: mockTranslate
      }
    })

    await wrapper.findComponent(Button).trigger('click')

    await flushPromises()

    await wrapper.findComponent(Form).findComponent(RadioGroup).findAll('input').at(0).trigger('click')

    await wrapper.findAllComponents(Button).at(1).trigger('click')

    await flushPromises()
  })

  it('submit reopen case', async () => {
    const treatmentOutcome = 'Cured'
    const availableMerms = [
      '123',
      '456',
      '789'
    ]

    const wrapper = mount(CloseCase, {
      store: new Vuex.Store({
        modules: {
          Patient: {
            namespaced: true,
            state: {
              patientId: 1234,
              stage: 'OFF_TREATMENT',
              endDate: '20-01-2022',
              treatmentOutcome,
              availableMerms,
              monitoringMethod: 'MERM'
            },
            actions: {
              reOpenCase: async ({ commit }, payload) => {
                return 'Patient brought back on treatment'
              }
            }
          }
        }
      }),
      localVue,
      mocks: {
        $t: mockTranslate
      }
    })

    await wrapper.findComponent(Button).trigger('click')

    await flushPromises()

    await wrapper.findComponent(Select).findAll('ul li').at(0).trigger('click')

    await flushPromises()

    await wrapper.findAllComponents(Button).at(0).trigger('click')

    await flushPromises()
  })

  it('submit reopen case without imei', async () => {
    const treatmentOutcome = 'Cured'
    const availableMerms = [
      '123',
      '456',
      '789'
    ]

    const wrapper = mount(CloseCase, {
      store: new Vuex.Store({
        modules: {
          Patient: {
            namespaced: true,
            state: {
              patientId: 1234,
              stage: 'OFF_TREATMENT',
              endDate: '20-01-2022',
              treatmentOutcome,
              availableMerms
            },
            actions: {
              reOpenCase: async ({ commit }, payload) => {
                return 'Patient brought back on treatment'
              }
            }
          }
        }
      }),
      localVue,
      mocks: {
        $t: mockTranslate
      }
    })

    await wrapper.findComponent(Button).trigger('click')

    await flushPromises()

    await wrapper.findAllComponents(Button).at(0).trigger('click')

    await flushPromises()
  })

  it('reopen case failed', async () => {
    const treatmentOutcome = 'Cured'
    const availableMerms = [
      '123',
      '456',
      '789'
    ]

    const wrapper = mount(CloseCase, {
      store: new Vuex.Store({
        modules: {
          Patient: {
            namespaced: true,
            state: {
              patientId: 1234,
              stage: 'OFF_TREATMENT',
              endDate: '20-01-2022',
              treatmentOutcome,
              availableMerms
            },
            actions: {
              reOpenCase: async ({ commit }, payload) => {
                return ''
              }
            }
          }
        }
      }),
      localVue,
      mocks: {
        $t: mockTranslate
      }
    })

    await wrapper.findComponent(Button).trigger('click')

    await flushPromises()

    await wrapper.findAllComponents(Button).at(0).trigger('click')

    await flushPromises()
  })
})
