import * as helper from '@/utils/toastUtils.js'

describe('Toast utils tests', () => {
  it('default toast test', () => {
    const spy = jest.spyOn(helper, 'createToast')
    spy.mockReturnValue(true)
    helper.defaultToast(helper.ToastType.Success, 'xyz')
    expect(spy.mock.calls).toHaveLength(1)
    var calledWithData = spy.mock.calls[0]
    expect(calledWithData[0]).toEqual('xyz')
    expect(calledWithData[2].type).toEqual('success')// will always be success
    expect(calledWithData[2].duration).toEqual(5000)
    expect(calledWithData[2].theme).toEqual('toasted-primary')
    expect(calledWithData[2].className).toEqual('toastSuccess text-ibm-plex-sans')
    spy.mockRestore()
  })

  it('default toast with overridden propertirs test', () => {
    const spy = jest.spyOn(helper, 'createToast')
    spy.mockReturnValue(true)
    helper.defaultToast(helper.ToastType.Error, 'xyz', 4000)
    expect(spy.mock.calls).toHaveLength(1)
    var calledWithData = spy.mock.calls[0]
    expect(calledWithData[0]).toEqual('xyz')
    expect(calledWithData[2].type).toEqual('success')// will always be success
    expect(calledWithData[2].duration).toEqual(4000)
    expect(calledWithData[2].theme).toEqual('toasted-primary')
    expect(calledWithData[2].className).toEqual('toastError text-ibm-plex-sans')
    spy.mockRestore()
  })

  it('toastWithActionButton test', () => {
    const spy = jest.spyOn(helper, 'createToast')
    spy.mockReturnValue(true)
    helper.toastWithActionButton(helper.ToastType.Success, 'xyz', 'yay', () => {})
    expect(spy.mock.calls).toHaveLength(1)
    var calledWithData = spy.mock.calls[0]
    expect(calledWithData[0]).toEqual('xyz')
    expect(calledWithData[1].text).toEqual('yay')
    expect(calledWithData[1].class).toEqual('toastBtnWhite')
    expect(calledWithData[2].type).toEqual('success')// will always be success
    expect(calledWithData[2].duration).toEqual(5000)
    expect(calledWithData[2].theme).toEqual('toasted-primary')
    expect(calledWithData[2].className).toEqual('toastSuccess text-ibm-plex-sans')
    spy.mockRestore()
  })
})
