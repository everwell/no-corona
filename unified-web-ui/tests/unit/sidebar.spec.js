import { mount, createLocalVue } from '@vue/test-utils'
import Sidebar from '@/app/shared/components/Sidebar.vue'
import Vuex from 'vuex'

const getLocalVue = () => {
  const localVue = createLocalVue()
  localVue.use(Vuex)
  return  localVue
}
const localVue = createLocalVue()

localVue.use(Vuex)
const getStore = () => {
  return new Vuex.Store({
  })
}

describe('Sidebar.vue', () => {
  it('renders props.list.name when passed', () => {
    const list = [
      {
        name: 'TestMenuItem',
        icon: 'tachometer-alt',
        link: '/dashboard/TestMenuItem'
      }]
      const url = "http://dummy.com";
      Object.defineProperty(window, 'location', {
          writable: true,
          value: {
          href: url,
          pathname: '/dashboard/TestMenuItem/'
          }
      })
    expect(window.location.href).toEqual(url)
    const wrapper = mount(Sidebar, {
      store:getStore(),
      localVue,
      propsData: { list },
      mocks: {
        $t: (key) => key
      }
    })
    expect(wrapper.text()).toMatch(list[0].name)
  })

  it('no item gets selected by default if selectedMenuItemName not supplied', () => {
    const list = [
      {
        name: 'TestMenuItem',
        icon: 'tachometer-alt',
        link: '/dashboard/TestMenuItem'
      },
      {
        name: 'TestMenuItem',
        icon: 'tachometer-alt'
      }]
    const wrapper = mount(Sidebar, {
      store:getStore(),
      localVue,
      propsData: { list },
      mocks: {
        $t: (key) => key
      }
    })

    expect(wrapper.findAll('.selected')).toHaveLength(0)
  })

  it('item gets selected and selected gets changed on click', async () => {
    const list = [
      {
        name: 'TestMenuItem',
        icon: 'tachometer-alt',
        link: '/dashboard/TestMenuItem'
      },
      {
        name: 'TestMenuItem1',
        icon: 'tachometer-alt'
      }]
    const wrapper = mount(Sidebar, {
      store:getStore(),
      localVue,
      propsData: { list },
      mocks: {
        $t: (key) => key
      }
    })

    window.open = jest.fn()

    await wrapper.findAll('.sidebar-item').at(0).trigger('click')
    expect(wrapper.findAll('.sidebar-item').at(0).classes('selected')).toBeTruthy()
    await wrapper.findAll('.sidebar-item').at(1).trigger('click')
    expect(wrapper.findAll('.sidebar-item').at(1).classes('selected')).toBeTruthy()

    expect(window.open).toHaveBeenCalledTimes(2)
  })

  it('shows children when arrow is clicked', async () => {
    const list = [
      {
        name: 'TestMenuItem',
        icon: 'tachometer-alt',
        link: '/dashboard/TestMenuItem',
        children: [
          {
            name: 'TestChildren'
          }
        ]
      },
      {
        name: 'TestMenuItem',
        icon: 'tachometer-alt'
      }]
    const wrapper = mount(Sidebar, {
      store:getStore(),
      localVue,
      propsData: { list },
      mocks: {
        $t: (key) => key
      }
    })

    await wrapper.findAll('.open-children-arrow').at(0).trigger('click')
    expect(wrapper.findAll('.sidebar-name').filter(item => {
      return item.text() === list[0].children[0].name
    })).toHaveLength(1)
  })

  it('checking router redirection', async () => {
    const list = [
      {
        name: 'TestMenuItem',
        icon: 'tachometer-alt',
        relativePath: true,
        link: '/dashboard/TestMenuItem'
      },
      {
        name: 'TestMenuItem',
        icon: 'tachometer-alt'
      }]
    const wrapper = mount(Sidebar, {
      store:getStore(),
      localVue,
      propsData: { list },
      mocks: {
        $router: {
          push: path => path
        },
        $t: (key) => key
      }
    })

    await wrapper.findAll('.sidebar-item').at(0).trigger('click')
    expect(wrapper.findAll('.sidebar-item').at(0).classes('selected')).toBeTruthy()
  })
})
