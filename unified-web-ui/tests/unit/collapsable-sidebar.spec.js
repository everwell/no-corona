import { mount, createLocalVue } from '@vue/test-utils'
import CollapsableSidebar from '@/app/shared/components/CollapsableSidebar.vue'
import Vuex from 'vuex'

const getLocalVue = () => {
  const localVue = createLocalVue()
  localVue.use(Vuex)
  return  localVue
}
const localVue = createLocalVue()

localVue.use(Vuex)
const getStore = () => {
  return new Vuex.Store({
  })
}

describe('CollapsableSidebar.vue', () => {
  it('toggle collapse state', async () => {
    const list = [
      {
        name: 'TestMenuItem',
        icon: 'tachometer-alt',
        link: '/dashboard/TestMenuItem'
      }]
    const url = "http://dummy.com";
    Object.defineProperty(window, 'location', {
        writable: true,
        value: {
        href: url,
        pathname: '/dashboard/TestMenuItem/'
        }
    })
    expect(window.location.href).toEqual(url)
    const wrapper = mount(CollapsableSidebar, {
      store:getStore(),
      localVue,
      propsData: { list },
      mocks: {
        $t: (key) => key
      }
    })
    expect(wrapper.find('.collapse-arrow').classes('collapsed')).toBeFalsy()
    await wrapper.find('.collapse-button').trigger('click')
    expect(wrapper.find('.collapse-arrow').classes('collapsed')).toBeTruthy()
  })

  it('collapse closes children', async () => {
    const list = [
      {
        name: 'TestMenuItem',
        icon: 'tachometer-alt',
        link: '/dashboard/TestMenuItem',
        children: [
          {
            name: 'TestChildren'
          }
        ]
      }]
    const wrapper = mount(CollapsableSidebar, {
      store:getStore(),
      localVue,
      propsData: { list },
      mocks: {
        $t: (key) => key
      }
    })
    wrapper.findAll('.open-children-arrow').at(0).trigger('click') // open children
    await wrapper.find('.collapse-button').trigger('click') // collapse
    expect(wrapper.findAll('.sidebar-name').filter(item => {
      return item.text() === list[0].children[0].name
    })).toHaveLength(0)
  })

  it('expand to view children if clicked while in collapase state', async () => {
    const list = [
      {
        name: 'TestMenuItem',
        icon: 'tachometer-alt',
        link: '/dashboard/TestMenuItem',
        children: [
          {
            name: 'TestChildren'
          }
        ]
      }]
    const wrapper = mount(CollapsableSidebar, {
      store:getStore(),
      localVue,
      propsData: { list },
      mocks: {
        $t: (key) => key
      }
    })
    await wrapper.find('.collapse-button').trigger('click') // collapse
    await wrapper.find('.sidebar-item').trigger('click') // open children
    expect(wrapper.findAll('.sidebar-name').filter(item => {
      return item.text() === list[0].children[0].name
    })).toHaveLength(1)
  })
})
