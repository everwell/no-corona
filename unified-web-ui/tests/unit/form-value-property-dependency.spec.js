import { mount, shallowMount, createLocalVue } from '@vue/test-utils'
import Form from '@/app/shared/components/Form.vue'
import Vuex from 'vuex'
import FormStore from '@/app/shared/store/modules/form'
import DatePicker from '@/app/shared/components/DatePicker'
import Select from '@/app/shared/components/Select'
import flushPromises from 'flush-promises'
import Vue from 'vue'
import formData from './form-value-property-dependency-data-test'
import VerticalFormPart from '@/app/shared/components/VerticalFormPart'
const localVue = createLocalVue()

localVue.use(Vuex)
window.EventBus = new Vue()


jest.mock('../../src/utils/toastUtils', () => {
  return {
    __esModule: true,
    defaultToast: jest.fn(),
    ToastType: {
      Success: 'Success',
      Error: 'Error',
      Warning: 'Warning',
      Neutal: 'Neutal'
    }
  }
})

const getStore = (actions) => {
  const formattedActions = {
    ...FormStore.actions,
    ...actions
  }
  return new Vuex.Store({
    modules: {
      Form: {
        state: FormStore.state,
        actions: formattedActions,
        mutations: FormStore.mutations,
        namespaced: true
      }
    }
  })
}
describe('Form.vue', () => {
  it('checks if dependent adherence technology changes then TBTreatmentStartDate  reverts to orignal value', async () => {
    const wrapper = mount(Form, { 
      store: getStore({
        getFormData: async () => (
          Promise.resolve(
            formData
          )
        )
      }),
      localVue,
      propsData: { isEditing: true, name: 'BasicDetails' },
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises()
    wrapper.findComponent(VerticalFormPart).findComponent(DatePicker).setData({ localValue :{year :2021, month: 4, date:1} })
    await wrapper.findComponent(VerticalFormPart).findComponent(DatePicker).trigger('input',Date('2021-05-01T00:00:00'))
    await wrapper.findComponent(VerticalFormPart).findComponent(DatePicker).trigger('change',Date('2021-05-01T00:00:00'))
    await wrapper.findComponent(VerticalFormPart).findComponent(DatePicker).vm.$emit('change')
    expect(wrapper.findComponent(DatePicker).find('input').element.value).toBe('1 May 2021')
    wrapper.findComponent(Select).setData({localValue: {Key:'VOT',Value:'VOT'}})
    await wrapper.findComponent(Select).trigger('input', {Key:'VOT',Value:'VOT'})
    await wrapper.findComponent(Select).trigger('change', {Key:'VOT',Value:'VOT'})
    await wrapper.findComponent(Select).vm.$emit('change')
    expect(wrapper.findComponent(Select).find('input').element.value).toBe('VOT')
    expect(wrapper.findComponent(DatePicker).find('input').element.value).toContain('1 May 2019')   
  })
})


