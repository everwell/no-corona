import { mount, createLocalVue } from '@vue/test-utils'
import UnifiedPatient from '../../src/app/Pages/dashboard/UnifiedPatient/UnifiedPatient'
import HubFilterOptions from '../../src/app/Pages/dashboard/UnifiedPatient/components/HubFilterOptions'
import Select from '@/app/shared/components/Select.vue'
import DatePicker from '@/app/shared/components/DatePicker.vue'
import Button from '@/app/shared/components/Button.vue'
import Vuex from 'vuex'
import { List, Map } from 'immutable'

const localVue = createLocalVue()
localVue.use(Vuex)
const $t = () => {}
const optionsData = {
  HierarchyOptions:
[[{ Id: 33710, Name: 'Uganda', Parent: null, Type: 'COUNTRY' }],
  [{ Key: 33719, Id: 33719, Name: 'betaa', Type: 'DISTRICT', Parent: 33710 },
    { Key: 266713, Id: 266713, Name: 'Bugiri', Type: 'DISTRICT', Parent: 33710 }],
  [{ Key: 266714, Id: 266714, Name: 'Bugiri HOSPITAL', Type: 'FACILITY', Parent: 266713 },
    { Key: 266917, Id: 266917, Name: 'C test', Type: 'FACILITY', Parent: 33719 },
    { Key: 266918, Id: 266918, Name: 'dd', Type: 'FACILITY', Parent: 33719 },
    { Key: 33720, Id: 33720, Name: 'Facility C1', Type: 'FACILITY', Parent: 33719 }],
  [{ Key: 33722, Id: 33722, Name: 'Test', Type: 'TU', Parent: 266917 }],
  [{ Key: 33725, Id: 33725, Name: 'Test PHI', Type: 'PHI', Parent: 33722 }]],
  StageOptions: [{ value: 'ON_TREATMENT', label: 'on_treatment', hasCalendar: 'True' },
    { value: 'OFF_TREATMENT', label: 'outcome_assigned', hasCalendar: 'False' }],
  TechnologyOptions: [{ Key: '99DOTS', Value: '_99dots', AdherenceTechOptions: '99DOTS' }],
  EarliestEnrollment: '2019-05-01T00:00:00'
}

describe('Hub Filter Options Tests', () => {
  let store
  let actions
  beforeEach(() => {
    actions = {
      loadOptions: jest.fn(),
      setCalendarView: jest.fn(),
      updateClientName: jest.fn(),
      getPatients: jest.fn(),
      setSelectedFilters: jest.fn(),
      setStage: jest.fn(),
      loadHierarchyLevelOptions: jest.fn(),
      setCurrentPage: jest.fn()
    }
    store = new Vuex.Store({
      modules: {
        UnifiedPatient: {
          namespaced: true,
          state: {
            client: null,
            deploymentCode: 'DEFAULT',
            isOptionsLoading: true,
            isPatientsLoading: false,
            isCalendar: false,
            firstHierarchies: [],
            secondHierarchies: [],
            thirdHierarchies: [],
            fourthHierarchies: [],
            fifthHierarchies: [],
            PHIPatientTypeOptions: List(),
            PHIPatientType: '',
            stageOptions: List(),
            patientTypes: List(),
            caseTypes: List(),
            techOptions: List(),
            dateOptions: List(),
            patients: Map(),
            columns: List(),
            calendarModel: Map(),
            pageSize: 20,
            selectedPage: 1,
            stage: '',
            numberPages: 0,
            totalPatients: 0,
            currentMonth: new Date().getMonth(),
            currentYear: new Date().getFullYear(),
            filters: null,
            earliestEnrollment: new Date('2017-05-10')
          },
          actions
        }
      }
    })
  })

  it('Page Loading', async () => {
    store.state.UnifiedPatient.client = 'NNDOTS'
    store.state.UnifiedPatient.isOptionsLoading = false
    store.state.UnifiedPatient.stage = { Key: 'ON_TREATMENT', Value: 'on_treatment' }
    store.state.UnifiedPatient.isCalendar = false
    store.state.UnifiedPatient.firstHierarchies = optionsData.HierarchyOptions[0]
    store.state.UnifiedPatient.secondHierarchies = optionsData.HierarchyOptions[1]
    store.state.UnifiedPatient.thirdHierarchies = optionsData.HierarchyOptions[2]
    store.state.UnifiedPatient.fourthHierarchies = optionsData.HierarchyOptions[3]
    store.state.UnifiedPatient.fifthHierarchies = optionsData.HierarchyOptions[4]
    store.state.UnifiedPatient.stageOptions = optionsData.StageOptions
    store.state.UnifiedPatient.TechnologyOptions = optionsData.TechnologyOptions
    store.state.UnifiedPatient.EarliestEnrollment = new Date(optionsData.EarliestEnrollment)
    store.state.UnifiedPatient.totalPatients = 1
    store.state.UnifiedPatient.patients = [{ Id: 314390, PatientName: 'sadawd ', TBNumber: '4353', Facility: 'DEMO_TU_2', MonitoringMethod: 'EvriMED(MERM)', Phone: '3232424234', EnrollmentDate: '04-01-2022', TBTreatmentStartDate: '04-01-2022', EndDate: '21-06-2022', TotalTreatmentDays: 168, MissedDoses: 16, DigitalDoses: '0%', DigitalManual: '0%', LastDosage: '04-01-2022', CurrentTags: '', TreatmentOutcome: null, AdherenceString: '6666666666666666', C_PatientTag: '', DeviceIMEI: '862927043270280', BatteryLevel: '0 %', AdherenceResults: [{ Item1: 'blank ', Item2: 'Saturday January 01-01-2022 00:00:00, 2022: No tags added' }, { Item1: 'blank ', Item2: 'Sunday January 02-01-2022 00:00:00, 2022: No tags added' }, { Item1: 'blank ', Item2: 'Monday January 03-01-2022 00:00:00, 2022: No tags added' }, { Item1: 'no_info ', Item2: 'Tuesday January 04-01-2022 00:00:00, 2022: No tags added' }, { Item1: 'no_info ', Item2: 'Wednesday January 05-01-2022 00:00:00, 2022: No tags added' }, { Item1: 'no_info ', Item2: 'Thursday January 06-01-2022 00:00:00, 2022: No tags added' }, { Item1: 'no_info ', Item2: 'Friday January 07-01-2022 00:00:00, 2022: No tags added' }, { Item1: 'no_info ', Item2: 'Saturday January 08-01-2022 00:00:00, 2022: No tags added' }, { Item1: 'no_info ', Item2: 'Sunday January 09-01-2022 00:00:00, 2022: No tags added' }, { Item1: 'no_info ', Item2: 'Monday January 10-01-2022 00:00:00, 2022: No tags added' }, { Item1: 'no_info ', Item2: 'Tuesday January 11-01-2022 00:00:00, 2022: No tags added' }, { Item1: 'no_info ', Item2: 'Wednesday January 12-01-2022 00:00:00, 2022: No tags added' }, { Item1: 'no_info ', Item2: 'Thursday January 13-01-2022 00:00:00, 2022: No tags added' }, { Item1: 'no_info ', Item2: 'Friday January 14-01-2022 00:00:00, 2022: No tags added' }, { Item1: 'no_info ', Item2: 'Saturday January 15-01-2022 00:00:00, 2022: No tags added' }, { Item1: 'no_info ', Item2: 'Sunday January 16-01-2022 00:00:00, 2022: No tags added' }, { Item1: 'no_info ', Item2: 'Monday January 17-01-2022 00:00:00, 2022: No tags added' }, { Item1: 'no_info ', Item2: 'Tuesday January 18-01-2022 00:00:00, 2022: No tags added' }, { Item1: 'no_info ', Item2: 'Wednesday January 19-01-2022 00:00:00, 2022: No tags added' }] }]
    const wrapper = mount(HubFilterOptions, { store, localVue, mocks: { $t: key => key } })
    await wrapper.findComponent(Select).findAll('.dropdown ul li').at(0).trigger('click')
    expect(wrapper.find('input').element.value).toBe('betaa')
    await wrapper.setData({ secondHierarchy: [{ Key: 33719, Id: 33719, Name: 'betaa', Type: 'DISTRICT', Parent: 33710 }] })
    await wrapper.findAllComponents(Select).at(1).findAll('.dropdown ul li').at(0).trigger('click')
    expect(wrapper.findComponent({ name: 'Select Third Hierarchy' }).exists()).toBe(true)
    await wrapper.setData({ thirdHierarchy: [{ Key: 266917, Id: 266917, Name: 'C test', Type: 'FACILITY', Parent: 33719 }] })
    await wrapper.findAllComponents(Select).at(2).findAll('.dropdown ul li').at(0).trigger('click')
    expect(wrapper.findComponent({ name: 'Select Fourth Hierarchy' }).exists()).toBe(true)
    await wrapper.setData({ fourthSelectedHierarchies: [{ Key: 33722, Id: 33722, Name: 'Test', Type: 'TU', Parent: 266917 }] })
    await wrapper.findAllComponents(Select).at(3).findAll('.dropdown ul li').at(0).trigger('click')
    expect(wrapper.findComponent({ name: 'Select Fifth Hierarchy' }).exists()).toBe(true)
    expect(wrapper.findComponent({ name: 'Stage' }).exists()).toBe(true)
    await wrapper.setData({ selectedStage: { Key: 'ON_TREATMENT', Value: 'on_treatment' } })
    await wrapper.findAllComponents(DatePicker).at(0).find('.input').trigger('click')
    const date = new Date()
    date.setMonth(date.getMonth() - 3)
    await wrapper.setData({ dateStart: date.toISOString().split('T')[0] })
    expect(new Date(wrapper.find('.input').element.value).getMonth()).toBe(date.getMonth())
    await wrapper.findAllComponents(DatePicker).at(1).find('.input').trigger('click')
    await wrapper.setData({ dateEnd: new Date().toISOString().split('T')[0] })
    expect(new Date(wrapper.findAll('.input').at(1).element.value).getMonth()).toBe(new Date().getMonth())
  })

  it('Stage selection toggle', async () => {
    const wrapper = mount(HubFilterOptions, { store, localVue, mocks: { $t } })
    await wrapper.setData({ selectedStage: { Key: 'ON_TREATMENT', Value: 'on_treatment' } })
    expect(wrapper.findComponent({ name: 'Select Tech' }).exists()).toBe(true)
    await wrapper.setData({ selectedStage: { Key: 'OFF_TREATMENT', Value: 'off_treatment' } })
    expect(wrapper.findComponent({ name: 'Select Tech' }).exists()).toBe(false)
  })

  it('Press select for second selected hierarchy', async () => {
    const wrapper = mount(HubFilterOptions, { store, localVue, mocks: { $t } })
    await wrapper.setData({ selectedTech: optionsData.TechnologyOptions })
    await wrapper.setData({ selectedStage: { Key: 'ON_TREATMENT', Value: 'on_treatment' } })
    await wrapper.setData({ secondHierarchy: [{ Key: 33719, Id: 33719, Name: 'betaa', Type: 'DISTRICT', Parent: 33710 }] })
    expect(wrapper.findComponent(Button).exists()).toBe(true)
    wrapper.findAllComponents(Button).at(1).vm.$emit('click')
  })

  it('Press select for third selected hierarchy', async () => {
    const wrapper = mount(HubFilterOptions, { store, localVue, mocks: { $t } })
    await wrapper.setData({ selectedTech: optionsData.TechnologyOptions })
    await wrapper.setData({ selectedStage: { Key: 'ON_TREATMENT', Value: 'on_treatment' } })
    expect(wrapper.findComponent(Button).exists()).toBe(true)
    await wrapper.setData({ thirdHierarchy: [{ Key: 266917, Id: 266917, Name: 'C test', Type: 'FACILITY', Parent: 33719 }] })
    wrapper.findAllComponents(Button).at(1).vm.$emit('click')
  })

  it('Press select for fourth selected hierarchy', async () => {
    const wrapper = mount(HubFilterOptions, { store, localVue, mocks: { $t } })
    await wrapper.setData({ selectedTech: optionsData.TechnologyOptions })
    await wrapper.setData({ selectedStage: { Key: 'ON_TREATMENT', Value: 'on_treatment' } })
    expect(wrapper.findComponent(Button).exists()).toBe(true)
    await wrapper.setData({ fourthSelectedHierarchies: [{ Key: 33722, Id: 33722, Name: 'Test', Type: 'TU', Parent: 266917 }] })
    wrapper.findAllComponents(Button).at(1).vm.$emit('click')
  })

  it('Press select for fifth selected hierarchy', async () => {
    const wrapper = mount(HubFilterOptions, { store, localVue, mocks: { $t } })
    await wrapper.setData({ selectedTech: optionsData.TechnologyOptions })
    await wrapper.setData({ selectedStage: { Key: 'ON_TREATMENT', Value: 'on_treatment' } })
    expect(wrapper.findComponent(Button).exists()).toBe(true)
    await wrapper.setData({ fifthSelectedHierarchies: [{ Key: 33725, Id: 33725, Name: 'Test PHI', Type: 'PHI', Parent: 33722 }] })
    wrapper.findAllComponents(Button).at(1).vm.$emit('click')
    wrapper.findAllComponents(Button).at(0).vm.$emit('click')
  })

  it('Press select for fourth selected hierarchy', async () => {
    const wrapper = mount(HubFilterOptions, { store, localVue, mocks: { $t } })
    await wrapper.setData({ selectedTech: optionsData.TechnologyOptions })
    await wrapper.setData({ selectedStage: { Key: 'ON_TREATMENT', Value: 'on_treatment' } })
    expect(wrapper.findComponent(Button).exists()).toBe(true)
    await wrapper.setData({ fourthSelectedHierarchies: [{ Key: 33722, Id: 33722, Name: 'Test', Type: 'TU', Parent: 266917 }] })
    wrapper.findComponent(Button).vm.$emit('click')
  })

  it('Press select for fifth selected hierarchy', async () => {
    const wrapper = mount(HubFilterOptions, { store, localVue, mocks: { $t } })
    await wrapper.setData({ selectedTech: optionsData.TechnologyOptions })
    await wrapper.setData({ selectedStage: { Key: 'ON_TREATMENT', Value: 'on_treatment' } })
    expect(wrapper.findComponent(Button).exists()).toBe(true)
    await wrapper.setData({ fifthSelectedHierarchies: [{ Key: 33725, Id: 33725, Name: 'Test PHI', Type: 'PHI', Parent: 33722 }] })
    wrapper.findComponent(Button).vm.$emit('click')
  })

  it('Toggle filters test', async () => {
    store.state.UnifiedPatient.client = 'NNDOTS'
    store.state.UnifiedPatient.isOptionsLoading = false
    store.state.UnifiedPatient.stage = { Key: 'ON_TREATMENT', Value: 'on_treatment' }
    store.state.UnifiedPatient.isCalendar = false
    store.state.UnifiedPatient.firstHierarchies = optionsData.HierarchyOptions[0]
    store.state.UnifiedPatient.stageOptions = optionsData.StageOptions
    store.state.UnifiedPatient.TechnologyOptions = optionsData.TechnologyOptions
    store.state.UnifiedPatient.EarliestEnrollment = new Date(optionsData.EarliestEnrollment)
    const wrapper = mount(HubFilterOptions, { store, localVue, mocks: { $t }, computed: {} })
    await wrapper.setData({ firstHierarchy: [{ Id: 33711, Name: 'Uganda', Parent: null, Type: 'COUNTRY' }] })
    await wrapper.setData({ selectedTech: optionsData.TechnologyOptions })
    await wrapper.setData({ selectedStage: { Key: 'ON_TREATMENT', Value: 'on_treatment' } })
    expect(wrapper.findComponent({ name: 'Select Third Hierarchy' }).exists()).toBe(false)
  })
})
