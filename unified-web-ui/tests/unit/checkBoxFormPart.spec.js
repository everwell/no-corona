import { createLocalVue, mount } from '@vue/test-utils'
import CheckBoxGroupFormPart from '@/app/shared/components/CheckboxGridFormPart'
import Vuex from 'vuex'
import FormStore from '@/app/shared/store/modules/form'
import flushPromises from 'flush-promises'
import Vue from 'vue'
import { List } from 'immutable'
import { createMapOf, createMapListOf } from '@/app/shared/utils/Objects'

const formWithOneInput = {
  Success: true,
  Data: {
    Form: {
      Name: 'SingleInputForm1',
      Title: '_dummy_input_form',
      Parts: [
        {
          FormName: 'SingleInputForm1',
          Name: 'PartName2',
          Title: '',
          TitleKey: 'PartName2',
          Order: 1,
          IsVisible: true,
          Id: 119,
          Type: 'checkbox-grid-form-part',
          Rows: null,
          Columns: null,
          RowDataName: 'PartName2',
          AllowRowOpen: false,
          AllowRowDelete: false,
          ListItemTitle: null,
          ItemDescriptionField: null,
          IsRepeatable: false,
          RecordId: null
        }
      ],
      PartOptions: null,
      Fields: [
        {
          Value: null,
          PartName: 'PartName2',
          Placeholder: null,
          Component: 'app-checkbox-group',
          IsDisabled: false,
          IsVisible: true,
          LabelKey: '_product_name',
          Label: 'Product Name',
          Name: 'FieldProductName',
          IsHierarchySelector: false,
          IsRequired: false,
          RemoteUrl: null,
          Type: null,
          AddOn: null,
          Options: null,
          HierarchyOptions: null,
          OptionsWithKeyValue: [
            {
              Value: '',
              Key: 'true'
            }
          ],
          AdditionalInfo: null,
          DefaultVisibilty: false,
          Order: 0,
          OptionsWithLabel: null,
          Validations: {
            Or: null,
            And: []
          },
          ResponseDataPath: null,
          OptionDisplayKeys: null,
          OptionValueKey: null,
          LoadImmediately: false,
          HierarchySelectionConfigs: null,
          DisabledDateConfig: null,
          RemoteUpdateConfig: null,
          RowNumber: null,
          ColumnNumber: 1,
          Id: 11206,
          ParentType: 'PART',
          ParentId: 71,
          FieldOptions: '[{"Value" : "", "Key" : "true"}]',
          ValidationList: null,
          DefaultValue: '',
          Key: 'PartName2FieldProductName0',
          HasInfo: false,
          InfoText: null,
          Config: null,
          ColumnWidth: 1,
          HasToggleButton: false
        },
        {
          Value: '',
          PartName: 'PartName2',
          Placeholder: null,
          Component: 'app-select',
          IsDisabled: false,
          IsVisible: true,
          LabelKey: '_return_quantity',
          Label: 'Return Quantity',
          Name: 'FieldReturnQuantity',
          IsHierarchySelector: false,
          IsRequired: true,
          RemoteUrl: null,
          Type: null,
          AddOn: null,
          Options: null,
          HierarchyOptions: null,
          OptionsWithKeyValue: [
            {
              value: '1',
              id: '1'
            },
            {
              value: '2',
              id: '2'
            },
            {
              value: '3',
              id: '3'
            }
          ],
          AdditionalInfo: null,
          DefaultVisibilty: false,
          Order: 0,
          OptionsWithLabel: null,
          Validations: {
            Or: null,
            And: []
          },
          ResponseDataPath: null,
          OptionDisplayKeys: null,
          OptionValueKey: null,
          LoadImmediately: false,
          HierarchySelectionConfigs: null,
          DisabledDateConfig: null,
          RemoteUpdateConfig: null,
          RowNumber: null,
          ColumnNumber: 2,
          Id: 11207,
          ParentType: 'PART',
          ParentId: 71,
          FieldOptions: null,
          ValidationList: null,
          DefaultValue: '',
          Key: 'PartName2FieldReturnQuantity0',
          HasInfo: false,
          InfoText: null,
          Config: null,
          ColumnWidth: 1,
          HasToggleButton: false
        },
        {
          Value: '',
          PartName: 'PartName2',
          Placeholder: null,
          Component: 'app-select',
          IsDisabled: false,
          IsVisible: false,
          LabelKey: '_return_quantity',
          Label: 'Return Quantity',
          Name: 'FieldReturnQuantity2',
          IsHierarchySelector: false,
          IsRequired: true,
          RemoteUrl: null,
          Type: null,
          AddOn: null,
          Options: null,
          HierarchyOptions: null,
          OptionsWithKeyValue: [
            {
              value: '1',
              id: '1'
            },
            {
              value: '2',
              id: '2'
            },
            {
              value: '3',
              id: '3'
            }
          ],
          AdditionalInfo: null,
          DefaultVisibilty: false,
          Order: 0,
          OptionsWithLabel: null,
          Validations: {
            Or: null,
            And: []
          },
          ResponseDataPath: null,
          OptionDisplayKeys: null,
          OptionValueKey: null,
          LoadImmediately: false,
          HierarchySelectionConfigs: null,
          DisabledDateConfig: null,
          RemoteUpdateConfig: null,
          RowNumber: null,
          ColumnNumber: 2,
          Id: 11207,
          ParentType: 'PART',
          ParentId: 71,
          FieldOptions: null,
          ValidationList: null,
          DefaultValue: '',
          Key: 'PartName2FieldReturnQuantity20',
          HasInfo: false,
          InfoText: null,
          Config: null,
          ColumnWidth: 1,
          HasToggleButton: false
        }
      ],
      Triggers: null,
      TriggerConfigs: null,
      ValueDependencies: [],
      FilterDependencies: [],
      VisibilityDependencies: [],
      DateConstraintDependencies: [],
      IsRequiredDependencies: [],
      RemoteUpdateConfigs: [],
      ValuePropertyDependencies: [],
      CompoundValueDependencies: null,
      SaveEndpoint: '/api/patients',
      SaveText: null,
      SaveTextKey: '_submit'
    },
    ExistingData: {}
  },
  Error: null
}

const localVue = createLocalVue()

localVue.use(Vuex)
window.EventBus = new Vue()

jest.mock('../../src/utils/toastUtils', () => {
  return {
    __esModule: true,
    defaultToast: jest.fn(),
    ToastType: {
      Success: 'Success',
      Error: 'Error',
      Warning: 'Warning',
      Neutal: 'Neutal'
    }
  }
})

const getStore = (actions, state) => {
  const formattedActions = {
    ...FormStore.actions,
    ...actions
  }
  const formattedState = {
    ...FormStore.state,
    ...state
  }
  return new Vuex.Store({
    modules: {
      Form: {
        state: formattedState,
        actions: formattedActions,
        mutations: FormStore.mutations,
        namespaced: true
      }
    }
  })
}

const getNewForm = () => {
  return JSON.parse(JSON.stringify(formWithOneInput))
}
const tableFormPartFieldLabels = [{
  FieldProductName: 'Test',
  FieldReturnQuantity: null,
  FieldReturnQuantity2: null
}]

const localValue = {
  rows: tableFormPartFieldLabels
}

describe('CheckboxGrid Form Part Tests', () => {
  it('renders props.headingText when passed', async () => {
    const formData = getNewForm()
    const heading = formData.Data.Form.Parts[0].Name
    const wrapper = mount(CheckBoxGroupFormPart, {
      store: getStore({}, {
        fieldsMappedByFormPartName: { get: (_) => null, has: (_) => true },
        allFields: List(formData.Data.Form.Fields)
      }),
      localVue,
      propsData: { headingText: heading },
      data () {
        return {
          localValue: localValue
        }
      },
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises()
    expect(wrapper.findComponent(CheckBoxGroupFormPart).find('#checkbox-grid-part-heading').text()).toBe(heading)
  })

  it('making the checkgridbox form part', async () => {
    const formData = getNewForm()
    const heading = formData.Data.Form.Parts[0].Name
    const wrapper = mount(CheckBoxGroupFormPart, {
      store: getStore({}, {
        fieldsMappedByFormPartName: createMapListOf(
          'PartName', formData.Data.Form.Fields),
        allFields: formData.Data.Form.Fields,
        fieldsMappedByName: createMapOf('Key', formData.Data.Form.Fields)
        // fieldsMappedByName:  { get: (_) => formData.Data.Form.Fields[0], has: (_) => true }
      }),
      localVue,
      propsData: { headingText: heading, name: 'PartName2' },
      data () {
        return {
          localValue: localValue
        }
      },
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises()
    const checkBoxInput = wrapper.find('input[type="checkbox"]')
    checkBoxInput.element.checked = true
    checkBoxInput.trigger('change')
    await wrapper.vm.$nextTick()
    expect(checkBoxInput.element.checked).toBeTruthy()
    expect(wrapper.find('#checkbox-grid-part-heading').exists()).toBe(true)
    expect(wrapper.find('#checkbox-group-component').exists()).toBe(true)
    expect(wrapper.find('#field-component').exists()).toBe(true)
  })

  it('making the checkgridbox form part with undefined localValue', async () => {
    const formData = getNewForm()
    const heading = formData.Data.Form.Parts[0].Name
    const wrapper = mount(CheckBoxGroupFormPart, {
      store: getStore({}, {
        fieldsMappedByFormPartName: { get: (_) => List(formData.Data.Form.Fields), has: (_) => true },
        allFields: List(formData.Data.Form.Fields)
      }),
      localVue,
      propsData: { headingText: heading, name: 'PartName2' },
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises()
    expect(wrapper.find('#checkbox-group-component').exists()).toBe(false)
    expect(wrapper.find('#field-component').exists()).toBe(false)
  })
})
