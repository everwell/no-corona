import {
  createLocalVue,
  shallowMount,
  mount
} from '@vue/test-utils'
import pageTurner from '@/app/shared/components/PageTurner.vue'
const number = 3
describe('PageTurner unit test', () => {
  test('is a Vue instance', () => {
    const wrapper = mount(pageTurner)
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  test('Rendering props', () => {
    const wrapper = mount(pageTurner, {
      propsData: {
        pageSize: 5,
        totalItems: 10,
        currentPage: 3
      }
    })
    expect(wrapper.vm.paginationStartFrom).toBe(1)
    expect(wrapper.vm.paginationEnd).toBe(2)
    expect(wrapper.vm.totalPages).toBe(2)
    expect(wrapper.props().totalItems).toBe(10)
    expect(wrapper.props().currentPage).toBe(3)
  })

  test('emit event test', async () => {
    const wrapper = mount(pageTurner, {
      propsData: {
        pageSize: 5,
        totalItems: 10,
        currentPage: 3
      }
    })
    wrapper.vm.$emit('changePageEvent')
    wrapper.vm.$emit('changePageEvent', 3)
    await wrapper.vm.$nextTick()
    expect(wrapper.emitted().changePageEvent).toBeTruthy()
    expect(wrapper.emitted().changePageEvent.length).toBe(2)
    expect(wrapper.emitted().changePageEvent[1][0]).toEqual(3)
  })

  test('methods', async () => {
    const localVue = createLocalVue()
    const wrapper = shallowMount(pageTurner, {
      localVue,
      propsData: {
        pageSize: 5,
        totalItems: 10,
        currentPage: 3
      }
    })
    console.log = jest.fn()
    wrapper.vm.changePage(number)
    expect(wrapper.vm.selectedPage).toBe(3)
    expect(wrapper.props().pageSize).toBe(5)
  })
})
