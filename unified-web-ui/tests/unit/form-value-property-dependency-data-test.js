const formData = {
    "Success": true,
    "Data": {
        "Form": {
            "Name": "RegistrationForm",
            "Title": "Registration Form",
            "Parts": [
                {
                    "FormName": "RegistrationForm",
                    "Name": "TreatmentDetails",
                    "Title": "Treatment Details",
                    "TitleKey": "Treatment Details",
                    "Order": 4,
                    "IsVisible": true,
                    "Id": 0,
                    "Type": "vertical-form-part",
                    "Rows": null,
                    "Columns": null,
                    "RowDataName": null,
                    "AllowRowOpen": false,
                    "AllowRowDelete": false,
                    "ListItemTitle": null,
                    "ItemDescriptionField": null,
                    "IsRepeatable": false,
                    "RecordId": null
                }
            ],
            "PartOptions": null,
            "Fields": [
                {
                    "Value": null,
                    "PartName": "TreatmentDetails",
                    "Placeholder": null,
                    "Component": "app-datepicker",
                    "IsDisabled": false,
                    "IsVisible": true,
                    "LabelKey": "TBTreatmentStartDate",
                    "Label": "Date on which patient started treatment (Can be same or before adherence technology start date)",
                    "Name": "TBTreatmentStartDate",
                    "IsHierarchySelector": false,
                    "IsRequired": false,
                    "RemoteUrl": null,
                    "Type": null,
                    "AddOn": null,
                    "Options": null,
                    "HierarchyOptions": null,
                    "OptionsWithKeyValue": null,
                    "AdditionalInfo": null,
                    "DefaultVisibilty": false,
                    "Order": 1,
                    "OptionsWithLabel": null,
                    "Validations": {
                        "Or": null,
                        "And": [
                            {
                                "Type": "Required",
                                "Max": 0,
                                "Min": 0,
                                "IsBackendValidation": false,
                                "ValidationUrl": null,
                                "ShowServerError": false,
                                "ErrorTargetField": null,
                                "ValidationParams": null,
                                "ErrorMessage": "This field is required",
                                "ErrorMessageKey": "error_field_required",
                                "RequiredOnlyWhen": null,
                                "Regex": null
                            }
                        ]
                    },
                    "ResponseDataPath": null,
                    "OptionDisplayKeys": null,
                    "OptionValueKey": null,
                    "LoadImmediately": false,
                    "HierarchySelectionConfigs": null,
                    "DisabledDateConfig": {
                        "To": "2021-07-16T00:00:00+05:30",
                        "From": "2022-01-12T05:59:53.7070279",
                        "Days": null,
                        "DaysOfMonth": null,
                        "Dates": null,
                        "Ranges": null,
                        "AddDaysFromToday": null,
                        "SubtractDaysFromToday": null
                    },
                    "RemoteUpdateConfig": null,
                    "RowNumber": 1,
                    "ColumnNumber": null,
                    "Id": 0,
                    "ParentType": null,
                    "ParentId": null,
                    "FieldOptions": null,
                    "ValidationList": null,
                    "DefaultValue": null,
                    "Key": "TreatmentDetailsTBTreatmentStartDate",
                    "HasInfo": false,
                    "InfoText": null,
                    "Config": null,
                    "ColumnWidth": 6,
                    "HasToggleButton": false
                },
                {
                    "Value": null,
                    "PartName": "TreatmentDetails",
                    "Placeholder": null,
                    "Component": "app-select",
                    "IsDisabled": false,
                    "IsVisible": true,
                    "LabelKey": "adherence_technology",
                    "Label": "Adherence Technology",
                    "Name": "MonitoringMethod",
                    "IsHierarchySelector": false,
                    "IsRequired": true,
                    "RemoteUrl": null,
                    "Type": null,
                    "AddOn": null,
                    "Options": null,
                    "HierarchyOptions": null,
                    "OptionsWithKeyValue": [
                        {
                            "Key": "99DOTS",
                            "ValueKey": "_99DOTS",
                            "Value": "99DOTS",
                            "AdherenceTechOptions": "99DOTS"
                        },
                        {
                            "Key": "VOT",
                            "ValueKey": "_VOT",
                            "Value": "VOT",
                            "AdherenceTechOptions": "VOT"
                        },
                        {
                            "Key": "NONE",
                            "ValueKey": "FollowedWithoutTechnology",
                            "Value": "Followed up Without Technology",
                            "AdherenceTechOptions": "NONE"
                        }
                    ],
                    "AdditionalInfo": null,
                    "DefaultVisibilty": false,
                    "Order": 7,
                    "OptionsWithLabel": null,
                    "Validations": {
                        "Or": null,
                        "And": [
                            {
                                "Type": "Required",
                                "Max": 0,
                                "Min": 0,
                                "IsBackendValidation": false,
                                "ValidationUrl": null,
                                "ShowServerError": false,
                                "ErrorTargetField": null,
                                "ValidationParams": null,
                                "ErrorMessage": "This field is required",
                                "ErrorMessageKey": "error_field_required",
                                "RequiredOnlyWhen": null,
                                "Regex": null
                            }
                        ]
                    },
                    "ResponseDataPath": null,
                    "OptionDisplayKeys": null,
                    "OptionValueKey": null,
                    "LoadImmediately": false,
                    "HierarchySelectionConfigs": null,
                    "DisabledDateConfig": null,
                    "RemoteUpdateConfig": null,
                    "RowNumber": 7,
                    "ColumnNumber": null,
                    "Id": 0,
                    "ParentType": null,
                    "ParentId": null,
                    "FieldOptions": null,
                    "ValidationList": null,
                    "DefaultValue": null,
                    "Key": "TreatmentDetailsMonitoringMethod",
                    "HasInfo": false,
                    "InfoText": null,
                    "Config": null,
                    "ColumnWidth": 6,
                    "HasToggleButton": false
                }
            ],
            "Triggers": null,
            "TriggerConfigs": null,
            "ValueDependencies": [],
            "FilterDependencies": null,
            "VisibilityDependencies": [],
            "DateConstraintDependencies": [],
            "IsRequiredDependencies": [],
            "RemoteUpdateConfigs": [],
            "ValuePropertyDependencies": [
                {
                    "Lookups": null,
                    "FormPart": "TreatmentDetails",
                    "Field": "TBTreatmentStartDate",
                    "FormPartId": null,
                    "FieldId": null,
                    "Type": null,
                    "IsForm": false,
                    "PropertyAndValues": [
                        {
                            "TriggerOperator": null,
                            "Dependency": {
                                "FormPart": "TreatmentDetails",
                                "Field": "MonitoringMethod",
                                "ExpectedValue": null,
                                "ExpectedValues": [
                                    "%current%"
                                ],
                                "IsObject": false,
                                "ObjectKeyAndValues": null,
                                "ObjectKeyAndValue": null,
                                "AttributeName": null,
                                "ConstraintName": null,
                                "RestrictedValue": null,
                                "ComparisonOperator": "NOT_EQ",
                                "AnyObjectKeyValue": false,
                                "FilteredKeys": null,
                                "LookupFormPart": "TreatmentDetails",
                                "LookupField": "MonitoringMethod"
                            },
                            "PropertyValueList": [
                                {
                                    "Property": "IsDisabled",
                                    "Value": true
                                },
                                {
                                    "Property": "Value",
                                    "Value": "%current%"
                                },
                                {
                                    "Property": "HasInfo",
                                    "Value": true
                                },
                                {
                                    "Property": "InfoText",
                                    "Value": "Date change not supported if switching Adherence Method"
                                }
                            ]
                        }
                    ]
                }
            ],
            "CompoundValueDependencies": null,
            "SaveEndpoint": "/API/Patients/Update",
            "SaveText": "Submit",
            "SaveTextKey": "submit"
        },
        "ExistingData": {
            "TBTreatmentStartDate": "2019-05-01T00:00:00",
            "MonitoringMethod": "99DOTS"
        }
    }
}
export default formData