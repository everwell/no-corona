import { mount, createLocalVue } from '@vue/test-utils'
import Evrimed from '../../src/app/Pages/dashboard/components/Evrimed.vue'
import Vuex from 'vuex'
import Button from '@/app/shared/components/Button.vue'

const localVue = createLocalVue()
localVue.use(Vuex)

describe('Evrimed page Tests', () => {
  let store
  let actions
  beforeEach(() => {
    actions = {
      getMermDetails: jest.fn(),
      checkPermission: jest.fn()
    }
    store = new Vuex.Store({
      modules: {
        Evrimed: {
          namespaced: true,
          actions
        }
      }
    })
  })

  it('items displayed correctly', () => {
    const wrapper = mount(Evrimed, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      }
    })
    expect(actions.checkPermission.mock.calls).toHaveLength(1)
    wrapper.findComponent(Button).vm.$emit('click')
    expect(actions.getMermDetails.mock.calls).toHaveLength(1)
  })
})
