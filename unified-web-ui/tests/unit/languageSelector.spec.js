import { mount,createLocalVue } from '@vue/test-utils'
import LanguageSelector from '@/app/shared/components/LanguageSelector.vue'
import { getCookie, setCookie } from '@/utils/cookieUtils.js'

import Vuex from 'vuex'

const localVue = createLocalVue()

localVue.use(Vuex)

describe('LanguageSelector.vue', () => {
  let store
  let actions
  beforeEach(() => {
    actions = {
      GetAllowedLanguagesForDeployment: jest.fn()
    }
    store = new Vuex.Store({
      modules: {
        Sidebar: {
          namespaced: true,
          actions
        }
      }
    })
  })
  
  it('Language selector backend call', async () => {
    let sampleResponse = { Success: true, languages: [{ id: '1', value: 'Option 1' }, { id: '2', value: 'Option 2' }, { id: '3', value: 'Option 3' }, { id: '4', value: 'Option 4' }, { id: '5', value: 'Option 5' }, { id: '6', value: 'Option 6' }, { id: '7', value: 'Option 7' }, { id: '8', value: 'Option 8' }, { id: '9', value: 'Option 4' }, { id: '10', value: 'Option 5' }, { id: '11', value: 'Option 6' }, { id: '12', value: 'Option 7' }, { id: '13', value: 'Option 8' }], Error: null }
    actions.GetAllowedLanguagesForDeployment.mockReturnValue(sampleResponse)
    mount(LanguageSelector, {
      store,
      localVue,
      mocks: {
        $i18n: () => {
          return {
            locale: ''
          }
        },
        $t: (key) => key
      }
    })
    expect(actions.GetAllowedLanguagesForDeployment.mock.calls).toHaveLength(1)
  })
  it('emits event when language is changed', async () => {
    let data
    let mockCommit = (state, payload) => {
      data = payload
    }
    const wrapper = mount(LanguageSelector, {
      store,
      localVue,
      mocks: {
        $i18n: () => {
          return {
            locale: ''
          }
        },
        $t: (key) => key
      }
    })
    const sampleCookieValue = 'someLanguage'
    const languageCookieName = 'languageCode' // original name, used everywhere
    setCookie('test', 'testvalue', 1)
    expect(getCookie(languageCookieName) !== sampleCookieValue).toBeTruthy()
  })
  it('calling changeLanguage',  async () => {
    const changeLanguage = jest.fn()
    const wrapper = mount(LanguageSelector, {
      store,
      localVue,
      mocks: {
        $i18n: () => {
          return {
            locale: ''
          }
        },
        $t: (key) => key
      }
    })
    wrapper.setMethods({
      changeLanguage: changeLanguage
    })
    wrapper.find('ul').trigger('change')
    expect(changeLanguage).toHaveBeenCalled
    const sampleCookieValue = 'someLanguage'
    const languageCookieName = 'languageCode' // original name, used everywhere
    setCookie('test', 'testvalue', 1)
    expect(getCookie(languageCookieName) !== sampleCookieValue).toBeTruthy()
    expect(wrapper.vm.$i18n.locale).toBeUndefined()
    wrapper.vm.$emit('LANGUAGE_CHANGE')
    
  })
  
})
