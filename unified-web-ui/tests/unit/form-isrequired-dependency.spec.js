import { mount, shallowMount, createLocalVue } from '@vue/test-utils'
import Form from '@/app/shared/components/Form.vue'
import Vuex from 'vuex'
import FormStore from '@/app/shared/store/modules/form'
import HierarchySelectionFieldStore from '@/app/shared/store/modules/HierarchySelectionField'
import flushPromises from 'flush-promises'
import Vue from 'vue'
import testForm from './form-isrequired-dependency-data-test'
import RadioGroup from '@/app/shared/components/RadioGroup'
import TextArea from '@/app/shared/components/TextArea'
import VerticalFormPart from '@/app/shared/components/VerticalFormPart'

const localVue = createLocalVue()

localVue.use(Vuex)
window.EventBus = new Vue()


jest.mock('../../src/utils/toastUtils', () => {
  return {
    __esModule: true,
    defaultToast: jest.fn(),
    ToastType: {
      Success: 'Success',
      Error: 'Error',
      Warning: 'Warning',
      Neutal: 'Neutal'
    }
  }
})

const getStore = (actions) => {
  const formattedActions = {
    ...FormStore.actions,
    ...actions
  }
  return new Vuex.Store({
    modules: {
      Form: {
        state: FormStore.state,
        actions: formattedActions,
        mutations: FormStore.mutations,
        namespaced: true
      },
      HierarchySelectionField: {
        actions: HierarchySelectionFieldStore.actions,
        state: HierarchySelectionFieldStore.state,
        mutations: HierarchySelectionFieldStore.mutations,
        namespaced: true
      }
    }
  })
}
describe('Form.vue', () => {
  it('checks if dependent text area field isRequired property becomes true on "OTHER" selection in radio button group input', async () => {
    const wrapper = mount(Form, { 
      store: getStore({
        getFormData: async () => (
          Promise.resolve(
            testForm
          )
        )
      }),
      localVue,
      propsData: { isEditing: true, name: 'CloseCase', getFormUrlParams: 'idName=PATIENT&id=1'},
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises()
    let allRadioButtons= wrapper.findComponent(VerticalFormPart).findComponent(RadioGroup).findAll('input[type="radio"]')
    let otherOption = allRadioButtons.at(allRadioButtons.length-1)
    otherOption.setChecked(true)
    await wrapper.vm.$nextTick(); 
    let textArea =  wrapper.findComponent(VerticalFormPart).findComponent(TextArea)
    expect(textArea.vm.isRequired).toBeTruthy()
  })

  it('checks if dependent text area field isRequired property becomes false on any selection except "OTHER" in radio button group input', async () => {
    const wrapper = mount(Form, { 
      store: getStore({
        getFormData: async () => (
          Promise.resolve(
            testForm
          )
        )
      }),
      localVue,
      propsData: { isEditing: true, name: 'CloseCase', getFormUrlParams: 'idName=PATIENT&id=1'},
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises()
    let allRadioButtons= wrapper.findComponent(VerticalFormPart).findComponent(RadioGroup).findAll('input[type="radio"]')
    let otherOption = allRadioButtons.at(0)
    otherOption.setChecked(true)
    await wrapper.vm.$nextTick(); 
    let textArea =  wrapper.findComponent(VerticalFormPart).findComponent(TextArea)
    expect(textArea.vm.isRequired).toBeFalsy()
  })
})


