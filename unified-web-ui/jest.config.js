module.exports = {
  preset: '@vue/cli-plugin-unit-jest',
  collectCoverage: true,
  collectCoverageFrom: [
    'src/**/*',
    'utils/**/*'
  ],
  coveragePathIgnorePatterns: [
    'node_modules',
    'scss',
    'assets',
    'store',
    'app-routes.js',
    'main.js',
    'registerServiceWorker.js',
    'dashboard-routes.js',
    'home-routes.js',
    'vuetify.js',
    'ThemeExample.vue', // demo component only hence no tests
    'Demo.vue', // demo component only hence no test
    'utils/languages/strings',
    'src/app/Pages/dashboard/patient/Tabs/Adherence/index.js' // exclude for now
  ]
}
