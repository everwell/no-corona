import { ApiServerClient, errorCallback } from '../../Api'
import { getCookie } from '../../../../../utils/cookieUtils'

export default {
  namespaced: true,
  actions: {
    async getMermDetails ({ commit, state }, imei) {
      const url = '/api/Merm/GetMermDetailsByImeiAndHierarchy?imei=' + imei + '&languageCode=' + getCookie('languageCode')
      const response = await ApiServerClient.get(url, null, errorCallback)
      if (response.success) {
        return (response.data)
      } else {
        // toastError('Unable to load Merm Details')
      }
    },

    async checkPermission () {
      const url = 'api/DashboardPermission/Evrimed'
      const response = await ApiServerClient.get(url, null, errorCallback)
      if (response.success && !response.data) {
        window.location.href = '/Dashboard'
        // todo error page
      }
    }
  }
}
