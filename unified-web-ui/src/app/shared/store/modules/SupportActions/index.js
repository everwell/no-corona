import { ApiServerClient, errorCallback } from '../../Api'
import { defaultToast, ToastType } from '../../../../../utils/toastUtils'

export default {
  namespaced: true,
  actions: {
    async getSupportActionTabViewDetails ({ commit, state }, patientId) {
      const url = 'api/Patients/FormParentConfig?formName=SupportActions&idName=PATIENT&id=' + patientId
      const response = await ApiServerClient.get(url, null, errorCallback)
      if (response.Success) {
        return response
      } else {
        defaultToast(ToastType.Error, 'Unable to load  Details')
      }
    }
  }
}
