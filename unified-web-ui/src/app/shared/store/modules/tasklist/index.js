import { ApiServerClient, errorCallback } from '../../Api'
import { getCookie } from '../../../../../utils/cookieUtils.js'
export default {
  namespaced: true,
  state: {
    taskListsDetails: [],
    allcolumns: [],
    taskListsDetailsLoading: false,
    taskListsDetailsLoaded: false,
    hierarchyId: 0,
    patientsLoading: false,
    patientsData: {},
    pageSize: 20,
    totalPatients: 0,
    taskListDescInputDataMap: {},
    taskListItemMappedByTasklistDesc: {},
    inputData: {},
    selectedTaskListItem: {},
    // Table specific data
    columnDetailsMappedByValue: {}
  },
  mutations: {
    UPDATE_HIERARCHY_ID (state, data) {
      state.hierarchyId = data
    },

    START_LOADING_TASK_LIST_DETAILS (state) {
      state.taskListsDetails = []
      state.taskListsDetailsLoading = true
      state.taskListsDetailsLoaded = false
    },

    END_LOADING_TASK_LIST_DETAILS (state) {
      state.taskListsDetailsLoading = false
      state.taskListsDetailsLoaded = true
    },

    START_PATIENTS_LOADING (state) {
      state.patientsLoading = true
    },

    END_PATIENTS_LOADING (state) {
      state.patientsLoading = false
    },

    SET_COLUMNS (state, data) {
      state.allcolumns = data.columns
      state.columnDetailsMappedByValue = data.columnDetailsMappedByValue
    },

    UPDATE_PATIENTS_DATA (state, data) {
      state.patientsData = data
    },

    UPDATE_TOTAL_PATIENTS (state, data) {
      state.totalPatients = data
    },

    SET_TASK_LIST_DETAILS (state, data) {
      state.taskListsDetails = data
    },
    SET_INPUT_DATA (state, data) {
      state.inputData = data
    },
    SET_INPUT_MAP (state, data) {
      state.taskListDescInputDataMap = data
    },
    SET_TASKLIST_MAP (state, data) {
      state.taskListItemMappedByTasklistDesc = data
    },
    SET_SELECTED_TASKLIST_ITEM (state, data) {
      state.selectedTaskListItem = data
    }

  },
  actions: {
    updateHirarchyId ({ commit }, hierarchyId) {
      commit('UPDATE_HIERARCHY_ID', hierarchyId)
    },

    async getLoggedInUserHierarchyId ({ commit }) {
      const url = process.env.VUE_APP_THEME === 'hub' ? '/api/Hierarchy/GetHierarchyChildren' : '/api/Hierarchy/GetUsersHierarchyWithChildren'
      try {
        const response = await ApiServerClient.get(url, null, errorCallback)
        if (response) {
          if (process.env.VUE_APP_THEME === 'hub') { commit('UPDATE_HIERARCHY_ID', response.Data[0].Key) } else {
            commit('UPDATE_HIERARCHY_ID', response.Data.Current.Id)
          }
        }
        // // console.log(response.Data[0].Key)
      } catch (ex) {
        // console.log(ex)
      }
    },

    async setInputData ({ commit }, inputData) {
      // console.log(inputData)
      commit('SET_INPUT_DATA', inputData)
    },

    async setSelectedTasklistItem ({ commit }, item) {
      // console.log(item)
      commit('SET_SELECTED_TASKLIST_ITEM', item)
    },

    setColumnsToDisplay ({ commit }, columnsToDisplay) {
      commit('UPDATE_COLUMNS_TO_DISPLAY', columnsToDisplay)
    },

    async loadTaskListDetails ({ commit, state }) {
      if (state.taskListsDetailsLoaded) return

      commit('START_LOADING_TASK_LIST_DETAILS')

      let language = 'en'
      try {
        // eslint-disable-next-line no-undef
        language = getCookie('languageCode')
      } catch {
        language = 'en'
      }
      const url = '/api/Patients/TaskListsDetails?language=' + language
      const taskListsDetailsData = await ApiServerClient.get(url, null)
      const tasklistDetails = taskListsDetailsData.TasklistDetails.map(item => {
        const newItem = item
        newItem.inputData.tId = item.id
        return newItem
      })
      commit('SET_TASK_LIST_DETAILS', tasklistDetails)
      // eslint-disable-next-line prefer-const
      let inputMap = tasklistDetails.reduce((acc, item) => {
        acc[item.descKey] = item.inputData
        return acc
      }, {})
      // eslint-disable-next-line prefer-const
      let tasklistMap = tasklistDetails.reduce((acc, item) => {
        acc[item.descKey] = item
        return acc
      }, {})
      commit('SET_INPUT_MAP', inputMap)
      commit('SET_TASKLIST_MAP', tasklistMap)
      commit('END_LOADING_TASK_LIST_DETAILS')
    },
    async loadTaskListDetailsForced ({ commit, state, dispatch }) {
      commit('START_LOADING_TASK_LIST_DETAILS')
      await dispatch('loadTaskListDetails')
    },
    async loadTaskListFunction ({ commit, state }, inputData) {
      commit('START_PATIENTS_LOADING')
      // await dispatch('setActive', inputData)

      commit('UPDATE_PATIENTS_DATA', {})
      commit('SET_COLUMNS', { columns: [], columnDetailsMappedByValue: {} })

      // eslint-disable-next-line prefer-const
      const data = inputData.input
      const req = { ...data }
      delete req.tId
      delete req.id
      delete req.tasklistId
      req.id = state.hierarchyId
      req.tasklistId = inputData.input.tId
      req.page = inputData.page
      req.pageSize = state.pageSize
      req.sortBy = inputData.sortBy
      req.sortDesc = inputData.sortDesc

      const url = '/api/Patients/TaskListData'

      try {
        const response = await ApiServerClient.get(url, req)
        const columns = response.data.Columns.map(c => { return { id: c.key, value: c.label, sortable: c.sortable } })
        const columnDetailsMappedByValue = columns.reduce((acc, item) => {
          acc[item.id] = item
          return acc
        }, {})
        commit('SET_COLUMNS', { columns, columnDetailsMappedByValue })

        var patientsList = []

        response.data.Patients.forEach(function (patient) {
          var patientObj = {}
          response.data.Columns.forEach(function (column) {
            patientObj[column.key] = patient[column.key]
          })
          patientsList.push(patientObj)
        })

        commit('UPDATE_PATIENTS_DATA', {
          patients: patientsList
        })
        commit('UPDATE_TOTAL_PATIENTS', response.data.TotalPatients)
      } catch (ex) {
      }

      commit('END_PATIENTS_LOADING')
    }

  }

}
