package com.everwell.dispensationservice.repositories;

import com.everwell.dispensationservice.models.db.ProductInventoryHierarchyMapping;
import com.everwell.dispensationservice.models.dto.ProductInventoryHierarchyMappingMinDto;
import com.everwell.dispensationservice.models.dto.HierarchyData;
import com.everwell.dispensationservice.models.dto.HierarchyInventoryDto;
import com.everwell.dispensationservice.models.dto.ProductStockHierarchyDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;

import javax.persistence.LockModeType;
import java.util.List;

public interface ProductInventoryHierarchyMappingRepository extends JpaRepository<ProductInventoryHierarchyMapping, Long> {
    @Lock(LockModeType.PESSIMISTIC_WRITE)
    ProductInventoryHierarchyMapping findProductInventoryHierarchyMappingByInventoryIdAndHierarchyMappingIdAndClientId(Long inventoryId, Long hierarchyMappingId, Long clientId);

    List<ProductInventoryHierarchyMapping> findAllByInventoryIdInAndHierarchyMappingIdIn(List<Long> inventoryIds, List<Long> hierarchyMappingIds);

    @Query(value = "select new " + "com.everwell.dispensationservice.models.dto.ProductInventoryHierarchyMappingMinDto(pihm.id,pihm.inventoryId)" + "from ProductInventoryHierarchyMapping as pihm where pihm.id in :ids")
    List<ProductInventoryHierarchyMappingMinDto> findAllByIdIn(List<Long> ids);

    @Query("SELECT NEW com.everwell.dispensationservice.models.dto.ProductStockHierarchyDto(pi.id, pi.productId, pi.batchNumber, pi.expiryDate, pihm.hierarchyMappingId, pihm.availableQuantity) " +
            "FROM ProductInventory pi " +
            "left JOIN ProductInventoryHierarchyMapping pihm ON pi.id = pihm.inventoryId " +
            "WHERE pi.productId IN :productIds AND pihm.hierarchyMappingId IN :hierarchyMappingIds")
    List<ProductStockHierarchyDto> getProductStockHierarchyDto(List<Long> productIds, List<Long> hierarchyMappingIds);

}