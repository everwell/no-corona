package com.everwell.dispensationservice.services.impl;

import com.everwell.dispensationservice.Utils.CacheUtils;
import com.everwell.dispensationservice.Utils.Utils;
import com.everwell.dispensationservice.constants.Constants;
import com.everwell.dispensationservice.elasticsearch.dto.MatchSearchDto;
import com.everwell.dispensationservice.elasticsearch.dto.ProductSearchResult;
import com.everwell.dispensationservice.elasticsearch.indices.ProductIndex;
import com.everwell.dispensationservice.elasticsearch.service.ESProductService;
import com.everwell.dispensationservice.enums.event.EventActionEnum;
import com.everwell.dispensationservice.enums.event.EventCategoryEnum;
import com.everwell.dispensationservice.enums.event.EventNameEnum;
import com.everwell.dispensationservice.exceptions.NotFoundException;
import com.everwell.dispensationservice.exceptions.ValidationException;
import com.everwell.dispensationservice.models.db.Product;
import com.everwell.dispensationservice.models.dto.CacheMultiSetWithTimeDto;
import com.everwell.dispensationservice.models.dto.eventstreaming.EventDto;
import com.everwell.dispensationservice.models.http.requests.ProductBulkRequest;
import com.everwell.dispensationservice.models.http.requests.ProductRequest;
import com.everwell.dispensationservice.models.http.requests.ProductSearchRequest;
import com.everwell.dispensationservice.models.http.responses.ProductResponse;
import com.everwell.dispensationservice.repositories.ProductRepository;
import com.everwell.dispensationservice.services.ProductService;
import com.fasterxml.jackson.core.type.TypeReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class ProductServiceImpl implements ProductService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProductServiceImpl.class);

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ESProductService esProductService;

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    private void validateProductIdsIn(List<Long> productIds, List<Long> validIds) {
        List<String> invalidIds = productIds
                .stream()
                .filter(id -> !validIds.contains(id))
                .map(Objects::toString)
                .collect(Collectors.toList());
        if(!invalidIds.isEmpty()) {
            throw new ValidationException(String.format("Invalid product id(s) %s", String.join(",", invalidIds)));
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Product addProduct(ProductRequest productRequest) {
        Product product = getProductByProductData(productRequest);
        if(null == product) {
            product =  new Product(productRequest.getName(), productRequest.getDrugCode(), productRequest.getDosage(), productRequest.getDosageForm(), productRequest.getManufacturer(), productRequest.getComposition(), productRequest.getCategory());
            productRepository.save(product);
            esProductService.save(product.toProductIndex());
        }
        return product;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public List<Product> addProductBulk(ProductBulkRequest productBulkRequest) {
       List<Product> allProducts =  new ArrayList<>(), newProducts = new ArrayList<>();
       for (ProductRequest productRequest: productBulkRequest.getProductDetails()){
           Product product = getProductByProductData(productRequest);
           if(null == product) {
              product = new Product(productRequest.getName(), productRequest.getDrugCode(), productRequest.getDosage(), productRequest.getDosageForm(), productRequest.getManufacturer(), productRequest.getComposition(), productRequest.getCategory());
              newProducts.add(product);
           }
           allProducts.add(product);
        }
       productRepository.saveAll(newProducts);
       List<ProductIndex> productIndexList = new ArrayList<>();
       newProducts.forEach(product -> {
           productIndexList.add(product.toProductIndex());
       });
       esProductService.saveAll(productIndexList);
       return allProducts;
    }

    @Override
    public Product formatCacheForProduct(String cachedEntry) {
        Product cachedProduct = new Product();
        try {
            cachedProduct = Utils.convertStringToObject(cachedEntry, Product.class);
        } catch (IOException e) {
            LOGGER.error("[formatCacheForProduct] could not parse cache response");
        }
        return cachedProduct;
    }

    @Override
    public Product getProduct(Long productId) {
        String cachedData = CacheUtils.getFromCache(Constants.CACHE_DISPENSATION_PRODUCT_ID + productId.toString());
        Product cachedProductData = null != cachedData ? formatCacheForProduct(cachedData) : new Product();
        if(null == cachedProductData.getId()) {
            Optional<Product> product = productRepository.findById(productId);
            if(!product.isPresent()) {
                throw new NotFoundException("No product found with id " + productId);
            }
            CacheUtils.putIntoCache(Constants.CACHE_DISPENSATION_PRODUCT_ID + productId, Utils.asJsonString(product.get()), Constants.PRODUCT_VALIDITY_EXPIRY);
            return product.get();
        } else {
            return cachedProductData;
        }
    }

    @Override
    public List<ProductResponse> getAllProducts() {
        List<Long> productIds = CacheUtils.getFromCacheWithResponseType(Constants.CACHE_ALL_PRODUCT_IDS_KEY, new TypeReference<List<Long>>(){});
        List<ProductResponse> productResponseList;
        if(null == productIds) {
            productResponseList = getAllProductDetailsFromProducts(productRepository.findAll());
            productIds = productResponseList.stream().map(ProductResponse::getProductId).collect(Collectors.toList());
            CacheUtils.putIntoCache(Constants.CACHE_ALL_PRODUCT_IDS_KEY, Utils.asJsonString(productIds), Constants.PRODUCT_VALIDITY_EXPIRY);
        } else {
            productResponseList = getAllProductDetailsFromProducts(getProducts(productIds));
        }
        return productResponseList;
    }

    @Override
    public Product getProductByProductData(ProductRequest productRequest) {
        return  productRepository.findByProductNameAndDrugCodeAndDosageAndDosageFormAndManufacturerAndComposition(productRequest.getName(), productRequest.getDrugCode(),
                productRequest.getDosage(), productRequest.getDosageForm(), productRequest.getManufacturer(), productRequest.getComposition());
    }

    @Override
    public Boolean deleteAllProductIdsCacheEntry() {
        return CacheUtils.deleteFromCache(Constants.CACHE_ALL_PRODUCT_IDS_KEY);
    }

    @Override
    public List<ProductResponse> getAllProductDetails(ProductSearchRequest productSearchRequest, Long clientId) {
        List<ProductResponse> productResponses = new ArrayList<>();
        List<String> drugCodeList = productSearchRequest.getDrugCodes();
        List<Long> productIdList = productSearchRequest.getProductIds();
        List<String> productNameList = new ArrayList<>();
        boolean shouldTrackMatomoEventWithPattern = false;
        if (null != productIdList) {
            productIdList.removeAll(Collections.singleton(null));
        }
        if (null != drugCodeList) {
            drugCodeList.removeAll(Collections.singleton(null));
        }
        if(null != productIdList && !productIdList.isEmpty()) {
            productResponses = getProducts(productIdList)
                    .stream()
                    .map(ProductResponse::new)
                    .collect(Collectors.toList());
            if( null != drugCodeList && !drugCodeList.isEmpty()) {
                HashSet<String> drugCodes = new HashSet<>(drugCodeList);
                productResponses = productResponses
                        .stream()
                        .filter(i-> drugCodes.contains(i.getDrugCode()))
                        .collect(Collectors.toList());
            }
        } else if (null != drugCodeList && !productSearchRequest.getDrugCodes().isEmpty()) {
            productResponses = productRepository.findAllByDrugCodeIn(productSearchRequest.getDrugCodes())
                    .stream()
                    .map(ProductResponse::new)
                    .collect(Collectors.toList());
        } else if (!StringUtils.isEmpty(productSearchRequest.getPattern())) {
            shouldTrackMatomoEventWithPattern = true;
            productSearchRequest.setUpDefaultValues();
            productResponses.addAll(getProductResponseListFromElasticSearch(productSearchRequest));
            productSearchRequest.setUpProductSearchRequestForOther();
            productResponses.addAll(getProductResponseListFromElasticSearch(productSearchRequest));
            productNameList.add(productSearchRequest.getPattern());
        } else if (null != productSearchRequest.getSearch()) {
            shouldTrackMatomoEventWithPattern = true;
            productResponses = getProductResponseListFromElasticSearch(productSearchRequest);
            if (null != productSearchRequest.getSearch().getMust() && !CollectionUtils.isEmpty(productSearchRequest.getSearch().getMust().getMatchSearchList())) {
                List<MatchSearchDto> matchSearchDtoList = productSearchRequest.getSearch().getMust().getMatchSearchList();
                matchSearchDtoList.forEach(matchSearchDto -> {
                    productNameList.add(matchSearchDto.getSearchTerm());
                });
            }
        }
        String eventName = shouldTrackMatomoEventWithPattern ? EventNameEnum.GET_PRODUCT_BULK_SEARCH_FROM_ELASTIC.getName() : EventNameEnum.GET_PRODUCT_BULK.getName();
        String eventAction = shouldTrackMatomoEventWithPattern ? String.format(EventActionEnum.ELASTIC_SEARCH_MUST_MATCH_FETCH.getName(), productNameList) : EventActionEnum.DB_RECORD_FETCH.getName();
        applicationEventPublisher.publishEvent(new EventDto(EventCategoryEnum.PRODUCT.getName(), eventName, eventAction, clientId));
        return productResponses;
    }

    private List<ProductResponse> getProductResponseListFromElasticSearch(ProductSearchRequest productSearchRequest) {
        ProductSearchResult productSearchResult = esProductService.searchProducts(productSearchRequest);
        return  productSearchResult.getProductIndexList()
                .stream()
                .map(ProductIndex::toProductResponse)
                .collect(Collectors.toList());
    }

    @Override
    public List<ProductResponse> getAllProductDetailsFromProducts(List<Product> products) {
        return products
                .stream()
                .map(ProductResponse::new)
                .collect(Collectors.toList());
    }

    @Override
    public void validateProductIds(List<Long> productIds) {
        List<Long> validProductIds = getProducts(productIds)
                .stream()
                .map(Product::getId)
                .collect(Collectors.toList());
        validateProductIdsIn(productIds, validProductIds);
    }

    public List<Product> getProductsFromCache(List<Long> ids) {
        List<Product> productList = new ArrayList<>();
        HashSet<String> idList =  new HashSet<>();
        ids.forEach(id ->idList.add(Constants.CACHE_DISPENSATION_PRODUCT_ID + id));
        List<String> cachedData = CacheUtils.getFromCacheBulk(idList);
        if(cachedData != null ) {
            productList.addAll(cachedData
                    .stream().filter(Objects::nonNull).collect(Collectors.toList())
                    .stream().map(this::formatCacheForProduct).collect(Collectors.toList()));
        }
        return productList;
    }

    @Override
    public List<Product> getProducts(List<Long> ids) {
        List<CacheMultiSetWithTimeDto> cacheMultiSetWithTimeDtoList = new ArrayList<>();
        List<Product> productList = getProductsFromCache(ids);
        HashSet<Long> cachedProductIds = (HashSet<Long>) productList
                .stream()
                .map(Product::getId).collect(Collectors.toSet());
        List<Long> idsToFetch = ids
                .stream()
                .filter(id -> !cachedProductIds.contains(id)).collect(Collectors.toList());
        if(!idsToFetch.isEmpty()) {
            List<Product> products = productRepository.findAllById(idsToFetch);
            for(Product product : products){
                cacheMultiSetWithTimeDtoList.add(new CacheMultiSetWithTimeDto(Constants.CACHE_DISPENSATION_PRODUCT_ID + product.getId().toString(), Utils.asJsonString(product), Constants.PRODUCT_VALIDITY_EXPIRY));
            }
            CacheUtils.bulkInsertWithTtl(cacheMultiSetWithTimeDtoList);
            productList.addAll(products);
        }
        return productList;
    }

}
