package com.everwell.dispensationservice.services.impl;

import com.everwell.dispensationservice.Utils.Utils;
import com.everwell.dispensationservice.enums.CommentType;
import com.everwell.dispensationservice.enums.TransactionIdType;
import com.everwell.dispensationservice.enums.TransactionType;
import com.everwell.dispensationservice.exceptions.ValidationException;
import com.everwell.dispensationservice.models.db.ProductInventory;
import com.everwell.dispensationservice.models.db.ProductInventoryHierarchyMapping;
import com.everwell.dispensationservice.models.db.ProductInventoryLog;
import com.everwell.dispensationservice.models.dto.*;
import com.everwell.dispensationservice.models.http.requests.ProductStockSearchRequest;
import com.everwell.dispensationservice.models.http.requests.StockRequest;
import com.everwell.dispensationservice.repositories.ProductInventoryHierarchyMappingRepository;
import com.everwell.dispensationservice.repositories.ProductInventoryLogRepository;
import com.everwell.dispensationservice.repositories.ProductInventoryRepository;
import com.everwell.dispensationservice.services.ProductService;
import com.everwell.dispensationservice.services.StockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.*;
import java.util.stream.Collectors;

import static com.everwell.dispensationservice.constants.Constants.CREDIT_COMMENT;
import static com.everwell.dispensationservice.constants.Constants.DEBIT_COMMENT;

@Service
public class StockServiceImpl implements StockService {

    @Autowired
    private ProductInventoryRepository productInventoryRepository;

    @Autowired
    private ProductInventoryHierarchyMappingRepository productInventoryHierarchyMappingRepository;

    @Autowired
    private ProductInventoryLogRepository productInventoryLogRepository;

    @Autowired
    private ProductService productService;

    @Override
    public Long getInventoryData(ProductInventoryDto productInventoryDto) {
        return productInventoryRepository.findIdByProductIdAndBatchNumber(productInventoryDto.getProductId(), productInventoryDto.getBatchNumber());
    }

    @Override
    public ProductInventory addInventoryData(ProductInventoryDto productInventoryDto) {
        ProductInventory inventoryData = new ProductInventory(productInventoryDto.getProductId(), productInventoryDto.getBatchNumber(), productInventoryDto.getExpiryDate());
        productInventoryRepository.save(inventoryData);
        return inventoryData;
    }

    @Override
    public ProductInventoryHierarchyMapping getInventoryHierarchyData(ProductInventoryHierarchyMappingDto productInventoryHierarchyMappingDto) {
        return productInventoryHierarchyMappingRepository.findProductInventoryHierarchyMappingByInventoryIdAndHierarchyMappingIdAndClientId(productInventoryHierarchyMappingDto.getInventoryId(),
                productInventoryHierarchyMappingDto.getHierarchyMappingId(), productInventoryHierarchyMappingDto.getClientId());
    }

    @Override
    public ProductInventoryHierarchyMapping addInventoryHierarchyData(ProductInventoryHierarchyMappingDto productInventoryHierarchyMappingDto) {
        ProductInventoryHierarchyMapping inventoryHierarchyData = new ProductInventoryHierarchyMapping(productInventoryHierarchyMappingDto.getInventoryId(),
                productInventoryHierarchyMappingDto.getHierarchyMappingId(), productInventoryHierarchyMappingDto.getClientId(), productInventoryHierarchyMappingDto.getAvailableQuantity(),
                productInventoryHierarchyMappingDto.getLastUpdatedBy());
        productInventoryHierarchyMappingRepository.save(inventoryHierarchyData);
        return inventoryHierarchyData;
    }

    @Override
    public ProductInventoryHierarchyMapping updateInventoryHierarchyData(ProductInventoryHierarchyMapping productInventoryHierarchyMapping, Long quantity, Long lastUpdatedBy) {
        productInventoryHierarchyMapping.setAvailableQuantity(quantity);
        productInventoryHierarchyMapping.setLastUpdatedBy(lastUpdatedBy);
        productInventoryHierarchyMapping.setLastUpdatedAt(Utils.getCurrentDate());
        productInventoryHierarchyMappingRepository.save(productInventoryHierarchyMapping);
        return productInventoryHierarchyMapping;
    }

    @Override
    public ProductInventoryLog addInventoryLog(ProductInventoryLogDto productInventoryLogDto) {
        ProductInventoryLog productInventoryLog = new ProductInventoryLog(productInventoryLogDto.getTransactionType(),productInventoryLogDto.getTransactionId(), productInventoryLogDto.getTransactionQuantity(),
                productInventoryLogDto.getCommentType(), productInventoryLogDto.getComment(), productInventoryLogDto.getPInventoryId(), productInventoryLogDto.getProductId(),
                productInventoryLogDto.getUpdatedBy(), productInventoryLogDto.getTransactionIdType());
        productInventoryLogRepository.save(productInventoryLog);
        return productInventoryLog;
    }

    @Override
    public Long setInventoryData(ProductInventoryDto productInventoryDto, TransactionType transactionType) {
        Long id = getInventoryData(productInventoryDto);
        ProductInventory inventoryData = null;
        if(null == id && (transactionType == TransactionType.DEBIT || transactionType == TransactionType.ISSUED || transactionType == TransactionType.RETURNED)) {
            throw new ValidationException("product Id, batch number and expiry date does not exist in the inventory");
        } else if(null == id && transactionType == TransactionType.CREDIT) {
            inventoryData = addInventoryData(productInventoryDto);
        }
        id = null != inventoryData ? inventoryData.getId() : id;
        return id;
    }

    @Override
    public ProductInventoryHierarchyMapping setInventoryHierarchyData(ProductInventoryHierarchyMappingDto productInventoryHierarchyMappingDto, TransactionType transactionType){
        ProductInventoryHierarchyMapping inventoryHierarchyMappingData = getInventoryHierarchyData(productInventoryHierarchyMappingDto);
        if(null ==  inventoryHierarchyMappingData && (transactionType == TransactionType.DEBIT || transactionType == TransactionType.ISSUED || transactionType == TransactionType.RETURNED || transactionType == TransactionType.UPDATE)) {
            throw new ValidationException("hierarchy Id does not exist in the inventory");
        } else if(null ==  inventoryHierarchyMappingData && transactionType == TransactionType.CREDIT) {
            inventoryHierarchyMappingData = addInventoryHierarchyData(productInventoryHierarchyMappingDto);
        }
        return inventoryHierarchyMappingData ;
    }

    @Override
    public ProductInventoryLog setInventoryLogData(StockRequest stockRequest, TransactionType transactionType, Long pInventoryId) {
        ProductInventoryLog productInventoryLog = new ProductInventoryLog();
        if(transactionType == TransactionType.CREDIT) {
           productInventoryLog =  addInventoryLog(new ProductInventoryLogDto(TransactionType.CREDIT, stockRequest.getExternalTransactionId(), stockRequest.getQuantity(),
                    CommentType.REASON_FOR_CREDIT, CREDIT_COMMENT, pInventoryId, stockRequest.getProductId(), stockRequest.getLastUpdatedBy(), TransactionIdType.EXTERNAL));
        } else if(transactionType == TransactionType.DEBIT) {
           productInventoryLog =  addInventoryLog(new ProductInventoryLogDto(TransactionType.DEBIT, stockRequest.getExternalTransactionId(), stockRequest.getQuantity(),
                    CommentType.REASON_FOR_DEBIT, DEBIT_COMMENT, pInventoryId, stockRequest.getProductId(), stockRequest.getLastUpdatedBy(), TransactionIdType.EXTERNAL));
        } else if(transactionType == TransactionType.ISSUED) {
            productInventoryLog = addInventoryLog(new ProductInventoryLogDto(TransactionType.ISSUED, stockRequest.getExternalTransactionId(), stockRequest.getQuantity(),
                    CommentType.REASON_FOR_ADD, stockRequest.getReasonForTransaction(), pInventoryId, stockRequest.getProductId(), stockRequest.getLastUpdatedBy(), TransactionIdType.INTERNAL));
        } else if (transactionType == TransactionType.RETURNED) {
            productInventoryLog = addInventoryLog(new ProductInventoryLogDto(TransactionType.RETURNED, stockRequest.getExternalTransactionId(), stockRequest.getQuantity(),
                    CommentType.REASON_FOR_RETURN, stockRequest.getReasonForTransaction(), pInventoryId, stockRequest.getProductId(), stockRequest.getLastUpdatedBy(), TransactionIdType.INTERNAL));
        }
        else if(transactionType == TransactionType.UPDATE) {
            productInventoryLog = addInventoryLog(new ProductInventoryLogDto(TransactionType.UPDATE, stockRequest.getExternalTransactionId(), stockRequest.getQuantity(),
                    CommentType.REASON_FOR_UPDATE, stockRequest.getReasonForTransaction(), pInventoryId, stockRequest.getProductId(), stockRequest.getLastUpdatedBy(), TransactionIdType.INTERNAL));
        }
        return  productInventoryLog;
    }

    @Override
    @Transactional(rollbackFor =  Exception.class)
    public ProductInventoryLog setStockData(StockRequest stockRequest, TransactionType transactionType, Long clientId) {
        ProductInventoryDto productInventoryDto = new ProductInventoryDto(stockRequest.getProductId(),stockRequest.getBatchNumber(), Utils.convertStringToDateOrDefault(stockRequest.getExpiryDate()));
        Long inventoryId = setInventoryData(productInventoryDto, transactionType);
        ProductInventoryHierarchyMappingDto productInventoryHierarchyMappingDto = new ProductInventoryHierarchyMappingDto(inventoryId, stockRequest.getHierarchyMappingId(), clientId, 0L, stockRequest.getLastUpdatedBy());
        ProductInventoryHierarchyMapping inventoryHierarchyMappingData = setInventoryHierarchyData(productInventoryHierarchyMappingDto,transactionType);
        long updatedQuantity = 0;
        if(transactionType == TransactionType.DEBIT || transactionType == TransactionType.ISSUED) {
            updatedQuantity = inventoryHierarchyMappingData.getAvailableQuantity() - stockRequest.getQuantity();
        } else if (transactionType == TransactionType.CREDIT || transactionType == TransactionType.RETURNED) {
            updatedQuantity = stockRequest.getQuantity() + inventoryHierarchyMappingData.getAvailableQuantity();
        } else if(transactionType == TransactionType.UPDATE) {
            updatedQuantity = stockRequest.getQuantity();
        }
        if(updatedQuantity < 0L ){
            String productName = productService.getProduct(stockRequest.getProductId()).getProductName();
            throw  new ValidationException("Stock is currently not available for "+ productName +" and batch number "+stockRequest.getBatchNumber());
        }
        inventoryHierarchyMappingData = updateInventoryHierarchyData(inventoryHierarchyMappingData, updatedQuantity, stockRequest.getLastUpdatedBy());
        return setInventoryLogData(stockRequest, transactionType, inventoryHierarchyMappingData.getId());
    }

    @Override
    public List<ProductStockData> getProductStockData(ProductStockSearchRequest productStockSearchRequest) {
        List<ProductStockHierarchyDto> productStockHierarchyDtoList = productInventoryHierarchyMappingRepository.getProductStockHierarchyDto(productStockSearchRequest.getProductIds(), productStockSearchRequest.getHierarchyMappingIds());
        productStockHierarchyDtoList = productStockHierarchyDtoList.stream().filter(dto -> dto.getQuantity() != 0).collect(Collectors.toList());
        Map<Long, List<HierarchyData>> hierarchyDataMap = new HashMap<>();
        Map<Long, ProductInventory> productInventoryDataMap = new HashMap<>();
        for (ProductStockHierarchyDto dto : productStockHierarchyDtoList) {
            productInventoryDataMap.putIfAbsent(dto.getInventoryId(), new ProductInventory(dto.getProductId(), dto.getBatchNumber(), dto.getExpiryDate()));
            hierarchyDataMap.putIfAbsent(dto.getInventoryId(), new ArrayList<>());
            hierarchyDataMap.get(dto.getInventoryId()).add(new HierarchyData(dto.getHierarchyId(), dto.getQuantity()));
        }
        List<ProductStockData> productStockDataList = new ArrayList<>();
        for (Map.Entry<Long, ProductInventory> entry : productInventoryDataMap.entrySet()) {
            ProductInventory productInventory = entry.getValue();
            List<HierarchyData> hierarchyData = hierarchyDataMap.get(entry.getKey());
            productStockDataList.add(new ProductStockData(productInventory.getProductId(), productInventory.getBatchNumber(), productInventory.getExpiryDate(), hierarchyData));
        }
        return productStockDataList;
    }

    @Override
    public List<ProductInventoryLogDto> getProductInventoryLogDto(List<ProductInventoryLog> productInventoryLogList)
    {
        List<ProductInventoryLogDto> productInventoryLogDtoList = new ArrayList<>();
        List<Long> pInventoryIdList = productInventoryLogList.stream().map(ProductInventoryLog::getPInventoryId).collect(Collectors.toList());
        List<ProductInventoryHierarchyMappingMinDto> productInventoryHierarchyMappingList = productInventoryHierarchyMappingRepository.findAllByIdIn(pInventoryIdList);
        List<Long> inventoryIdList = productInventoryHierarchyMappingList.stream().map(ProductInventoryHierarchyMappingMinDto::getInventoryId).collect(Collectors.toList());
        Map<Long, Long> pInventoryIdToInventoryIdMap = productInventoryHierarchyMappingList
                .stream()
                .collect(Collectors.toMap(ProductInventoryHierarchyMappingMinDto::getId, ProductInventoryHierarchyMappingMinDto::getInventoryId));
        Map<Long, ProductInventory> productInventoryMap = productInventoryRepository.findAllByIdIn(inventoryIdList)
                .stream()
                .collect(Collectors.toMap(ProductInventory::getId, productInventory->productInventory));

        for(ProductInventoryLog productInventoryLog : productInventoryLogList) {
            String batchNumber = null;
            Date expiryDate = null;
            Long pInventoryId = productInventoryLog.getPInventoryId();
            if(null != pInventoryId) {
                Long inventoryId = pInventoryIdToInventoryIdMap.get(pInventoryId);
                ProductInventory productInventory = productInventoryMap.get(inventoryId);
                batchNumber = productInventory.getBatchNumber();
                expiryDate = productInventory.getExpiryDate();
            }
            ProductInventoryLogDto productMappingInventoryDto = new ProductInventoryLogDto(productInventoryLog.getId(), productInventoryLog.getTransactionType(), productInventoryLog.getTransactionId(), productInventoryLog.getTransactionQuantity(), productInventoryLog.getCommentType(),
                productInventoryLog.getComment(), productInventoryLog.getDateOfAction(),productInventoryLog.getPInventoryId(), productInventoryLog.getProductId(), productInventoryLog.getUpdatedBy(), productInventoryLog.getTransactionIdType(), batchNumber, expiryDate);
            productInventoryLogDtoList.add(productMappingInventoryDto);
        }
        return productInventoryLogDtoList;
    }

}
