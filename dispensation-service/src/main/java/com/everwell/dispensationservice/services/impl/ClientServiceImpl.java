package com.everwell.dispensationservice.services.impl;

import com.everwell.dispensationservice.exceptions.NotFoundException;
import com.everwell.dispensationservice.exceptions.ValidationException;
import com.everwell.dispensationservice.models.db.Client;
import com.everwell.dispensationservice.models.http.requests.RegisterClientRequest;
import com.everwell.dispensationservice.models.http.responses.ClientResponse;
import com.everwell.dispensationservice.repositories.ClientRepository;
import com.everwell.dispensationservice.services.AuthenticationService;
import com.everwell.dispensationservice.services.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;

@Service
public class ClientServiceImpl implements ClientService, UserDetailsService {

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private AuthenticationService authenticationService;

    private Client getClientByName(String name) {
        Client client = clientRepository.findClientByName(name);
        if(null == client) {
            throw new NotFoundException("client not found with username : " + name);
        }
        return client;
    }

    @Override
    public ClientResponse registerClient(RegisterClientRequest clientRequest) {
        Client existingClient = clientRepository.findClientByName(clientRequest.getName());
        if(null != existingClient) {
            throw new ValidationException("User already exists");
        }
        String encodedPassword = new BCryptPasswordEncoder().encode(clientRequest.getPassword());
        Client newClient = new Client(clientRequest.getName(), encodedPassword);
        clientRepository.save(newClient);
        ClientResponse response = new ClientResponse(newClient);
        response.setPassword(clientRequest.getPassword());
        return response;
    }

    @Override
    public ClientResponse getClientWithNewToken(Long id) {
        Client client = getClient(id);
        String secureClientToken = authenticationService.generateToken(client);
        ClientResponse response = new ClientResponse(client);
        response.setAuthToken(secureClientToken);
        return response;
    }

    @Override
    public Client getClient(Long id) {
        if(null == id) {
            throw new ValidationException("id cannot be null");
        }
        Client client = clientRepository.findById(id).orElse(null);
        if(null == client) {
            throw new NotFoundException("No client found with id:" + id);
        }
        return client;
    }

    @Override
    public UserDetails loadUserByUsername(String name) throws UsernameNotFoundException {
        Client client = getClientByName(name);
        return new User(client.getName(), client.getPassword(), Collections.emptyList());
    }
}
