package com.everwell.dispensationservice.services;

import com.everwell.dispensationservice.models.db.Client;
import com.everwell.dispensationservice.models.http.requests.RegisterClientRequest;
import com.everwell.dispensationservice.models.http.responses.ClientResponse;

public interface ClientService {

    ClientResponse registerClient(RegisterClientRequest clientRequest);

    ClientResponse getClientWithNewToken(Long id);

    Client getClient(Long id);
}
