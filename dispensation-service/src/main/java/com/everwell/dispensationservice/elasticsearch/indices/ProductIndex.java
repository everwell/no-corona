package com.everwell.dispensationservice.elasticsearch.indices;

import com.everwell.dispensationservice.elasticsearch.constants.ElasticConstants;
import com.everwell.dispensationservice.models.http.responses.ProductResponse;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import javax.persistence.Id;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(indexName = ElasticConstants.PRODUCT_INDEX)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProductIndex {
    @Id
    private String id;

    @Field(type = FieldType.Keyword)
    private String drugCode;

    @Field(type = FieldType.Text)
    private String productName;

    @Field(type = FieldType.Text)
    private String dosage;

    @Field(type = FieldType.Text)
    private String dosageForm;

    @Field(type = FieldType.Text)
    private String manufacturer;

    @Field(type = FieldType.Text)
    private String composition;

    @Field(type = FieldType.Keyword)
    private String category;

    public ProductResponse toProductResponse() {
        return  new ProductResponse(
                NumberUtils.toLong(id),
                drugCode,
                productName,
                dosage,
                dosageForm,
                manufacturer,
                composition,
                category);
    }
}
