package com.everwell.dispensationservice.elasticsearch.constants;

import org.elasticsearch.common.unit.Fuzziness;
import org.elasticsearch.index.query.Operator;

import java.util.*;

public class ElasticConstants {
    public static final String PRODUCT_INDEX = "products";

    public static final String PRODUCT_NAME_FIELD = "productName";

    public static final String ELASTIC_SEARCH_KEYWORD_MUST = "must";

    public static final String ELASTIC_OPERATOR_OR = "or";

    public static final String ELASTIC_OPERATOR_AND = "and";

    public static final Set<String> ELASTIC_QUERY_OPERATORS  =  Collections.unmodifiableSet(new HashSet<>(Arrays.asList(ELASTIC_OPERATOR_OR, ELASTIC_OPERATOR_AND)));

    public static final Map<Long, Fuzziness> FUZZINESS_VALUE_TO_OBJECT_MAP = Collections.unmodifiableMap( new HashMap<Long, Fuzziness>() {
        {
            put(0L, Fuzziness.ZERO);
            put(1L, Fuzziness.ONE);
            put(2L, Fuzziness.TWO);
        };
    });

    public static final Map<String, Operator> OPERATOR_VALUE_TO_OBJECT_MAP = Collections.unmodifiableMap(new HashMap<String, Operator>() {
        {
            put(ELASTIC_OPERATOR_OR, Operator.OR);
            put(ELASTIC_OPERATOR_AND, Operator.AND);
        };
    });
}
