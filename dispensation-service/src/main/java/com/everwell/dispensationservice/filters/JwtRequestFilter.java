package com.everwell.dispensationservice.filters;

import com.everwell.dispensationservice.Utils.Utils;
import com.everwell.dispensationservice.exceptions.NotFoundException;
import com.everwell.dispensationservice.exceptions.ValidationException;
import com.everwell.dispensationservice.models.Response;
import com.everwell.dispensationservice.models.db.Client;
import com.everwell.dispensationservice.services.AuthenticationService;
import com.everwell.dispensationservice.services.ClientService;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.UnsupportedJwtException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;

import static com.everwell.dispensationservice.constants.Constants.X_DS_AUTH_TOKEN;
import static com.everwell.dispensationservice.constants.Constants.X_DS_CLIENT_ID;


@Component
public class JwtRequestFilter extends OncePerRequestFilter {

    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private ClientService clientService;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
            throws ServletException, NotFoundException, IOException {
        final String requestToken = request.getHeader(X_DS_AUTH_TOKEN);
        final String requestClientId = request.getHeader(X_DS_CLIENT_ID);
        if (!StringUtils.isEmpty(requestToken) && !StringUtils.isEmpty(requestClientId)
                && null == SecurityContextHolder.getContext().getAuthentication()) {
            try {
                Client client = clientService.getClient(Long.parseLong(requestClientId));
                if(authenticationService.isValidToken(requestToken, client)) {
                    UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(client, null, Collections.emptyList());
                    usernamePasswordAuthenticationToken
                            .setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                    SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
                }
            } catch (IllegalArgumentException | UnsupportedJwtException | ValidationException e) {
                Utils.writeCustomResponseToOutputStream(response, e, HttpStatus.BAD_REQUEST);
                return;
            } catch (ExpiredJwtException e) {
                Utils.writeCustomResponseToOutputStream(response, e, HttpStatus.UNAUTHORIZED);
                return;
            } catch (NotFoundException e) {
                Utils.writeCustomResponseToOutputStream(response, e, HttpStatus.NOT_FOUND);
                return;
            } catch (Exception e) {
                Utils.writeCustomResponseToOutputStream(response, e, HttpStatus.INTERNAL_SERVER_ERROR);
                return;
            }
        }
        //WebSecurity adapter seems to disable authentication for specified urls after this filter is applied
        //Will need to figure out how to efficiently handle this for those urls which need not require authentication and send a customized error message.
        //For now this will be handled by AuthenticationEntryPoint which throws UnAuthorised exception
        else if (StringUtils.isEmpty(requestToken) || StringUtils.isEmpty(requestClientId)){
//           Utils.writeCustomResponseToOutputStream(response, new Response("Token and clientId cannot be null"), HttpStatus.BAD_REQUEST);
//           return;
        }
        chain.doFilter(request, response);
    }



}
