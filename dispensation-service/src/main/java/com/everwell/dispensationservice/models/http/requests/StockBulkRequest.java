package com.everwell.dispensationservice.models.http.requests;

import lombok.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class StockBulkRequest {
    List<StockRequest> stockData;
}
