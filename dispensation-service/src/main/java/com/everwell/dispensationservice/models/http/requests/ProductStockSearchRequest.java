package com.everwell.dispensationservice.models.http.requests;

import com.everwell.dispensationservice.exceptions.ValidationException;
import lombok.*;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class ProductStockSearchRequest {
    List<Long> productIds;
    List<Long> hierarchyMappingIds;

    public void validate() {
        if(null == productIds || productIds.isEmpty()) {
            throw new ValidationException("product id is required");
        }
        else {
            for(Long productId : productIds){
                if (null == productId) {
                    throw new ValidationException("product id cannot be null");
                }
            }
        }

        if(null == hierarchyMappingIds || hierarchyMappingIds.isEmpty()) {
            throw new ValidationException("hierarchy mapping id is required");
        }
        else{
            for(Long hierarchyMappingId : hierarchyMappingIds){
                if (null == hierarchyMappingId) {
                    throw new ValidationException("hierarchy mapping id cannot be null");
                }
            }
        }
    }
}
