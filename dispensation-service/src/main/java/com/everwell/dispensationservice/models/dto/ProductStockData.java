package com.everwell.dispensationservice.models.dto;

import lombok.*;

import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductStockData {

    private Long productId;
    private String batchNumber;
    private Date expiryDate ;
    private List<HierarchyData> hierarchyData;

    public ProductStockData(Long productId, String batchNumber, Date expiryDate) {
        this.productId = productId;
        this.batchNumber = batchNumber;
        this.expiryDate = expiryDate;
    }
}

