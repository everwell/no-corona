package com.everwell.dispensationservice.models.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Date;

@AllArgsConstructor
@Getter
public class ClientAuth {

    private Long Id;

    private String Name;

    private Date nextRefresh;
}
