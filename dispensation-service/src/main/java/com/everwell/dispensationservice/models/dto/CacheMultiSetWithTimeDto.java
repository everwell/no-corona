package com.everwell.dispensationservice.models.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class CacheMultiSetWithTimeDto {
    String key;
    String value;
    Long secondsToEndDay;
}