package com.everwell.dispensationservice.models.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import java.util.Date;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@ToString
public class DispensationProductDto {

    private Long id;

    private Long productId;

    @Setter
    private String productName;

    private Long productConfigId;

    private Long dispensationId;

    @Setter
    private String unitOfMeasurement;

    private Long numberOfUnitsIssued;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    private Date refillDate;

    private String dosingSchedule;

    @Setter
    private List<StockQuantityDto> stockData;

    @Setter
    private List<ProductInventoryLogDto> logs;

    public DispensationProductDto(Long id, Long productId, Long productConfigId, Long dispensationId, Long numberOfUnitsIssued, Date refillDate, String dosingSchedule) {
        this.id = id;
        this.productId = productId;
        this.productConfigId = productConfigId;
        this.dispensationId = dispensationId;
        this.numberOfUnitsIssued = numberOfUnitsIssued;
        this.refillDate = refillDate;
        this.dosingSchedule = dosingSchedule;
    }

}
