package com.everwell.dispensationservice.models.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Getter
@Setter
@ToString
@AllArgsConstructor
public class ProductMappingInventoryDto {

    Long productId;
    Long productConfigId;
    Long unitsIssued;
    String dosingSchedule;
    Date refillDate;
    Map<String, Long> batchNumberToQuantity;
    Map<String, String> batchNumberToExpiryDateMap;
}
