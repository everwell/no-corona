package com.everwell.dispensationservice.models.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductStockHierarchyDto {
    private Long inventoryId;
    private Long productId;
    private String batchNumber;
    private Date expiryDate ;
    private Long hierarchyId;
    private Long quantity;
}
