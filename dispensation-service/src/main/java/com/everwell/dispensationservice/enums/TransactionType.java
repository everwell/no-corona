package com.everwell.dispensationservice.enums;

public enum TransactionType {
    ISSUED,
    RETURNED,
    CREDIT,
    DEBIT,
    UPDATE
}
