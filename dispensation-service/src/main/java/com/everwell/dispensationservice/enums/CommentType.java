package com.everwell.dispensationservice.enums;

public enum CommentType {
    REASON_FOR_RETURN,
    REASON_FOR_CREDIT,
    REASON_FOR_DEBIT,
    REASON_FOR_ADD,
    REASON_FOR_UPDATE
}
