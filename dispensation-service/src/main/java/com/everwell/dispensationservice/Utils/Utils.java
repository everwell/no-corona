package com.everwell.dispensationservice.Utils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import org.apache.commons.io.IOUtils;
import com.everwell.dispensationservice.models.Response;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import javax.servlet.http.HttpServletResponse;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class Utils {

    private static final Logger LOGGER = LoggerFactory.getLogger(Utils.class);

    private static ObjectMapper objectMapper = new ObjectMapper();

    private static String[] supportedDateFormats = new String[] {"dd-MM-yyyy HH:mm:ss", "yyyy-MM-dd HH:mm:ss"};

    public static Date getCurrentDate() {
        return formatDate(new Date());
    }

    public static Date formatDate(Date date) {
        return formatDate(date, "dd-MM-yyyy HH:mm:ss");
    }

    public static Date formatDate(Date date, String format) {
        try {
            date = convertStringToDate(getFormattedDate(date, format), format);
        } catch (ParseException e) {
            LOGGER.error("unable to convert record date " + date);
        }
        return date;
    }

    public static Date convertStringToDate(String date, String format) throws ParseException {
        DateFormat dateFormat = new SimpleDateFormat(format);
        dateFormat.setLenient(false);
        return dateFormat.parse(date);
    }

    public static Date convertStringToDateWithSupportedDateFormats(String date) throws ParseException {
        return DateUtils.parseDateStrictly(date, supportedDateFormats);
    }

    public static Date convertStringToDate(String date) throws ParseException {
        return convertStringToDate(date, "dd-MM-yyyy HH:mm:ss");
    }

    public static String getFormattedDate(Date date, String format) {
        DateFormat dateFormat = new SimpleDateFormat(format);
        dateFormat.setLenient(false);
        return dateFormat.format(date);
    }

    public static Date convertStringToDateOrDefault(String date) {
        Date parsedDate = null;
        try {
            parsedDate = convertStringToDateWithSupportedDateFormats(date);
        } catch (ParseException e) {
            LOGGER.warn("[convertStringToDateOrDefault] unable to parse date");
        }
        return parsedDate;
    }

    public static long getDifferenceDays(Date d1, Date d2) {
        return getDifference(d1, d2, TimeUnit.DAYS);
    }

    public static long getDifference(Date d1, Date d2, TimeUnit timeUnit) {
        long diff = d2.getTime() - d1.getTime();
        return timeUnit.convert(diff, TimeUnit.MILLISECONDS);
    }

    public static String readRequestBody(InputStream inputStream) throws IOException {
        StringWriter writer = null;
        if (inputStream != null) {
            writer = new StringWriter();
            IOUtils.copy(inputStream, writer, "UTF-8");
        }
        return writer != null ? writer.toString() : null;
    }
    
    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static void writeCustomResponseToOutputStream(HttpServletResponse response, Exception e, HttpStatus httpStatus) throws IOException {
        writeCustomResponseToOutputStream(response, new Response(e.getMessage()), httpStatus);
    }

    public static void writeCustomResponseToOutputStream(HttpServletResponse response, Response CustomResponse, HttpStatus httpStatus) throws IOException {
        response.setContentType("application/json");
        response.setStatus(httpStatus.value());
        response.getOutputStream()
                .println(Utils.asJsonString(CustomResponse));
    }

    public static <T> T convertStringToObject(String json, Class<T> valueType) throws IOException {
        //Disabling the FAIL_ON_UNKNOWN_PROPERTIES so that in case the input json has extra properties
        //other than the properties in the Class that needs to be converted to, jackson mapper doesn't throw exception
        objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        return objectMapper.readValue(json, valueType);
    }

    public static <T> T jsonToObject (String json, TypeReference<T> typeReference) throws IOException {
        //Disabling the FAIL_ON_UNKNOWN_PROPERTIES so that in case the input json has extra properties
        //other than the properties in the Class that needs to be converted to, jackson mapper doesn't throw exception
        objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        return objectMapper.readValue(json, typeReference);
    }
}
