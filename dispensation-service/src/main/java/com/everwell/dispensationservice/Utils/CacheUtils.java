package com.everwell.dispensationservice.Utils;

import com.everwell.dispensationservice.models.dto.CacheMultiSetWithTimeDto;
import com.fasterxml.jackson.core.type.TypeReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.TimeUnit;

@Component
public class CacheUtils {
    private static Logger LOGGER = LoggerFactory.getLogger(CacheUtils.class);
    private static RedisTemplate<String, Object> redisTemplate;

    @Autowired
    public CacheUtils(RedisTemplate<String, Object> redisTemplate) {
        CacheUtils.redisTemplate = redisTemplate;
    }

    public static <T> T getFromCacheBulk(HashSet<String> key) {
        Object obj = redisTemplate.opsForValue().multiGet(key);
        return (null == obj) ? null : ((T) obj);
    }
    public static <T> T getFromCache(String key) {
        Object obj = redisTemplate.opsForValue().get(key);
        return (null == obj)?null:(T)(obj);
    }

    public static <T> T getFromCacheWithResponseType(String key, TypeReference<T> typeReference) {
        T object = null;
        try {
            object = Utils.jsonToObject(getFromCache(key), typeReference);
        } catch(Exception ignored) {

        }
        return object;
    }

    public static Boolean deleteFromCache(String key) {
        return redisTemplate.delete(key);
    }

    public static void putIntoCache(String key, Object value, Long ttl) {
        redisTemplate.opsForValue().set(key, value, ttl, TimeUnit.SECONDS);
    }

    public static <V> void putIntoCacheBulk(Map<String, V> map) {
        redisTemplate.opsForValue().multiSet(map);
    }

    public static void bulkInsertWithTtl(List<CacheMultiSetWithTimeDto> cacheMultiSetWithTimeDtoList) {
        HashMap<String, String> cacheMap = new HashMap<>();
        List<String> ttl = new ArrayList<>();
        List<String> ids = new ArrayList<>();
        cacheMultiSetWithTimeDtoList.forEach(cache -> {
            cacheMap.put(cache.getKey(), cache.getValue());
            ids.add(cache.getKey());
            ttl.add(String.valueOf(cache.getSecondsToEndDay()));
        });
        putIntoCacheBulk(cacheMap);
        setBulkExpiry(ids, ttl.toArray());
    }


    private static void setBulkExpiry(List<String> keys, Object[] argv) {
        redisEvalForNoResult("for i=1, #KEYS do redis.call('expire', KEYS[i], ARGV[i]) end", keys, argv);
    }

    private static void redisEvalForNoResult(String scriptText, List<String> keys, Object[] argv) {
        // since spring boot serializes data to - "\xac\xed\x00\x05t\x00\x0" it was not being recognized by redis as integer
        redisTemplate.execute(new DefaultRedisScript<>(scriptText), new StringRedisSerializer(), new StringRedisSerializer(), keys, argv);
    }
}