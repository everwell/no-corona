package com.everwell.dispensationservice.constants;

public class Constants {

    // 2 days
    public static Integer REFRESH_EXPIRY = 2 * 24 * 60 * 60 * 1000;

    public static final String X_DS_CLIENT_ID = "X-DS-Client-Id";

    public static final String X_DS_AUTH_TOKEN = "X-DS-Access-Token";

    public static String CREDIT_COMMENT = "Credit from external vendor";

    public static String DEBIT_COMMENT = "Debit from external vendor";

    public final static Long PRODUCT_VALIDITY_EXPIRY = 2 * 30 * 24 * 60 * 60L; // 2 months

    public final static Long DISPENSATION_ENTITY_VALIDITY = 60L * 60 * 24 * 7 * 7; // 7 Weeks

    public final static String CACHE_DISPENSATION_PRODUCT_ID = "dsp_id:";

    public final static String CACHE_ALL_PRODUCT_IDS_KEY = "dsp_all_product_ids";

    public final static String CACHE_ENTITY_DISPENSATION_KEY = "ds_entityId:%s_clientId:%s";

    public final static String DISPENSATION_FIELD_LAST_REFILL_DATE = "lastRefillDate";

    public final static String ID = "id";

    public final static String MY_MODULE = "dispensation";
    public final static String OTHER = "Other";

    public final static Long DEFAULT_TOTAL_DISPENSED_DRUGS = 0L;
}
