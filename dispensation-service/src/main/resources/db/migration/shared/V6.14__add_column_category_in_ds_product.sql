ALTER TABLE ds_product
ADD COLUMN IF NOT EXISTS category varchar(255);

UPDATE ds_product
SET category = 'prescription'
where category is null;
