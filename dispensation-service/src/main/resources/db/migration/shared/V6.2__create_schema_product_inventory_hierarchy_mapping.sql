create table if not exists ds_product_inventory_hierarchy_mapping
(
    id bigserial not null
        constraint ds_product_inventory__hierarchy_mapping_pkey
            primary key,
    inventory_id bigint not null,
    hierarchy_mapping_id bigint not null,
    client_id bigint not null,
    available_quantity bigint,
    last_updated_by bigint,
    last_updated_at timestamp
);