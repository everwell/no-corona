create table if not exists ds_dispensation
(
	id bigserial not null
		constraint ds_dispensation_pkey
			primary key,
	added_by bigint not null,
	added_date timestamp not null,
	client_id bigint,
	date_of_prescription timestamp,
	deleted boolean,
	drug_dispensed_for_days bigint,
	entity_id bigint not null,
	issue_date timestamp,
	issuing_facility bigint,
	notes varchar(1000),
	phase varchar(255),
	refill_date timestamp,
	weight bigint,
	weight_band varchar(255)
);