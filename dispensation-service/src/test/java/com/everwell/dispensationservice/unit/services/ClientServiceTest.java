package com.everwell.dispensationservice.unit.services;

import com.everwell.dispensationservice.Utils.Utils;
import com.everwell.dispensationservice.exceptions.NotFoundException;
import com.everwell.dispensationservice.exceptions.ValidationException;
import com.everwell.dispensationservice.models.db.Client;
import com.everwell.dispensationservice.models.http.requests.RegisterClientRequest;
import com.everwell.dispensationservice.models.http.responses.ClientResponse;
import com.everwell.dispensationservice.repositories.ClientRepository;
import com.everwell.dispensationservice.services.AuthenticationService;
import com.everwell.dispensationservice.services.impl.ClientServiceImpl;
import com.everwell.dispensationservice.unit.BaseTestNoSpring;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class ClientServiceTest extends BaseTestNoSpring {


    @Spy
    @InjectMocks
    ClientServiceImpl clientService;

    @MockBean
    private ClientRepository clientRepository;

    @MockBean
    private AuthenticationService authenticationService;

    @BeforeEach
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void test_get_client_success() {
        Client client = new Client(1L, "TestClient", "password", Utils.convertStringToDateOrDefault("01-09-2020 00:00:00"));
        when(clientRepository.findById(any())).thenReturn(Optional.of(client));
        Client actualClient = clientService.getClient(1L);

        assertEquals(client.getName(), actualClient.getName());
    }

    @Test
    public void test_get_client_by_name_throws_not_found_exception() {
        when(clientRepository.findById(any())).thenReturn(Optional.empty());

        assertThrows(NotFoundException.class, () -> clientService.getClient(1L));
    }

    @Test
    public void test_get_client_by_name_throws_validation_exception() {
        when(clientRepository.findById(any())).thenReturn(Optional.empty());

        assertThrows(ValidationException.class, () -> clientService.getClient(null));
    }

    @Test
    public void test_register_client_success() {
        Client expectedClient = new Client(1L, "TestClient", "password", Utils.convertStringToDateOrDefault("01-09-2020 00:00:00"));
        RegisterClientRequest request = new RegisterClientRequest("TestClient", "password");
        when(clientRepository.findClientByName(any())).thenReturn(null);
        ClientResponse response = clientService.registerClient(request);

        assertEquals(expectedClient.getName(), response.getName());
        assertEquals(expectedClient.getPassword(), response.getPassword());
    }

    @Test
    public void test_register_client_throws_validation_exception() {
        Client client = new Client(1L, "TestClient", "password", Utils.convertStringToDateOrDefault("01-09-2020 00:00:00"));
        RegisterClientRequest request = new RegisterClientRequest("TestClient", "password");
        when(clientRepository.findClientByName(any())).thenReturn(client);

        assertThrows(ValidationException.class, ()-> clientService.registerClient(request));
    }

    @Test
    public void test_get_client_with_token_success() {
        Client client = new Client(1L, "TestClient", "password", Utils.convertStringToDateOrDefault("01-09-2020 00:00:00"));
        when(clientRepository.findById(any())).thenReturn(Optional.of(client));
        when(authenticationService.generateToken(any())).thenReturn("Token");
        ClientResponse response = clientService.getClientWithNewToken(1L);

        assertEquals(client.getName(), response.getName());
        assertEquals(client.getId(), response.getId());
        assertNotNull(response.getAuthToken());
    }


}
