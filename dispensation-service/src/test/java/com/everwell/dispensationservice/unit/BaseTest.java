package com.everwell.dispensationservice.unit;

import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@SpringBootTest(classes = com.everwell.dispensationservice.DispensationServiceApplication.class)
@AutoConfigureMockMvc
public abstract class BaseTest {


}
