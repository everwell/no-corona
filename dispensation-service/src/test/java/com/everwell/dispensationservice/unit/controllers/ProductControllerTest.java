package com.everwell.dispensationservice.unit.controllers;

import com.everwell.dispensationservice.controllers.ProductController;
import com.everwell.dispensationservice.exceptions.CustomExceptionHandler;
import com.everwell.dispensationservice.models.http.requests.ProductBulkRequest;
import com.everwell.dispensationservice.models.http.requests.ProductRequest;
import com.everwell.dispensationservice.models.http.requests.ProductSearchRequest;
import com.everwell.dispensationservice.models.http.responses.ProductResponse;
import com.everwell.dispensationservice.services.ProductService;
import com.everwell.dispensationservice.models.db.Product;
import com.everwell.dispensationservice.unit.BaseTestNoSpring;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.everwell.dispensationservice.Utils.Utils.asJsonString;
import static com.everwell.dispensationservice.constants.Constants.X_DS_CLIENT_ID;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class ProductControllerTest extends BaseTestNoSpring {

    private MockMvc mockMvc;

    @Spy
    @InjectMocks
    private ProductController productController;

    @Mock
    private ProductService productService;

    @Mock
    private ApplicationEventPublisher applicationEventPublisher;

    @BeforeEach
    public void setUp() {
        mockMvc = MockMvcBuilders
                .standaloneSetup(productController)
                .setControllerAdvice(new CustomExceptionHandler())
                .build();
    }
    private final String CLIENT_ID = "1";
    private Optional<Product> product_optional_entity() {
        return Optional.of(new Product(1L, "AAA", "ProductA", "dosageA", "dosageFormA", "manufacturerA", "compositionA", "category"));
    }

    private List<Product> products() {
        List<Product> productList = new ArrayList<>();
        productList.add(new Product(1L, "AAA", "ProductA", "dosageA", "dosageFormA", "manufacturerA", "compositionA",  "category"));
        productList.add(new Product(2L, "BBB", "ProductB", "dosageB", "dosageFormB", "manufacturerB", "compositionB", "category"));
        productList.add(new Product(3L, "CCC", "ProductC", "dosageC", "dosageFormC", "manufacturerC", "compositionC", "category"));
        productList.add(new Product(4L, "DDD", "ProductD", "dosageD", "dosageFormD", "manufacturerD", "compositionD", "category"));
        productList.add(new Product(5L, "EEE", "ProductE", "dosageE", "dosageFormE", "manufacturerE", "compositionE", "category"));
        productList.add(new Product(6L, "FFF", "ProductF", "dosageF", "dosageFormF", "manufacturerF", "compositionF", "category"));
        productList.add(new Product(7L, "GGG", "ProductG", "dosageG", "dosageFormG", "manufacturerG", "compositionG", "category"));
        return productList;
    }

    @Test
    public void test_get_product() throws Exception {
        Product product = product_optional_entity().get();
        when(productService.getProduct(any())).thenReturn(product);
        mockMvc
                .perform(get("/v1/product/{id}", 1L)
                        .header(X_DS_CLIENT_ID, CLIENT_ID))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.message").value("product details fetched successfully"));
    }

    @Test
    public void test_search_products() throws Exception {
        List<Long> productIdsList = new ArrayList<>();
        List<String> drugCodesList = new ArrayList<>();
        String pattern = "pattern";
        List<ProductResponse>  productResponseList = new ArrayList<>();
        for(Product product : products()) {
            productIdsList.add(product.getId());
            drugCodesList.add(product.getDrugCode());
            productResponseList.add(new ProductResponse(product));
        }
        when(productService.getAllProductDetails(any(), any())).thenReturn(productResponseList);
        ProductSearchRequest productSearchRequest = new ProductSearchRequest(productIdsList, drugCodesList, pattern);
        mockMvc
                .perform(post("/v1/product/search")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(productSearchRequest))
                        .header(X_DS_CLIENT_ID, CLIENT_ID))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.message").value("product details fetched successfully"));
    }

    @Test
    public void test_add_product() throws Exception {
        Product product = product_optional_entity().get();
        ProductRequest productRequest = new ProductRequest(product.getProductName(), product.getDosage(), product.getDrugCode(), product.getDosageForm(), product.getManufacturer(), product.getComposition(), product.getCategory());
        doNothing().when(productService).validateProductIds(any());
        when(productService.addProduct(any())).thenReturn(product);
        mockMvc
                .perform(post("/v1/product")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(productRequest))
                        .header(X_DS_CLIENT_ID, CLIENT_ID))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.message").value("product added successfully"));
    }

    @Test
    public void test_add_product_throws_validation_exception_product_name() throws Exception {
        Product product = product_optional_entity().get();
        ProductRequest productRequest = new ProductRequest(null, product.getDosage(), product.getDrugCode(), product.getDosageForm(), product.getManufacturer(), product.getComposition(), product.getCategory());
        doNothing().when(productService).validateProductIds(any());
        when(productService.addProduct(any())).thenReturn(product);
        mockMvc
                .perform(post("/v1/product")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(productRequest))
                        .header(X_DS_CLIENT_ID, CLIENT_ID))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("product name is required"));
    }

    @Test
    public void test_add_product_throws_validation_exception_dosage_form() throws Exception {
        Product product = product_optional_entity().get();
        ProductRequest productRequest = new ProductRequest(product.getProductName(), product.getDosage(), product.getDrugCode(), null, product.getManufacturer(), product.getComposition(), product.getCategory());
        doNothing().when(productService).validateProductIds(any());
        when(productService.addProduct(any())).thenReturn(product);
        mockMvc
                .perform(post("/v1/product")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(productRequest))
                        .header(X_DS_CLIENT_ID, CLIENT_ID))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("dosage form is required"));
    }

    @Test
    public void test_add_product_throws_validation_exception_dosage() throws Exception {
        Product product = product_optional_entity().get();
        ProductRequest productRequest = new ProductRequest(product.getProductName(), null, product.getDrugCode(), product.getDosageForm(), product.getManufacturer(), product.getComposition(), product.getCategory());
        doNothing().when(productService).validateProductIds(any());
        when(productService.addProduct(any())).thenReturn(product);
        mockMvc
                .perform(post("/v1/product")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(productRequest))
                        .header(X_DS_CLIENT_ID, CLIENT_ID))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("dosage is required"));
    }

    @Test
    public void test_add_product_bulk() throws  Exception {
        List<ProductRequest> productDetails = new ArrayList<>();
        productDetails.add(new ProductRequest("ProductA", "dosageA", "AAA", "dosageFormA", "manufacturerA", "compositionA", "category"));
        productDetails.add(new ProductRequest("ProductB", "dosageB", "BBB", "dosageFormB", "manufacturerB", "compositionB", "category"));
        ProductBulkRequest productBulkRequest = new ProductBulkRequest(productDetails);
        when(productService.addProductBulk(productBulkRequest)).thenReturn(products());
        mockMvc
                .perform(post("/v1/products")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(productBulkRequest))
                        .header(X_DS_CLIENT_ID, CLIENT_ID))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.message").value("product list added successfully"));
    }

    @Test
    public void test_add_product_bulk_throws_validation_exception_null_product_details() throws  Exception {
        ProductBulkRequest productBulkRequest = new ProductBulkRequest(null);
        mockMvc
                .perform(post("/v1/products")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(productBulkRequest))
                        .header(X_DS_CLIENT_ID, CLIENT_ID))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("product details is required"));
    }

    @Test
    public void test_get_all_products() throws Exception {
        when(productService.getAllProducts()).thenReturn(new ArrayList<>());
        mockMvc
                .perform(get("/v1/products")
                        .header(X_DS_CLIENT_ID, CLIENT_ID))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.message").value("all product details fetched successfully"));
    }
}
