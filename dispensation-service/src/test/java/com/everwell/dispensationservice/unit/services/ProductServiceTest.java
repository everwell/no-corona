package com.everwell.dispensationservice.unit.services;

import com.everwell.dispensationservice.Utils.CacheUtils;
import com.everwell.dispensationservice.Utils.Utils;
import com.everwell.dispensationservice.constants.Constants;
import com.everwell.dispensationservice.elasticsearch.dto.MatchSearchDto;
import com.everwell.dispensationservice.elasticsearch.dto.ProductSearchResult;
import com.everwell.dispensationservice.elasticsearch.dto.QueryDto;
import com.everwell.dispensationservice.elasticsearch.dto.SupportedSearchMethods;
import com.everwell.dispensationservice.elasticsearch.indices.ProductIndex;
import com.everwell.dispensationservice.elasticsearch.service.ESProductService;
import com.everwell.dispensationservice.models.http.requests.ProductBulkRequest;
import com.everwell.dispensationservice.models.http.requests.ProductRequest;
import com.everwell.dispensationservice.models.http.responses.ProductResponse;
import com.everwell.dispensationservice.services.impl.ProductServiceImpl;
import com.everwell.dispensationservice.exceptions.NotFoundException;
import com.everwell.dispensationservice.exceptions.ValidationException;
import com.everwell.dispensationservice.models.db.Product;
import com.everwell.dispensationservice.models.http.requests.ProductSearchRequest;
import com.everwell.dispensationservice.repositories.ProductRepository;
import com.everwell.dispensationservice.unit.BaseTestNoSpring;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class ProductServiceTest extends BaseTestNoSpring {

    @Spy
    @InjectMocks
    private ProductServiceImpl productService;

    @Mock
    private ProductRepository productRepository;

    @Mock
    private ESProductService esProductService;

    CacheUtils cacheUtils;

    @Mock
    RedisTemplate<String, Object> redisTemplate;

    @Mock
    private ValueOperations<String, Object> valueOperations;

    @Mock
    private ApplicationEventPublisher applicationEventPublisher;

    @BeforeEach
    public void initMocks() {
        cacheUtils = new CacheUtils(redisTemplate);
        when(redisTemplate.opsForValue()).thenReturn(valueOperations);
    }

    private List<Product> products() {
        List<Product> productList = new ArrayList<>();
        productList.add(new Product(1L, "AAA", "ProductA", "dosageA", "dosageFormA", "manufacturerA", "compositionA", "category"));
        productList.add(new Product(2L, "BBB", "ProductB", "dosageB", "dosageFormB", "manufacturerB", "compositionB", "category"));
        productList.add(new Product(3L, "CCC", "ProductC", "dosageC", "dosageFormC", "manufacturerC", "compositionC", "category"));
        productList.add(new Product(4L, "DDD", "ProductD", "dosageD", "dosageFormD", "manufacturerD", "compositionD", "category"));
        productList.add(new Product(5L, "EEE", "ProductE", "dosageE", "dosageFormE", "manufacturerE", "compositionE", "category"));
        productList.add(new Product(6L, "FFF", "ProductF", "dosageF", "dosageFormF", "manufacturerF", "compositionF", "category"));
        productList.add(new Product(7L, "GGG", "ProductG", "dosageG", "dosageFormG", "manufacturerG", "compositionG", "category"));
        return productList;
    }

    @Test
    public void test_validate_product_ids() {
        when(productRepository.findAllById(any())).thenReturn(products());
        productService.validateProductIds(products().stream().map(Product::getId).collect(Collectors.toList()));
    }

    @Test
    public void test_validate_product_Ids() {
        when(productRepository.findAllById(any())).thenReturn(products());
        List<Long> productIds = products().stream().map(Product::getId).collect(Collectors.toList());
        productIds.add(8L);
        productIds.add(9L);
        assertThrows(ValidationException.class, () -> productService.validateProductIds(productIds));
    }

    @Test
    public void test_get_product_without_cache() {
        Product product = products().stream().findFirst().get();
        when(valueOperations.get(any())).thenReturn(null);
        when(productRepository.findById(any())).thenReturn(Optional.of(product));
        assertEquals(product, productService.getProduct(product.getId()));
        verify(productRepository, Mockito.times(1)).findById(any());
    }

    @Test
    public void test_get_product_with_cache() throws IOException {
        String productString = Utils.asJsonString(products().stream().findFirst().get());
        Product product = Utils.convertStringToObject(productString, Product.class);
        when(valueOperations.get(any())).thenReturn(productString);
        assertEquals(product.getId(), productService.getProduct(product.getId()).getId());
        verify(productRepository, Mockito.times(0)).findById(any());
    }

    @Test
    public void test_format_cache_io_exception() {
        String productString = " ";
        when(valueOperations.get(any())).thenReturn(productString);
        Product product = productService.formatCacheForProduct(productString);
        assertNull(product.getId());
    }

    @Test
    public void test_get_product_throws_not_found_exception() {
        when(valueOperations.get(any())).thenReturn(null);
        when(productRepository.findById(any())).thenReturn(Optional.empty());
        assertThrows(NotFoundException.class, () -> productService.getProduct(1L));
        verify(productRepository, Mockito.times(1)).findById(any());
    }

    @Test
    public void test_get_products_without_cache() {
        List<Long> productIds = products().stream().map(Product::getId).collect(Collectors.toList());
        when(valueOperations.multiGet(any())).thenReturn(null);
        when(productRepository.findAllById(any())).thenReturn(products());
        assertEquals(products().size(), productService.getProducts(productIds).size());
        verify(productRepository, Mockito.times(1)).findAllById(any());
    }

    @Test
    public void test_get_products_with_cache() {
        String productString = Utils.asJsonString(products().stream().findFirst().get());
        List<Product> productList = new ArrayList<>();
        productList.add(products().stream().findFirst().get());
        List<Long> productIds = productList.stream().map(Product::getId).collect(Collectors.toList());
        when(valueOperations.multiGet(any())).thenReturn(Collections.singletonList(productString));
        when(productRepository.findAllById(any())).thenReturn(productList);
        assertEquals(productList.size(), productService.getProducts(productIds).size());
        verify(productRepository, Mockito.times(0)).findAllById(any());
    }

    @Test
    public void test_get_all_product_details() {
        String pattern = "pattern";
        when(productRepository.findAllById(any())).thenReturn(products());
        List<Long> productIdsList = products().stream().map(Product::getId).collect(Collectors.toList());
        List<String> drugCodesList = products().stream().map(Product::getDrugCode).collect(Collectors.toList());
        ProductSearchRequest productSearchRequest = new ProductSearchRequest(productIdsList, drugCodesList, pattern);
        List<ProductResponse> notFetchedIds = productService.getAllProductDetails(productSearchRequest, 1L)
                .stream()
                .filter(productResponse -> !productIdsList.contains(productResponse.getProductId()))
                .collect(Collectors.toList());
        assertTrue(notFetchedIds.isEmpty());
    }

    @Test
    public void test_get_all_product_details_with_null_productIds() {
        String pattern = "pattern";
        when(productRepository.findAllById(any())).thenReturn(products());
        List<Long> productIdsList = products().stream().map(Product::getId).collect(Collectors.toList());
        List<String> drugCodesList = products().stream().map(Product::getDrugCode).collect(Collectors.toList());
        ProductSearchRequest productSearchRequest = new ProductSearchRequest(null, drugCodesList, pattern);
        List<ProductResponse> notFetchedIds = productService.getAllProductDetails(productSearchRequest, 1L)
                .stream()
                .filter(productResponse -> !productIdsList.contains(productResponse.getProductId()))
                .collect(Collectors.toList());
        assertTrue(notFetchedIds.isEmpty());
    }

    @Test
    public void test_get_all_product_details_with_null_drugCodes() {
        String pattern = "pattern";
        when(productRepository.findAllById(any())).thenReturn(products());
        List<Long> productIdsList = products().stream().map(Product::getId).collect(Collectors.toList());
        ProductSearchRequest productSearchRequest = new ProductSearchRequest(productIdsList, null, pattern);
        List<ProductResponse> notFetchedIds = productService.getAllProductDetails(productSearchRequest, 1L)
                .stream()
                .filter(productResponse -> !productIdsList.contains(productResponse.getProductId()))
                .collect(Collectors.toList());
        assertTrue(notFetchedIds.isEmpty());
    }

    @Test
    public void test_get_all_product_details_with_pattern() {
        String pattern = "test";
        ProductSearchRequest productSearchRequest = new ProductSearchRequest(null, null, pattern);
        ProductSearchResult productSearchResult = new ProductSearchResult(1L, Collections.singletonList(new ProductIndex("1", "ET", "vitamin", "pill", "pill", "vendor A", "test", "category")));
        when(esProductService.searchProducts(productSearchRequest)).thenReturn(productSearchResult);
        List<ProductResponse> productResponseList = productService.getAllProductDetails(productSearchRequest, 1L);
        assertEquals(2L, productResponseList.size());
        assertEquals("ET", productResponseList.get(0).getDrugCode());
        verify(esProductService, Mockito.times(2)).searchProducts(any());
    }

    @Test
    public void test_get_all_product_details_with_elastic_search_request() {
        String pattern = null;
        MatchSearchDto matchSearchDto = new MatchSearchDto("productName", "test");
        QueryDto queryDto = new QueryDto(Collections.singletonList(matchSearchDto));
        SupportedSearchMethods supportedSearchMethods = new SupportedSearchMethods(queryDto);
        ProductSearchRequest productSearchRequest = new ProductSearchRequest(null, null, pattern);
        productSearchRequest.setSearch(supportedSearchMethods);
        ProductSearchResult productSearchResult = new ProductSearchResult(1L, Collections.singletonList(new ProductIndex("1", "ET", "vitamin", "pill", "pill", "vendor A", "test", "category")));
        when(esProductService.searchProducts(productSearchRequest)).thenReturn(productSearchResult);
        List<ProductResponse> productResponseList = productService.getAllProductDetails(productSearchRequest, 1L);
        assertEquals(1L, productResponseList.size());
        assertEquals("ET", productResponseList.get(0).getDrugCode());
        verify(esProductService, Mockito.times(1)).searchProducts(any());
    }

    @Test
    public void test_get_all_product_details_with_invalid_elastic_search_request_with_null_must() {
        String pattern = "";
        SupportedSearchMethods supportedSearchMethods = new SupportedSearchMethods(null);
        ProductSearchRequest productSearchRequest = new ProductSearchRequest(null, null, pattern);
        productSearchRequest.setSearch(supportedSearchMethods);
        ProductSearchResult productSearchResult = new ProductSearchResult(1L, Collections.singletonList(new ProductIndex("1", "ET", "vitamin", "pill", "pill", "vendor A", "test", "category")));
        when(esProductService.searchProducts(productSearchRequest)).thenReturn(productSearchResult);
        List<ProductResponse> productResponseList = productService.getAllProductDetails(productSearchRequest, 1L);
        assertEquals(1L, productResponseList.size());
        assertEquals("ET", productResponseList.get(0).getDrugCode());
        verify(esProductService, Mockito.times(1)).searchProducts(any());
    }

    @Test
    public void test_get_all_product_details_with_invalid_elastic_search_request_with_empty_match_list() {
        String pattern = "";
        SupportedSearchMethods supportedSearchMethods = new SupportedSearchMethods(new QueryDto(new ArrayList<>()));
        ProductSearchRequest productSearchRequest = new ProductSearchRequest(null, null, pattern);
        productSearchRequest.setSearch(supportedSearchMethods);
        ProductSearchResult productSearchResult = new ProductSearchResult(1L, Collections.singletonList(new ProductIndex("1", "ET", "vitamin", "pill", "pill", "vendor A", "test", "category")));
        when(esProductService.searchProducts(productSearchRequest)).thenReturn(productSearchResult);
        List<ProductResponse> productResponseList = productService.getAllProductDetails(productSearchRequest, 1L);
        assertEquals(1L, productResponseList.size());
        assertEquals("ET", productResponseList.get(0).getDrugCode());
        verify(esProductService, Mockito.times(1)).searchProducts(any());
    }

    @Test
    public void test_add_product() {
        ProductRequest productRequest = new ProductRequest("ProductA", "dosageA", "AAA", "dosageFormA", "manufacturerA", "compositionA", "category");
        Product product = new Product(1L, "AAA", "ProductA", "dosageA", "dosageFormA", "manufacturerA", "compositionA", "category");
        when(productService.getProductByProductData(productRequest)).thenReturn(null);
        when(productRepository.save(any())).thenReturn(product);
        doNothing().when(esProductService).save(any());
        productService.addProduct(productRequest);
        verify(productRepository, Mockito.times(1)).save(any());
        verify(esProductService, Mockito.times(1)).save(any());
    }

    @Test
    public void test_add_product_duplicate() {
        ProductRequest productRequest = new ProductRequest("ProductA", "dosageA", "AAA", "dosageFormA", "manufacturerA", "compositionA", "category");
        Product product = new Product(1L, "AAA", "ProductA", "dosageA", "dosageFormA", "manufacturerA", "compositionA", "category");
        when(productService.getProductByProductData(productRequest)).thenReturn(product);
        productService.addProduct(productRequest);
        verify(productRepository, Mockito.times(0)).save(any());
        verify(esProductService, Mockito.times(0)).save(any());
    }

    @Test
    public void test_add_product_bulk() {
        List<ProductRequest> productDetails = new ArrayList<>();
        productDetails.add(new ProductRequest("ProductA", "dosageA", "AAA", "dosageFormA", "manufacturerA", "compositionA", "category"));
        productDetails.add(new ProductRequest("ProductB", "dosageB", "BBB", "dosageFormB", "manufacturerB", "compositionB", "category"));
        Product productOne = new Product(1L, "AAA", "ProductA", "dosageA", "dosageFormA", "manufacturerA", "compositionA", "category");
        Product productTwo = new Product(2L, "BBB", "ProductB", "dosageB", "dosageFormB", "manufacturerB", "compositionB", "category");
        List<Product> allProducts = new ArrayList<>();
        allProducts.add(productOne);
        allProducts.add(productTwo);
        when(productService.getProductByProductData(productDetails.get(0))).thenReturn(productOne);
        when(productService.getProductByProductData(productDetails.get(1))).thenReturn(null);
        when(productRepository.saveAll(any())).thenReturn(allProducts);
        doNothing().when(esProductService).saveAll(any());
        productService.addProductBulk(new ProductBulkRequest(productDetails));
        verify(productRepository, Mockito.times(1)).saveAll(any());
        verify(esProductService, Mockito.times(1)).saveAll(any());
    }

    @Test
    public void test_get_product_details_from_products() {
        List<ProductResponse> productDetails = new ArrayList<>();
        List<Product> products = new ArrayList<>();
        productDetails.add(new ProductResponse(products().stream().findFirst().get()));
        products.add(products().stream().findFirst().get());
        assertEquals(productDetails.size(), productService.getAllProductDetailsFromProducts(products).size());
    }

    @Test
    public void test_get_all_products_without_cache_entry() {
        when(valueOperations.get(any())).thenReturn(null);
        doReturn(new ArrayList<>()).when(productService).getAllProductDetailsFromProducts(any());
        List<ProductResponse> response = productService.getAllProducts();
        verify(redisTemplate, Mockito.times(2)).opsForValue();
        assertEquals(0, response.size());
    }

    @Test
    public void test_get_all_products_with_cache_entry() {
        when(valueOperations.get(any())).thenReturn(Utils.asJsonString(Arrays.asList(1L,2L,3L,4L)));
        doReturn(new ArrayList<>()).when(productService).getProducts(any());
        doReturn(new ArrayList<>()).when(productService).getAllProductDetailsFromProducts(any());
        List<ProductResponse> response = productService.getAllProducts();
        verify(redisTemplate, Mockito.times(1)).opsForValue();
        assertEquals(0, response.size());
    }

    @Test
    public void test_delete_all_product_ids_from_cache() {
        productService.deleteAllProductIdsCacheEntry();
        verify(redisTemplate, Mockito.times(1)).delete(eq(Constants.CACHE_ALL_PRODUCT_IDS_KEY));
    }

}
