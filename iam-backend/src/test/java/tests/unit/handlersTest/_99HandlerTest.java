package tests.unit.handlersTest;

import com.everwell.iam.enums.AdherenceCodeEnum;
import com.everwell.iam.handlers.SpringEventHandler;
import com.everwell.iam.handlers.impl.NNDHandler;
import com.everwell.iam.models.db.*;
import com.everwell.iam.repositories.*;
import com.everwell.iam.services.ClientService;
import com.everwell.iam.utils.CacheUtils;
import com.everwell.iam.utils.Utils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.stubbing.Answer;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.data.redis.core.RedisTemplate;
import tests.BaseTest;

import java.math.BigInteger;
import java.text.ParseException;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class _99HandlerTest extends BaseTest {
  @SpyBean
  NNDHandler nndHandler;

  @MockBean
  PhoneMapRepository phoneMapRepository;

  @MockBean
  AdhStringLogRepository adhStringLogRepository;

  @MockBean
  CallLogRepository callLogRepository;

  @MockBean
  RegistrationRepository registrationRepository;

  @MockBean
  AccessMappingRepository accessMappingRepository;

  @MockBean
  SpringEventHandler springEventHandler;

  @MockBean
  ClientService clientService;

  @Spy
  @InjectMocks
  CacheUtils cacheUtils;

  @Mock
  RedisTemplate<String, Object> redisTemplate;

  private List<AdherenceStringLog> logs = new ArrayList<>();
  private CallLogs callLogs;
  private Registration REG;
  private static Long iamId = 1L;

  @Before
  public void initialise() {
    List<AccessMapping> accessMappingList = new ArrayList<>();
    AccessMapping access= new AccessMapping(1L,1L,"123");
    accessMappingList.add(access);
    when(registrationRepository.findById(any())).thenReturn(Optional.empty());
    when(registrationRepository.save(any())).thenReturn(new Registration());
    when(accessMappingRepository.getByIamId(any())).thenReturn(accessMappingList);
    when(adhStringLogRepository.saveAll(any())).thenAnswer((Answer) invocation -> {
      logs = (List<AdherenceStringLog>) invocation.getArguments()[0];
      return logs;
    });
  }

  // test to check when we do a regenerate adherence -> pick up all call logs and generate adherence

  @Test
  public void adherenceStringCorrectnessTest_UniqueNumber_UniqueTFN_TestCase1() throws ParseException, ExecutionException, InterruptedException {
    when(phoneMapRepository.findAllActiveIamIdsByPhoneNumbers(any(), any())).thenReturn(getUniqueNumber());
    when(registrationRepository.findByIdIn(any())).thenReturn(getUniqueRegistrationWithScheduleType());
    Registration registration = new Registration(1L, 1L, Utils.convertStringToDate("2019-08-31 18:30:00"), Utils.convertStringToDate("2019-09-06 18:30:00"), "91929399495", "666666", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"));
    when(registrationRepository.findById(any())).thenReturn(Optional.of(registration));
    CompletableFuture<List<Long>> l = nndHandler.computeCompleteAdherenceString(getCalls_UniqueTFN_TestCase1(), registration);
    //waits for completable to finish
    l.get();
    String adherenceString = nndHandler.generateAdherenceString(-1L, logs, Utils.convertStringToDate("2019-08-31 18:30:00"), Utils.convertStringToDate("2019-09-06 18:30:00"), registration.getScheduleTypeId());
    assertEquals("444444", adherenceString);
  }


  @Test
  public void last_dosage_updated_on_updateAdherenceMap_recvd() throws ParseException{
    doNothing().when(springEventHandler).anyAdherenceEvent(any());
    when(registrationRepository.save(any())).thenAnswer((Answer) invocation ->{
      REG = (Registration) invocation.getArguments()[0];
      return REG;
    });
    TreeMap<Date, AdherenceCodeEnum> map = new TreeMap<>();
    map.put(Utils.convertStringToDate("2019-08-31 18:30:00"), AdherenceCodeEnum.RECEIVED);
    Registration registration = new Registration(1L, 1L, Utils.convertStringToDate("2019-08-31 18:30:00"), Utils.convertStringToDate("2019-09-06 18:30:00"), "91929399495", "666666", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"));
    CAccess cAccess = new CAccess();
    when(clientService.getClientById(anyLong())).thenReturn(cAccess);
    nndHandler.updateAdherenceMap(map, registration);

    Assert.assertNotNull(REG.getLastDosage());
    Assert.assertEquals(REG.getLastDosage(), Utils.convertStringToDate("2019-08-31 18:30:00"));
    verify(springEventHandler, Mockito.times(1)).anyAdherenceEvent(any());
  }

  @Test
  public void last_dosage_not_updated_on_updateAdherenceMap_missed() throws ParseException{
    doNothing().when(springEventHandler).anyAdherenceEvent(any());
    when(registrationRepository.save(any())).thenAnswer((Answer) invocation ->{
      REG = (Registration) invocation.getArguments()[0];
      return REG;
    });
    TreeMap<Date, AdherenceCodeEnum> map = new TreeMap<>();
    map.put(Utils.convertStringToDate("2019-08-31 18:30:00"), AdherenceCodeEnum.MISSED);
    Registration registration = new Registration(1L, 1L, Utils.convertStringToDate("2019-08-31 18:30:00"), Utils.convertStringToDate("2019-09-06 18:30:00"), "91929399495", "666666", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"));
    CAccess cAccess = new CAccess();
    when(clientService.getClientById(anyLong())).thenReturn(cAccess);
    nndHandler.updateAdherenceMap(map, registration);

    Assert.assertEquals(REG.getLastDosage(), null);
    verify(springEventHandler, Mockito.times(1)).anyAdherenceEvent(any());
  }

  @Test
  public void last_dosage_updated_to_last_day_on_updateAdherenceMap_no_info_on_stopped_reg() throws ParseException{
    doNothing().when(springEventHandler).anyAdherenceEvent(any());
    when(registrationRepository.save(any())).thenAnswer((Answer) invocation ->{
      REG = (Registration) invocation.getArguments()[0];
      return REG;
    });
    TreeMap<Date, AdherenceCodeEnum> map = new TreeMap<>();
    map.put(Utils.convertStringToDate("2019-09-05 18:30:00"), AdherenceCodeEnum.NO_INFO);
    Registration registration = new Registration(1L, 1L, Utils.convertStringToDate("2019-08-31 18:30:00"), Utils.convertStringToDate("2019-09-06 18:30:00"), "91929399495", "666466", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-09-05 18:30:00"));
    CAccess cAccess = new CAccess();
    when(clientService.getClientById(anyLong())).thenReturn(cAccess);
    nndHandler.updateAdherenceMap(map, registration);

    Assert.assertEquals(REG.getLastDosage(), Utils.convertStringToDate("2019-09-04 18:30:00"));
    verify(springEventHandler, Mockito.times(1)).anyAdherenceEvent(any());
  }

  @Test
  public void adherenceStringCorrectnessTest_UniqueNumber_UniqueTFN_TestCase2() throws ParseException, ExecutionException, InterruptedException {
    doNothing().when(springEventHandler).anyAdherenceEvent(any());
    when(phoneMapRepository.findAllActiveIamIdsByPhoneNumbers(any(), any())).thenReturn(getUniqueNumber());
    when(registrationRepository.findByIdIn(any())).thenReturn(getUniqueRegistrationWithScheduleType());
    Registration registration = new Registration(1L, 1L, Utils.convertStringToDate("2019-01-01 18:30:00"), Utils.convertStringToDate("2019-01-16 18:30:00"), "91929399495", "666666", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"));
    when(registrationRepository.findById(any())).thenReturn(Optional.of(registration));
    CompletableFuture<List<Long>> l = nndHandler.computeCompleteAdherenceString(getCalls_UniqueTFN_TestCase2(), registration);
    l.get();
    String adherenceString = nndHandler.generateAdherenceString(-1L, logs, Utils.convertStringToDate("2019-01-01 18:30:00"), Utils.convertStringToDate("2019-01-16 18:30:00"), registration.getScheduleTypeId());
    assertEquals("466646466646664", adherenceString);
    verify(springEventHandler, Mockito.times(1)).anyAdherenceEvent(any());
  }

  @Test
  public void adherenceStringCorrectnessTest_UniqueNumber_SameTFN_TestCase1() throws ParseException, ExecutionException, InterruptedException {
    doNothing().when(springEventHandler).anyAdherenceEvent(any());
    when(phoneMapRepository.findAllActiveIamIdsByPhoneNumbers(any(), any())).thenReturn(getUniqueNumber());
    when(registrationRepository.findByIdIn(any())).thenReturn(getUniqueRegistrationWithScheduleType());
    Registration registration = new Registration(1L, 1L, Utils.convertStringToDate("2018-12-31 18:30:00"), Utils.convertStringToDate("2019-09-06 18:30:00"), "91929399495", "666666", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"));
    when(registrationRepository.findById(any())).thenReturn(Optional.of(registration));
    CompletableFuture<List<Long>> l = nndHandler.computeCompleteAdherenceString(getCalls_SameTFN_TestCase1(), registration);
    l.get();
    String adherenceString = nndHandler.generateAdherenceString(-1L, logs, Utils.convertStringToDate("2018-12-31 18:30:00"), Utils.convertStringToDate("2019-01-06 18:30:00"), registration.getScheduleTypeId());
    assertEquals("4AAAAA", adherenceString);
    verify(springEventHandler, Mockito.times(1)).anyAdherenceEvent(any());
  }

  @Test
  public void adherenceStringCorrectnessTest_UniqueNumber_SameTFN_TestCase2() throws ParseException, ExecutionException, InterruptedException {
    doNothing().when(springEventHandler).anyAdherenceEvent(any());
    when(phoneMapRepository.findAllActiveIamIdsByPhoneNumbers(any(), any())).thenReturn(getUniqueNumber());
    when(registrationRepository.findByIdIn(any())).thenReturn(getUniqueRegistrationWithScheduleType());
    Registration registration = new Registration(1L, 1L, Utils.convertStringToDate("2018-12-31 18:30:00"), Utils.convertStringToDate("2019-01-06 18:30:00"), "91929399495", "666666", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"));
    when(registrationRepository.findById(any())).thenReturn(Optional.of(registration));
    CompletableFuture<List<Long>> l = nndHandler.computeCompleteAdherenceString(getCalls_SameTFN_TestCase2(), registration);
    l.get();
    String adherenceString = nndHandler.generateAdherenceString(-1L, logs, Utils.convertStringToDate("2018-12-31 18:30:00"), Utils.convertStringToDate("2019-01-06 18:30:00"), registration.getScheduleTypeId());
    assertEquals("44AAAA", adherenceString);
    verify(springEventHandler, Mockito.times(1)).anyAdherenceEvent(any());
  }

  @Test
  public void adherenceStringCorrectnessTest_UniqueNumber_SameTFN_PartialCallsBeforeStartDate() throws ParseException, ExecutionException, InterruptedException {
    doNothing().when(springEventHandler).anyAdherenceEvent(any());
    when(phoneMapRepository.findAllActiveIamIdsByPhoneNumbers(any(), any())).thenReturn(getUniqueNumber());
    when(registrationRepository.findByIdIn(any())).thenReturn(getUniqueRegistrationWithScheduleType());
    Date startDate = Utils.convertStringToDate("2019-01-01 18:30:00");
    Date endDate = Utils.convertStringToDate("2019-01-06 18:29:59");
    Registration registration = new Registration(1L, 1L, startDate, endDate, "91929399495", "66666", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"));
    when(registrationRepository.findById(any())).thenReturn(Optional.of(registration));
    CompletableFuture<List<Long>> l = nndHandler.computeCompleteAdherenceString(getCalls_SameTFN_TestCase2(), registration);
    l.get();
    String adherenceString = nndHandler.generateAdherenceString(-1L, logs, startDate, endDate, registration.getScheduleTypeId());
    assertEquals("4AAAA", adherenceString);
    verify(springEventHandler, Mockito.times(1)).anyAdherenceEvent(any());
  }

  @Test
  public void adherenceStringCorrectnessTest_UniqueNumber_SameTFN_TestCase3() throws ParseException, ExecutionException, InterruptedException {
    doNothing().when(springEventHandler).anyAdherenceEvent(any());
    when(phoneMapRepository.findAllActiveIamIdsByPhoneNumbers(any(), any())).thenReturn(getUniqueNumber());
    when(registrationRepository.findByIdIn(any())).thenReturn(getUniqueRegistrationWithScheduleType());
    Registration registration = new Registration(1L, 1L, Utils.convertStringToDate("2018-12-31 18:30:00"), Utils.convertStringToDate("2019-01-06 18:30:00"), "91929399495", "666666", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"));
    when(registrationRepository.findById(any())).thenReturn(Optional.of(registration));
    CompletableFuture<List<Long>> l = nndHandler.computeCompleteAdherenceString(getCalls_SameTFN_TestCase3(), registration);
    l.get();
    String adherenceString = nndHandler.generateAdherenceString(-1L, logs, Utils.convertStringToDate("2018-12-31 18:30:00"), Utils.convertStringToDate("2019-01-12 18:00:00"), registration.getScheduleTypeId());
    assertEquals("46646A66A66A", adherenceString);
    verify(springEventHandler, Mockito.times(1)).anyAdherenceEvent(any());
  }

  @Test
  public void adherenceStringCorrectnessTest_SharedNumber_UniqueTFN_TestCase1() throws ParseException, ExecutionException, InterruptedException {
    doNothing().when(springEventHandler).anyAdherenceEvent(any());
    when(phoneMapRepository.findAllActiveIamIdsByPhoneNumbers(any(), any())).thenReturn(getSharedNumber());
    when(registrationRepository.findByIdIn(any())).thenReturn(getRegistrations_SharedNumbersWithScheduleType());
    Registration registration = new Registration(1L, 1L, Utils.convertStringToDate("2019-08-31 18:30:00"), Utils.convertStringToDate("2019-09-06 18:30:00"), "91929399495", "666666", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"));
    when(registrationRepository.findById(any())).thenReturn(Optional.of(registration));
    CompletableFuture<List<Long>> l = nndHandler.computeCompleteAdherenceString(getCalls_UniqueTFN_TestCase1(), registration);
    //waits for completable to finish
    l.get();
    String adherenceString = nndHandler.generateAdherenceString(-1L, logs, Utils.convertStringToDate("2019-08-31 18:30:00"), Utils.convertStringToDate("2019-09-06 18:30:00"), registration.getScheduleTypeId());
    assertEquals("555555", adherenceString);
    verify(springEventHandler, Mockito.times(1)).anyAdherenceEvent(any());
  }

  @Test
  public void adherenceStringCorrectnessTest_SharedNumber_UniqueTFN_TestCase2() throws ParseException, ExecutionException, InterruptedException {
    doNothing().when(springEventHandler).anyAdherenceEvent(any());
    when(phoneMapRepository.findAllActiveIamIdsByPhoneNumbers(any(), any())).thenReturn(getSharedNumber());
    when(registrationRepository.findByIdIn(any())).thenReturn(getRegistrations_SharedNumbersWithScheduleType());
    Registration registration = new Registration(1L, 1L, Utils.convertStringToDate("2019-01-01 18:30:00"), Utils.convertStringToDate("2019-01-16 18:30:00"), "91929399495", "666666", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"));
    when(registrationRepository.findById(any())).thenReturn(Optional.of(registration));
    CompletableFuture<List<Long>> l = nndHandler.computeCompleteAdherenceString(getCalls_UniqueTFN_TestCase2(), registration);
    l.get();
    String adherenceString = nndHandler.generateAdherenceString(-1L, logs, Utils.convertStringToDate("2019-01-01 18:30:00"), Utils.convertStringToDate("2019-01-16 18:30:00"), registration.getScheduleTypeId());
    assertEquals("566656566656665", adherenceString);
    verify(springEventHandler, Mockito.times(1)).anyAdherenceEvent(any());
  }

  @Test
  public void adherenceStringCorrectnessTest_SharedNumber_SameTFN_TestCase1() throws ParseException, ExecutionException, InterruptedException {
    doNothing().when(springEventHandler).anyAdherenceEvent(any());
    when(phoneMapRepository.findAllActiveIamIdsByPhoneNumbers(any(), any())).thenReturn(getSharedNumber());
    when(registrationRepository.findByIdIn(any())).thenReturn(getRegistrations_SharedNumbersWithScheduleType());
    Registration registration = new Registration(1L, 1L, Utils.convertStringToDate("2018-12-31 18:30:00"), Utils.convertStringToDate("2019-09-06 18:30:00"), "91929399495", "666666", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"));
    when(registrationRepository.findById(any())).thenReturn(Optional.of(registration));
    CompletableFuture<List<Long>> l = nndHandler.computeCompleteAdherenceString(getCalls_SameTFN_TestCase1(), registration);
    l.get();
    String adherenceString = nndHandler.generateAdherenceString(-1L, logs, Utils.convertStringToDate("2018-12-31 18:30:00"), Utils.convertStringToDate("2019-01-06 18:30:00"), registration.getScheduleTypeId());
    assertEquals("5BBBBB", adherenceString);
    verify(springEventHandler, Mockito.times(1)).anyAdherenceEvent(any());
  }

  @Test
  public void adherenceStringCorrectnessTest_SharedNumber_SameTFN_TestCase2() throws ParseException, ExecutionException, InterruptedException {
    doNothing().when(springEventHandler).anyAdherenceEvent(any());
    when(phoneMapRepository.findAllActiveIamIdsByPhoneNumbers(any(), any())).thenReturn(getSharedNumber());
    when(registrationRepository.findByIdIn(any())).thenReturn(getRegistrations_SharedNumbersWithScheduleType());
    Registration registration = new Registration(1L, 1L, Utils.convertStringToDate("2018-12-31 18:30:00"), Utils.convertStringToDate("2019-01-06 18:30:00"), "91929399495", "666666", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"));
    when(registrationRepository.findById(any())).thenReturn(Optional.of(registration));
    CompletableFuture<List<Long>> l = nndHandler.computeCompleteAdherenceString(getCalls_SameTFN_TestCase2(), registration);
    l.get();
    String adherenceString = nndHandler.generateAdherenceString(-1L, logs, Utils.convertStringToDate("2018-12-31 18:30:00"), Utils.convertStringToDate("2019-01-06 18:30:00"), registration.getScheduleTypeId());
    assertEquals("55BBBB", adherenceString);
    verify(springEventHandler, Mockito.times(1)).anyAdherenceEvent(any());
  }

  @Test
  public void adherenceStringCorrectnessTest_SharedNumber_SameTFN_TestCase3() throws ParseException, ExecutionException, InterruptedException {
    doNothing().when(springEventHandler).anyAdherenceEvent(any());
    when(phoneMapRepository.findAllActiveIamIdsByPhoneNumbers(any(), any())).thenReturn(getSharedNumber());
    when(registrationRepository.findByIdIn(any())).thenReturn(getRegistrations_SharedNumbersWithScheduleType());
    Registration registration = new Registration(1L, 1L, Utils.convertStringToDate("2018-12-31 18:30:00"), Utils.convertStringToDate("2019-01-06 18:30:00"), "91929399495", "666666", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"));
    when(registrationRepository.findById(any())).thenReturn(Optional.of(registration));
    CompletableFuture<List<Long>> l = nndHandler.computeCompleteAdherenceString(getCalls_SameTFN_TestCase3(), registration);
    l.get();
    String adherenceString = nndHandler.generateAdherenceString(-1L, logs, Utils.convertStringToDate("2018-12-31 18:30:00"), Utils.convertStringToDate("2019-01-12 18:00:00"), 1L);
    assertEquals("56656B66B66B", adherenceString);
    verify(springEventHandler, Mockito.times(1)).anyAdherenceEvent(any());
  }

  @Test
  public void adherenceStringCorrectnessTest_SharedUniqueNumber_SameTFN_TestCase3() throws ParseException, ExecutionException, InterruptedException {
    doNothing().when(springEventHandler).anyAdherenceEvent(any());
    when(phoneMapRepository.findAllActiveIamIdsByPhoneNumbers(any(), any())).thenReturn(new ArrayList<>()).thenReturn(new ArrayList<>());
    when(registrationRepository.findByIdIn(any())).thenReturn(getRegistrations_SharedNumbersWithScheduleType()).thenReturn(getRegistrations_SharedNumbersWithScheduleType()).thenReturn(getUniqueRegistrationWithScheduleType());
    Registration registration = new Registration(1L, 1L, Utils.convertStringToDate("2018-12-31 18:30:00"), Utils.convertStringToDate("2019-01-06 18:30:00"), "91929399495", "666666", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"));
    when(registrationRepository.findById(any())).thenReturn(Optional.of(registration));
    CompletableFuture<List<Long>> l = nndHandler.computeCompleteAdherenceString(getCalls_SameTFN_TestCase3(), registration);
    l.get();
    String adherenceString = nndHandler.generateAdherenceString(-1L, logs, Utils.convertStringToDate("2018-12-31 18:30:00"), Utils.convertStringToDate("2019-01-12 18:00:00"), registration.getScheduleTypeId());
    assertEquals("56646A66A66A", adherenceString);
    verify(springEventHandler, Mockito.times(1)).anyAdherenceEvent(any());
  }

  // this is to compute correct adherence code when a call comes in

  @Test
  public void adherenceStringCorrectnessTest_IncomingCall_UniqueNumber_UniqueTFN_TestCase1() throws ParseException {
    when(adhStringLogRepository.findAllByIamIdOrderByRecordDateDescCustom(any(), any(), any())).thenReturn(new ArrayList<>());
    when(callLogRepository.getAllByIamIdOrderByClientTimeLimitByDateCustom(any(), any())).thenReturn(getCalls_UniqueTFN_TestCase1());
    Registration dummyRegistration = new Registration(1L, 1L, Utils.convertStringToDate("2019-09-09 18:30:00"), null, "1", "66", Utils.convertStringToDate("2019-01-01 12:23:26"), Utils.convertStringToDate("2019-01-10 12:23:26"));
    List<AdherenceStringLog> logs = nndHandler.computeSingleAdherenceCodeForDay(Collections.singletonList(dummyRegistration), Utils.getCurrentDate());
    String adhString = logs.stream().map(AdherenceStringLog::getValue).map(String::valueOf).collect(Collectors.joining());
    assertEquals("4", adhString);
  }

  @Test
  public void adherenceStringCorrectnessTest_IncomingCall_UniqueNumber_UniqueTFN_TestCase2() throws ParseException {
    when(adhStringLogRepository.findAllByIamIdOrderByRecordDateDescCustom(any(), any(), any())).thenReturn(new ArrayList<>());
    when(callLogRepository.getAllByIamIdOrderByClientTimeLimitByDateCustom(any(), any())).thenReturn(getCalls_UniqueTFN_TestCase2());
    Registration dummyRegistration = new Registration(1L, 1L, Utils.convertStringToDate("2019-09-09 18:30:00"), null, "1", "66", Utils.convertStringToDate("2019-01-01 12:23:26"), Utils.convertStringToDate("2019-01-10 12:23:26"));
    List<AdherenceStringLog> logs = nndHandler.computeSingleAdherenceCodeForDay(Collections.singletonList(dummyRegistration), Utils.convertStringToDate("2019-01-16 12:23:26"));
    String adhString = logs.stream().map(AdherenceStringLog::getValue).map(String::valueOf).collect(Collectors.joining());
    assertEquals("4", adhString);
  }

  @Test
  public void adherenceStringCorrectnessTest_IncomingCall_UniqueNumber_SameTFN_TestCase1() throws ParseException {
    when(adhStringLogRepository.findAllByIamIdOrderByRecordDateDescCustom(any(), any(), any())).thenReturn(new ArrayList<>());
    when(callLogRepository.getAllByIamIdOrderByClientTimeLimitByDateCustom(any(), any())).thenReturn(getCalls_SameTFN_TestCase1());
    Registration dummyRegistration = new Registration(1L, 1L, Utils.convertStringToDate("2019-09-09 18:30:00"), null, "1", "66", Utils.convertStringToDate("2019-01-01 12:23:26"), Utils.convertStringToDate("2019-01-10 12:23:26"));
    List<AdherenceStringLog> logs = nndHandler.computeSingleAdherenceCodeForDay(Collections.singletonList(dummyRegistration), Utils.convertStringToDate("2019-01-06 12:23:26"));
    String adhString = logs.stream().map(AdherenceStringLog::getValue).map(String::valueOf).collect(Collectors.joining());
    assertEquals("A", adhString);
  }

  @Test
  public void adherenceStringCorrectnessTest_IncomingCall_UniqueNumber_SameTFN_TestCase2() throws ParseException {
    List<AdherenceStringLog> l = new ArrayList<>();
    l.add(new AdherenceStringLog(-1L, '4'));
    when(adhStringLogRepository.findAllByIamIdOrderByRecordDateDescCustom(any(), any(), any())).thenReturn(l);
    when(callLogRepository.getAllByIamIdOrderByClientTimeLimitByDateCustom(any(), any())).thenReturn(getCalls_SameTFN_TestCase4());
    Registration dummyRegistration = new Registration(1L, 1L, Utils.convertStringToDate("2019-09-09 18:30:00"), null, "1", "66", Utils.convertStringToDate("2019-01-01 12:23:26"), Utils.convertStringToDate("2019-01-10 12:23:26"));
    List<AdherenceStringLog> logs = nndHandler.computeSingleAdherenceCodeForDay(Collections.singletonList(dummyRegistration), Utils.convertStringToDate("2019-01-12 12:23:26"));
    String adhString = logs.stream().map(AdherenceStringLog::getValue).map(String::valueOf).collect(Collectors.joining());
    // flag previous day, and calculate for today
    assertEquals("AA", adhString);
  }

  @Test
  public void adherenceStringCorrectnessTest_IncomingCall_SharedNumber_SameTFN_TestCase1() throws ParseException {
    List<AdherenceStringLog> l = new ArrayList<>();
    l.add(new AdherenceStringLog(-1L, '4'));
    when(adhStringLogRepository.findAllByIamIdOrderByRecordDateDescCustom(any(), any(), any())).thenReturn(l);
    when(callLogRepository.getAllByIamIdOrderByClientTimeLimitByDateCustom(any(), any())).thenReturn(getCalls_SameTFN_TestCase4());
    Registration dummyRegistration1 = new Registration(1L, 1L, Utils.convertStringToDate("2019-09-09 18:30:00"), null, "1", "66", Utils.convertStringToDate("2019-01-01 12:23:26"), Utils.convertStringToDate("2019-01-10 12:23:26"));
    Registration dummyRegistration2 = new Registration(2L, 1L, Utils.convertStringToDate("2019-09-09 18:30:00"), null, "1", "66", Utils.convertStringToDate("2019-01-01 12:23:26"), Utils.convertStringToDate("2019-01-10 12:23:26"));
    List<AdherenceStringLog> logs = nndHandler.computeSingleAdherenceCodeForDay(Arrays.asList(dummyRegistration1, dummyRegistration2), Utils.convertStringToDate("2019-01-12 12:23:26"));
    String adhString = logs.stream().map(AdherenceStringLog::getValue).map(String::valueOf).collect(Collectors.joining());
    // flag previous day, and calculate for today - for 2 iam ids
    assertEquals("ABAB", adhString);
  }

  private List<BigInteger> getUniqueNumber() {
    //one item in list (i.e one iamId makes a number unique)
    List<BigInteger> ids = new ArrayList<>();
    ids.add(new BigInteger("1"));
    return ids;
  }

  private List<Registration> getUniqueRegistrationWithScheduleType() {
    List<Registration> registrations = new ArrayList<>();
    registrations.add(new Registration(1L, 1L));
    return registrations;
  }

  private List<BigInteger> getSharedNumber() {
    //more than one id (i.e > 1 iamIds makes a number shared)
    List<BigInteger> ids = new ArrayList<>();
    ids.add(new BigInteger("1"));
    ids.add(new BigInteger("2"));
    return ids;
  }

  private List<Registration> getRegistrations_SharedNumbersWithScheduleType() {
    List<Registration> registrations = new ArrayList<>();
    registrations.add(new Registration(1L, 1L));
    registrations.add(new Registration(2L, 1L));
    return  registrations;
  }

  private List<CallLogs> getCalls_UniqueTFN_TestCase1() throws ParseException {
    List<CallLogs> callLogs = new ArrayList<>();
    callLogs.add(new CallLogs("1", "?", Utils.convertStringToDate("2019-09-01 02:00:26"), Utils.convertStringToDate("2019-08-31 21:30:26"), "1"));
    callLogs.add(new CallLogs("2", "?", Utils.convertStringToDate("2019-09-02 21:30:26"), Utils.convertStringToDate("2019-09-02 16:00:26"), "1"));
    callLogs.add(new CallLogs("3", "?", Utils.convertStringToDate("2019-09-03 21:23:26"), Utils.convertStringToDate("2019-09-03 16:00:26"), "1"));
    callLogs.add(new CallLogs("4", "?", Utils.convertStringToDate("2019-09-04 03:00:26"), Utils.convertStringToDate("2019-09-03 22:30:26"), "1"));
    callLogs.add(new CallLogs("5", "?", Utils.convertStringToDate("2019-09-05 21:23:26"), Utils.convertStringToDate("2019-09-05 16:00:26"), "1"));
    callLogs.add(new CallLogs("6", "?", Utils.convertStringToDate("2019-09-06 17:23:26"), Utils.convertStringToDate("2019-09-06 12:00:26"), "1"));
    return callLogs;
  }

  private List<CallLogs> getCalls_UniqueTFN_TestCase2() throws ParseException {
    List<CallLogs> callLogs = new ArrayList<>();
    callLogs.add(new CallLogs("1", "?", Utils.convertStringToDate("2019-01-02 03:23:26") /* for reference this is client time */, Utils.convertStringToDate("2019-01-01 22:23:26") /* this is client time converted roughly to UTC */, "1"));
    callLogs.add(new CallLogs("1", "?", Utils.convertStringToDate("2019-01-02 03:23:26"), Utils.convertStringToDate("2019-01-01 22:23:26"), "1"));
    callLogs.add(new CallLogs("1", "?", Utils.convertStringToDate("2019-01-02 03:23:26"), Utils.convertStringToDate("2019-01-01 2:23:26"), "1"));

    callLogs.add(new CallLogs("1", "?", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 07:23:26"), "1"));
    callLogs.add(new CallLogs("2", "?", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 07:23:26"), "1"));
    callLogs.add(new CallLogs("2", "?", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 07:23:26"), "1"));

    callLogs.add(new CallLogs("2", "?", Utils.convertStringToDate("2019-01-08 12:23:26"), Utils.convertStringToDate("2019-01-08 07:23:26"), "1"));
    callLogs.add(new CallLogs("3", "?", Utils.convertStringToDate("2019-01-08 12:23:26"), Utils.convertStringToDate("2019-01-08 07:23:26"), "1"));
    callLogs.add(new CallLogs("3", "?", Utils.convertStringToDate("2019-01-08 12:23:26"), Utils.convertStringToDate("2019-01-08 07:23:26"), "1"));

    callLogs.add(new CallLogs("5", "?", Utils.convertStringToDate("2019-01-12 02:23:26"), Utils.convertStringToDate("2019-01-11 21:00:26"), "1"));
    callLogs.add(new CallLogs("6", "?", Utils.convertStringToDate("2019-01-16 12:23:26"), Utils.convertStringToDate("2019-01-16 07:23:26"), "1"));
    return callLogs;
  }

  private List<CallLogs> getCalls_SameTFN_TestCase1() throws ParseException {
    List<CallLogs> callLogs = new ArrayList<>();
    callLogs.add(new CallLogs("1", "?", Utils.convertStringToDate("2019-01-01 02:23:26"), Utils.convertStringToDate("2018-12-31 21:00:26"), "1"));
    callLogs.add(new CallLogs("1", "?", Utils.convertStringToDate("2019-01-02 12:23:26"), Utils.convertStringToDate("2019-01-02 07:23:26"), "1"));
    callLogs.add(new CallLogs("1", "?", Utils.convertStringToDate("2019-01-03 12:23:26"), Utils.convertStringToDate("2019-01-03 07:23:26"), "1"));
    callLogs.add(new CallLogs("1", "?", Utils.convertStringToDate("2019-01-04 12:23:26"), Utils.convertStringToDate("2019-01-04 07:23:26"), "1"));
    callLogs.add(new CallLogs("1", "?", Utils.convertStringToDate("2019-01-05 12:23:26"), Utils.convertStringToDate("2019-01-05 07:23:26"), "1"));
    callLogs.add(new CallLogs("1", "?", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 07:23:26"), "1"));
    return callLogs;
  }

  private List<CallLogs> getCalls_SameTFN_TestCase2() throws ParseException {
    List<CallLogs> callLogs = new ArrayList<>();
    callLogs.add(new CallLogs("1", "?", Utils.convertStringToDate("2019-01-01 02:23:26"), Utils.convertStringToDate("2018-12-31 21:00:26"), "1"));
    callLogs.add(new CallLogs("1", "?", Utils.convertStringToDate("2019-01-01 12:23:26"), Utils.convertStringToDate("2019-01-01 07:23:26"), "1"));
    callLogs.add(new CallLogs("1", "?", Utils.convertStringToDate("2019-01-02 12:23:26"), Utils.convertStringToDate("2019-01-02 07:23:26"), "1"));
    callLogs.add(new CallLogs("2", "?", Utils.convertStringToDate("2019-01-02 12:23:26"), Utils.convertStringToDate("2019-01-02 07:23:26"), "1"));
    callLogs.add(new CallLogs("2", "?", Utils.convertStringToDate("2019-01-03 12:23:26"), Utils.convertStringToDate("2019-01-03 07:23:26"), "1"));
    callLogs.add(new CallLogs("2", "?", Utils.convertStringToDate("2019-01-03 12:23:26"), Utils.convertStringToDate("2019-01-03 07:23:26"), "1"));
    callLogs.add(new CallLogs("2", "?", Utils.convertStringToDate("2019-01-04 12:23:26"), Utils.convertStringToDate("2019-01-04 07:23:26"), "1"));
    callLogs.add(new CallLogs("2", "?", Utils.convertStringToDate("2019-01-05 12:23:26"), Utils.convertStringToDate("2019-01-05 07:23:26"), "1"));
    callLogs.add(new CallLogs("2", "?", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 07:23:26"), "1"));
    return callLogs;
  }

  private List<CallLogs> getCalls_SameTFN_TestCase3() throws ParseException {
    List<CallLogs> callLogs = new ArrayList<>();
    callLogs.add(new CallLogs("1", "?", Utils.convertStringToDate("2019-01-01 02:23:26"), Utils.convertStringToDate("2018-12-31 21:00:26"), "1"));
    callLogs.add(new CallLogs("1", "?", Utils.convertStringToDate("2019-01-01 02:23:26"), Utils.convertStringToDate("2018-12-31 21:00:26"), "1"));

    callLogs.add(new CallLogs("1", "?", Utils.convertStringToDate("2019-01-04 12:23:26"), Utils.convertStringToDate("2019-01-04 07:23:26"), "1"));
    callLogs.add(new CallLogs("2", "?", Utils.convertStringToDate("2019-01-04 12:23:26"), Utils.convertStringToDate("2019-01-04 07:23:26"), "1"));

    callLogs.add(new CallLogs("2", "?", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 07:23:26"), "1"));
    callLogs.add(new CallLogs("2", "?", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 07:23:26"), "1"));
    callLogs.add(new CallLogs("2", "?", Utils.convertStringToDate("2019-01-09 12:23:26"), Utils.convertStringToDate("2019-01-09 07:23:26"), "1"));
    callLogs.add(new CallLogs("2", "?", Utils.convertStringToDate("2019-01-09 12:23:26"), Utils.convertStringToDate("2019-01-09 07:23:26"), "1"));
    callLogs.add(new CallLogs("2", "?", Utils.convertStringToDate("2019-01-12 12:23:26"), Utils.convertStringToDate("2019-01-12 07:23:26"), "1"));
    return callLogs;
  }

  private List<CallLogs> getCalls_SameTFN_TestCase4() throws ParseException {
    List<CallLogs> callLogs = new ArrayList<>();
    callLogs.add(new CallLogs("1", "?", Utils.convertStringToDate("2019-01-01 02:23:26"), Utils.convertStringToDate("2018-12-31 21:00:26"), "1"));
    callLogs.add(new CallLogs("1", "?", Utils.convertStringToDate("2019-01-01 02:23:26"), Utils.convertStringToDate("2018-12-31 21:00:26"), "1"));

    callLogs.add(new CallLogs("1", "?", Utils.convertStringToDate("2019-01-04 12:23:26"), Utils.convertStringToDate("2019-01-04 07:23:26"), "1"));
    callLogs.add(new CallLogs("2", "?", Utils.convertStringToDate("2019-01-04 12:23:26"), Utils.convertStringToDate("2019-01-04 07:23:26"), "1"));

    callLogs.add(new CallLogs("2", "?", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 07:23:26"), "1"));
    callLogs.add(new CallLogs("2", "?", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 07:23:26"), "1"));

    callLogs.add(new CallLogs("2", "?", Utils.convertStringToDate("2019-01-12 12:23:26"), Utils.convertStringToDate("2019-01-12 07:23:26"), "1"));
    return callLogs;
  }

  @Test
  public void testDeleteRegistration()
  {
    doNothing().when(springEventHandler).regenerateAdherenceEvent(any());
    List<PhoneMap> phoneMaps = Arrays.asList(
            new PhoneMap("9988799887", iamId)
    );
    when(phoneMapRepository.findAllByIamIdIn(Collections.singleton(iamId))).thenReturn(phoneMaps);

    doNothing().when(phoneMapRepository).deleteAllByIamIdIn(Collections.singleton(iamId));
    when(phoneMapRepository.getActiveIamIdsByPhoneNumber(anyList())).thenReturn(Collections.singletonList(BigInteger.valueOf(iamId)));
    nndHandler.deleteRegistration(Collections.singleton(iamId));

    verify(springEventHandler, Mockito.times(1)).regenerateAdherenceEvent(any());
    verify(phoneMapRepository, Mockito.times(1)).deleteAllByIamIdIn(anySet());
  }

  //return all dummy phones
  private List<PhoneMap> getAllDummyPhoneNumbers() {
    List<PhoneMap> phoneMaps = new ArrayList<>();
    phoneMaps.add(new PhoneMap("910000000000", 1L));
    phoneMaps.add(new PhoneMap("911234567890", 1L));
    phoneMaps.add(new PhoneMap("912111111111", 1L));
    phoneMaps.add(new PhoneMap("913111111111", 1L));
    phoneMaps.add(new PhoneMap("914111111111", 1L));
    phoneMaps.add(new PhoneMap("915111111111", 1L));
    phoneMaps.add(new PhoneMap("911111111111", 1L));
    return phoneMaps;
  }

  //return partial dummy phones
  private List<PhoneMap> getPartialDummyPhoneNumbers() {
    List<PhoneMap> phoneMaps = new ArrayList<>();
    phoneMaps.add(new PhoneMap("910000000000", 1L));
    phoneMaps.add(new PhoneMap("911234567890", 1L));
    phoneMaps.add(new PhoneMap("918826728088", 1L));
    return phoneMaps;
  }

  // case 1: when all phones of entity to delete are just dummy phones
  @Test
  public void preDeletion_AllDummyPhones() throws ParseException {
    doNothing().when(springEventHandler).regenerateAdherenceEvent(any());
    Registration registration = new Registration(1L, 1L, Utils.convertStringToDate("2019-01-01 18:30:00"), Utils.convertStringToDate("2019-01-16 18:30:00"), "91929399495", "666666", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"));
    when(phoneMapRepository.findAllByIamId(any())).thenReturn(getAllDummyPhoneNumbers());
    doNothing().when(phoneMapRepository).deactivatePhonesByIamIds(anySet(), any());
    nndHandler.preDeletion(registration);

    //should not happen
    verify(springEventHandler, Mockito.times(0)).regenerateAdherenceEvent(any());
    // deactivate must happen regardless of dummy phone or not
    verify(phoneMapRepository, Mockito.times(1)).deactivatePhonesByIamIds(anySet(), any());
  }

  // case 2: when some phones of entity to delete are dummy phones
  @Test
  public void preDeletion_PartialDummyPhones() throws ParseException {
    doNothing().when(springEventHandler).regenerateAdherenceEvent(any());
    String nonDummyPhNo = "918826728088";
    Registration registration = new Registration(1L, 1L, Utils.convertStringToDate("2019-01-01 18:30:00"), Utils.convertStringToDate("2019-01-16 18:30:00"), "91929399495", "666666", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"));
    when(phoneMapRepository.findAllByIamId(any())).thenReturn(getPartialDummyPhoneNumbers());
    List<BigInteger> bigIntegers = new ArrayList<>();
    when(phoneMapRepository.getActiveIamIdsByPhoneNumber(any())).thenReturn(bigIntegers);
    doNothing().when(phoneMapRepository).deactivatePhonesByIamIds(anySet(), any());
    when(cacheUtils.hasKey(eq(nonDummyPhNo))).thenReturn(true);
    nndHandler.preDeletion(registration);

    //should happen
    verify(springEventHandler, Mockito.times(1)).regenerateAdherenceEvent(any());
    // deactivate must happen regardless of dummy phone or not
    verify(phoneMapRepository, Mockito.times(1)).deactivatePhonesByIamIds(anySet(), any());
  }

  @Test
  public void testDeleteRegistration_EmptyIAMs()
  {
    doNothing().when(springEventHandler).regenerateAdherenceEvent(any());
    List<PhoneMap> phoneMaps = Arrays.asList(
            new PhoneMap("9988799887", iamId)
    );
    when(phoneMapRepository.findAllByIamIdIn(Collections.singleton(iamId))).thenReturn(phoneMaps);

    doNothing().when(phoneMapRepository).deleteAllByIamIdIn(Collections.singleton(iamId));
    when(phoneMapRepository.getActiveIamIdsByPhoneNumber(anyList())).thenReturn(new ArrayList<>());

    nndHandler.deleteRegistration(Collections.singleton(iamId));

    //should not happen
    verify(springEventHandler, Mockito.times(0)).regenerateAdherenceEvent(any());

    verify(phoneMapRepository, Mockito.times(1)).deleteAllByIamIdIn(anySet());

  }

}
