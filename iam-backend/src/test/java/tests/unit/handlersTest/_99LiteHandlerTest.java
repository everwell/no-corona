package tests.unit.handlersTest;

import com.everwell.iam.enums.AdherenceCodeEnum;
import com.everwell.iam.handlers.SpringEventHandler;
import com.everwell.iam.handlers.impl.NNDHandler;
import com.everwell.iam.handlers.impl.NNDLiteHandler;
import com.everwell.iam.models.db.*;
import com.everwell.iam.repositories.*;
import com.everwell.iam.utils.CacheUtils;
import com.everwell.iam.utils.Utils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.stubbing.Answer;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.data.redis.core.RedisTemplate;
import tests.BaseTest;

import java.math.BigInteger;
import java.text.ParseException;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class _99LiteHandlerTest extends BaseTest {
    @SpyBean
    NNDLiteHandler nndLiteHandler;

    @MockBean
    PhoneMapRepository phoneMapRepository;

    @MockBean
    AdhStringLogRepository adhStringLogRepository;

    @MockBean
    CallLogRepository callLogRepository;

    @MockBean
    RegistrationRepository registrationRepository;

    @MockBean
    AccessMappingRepository accessMappingRepository;

    @MockBean
    SpringEventHandler springEventHandler;

    private List<AdherenceStringLog> logs = new ArrayList<>();

    @Before
    public void initialise() {
        List<AccessMapping> accessMappingList = new ArrayList<>();
        AccessMapping access= new AccessMapping(1L,1L,"123");
        accessMappingList.add(access);
        when(registrationRepository.findById(any())).thenReturn(Optional.empty());
        when(registrationRepository.save(any())).thenReturn(new Registration());
        when(accessMappingRepository.getByIamId(any())).thenReturn(accessMappingList);
        when(adhStringLogRepository.saveAll(any())).thenAnswer((Answer) invocation -> {
            logs = (List<AdherenceStringLog>) invocation.getArguments()[0];
            return logs;
        });
    }

    @Test
    public void adherenceStringCorrectnessTest_UniqueNumber_UniqueTFN_TestCase1() throws ParseException, ExecutionException, InterruptedException {
        when(phoneMapRepository.findAllActiveIamIdsByPhoneNumbers(any(), any())).thenReturn(getUniqueNumber());
        when(registrationRepository.findByIdIn(any())).thenReturn(getUniqueRegistrationWithScheduleType());
        Registration registration = new Registration(1L, 6L, Utils.convertStringToDate("2019-08-31 18:30:00"), Utils.convertStringToDate("2019-09-06 18:30:00"), "91929399495", "666666", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"));
        when(registrationRepository.findById(any())).thenReturn(Optional.of(registration));
        CompletableFuture<List<Long>> l = nndLiteHandler.computeCompleteAdherenceString(getCalls_UniqueTFN_TestCase1(), registration);
        //waits for completable to finish
        l.get();
        String adherenceString = nndLiteHandler.generateAdherenceString(-1L, logs, Utils.convertStringToDate("2019-08-31 18:30:00"), Utils.convertStringToDate("2019-09-06 18:30:00"), registration.getScheduleTypeId());
        assertEquals("444444", adherenceString);
    }

    @Test
    public void adherenceStringCorrectnessTest_UniqueNumber_UniqueTFN_TestCase2() throws ParseException, ExecutionException, InterruptedException {
        doNothing().when(springEventHandler).anyAdherenceEvent(any());
        when(phoneMapRepository.findAllActiveIamIdsByPhoneNumbers(any(), any())).thenReturn(getUniqueNumber());
        when(registrationRepository.findByIdIn(any())).thenReturn(getUniqueRegistrationWithScheduleType());
        Registration registration = new Registration(1L, 6L, Utils.convertStringToDate("2019-01-01 18:30:00"), Utils.convertStringToDate("2019-01-16 18:30:00"), "91929399495", "666666", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"));
        when(registrationRepository.findById(any())).thenReturn(Optional.of(registration));
        CompletableFuture<List<Long>> l = nndLiteHandler.computeCompleteAdherenceString(getCalls_UniqueTFN_TestCase2(), registration);
        l.get();
        String adherenceString = nndLiteHandler.generateAdherenceString(-1L, logs, Utils.convertStringToDate("2019-01-01 18:30:00"), Utils.convertStringToDate("2019-01-16 18:30:00"), registration.getScheduleTypeId());
        assertEquals("466646466646664", adherenceString);
        verify(springEventHandler, Mockito.times(1)).anyAdherenceEvent(any());
    }

    @Test
    public void adherenceStringCorrectnessTest_UniqueNumber_SameTFN_TestCase1() throws ParseException, ExecutionException, InterruptedException {
        doNothing().when(springEventHandler).anyAdherenceEvent(any());
        when(phoneMapRepository.findAllActiveIamIdsByPhoneNumbers(any(), any())).thenReturn(getUniqueNumber());
        when(registrationRepository.findByIdIn(any())).thenReturn(getUniqueRegistrationWithScheduleType());
        Registration registration = new Registration(1L, 6L, Utils.convertStringToDate("2018-12-31 18:30:00"), Utils.convertStringToDate("2019-09-06 18:30:00"), "91929399495", "666666", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"));
        when(registrationRepository.findById(any())).thenReturn(Optional.of(registration));
        CompletableFuture<List<Long>> l = nndLiteHandler.computeCompleteAdherenceString(getCalls_SameTFN_TestCase1(), registration);
        l.get();
        String adherenceString = nndLiteHandler.generateAdherenceString(-1L, logs, Utils.convertStringToDate("2018-12-31 18:30:00"), Utils.convertStringToDate("2019-01-06 18:30:00"), registration.getScheduleTypeId());
        assertEquals("444444", adherenceString);
        verify(springEventHandler, Mockito.times(1)).anyAdherenceEvent(any());
    }

    @Test
    public void adherenceStringCorrectnessTest_UniqueNumber_SameTFN_TestCase2() throws ParseException, ExecutionException, InterruptedException {
        doNothing().when(springEventHandler).anyAdherenceEvent(any());
        when(phoneMapRepository.findAllActiveIamIdsByPhoneNumbers(any(), any())).thenReturn(getUniqueNumber());
        when(registrationRepository.findByIdIn(any())).thenReturn(getUniqueRegistrationWithScheduleType());
        Registration registration = new Registration(1L, 6L, Utils.convertStringToDate("2018-12-31 18:30:00"), Utils.convertStringToDate("2019-01-06 18:30:00"), "91929399495", "666666", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"));
        when(registrationRepository.findById(any())).thenReturn(Optional.of(registration));
        CompletableFuture<List<Long>> l = nndLiteHandler.computeCompleteAdherenceString(getCalls_SameTFN_TestCase2(), registration);
        l.get();
        String adherenceString = nndLiteHandler.generateAdherenceString(-1L, logs, Utils.convertStringToDate("2018-12-31 18:30:00"), Utils.convertStringToDate("2019-01-06 18:30:00"), registration.getScheduleTypeId());
        assertEquals("444444", adherenceString);
        verify(springEventHandler, Mockito.times(1)).anyAdherenceEvent(any());
    }

    @Test
    public void adherenceStringCorrectnessTest_UniqueNumber_SameTFN_TestCase3() throws ParseException, ExecutionException, InterruptedException {
        doNothing().when(springEventHandler).anyAdherenceEvent(any());
        when(phoneMapRepository.findAllActiveIamIdsByPhoneNumbers(any(), any())).thenReturn(getUniqueNumber());
        when(registrationRepository.findByIdIn(any())).thenReturn(getUniqueRegistrationWithScheduleType());
        Registration registration = new Registration(1L, 6L, Utils.convertStringToDate("2018-12-31 18:30:00"), Utils.convertStringToDate("2019-01-06 18:30:00"), "91929399495", "666666", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"));
        when(registrationRepository.findById(any())).thenReturn(Optional.of(registration));
        CompletableFuture<List<Long>> l = nndLiteHandler.computeCompleteAdherenceString(getCalls_SameTFN_TestCase3(), registration);
        l.get();
        String adherenceString = nndLiteHandler.generateAdherenceString(-1L, logs, Utils.convertStringToDate("2018-12-31 18:30:00"), Utils.convertStringToDate("2019-01-12 18:00:00"), registration.getScheduleTypeId());
        assertEquals("466464664664", adherenceString);
        verify(springEventHandler, Mockito.times(1)).anyAdherenceEvent(any());
    }

    @Test
    public void adherenceStringCorrectnessTest_SharedNumber_UniqueTFN_TestCase1() throws ParseException, ExecutionException, InterruptedException {
        doNothing().when(springEventHandler).anyAdherenceEvent(any());
        when(phoneMapRepository.findAllActiveIamIdsByPhoneNumbers(any(), any())).thenReturn(getSharedNumber());
        when(registrationRepository.findByIdIn(any())).thenReturn(getRegistrations_SharedNumbersWithScheduleType());
        Registration registration = new Registration(1L, 6L, Utils.convertStringToDate("2019-08-31 18:30:00"), Utils.convertStringToDate("2019-09-06 18:30:00"), "91929399495", "666666", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"));
        when(registrationRepository.findById(any())).thenReturn(Optional.of(registration));
        CompletableFuture<List<Long>> l = nndLiteHandler.computeCompleteAdherenceString(getCalls_UniqueTFN_TestCase1(), registration);
        //waits for completable to finish
        l.get();
        String adherenceString = nndLiteHandler.generateAdherenceString(-1L, logs, Utils.convertStringToDate("2019-08-31 18:30:00"), Utils.convertStringToDate("2019-09-06 18:30:00"), registration.getScheduleTypeId());
        assertEquals("555555", adherenceString);
        verify(springEventHandler, Mockito.times(1)).anyAdherenceEvent(any());
    }

    @Test
    public void adherenceStringCorrectnessTest_SharedNumber_UniqueTFN_TestCase2() throws ParseException, ExecutionException, InterruptedException {
        doNothing().when(springEventHandler).anyAdherenceEvent(any());
        when(phoneMapRepository.findAllActiveIamIdsByPhoneNumbers(any(), any())).thenReturn(getSharedNumber());
        when(registrationRepository.findByIdIn(any())).thenReturn(getRegistrations_SharedNumbersWithScheduleType());
        Registration registration = new Registration(1L, 6L, Utils.convertStringToDate("2019-01-01 18:30:00"), Utils.convertStringToDate("2019-01-16 18:30:00"), "91929399495", "666666", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"));
        when(registrationRepository.findById(any())).thenReturn(Optional.of(registration));
        CompletableFuture<List<Long>> l = nndLiteHandler.computeCompleteAdherenceString(getCalls_UniqueTFN_TestCase2(), registration);
        l.get();
        String adherenceString = nndLiteHandler.generateAdherenceString(-1L, logs, Utils.convertStringToDate("2019-01-01 18:30:00"), Utils.convertStringToDate("2019-01-16 18:30:00"), registration.getScheduleTypeId());
        assertEquals("566656566656665", adherenceString);
        verify(springEventHandler, Mockito.times(1)).anyAdherenceEvent(any());
    }

    @Test
    public void adherenceStringCorrectnessTest_SharedNumber_SameTFN_TestCase1() throws ParseException, ExecutionException, InterruptedException {
        doNothing().when(springEventHandler).anyAdherenceEvent(any());
        when(phoneMapRepository.findAllActiveIamIdsByPhoneNumbers(any(), any())).thenReturn(getSharedNumber());
        when(registrationRepository.findByIdIn(any())).thenReturn(getRegistrations_SharedNumbersWithScheduleType());
        Registration registration = new Registration(1L, 6L, Utils.convertStringToDate("2018-12-31 18:30:00"), Utils.convertStringToDate("2019-09-06 18:30:00"), "91929399495", "666666", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"));
        when(registrationRepository.findById(any())).thenReturn(Optional.of(registration));
        CompletableFuture<List<Long>> l = nndLiteHandler.computeCompleteAdherenceString(getCalls_SameTFN_TestCase1(), registration);
        l.get();
        String adherenceString = nndLiteHandler.generateAdherenceString(-1L, logs, Utils.convertStringToDate("2018-12-31 18:30:00"), Utils.convertStringToDate("2019-01-06 18:30:00"), registration.getScheduleTypeId());
        assertEquals("555555", adherenceString);
        verify(springEventHandler, Mockito.times(1)).anyAdherenceEvent(any());
    }

    @Test
    public void adherenceStringCorrectnessTest_SharedNumber_SameTFN_TestCase2() throws ParseException, ExecutionException, InterruptedException {
        doNothing().when(springEventHandler).anyAdherenceEvent(any());
        when(phoneMapRepository.findAllActiveIamIdsByPhoneNumbers(any(), any())).thenReturn(getSharedNumber());
        when(registrationRepository.findByIdIn(any())).thenReturn(getRegistrations_SharedNumbersWithScheduleType());
        Registration registration = new Registration(1L, 6L, Utils.convertStringToDate("2018-12-31 18:30:00"), Utils.convertStringToDate("2019-01-06 18:30:00"), "91929399495", "666666", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"));
        when(registrationRepository.findById(any())).thenReturn(Optional.of(registration));
        CompletableFuture<List<Long>> l = nndLiteHandler.computeCompleteAdherenceString(getCalls_SameTFN_TestCase2(), registration);
        l.get();
        String adherenceString = nndLiteHandler.generateAdherenceString(-1L, logs, Utils.convertStringToDate("2018-12-31 18:30:00"), Utils.convertStringToDate("2019-01-06 18:30:00"), registration.getScheduleTypeId());
        assertEquals("555555", adherenceString);
        verify(springEventHandler, Mockito.times(1)).anyAdherenceEvent(any());
    }

    @Test
    public void adherenceStringCorrectnessTest_SharedNumber_SameTFN_TestCase3() throws ParseException, ExecutionException, InterruptedException {
        doNothing().when(springEventHandler).anyAdherenceEvent(any());
        when(phoneMapRepository.findAllActiveIamIdsByPhoneNumbers(any(), any())).thenReturn(getSharedNumber());
        when(registrationRepository.findByIdIn(any())).thenReturn(getRegistrations_SharedNumbersWithScheduleType());
        Registration registration = new Registration(1L, 6L, Utils.convertStringToDate("2018-12-31 18:30:00"), Utils.convertStringToDate("2019-01-06 18:30:00"), "91929399495", "666666", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"));
        when(registrationRepository.findById(any())).thenReturn(Optional.of(registration));
        CompletableFuture<List<Long>> l = nndLiteHandler.computeCompleteAdherenceString(getCalls_SameTFN_TestCase3(), registration);
        l.get();
        String adherenceString = nndLiteHandler.generateAdherenceString(-1L, logs, Utils.convertStringToDate("2018-12-31 18:30:00"), Utils.convertStringToDate("2019-01-12 18:00:00"), 1L);
        assertEquals("566565665665", adherenceString);
        verify(springEventHandler, Mockito.times(1)).anyAdherenceEvent(any());
    }

    @Test
    public void adherenceStringCorrectnessTest_SharedUniqueNumber_SameTFN_TestCase3() throws ParseException, ExecutionException, InterruptedException {
        doNothing().when(springEventHandler).anyAdherenceEvent(any());
        when(phoneMapRepository.findAllActiveIamIdsByPhoneNumbers(any(), any())).thenReturn(new ArrayList<>()).thenReturn(new ArrayList<>());
        when(registrationRepository.findByIdIn(any())).thenReturn(getRegistrations_SharedNumbersWithScheduleType()).thenReturn(getRegistrations_SharedNumbersWithScheduleType()).thenReturn(getUniqueRegistrationWithScheduleType());
        Registration registration = new Registration(1L, 6L, Utils.convertStringToDate("2018-12-31 18:30:00"), Utils.convertStringToDate("2019-01-06 18:30:00"), "91929399495", "666666", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"));
        when(registrationRepository.findById(any())).thenReturn(Optional.of(registration));
        CompletableFuture<List<Long>> l = nndLiteHandler.computeCompleteAdherenceString(getCalls_SameTFN_TestCase3(), registration);
        l.get();
        String adherenceString = nndLiteHandler.generateAdherenceString(-1L, logs, Utils.convertStringToDate("2018-12-31 18:30:00"), Utils.convertStringToDate("2019-01-12 18:00:00"), registration.getScheduleTypeId());
        assertEquals("566464664664", adherenceString);
        verify(springEventHandler, Mockito.times(1)).anyAdherenceEvent(any());
    }

    private List<BigInteger> getUniqueNumber() {
        //one item in list (i.e one iamId makes a number unique)
        List<BigInteger> ids = new ArrayList<>();
        ids.add(new BigInteger("1"));
        return ids;
    }

    private List<Registration> getUniqueRegistrationWithScheduleType() {
        List<Registration> registrations = new ArrayList<>();
        registrations.add(new Registration(1L, 1L));
        return registrations;
    }

    private List<BigInteger> getSharedNumber() {
        //more than one id (i.e > 1 iamIds makes a number shared)
        List<BigInteger> ids = new ArrayList<>();
        ids.add(new BigInteger("1"));
        ids.add(new BigInteger("2"));
        return ids;
    }

    private List<Registration> getRegistrations_SharedNumbersWithScheduleType() {
        List<Registration> registrations = new ArrayList<>();
        registrations.add(new Registration(1L, 1L));
        registrations.add(new Registration(2L, 1L));
        return  registrations;
    }

    private List<CallLogs> getCalls_UniqueTFN_TestCase1() throws ParseException {
        List<CallLogs> callLogs = new ArrayList<>();
        callLogs.add(new CallLogs("1", "?", Utils.convertStringToDate("2019-09-01 02:00:26"), Utils.convertStringToDate("2019-08-31 21:30:26"), "1"));
        callLogs.add(new CallLogs("2", "?", Utils.convertStringToDate("2019-09-02 21:30:26"), Utils.convertStringToDate("2019-09-02 16:00:26"), "1"));
        callLogs.add(new CallLogs("3", "?", Utils.convertStringToDate("2019-09-03 21:23:26"), Utils.convertStringToDate("2019-09-03 16:00:26"), "1"));
        callLogs.add(new CallLogs("4", "?", Utils.convertStringToDate("2019-09-04 03:00:26"), Utils.convertStringToDate("2019-09-03 22:30:26"), "1"));
        callLogs.add(new CallLogs("5", "?", Utils.convertStringToDate("2019-09-05 21:23:26"), Utils.convertStringToDate("2019-09-05 16:00:26"), "1"));
        callLogs.add(new CallLogs("6", "?", Utils.convertStringToDate("2019-09-06 17:23:26"), Utils.convertStringToDate("2019-09-06 12:00:26"), "1"));
        return callLogs;
    }

    private List<CallLogs> getCalls_UniqueTFN_TestCase2() throws ParseException {
        List<CallLogs> callLogs = new ArrayList<>();
        callLogs.add(new CallLogs("1", "?", Utils.convertStringToDate("2019-01-02 03:23:26") /* for reference this is client time */, Utils.convertStringToDate("2019-01-01 22:23:26") /* this is client time converted roughly to UTC */, "1"));
        callLogs.add(new CallLogs("1", "?", Utils.convertStringToDate("2019-01-02 03:23:26"), Utils.convertStringToDate("2019-01-01 22:23:26"), "1"));
        callLogs.add(new CallLogs("1", "?", Utils.convertStringToDate("2019-01-02 03:23:26"), Utils.convertStringToDate("2019-01-01 2:23:26"), "1"));

        callLogs.add(new CallLogs("1", "?", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 07:23:26"), "1"));
        callLogs.add(new CallLogs("2", "?", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 07:23:26"), "1"));
        callLogs.add(new CallLogs("2", "?", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 07:23:26"), "1"));

        callLogs.add(new CallLogs("2", "?", Utils.convertStringToDate("2019-01-08 12:23:26"), Utils.convertStringToDate("2019-01-08 07:23:26"), "1"));
        callLogs.add(new CallLogs("3", "?", Utils.convertStringToDate("2019-01-08 12:23:26"), Utils.convertStringToDate("2019-01-08 07:23:26"), "1"));
        callLogs.add(new CallLogs("3", "?", Utils.convertStringToDate("2019-01-08 12:23:26"), Utils.convertStringToDate("2019-01-08 07:23:26"), "1"));

        callLogs.add(new CallLogs("5", "?", Utils.convertStringToDate("2019-01-12 02:23:26"), Utils.convertStringToDate("2019-01-11 21:00:26"), "1"));
        callLogs.add(new CallLogs("6", "?", Utils.convertStringToDate("2019-01-16 12:23:26"), Utils.convertStringToDate("2019-01-16 07:23:26"), "1"));
        return callLogs;
    }

    private List<CallLogs> getCalls_SameTFN_TestCase1() throws ParseException {
        List<CallLogs> callLogs = new ArrayList<>();
        callLogs.add(new CallLogs("1", "?", Utils.convertStringToDate("2019-01-01 02:23:26"), Utils.convertStringToDate("2018-12-31 21:00:26"), "1"));
        callLogs.add(new CallLogs("1", "?", Utils.convertStringToDate("2019-01-02 12:23:26"), Utils.convertStringToDate("2019-01-02 07:23:26"), "1"));
        callLogs.add(new CallLogs("1", "?", Utils.convertStringToDate("2019-01-03 12:23:26"), Utils.convertStringToDate("2019-01-03 07:23:26"), "1"));
        callLogs.add(new CallLogs("1", "?", Utils.convertStringToDate("2019-01-04 12:23:26"), Utils.convertStringToDate("2019-01-04 07:23:26"), "1"));
        callLogs.add(new CallLogs("1", "?", Utils.convertStringToDate("2019-01-05 12:23:26"), Utils.convertStringToDate("2019-01-05 07:23:26"), "1"));
        callLogs.add(new CallLogs("1", "?", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 07:23:26"), "1"));
        return callLogs;
    }

    private List<CallLogs> getCalls_SameTFN_TestCase2() throws ParseException {
        List<CallLogs> callLogs = new ArrayList<>();
        callLogs.add(new CallLogs("1", "?", Utils.convertStringToDate("2019-01-01 02:23:26"), Utils.convertStringToDate("2018-12-31 21:00:26"), "1"));
        callLogs.add(new CallLogs("1", "?", Utils.convertStringToDate("2019-01-01 12:23:26"), Utils.convertStringToDate("2019-01-01 07:23:26"), "1"));
        callLogs.add(new CallLogs("1", "?", Utils.convertStringToDate("2019-01-02 12:23:26"), Utils.convertStringToDate("2019-01-02 07:23:26"), "1"));
        callLogs.add(new CallLogs("2", "?", Utils.convertStringToDate("2019-01-02 12:23:26"), Utils.convertStringToDate("2019-01-02 07:23:26"), "1"));
        callLogs.add(new CallLogs("2", "?", Utils.convertStringToDate("2019-01-03 12:23:26"), Utils.convertStringToDate("2019-01-03 07:23:26"), "1"));
        callLogs.add(new CallLogs("2", "?", Utils.convertStringToDate("2019-01-03 12:23:26"), Utils.convertStringToDate("2019-01-03 07:23:26"), "1"));
        callLogs.add(new CallLogs("2", "?", Utils.convertStringToDate("2019-01-04 12:23:26"), Utils.convertStringToDate("2019-01-04 07:23:26"), "1"));
        callLogs.add(new CallLogs("2", "?", Utils.convertStringToDate("2019-01-05 12:23:26"), Utils.convertStringToDate("2019-01-05 07:23:26"), "1"));
        callLogs.add(new CallLogs("2", "?", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 07:23:26"), "1"));
        return callLogs;
    }

    private List<CallLogs> getCalls_SameTFN_TestCase3() throws ParseException {
        List<CallLogs> callLogs = new ArrayList<>();
        callLogs.add(new CallLogs("1", "?", Utils.convertStringToDate("2019-01-01 02:23:26"), Utils.convertStringToDate("2018-12-31 21:00:26"), "1"));
        callLogs.add(new CallLogs("1", "?", Utils.convertStringToDate("2019-01-01 02:23:26"), Utils.convertStringToDate("2018-12-31 21:00:26"), "1"));

        callLogs.add(new CallLogs("1", "?", Utils.convertStringToDate("2019-01-04 12:23:26"), Utils.convertStringToDate("2019-01-04 07:23:26"), "1"));
        callLogs.add(new CallLogs("2", "?", Utils.convertStringToDate("2019-01-04 12:23:26"), Utils.convertStringToDate("2019-01-04 07:23:26"), "1"));

        callLogs.add(new CallLogs("2", "?", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 07:23:26"), "1"));
        callLogs.add(new CallLogs("2", "?", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 07:23:26"), "1"));
        callLogs.add(new CallLogs("2", "?", Utils.convertStringToDate("2019-01-09 12:23:26"), Utils.convertStringToDate("2019-01-09 07:23:26"), "1"));
        callLogs.add(new CallLogs("2", "?", Utils.convertStringToDate("2019-01-09 12:23:26"), Utils.convertStringToDate("2019-01-09 07:23:26"), "1"));
        callLogs.add(new CallLogs("2", "?", Utils.convertStringToDate("2019-01-12 12:23:26"), Utils.convertStringToDate("2019-01-12 07:23:26"), "1"));
        return callLogs;
    }
}
