package tests.unit.handlersTest;

import com.everwell.iam.enums.AdherenceCodeEnum;
import com.everwell.iam.handlers.impl.NNDLiteHandler;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import tests.BaseTestNoSpring;

import java.util.List;

public class NNDLiteHandlerTest extends BaseTestNoSpring {

    @Spy
    @InjectMocks
    NNDLiteHandler nndLiteHandler;

    @Test
    public void testAdherenceCodeToRegenerate()
    {
        List<Character> adherenceCodeList = nndLiteHandler.getAdherenceCodesToRegenerate();
        Assert.assertEquals(adherenceCodeList.size(),3);
    }

    @Test
    public void testEligibleForUpdate_Before_RECEIVED_UNSCHEDULED_After_RECEIVED()
    {
        boolean isEligible = nndLiteHandler.eligibleForUpdate(AdherenceCodeEnum.RECEIVED_UNSCHEDULED.getCode(), AdherenceCodeEnum.RECEIVED.getCode());
        Assert.assertTrue(isEligible);
    }

    @Test
    public void testEligibleForUpdate_Before_NOT_RECEIVED_After_RECEIVED_UNSCHEDULED()
    {
        boolean isEligible = nndLiteHandler.eligibleForUpdate(AdherenceCodeEnum.QUIET.getCode(), AdherenceCodeEnum.RECEIVED_UNSCHEDULED.getCode());
        Assert.assertTrue(isEligible);
    }


    @Test
    public void testEligibleForUpdate_Before_RECEIVED_UNSCHEDULED()
    {
        boolean isEligible = nndLiteHandler.eligibleForUpdate(AdherenceCodeEnum.RECEIVED_UNSCHEDULED.getCode(), AdherenceCodeEnum.QUIET.getCode());
        Assert.assertFalse(isEligible);
    }

    @Test
    public void testEligibleForUpdate_Before_NO_INFO()
    {
        boolean isEligible = nndLiteHandler.eligibleForUpdate(AdherenceCodeEnum.NO_INFO.getCode(), AdherenceCodeEnum.QUIET.getCode());
        Assert.assertTrue(isEligible);
    }

    @Test
    public void testEligibleForUpdate_Before_RECEIVED_UNSURE_After_RECEIVED()
    {
        boolean isEligible = nndLiteHandler.eligibleForUpdate(AdherenceCodeEnum.RECEIVED_UNSURE.getCode(), AdherenceCodeEnum.RECEIVED.getCode());
        Assert.assertTrue(isEligible);
    }

    @Test
    public void testEligibleForUpdate_Before_MANUAL_After_NO_INFO()
    {
        boolean isEligible = nndLiteHandler.eligibleForUpdate(AdherenceCodeEnum.MANUAL.getCode(), AdherenceCodeEnum.NO_INFO.getCode());
        Assert.assertTrue(isEligible);
    }

    @Test
    public void testEligibleForUpdate_Before_MANUAL_After_RECEIVED()
    {
        boolean isEligible = nndLiteHandler.eligibleForUpdate(AdherenceCodeEnum.MANUAL.getCode(), AdherenceCodeEnum.RECEIVED.getCode());
        Assert.assertTrue(isEligible);
    }

    @Test
    public void testEligibleForUpdate_Before_MANUAL_After_RECEIVED_UNSURE()
    {
        boolean isEligible = nndLiteHandler.eligibleForUpdate(AdherenceCodeEnum.MANUAL.getCode(), AdherenceCodeEnum.RECEIVED_UNSURE.getCode());
        Assert.assertTrue(isEligible);
    }

    @Test
    public void testEligibleForUpdate_Before_MISSED_After_NO_INFO()
    {
        boolean isEligible = nndLiteHandler.eligibleForUpdate(AdherenceCodeEnum.MISSED.getCode(), AdherenceCodeEnum.NO_INFO.getCode());
        Assert.assertTrue(isEligible);
    }

    @Test
    public void testEligibleForUpdate_Before_MISSED_After_RECEIVED()
    {
        boolean isEligible = nndLiteHandler.eligibleForUpdate(AdherenceCodeEnum.MISSED.getCode(), AdherenceCodeEnum.RECEIVED.getCode());
        Assert.assertTrue(isEligible);
    }

    @Test
    public void testEligibleForUpdate_Before_MISSED_After_RECEIVED_UNSURE()
    {
        boolean isEligible = nndLiteHandler.eligibleForUpdate(AdherenceCodeEnum.MISSED.getCode(), AdherenceCodeEnum.RECEIVED_UNSURE.getCode());
        Assert.assertTrue(isEligible);
    }

    @Test
    public void testEligibleForUpdate_Default()
    {
        boolean isEligible = nndLiteHandler.eligibleForUpdate(AdherenceCodeEnum.BLANK.getCode(), AdherenceCodeEnum.BLANK.getCode());
        Assert.assertFalse(isEligible);
    }

}
