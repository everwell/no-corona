package tests.unit.handlersTest;

import com.everwell.iam.exceptions.NotFoundException;
import com.everwell.iam.handlers.ScheduleHandler;
import com.everwell.iam.handlers.ScheduleTypeHandlerMap;
import com.everwell.iam.handlers.impl.MERMHandler;
import com.everwell.iam.models.db.AccessMapping;
import com.everwell.iam.models.db.ImeiMap;
import com.everwell.iam.models.db.MermLogs;
import com.everwell.iam.models.db.Registration;
import com.everwell.iam.models.dto.AdhTechLogsData;
import com.everwell.iam.models.dto.springevents.EpisodeEvent;
import com.everwell.iam.models.http.requests.GetBulkImeiRequest;
import com.everwell.iam.models.http.requests.MERMEntityRequest;
import com.everwell.iam.models.http.requests.RegenerateAdherenceRequest;
import com.everwell.iam.models.http.responses.ImeiBulkResponse;
import com.everwell.iam.repositories.ImeiMapRepository;
import com.everwell.iam.repositories.MermLogRepository;
import com.everwell.iam.utils.SentryUtils;
import com.everwell.iam.utils.Utils;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.springframework.context.ApplicationEventPublisher;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

public class MermHandlerTest extends AdherenceTechHandlerTest {

  @Spy
  @InjectMocks
  MERMHandler mermHandler;

  @Mock
  MermLogRepository mermLogRepository;

  @Mock
  ImeiMapRepository imeiMapRepository;

  @Mock
  ScheduleTypeHandlerMap scheduleTypeHandlerMap;

  @Mock
  ScheduleHandler scheduleHandler;

  @Mock
  ApplicationEventPublisher applicationEventPublisher;

  private static final Long MERM_ID = 3L;

  @Override
  public void getTechLogsTest() throws Exception {
    when(mermLogRepository.getAllByIamIdOrderByClientTimeCustom(anyLong())).thenReturn(getMermLogs("123"));
    List<AdhTechLogsData> adhTechLogsData = mermHandler.getTechLogs(1L);
    assertEquals(3, adhTechLogsData.size());
  }

  @Override
  public void deleteRegistration() {
    mermHandler.deleteRegistration(anySet());
    Mockito.verify(registrationRepository, Mockito.times(1)).deleteAllByIdIn(anySet());
    Mockito.verify(adhStringLogRepository, Mockito.times(1)).deleteAllByIamIds(anySet());
    Mockito.verify(imeiMapRepository, Mockito.times(1)).deleteAllByIamIdIn(anySet());
  }

  @Test
  public void testRegenerateAdherence() throws Exception
  {
    doNothing().when(mermHandler).deleteAdherenceLogByIamIdAndAdherenceCodes(anyList(), anyLong());
    List<MermLogs> mermLogs = getMermLogs("123");
    when(mermLogRepository.getAllByIamIdOrderByClientTimeCustom(anyLong())).thenReturn(mermLogs);
    doReturn(Arrays.asList(1L,2L)).when(mermHandler).computeCompleteAdherenceString(anyList(), anyLong());
    List<Long> logs = mermHandler.regenerateAdherence(Collections.singleton(1L)).get();
    Assert.assertEquals(logs.size(), 2);
    Assert.assertEquals(logs.get(0).longValue(), 1L);
  }

  @Test
  public void testComputeCompleteAdherenceString() throws Exception
  {
    Registration registration = new Registration(1L, 3L, Utils.convertStringToDate("2019-08-31 18:30:00"), Utils.convertStringToDate("2019-09-06 18:30:00"), "9988799887", "666666", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"));
    when(registrationRepository.findById(any())).thenReturn(Optional.of(registration));

    List<MermLogs> mermLogs = getMermLogs("123");

    when(scheduleTypeHandlerMap.getHandler(anyLong())).thenReturn(scheduleHandler);
    when(scheduleHandler.shouldTakeDose(anyLong(), any())).thenReturn(true);
    doNothing().when(mermHandler).updateRegistrationAfterRegen(any(Registration.class), anyList());
    List<Long> adherenceStringIds = mermHandler.computeCompleteAdherenceString(mermLogs, 1L);
    Assert.assertEquals(adherenceStringIds.size(), 3);
  }

  @Test
  public void testPostRegistration_NewIMEI() throws ParseException {
    Registration registration = new Registration(1L, 3L, Utils.convertStringToDate("2019-01-01 18:30:00"), Utils.convertStringToDate("2019-01-16 18:30:00"), "91929399495", "666666", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"));
    MERMEntityRequest mermEntityRequest = new MERMEntityRequest();
    mermEntityRequest.setImei("123");
    mermEntityRequest.setStartDate("2019-01-02 18:30:00");
    mermEntityRequest.setClientId(1L);
    mermHandler.postRegistration(registration, mermEntityRequest, true);
    Mockito.verify(imeiMapRepository, Mockito.times(1)).save(any());
    verify(applicationEventPublisher, Mockito.times(1)).publishEvent(any(RegenerateAdherenceRequest.class));
    verify(applicationEventPublisher, Mockito.times(1)).publishEvent(any(EpisodeEvent.class));
  }

  @Test
  public void testPostRegistration_ExistingIMEI() throws ParseException {
    Registration registration = new Registration(1L, 3L, Utils.convertStringToDate("2019-01-01 18:30:00"), Utils.convertStringToDate("2019-01-16 18:30:00"), "91929399495", "666666", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"));
    MERMEntityRequest mermEntityRequest = new MERMEntityRequest();
    mermEntityRequest.setImei("123");
    mermEntityRequest.setClientId(1L);
    mermHandler.postRegistration(registration, mermEntityRequest, false);
    verify(applicationEventPublisher, Mockito.times(1)).publishEvent(any(RegenerateAdherenceRequest.class));
    verify(applicationEventPublisher, Mockito.times(1)).publishEvent(any(EpisodeEvent.class));
  }

  List<MermLogs> getMermLogs (String imei) throws ParseException {
    List<MermLogs> mermLogs = new ArrayList<>();
    mermLogs.add(new MermLogs(imei, Utils.convertStringToDate("2019-11-01 11:11:11")));
    mermLogs.add(new MermLogs(imei, Utils.convertStringToDate("2019-11-02 11:11:11")));
    mermLogs.add(new MermLogs(imei, Utils.convertStringToDate("2019-11-04 11:11:11")));
    return mermLogs;
  }

  @Test
  public void testGetBulkIMEI_AllIMEIs() throws ParseException {
    List<String> entityIdList = Arrays.asList("1", "2");
    Long iamId1 = 1L;
    Long iamId2 = 2L;

    String imei1 = "abcdefgh";
    String imei2 = "a1212121";
    String imei3 = "qwerqwer";

    List<AccessMapping> accessMapping = new ArrayList<>();
    accessMapping.add(new AccessMapping(29L, iamId1, entityIdList.get(0)));
    accessMapping.add(new AccessMapping(29L, iamId2, entityIdList.get(1)));

    when(mappingRepository.getByClientIdAndEntityIdInAndActiveIsTrue(any(), any())).thenReturn(accessMapping);

    Date startDate1 = new SimpleDateFormat("dd-MM-yyyy").parse("02-03-2022");
    Date startDate2 = new SimpleDateFormat("dd-MM-yyyy").parse("02-04-2022");
    Date startDate3 = new SimpleDateFormat("dd-MM-yyyy").parse("03-04-2022");
    Date stopDate = new SimpleDateFormat("dd-MM-yyyy").parse("02-04-2022");

    ImeiMap imeiMap1 = new ImeiMap(imei1, iamId1, startDate1);
    imeiMap1.setStopDate(stopDate);
    ImeiMap imeiMap2 = new ImeiMap(imei2, iamId1, startDate2);
    ImeiMap imeiMap3 = new ImeiMap(imei3, iamId2, startDate3);
    List<ImeiMap> imeiMapList1 = Arrays.asList(imeiMap1, imeiMap2, imeiMap3);

    when(imeiMapRepository.findAllByIamIdIn(any())).thenReturn(imeiMapList1);

    GetBulkImeiRequest imeiRequest = new GetBulkImeiRequest(entityIdList, false);

    List<ImeiBulkResponse> imeiBulkResponse = mermHandler.getImeiBulk(imeiRequest, 29L);
    assertEquals(3, imeiBulkResponse.size());
    assertEquals(imei1, imeiBulkResponse.get(0).getImei());
    assertEquals(entityIdList.get(0), imeiBulkResponse.get(0).getEntityId());
    assertEquals(startDate1, imeiBulkResponse.get(0).getStartDate());
    assertEquals(stopDate, imeiBulkResponse.get(0).getEndDate());

    assertEquals(imei2, imeiBulkResponse.get(1).getImei());
    assertEquals(entityIdList.get(0), imeiBulkResponse.get(1).getEntityId());
    assertEquals(startDate2, imeiBulkResponse.get(1).getStartDate());
    assertNull(imeiBulkResponse.get(1).getEndDate());

    assertEquals(imei3, imeiBulkResponse.get(2).getImei());
    assertEquals(entityIdList.get(1), imeiBulkResponse.get(2).getEntityId());
    assertEquals(startDate3, imeiBulkResponse.get(2).getStartDate());
    assertNull(imeiBulkResponse.get(2).getEndDate());
    verify(imeiMapRepository, Mockito.times(1)).findAllByIamIdIn(any());
    verify(imeiMapRepository, Mockito.times(0)).findActiveImeiByIamId(any());
  }

  @Test
  public void testGetBulkIMEI_ActiveIMEIs() throws ParseException {
    List<String> entityIdList = Arrays.asList("1", "2");
    Long iamId1 = 1L;
    Long iamId2 = 2L;

    String imei1 = "abcdefgh";
    String imei2 = "a1212121";

    List<AccessMapping> accessMapping = new ArrayList<>();
    accessMapping.add(new AccessMapping(29L, iamId1, entityIdList.get(0)));
    accessMapping.add(new AccessMapping(29L, iamId2, entityIdList.get(1)));

    when(mappingRepository.getByClientIdAndEntityIdInAndActiveIsTrue(any(), any())).thenReturn(accessMapping);

    Date startDate1 = new SimpleDateFormat("dd-MM-yyyy").parse("02-03-2022");
    Date startDate2 = new SimpleDateFormat("dd-MM-yyyy").parse("02-04-2022");

    ImeiMap imeiMap1 = new ImeiMap(imei1, iamId1, startDate1);
    ImeiMap imeiMap2 = new ImeiMap(imei2, iamId2, startDate2);
    List<ImeiMap> imeiMapList1 = Arrays.asList(imeiMap1, imeiMap2);

    when(imeiMapRepository.findActiveImeiByIamId(any())).thenReturn(imeiMapList1);

    GetBulkImeiRequest imeiRequest = new GetBulkImeiRequest(entityIdList, true);

    List<ImeiBulkResponse> imeiBulkResponse = mermHandler.getImeiBulk(imeiRequest, 29L);
    assertEquals(2, imeiBulkResponse.size());
    assertEquals(imei1, imeiBulkResponse.get(0).getImei());
    assertEquals(entityIdList.get(0), imeiBulkResponse.get(0).getEntityId());
    assertEquals(startDate1, imeiBulkResponse.get(0).getStartDate());
    assertNull(imeiBulkResponse.get(0).getEndDate());

    assertEquals(imei2, imeiBulkResponse.get(1).getImei());
    assertEquals(entityIdList.get(1), imeiBulkResponse.get(1).getEntityId());
    assertEquals(startDate2, imeiBulkResponse.get(1).getStartDate());
    assertNull(imeiBulkResponse.get(1).getEndDate());

    verify(imeiMapRepository, Mockito.times(0)).findAllByIamIdIn(any());
    verify(imeiMapRepository, Mockito.times(1)).findActiveImeiByIamId(any());
  }

  @Test(expected = NotFoundException.class)
  @PrepareForTest({SentryUtils.class})
  public void testGetBulkIMEI_EntityNotFound() {
    List<String> entityIdList = Arrays.asList("1", "2");
    GetBulkImeiRequest imeiRequest = new GetBulkImeiRequest(entityIdList, true);
    List<AccessMapping> accessMapping = new ArrayList<>();
    when(mappingRepository.getByClientIdAndEntityIdInAndActiveIsTrue(any(), any())).thenReturn(accessMapping);
    mermHandler.getImeiBulk(imeiRequest, 29L);
    PowerMockito.verifyStatic(SentryUtils.class, Mockito.times(1));
    SentryUtils.captureException(any());
  }

}
