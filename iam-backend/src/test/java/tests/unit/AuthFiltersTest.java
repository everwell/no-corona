package tests.unit;

import com.everwell.iam.filters.AuthFilter;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.savedrequest.Enumerator;
import org.springframework.security.web.util.matcher.AndRequestMatcher;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.NegatedRequestMatcher;
import tests.BaseTest;
import tests.BaseTestNoSpring;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

public class AuthFiltersTest extends BaseTestNoSpring {


    AuthFilter authFilter;

    @Mock
    HttpServletRequest httpServletRequest;

    @Mock
    HttpServletResponse httpServletResponse;

    @Mock
    FilterChain filterChain;


    @Before
    public void setUp() {
        authFilter = new AuthFilter(new NegatedRequestMatcher(
                new AndRequestMatcher(
                        new AntPathRequestMatcher("/v1/client")
                )
        ));
    }

    @Test
    public void testDoFilterFilterEmptyToken() throws ServletException, IOException {
        Enumeration<String> headers = new Enumerator<>(Arrays.asList("X-IAM-Access-Token", "X-IAM-Client-Id"));
        when(httpServletRequest.getHeaderNames()).thenReturn(headers);
        when(httpServletRequest.getHeader("X-IAM-Access-Token")).thenReturn("testtoken");

        authFilter.doFilter(httpServletRequest, httpServletResponse, filterChain);

        Mockito.verify(filterChain, Mockito.times(1)).doFilter(any(), any());
    }

    @Test
    public void testDoFilterFilterNotRequired() throws ServletException, IOException {
        Enumeration<String> headers = new Enumerator<>(Arrays.asList( "X-IAM-Client-Id", "X-IAM-Access-Token"));
        Enumeration<String> headers2 = new Enumerator<>(Arrays.asList( "X-IAM-Client-Id", "X-IAM-Access-Token"));
        when(httpServletRequest.getHeaderNames()).thenReturn(headers).thenReturn(headers2);
        when(httpServletRequest.getHeader("X-IAM-Access-Token")).thenReturn("testtoken");
        when(httpServletRequest.getHeader("X-IAM-Client-Id")).thenReturn("29");
        when(httpServletRequest.getServletPath()).thenReturn("/v1/client");

        authFilter.doFilter(httpServletRequest, httpServletResponse, filterChain);

        Mockito.verify(filterChain, Mockito.times(1)).doFilter(any(), any());
    }
}
