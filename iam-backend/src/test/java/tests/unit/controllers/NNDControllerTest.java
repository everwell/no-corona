package tests.unit.controllers;

import com.everwell.iam.exceptions.CustomExceptionHandler;
import com.everwell.iam.handlers.impl.NNDHandler;
import com.everwell.iam.handlers.impl.NNDLiteHandler;
import com.everwell.iam.models.db.AccessMapping;
import com.everwell.iam.models.db.Registration;
import com.everwell.iam.models.dto.PhoneDeleteDto;
import com.everwell.iam.models.dto.PhoneEditDto;
import com.everwell.iam.models.http.requests.IncomingCallRequest;
import com.everwell.iam.models.http.requests.PhoneRequest;
import com.everwell.iam.repositories.AdhStringLogRepository;
import com.everwell.iam.repositories.CallLogRepository;
import com.everwell.iam.repositories.PhoneMapRepository;
import com.everwell.iam.services.AdherenceService;
import com.everwell.iam.services.EntityService;
import com.everwell.iam.utils.Utils;
import com.everwell.iam.vendors.NNDController;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import tests.BaseTest;

import java.util.*;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class NNDControllerTest extends BaseTest {
  private MockMvc mockMvc;

  @Spy
  @InjectMocks
  NNDHandler nndHandler;

  @Spy
  @InjectMocks
  NNDLiteHandler nndLiteHandler;

  @Mock
  CallLogRepository callLogRepository;

  @Mock
  PhoneMapRepository phoneMapRepository;

  @Mock
  AdhStringLogRepository adhStringLogRepository;

  @Mock
  private EntityService entityService;

  @Mock
  private AdherenceService adherenceService;

  @InjectMocks
  private NNDController nndController;

  private static Long clientId = 4L;
  private static String primaryPhone = "9988799887";
  private static PhoneRequest.PhoneNumbers phoneNumbers = new PhoneRequest.PhoneNumbers(primaryPhone, Utils.getFormattedDate(new Date()));
  private static String entityId = "1234";
  private static Long iamId = 1L;

  private static String MESSAGE_ENTITY_MAPPING_REQUIRED = "entity mapping is required";
  private static String MESSAGE_ENTITY_NOT_FOUND = "entity not found!";
  private static String MESSAGE_ONE_OR_MORE_ENTITY_MAPPING_NOT_FOUND = "one or more entity mapping not found!";
  private static String MESSAGE_PRIMARY_PHONE_NOT_FOUND = "Primary Phone Not Found!";
  private static String MESSAGE_INVALID_IDENTIFIER = "invalid identifier";
  private static String MESSAGE_CALL_REGISTERED = "Call Registered!";

  @Before
  public void setup() {
    mockMvc = MockMvcBuilders.standaloneSetup(nndController)
            .setControllerAdvice(new CustomExceptionHandler())
            .build();
  }

  @Test
  public void Route_V1_Call_Process_Test() throws Exception {
    String CALL_NUMBER = "919812107115";
    String TFN = "18003136110";
    doReturn(null).when(nndHandler).processIncomingCall(any());
    doReturn(null).when(nndLiteHandler).processIncomingCall(any());
    mockMvc
      .perform(
          MockMvcRequestBuilders
              .get("/v1/call/process")
              .param("CallNumber", TFN)
              .param("msisdn", CALL_NUMBER)
              .param("date", "Fri Aug 09 2019 23:05:36 GMT 0530 (IST)")
              .accept(MediaType.APPLICATION_JSON)
      )
      .andExpect(status().isOk());
  }

  @Test
  public void Route_V1_Call_Process_Test_InvalidDateFormat() throws Exception {
    String CALL_NUMBER = "919812107115";
    String TFN = "18003136110";
    doReturn(null).when(nndHandler).processIncomingCall(any());
    doReturn(null).when(nndLiteHandler).processIncomingCall(any());
    mockMvc
            .perform(
                    MockMvcRequestBuilders
                            .get("/v1/call/process")
                            .param("CallNumber", TFN)
                            .param("msisdn", CALL_NUMBER)
                            .param("date", "Fri Aug 09 2019")
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(MESSAGE_CALL_REGISTERED));
  }

  @Test
  public void Route_V1_Call_Process_Test_EmptyDate() throws Exception {
    String CALL_NUMBER = "919812107115";
    String TFN = "18003136110";
    doReturn(null).when(nndHandler).processIncomingCall(any());
    doReturn(null).when(nndLiteHandler).processIncomingCall(any());
    mockMvc
            .perform(
                    MockMvcRequestBuilders
                            .get("/v1/call/process")
                            .param("CallNumber", TFN)
                            .param("msisdn", CALL_NUMBER)
                            .param("date", "")
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isOk());
  }

  @Test
  public void Route_V1_Call_Process_All_Test() throws Exception {
    String CALL_NUMBER = "919812107115";
    String TFN = "18003136110";
    String utcDate = "2019-01-12 15:14:13";
    doReturn(null).when(nndHandler).processIncomingCall(any());
    doReturn(null).when(nndLiteHandler).processIncomingCall(any());
    String utcDateInvalidFormat = "2019-01-12";
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders
        .post("/v1/call/process")
        .contentType(MediaType.APPLICATION_JSON);

    IncomingCallRequest incomingCallRequest = new IncomingCallRequest(null, null, null, null);

    //tfn, caller, date missing, so 404
    requestBuilder.content(Utils.asJsonString(incomingCallRequest));
    mockMvc.perform(requestBuilder)
        .andExpect(status().isBadRequest());

    //caller, date missing, so 404
    incomingCallRequest.setNumberDialed(TFN);
    requestBuilder.content(Utils.asJsonString(incomingCallRequest));
    mockMvc.perform(requestBuilder)
        .andExpect(status().isBadRequest());

    //date missing, so 404
    incomingCallRequest.setCaller(CALL_NUMBER);
    requestBuilder.content(Utils.asJsonString(incomingCallRequest));
    mockMvc.perform(requestBuilder)
        .andExpect(status().isBadRequest());

    //everything present, with invalid date format, so 200
    incomingCallRequest.setUtcDateTime(utcDateInvalidFormat);
    requestBuilder.content(Utils.asJsonString(incomingCallRequest));
    mockMvc.perform(requestBuilder)
            .andExpect(status().isOk());

    //everything present, so 200
    incomingCallRequest.setUtcDateTime(utcDate);
    requestBuilder.content(Utils.asJsonString(incomingCallRequest));
    mockMvc.perform(requestBuilder)
        .andExpect(status().isOk());
  }

  @Test
  public void testAddPhones_EntityMappingRequired() throws Exception
  {
    String uri = "/v1/phone";
    PhoneRequest request = new PhoneRequest(null, Collections.singletonList(phoneNumbers));

    mockMvc.perform(MockMvcRequestBuilders.post(uri)
            .content(Utils.asJsonString(request))
            .contentType(MediaType.APPLICATION_JSON)
            .header("X-IAM-Client-Id", clientId.toString()))
            .andExpect(status().isBadRequest())
            .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(MESSAGE_ENTITY_MAPPING_REQUIRED));
  }

  @Test
  public void testAddPhones_EntityNotFound() throws Exception
  {
    String uri = "/v1/phone";
    PhoneRequest request = new PhoneRequest(entityId, Collections.singletonList(phoneNumbers));

    when(entityService.validateActiveIamMappingByEntityId(clientId, entityId)).thenReturn(false);

    mockMvc.perform(MockMvcRequestBuilders.post(uri)
            .content(Utils.asJsonString(request))
            .contentType(MediaType.APPLICATION_JSON)
            .header("X-IAM-Client-Id", clientId.toString()))
            .andExpect(status().isNotFound())
            .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(MESSAGE_ENTITY_NOT_FOUND));
  }

  @Test
  public void testAddPhones_Success() throws Exception
  {
    String uri = "/v1/phone";
    PhoneRequest request = new PhoneRequest(entityId, Collections.singletonList(phoneNumbers));

    when(entityService.validateActiveIamMappingByEntityId(clientId, entityId)).thenReturn(true);

    AccessMapping accessMapping = new AccessMapping(clientId, iamId, entityId);
    when(entityService.getActiveIamMappingByEntityId(clientId, entityId)).thenReturn(accessMapping);
    Registration registration = new Registration(1L, 1L, Utils.convertStringToDate("2019-01-01 18:30:00"), Utils.convertStringToDate("2019-01-16 18:30:00"), "91929399495", "666666", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"));
    when(adherenceService.getRegistrationsByIamId(any())).thenReturn(Collections.singletonList(registration));

    doReturn(true).when(nndHandler).savePhoneNumbers(anyList(), anyLong());

    mockMvc.perform(MockMvcRequestBuilders.post(uri)
            .content(Utils.asJsonString(request))
            .contentType(MediaType.APPLICATION_JSON)
            .header("X-IAM-Client-Id", clientId.toString()))
            .andExpect(status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$.success").value(true));
  }

  @Test
  public void testDeletePhones_OneOrMoreEntityMappingNotFound() throws Exception
  {
    String uri = "/v1/phone";

    PhoneDeleteDto request = new PhoneDeleteDto();
    request.setIamIds(Collections.singletonList(iamId));
    request.setEntityIds(Collections.singletonList(entityId));

    mockMvc.perform(MockMvcRequestBuilders.delete(uri)
            .content(Utils.asJsonString(request))
            .contentType(MediaType.APPLICATION_JSON)
            .header("X-IAM-Client-Id", clientId.toString()))
            .andExpect(status().isNotFound())
            .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(MESSAGE_ONE_OR_MORE_ENTITY_MAPPING_NOT_FOUND));
  }

  @Test
  public void testDeletePhones_EntityNotFound() throws Exception
  {
    String uri = "/v1/phone";

    PhoneDeleteDto request = new PhoneDeleteDto();
    request.setIamIds(new ArrayList<>());
    request.setEntityIds(Collections.singletonList(entityId));

    mockMvc.perform(MockMvcRequestBuilders.delete(uri)
            .content(Utils.asJsonString(request))
            .contentType(MediaType.APPLICATION_JSON)
            .header("X-IAM-Client-Id", clientId.toString()))
            .andExpect(status().isNotFound())
            .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(MESSAGE_ENTITY_NOT_FOUND));
  }

  @Test
  public void testDeletePhones_Success() throws Exception
  {
    String uri = "/v1/phone";

    PhoneDeleteDto request = new PhoneDeleteDto();
    request.setIamIds(new ArrayList<>());
    request.setEntityIds(new ArrayList<>());

    doNothing().when(nndHandler).deactivatePhoneNumbersByIamIds(anySet());

    mockMvc.perform(MockMvcRequestBuilders.delete(uri)
            .content(Utils.asJsonString(request))
            .contentType(MediaType.APPLICATION_JSON)
            .header("X-IAM-Client-Id", clientId.toString()))
            .andExpect(status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$.success").value(true));
  }

  @Test
  public void testDeletePhones_Success_EntityIds() throws Exception
  {
    String uri = "/v1/phone";

    PhoneDeleteDto request = new PhoneDeleteDto();
    List<String> entityIds = Collections.singletonList(entityId);
    List<Long> iamIds = new ArrayList<>();
    request.setIamIds(iamIds);
    request.setEntityIds(entityIds);

    List<AccessMapping> accessMappings = new ArrayList<AccessMapping>(){
      {
        add(new AccessMapping(clientId, iamId, entityId));
      }
    };

    when(entityService.validateActiveIamMappingByEntityId(clientId, entityIds)).thenReturn(true);
    when(entityService.getActiveIamMappingByEntityId(clientId, entityIds)).thenReturn(accessMappings);

    doNothing().when(nndHandler).deactivatePhoneNumbersByIamIds(anySet());

    mockMvc.perform(MockMvcRequestBuilders.delete(uri)
            .content(Utils.asJsonString(request))
            .contentType(MediaType.APPLICATION_JSON)
            .header("X-IAM-Client-Id", clientId.toString()))
            .andExpect(status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$.success").value(true));
  }

  @Test
  public void testDeletePhones_Success_IamIds() throws Exception
  {
    String uri = "/v1/phone";

    PhoneDeleteDto request = new PhoneDeleteDto();
    List<String> entityIds = new ArrayList<>();
    List<Long> iamIds = Collections.singletonList(iamId);
    request.setIamIds(iamIds);
    request.setEntityIds(entityIds);

    when(entityService.validateMapping(iamIds, clientId)).thenReturn(true);

    doNothing().when(nndHandler).deactivatePhoneNumbersByIamIds(anySet());

    mockMvc.perform(MockMvcRequestBuilders.delete(uri)
            .content(Utils.asJsonString(request))
            .contentType(MediaType.APPLICATION_JSON)
            .header("X-IAM-Client-Id", clientId.toString()))
            .andExpect(status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$.success").value(true));
  }

  @Test
  public void testUpdatePhones_PrimaryPhoneNotFound() throws Exception
  {
    String uri = "/v1/phone";

    PhoneEditDto request = new PhoneEditDto();

    mockMvc.perform(MockMvcRequestBuilders.put(uri)
            .content(Utils.asJsonString(request))
            .contentType(MediaType.APPLICATION_JSON)
            .header("X-IAM-Client-Id", clientId.toString()))
            .andExpect(status().isBadRequest())
            .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(MESSAGE_PRIMARY_PHONE_NOT_FOUND));
  }

  @Test
  public void testUpdatePhones_EntityNotFound() throws Exception
  {
    String uri = "/v1/phone";

    PhoneEditDto request = new PhoneEditDto();
    request.setPrimaryPhone(primaryPhone);
    request.setEntityId(entityId);

    when(entityService.validateActiveIamMappingByEntityId(clientId, entityId)).thenReturn(false);

    mockMvc.perform(MockMvcRequestBuilders.put(uri)
            .content(Utils.asJsonString(request))
            .contentType(MediaType.APPLICATION_JSON)
            .header("X-IAM-Client-Id", clientId.toString()))
            .andExpect(status().isNotFound())
            .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(MESSAGE_ENTITY_NOT_FOUND));
  }

  @Test
  public void testUpdatePhones_InvalidIdentifier() throws Exception
  {
    String uri = "/v1/phone";

    PhoneEditDto request = new PhoneEditDto();
    request.setPrimaryPhone(primaryPhone);

    mockMvc.perform(MockMvcRequestBuilders.put(uri)
            .content(Utils.asJsonString(request))
            .contentType(MediaType.APPLICATION_JSON)
            .header("X-IAM-Client-Id", clientId.toString()))
            .andExpect(status().isBadRequest())
            .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(MESSAGE_INVALID_IDENTIFIER));
  }

  @Test
  public void testUpdatePhones_Success() throws Exception
  {
    String uri = "/v1/phone";

    PhoneEditDto request = new PhoneEditDto();
    request.setPrimaryPhone(primaryPhone);
    request.setEntityId(entityId);

    when(entityService.validateActiveIamMappingByEntityId(clientId, entityId)).thenReturn(true);

    AccessMapping accessMapping = new AccessMapping(clientId, iamId, entityId);
    when(entityService.getActiveIamMappingByEntityId(clientId, entityId)).thenReturn(accessMapping);
    Registration registration = new Registration(1L, 1L, Utils.convertStringToDate("2019-01-01 18:30:00"), Utils.convertStringToDate("2019-01-16 18:30:00"), "91929399495", "666666", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"));
    when(adherenceService.getRegistrationsByIamId(any())).thenReturn(Collections.singletonList(registration));

    doNothing().when(nndHandler).deactivateOldPhonesAndAddNew(any(PhoneEditDto.class));
    doNothing().when(nndLiteHandler).deactivateOldPhonesAndAddNew(any(PhoneEditDto.class));

    mockMvc.perform(MockMvcRequestBuilders.put(uri)
            .content(Utils.asJsonString(request))
            .contentType(MediaType.APPLICATION_JSON)
            .header("X-IAM-Client-Id", clientId.toString()))
            .andExpect(status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$.success").value(true));
  }

  @Test
  public void testUpdatePhones_Success_NonEmptyIamId() throws Exception
  {
    String uri = "/v1/phone";

    PhoneEditDto request = new PhoneEditDto();
    request.setIamId(iamId);
    request.setPrimaryPhone(primaryPhone);
    request.setEntityId(entityId);

    when(entityService.validateActiveIamMappingByEntityId(clientId, entityId)).thenReturn(true);

    AccessMapping accessMapping = new AccessMapping(clientId, iamId, entityId);
    when(entityService.getActiveIamMappingByEntityId(clientId, entityId)).thenReturn(accessMapping);
    Registration registration = new Registration(1L, 1L, Utils.convertStringToDate("2019-01-01 18:30:00"), Utils.convertStringToDate("2019-01-16 18:30:00"), "91929399495", "666666", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"));
    when(adherenceService.getRegistrationsByIamId(any())).thenReturn(Collections.singletonList(registration));

    doNothing().when(nndHandler).deactivateOldPhonesAndAddNew(any(PhoneEditDto.class));
    doNothing().when(nndLiteHandler).deactivateOldPhonesAndAddNew(any(PhoneEditDto.class));

    mockMvc.perform(MockMvcRequestBuilders.put(uri)
            .content(Utils.asJsonString(request))
            .contentType(MediaType.APPLICATION_JSON)
            .header("X-IAM-Client-Id", clientId.toString()))
            .andExpect(status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$.success").value(true));
  }

}
