package tests.unit.service;

import com.everwell.iam.exceptions.ConflictException;
import com.everwell.iam.models.db.CAccess;
import com.everwell.iam.models.http.requests.RegisterClientRequest;
import com.everwell.iam.models.http.responses.ClientResponse;
import com.everwell.iam.repositories.CAccessRepository;
import com.everwell.iam.services.impl.ClientServiceImpl;
import com.everwell.iam.utils.RandomPasswordGenerator;
import org.apache.commons.text.RandomStringGenerator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import tests.TestUtils;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class ClientServiceTest {

    @Autowired
    @InjectMocks
    private ClientServiceImpl clientService;

    @Mock
    private CAccessRepository accessRepository;

    private static RegisterClientRequest SAMPLE_CLIENT_REGISRATION_REQUEST;

    private RegisterClientRequest getRegisterClientRequest() {
        RandomStringGenerator strGenerator = new RandomStringGenerator.Builder()
                .withinRange('a', 'z')
                .build();
        RandomPasswordGenerator passwordGenerator = new RandomPasswordGenerator.Builder()
                .useDigits(true)
                .build();


        String name = strGenerator.generate(8);
        String password = passwordGenerator.generate(6);
        return new RegisterClientRequest(name, password);
    }

    @Before
    public void setup() {
        // Client Registration
        SAMPLE_CLIENT_REGISRATION_REQUEST = getRegisterClientRequest();
    }

    @Test
    public void testClientCanBeCreatedSuccessfully() {
        Long mockId = 1L;
        String mockSecureToken = "secureEnough";

        Mockito.when(accessRepository.findByName(SAMPLE_CLIENT_REGISRATION_REQUEST.getName())).thenReturn(null);
        Mockito.when(accessRepository.save(Mockito.any(CAccess.class))).thenAnswer(new Answer<CAccess>() {
            @Override
            public CAccess answer(InvocationOnMock invocation) throws Throwable {
                CAccess access = (CAccess) invocation.getArguments()[0];
                access.setId(mockId);
                return access;
            }
        });

        ClientResponse response = clientService.registerClient(SAMPLE_CLIENT_REGISRATION_REQUEST);
        Assert.assertEquals(response.getId(), mockId);
        Assert.assertEquals(response.getName(), SAMPLE_CLIENT_REGISRATION_REQUEST.getName());
        verify(accessRepository, Mockito.times(1)).findByName(any());
        verify(accessRepository, Mockito.times(1)).save(any());
    }

    @Test(expected = ConflictException.class)
    public void testClientCannotBeCreatedAgain() {
        Mockito.when(accessRepository.findByName(SAMPLE_CLIENT_REGISRATION_REQUEST.getName())).thenReturn(new CAccess());

        clientService.registerClient(SAMPLE_CLIENT_REGISRATION_REQUEST);
        TestUtils.print("Successfully passed testClientCannotBeCreatedAgain");
    }

}
