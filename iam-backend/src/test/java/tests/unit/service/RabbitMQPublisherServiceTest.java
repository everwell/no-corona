package tests.unit.service;

import com.everwell.iam.models.dto.eventstreaming.EventStreamingDto;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import tests.BaseTest;

import static org.assertj.core.api.Assertions.assertThatCode;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.eq;


public class RabbitMQPublisherServiceTest extends BaseTest {

    private RabbitTemplate rabbitTemplateMock;

    String test = "Test";
    EventStreamingDto<String> message = new EventStreamingDto<>(test, test);

    @Before
    public void setUp() {
        rabbitTemplateMock = Mockito.mock(RabbitTemplate.class);
    }

    @Test
    public void publish() {
        assertThatCode(() -> rabbitTemplateMock.convertAndSend(test, message))
                                .doesNotThrowAnyException();
    }
}
