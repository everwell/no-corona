package tests.integration;

import com.everwell.iam.constants.EndPointConstants;
import com.everwell.iam.exceptions.CustomExceptionHandler;
import com.everwell.iam.handlers.AdhTechHandlerMap;
import com.everwell.iam.handlers.AdherenceTechHandler;
import com.everwell.iam.handlers.ScheduleTypeHandlerMap;
import com.everwell.iam.handlers.SpringEventHandler;
import com.everwell.iam.handlers.impl.DailyScheduleHandler;
import com.everwell.iam.handlers.impl.MERMHandler;
import com.everwell.iam.models.db.*;
import com.everwell.iam.models.http.requests.*;
import com.everwell.iam.repositories.*;
import com.everwell.iam.services.AdherenceService;
import com.everwell.iam.services.ClientService;
import com.everwell.iam.services.EntityService;
import com.everwell.iam.services.impl.AdherenceServiceImpl;
import com.everwell.iam.services.impl.EntityServiceImpl;
import com.everwell.iam.utils.Utils;
import com.everwell.iam.vendors.MERMController;
import org.junit.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import tests.BaseTest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class MERMControllerTest extends BaseTest {

  private MockMvc mockMvc;

  @Mock
  MermLogRepository mermLogRepository;

  @Mock
  ImeiMapRepository imeiMapRepository;

  @Mock
  RegistrationRepository registrationRepository;

  @Mock
  AccessMappingRepository accessMappingRepository;

  @Mock
  AdhStringLogRepository logRepository;

  @Mock
  AdhTechHandlerMap adherenceTechHandlerMap;

  @Mock
  SpringEventHandler springEventHandler;

  @Mock
  ClientService clientService;

  //SpyBean sends this instance of merm handler
  @Spy
  @InjectMocks
  MERMHandler mermHandler;

  List<AdherenceStringLog> adherenceStringLog = new ArrayList<>();

  private ImeiMap imeiMap;

  Long MERM_ADHTECH_ID;

  @Spy
  @InjectMocks
  DailyScheduleHandler dailyScheduleHandler;

  @Mock
  ScheduleTypeHandlerMap scheduleTypeHandlerMap;

  @Spy
  @InjectMocks
  private EntityServiceImpl entityService;

  @Spy
  @InjectMocks
  private AdherenceServiceImpl adherenceService;

  @InjectMocks
  private MERMController mermController;

  @Mock
  private ApplicationEventPublisher applicationEventPublisher;

  @Before
  public void initMocks() {
    mockMvc = MockMvcBuilders.standaloneSetup(mermController)
            .setControllerAdvice(new CustomExceptionHandler())
            .build();
    List<AccessMapping> accessMappingList = new ArrayList<>();
    AccessMapping access= new AccessMapping(1L,1L,"123");
    accessMappingList.add(access);
    when(accessMappingRepository.getByIamId(any())).thenReturn(accessMappingList);
    MERM_ADHTECH_ID = 3L;
    mermHandler.setAdherenceTechId(MERM_ADHTECH_ID);
    when(adherenceTechHandlerMap.getHandler(MERM_ADHTECH_ID)).thenReturn(mermHandler);
    dailyScheduleHandler.setScheduleTypeId(1L);
    when(scheduleTypeHandlerMap.getHandler(anyLong())).thenReturn(dailyScheduleHandler);
  }

  @Test
  public void route_v1_merm_process_call_old_date() throws Exception {
    doNothing().when(springEventHandler).anyAdherenceEvent(any());
    String DUMMY_IMEI = "12345678";
    Long DUMMY_IAM_ID = 1L;
    when(mermLogRepository.save(any())).thenReturn(new MermLogs());
    ImeiMap map = new ImeiMap(DUMMY_IMEI, DUMMY_IAM_ID, null);
    List<ImeiMap> dummyImeiMap = Collections.singletonList(map);
    when(imeiMapRepository.findActiveIamIdByImei(any(), any())).thenReturn(dummyImeiMap);
    Registration registration = new Registration(DUMMY_IAM_ID, 3L, Utils.convertStringToDate("2019-11-01 18:30:00"), Utils.convertStringToDate("2019-11-18 17:29:59"), DUMMY_IMEI, "66666666666666666", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"));
    List<Registration> registrations = Collections.singletonList(registration);
    when(registrationRepository.findByIdIn(any())).thenReturn(registrations);
    when(registrationRepository.save(any())).thenReturn(registration);
    doNothing().when(logRepository).deleteByIamIdAndValueIn(any(),any());
    MermEventRequest mermEventRequest = new MermEventRequest(DUMMY_IMEI, "021119232323");
    when(dailyScheduleHandler.computeAdherenceCodeForDay(any(), any())).thenReturn('4');
    Date eventDate = Utils.convertStringToDate("021119232323", "ddMMyyHHmmss");
    when(dailyScheduleHandler.getDateToAttribute(any(), any())).thenReturn(eventDate);
    CAccess cAccess = new CAccess();
    when(clientService.getClientById(anyLong())).thenReturn(cAccess);
    mockMvc
            .perform(
                    MockMvcRequestBuilders
                            .post("/v1/merm/event")
                            .content(Utils.asJsonString(mermEventRequest))
                            .contentType(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isOk());
    // this is an async call, need a few ms to update registration
    Thread.sleep(1000);
    assertEquals("64666666666666666", registration.getAdherenceString());
  }

  @Test
  public void same_imei_for_different_iams() throws Exception {
    doNothing().when(springEventHandler).anyAdherenceEvent(any());
    String DUMMY_IMEI = "12345678";
    Long DUMMY_IAM_ID_1 = 1L;
    Long DUMMY_IAM_ID_2 = 2L;
    when(mermLogRepository.save(any())).thenReturn(new MermLogs());
    ImeiMap map1 = new ImeiMap(DUMMY_IMEI, DUMMY_IAM_ID_1, null);
    ImeiMap map2 = new ImeiMap(DUMMY_IMEI,DUMMY_IAM_ID_2,null);
    List<ImeiMap> dummyImeiMap = Arrays.asList(map1,map2);
    when(imeiMapRepository.findActiveIamIdByImei(any(), any())).thenReturn(dummyImeiMap);
    Registration registration_1 = new Registration(DUMMY_IAM_ID_1, 3L, Utils.convertStringToDate("2019-11-01 18:30:00"), Utils.convertStringToDate("2019-11-18 17:29:59"), DUMMY_IMEI, "66666666666666666", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"));
    Registration registration_2 = new Registration(DUMMY_IAM_ID_2, 3L, Utils.convertStringToDate("2019-11-01 18:30:00"), Utils.convertStringToDate("2019-11-18 17:29:59"), DUMMY_IMEI, "66666666666666666", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"));
    List<Registration> registrations = Arrays.asList(registration_1,registration_2);
    when(registrationRepository.findByIdIn(any())).thenReturn(registrations);
    when(registrationRepository.save(any())).thenReturn(registration_1);
    doNothing().when(logRepository).deleteByIamIdAndValueIn(any(),any());
    MermEventRequest mermEventRequest = new MermEventRequest(DUMMY_IMEI, "021119232323");
    when(dailyScheduleHandler.computeAdherenceCodeForDay(any(), any())).thenReturn('4');
    Date eventDate = Utils.convertStringToDate("021119232323", "ddMMyyHHmmss");
    when(dailyScheduleHandler.getDateToAttribute(any(), any())).thenReturn(eventDate);
    CAccess cAccess = new CAccess();
    when(clientService.getClientById(anyLong())).thenReturn(cAccess);
    mockMvc
            .perform(
                    MockMvcRequestBuilders
                            .post("/v1/merm/event")
                            .content(Utils.asJsonString(mermEventRequest))
                            .contentType(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isOk());
    // this is an async call, need a few ms to update registration
    Thread.sleep(1000);
    assertEquals("64666666666666666", registration_1.getAdherenceString());
    assertEquals("64666666666666666", registration_2.getAdherenceString());
  }

  @Test
  public void already_allocated_reallocate_imei() throws Exception {
    String DUMMY_IMEI = "abcd";
    String DUMMY_ENTITY_ID = "1";
    Long DUMMY_IAM_ID = 1L;
    ImeiEditRequest imeiEditRequest = new ImeiEditRequest(DUMMY_ENTITY_ID, DUMMY_IMEI);
    ImeiMap map = new ImeiMap(DUMMY_IMEI, DUMMY_IAM_ID,  Utils.getCurrentDate());
    List<ImeiMap> imeiDetails = Collections.singletonList(map);
    when(imeiMapRepository.findActiveIamIdByImei(eq(DUMMY_IMEI), any())).thenReturn(imeiDetails);
    mockMvc
            .perform(
                    MockMvcRequestBuilders
                            .put("/v1/imei")
                            .content(Utils.asJsonString(imeiEditRequest))
                            .header("X-IAM-Client-Id", "29")
                            .contentType(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isConflict());
  }

  @Test
  public void absent_mapping_reallocate_imei() throws Exception {
    String DUMMY_IMEI = "abcd";
    String DUMMY_ENTITY_ID = "1";
    Long DUMMY_CLIENT_ID = 29L;
    ImeiEditRequest imeiEditRequest = new ImeiEditRequest(DUMMY_ENTITY_ID, DUMMY_IMEI);
    when(imeiMapRepository.findActiveIamIdByImei(eq(DUMMY_IMEI), any())).thenReturn(new ArrayList<>());
    when(accessMappingRepository.findActiveAccessMappingByEntityIdAndClientId(DUMMY_ENTITY_ID, DUMMY_CLIENT_ID))
            .thenReturn(null);
    mockMvc
            .perform(
                    MockMvcRequestBuilders
                            .put("/v1/imei")
                            .content(Utils.asJsonString(imeiEditRequest))
                            .header("X-IAM-Client-Id", DUMMY_CLIENT_ID.toString())
                            .contentType(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isNotFound());
  }


  @Test
  public void route_v1_imei_reallocate_imei() throws Exception {
    String DUMMY_IMEI = "abcd";
    String DUMMY_ENTITY_ID = "1";
    Long DUMMY_IAM_ID = 1L;
    ImeiEditRequest imeiEditRequest = new ImeiEditRequest(DUMMY_ENTITY_ID, DUMMY_IMEI);
    AccessMapping accessMapping = new AccessMapping(29L, DUMMY_IAM_ID, DUMMY_ENTITY_ID);
    List<AccessMapping> list = new ArrayList<>();
    list.add(accessMapping);
    when(accessMappingRepository.findActiveAccessMappingByEntityIdAndClientId(any(),any())).thenReturn(accessMapping);
    doNothing().when(imeiMapRepository).deallocateImeiByIamIds(any(),any());
    ImeiMap dummyImeiMap = new ImeiMap(DUMMY_IMEI, DUMMY_IAM_ID, null);
    AdherenceTechHandler handler = new MERMHandler();
    AdherenceTechnology adherenceTechnology = new AdherenceTechnology();
    adherenceTechnology.setId(MERM_ADHTECH_ID);
    handler.setAdherenceTechId(MERM_ADHTECH_ID);
    when(imeiMapRepository.save(any())).thenReturn(dummyImeiMap);
    when(accessMappingRepository.getByClientIdAndEntityIdInAndActiveIsTrue(any(),any())).thenReturn(list);
    when(accessMappingRepository.getByIamIdInAndClientId(any(), any())).thenReturn(list);
    Registration registration = new Registration(DUMMY_IAM_ID, MERM_ADHTECH_ID, Utils.convertStringToDate("2019-10-31 18:30:00"), Utils.convertStringToDate("2019-11-10 18:30:00"), "91929399495", "6666666666", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"));
    when(registrationRepository.getOne(anyLong())).thenReturn(registration);
    when(registrationRepository.findById(anyLong())).thenReturn(Optional.of(registration));
    when(registrationRepository.findByIdIn(any())).thenReturn(Arrays.asList(registration));
    when(mermLogRepository.getAllByIamIdOrderByClientTimeCustom(any())).thenReturn(getMermLogs(imeiEditRequest.getImei()));
    mockMvc
            .perform(
                    MockMvcRequestBuilders
                            .put("/v1/imei")
                            .content(Utils.asJsonString(imeiEditRequest))
                            .header("X-IAM-Client-Id", "29")
                            .contentType(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isOk());
    Thread.sleep(100);
    verify(imeiMapRepository, Mockito.times(1)).deallocateImeiByIamIds(any(),any());
    verify(imeiMapRepository, Mockito.times(1)).save(any(ImeiMap.class));
  }

  @Test
  public void route_v1_imei_get_active_imei() throws Exception {
    String entityId = "1";
    String imei = "abcdefgh";
    Long iamId = 1L;
    AccessMapping accessMapping = new AccessMapping(29L, iamId, entityId);
    when(accessMappingRepository.findActiveAccessMappingByEntityIdAndClientId(any(),any())).thenReturn(accessMapping);
    List<ImeiMap> imeiMap = Collections.singletonList(new ImeiMap(imei,iamId,null));
    when(imeiMapRepository.findActiveImeiByIamId(any())).thenReturn(imeiMap);
    mockMvc
            .perform(
                    MockMvcRequestBuilders
                            .get("/v1/imei")
                            .param("entityId" , entityId)
                            .param("active" , "true")
                            .header("X-IAM-Client-Id", "29")
                            .contentType(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.data[0].imei").value(imeiMap.get(0).getImei()));
  }

  @Test
  public void route_v1_imei_get_all_imei() throws Exception {
    String entityId = "1";
    String imei1 = "abcd";
    String imei2 = "efgh";
    Long iamId = 1L;
    List<AccessMapping> accessMappings = new ArrayList<>();
    AccessMapping accessMapping = new AccessMapping(29L, iamId, entityId);
    accessMappings.add(accessMapping);
    List<ImeiMap> imeiMaps = new ArrayList<>();
    ImeiMap imeiMap1 = new ImeiMap(imei1,iamId,null);
    ImeiMap imeiMap2 = new ImeiMap(imei2,iamId,null);
    imeiMaps.add(imeiMap1);
    imeiMaps.add(imeiMap2);
    when(accessMappingRepository.getAllByEntityIdAndClientId(any(),any())).thenReturn(accessMappings);
    when(imeiMapRepository.findAllByIamIdIn(any())).thenReturn(imeiMaps);
    mockMvc
            .perform(
                    MockMvcRequestBuilders
                            .get("/v1/imei")
                            .param("entityId" , entityId)
                            .param("active" , "false")
                            .header("X-IAM-Client-Id", "29")
                            .contentType(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.data[0].imei").value(imeiMap1.getImei()))
            .andExpect(jsonPath("$.data[1].imei").value(imeiMap2.getImei()));
  }

  @Test
  public void route_v1_imei_get_imei_active_imei_not_found() throws Exception {
    String entityId = "1";
    String imei = "abcdefgh";
    Long iamId = 1L;
    AccessMapping accessMapping = new AccessMapping(29L, iamId, entityId);
    when(accessMappingRepository.findActiveAccessMappingByEntityIdAndClientId(any(),any())).thenReturn(accessMapping);
    when(imeiMapRepository.findActiveImeiByIamId(any())).thenReturn(new ArrayList<>());
    mockMvc
            .perform(
                    MockMvcRequestBuilders
                            .get("/v1/imei")
                            .param("entityId" , entityId)
                            .param("active" , "true")
                            .header("X-IAM-Client-Id", "29")
                            .contentType(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isNotFound());
  }

  @Test
  public void route_v1_imei_get_imei_entity_not_found() throws Exception {
    String entityId = "1";
    when(accessMappingRepository.findActiveAccessMappingByEntityIdAndClientId(any(),any())).thenReturn(null);
    mockMvc
            .perform(
                    MockMvcRequestBuilders
                            .get("/v1/imei")
                            .param("entityId" , entityId)
                            .param("active" , "true")
                            .header("X-IAM-Client-Id", "29")
                            .contentType(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isNotFound());
  }

  List<MermLogs> getMermLogs (String imei) throws ParseException {
    List<MermLogs> mermLogs = new ArrayList<>();
    mermLogs.add(new MermLogs(imei, Utils.convertStringToDate("2019-11-01 11:11:11")));
    mermLogs.add(new MermLogs(imei, Utils.convertStringToDate("2019-11-02 11:11:11")));
    mermLogs.add(new MermLogs(imei, Utils.convertStringToDate("2019-11-04 11:11:11")));
    return mermLogs;
  }

  List<MermEventRequest> mermLogsBulk () throws ParseException {
    List<MermEventRequest> data = new ArrayList<>();
    data.add(new MermEventRequest("123456789","011119111111"));
    data.add(new MermEventRequest("123456781","021119111111"));
    data.add(new MermEventRequest("123456782","031119111111"));
    return data;
  }

  @Test
  public void route_v1_imei_entity_found() throws Exception {
    String entityId = "1L";
    Long iamId = 1L;
    String imei = "123456789";
    Date startDate= Utils.convertStringToDate("2010-08-04 10:22:31.0");
    AccessMapping accessMapping = new AccessMapping(29L, iamId, entityId);
    List<ImeiMap> imeiMap = Collections.singletonList(new ImeiMap(imei, iamId, startDate));
    when(accessMappingRepository.findEntityIdByIMEIAndClientId(any(), any())).thenReturn(accessMapping);
    when(imeiMapRepository.findActiveImeiByIamId(any())).thenReturn(imeiMap);
    mockMvc
            .perform(
                    MockMvcRequestBuilders
                            .get("/v1/imei/entity")
                            .param("imei" , imei)
                            .header("X-IAM-Client-Id", 29)
                            .contentType(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.data[0].entityId").value(entityId));
  }
  @Test
  public void route_v1_imei_entity_not_found() throws Exception {
    String imei = "123456789";
    when(accessMappingRepository.findEntityIdByIMEIAndClientId(any(), any())).thenReturn(null);
    mockMvc
            .perform(
                    MockMvcRequestBuilders
                            .get("/v1/imei/entity")
                            .param("imei" , imei)
                            .header("X-IAM-Client-Id", 29)
                            .contentType(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isNotFound());
  }

  @Test
  public void route_v1_bulk_imei_empty_list() throws Exception {
    List<String> entityId = Arrays.asList("1");
    String imei = "abcdefgh";
    Long iamId = 1L;
    List<AccessMapping> accessMapping = new ArrayList<>();
    entityId.forEach(id -> {
      accessMapping.add(new AccessMapping(29L, iamId, id));
    });
    when(accessMappingRepository.getByClientIdAndEntityIdInAndActiveIsTrue(any(),any())).thenReturn(accessMapping);
    List<ImeiMap> imeiMap = Arrays.asList(new ImeiMap(imei,iamId,null));
    when(imeiMapRepository.findAllByIamIdIn(any())).thenReturn(imeiMap);
    GetBulkImeiRequest imeiRequest = new GetBulkImeiRequest();
    mockMvc
            .perform(
                    MockMvcRequestBuilders
                            .post("/v1/imei/bulk")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(Utils.asJsonString(imeiRequest))
                            .header("X-IAM-Client-Id", "29")
                            .contentType(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isBadRequest());
  }

  @Test
  public void route_v1_bulk_imei_max_limit_list() throws Exception {
    List<String> entityId = new ArrayList<>();
    List<String> imei = new ArrayList<>();
    List<Long> iamId = new ArrayList<>();
    List<AccessMapping> accessMapping = new ArrayList<>();
    for (int i = 0 ; i < 3000 ; i++) {
      entityId.add("id"+i);
      imei.add("imei"+i);
      iamId.add((long) i);
      accessMapping.add(new AccessMapping(29L, (long) i, "id"+i));
    }
    when(accessMappingRepository.getByClientIdAndEntityIdInAndActiveIsTrue(any(),any())).thenReturn(accessMapping);
    List<ImeiMap> imeiMap = new ArrayList<>();
    for (int i = 0 ; i < 3000 ; i++) {
      imeiMap.add(new ImeiMap(imei.get(i),iamId.get(i),null));
    }
    when(imeiMapRepository.findAllByIamIdIn(any())).thenReturn(imeiMap);
    GetBulkImeiRequest imeiRequest = new GetBulkImeiRequest();
    imeiRequest.setEntityIdList(entityId);
    mockMvc
            .perform(
                    MockMvcRequestBuilders
                            .post("/v1/imei/bulk")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(Utils.asJsonString(imeiRequest))
                            .header("X-IAM-Client-Id", "29")
                            .contentType(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isBadRequest());
  }


  @Test
  public void route_v1_get_bulk_active_entity_all_IMEI() throws Exception {
    Long clientId = 29l;
    List<String> entityIdList = Arrays.asList("1", "2");
    Long iamId1 = 1L;
    Long iamId2 = 2L;

    String imei1 = "abcdefgh";
    String imei2 = "a1212121";
    String imei3 = "qwerqwer";

    List<AccessMapping> accessMapping = new ArrayList<>();
    accessMapping.add(new AccessMapping(clientId, iamId1, entityIdList.get(0)));
    accessMapping.add(new AccessMapping(clientId, iamId2, entityIdList.get(1)));

    when(accessMappingRepository.getByClientIdAndEntityIdInAndActiveIsTrue(any(), any())).thenReturn(accessMapping);

    Date startDate1 = new SimpleDateFormat("dd-MM-yyyy").parse("02-03-2022");
    Date startDate2 = new SimpleDateFormat("dd-MM-yyyy").parse("02-04-2022");
    Date startDate3 = new SimpleDateFormat("dd-MM-yyyy").parse("03-04-2022");
    Date stopDate = new SimpleDateFormat("dd-MM-yyyy").parse("02-04-2022");

    ImeiMap imeiMap1 = new ImeiMap(imei1, iamId1, startDate1);
    imeiMap1.setStopDate(stopDate);
    ImeiMap imeiMap2 = new ImeiMap(imei2, iamId1, startDate2);
    ImeiMap imeiMap3 = new ImeiMap(imei3, iamId2, startDate3);
    List<ImeiMap> imeiMapList1 = Arrays.asList(imeiMap1, imeiMap2, imeiMap3);

    when(imeiMapRepository.findAllByIamIdIn(any())).thenReturn(imeiMapList1);

    GetBulkImeiRequest imeiRequest = new GetBulkImeiRequest();
    imeiRequest.setEntityIdList(entityIdList);

    mockMvc
            .perform(
                    MockMvcRequestBuilders
                            .post("/v1/imei/bulk")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(Utils.asJsonString(imeiRequest))
                            .header("X-IAM-Client-Id", clientId)
                            .contentType(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.data[0].imei").value(imei1))
            .andExpect(jsonPath("$.data[0].entityId").value(entityIdList.get(0)))
            .andExpect(jsonPath("$.data[0].startDate").value("02-03-2022 00:00:00"))
            .andExpect(jsonPath("$.data[0].endDate").value("02-04-2022 00:00:00"))
            .andExpect(jsonPath("$.data[1].imei").value(imei2))
            .andExpect(jsonPath("$.data[1].entityId").value(entityIdList.get(0)))
            .andExpect(jsonPath("$.data[1].startDate").value("02-04-2022 00:00:00"))
            .andExpect(jsonPath("$.data[1].endDate").value(nullValue()))
            .andExpect(jsonPath("$.data[2].imei").value(imei3))
            .andExpect(jsonPath("$.data[2].entityId").value(entityIdList.get(1)))
            .andExpect(jsonPath("$.data[2].startDate").value("03-04-2022 00:00:00"))
            .andExpect(jsonPath("$.data[2].endDate").value(nullValue()));
  }

  @Test
  public void route_v1_imei_get_bulk_active_imei() throws Exception {
    Long clientId = 29L;
    List<String> entityIdList = Arrays.asList("1", "2");
    Long iamId1 = 1L;
    Long iamId2 = 2L;

    String imei1 = "abcdefgh";
    String imei2 = "a1212121";

    List<AccessMapping> accessMapping = new ArrayList<>();
    accessMapping.add(new AccessMapping(29L, iamId1, entityIdList.get(0)));
    accessMapping.add(new AccessMapping(29L, iamId2, entityIdList.get(1)));

    when(accessMappingRepository.getByClientIdAndEntityIdInAndActiveIsTrue(any(), any())).thenReturn(accessMapping);

    Date startDate1 = new SimpleDateFormat("dd-MM-yyyy").parse("02-03-2022");
    Date startDate2 = new SimpleDateFormat("dd-MM-yyyy").parse("02-04-2022");

    ImeiMap imeiMap1 = new ImeiMap(imei1, iamId1, startDate1);
    ImeiMap imeiMap2 = new ImeiMap(imei2, iamId2, startDate2);
    List<ImeiMap> imeiMapList1 = Arrays.asList(imeiMap1, imeiMap2);

    when(imeiMapRepository.findActiveImeiByIamId(any())).thenReturn(imeiMapList1);
    GetBulkImeiRequest imeiRequest = new GetBulkImeiRequest(entityIdList, true);

    mockMvc
            .perform(
                    MockMvcRequestBuilders
                            .post("/v1/imei/bulk")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(Utils.asJsonString(imeiRequest))
                            .header("X-IAM-Client-Id", clientId)
                            .contentType(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.data[0].imei").value(imei1))
            .andExpect(jsonPath("$.data[0].entityId").value(entityIdList.get(0)))
            .andExpect(jsonPath("$.data[0].startDate").value("02-03-2022 00:00:00"))
            .andExpect(jsonPath("$.data[0].endDate").value(nullValue()))
            .andExpect(jsonPath("$.data[1].imei").value(imei2))
            .andExpect(jsonPath("$.data[1].entityId").value(entityIdList.get(1)))
            .andExpect(jsonPath("$.data[1].startDate").value("02-04-2022 00:00:00"))
            .andExpect(jsonPath("$.data[1].endDate").value(nullValue()));
  }

  @Test
  public void route_v1_imei_entity_get_bulk() throws Exception {
    String endPoint = EndPointConstants.IMEI_ENTITY_BULK;
    Long clientId = 29L;
    ImeiBulkRequest imeiBulkRequest = new ImeiBulkRequest(Arrays.asList("123"));
    List<ImeiMap> imeiMapList = new ArrayList<>();
    imeiMapList.add(new ImeiMap("123", 1L, Utils.getCurrentDate()));
    List<AccessMapping> accessMappingList = new ArrayList<>();
    accessMappingList.add(new AccessMapping(1L, 29L, 1L, "12345", true));
    accessMappingList.add(new AccessMapping(2L, 29L, 2L, "22222", true));
    when(imeiMapRepository.findAllByImeiInAndStopDate(any(), any())).thenReturn(imeiMapList);
    when(accessMappingRepository.getByIamIdInAndClientIdAndActive(any(), any(), anyBoolean())).thenReturn(accessMappingList);

    mockMvc
            .perform(
                    MockMvcRequestBuilders
                            .post(endPoint)
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(Utils.asJsonString(imeiBulkRequest))
                            .header("X-IAM-Client-Id", clientId)
                            .contentType(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.data.size()").value("1"))
            .andExpect(jsonPath("$.data[0].imei").value("123"))
            .andExpect(jsonPath("$.data[0].entityId").value("12345"));
  }

  @Test
  public void route_v1_imei_entity_get_bulk_throws_validation_error() throws Exception {
    String endPoint = EndPointConstants.IMEI_ENTITY_BULK;
    Long clientId = 29L;
    ImeiBulkRequest imeiBulkRequest = new ImeiBulkRequest();
    mockMvc
            .perform(
                    MockMvcRequestBuilders
                            .post(endPoint)
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(Utils.asJsonString(imeiBulkRequest))
                            .header("X-IAM-Client-Id", clientId)
                            .contentType(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isBadRequest());
  }

  @Test
  public void route_v1_imei_get_bulk_active_imei_throws_entity_not_found() throws Exception {

    List<AccessMapping> accessMapping = new ArrayList<>();
    when(accessMappingRepository.getByClientIdAndEntityIdInAndActiveIsTrue(any(),any())).thenReturn(accessMapping);
    GetBulkImeiRequest imeiRequest = new GetBulkImeiRequest();
    imeiRequest.setEntityIdList(Collections.singletonList("1"));
    mockMvc
            .perform(
                    MockMvcRequestBuilders
                            .post("/v1/imei/bulk")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(Utils.asJsonString(imeiRequest))
                            .header("X-IAM-Client-Id", "29")
                            .contentType(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isNotFound());
  }
}
