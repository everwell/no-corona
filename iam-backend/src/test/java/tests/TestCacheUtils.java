package tests;

import com.everwell.iam.models.dto.CacheMultiSetWithTimeDto;
import com.everwell.iam.utils.CacheUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

public class TestCacheUtils extends BaseTest{

    CacheUtils cacheUtils;

    @Mock
    RedisTemplate<String, Object> redisTemplate;


    @Mock
    private ValueOperations<String, Object> valueOperations;

    @Before
    public void init() {
        cacheUtils = new CacheUtils(redisTemplate);
        when(redisTemplate.opsForValue()).thenReturn(valueOperations);
        doNothing().when(valueOperations).set(anyString(), anyString());
        doNothing().when(valueOperations).multiSet(any());
        doReturn(null).when(redisTemplate).execute(any(), any(), any(), any(), any());
    }

    @Test
    public void testGetFromCache() {
        String DUMMY_KEY = "key";
        Object DUMMY_VAL = new Object();
        when(valueOperations.get(eq(DUMMY_KEY))).thenReturn(DUMMY_VAL);
        Object obj = cacheUtils.getFromCache(DUMMY_KEY);
        Assert.assertEquals(obj, DUMMY_VAL);
    }

    @Test
    public void testHasKey() {
        String DUMMY_KEY = "key";
        when(redisTemplate.hasKey(eq(DUMMY_KEY))).thenReturn(true);
        boolean ans = cacheUtils.hasKey(DUMMY_KEY);
        Assert.assertTrue(ans);
    }

    @Test
    public void bulkInsertWithTtlTest() {
        List<CacheMultiSetWithTimeDto> cacheMultiSetWithTimeDtoList = new ArrayList<>();
        cacheMultiSetWithTimeDtoList.add(new CacheMultiSetWithTimeDto("key", "val", 100L));
        cacheMultiSetWithTimeDtoList.add(new CacheMultiSetWithTimeDto("key2", "val1", 200L));
        cacheMultiSetWithTimeDtoList.add(new CacheMultiSetWithTimeDto("key3", "val2", 300L));
        cacheMultiSetWithTimeDtoList.add(new CacheMultiSetWithTimeDto("key4", "val3", 300L));
        CacheUtils.bulkInsertWithTtl(cacheMultiSetWithTimeDtoList);
        verify(redisTemplate, Mockito.times(1)).execute(any(), any(), any(), any(), any());
        verify(valueOperations, Mockito.times(1)).multiSet(any());
    }

    @Test
    public void deleteKeyFromCache() {
        String key = "1";
        when(redisTemplate.delete(anyString())).thenReturn(true);
        assertEquals(true, redisTemplate.delete(key));
        verify(redisTemplate, Mockito.times(1)).delete(anyString());
    }

}
