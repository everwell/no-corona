package com.everwell.iam.utils;

import com.everwell.iam.models.dto.CacheMultiSetWithTimeDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.data.redis.core.script.RedisScript;
import org.springframework.data.redis.serializer.GenericToStringSerializer;
import org.springframework.data.redis.serializer.JdkSerializationRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.*;
import java.util.concurrent.TimeUnit;

@Component
public class CacheUtils {

    private static RedisTemplate<String, Object> redisTemplate;

    @Autowired
    public CacheUtils(RedisTemplate<String, Object> redisTemplate) {
        CacheUtils.redisTemplate = redisTemplate;
    }

    public static <V> void putIntoCacheBulk(Map<String, V> map) {
        redisTemplate.opsForValue().multiSet(map);
    }

    public static void bulkInsertWithTtl(List<CacheMultiSetWithTimeDto> cacheMultiSetWithTimeDtoList) {
        HashMap<String, String> cacheMap = new HashMap<>();
        List<String> entityIds = new ArrayList<>();
        List<String> ttl = new ArrayList<>();
        cacheMultiSetWithTimeDtoList.forEach(cache -> {
            cacheMap.put(cache.getKey(), cache.getValue());
            entityIds.add(cache.getKey());
            ttl.add(String.valueOf(cache.getSecondsToEndDay()));
        });
        putIntoCacheBulk(cacheMap);
        setBulkExpiry(entityIds, ttl.toArray());
    }

    public static void deleteKey(String key) {
        redisTemplate.delete(key);
    }

    private static void setBulkExpiry(List<String> keys, Object[] argv) {
        redisEvalForNoResult("for i=1, #KEYS do redis.call('expire', KEYS[i], ARGV[i]) end", keys, argv);
    }

    public static <T> T getKeys(Object[] argv, boolean getExisting, Class<T> klass) {
        return redisEvalForResult("local r = {} for i=1, #ARGV do if redis.call('exists', ARGV[i]) == " + (getExisting ? 1 : 0) + " then r[#r+1] = ARGV[i] end end return r", new ArrayList<>(), argv, klass);
    }

    private static void redisEvalForNoResult(String scriptText, List<String> keys, Object[] argv) {
        // since spring boot serializes data to - "\xac\xed\x00\x05t\x00\x0" it was not being recognized by redis as integer
        redisTemplate.execute(new DefaultRedisScript<>(scriptText), new StringRedisSerializer(), new StringRedisSerializer(), keys, argv);
    }

    private static <T> T redisEvalForResult(String scriptText, List<String> keys, Object[] argv, Class<T> resultType) {
        Object obj = redisTemplate.execute(RedisScript.of(scriptText, resultType), new StringRedisSerializer(), new StringRedisSerializer(), keys, argv);
        return (null == obj) ? null : ((T) obj);
    }

    public static void putIntoCache(String key, Object value, Long ttl) {
        redisTemplate.opsForValue().set(key, value, ttl, TimeUnit.SECONDS);
    }

    public static <T> T getFromCache(List<String> key) {
        Object obj = redisTemplate.opsForValue().multiGet(key);
        return (null == obj) ? null : ((T) obj);
    }

    public static <T> T getFromCache(String key) {
        Object obj = redisTemplate.opsForValue().get(key);
        return (null == obj)?null:(T)(obj);
    }

    public static boolean incrementKeyInCache(String key, Long ttl) {
        Integer value = getFromCache(key);
        if(null == value) {
            putIntoCache(key, 1, ttl);
        } else {
            putIntoCache(key, value + 1, ttl);
        }
        return true;
    }

    public static boolean hasKey(String key) {
        return redisTemplate.hasKey(key);
    }

    public static void putSetIntoCache(String key,String value) {
        redisTemplate.opsForSet().add(key,value);
    }
}
