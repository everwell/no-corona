package com.everwell.iam.models.dto.eventstreaming.analytics;

import com.everwell.iam.enums.AdherenceCodeEnum;
import com.everwell.iam.models.db.Registration;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Date;
import java.util.TreeMap;

@Getter
@AllArgsConstructor
public class RecordAdherenceTrackingDto {
    TreeMap<Date, AdherenceCodeEnum> datesToValueMapList;
    Registration registration;
    String adherenceTechName;
}
