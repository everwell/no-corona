package com.everwell.iam.models.http.requests;

import lombok.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class RecordAdherenceRequest {

    @Setter
    private Long iamId;

    private Character value;

    @Setter
    private String entityId;

    @Setter
    private String date;

    private Long adherenceType;

    @Setter
    private Map<String, Character> datesToValueMapList;
     /* accepts data as <K, V>:
        "datesToValueMapList" : {
            "2019-07-03 04:52:56.000000" : "",
            "2019-07-04 04:52:56.000000" : "5",
            "2019-07-05 04:52:56.000000" : "4",
            "2019-07-06 04:52:56.000000" : "5"
        }
    */
     public RecordAdherenceRequest(char value, Long adherenceType) {
         this.value = value;
         this.adherenceType = adherenceType;
     }
}
