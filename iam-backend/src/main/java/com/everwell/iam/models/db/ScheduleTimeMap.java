package com.everwell.iam.models.db;


import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import lombok.*;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.sql.Time;
import java.util.Date;

@Entity
@Table(name = "iam_schedule_time_map")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
public class ScheduleTimeMap {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "active")
    private boolean active;

    @Column(name = "created_date")
    private Date createdDate;

    @Column(name = "schedule_map_id")
    private Long scheduleMapId;

    @Column(name = "dose_time")
    private Time doseTime;

    @Column(name = "start_date")
    private Date startDate;

    @Column(name = "end_date")
    private Date endDate;

    @Column(name = "adh_string", columnDefinition = "TEXT")
    private String adhString;

    @Column(name = "dose_info", columnDefinition = "jsonb")
    @org.hibernate.annotations.Type( type = "jsonb" )
    private Object doseInfo;

    public ScheduleTimeMap(Long id, boolean active, Date createdDate, Long scheduleMapId, Time doseTime) {
        this.id = id;
        this.active = active;
        this.createdDate = createdDate;
        this.scheduleMapId = scheduleMapId;
        this.doseTime = doseTime;
    }

    public ScheduleTimeMap(Long id, boolean active, Date createdDate, Long scheduleMapId, Time doseTime, Date startDate, Date endDate, String adhString) {
        this.id = id;
        this.active = active;
        this.createdDate = createdDate;
        this.scheduleMapId = scheduleMapId;
        this.doseTime = doseTime;
        this.startDate = startDate;
        this.endDate = endDate;
        this.adhString = adhString;
    }
}
