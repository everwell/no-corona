package com.everwell.iam.models.dto;

import com.everwell.iam.constants.Constants;
import com.everwell.iam.repositories.RegistrationShortRepo;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@JsonInclude(JsonInclude.Include.NON_NULL)
@ToString
public class AdherenceData implements Serializable, RegistrationShortRepo {
    @Setter
    public Long iamId;

    @Setter
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    private Date startDate;

    @Setter
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    private Date endDate;

    @Setter
    private Long adherenceType;

    private String adherenceTypeString;

    @Setter
    private Long scheduleType;

    @Setter
    private String scheduleTypeString;

    @Setter
    private String currentSchedule;

    @Setter
    private String uniqueIdentifier;

    @ApiModelProperty(notes = "Adherence String of this Adherence Method")
    private String adherenceString;

    @Getter
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    private Date createdDate;

    @Setter
    @ApiModelProperty(notes = "Last Dosage of this Adherence Method")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    private Date lastDosage;

    @Setter
    @ApiModelProperty(notes = "Last Dosage of this Adherence Method")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    private Date lastMissedDosage;

    @Setter
    private int technologyDoses;

    @Setter
    private int manualDoses;

    @Setter
    private int totalDoses;

    @Setter
    List<AdhTechLogsData> adhTechLogsData;


    public AdherenceData(String adherenceString, String adherenceTypeString) {
        this.adherenceString = adherenceString;
        this.adherenceTypeString = adherenceTypeString;
    }

    public AdherenceData(Long iamId, String adherenceString, Date startDate, Date endDate, Date createdDate, Long scheduleType) {
        this.iamId = iamId;
        this.adherenceString = adherenceString;
        this.startDate = startDate;
        this.endDate = endDate;
        this.createdDate = createdDate;
        this.scheduleType = scheduleType;
    }

}
