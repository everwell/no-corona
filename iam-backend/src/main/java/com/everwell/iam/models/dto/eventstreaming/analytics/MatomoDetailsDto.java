package com.everwell.iam.models.dto.eventstreaming.analytics;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@ToString
public class MatomoDetailsDto {
    String eventCategory;
    String eventAction;
    String eventName;
}
