package com.everwell.iam.models.dto;

import com.everwell.iam.enums.AverageAdherenceIntervals;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@AllArgsConstructor
@Setter
@Getter
public class CumulativeDoseCountDto {
    Map<AverageAdherenceIntervals,IntervalAdherenceDto> avgAdherenceIntervals;
    public CumulativeDoseCountDto() {
        this.avgAdherenceIntervals = new HashMap<>();
        for (AverageAdherenceIntervals value : AverageAdherenceIntervals.values()) {
            avgAdherenceIntervals.put(value, new IntervalAdherenceDto());
        }
    }
}


