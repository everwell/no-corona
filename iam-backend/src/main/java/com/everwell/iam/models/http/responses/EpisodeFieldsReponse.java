package com.everwell.iam.models.http.responses;

import lombok.AllArgsConstructor;
import lombok.Data;
import java.util.Map;

@Data
@AllArgsConstructor
public class EpisodeFieldsReponse {
    String module;
    Map<String, Object> fields;
}
