package com.everwell.iam.models.http.responses;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;

@Data
@AllArgsConstructor
public class ImeiBulkResponse extends ImeiResponse {
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    private Date endDate;

    public ImeiBulkResponse(String imei, String entityId, Date startDate, Date endDate) {
        super(imei, entityId, startDate);
        this.endDate = endDate;
    }
}
