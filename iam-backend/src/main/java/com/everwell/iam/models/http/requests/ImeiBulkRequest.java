package com.everwell.iam.models.http.requests;


import com.everwell.iam.constants.FieldValidationMessages;
import com.everwell.iam.exceptions.ValidationException;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ImeiBulkRequest {
    List<String> imeiList;

    public void validate() {
        if (CollectionUtils.isEmpty(imeiList)) {
            throw new ValidationException(FieldValidationMessages.IMEI_LIST_MANDATORY);
        }
    }
}
