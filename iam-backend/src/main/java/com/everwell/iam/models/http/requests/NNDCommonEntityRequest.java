package com.everwell.iam.models.http.requests;

import java.util.List;

public interface NNDCommonEntityRequest {
    List<PhoneRequest.PhoneNumbers> getPhoneNumbers();
}
