package com.everwell.iam.models.db;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "IAM_Adhtech")
public class AdherenceTechnology {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    @Setter
    private Long id;

    @Getter
    @Setter
    private String name;

    @Column(name = "created_date")
    @Temporal(TemporalType.DATE)
    private Date createdDate;

}
