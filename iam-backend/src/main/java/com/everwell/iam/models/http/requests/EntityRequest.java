package com.everwell.iam.models.http.requests;

import com.everwell.iam.exceptions.ValidationException;
import com.everwell.iam.models.db.PhoneMap;
import com.everwell.iam.models.db.Registration;
import com.everwell.iam.utils.Utils;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.sql.Time;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@AllArgsConstructor
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "adherenceType", include = JsonTypeInfo.As.EXISTING_PROPERTY)
@JsonSubTypes({
    @JsonSubTypes.Type(value = NNDEntityRequest.class, name = "1"),
    @JsonSubTypes.Type(value = VOTEntityRequest.class, name = "2"),
    @JsonSubTypes.Type(value = MERMEntityRequest.class, name = "3"),
    @JsonSubTypes.Type(value = OpashaEntityRequest.class, name = "4"),
    @JsonSubTypes.Type(value = NoneEntityRequest.class, name = "5"),
    @JsonSubTypes.Type(value = NNDLiteEntityRequest.class, name = "6")
})
@ToString
public class EntityRequest {

    private String entityId;

    private Integer adherenceType;

    private String uniqueIdentifier;

    // for multi-freq start date acts as dayStartTime date
    private String startDate;

    private Long scheduleTypeId = 1L;

    private String scheduleString;

    private Long positiveSensitivity;

    private Long negativeSensitivity;

    private Long firstDoseOffset;

    private Long clientId;

    private List<DoseTimeInfo> doseDetails;

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class DoseTimeInfo {
        String time;
        String extraInfo;
        Integer pillNumber;
    }

    public EntityRequest(String entityId, Integer adherenceType, String uniqueIdentifier) {
        this.entityId = entityId;
        this.adherenceType = adherenceType;
        this.uniqueIdentifier = uniqueIdentifier;
        this.startDate = Utils.getFormattedDate(new Date());
    }

    public void validate(EntityRequest entityRequest) {
        if(StringUtils.isBlank(entityRequest.getEntityId())) {
            throw new ValidationException("entity mapping is required");
        }
        if(null == entityRequest.getAdherenceType()) {
            throw new ValidationException("adherence type mapping is required");
        }
        if(StringUtils.isBlank(entityRequest.getUniqueIdentifier())) {
            throw new ValidationException("unqiue identifier is required");
        }
        if(null == entityRequest.getScheduleTypeId()) {
            throw new ValidationException("Schedule type id is required");
        }
        if (null != entityRequest.getDoseDetails()) {
            try {
                HashSet<String> timeSet = new HashSet<>();
                entityRequest.getDoseDetails().stream().map(m -> {
                    timeSet.add(m.getTime());
                    return Time.valueOf(m.getTime());
                }).collect(Collectors.toList());
                if (timeSet.size() != entityRequest.getDoseDetails().size())
                    throw new ValidationException("Duplicate times are not allowed!");
            } catch (IllegalArgumentException e) {
                throw new ValidationException("Incorrect time format, supported format: HH:mm:ss");
            }
        }
    }

    public EntityRequest (Integer adherenceType) {
        this.adherenceType = adherenceType;
    }

}
