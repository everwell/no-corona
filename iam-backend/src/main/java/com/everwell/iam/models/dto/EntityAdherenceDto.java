package com.everwell.iam.models.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@ToString
public class EntityAdherenceDto {
    public String entityId;
    public String adherenceString;
    public Date startDate;
    public Date endDate;
}
