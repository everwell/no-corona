package com.everwell.iam.models.dto.eventstreaming.analytics;

import com.everwell.iam.models.db.Registration;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class RegisterEntityTrackingDto {
    String entityId;
    Long clientId;
    Long adherenceType;
    boolean isNew;
    String adherenceTechName;
}
