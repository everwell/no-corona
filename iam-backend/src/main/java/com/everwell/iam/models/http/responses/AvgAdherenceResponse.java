package com.everwell.iam.models.http.responses;

import com.everwell.iam.enums.AverageAdherenceIntervals;
import com.everwell.iam.models.dto.IntervalAdherencePercentDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class AvgAdherenceResponse {
    private String entityId;
    private double digitalAdherence;
    private double totalAdherence;
    private Map<AverageAdherenceIntervals, IntervalAdherencePercentDto> averageAdherenceIntervals;

    public void filterIntervals(List<AverageAdherenceIntervals> intervals){
        if (intervals == null){
            this.averageAdherenceIntervals = null;
        }
        else {
            this.averageAdherenceIntervals = intervals.stream()
                    .filter(this.averageAdherenceIntervals::containsKey)
                    .collect(Collectors.toMap(Function.identity(), this.averageAdherenceIntervals::get));
        }
    }
}
