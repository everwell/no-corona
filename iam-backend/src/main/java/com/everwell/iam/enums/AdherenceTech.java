package com.everwell.iam.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum AdherenceTech {
    NNDOTS("99 Dots",1, "99DOTS"),
    VOT("Video Dots",2, "VOT"),
    MERM("MERM Technology",3, "MERM"),
    OPASHA("Operation Asha",4, "OPASHA"),
    NONE("None",5, "None"),
    NNDLITE("99 Dots Lite",6, "99DOTSLite");

    @Getter
    private final String name;

    @Getter
    private final int id;

    @Getter
    private final String techKey;

    public static AdherenceTech getMatch(String name) {
        AdherenceTech matchedTech = null;
        for(AdherenceTech tech: values()) {
            if(tech.name.toLowerCase().equalsIgnoreCase(name)) {
                matchedTech = tech;
                break;
            }
        }
        return matchedTech;
    }

    public static AdherenceTech getMatch(Long techId) {
        AdherenceTech matchedTech = null;
        for(AdherenceTech tech: values()) {
            if(tech.id == techId.intValue()) {
                matchedTech = tech;
                break;
            }
        }
        return matchedTech;
    }
}
