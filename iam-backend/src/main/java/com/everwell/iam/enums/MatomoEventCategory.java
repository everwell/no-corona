package com.everwell.iam.enums;

public enum MatomoEventCategory {
    REGISTER_ENTITY,
    CLOSE_ENTITY,
    RECORD_ADHERENCE,
    REOPEN_CASE,
}
