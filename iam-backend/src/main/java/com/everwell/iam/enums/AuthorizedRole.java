package com.everwell.iam.enums;

import org.springframework.security.core.GrantedAuthority;

public enum AuthorizedRole implements GrantedAuthority {
    AUTHORIZED;

    @Override
    public String getAuthority() {
        return name();
    }
}
