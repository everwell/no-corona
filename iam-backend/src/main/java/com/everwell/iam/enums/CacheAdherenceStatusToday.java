package com.everwell.iam.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum CacheAdherenceStatusToday {
    MANUAL("MANUAL"),
    DIGITAL("DIGITAL"),
    MISSED("MISSED"),
    NO_INFO("NO_INFO");

    @Getter
    private String status;
}
