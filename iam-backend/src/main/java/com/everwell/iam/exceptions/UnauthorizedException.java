package com.everwell.iam.exceptions;

public class UnauthorizedException extends RuntimeException {

    public UnauthorizedException(String exception) {
        super(exception);
    }
}
