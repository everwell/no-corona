package com.everwell.iam.exceptions;

public class ServiceException extends RuntimeException {

    public ServiceException(String exception) {
        super(exception);
    }
}
