package com.everwell.iam.repositories;

import java.util.Date;

public interface RegistrationShortRepo {
        Long getIamId();
        Date getStartDate();
        Date getEndDate();
        Date getCreatedDate();
        String getAdherenceString();
        Long getScheduleType();
}
