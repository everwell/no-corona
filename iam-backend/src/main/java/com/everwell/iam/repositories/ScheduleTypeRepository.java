package com.everwell.iam.repositories;

import com.everwell.iam.models.db.ScheduleType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ScheduleTypeRepository extends JpaRepository<ScheduleType, Long> {
}
