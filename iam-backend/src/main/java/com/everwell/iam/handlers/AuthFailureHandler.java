package com.everwell.iam.handlers;

import com.everwell.iam.exceptions.UnauthorizedException;
import com.everwell.iam.models.Response;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AuthFailureHandler
        implements AuthenticationFailureHandler {

    private static final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public void onAuthenticationFailure(
            HttpServletRequest request,
            HttpServletResponse response,
            AuthenticationException exception)
            throws UnauthorizedException, IOException {

        response.setStatus(HttpStatus.UNAUTHORIZED.value());
        Response errResponse = new Response(exception.getMessage());
        response.getOutputStream()
                .println(objectMapper.writeValueAsString(errResponse));
    }
}
