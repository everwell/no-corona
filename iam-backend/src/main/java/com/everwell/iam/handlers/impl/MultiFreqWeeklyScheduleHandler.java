package com.everwell.iam.handlers.impl;

import com.everwell.iam.enums.AdherenceCodeEnum;
import com.everwell.iam.enums.ScheduleTypeEnum;
import com.everwell.iam.exceptions.ValidationException;
import com.everwell.iam.handlers.ScheduleHandler;
import com.everwell.iam.models.db.*;
import com.everwell.iam.models.dto.DoseTimeData;
import com.everwell.iam.models.dto.ScheduleSensitivity;
import com.everwell.iam.models.dto.StopScheduleExtraInfoDto;
import com.everwell.iam.models.dto.ValidateScheduleDto;
import com.everwell.iam.models.http.requests.EntityRequest;
import com.everwell.iam.repositories.AdhStringLogRepository;
import com.everwell.iam.utils.Utils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.security.InvalidParameterException;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Component
public class MultiFreqWeeklyScheduleHandler extends ScheduleHandler {

    @Autowired
    protected AdhStringLogRepository adhStringLogRepository;

    private String DAILY_SCHEDULE_STRING = "1111111";

    @Override
    public ScheduleTypeEnum getScheduleTypeEnumerator() {
        return ScheduleTypeEnum.MULTI_FREQUENCY;
    }

    @Override
    public void validateNoOverlapSchedule(Schedule schedule, ScheduleSensitivity sensitivity) {
        // for now we just validate that schedule is daily
        if (!schedule.getValue().equals(DAILY_SCHEDULE_STRING))
            throw new ValidationException("Only daily schedule supported!");
    }

    @Override
    protected String getScheduleString(String scheduleValue) {
        return null;
    }

    @Override
    public boolean shouldTakeDose(Long iamId, Date calledDate) {
        return false;
    }

    @Override
    public boolean shouldTakeDoseForSchedule(ScheduleMap scheduleMap, Date calledDate) {
        return false;
    }

    @Override
    public boolean shouldTakeDoseForSchedule(ScheduleMap scheduleMap, Schedule schedule, Date calledDate) {
        return false;
    }

    @Override
    public boolean shouldTakeDose(List<ScheduleMap> scheduleMaps, Map<Long, Schedule> idScheduleMap, Date calledDate) {
        return false;
    }

    @Override
    public char computeAdherenceCodeForDay(List<ScheduleMap> scheduleMaps, Map<Long, Schedule> idScheduleMap, Date calledDate) {
        return 0;
    }

    @Override
    public Date getDateToAttribute(List<ScheduleMap> scheduleMaps, Map<Long, Schedule> idScheduleMap, Date calledDate) {
        return null;
    }

    private ScheduleMap getActiveScheduleMapForDate(List<ScheduleMap> scheduleMaps, Date date) {
        return scheduleMaps.stream()
                .filter(map -> map.getStartDate().compareTo(date) <= 0 && (null == map.getEndDate() || map.getEndDate().compareTo(date) >0))
                .findFirst()
                .orElse(null);
    }

    private List<ScheduleTimeMap> getActiveScheduleMapsForDate(List<ScheduleTimeMap> scheduleTimeMaps, Date date) {
        return scheduleTimeMaps.stream()
                .filter(map -> map.getStartDate().compareTo(date) <= 0 && (null == map.getEndDate() || map.getEndDate().compareTo(date) >0))
                .collect(Collectors.toList());
    }

    @Override
    public String generateAdherenceString(HashMap<Integer, AdherenceStringLog> callMap, Date startDate, Date endDate, long iamId, HashMap<Integer, List<Pair<String, Character>>> dayToAllDoses) {
        List<ScheduleMap> allScheduleMaps = findAllScheduleMappings(iamId).stream()
                .filter(scheduleMap -> {
                    if (null == scheduleMap.getEndDate()) {
                        return true;
                    }
                    return (scheduleMap.getStartDate().compareTo(startDate) <=0 &&  scheduleMap.getEndDate().compareTo(startDate) >= 0) ||
                            (scheduleMap.getStartDate().compareTo(endDate) <= 0 &&  scheduleMap.getEndDate().compareTo(endDate) >= 0) ||
                            (scheduleMap.getStartDate().compareTo(startDate) >= 0 && scheduleMap.getEndDate().compareTo(endDate)  <= 0);
                })
                .collect(Collectors.toList());
        List<Long> allScheduleMapIds = allScheduleMaps.stream().map(ScheduleMap::getId).collect(Collectors.toList());
        List<ScheduleTimeMap> scheduleTimeMapList = scheduleTimeMapRepository.findAllByScheduleMapIdIn(allScheduleMapIds);
        Map<Long, List<ScheduleTimeMap>> scheduleIdTimeMap = new HashMap<>();
        scheduleTimeMapList.forEach(s -> {
            scheduleIdTimeMap.putIfAbsent(s.getScheduleMapId(), new ArrayList<>());
            scheduleIdTimeMap.get(s.getScheduleMapId()).add(s);
        });
        List<ScheduleTimeMap> activeScheduleTimeMaps = scheduleTimeMapList.stream().filter(
                f -> f.getEndDate() == null || startDate.before(endDate)
        ).collect(Collectors.toList());
        generateAdherenceForSchedules(startDate, dayToAllDoses, activeScheduleTimeMaps);
        StringBuilder adherenceStringBuilder = new StringBuilder();
        Calendar start = Calendar.getInstance();
        start.setTime(startDate);
        Calendar end = Calendar.getInstance();
        end.setTime(endDate);
        for (Date date = start.getTime(); start.before(end); start.add(Calendar.DATE, 1), date = start.getTime()) {
            char adherenceCode = AdherenceCodeEnum.NO_INFO.getCode();
            ScheduleMap scheduleMap = getActiveScheduleMapForDate(allScheduleMaps, date);
            List<ScheduleTimeMap> currentScheduleTimeMap = null != scheduleMap ? scheduleIdTimeMap.getOrDefault(scheduleMap.getId(), new ArrayList<>()) : new ArrayList<>();
            int doseCount = currentScheduleTimeMap.size();
            if (!CollectionUtils.isEmpty(currentScheduleTimeMap)) {
                List<ScheduleTimeMap> activeForDate = getActiveScheduleMapsForDate(currentScheduleTimeMap, date);
                doseCount = activeForDate.size();
            }
            if (doseCount > 0) {
                long minutes = TimeUnit.MINUTES.convert(date.getTime() - startDate.getTime(), TimeUnit.MILLISECONDS);
                int day = (int) Math.floor((minutes / 60f) / 24f);
                int numberOfDoseTakenForDay = dayToAllDoses.getOrDefault(day, new ArrayList<>()).size();
                if (numberOfDoseTakenForDay > 0) {
                    adherenceCode = numberOfDoseTakenForDay >= doseCount ? AdherenceCodeEnum.PATIENT_MANUAL.getCode() : AdherenceCodeEnum.MULTI_DOSE_PARTIAL.getCode();
                }
            } else {
                adherenceCode = AdherenceCodeEnum.EMPTY_SCHEDULE.getCode();
            }
            adherenceStringBuilder.append(adherenceCode);
        }
        return adherenceStringBuilder.toString();
    }

    public void generateAdherenceForSchedules(Date startDateForRegen, HashMap<Integer, List<Pair<String, Character>>> dayToAllDoses, List<ScheduleTimeMap> scheduleTimeMapList) {
        for (ScheduleTimeMap scheduleTimeMap : scheduleTimeMapList) {
            Date scheduleEndDate = scheduleTimeMap.getEndDate() == null ? Utils.getCurrentDate() : scheduleTimeMap.getEndDate();
            /* condition to handle case when start date is after end date
             * happens when we do regen for close cases. Basically when we fetch adherence,
             * we need to regen from the last regen date, and if that date is after any 1 of the closed
             * schedules this condition happens.
             * We are not skipping closed schedules in loop, since this route is the only route which regenerates
             * adherence for child schedules, in case any data corruption happens, we can clear strings from db
             * and just do get adherence, it will regen.
             */
            if (startDateForRegen.after(scheduleEndDate)) continue;
            Date startDate = startDateForRegen.after(scheduleTimeMap.getStartDate()) ? startDateForRegen : scheduleTimeMap.getStartDate();
            Calendar start = Calendar.getInstance();
            start.setTime(startDate);
            Calendar end = Calendar.getInstance();
            end.setTime(scheduleEndDate);
            String substringTillStartDateForRegen = "";
            if (scheduleTimeMap.getAdhString() != null)
                substringTillStartDateForRegen = scheduleTimeMap.getAdhString().substring(0, (int) Utils.getDifferenceDays(scheduleTimeMap.getStartDate(), startDate));
            StringBuilder scheduleAdhString = new StringBuilder(substringTillStartDateForRegen);
            for (Date date = start.getTime(); start.before(end); start.add(Calendar.DATE, 1), date = start.getTime()) {
                char adherenceCode = AdherenceCodeEnum.NO_INFO.getCode();
                long minutes = TimeUnit.MINUTES.convert(date.getTime() - startDate.getTime(), TimeUnit.MILLISECONDS);
                int day = (int) Math.floor((minutes / 60f) / 24f);
                Optional<Pair<String, Character>> currentTime = dayToAllDoses.getOrDefault(day, new ArrayList<>()).stream().filter(f -> Utils.getFormattedDateOnlyTime(scheduleTimeMap.getDoseTime()).equals(f.getLeft())).findFirst();
                if (currentTime.isPresent()) {
                    adherenceCode = currentTime.get().getRight();
                }
                scheduleAdhString.append(adherenceCode);
            }
            if (scheduleAdhString.length() > 0)
                scheduleTimeMap.setAdhString(scheduleAdhString.toString());
        }
        scheduleTimeMapRepository.saveAll(scheduleTimeMapList);
    }

    @Override
    public boolean shouldFlagTFNRepeatForNND() {
        return false;
    }

    @Override
    public int getNumCharsInBuffer(Long registrationId, Date registrationStartDate, Date registrationEndDate) {
        return 0;
    }

    @Override
    public void shouldTakeDoseForDates(Set<Date> dates, Long iamId, List<AdherenceStringLog> logs) {
        boolean doseAlreadyExists = logs.stream().anyMatch(l -> dates.contains(l.getRecordDate()));
        if (doseAlreadyExists) {
            throw new ValidationException("Dose has already been marked");
        }
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        List<ScheduleMap> scheduleMapList =  scheduleMapRepository.findAllByIamId(iamId);
        List<Long> scheduleMapIdList = scheduleMapList
                .stream()
                .map(ScheduleMap::getId)
                .collect(Collectors.toList());
        Map<Long, List<ScheduleTimeMap>> scheduleIdToScheduleTimeMap = scheduleTimeMapRepository.findAllByScheduleMapIdIn(scheduleMapIdList)
                .stream()
                .collect(Collectors.groupingBy(ScheduleTimeMap::getScheduleMapId));
        dates.forEach(date -> {
            ScheduleMap scheduleMap = getActiveScheduleMapForDate(scheduleMapList, date);
            List<ScheduleTimeMap> scheduleTimeMapList = scheduleMap != null
                    ? scheduleIdToScheduleTimeMap.get(scheduleMap.getId())
                    : new ArrayList<>();
            if (CollectionUtils.isEmpty(scheduleTimeMapList)) {
                throw new ValidationException("Following schedule time does not exist");
            }
            List<String> doseTime = scheduleTimeMapList
                    .stream()
                    .map(s -> String.valueOf(s.getDoseTime()))
                    .collect(Collectors.toList());
            String timeString = sdf.format(date);
            if (!doseTime.contains(timeString)) {
                throw new ValidationException("Current time is not scheduled");
            }
        });
    }

    private HashMap<Integer, Integer> getDoseFrequencyMap(List<AdherenceStringLog> logList, Date startDate) {
        HashMap<Integer, Integer> doseFrequencyMap = new HashMap<>();
        logList.forEach(al -> {
            long minutes = TimeUnit.MINUTES.convert(al.getRecordDate().getTime() - startDate.getTime(), TimeUnit.MILLISECONDS);
            int day = (int) Math.floor((minutes / 60f) / 24f);
            doseFrequencyMap.put(day, doseFrequencyMap.getOrDefault(day, 0) + 1);
        });
        return doseFrequencyMap;
    }

    @Override
    public Character getTransformedValue(Character value, Long recordDays, long iamId, List<AdherenceStringLog> logs, Date startDate, Date date) {
        ScheduleMap scheduleMap = getActiveScheduleMapForDate(scheduleMapRepository.findAllByIamId(iamId), date);
        List<ScheduleTimeMap> scheduleTimeMapList =  null != scheduleMap
                ? scheduleTimeMapRepository.findAllByScheduleMapIdIn(Arrays.asList(scheduleMap.getId()))
                : new ArrayList<>();
        if (!CollectionUtils.isEmpty(scheduleTimeMapList)) {
            ScheduleTimeMap markingForSchedule = scheduleTimeMapList
                    .stream()
                    .filter(f -> Utils.getFormattedDateOnlyTime(f.getDoseTime()).equals(Utils.getFormattedDateOnlyTime(date)))
                    .findFirst()
                    .get();
            long markingForDayIndex = Utils.getDifferenceDays(markingForSchedule.getStartDate(), date);
            String adherenceString = markingForSchedule.getAdhString();
            adherenceString = adherenceString.substring(0, (int) markingForDayIndex) + value + adherenceString.substring((int) markingForDayIndex + 1);
            markingForSchedule.setAdhString(adherenceString);
            scheduleTimeMapRepository.save(markingForSchedule);
        }
        int doseCount = scheduleTimeMapList.size();
        HashMap<Integer, Integer> doseFrequencyMap = getDoseFrequencyMap(logs, startDate);
        int numberOfDoseTaken = doseFrequencyMap.getOrDefault(recordDays.intValue(), 0);
        char transformedValue = AdherenceCodeEnum.NO_INFO.getCode();
        if (numberOfDoseTaken > 0) {
            transformedValue = numberOfDoseTaken >= doseCount ? AdherenceCodeEnum.PATIENT_MANUAL.getCode() : AdherenceCodeEnum.MULTI_DOSE_PARTIAL.getCode();
        }
        return transformedValue;
    }

    @Override
    public List<DoseTimeData> getDoseTimes(Long iamId) {
        List<DoseTimeData> doseTimeDataList = new ArrayList<>();
        Map<Long, ScheduleMap> scheduleIdToScheduleMap = scheduleMapRepository.findAllByIamId(iamId)
                .stream()
                .collect(Collectors.toMap(ScheduleMap::getId, i->i));
        List<Long> scheduleMapIdList =  new ArrayList<>(scheduleIdToScheduleMap.keySet());
        List<ScheduleTimeMap> scheduleIdToScheduleTimeMap = scheduleTimeMapRepository.findAllByScheduleMapIdIn(scheduleMapIdList);
        scheduleIdToScheduleTimeMap.forEach(data -> {
            doseTimeDataList.add(new DoseTimeData(data.getStartDate(), data.getEndDate(), data.getDoseTime(), data.getAdhString(), data.getDoseInfo()));
        });
        return doseTimeDataList;
    }

    @Override
    public boolean isLogsRequired() {
        return true;
    }

    @Override
    public boolean isDoseTimeSupported() {
        return true;
    }

    @Override
    public boolean hasScheduleMapping() {
        return true;
    }

    @Override
    public StopScheduleExtraInfoDto stopSchedulePartiallyOrFull(ScheduleMap scheduleMap, List<String> dates, Date dayStartTime) {
        Character code = null;
        Date currentDateTime = Utils.getCurrentDate();
        boolean scheduleLastDayToday = false;
        List<ScheduleTimeMap> schedulesToDelete = new ArrayList<>();
        if (hasScheduleMapping()) {
            if (dayStartTime == null) { // re-open case
                List<ScheduleTimeMap> scheduleTimeMapList = scheduleTimeMapRepository.findAllByScheduleMapIdIn(Arrays.asList(scheduleMap.getId()));
                for (ScheduleTimeMap scheduleTimeMap : scheduleTimeMapList) {
                    scheduleTimeMap.setEndDate(null);
                    scheduleTimeMap.setActive(true);
                }
                scheduleTimeMapRepository.saveAll(scheduleTimeMapList);
            } else {
                List<ScheduleTimeMap> scheduleTimeMapList = scheduleTimeMapRepository.findAllByScheduleMapIdInAndEndDateIsNull(Arrays.asList(scheduleMap.getId()));
                Time now = new Time(Utils.getCurrentTimeWithoutDate());
                for (ScheduleTimeMap scheduleTimeMap : scheduleTimeMapList) {
                    if (dates == null || dates.contains(Utils.getFormattedDateOnlyTime(scheduleTimeMap.getDoseTime()))) {
                        if (dayStartTime.equals(scheduleTimeMap.getStartDate())) {
                            // hard delete same day schedules
                            schedulesToDelete.add(scheduleTimeMap);
                        } else if (scheduleTimeMap.getDoseTime().before(now) || scheduleTimeMap.getDoseTime().equals(now)) {
                            // compute new adherence code for today after deleting some schedules
                            code = xorAdherenceCodes(scheduleTimeMap.getAdhString(), code);
                            scheduleTimeMap.setActive(false);
                            scheduleTimeMap.setEndDate(currentDateTime);
                            scheduleLastDayToday = true;
                        } else {
                            // close yesterday
                            scheduleTimeMap.setActive(false);
                            scheduleTimeMap.setEndDate(DateUtils.addSeconds(dayStartTime, -1)); //day end time for yesterday
                            scheduleTimeMap.setAdhString(scheduleTimeMap.getAdhString().substring(0, scheduleTimeMap.getAdhString().length() - 1));
                        }
                    } else {
                        // compute new adherence code for today after deleting some schedules
                        code = xorAdherenceCodes(scheduleTimeMap.getAdhString(), code);
                    }
                }
                scheduleTimeMapRepository.saveAll(scheduleTimeMapList);
                if (!CollectionUtils.isEmpty(schedulesToDelete)) {
                    List<Date> timeToDeleteList = schedulesToDelete.stream().map(m -> Utils.formatDate(m.getDoseTime(), "HH:mm:ss")).collect(Collectors.toList());
                    scheduleTimeMapRepository.deleteAll(schedulesToDelete);
                    adhStringLogRepository.deleteByIamIdAndRecordDateIn(scheduleMap.getIamId(), dayStartTime, timeToDeleteList);
                }
            }
        }
        return new StopScheduleExtraInfoDto(code, scheduleLastDayToday ? currentDateTime : null, !CollectionUtils.isEmpty(schedulesToDelete));
    }

    Character xorAdherenceCodes (String adherenceString, Character code) {
        char currCode = adherenceString.charAt(adherenceString.length() - 1);
        return code == null || currCode == code ? currCode : AdherenceCodeEnum.MULTI_DOSE_PARTIAL.getCode();
    }

    @Override
    public void save(Long iamId, Date startDate, String scheduleString, Long firstScheduleOffset, ScheduleSensitivity sensitivity, List<EntityRequest.DoseTimeInfo> doseDetails) {
        sensitivity.setLeftBuffer(0L);
        sensitivity.setRightBuffer(0L);
        ScheduleMap scheduleMap = new ScheduleMap(iamId, null, startDate, 0L, sensitivity);
        Long scheduleMapId = scheduleMapRepository.save(scheduleMap).getId();
        List<ScheduleTimeMap> scheduleTimeMapList = new ArrayList<>();
        doseDetails.forEach(d -> {
            ScheduleTimeMap scheduleTimeMap = new ScheduleTimeMap(null, true, Utils.formatDate(Utils.getCurrentDate()), scheduleMapId, Time.valueOf(d.getTime()), startDate, null, null, Utils.asJsonString(d));
            scheduleTimeMapList.add(scheduleTimeMap);
        });
        scheduleTimeMapRepository.saveAll(scheduleTimeMapList);
    }

    @Override
    public void validateScheduleAndSensitivityOverLap(ValidateScheduleDto validateScheduleDto) {
        super.validateScheduleAndSensitivityOverLap(validateScheduleDto);
        List<Time> timeList = validateScheduleDto.getDoseDetails().stream().map(EntityRequest.DoseTimeInfo::getTime).map(Time::valueOf).collect(Collectors.toList());
        long distinctTimeCount = timeList.stream().distinct().count();
        if (distinctTimeCount != validateScheduleDto.getDoseDetails().size()) {
            throw new InvalidParameterException("Schedule can't overlap");
        }
    }

    @Override
    public StopScheduleExtraInfoDto updateMultiDoseSchedule(List<EntityRequest.DoseTimeInfo> doseDetails, Registration registration, Date dayStartTime) {
        ScheduleMap scheduleMap = getActiveScheduleMapForIam(registration.getId());
        List<ScheduleTimeMap> scheduleTimeMapList = scheduleTimeMapRepository.findAllByScheduleMapIdInAndEndDateIsNull(Arrays.asList(scheduleMap.getId()));
        List<String> scheduleTimeMapsToDeleteStr = new ArrayList<>();
        Character currCodeForToday = null;
        List<String> existingDates = new ArrayList<>();
        List<ScheduleTimeMap> newScheduleTimeMapList = new ArrayList<>();
        for (ScheduleTimeMap scheduleTimeMap : scheduleTimeMapList) {
            String formattedTime = Utils.getFormattedDateOnlyTime(scheduleTimeMap.getDoseTime());
            Optional<EntityRequest.DoseTimeInfo> doseTimeInfo = doseDetails.stream().filter(m -> m.getTime().equals(formattedTime)).findFirst();
            if (doseTimeInfo.isPresent()) {
                scheduleTimeMap.setDoseInfo(Utils.asJsonString(doseTimeInfo.get()));
                existingDates.add(formattedTime);
                newScheduleTimeMapList.add(scheduleTimeMap);
                currCodeForToday = xorAdherenceCodes(scheduleTimeMap.getAdhString(), currCodeForToday);
            } else {
                scheduleTimeMapsToDeleteStr.add(formattedTime);
            }
        }
        StopScheduleExtraInfoDto stopScheduleExtraInfoDto = new StopScheduleExtraInfoDto();
        if (!CollectionUtils.isEmpty(scheduleTimeMapsToDeleteStr)) {
            stopScheduleExtraInfoDto = stopSchedulePartiallyOrFull(scheduleMap, scheduleTimeMapsToDeleteStr, dayStartTime);
            stopScheduleExtraInfoDto.setScheduleDeleted(true);
        }
        if (!CollectionUtils.isEmpty(doseDetails)) {
            boolean addingNewMeds = false;
            for (EntityRequest.DoseTimeInfo doseDetail : doseDetails) {
                if (!existingDates.contains(doseDetail.getTime())) {
                    ScheduleTimeMap scheduleTimeMap = new ScheduleTimeMap(null, true, Utils.formatDate(Utils.getCurrentDate()), scheduleMap.getId(), Time.valueOf(doseDetail.getTime()), dayStartTime, null, String.valueOf(AdherenceCodeEnum.NO_INFO.getCode()), Utils.asJsonString(doseDetail));
                    newScheduleTimeMapList.add(scheduleTimeMap);
                    addingNewMeds = true;
                }
            }
            if (addingNewMeds) {
                if (stopScheduleExtraInfoDto.getAdherenceCode() != null)
                    currCodeForToday = stopScheduleExtraInfoDto.getAdherenceCode();
                if (currCodeForToday == null)
                    currCodeForToday = AdherenceCodeEnum.NO_INFO.getCode();
                // when we add a new schedule, new adh code will always be F, if it was not 6 before
                stopScheduleExtraInfoDto.setAdherenceCode(AdherenceCodeEnum.NO_INFO.getCode() == currCodeForToday ? currCodeForToday : AdherenceCodeEnum.MULTI_DOSE_PARTIAL.getCode());
            }
            scheduleTimeMapRepository.saveAll(newScheduleTimeMapList);
        }
        return stopScheduleExtraInfoDto;
    }
}
