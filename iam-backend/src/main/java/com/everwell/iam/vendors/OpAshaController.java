package com.everwell.iam.vendors;

import com.everwell.iam.enums.AdherenceCodeEnum;
import com.everwell.iam.exceptions.ValidationException;
import com.everwell.iam.handlers.impl.OpAshaHandler;
import com.everwell.iam.models.Response;
import com.everwell.iam.models.db.Registration;
import com.everwell.iam.models.http.requests.RecordAdherenceRequest;
import com.everwell.iam.models.http.responses.RecordAdherenceResponse;
import com.everwell.iam.services.AdherenceService;
import com.everwell.iam.utils.Utils;
import com.everwell.iam.vendors.models.http.requests.OpAshaRecordAdherenceRequest;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
public class OpAshaController {

    private static Logger LOGGER = LoggerFactory.getLogger(OpAshaController.class);

    @Autowired
    private OpAshaHandler opAshaHandler;

    @Autowired
    private AdherenceService adherenceService;

    @RequestMapping(value = "/v1/opasha/record/adherence", method = RequestMethod.POST)
    public ResponseEntity<Response<RecordAdherenceResponse>> recordAdherence(@RequestBody OpAshaRecordAdherenceRequest request) {

      Registration iamReg = opAshaHandler.getActiveIam(request.getEntityId(), opAshaHandler.getClientId());
      Date date = Utils.formatDate(new Date());
      if(StringUtils.isNotBlank(request.getDate())) {
        try {
          date = Utils.convertStringToDate(request.getDate());
        } catch (ParseException pE) {
          throw new ValidationException("Unable to parse date");
        }
      }
      List<Long> iamIds = new ArrayList<>();
      iamIds.add(iamReg.getId());
      RecordAdherenceRequest recordAdherenceRequest = new RecordAdherenceRequest(AdherenceCodeEnum.RECEIVED.getCode(), opAshaHandler.getAdherenceTechId());
      recordAdherenceRequest.setDate(date.toString());
      RecordAdherenceResponse response = adherenceService.recordAdherence(recordAdherenceRequest, iamIds);
      return new ResponseEntity<>(new Response<>(response, "entity request accepted"), HttpStatus.OK);
    }
}
