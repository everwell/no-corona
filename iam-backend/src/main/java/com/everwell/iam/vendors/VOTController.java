package com.everwell.iam.vendors;

import com.everwell.iam.annotations.IsAuthorized;
import com.everwell.iam.handlers.impl.VOTHandler;
import com.everwell.iam.models.Response;
import com.everwell.iam.models.db.Registration;
import com.everwell.iam.models.http.requests.RecordAdherenceRequest;
import com.everwell.iam.models.http.responses.RecordAdherenceResponse;
import com.everwell.iam.services.AdherenceService;
import com.everwell.iam.utils.Utils;
import com.everwell.iam.vendors.models.db.VideoEntityMapping;
import com.everwell.iam.vendors.models.http.requests.VOTRecordAdherenceRequest;
import com.everwell.iam.vendors.models.http.requests.VOTReviewAdherenceRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
public class VOTController {

    private static Logger LOGGER = LoggerFactory.getLogger(VOTController.class);

    @Autowired
    private VOTHandler votHandler;

    @Autowired
    private AdherenceService adherenceService;

    @IsAuthorized
    @RequestMapping(value = "/v1/vot/video/upload", method = RequestMethod.POST)
    public ResponseEntity<Response<RecordAdherenceResponse>> uploadVideo(@RequestBody VOTRecordAdherenceRequest adherenceRequest,
                                              @RequestHeader(name = "X-IAM-Client-Id") Long clientId) {
        LOGGER.info("[uploadVideo] request received: " + adherenceRequest);
        votHandler.validateVideoId(adherenceRequest.getVideoId());
        Registration iamReg = votHandler.getActiveIam(adherenceRequest.getEntityId(), clientId);
        Character value = votHandler.getUploadedValue();

        RecordAdherenceRequest recordAdherenceRequest = new RecordAdherenceRequest(value, votHandler.getAdherenceTechId());

        // As /video/upload is called from different clients along with VOT, it can come without date also. so handled here.
        String date = adherenceRequest.getDate() != null ? adherenceRequest.getDate() : Utils.getFormattedDate(new Date());

        recordAdherenceRequest.setDate(date);
        List<Long> iamIds = new ArrayList<>();
        iamIds.add(iamReg.getId());
        RecordAdherenceResponse response = adherenceService.recordAdherence(recordAdherenceRequest, iamIds);
        if(!CollectionUtils.isEmpty(response.getIamLogIds())) {
            votHandler.recordVideoMapping(adherenceRequest.getVideoId(), recordAdherenceRequest, iamReg.getId());
        }

        return new ResponseEntity<>(new Response<>(response, "video request accepted"), HttpStatus.ACCEPTED);
    }

    @IsAuthorized
    @RequestMapping(value = "/v1/vot/video/status", method = RequestMethod.PUT)
    public ResponseEntity<Response<RecordAdherenceResponse>> updateVideoStatus(@RequestBody VOTReviewAdherenceRequest reviewAdherenceRequest,
                                                    @RequestHeader(name = "X-IAM-Client-Id") Long clientId) {

        LOGGER.info("[updateVideoStatus] request received: " + reviewAdherenceRequest);
        Registration iamRegistration = votHandler.getActiveIam(reviewAdherenceRequest.getEntityId(), clientId);
        VideoEntityMapping mapping = votHandler.updateVideoMapping(reviewAdherenceRequest.getVideoId(), iamRegistration.getId(), reviewAdherenceRequest.getVideoReviewStatus().getCode());
        Date uploadDate = mapping.getUploadDate();
        Character value = votHandler.getAdherenceValue(reviewAdherenceRequest.getVideoReviewStatus());
        RecordAdherenceRequest recordAdherenceRequest = new RecordAdherenceRequest(value, votHandler.getAdherenceTechId());
        List<Long> iamIds = new ArrayList<>();
        iamIds.add(iamRegistration.getId());
        recordAdherenceRequest.setDate(uploadDate.toString());
        RecordAdherenceResponse response = adherenceService.recordAdherence(recordAdherenceRequest, iamIds);

        return new ResponseEntity<>(new Response<>(response, "entity request accepted"), HttpStatus.ACCEPTED);
    }
}
