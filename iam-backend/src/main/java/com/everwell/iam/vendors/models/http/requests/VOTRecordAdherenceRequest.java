package com.everwell.iam.vendors.models.http.requests;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@ToString
public class VOTRecordAdherenceRequest {

    private String videoId;

    private String date;

    private String entityId;
}
