package com.everwell.iam.vendors.models.http.requests;

import com.everwell.iam.vendors.enums.VideoReviewStatus;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class VOTReviewAdherenceRequest {

    private String entityId;

    private String videoId;

    private VideoReviewStatus videoReviewStatus;
}
