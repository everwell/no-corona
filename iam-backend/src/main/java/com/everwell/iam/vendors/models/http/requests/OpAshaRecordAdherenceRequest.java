package com.everwell.iam.vendors.models.http.requests;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class OpAshaRecordAdherenceRequest {

    private String entityId;

    private String date;

    @JsonProperty("Metadata")
    private JsonNode metadata;
}
