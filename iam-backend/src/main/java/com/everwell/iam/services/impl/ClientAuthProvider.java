package com.everwell.iam.services.impl;

import com.everwell.iam.models.db.CAccess;
import com.everwell.iam.repositories.CAccessRepository;
import com.everwell.iam.services.AuthProvider;
import com.everwell.iam.utils.JwtUtils;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
public class ClientAuthProvider extends AuthProvider {

    private static final Logger LOGGER = LoggerFactory.getLogger(ClientAuthProvider.class);

    @Autowired
    private CAccessRepository cAccessRepository;

    @Override
    protected void validate(String token, Long id) {
        CAccess access = cAccessRepository.findById(id).orElse(null);
        try {
            if (null == access || !JwtUtils.isValidToken(token, access.getPassword() + access.getName() + id)) {
                LOGGER.warn("[validate] Access denied for " + id + " with token " + token);
                throw new BadCredentialsException("Invalid token - " + token);
            }
        } catch (ExpiredJwtException | UnsupportedJwtException | SignatureException signatureException) {
            LOGGER.warn("[validate] Signature error for " + id + " with token " + token);
            throw new BadCredentialsException("Invalid token - " + token);
        }
    }
}
