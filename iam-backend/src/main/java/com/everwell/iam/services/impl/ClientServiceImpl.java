package com.everwell.iam.services.impl;

import com.everwell.iam.exceptions.ConflictException;
import com.everwell.iam.exceptions.NotFoundException;
import com.everwell.iam.exceptions.ServiceException;
import com.everwell.iam.exceptions.ValidationException;
import com.everwell.iam.models.db.CAccess;
import com.everwell.iam.models.http.requests.RegisterClientRequest;
import com.everwell.iam.models.http.responses.ClientResponse;
import com.everwell.iam.repositories.CAccessRepository;
import com.everwell.iam.services.ClientService;
import com.everwell.iam.utils.AuthenticationUtil;
import com.everwell.iam.utils.JwtUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.security.spec.InvalidKeySpecException;

@Service
public class ClientServiceImpl implements ClientService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ClientServiceImpl.class);

    @Autowired
    private CAccessRepository accessRepository;

    @Override
    public ClientResponse registerClient(RegisterClientRequest clientRequest) {
        String salt = AuthenticationUtil.generateSalt(30);

        CAccess existingClient = accessRepository.findByName(clientRequest.getName());
        if (null != existingClient) {
            LOGGER.error("[registerClient] User already exists");
            throw new ConflictException("User already exists");
        }
        // Create encrypted password
        String encryptedPassword;
        try {
            encryptedPassword = AuthenticationUtil.
                    generateSecurePassword(clientRequest.getPassword(), salt);
        } catch (InvalidKeySpecException ex) {
            throw new ServiceException(ex.getLocalizedMessage());
        }

        CAccess newClient = new CAccess(clientRequest.getName(), encryptedPassword);
        accessRepository.save(newClient);
        Long clientId = newClient.getId();
        String tokenSecret = newClient.getPassword() + newClient.getName() + clientId;
        String jwtToken = JwtUtils.generateToken(String.valueOf(clientId), tokenSecret);
        return new ClientResponse(newClient, jwtToken, JwtUtils.getExpirationDateFromToken(jwtToken, tokenSecret).getTime());
    }

    /**
     * Do not use this method directly.
     * Make sure all the entities for this client is removed before we do this
     * @param id
     * @return
     */
    @Override
    public boolean deleteClient(Long id) {
        accessRepository.deleteById(id);
        return true;
    }

    @Override
    public ClientResponse getClient(Long id) {
        if (null == id) {
            LOGGER.error("[getClient] id cannot be null");
            throw new ValidationException("id cannot be null");
        }
        CAccess client = getClientById(id);
        if (null == client) {
            LOGGER.error("[getClient] No client found with id:" + id);
            throw new NotFoundException("No client found with id:" + id);
        }
        String tokenSecret = client.getPassword() + client.getName() + id;
        String jwtToken = JwtUtils.generateToken(String.valueOf(id), tokenSecret);
        return new ClientResponse(client, jwtToken, JwtUtils.getExpirationDateFromToken(jwtToken, tokenSecret).getTime());
    }

    @Override
    public CAccess getClientById(Long id) {
        return accessRepository.findById(id).orElse(null);
    }
}
