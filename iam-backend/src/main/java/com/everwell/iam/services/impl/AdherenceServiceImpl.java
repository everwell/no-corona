package com.everwell.iam.services.impl;

import com.everwell.iam.constants.Constants;
import com.everwell.iam.enums.*;
import com.everwell.iam.exceptions.ConflictException;
import com.everwell.iam.exceptions.NotFoundException;
import com.everwell.iam.exceptions.ValidationException;
import com.everwell.iam.handlers.AdhTechHandlerMap;
import com.everwell.iam.handlers.AdherenceTechHandler;

import com.everwell.iam.handlers.ScheduleHandler;
import com.everwell.iam.handlers.ScheduleTypeHandlerMap;
import com.everwell.iam.models.db.*;
import com.everwell.iam.models.dto.*;
import com.everwell.iam.models.http.requests.AdherenceGlobalConfigRequest;
import com.everwell.iam.models.http.requests.RecordAdherenceRequest;
import com.everwell.iam.models.http.requests.EntityRequest;
import com.everwell.iam.models.http.responses.*;
import com.everwell.iam.repositories.*;
import com.everwell.iam.services.AdherenceService;
import com.everwell.iam.services.RabbitMQPublisherService;
import com.everwell.iam.utils.CacheUtils;
import com.everwell.iam.utils.Utils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.math.BigInteger;
import java.security.InvalidParameterException;
import java.text.ParseException;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.everwell.iam.constants.Constants.CACHE_USER_PREFIX;
import static com.everwell.iam.utils.Utils.getCurrentDate;
import static com.everwell.iam.utils.Utils.getDifferenceDays;
import static com.everwell.iam.utils.Utils.getExactDifferenceDays;
import static java.util.stream.Collectors.groupingBy;


@Service
public class AdherenceServiceImpl implements AdherenceService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AdherenceServiceImpl.class);

    @Autowired
    private RegistrationRepository registrationRepository;

    @Autowired
    private AdhTechHandlerMap adherenceTechnologyHandlerMap;

    @Autowired
    private ScheduleTypeHandlerMap scheduleTypeHandlerMap;

    @Autowired
    private RabbitMQPublisherService rabbitMQPublisherService;

    @Override
    public boolean stopAdherence(Long iamId, Date endDate) {

        if (null == endDate) {
            endDate = Utils.getCurrentDate();
        }

        Registration registration = registrationRepository.findById(iamId).orElse(null);
        if (null == registration) {
            throw new NotFoundException("entity was not found");
        } else if (endDate.before(registration.getStartDate())) {
            throw new ValidationException("end date cannot be before start date");
        }
        ScheduleHandler scheduleHandler = scheduleTypeHandlerMap.getHandler(registration.getScheduleTypeId());
        AdherenceTechHandler handler = adherenceTechnologyHandlerMap.getHandler(registration.getAdTechId());
        if (scheduleHandler.isDoseTimeSupported()) {
            // update adherence if haven't already
            // this is necessary since child schedules adherence can differ so that is calculated
            // during run-time by stop-schedule, and getAdherence works by checking the registration
            // level adherence length, which will be correct, but child ones would still need regen.
            getAdherence(registration, false);
        }
        registration.setEndDate(endDate);
        registration.setUpdatedDate(Utils.getCurrentDate());
        StopScheduleExtraInfoDto stopScheduleExtraInfoDto = scheduleHandler.stopSchedule(iamId, endDate);
        if (stopScheduleExtraInfoDto != null) {
            // no need for get adherence, this during closing treatment calculates adh code
            // we can do this because end date will always be = today
            if (stopScheduleExtraInfoDto.getEndDate() != null)
                registration.setEndDate(stopScheduleExtraInfoDto.getEndDate());
            registration.setAdherenceString(registration.getAdherenceString().substring(0, registration.getAdherenceString().length() - 1) + stopScheduleExtraInfoDto.getAdherenceCode());
            // cases when dose is marked and then schedule is deleted, last date needs to be refreshed
            if (stopScheduleExtraInfoDto.isScheduleDeleted() && stopScheduleExtraInfoDto.getAdherenceCode() != null) {
                Date lastDosage = null;
                if (registration.getAdherenceString().length() > 0)
                    lastDosage = handler.computeLastDosage(registration.getAdherenceString(), registration.getEndDate(), false);
                registration.setLastDosage(lastDosage);
                handler.publishLastDosageEventOnRabbitMQ(null, registrationRepository.getOne(registration.getId()), DosageTypeEnum.LAST_DOSAGE.getName());
            }
        } else {
            //if regen adherence happens, that will update adherence separately
            getAdherence(registration, false);  //to update adherence based on new end date
        }
        registrationRepository.save(registration);
        handler.preDeletion(registration);
        return true;
    }

    @Override
    public boolean stopAdherence(Long iamId) {
        return stopAdherence(iamId, new Date());
    }

    @Override
    public AdherenceResponse getAdherence(Long iamId, Boolean logsRequired) {
        LOGGER.debug("fetch for iam " + iamId);
        Registration registration = registrationRepository.findById(iamId).orElse(null);
        if (null == registration) {
            throw new NotFoundException("entity is not registered");
        }
        return getAdherence(registration, logsRequired);
    }

    @Override
    public AdherenceResponse getAdherence(Registration registration, Boolean logsRequired) {
        AdherenceTechHandler handler = adherenceTechnologyHandlerMap.getHandler(registration.getAdTechId());
        ScheduleHandler scheduleHandler = scheduleTypeHandlerMap.getHandler(registration.getScheduleTypeId());
        String activeScheduleString = scheduleHandler.getActiveScheduleString(registration.getId());

        String adherenceString = registration.getAdherenceString();
        AdherenceData adherenceDataForIam;
        AdherenceStatistics adherenceStats;

        Date endDate = registration.getEndDate();
        if(null == endDate) {
            endDate = Utils.getCurrentDate();
        }

        long noOfDaysBetween = getDifferenceDays(registration.getStartDate(), endDate);
        long stringCount = (null == adherenceString)?0:adherenceString.length();

        if(noOfDaysBetween + 1 - stringCount > 0) {
            //If adherence string is incorrect and is shorter than treatment duration, regenerate missing

            Date regenerateDate = DateUtils.addDays(registration.getStartDate(), (int) stringCount);

            String regenAdherenceString = (StringUtils.isNotBlank(registration.getAdherenceString())?registration.getAdherenceString():"") + handler.generateAdherenceString(registration.getId(), regenerateDate, endDate, registration.getScheduleTypeId());
            registration.setAdherenceString(regenAdherenceString);

            Date olderLastDosage = registration.getLastDosage();
            Date lastDosage = handler.computeLastDosage(regenAdherenceString, endDate,false);
            Date olderLastMissedDosage = registration.getLastMissedDosage();
            Date lastMissedDosage = handler.computeLastDosage(regenAdherenceString,endDate,true);
            registration.setLastDosage(lastDosage);
            registration.setLastMissedDosage(lastMissedDosage);
            registrationRepository.save(registration);
            if (null !=lastDosage  && !lastDosage.equals(olderLastDosage)) {
                handler.publishLastDosageEventOnRabbitMQ(null, registrationRepository.getOne(registration.getId()), DosageTypeEnum.LAST_DOSAGE.getName());
            }
            if(null != lastMissedDosage && !lastMissedDosage.equals(olderLastMissedDosage)) {
                handler.publishLastDosageEventOnRabbitMQ(null, registrationRepository.getOne(registration.getId()),DosageTypeEnum.LAST_MISSED_DOSAGE.getName());
            }
            adherenceStats = new AdherenceStatistics(regenAdherenceString);
            adherenceDataForIam = new AdherenceData(registration.getId(), registration.getStartDate(), registration.getEndDate(),
                    registration.getAdTechId(), handler.getAdhTechEnumerator().getName(), registration.getScheduleTypeId(), scheduleHandler.getScheduleTypeEnumerator().getName(), activeScheduleString, registration.getUniqueIdentifier(),
                    regenAdherenceString, registration.getCreatedDate(), registration.getLastDosage(),registration.getLastMissedDosage(),adherenceStats.getTechnologyDoses(), adherenceStats.getManualDoses(),adherenceStats.getTotalDoses(), (logsRequired != null && logsRequired) ? handler.getTechLogs(registration.getId()) : null);

        } else if(noOfDaysBetween + 1 - stringCount < 0) {
            //If adherence string is incorrect and is longer than treatment duration, regenerate all
             adherenceDataForIam = handler.getAdherenceDataForIam(registration.getId(), registration.getStartDate(), registration.getEndDate(), registration.getScheduleTypeId(), registration.getLastDosage(),registration.getLastMissedDosage());
            String regeneratedString = adherenceDataForIam.getAdherenceString();
            Date olderLastDosage = registration.getLastDosage();
            Date LastDosage = adherenceDataForIam.getLastDosage();
            Date olderLastMissedDosage = registration.getLastMissedDosage();
            Date lastMissedDosage = adherenceDataForIam.getLastMissedDosage();
            //Update Adherence
            registration.setLastDosage(LastDosage);
            registration.setAdherenceString(regeneratedString);
            registration.setLastMissedDosage(adherenceDataForIam.getLastMissedDosage());
            registrationRepository.save(registration);
            if (null !=LastDosage && !LastDosage.equals(olderLastDosage))
                handler.publishLastDosageEventOnRabbitMQ(null, registrationRepository.getOne(registration.getId()),DosageTypeEnum.LAST_DOSAGE.getName());
            if(null != lastMissedDosage && !lastMissedDosage.equals(olderLastMissedDosage)) {
                handler.publishLastDosageEventOnRabbitMQ(null, registrationRepository.getOne(registration.getId()),DosageTypeEnum.LAST_MISSED_DOSAGE.getName());
            }
            adherenceDataForIam.setLastDosage(registration.getLastDosage());
            adherenceDataForIam.setLastMissedDosage(registration.getLastMissedDosage());
            adherenceDataForIam.setStartDate(registration.getStartDate());
            adherenceDataForIam.setEndDate(registration.getEndDate());
            adherenceDataForIam.setUniqueIdentifier(registration.getUniqueIdentifier());
            adherenceDataForIam.setAdherenceType(registration.getAdTechId());
            adherenceDataForIam.setScheduleType(registration.getScheduleTypeId());
            adherenceDataForIam.setScheduleTypeString(scheduleHandler.getScheduleTypeEnumerator().getName());
            adherenceDataForIam.setScheduleTypeString(activeScheduleString);
            adherenceDataForIam.setIamId(registration.getId());
            adherenceDataForIam.setAdhTechLogsData((logsRequired != null && logsRequired) ? handler.getTechLogs(registration.getId()) : null);
            adherenceStats = new AdherenceStatistics(regeneratedString);
            adherenceDataForIam.setTechnologyDoses(adherenceStats.getTechnologyDoses());
            adherenceDataForIam.setManualDoses(adherenceStats.getManualDoses());
            adherenceDataForIam.setTotalDoses(adherenceStats.getTotalDoses());

        } else {
            adherenceStats = new AdherenceStatistics(adherenceString);
            adherenceDataForIam = new AdherenceData(registration.getId(), registration.getStartDate(), registration.getEndDate(),
                    registration.getAdTechId(), handler.getAdhTechEnumerator().getName(), registration.getScheduleTypeId(), scheduleHandler.getScheduleTypeEnumerator().getName(), activeScheduleString, registration.getUniqueIdentifier(),
                    adherenceString, registration.getCreatedDate(), registration.getLastDosage(), registration.getLastMissedDosage(), adherenceStats.getTechnologyDoses(), adherenceStats.getManualDoses(), adherenceStats.getTotalDoses(), (logsRequired != null && logsRequired) ? handler.getTechLogs(registration.getId()) : null);
        }

        AdherenceResponse response = new AdherenceResponse();
        response.setAdherenceData(Arrays.asList(adherenceDataForIam));
        return response;
    }

    @Override
    public RecordAdherenceResponse recordAdherence(RecordAdherenceRequest recordAdherenceRequest, List<Long> iamIds) {
        RecordAdherenceResponse response = null;

        Map<String, Character> adherenceMap = recordAdherenceRequest.getDatesToValueMapList();
        if(CollectionUtils.isEmpty(adherenceMap) && null != recordAdherenceRequest.getDate() && null != recordAdherenceRequest.getValue()) {
            // Only 1 date has been passed for recording of adherence
            adherenceMap = new HashMap<>();
            adherenceMap.put(recordAdherenceRequest.getDate(), recordAdherenceRequest.getValue());
        }

        TreeMap<Date, AdherenceCodeEnum> sortedAdherenceMap = getOrderedAdherenceMap(adherenceMap);
        List<Registration> registrationList = registrationRepository.findByIdIn(iamIds);

        response = updateAdherenceMap(sortedAdherenceMap, registrationList);

        return response;
    }

    public TreeMap<Date, AdherenceCodeEnum> getOrderedAdherenceMap(Map<String, Character> adherenceMap) {
        TreeMap<Date, AdherenceCodeEnum> validDatesMap = new TreeMap<>();
        for (Map.Entry<String, Character> entry : adherenceMap.entrySet()) {
            try {
                /**
                 * checking validity of all the dates and adherence codes,
                 * rejection with message = one or more dates with invalid format
                 **/
                Date recordDate = Utils.convertStringToDate(entry.getKey());
                AdherenceCodeEnum adherenceCodeEnum = AdherenceCodeEnum.getByCode(entry.getValue());
                validDatesMap.put(recordDate, adherenceCodeEnum);
            } catch (ParseException e) {
                throw new ValidationException("one or more dates with invalid format");
            }
        }
        return validDatesMap;
    }

    @Override
    public List<Long> deleteIamIdsByStopDate(List<Long> iamIds, Date stopDate, Boolean deleteAllEntries) {
        List<Registration> allRegistrations = registrationRepository.findAllById(iamIds);
        List<Registration> registrationsToDelete = new ArrayList<>();
        if(!deleteAllEntries) {
            registrationsToDelete = Utils.filter(allRegistrations, r -> r.getStartDate().after(stopDate));
            if (allRegistrations.size() == registrationsToDelete.size())
                throw new ValidationException("stop date cannot be before first start date!");
        } else {
            registrationsToDelete = allRegistrations;
        }
        if (!CollectionUtils.isEmpty(registrationsToDelete)) {
            Map<Long, Set<Long>> adhTechIdToIamIds = registrationsToDelete
                .stream().collect(Collectors
                    .groupingBy(
                        Registration::getAdTechId,
                        Collectors.mapping(Registration::getId, Collectors.toSet())
                    )
                );
            for (Map.Entry<Long, Set<Long>> entry : adhTechIdToIamIds.entrySet()) {
                AdherenceTechHandler handler = adherenceTechnologyHandlerMap.getHandler(entry.getKey());
                handler.deleteRegistration(entry.getValue());
            }
        }
        return registrationsToDelete.stream().map(Registration::getId).collect(Collectors.toList());
    }

    @Override
    public Long createIam(EntityRequest entityRequest) {
        AdherenceTechHandler handler = adherenceTechnologyHandlerMap.getHandler((long)entityRequest.getAdherenceType());
        ScheduleHandler scheduleHandler = scheduleTypeHandlerMap.getHandler(entityRequest.getScheduleTypeId());
        if (null == handler) {
            throw new ValidationException("Invalid handler");
        }
        ScheduleSensitivity sensitivity = new ScheduleSensitivity(entityRequest.getNegativeSensitivity(), entityRequest.getPositiveSensitivity());
        scheduleHandler.validateScheduleAndSensitivityOverLap(new ValidateScheduleDto(entityRequest.getScheduleString(), sensitivity, entityRequest.getDoseDetails()));
        Registration newRegistration = handler.save(entityRequest);
        scheduleHandler.save(newRegistration.getId(), newRegistration.getStartDate(), entityRequest.getScheduleString(), entityRequest.getFirstDoseOffset(), sensitivity, entityRequest.getDoseDetails());
        handler.postRegistration(newRegistration, entityRequest, true);
        return newRegistration.getId();
    }

    public RecordAdherenceResponse updateAdherenceMap(TreeMap<Date, AdherenceCodeEnum> datesToValueMapList, List<Registration> registrationList) {
        List<Long> iamLogIds = new ArrayList<>();
        if(!CollectionUtils.isEmpty(registrationList)) {
            for(Registration registration: registrationList) {
                AdherenceTechHandler handler = adherenceTechnologyHandlerMap.getHandler(registration.getAdTechId());
                iamLogIds = Stream.concat(iamLogIds.stream(),handler.updateAdherenceMap(datesToValueMapList, registration).stream()).collect(Collectors.toList());
            }
        }
        RecordAdherenceResponse response = new RecordAdherenceResponse();
        response.setIamLogIds(iamLogIds);
        return response;
    }

    @Override
    public List<AdherenceResponse> filterList(List<AdherenceResponse> adherenceResponseList, List<RangeFilters> filters) {
        for (RangeFilters f : filters) {
            adherenceResponseList = new ArrayList<>(filterList(adherenceResponseList, f));
        }
        return adherenceResponseList;
    }

    @Override
    public List<AdherenceResponse> filterList(List<AdherenceResponse> adherenceResponseList, RangeFilters filters){
        List<AdherenceResponse> result = adherenceResponseList;
        if (filters.getType() == RangeFilterType.LAST_DOSAGE) {
            // here 'to' & 'from' are dates
            try {
                Date fromDate = filters.getFrom() != null
                        ? Utils.convertStringToDate(filters.getFrom())
                        : null;
                Date tillDate = filters.getTo() != null
                        ? Utils.convertStringToDate(filters.getTo())
                        : null;
                result = new ArrayList<>(
                        Utils.filter(
                                adherenceResponseList,
                                p -> (fromDate == null || (p.getLastDosage() != null && p.getLastDosage().after(fromDate)))
                                        && (tillDate == null || (p.getLastDosage() != null && p.getLastDosage().before(tillDate)))
                        )
                );
            } catch (ParseException e) {
                throw new ValidationException("[filterList] unable to parse fromDate or tillDate");
            }
        }
        if (filters.getType() == RangeFilterType.ADHERENCE_STATISTICS) {
            // here 'to' & 'from' are dates
                result.forEach(ar -> {
                    Date fromDate, tillDate;
                    try {
                        fromDate = filters.getFrom() != null
                                ? Utils.convertStringToDate(filters.getFrom())
                                : ar.getStartDate();
                        tillDate = filters.getTo() != null
                                ? Utils.convertStringToDate(filters.getTo())
                                : ar.getEndDate();
                    } catch (ParseException e) {
                        throw new ValidationException("[filterList] unable to parse fromDate or tillDate");
                    }
                    String adherenceSubstring = getAdherenceSubString(ar.getAdherenceString(), fromDate, tillDate, ar.getStartDate(), ar.getEndDate());
                    if(adherenceSubstring == null) {
                        return;
                    }
                    AdherenceStatistics adherenceStats = new AdherenceStatistics(adherenceSubstring);
                    ar.adherenceStatistics = new AdherenceStatisticsResponse(filters.getFrom(), filters.getTo(), adherenceStats.getTechnologyDoses(), adherenceStats.getManualDoses(), adherenceStats.getTotalDoses());
                });
            }

        if (filters.getType() == RangeFilterType.CONSECUTIVE_MISSED_DOSES) {
            Integer from = filters.getFrom() != null ? Integer.parseInt(filters.getFrom()) : null;
            Integer to = filters.getTo() != null ? Integer.parseInt(filters.getTo()) : null;
            result = Utils.filter(
                    adherenceResponseList,
                    p -> (from == null || from <= p.getConsecutiveMissedDoses())
                            && (to == null || to >= p.getConsecutiveMissedDoses())
            );
        }

        if (filters.getType() == RangeFilterType.CONSECUTIVE_MISSED_DOSES_FROM_YESTERDAY) {
            Integer from = filters.getFrom() != null ? Integer.parseInt(filters.getFrom()) : null;
            Integer to = filters.getTo() != null ? Integer.parseInt(filters.getTo()) : null;
            result = Utils.filter(
                    adherenceResponseList,
                    p -> (from == null || from <= p.getConsecutiveMissedDosesFromYesterday())
                            && (to == null || to >= p.getConsecutiveMissedDosesFromYesterday())
            );
        }
         return result;
    }

    public String getAdherenceSubString(String adherenceString, Date fromDate, Date tillDate, Date startDate, Date endDate) {
        if(endDate == null)
            endDate= getCurrentDate();
        if(startDate.after(tillDate) || fromDate.after(endDate)) {
            return null;
        }
        int length = adherenceString.length();
        Long start = (fromDate.after(startDate))
                ? (long)Math.ceil(getExactDifferenceDays(startDate,fromDate))
                : 0;
        Long end = (endDate.after(tillDate))
                ? (long)Math.floor(getExactDifferenceDays(startDate,tillDate)) + 1
                : length;

       return adherenceString.substring(start.intValue(), end.intValue());
    }

    @Override
    public PositiveAdherenceDto countPositiveCallsForToday(List<String> entityIdList) {
        return countPositiveCallsForToday(entityIdList, false);
    }

    @Override
    public PositiveAdherenceDto countPositiveCallsForToday(List<String> entityIdList, boolean fallback) {
        List<String> cacheEntityNames = entityIdList.stream().map(m -> Constants.CACHE_ADH_STATUS_TODAY_PREFIX + m).collect(Collectors.toList());
        List<String> cacheData = CacheUtils.getFromCache(cacheEntityNames);
        Long manualCount = 0L;
        Long digitalCount = 0L;
        Long missedCount = 0L;
        PositiveAdherenceDto positiveAdherenceDto = new PositiveAdherenceDto(manualCount, digitalCount, missedCount, new ArrayList<>());
        if (!Utils.isEmptyOrAllNull(cacheData)) { //for cache misses redis returns null
            for (String cache : cacheData) {
                if (!StringUtils.isEmpty(cache)) {
                    if (cache.equals(CacheAdherenceStatusToday.MANUAL.getStatus()))
                        manualCount++;
                    else if (cache.equals(CacheAdherenceStatusToday.DIGITAL.getStatus()))
                        digitalCount++;
                    else if (cache.equals(CacheAdherenceStatusToday.MISSED.getStatus()))
                        missedCount++;
                }
            }
            positiveAdherenceDto.setDigitalCount(digitalCount);
            positiveAdherenceDto.setManualCount(manualCount);
            positiveAdherenceDto.setMissedCount(missedCount);
        }
        if (fallback) {
            List<String> nonExistingEntitiesFromCache = CacheUtils.getKeys(cacheEntityNames.toArray(), false, List.class);
            List<String> fallBackIds = new ArrayList<>();
            nonExistingEntitiesFromCache.forEach(ids -> {
                fallBackIds.add(ids.substring(Constants.CACHE_ADH_STATUS_TODAY_PREFIX.length()));
            });
            positiveAdherenceDto.setEntityIdList(fallBackIds);
        }
        return positiveAdherenceDto;
    }

    @Override
    public List<AdherenceResponse> getAdherenceBulk(Map<String, List<AccessMapping>> accessMappingToEntityMap, List<RangeFilters> filters) {
        AdherenceCacheDataDto<AdherenceResponse> adherenceCacheDataDto = getAdherenceFromCache(
                new ArrayList<>(accessMappingToEntityMap.keySet()), CACHE_USER_PREFIX,
                (cacheEntry, allIdsSet) -> formatCacheForAdherence(cacheEntry, allIdsSet));
        List<AdherenceResponse> allEntityResponseList = new ArrayList<>();
        if (adherenceCacheDataDto.getEntityIds().size() > 0) {
            List<CacheMultiSetWithTimeDto> cacheMultiSetWithTimeDtoList = new ArrayList<>();
            List<Long> faultyIamIds = new ArrayList<>();
            for(String entityId : adherenceCacheDataDto.getEntityIds()) {
                List<Long> mappedIamList = accessMappingToEntityMap.get(entityId).stream().map(AccessMapping::getIamId).collect(Collectors.toList());
                try {
                    AdherenceResponse response = getAllAdherence(mappedIamList, false); //we never want logs when doing bulk fetch
                    if(null != entityId) {
                        response.setEntityId(entityId);
                    }
                    allEntityResponseList.add(response);
                    cacheMultiSetWithTimeDtoList.add(new CacheMultiSetWithTimeDto(CACHE_USER_PREFIX + entityId, Utils.asJsonString(response), Utils.timeToEntityEndDate(response.getAdherenceData().get(0).getStartDate(), TimeUnit.SECONDS)));
                    EntityDoseCountDto entityDoseCount = generateEntityDoseCountStats(new EntityAdherenceDto(entityId, response.getAdherenceString(), response.getStartDate(), response.getEndDate()));
                    cacheMultiSetWithTimeDtoList.add(new CacheMultiSetWithTimeDto(Constants.CACHE_AVG_ADHERENCE_PREFIX + entityId, Utils.asJsonString(entityDoseCount),Utils.timeToEntityEndDate(response.getAdherenceData().get(0).getStartDate(), TimeUnit.SECONDS)));
                }
                catch (Exception e) {
                    LOGGER.error("[getAdherenceBulk] Fetch Adherence failed for entityId "+entityId);
                    faultyIamIds.addAll(mappedIamList);
                }
            }
            if(!CollectionUtils.isEmpty(faultyIamIds)){
                List<Registration> registrationList = registrationRepository.findByIdIn(faultyIamIds);
                Map<Long, List<Registration>> adherenceCodeRegistrationMap = registrationList.stream().collect(groupingBy(Registration::getAdTechId));
                adherenceCodeRegistrationMap.forEach((adhTechId, registrations) -> {
                    AdherenceTechHandler handler = adherenceTechnologyHandlerMap.getHandler(adhTechId);
                    handler.cacheNullDateEntity(registrations);
                });
            }
            if (!CollectionUtils.isEmpty(cacheMultiSetWithTimeDtoList))
                CacheUtils.bulkInsertWithTtl(cacheMultiSetWithTimeDtoList);
        }
        allEntityResponseList.addAll(adherenceCacheDataDto.getAdherenceResponse());
        if (filters != null) {
            allEntityResponseList = new ArrayList<>(filterList(allEntityResponseList, filters));
        }
        return allEntityResponseList;
    }

    @Override
    public String mergeEntityAdherence(List<AdherenceResponse> adherenceResponseList) {
        String mergedAdherence = null;
        if (!CollectionUtils.isEmpty(adherenceResponseList)) {
            Map<Integer, Character> dayToDoseStatus = new HashMap<>();
            Date minStartDate = findMinDateInAdherenceResponse(adherenceResponseList);
            for (AdherenceResponse adherenceResponse : adherenceResponseList) {
                Date endDate = adherenceResponse.getEndDate() == null ? Utils.getCurrentDate() : adherenceResponse.getEndDate();
                Calendar start = Calendar.getInstance();
                start.setTime(adherenceResponse.getStartDate());
                Calendar end = Calendar.getInstance();
                end.setTime(endDate);
                int adhIdx = 0;
                for (Date date = start.getTime(); start.before(end); start.add(Calendar.DATE, 1), date = start.getTime()) {
                    long minutes = TimeUnit.MINUTES.convert(date.getTime() - minStartDate.getTime(), TimeUnit.MILLISECONDS);
                    int day = (int) Math.floor((minutes / 60f) / 24f);
                    char currentAdhCode = adherenceResponse.getAdherenceString().charAt(adhIdx++);
                    if (dayToDoseStatus.containsKey(day)) {
                        char oldCode = dayToDoseStatus.get(day);
                        currentAdhCode = AdherenceCodeEnum.mergeCodes(
                                AdherenceCodeEnum.getByCode(currentAdhCode),
                                AdherenceCodeEnum.getByCode(oldCode)
                        ).getCode();
                    }
                    dayToDoseStatus.put(day, currentAdhCode);
                }
            }
            mergedAdherence = computeMergedAdherenceString(minStartDate, Utils.getCurrentDate(), dayToDoseStatus).toString();
        }
        return mergedAdherence;
    }

    Date findMinDateInAdherenceResponse(List<AdherenceResponse> adherenceResponseList) {
        Date minStartDate = adherenceResponseList.get(0).getStartDate();
        for (AdherenceResponse adherenceResponse : adherenceResponseList) {
            if (minStartDate.after(adherenceResponse.getStartDate()))
                minStartDate = adherenceResponse.getStartDate();
        }
        return minStartDate;
    }

    StringBuilder computeMergedAdherenceString(Date startDate, Date endDate, Map<Integer, Character> dayToDoseStatus) {
        StringBuilder mergedAdherenceString = new StringBuilder();
        Calendar start = Calendar.getInstance();
        start.setTime(startDate);
        Calendar end = Calendar.getInstance();
        end.setTime(endDate);
        for (Date date = start.getTime(); start.before(end); start.add(Calendar.DATE, 1), date = start.getTime()) {
            long minutes = TimeUnit.MINUTES.convert(date.getTime() - startDate.getTime(), TimeUnit.MILLISECONDS);
            int day = (int) Math.floor((minutes / 60f) / 24f);
            if (dayToDoseStatus.containsKey(day)) {
                mergedAdherenceString.append(dayToDoseStatus.get(day));
            } else {
                mergedAdherenceString.append(AdherenceCodeEnum.EMPTY_SCHEDULE.getCode());
            }
        }
        return mergedAdherenceString;
    }



    @Override
    public EntityAdherenceDto getEntityAdherence(String entityId, List<RegistrationShortRepo> registrationList) {
        List<AdherenceData> adherenceDataList =
                registrationList.stream()
                        .map(r -> new AdherenceData(r.getIamId(), r.getAdherenceString(), r.getStartDate(), r.getEndDate(), r.getCreatedDate(), r.getScheduleType()))
                        .collect(Collectors.toList());
        String patientAdherenceString = getEntityAdherenceString(adherenceDataList);

        if(patientAdherenceString == null)
        {
            return null;
        }

        Date startDate = null;
        for (AdherenceData adherenceData : adherenceDataList) {
            if (startDate == null) {
                if (adherenceData.getStartDate() != null)
                    startDate = adherenceData.getStartDate();
            } else if (adherenceData.getStartDate() != null && startDate.after(adherenceData.getStartDate()))
                startDate = adherenceData.getStartDate();
        }
        Date endDate = startDate;
        for (AdherenceData adherenceData : adherenceDataList) {
            if (adherenceData.getEndDate() == null) {
                endDate = null;
            } else if (endDate != null && endDate.before(adherenceData.getEndDate()))
                endDate = adherenceData.getEndDate();
        }
        return new EntityAdherenceDto(entityId, patientAdherenceString, startDate, endDate);
    }

    @Override
    public EntityDoseCountDto generateEntityDoseCountStats(EntityAdherenceDto entityAdherence){
        EntityDoseCountDto entityDoseCountDto = new EntityDoseCountDto(entityAdherence.getEntityId());
        for (AverageAdherenceIntervals value : AverageAdherenceIntervals.values()) {
            entityDoseCountDto.getAvgAdherenceIntervals().put(value, value.getIntervalDoseCounts(entityAdherence));
        }
        return entityDoseCountDto;
    }

    @Override
    public void updateCumulativeDoseCountStats(CumulativeDoseCountDto cumulativeDoseCounts ,CumulativeDoseCountDto entityDoseCounts) {
        for (AverageAdherenceIntervals value : AverageAdherenceIntervals.values()) {
            IntervalAdherenceDto cumulativeIntervalStats = cumulativeDoseCounts.getAvgAdherenceIntervals().get(value);
            IntervalAdherenceDto entityIntervalStats = entityDoseCounts.getAvgAdherenceIntervals().get(value);
            cumulativeIntervalStats.setDigitalDoses(cumulativeIntervalStats.getDigitalDoses() + entityIntervalStats.getDigitalDoses());
            cumulativeIntervalStats.setManualDoses(cumulativeIntervalStats.getManualDoses() + entityIntervalStats.getManualDoses());
            cumulativeIntervalStats.setTotalDoses(cumulativeIntervalStats.getTotalDoses() + entityIntervalStats.getTotalDoses());
        }
    }

    public AvgAdherenceResponse generateAverageAdherenceResponse(EntityDoseCountDto doseCounts){
        AvgAdherenceResponse avgAdherenceResponse = new AvgAdherenceResponse();
        avgAdherenceResponse.setEntityId(doseCounts.getEntityId());

        IntervalAdherenceDto allTime = doseCounts.getAvgAdherenceIntervals().get(AverageAdherenceIntervals.ALL_TIME);
        long entityTotalDays = allTime.getTotalDoses();
        entityTotalDays =  entityTotalDays > 0 ? entityTotalDays : 1;
        avgAdherenceResponse.setDigitalAdherence(allTime.getDigitalDoses()*100.0/entityTotalDays);
        avgAdherenceResponse.setTotalAdherence((allTime.getDigitalDoses() + allTime.getManualDoses())*100.0/entityTotalDays);
        avgAdherenceResponse.setAverageAdherenceIntervals(generateAvgAdherencePercentIntervalMap(doseCounts.getAvgAdherenceIntervals()));
        return avgAdherenceResponse;
    }

    public AllAvgAdherenceResponse generateAllAverageAdherenceResponse(CumulativeDoseCountDto doseCounts){
        AllAvgAdherenceResponse avgAdherenceResponse = new AllAvgAdherenceResponse();

        IntervalAdherenceDto allTime = doseCounts.getAvgAdherenceIntervals().get(AverageAdherenceIntervals.ALL_TIME);
        long entityTotalDays = allTime.getTotalDoses();
        entityTotalDays =  entityTotalDays > 0 ? entityTotalDays : 1;
        avgAdherenceResponse.setDigitalAdherence(allTime.getDigitalDoses()*100.0/entityTotalDays);
        avgAdherenceResponse.setTotalAdherence((allTime.getDigitalDoses() + allTime.getManualDoses())*100.0/entityTotalDays);
        avgAdherenceResponse.setAverageAdherenceIntervals(generateAvgAdherencePercentIntervalMap(doseCounts.getAvgAdherenceIntervals()));
        return avgAdherenceResponse;
    }

    private Map<AverageAdherenceIntervals, IntervalAdherencePercentDto> generateAvgAdherencePercentIntervalMap(Map<AverageAdherenceIntervals, IntervalAdherenceDto> allIntervalCounts){
        Map<AverageAdherenceIntervals, IntervalAdherencePercentDto> intervalAdherencePercentMap = new HashMap<>();
        for (Map.Entry<AverageAdherenceIntervals,IntervalAdherenceDto> entry : allIntervalCounts.entrySet()) {
            IntervalAdherencePercentDto intervalPercent = new IntervalAdherencePercentDto();
            AverageAdherenceIntervals interval = entry.getKey();

            IntervalAdherenceDto intervalCount = entry.getValue();
            long entityTotalDays = intervalCount.getTotalDoses();
            entityTotalDays =  entityTotalDays > 0 ? entityTotalDays : 1;
            intervalPercent.setDigitalAdherence(intervalCount.getDigitalDoses()*100.0/entityTotalDays);
            intervalPercent.setTotalAdherence((intervalCount.getDigitalDoses() + intervalCount.getManualDoses())*100.0/entityTotalDays);

            intervalAdherencePercentMap.put(interval, intervalPercent);
        }
        return intervalAdherencePercentMap;
    }

    @Override
    public CumulativeDoseCountDto calculateAdherenceDays(List<EntityAdherenceDto> entityAdherenceList, List<AvgAdherenceResponse> avgAdherenceResponseList){
        CumulativeDoseCountDto cumulativeDoseCount = new CumulativeDoseCountDto();
        List<CacheMultiSetWithTimeDto> cacheMultiSetWithTimeDtoList = new ArrayList<>();
        for (EntityAdherenceDto entityAdherence : entityAdherenceList) {
            EntityDoseCountDto entityDoseCount = generateEntityDoseCountStats(entityAdherence);
            updateCumulativeDoseCountStats(cumulativeDoseCount, entityDoseCount);
            CacheMultiSetWithTimeDto cacheAverageDto = new CacheMultiSetWithTimeDto(
                    Constants.CACHE_AVG_ADHERENCE_PREFIX + entityAdherence.getEntityId(),
                    Utils.asJsonString(entityDoseCount),
                    Utils.timeToEntityEndDate(entityAdherence.getStartDate(), TimeUnit.SECONDS));
            cacheMultiSetWithTimeDtoList.add(cacheAverageDto);
            if (avgAdherenceResponseList != null){
                avgAdherenceResponseList.add(generateAverageAdherenceResponse(entityDoseCount));
            }
        }
        if (!CollectionUtils.isEmpty(cacheMultiSetWithTimeDtoList))
            CacheUtils.bulkInsertWithTtl(cacheMultiSetWithTimeDtoList);

        return cumulativeDoseCount;
    }

    @Override
    public AllAvgAdherenceResponse getAverageAdherence(Map<String, List<AccessMapping>> accessMappingToEntityMap, boolean patientDetailsRequired, List<AverageAdherenceIntervals> intervals) {
        CumulativeDoseCountDto cumulativeDoseCount = new CumulativeDoseCountDto();
        List<EntityAdherenceDto> entityAdherenceList;
        List<AvgAdherenceResponse> entityAverageAdherenceList = new ArrayList<>();
        AdherenceCacheDataDto<EntityDoseCountDto> entityDoseCountFromCache = getAdherenceFromCache(
                new ArrayList<>(accessMappingToEntityMap.keySet()), Constants.CACHE_AVG_ADHERENCE_PREFIX,
                this::formatCacheForAvgAdherence);
        for (EntityDoseCountDto doses : entityDoseCountFromCache.getAdherenceResponse()){
            updateCumulativeDoseCountStats(cumulativeDoseCount, doses);
            if (patientDetailsRequired){
                entityAverageAdherenceList.add(generateAverageAdherenceResponse(doses));
            }
        }

        if (entityDoseCountFromCache.getEntityIds().size() > 0) {
            entityAdherenceList = new ArrayList<>();
            List<Long> allMappedIamList = new ArrayList<>();
            for (String entityId : entityDoseCountFromCache.getEntityIds()) {
                List<Long> mappedIamList = accessMappingToEntityMap.get(entityId).stream().map(AccessMapping::getIamId).collect(Collectors.toList());
                allMappedIamList.addAll(mappedIamList);
            }
            Map<Long, RegistrationShortRepo> entityIdToRegistrationMap = registrationRepository.getAdherenceDetailsFromIamId(allMappedIamList).stream()
                    .collect(Collectors.toMap(RegistrationShortRepo::getIamId, Function.identity()));
            for (String entityId : entityDoseCountFromCache.getEntityIds()) {
                List<RegistrationShortRepo> registrationList = accessMappingToEntityMap.get(entityId).stream()
                        .map(m -> entityIdToRegistrationMap.get(m.getIamId())).collect(Collectors.toList());
                boolean isAnyRegistrationWithSchedule = registrationList.stream().anyMatch(reg -> reg.getScheduleType() != ScheduleTypeEnum.DAILY.getId().longValue());
                if(isAnyRegistrationWithSchedule) {
                    List<Long> mappedIamList = accessMappingToEntityMap.get(entityId).stream().map(AccessMapping::getIamId).collect(Collectors.toList());
                    AdherenceResponse adh = getAllAdherence(mappedIamList, false);
                    entityAdherenceList.add(new EntityAdherenceDto(entityId, adh.getAdherenceString(), adh.getStartDate(), adh.getEndDate()));
                }
                else
                {
                    EntityAdherenceDto entityAdherenceDto = getEntityAdherence(entityId, registrationList);
                    if(null != entityAdherenceDto)
                    {
                        entityAdherenceList.add(entityAdherenceDto);
                    }
                }
            }
            CumulativeDoseCountDto adherenceStats = calculateAdherenceDays(entityAdherenceList, patientDetailsRequired ? entityAverageAdherenceList : null);
            updateCumulativeDoseCountStats(cumulativeDoseCount, adherenceStats);
        }

        entityAverageAdherenceList.forEach(x -> x.filterIntervals(intervals));
        AllAvgAdherenceResponse response = generateAllAverageAdherenceResponse(cumulativeDoseCount);
        response.filterIntervals(intervals);
        response.setEntityAverageAdherenceList(entityAverageAdherenceList);

        return response;
    }

    @Override
    public <T> AdherenceCacheDataDto<T> getAdherenceFromCache(List<String> entityIdList, String prefix, BiFunction<String, Set<String>, T> func) {
        List<String> ids = new ArrayList<>();
        Set<String> allIdsSet = new HashSet<>();
        List<T> responses = new ArrayList<>();
        for (String s : entityIdList) {
            ids.add(prefix + s);
            allIdsSet.add(s);
        }
        List<String> cacheData = null;
        try{
            cacheData = CacheUtils.getFromCache(ids);
        }
        catch (Exception e) {
            LOGGER.error("[getAdherenceFromCache] Cache Read Failed with exception "+ e.toString());
        }
        if (cacheData != null) {
            for (String cache : cacheData) {
                if (!StringUtils.isEmpty(cache)) {
                    responses.add(func.apply(cache, allIdsSet));
                }
            }
        }
        return new AdherenceCacheDataDto<>(responses, new ArrayList<>(allIdsSet));
    }

    @Override
    public AdherenceResponse formatCacheForAdherence(String cacheEntry, Set<String> allIdsSet) {
        AdherenceResponse cacheResponse = new AdherenceResponse();
        try {
            cacheResponse = Utils.convertStringToObject(cacheEntry, AdherenceResponse.class);
            allIdsSet.remove(cacheResponse.entityId);
        } catch (IOException e) {
            LOGGER.error("[formatCacheForAdherence] could not parse adherence response");
        }
        return cacheResponse;
    }

    @Override
    public EntityDoseCountDto formatCacheForAvgAdherence(String cacheEntry, Set<String> allIdsSet) {
        EntityDoseCountDto cacheResponse = new EntityDoseCountDto();
        try {
            cacheResponse = Utils.convertStringToObject(cacheEntry, EntityDoseCountDto.class);
            allIdsSet.remove(cacheResponse.entityId);
        } catch (IOException e) {
            LOGGER.error("[formatCacheForAvgAdherence] could not parse adherence response");
        }
        return cacheResponse;
    }

    @Override
    public AdherenceResponse getAllAdherence(List<Long> iamIdList, Boolean logsRequired) {
        AdherenceResponse response = new AdherenceResponse();
        AdherenceStatistics adherenceStats;
        List<AdherenceData> adherenceDataList = new ArrayList<AdherenceData>();
        List<Registration> registrationList = registrationRepository.findByIdIn(iamIdList);
        for (Registration registration : registrationList) {
            AdherenceResponse iamResponse = getAdherence(registration, logsRequired);
            adherenceDataList.add(iamResponse.getAdherenceData().get(0));
        }
        response.setAdherenceData(adherenceDataList);
        String adhString = getEntityAdherenceString(adherenceDataList);
        response.setAdherenceString(adhString);

        Date maxDate = null;
        Date startDate = null;
        Date lastMissedDosage = null;
        for (AdherenceData adherenceData : adherenceDataList) {
            if (maxDate == null) {
                if (adherenceData.getLastDosage() != null) {
                    maxDate = adherenceData.getLastDosage();
                }
            } else if (adherenceData.getLastDosage() != null && maxDate.before(adherenceData.getLastDosage()))
            {
                maxDate = adherenceData.getLastDosage();
            }
            if (startDate == null) {
                if (adherenceData.getStartDate() != null) {
                    startDate = adherenceData.getStartDate();
                }
            } else if (adherenceData.getStartDate() != null && startDate.after(adherenceData.getStartDate())) {
                startDate = adherenceData.getStartDate();
            }
            if (null == lastMissedDosage) {
                lastMissedDosage = adherenceData.getLastMissedDosage();
            } else if (adherenceData.getLastMissedDosage() != null  && lastMissedDosage.before(adherenceData.getLastMissedDosage())) {
                lastMissedDosage = adherenceData.getLastMissedDosage();
            }
        }
        response.setLastDosage(maxDate);
        response.setStartDate(startDate);
        response.setLastMissedDosage(lastMissedDosage);
        Date endDate = startDate;
        Long adhTechId = null;
        for (AdherenceData adherenceData : adherenceDataList) {
            if (adherenceData.getEndDate() == null) {
                endDate = null;
                adhTechId = adherenceData.getAdherenceType();
            } else if (endDate != null && endDate.before(adherenceData.getEndDate())) {
                endDate = adherenceData.getEndDate();
                adhTechId = adherenceData.getAdherenceType();
            }
        }
        response.setEndDate(endDate);
        if (null != adhTechId) {
            response.setMonitoringMethod(AdherenceTech.getMatch(adhTechId).getTechKey());
        }
        adherenceStats = new AdherenceStatistics(response.getAdherenceString());
        response.setTechnologyDoses(adherenceStats.getTechnologyDoses());
        response.setManualDoses(adherenceStats.getManualDoses());
        response.setTotalDoses(adherenceStats.getTotalDoses());
        response.setMissedDoses(adherenceStats.getMissedDoses());
        response.setNoInfoDoses(adherenceStats.getNoInfoDoses());

        adherenceDataList.sort((AdherenceData a, AdherenceData b) -> a.getStartDate().before(b.getStartDate()) ? -1 : 1);
        Date entityStartDate = adherenceDataList.get(0).getStartDate();
        AdherenceData latestIAMData = adherenceDataList.get(adherenceDataList.size() - 1);
        AdherenceTechHandler handler = adherenceTechnologyHandlerMap.getHandler(latestIAMData.getAdherenceType());
        response.setConsecutiveMissedDoses(handler.getEntityConsecutiveMissedDoses(adhString, entityStartDate, maxDate, latestIAMData,true));
        response.setConsecutiveMissedDosesFromYesterday(handler.getEntityConsecutiveMissedDoses(adhString, entityStartDate, maxDate, latestIAMData, false));
        response.setDoseTimeList(getDoseTimeForEntity(latestIAMData.getIamId(), latestIAMData.getScheduleType()));
        return response;
    }

    @Override
    public boolean updateIam(EntityRequest entityRequest, Long iamId) {
        Registration iamRegistration = registrationRepository.findById(iamId).orElse(null);
        Date newStartDate = Utils.getCurrentDate();
        if (null == iamRegistration) {
            throw new NotFoundException("active entity mapping not found");
        }
        ScheduleHandler currentScheduleHandler = scheduleTypeHandlerMap.getHandler(iamRegistration.getScheduleTypeId());
        if (!CollectionUtils.isEmpty(entityRequest.getDoseDetails()) && !isDoseTimeSupportedForScheduleType(currentScheduleHandler.getScheduleTypeId())) {
            throw new ValidationException("Dose timings not supported for the schedule");
        }
        if (CollectionUtils.isEmpty(entityRequest.getDoseDetails()) && isDoseTimeSupportedForScheduleType(currentScheduleHandler.getScheduleTypeId())) {
            throw new ValidationException("Dose timings is required for the schedule");
        }
        if(!StringUtils.isEmpty(entityRequest.getStartDate())) {
            try {
                newStartDate = Utils.convertStringToDate(entityRequest.getStartDate());
            } catch (ParseException e) {
                LOGGER.error("[updateIam] error when parsing date");
                throw new InvalidParameterException("Unable to parse start date");
            }
            ScheduleSensitivity sensitivity = new ScheduleSensitivity(entityRequest.getNegativeSensitivity(), entityRequest.getPositiveSensitivity());
            if(currentScheduleHandler.hasScheduleMapping() && !StringUtils.isEmpty(entityRequest.getScheduleString())) {
                currentScheduleHandler.validateScheduleAndSensitivityOverLap(new ValidateScheduleDto(entityRequest.getScheduleString(), sensitivity, new ArrayList<>()));
                ScheduleMap activeScheduleMap = currentScheduleHandler.getActiveScheduleMapForIam(iamId);
                Schedule schedule = currentScheduleHandler.findByScheduleValue(currentScheduleHandler.getScheduleValue(entityRequest.getScheduleString()));
                if(null != activeScheduleMap && !activeScheduleMap.getScheduleId().equals(schedule.getId())) {
                    currentScheduleHandler.stopSchedule(iamRegistration.getId(), newStartDate);
                    currentScheduleHandler.save(iamRegistration.getId(), newStartDate, entityRequest.getScheduleString(), entityRequest.getFirstDoseOffset(), sensitivity);
                }
            } else if (currentScheduleHandler.hasScheduleMapping() && currentScheduleHandler.isDoseTimeSupported()) {
                getAdherence(iamRegistration, false); // check if adherence needs to be updated
                // newStartDate = dayStartTime for multi-freq since start date cannot be updated for multi-freq, so re-used that
                updateMultiDoseSchedule(entityRequest.getDoseDetails(), iamRegistration, currentScheduleHandler, newStartDate);
            }  else {
                iamRegistration.setStartDate(newStartDate);
                currentScheduleHandler.updateStartDate(iamId, newStartDate, entityRequest.getFirstDoseOffset());
            }
        }
        if (CollectionUtils.isEmpty(entityRequest.getDoseDetails())) {
            AdherenceTechHandler handler = adherenceTechnologyHandlerMap.getHandler(iamRegistration.getAdTechId());
            String adherenceString = handler.generateAdherenceString(iamId, iamRegistration.getStartDate(), iamRegistration.getEndDate(), iamRegistration.getScheduleTypeId());
            iamRegistration.setAdherenceString(adherenceString);
            registrationRepository.save(iamRegistration);
        }
        return true;
    }

    public void updateMultiDoseSchedule(List<EntityRequest.DoseTimeInfo> newDoseTimes, Registration registration, ScheduleHandler currentScheduleHandler, Date dayStartTime) {
        if (Utils.getDifferenceDays(dayStartTime, Utils.getCurrentDate()) > 1) {
            throw new ValidationException("Start date should be day start time today!");
        }
        StopScheduleExtraInfoDto stopScheduleExtraInfoDto = currentScheduleHandler.updateMultiDoseSchedule(newDoseTimes, registration, dayStartTime);
        if (stopScheduleExtraInfoDto != null) {
            registration.setAdherenceString(registration.getAdherenceString().substring(0, registration.getAdherenceString().length() - 1) + stopScheduleExtraInfoDto.getAdherenceCode());
            if (stopScheduleExtraInfoDto.isScheduleDeleted()) {
                AdherenceTechHandler handler = adherenceTechnologyHandlerMap.getHandler(registration.getAdTechId());
                Date lastDosage = null;
                if (registration.getAdherenceString().length() > 0)
                    lastDosage = handler.computeLastDosage(registration.getAdherenceString(), registration.getEndDate(), false);
                registration.setLastDosage(lastDosage);
                handler.publishLastDosageEventOnRabbitMQ(null, registrationRepository.getOne(registration.getId()), DosageTypeEnum.LAST_DOSAGE.getName());
            }
            registrationRepository.save(registration);
        }
    }

    @Override
    public EntityResponse updateUniqueIdentifier(String uniqueIdentifier, Long iamId) {
        Registration registration = registrationRepository.getOne(iamId);
        registration.setUniqueIdentifier(uniqueIdentifier);
        registration.setUpdatedDate(Utils.getCurrentDate());
        registrationRepository.save(registration);
        return new EntityResponse(iamId);
    }

    @Override
    public Registration updateEndDate(Registration registration, Date endDate) {
        registration.setEndDate(endDate);
        registrationRepository.save(registration);
        return registration;
    }

    @Override
    public List<Registration> getRegistrationsByIamId(List<Long> iamIds) {
        return registrationRepository.findByIdIn(iamIds);
    }

    @Override
    public Registration getLastClosedRegistration(List<Long> iamIds) {
        List<Registration> registrationList = getRegistrationsByIamId(iamIds);
        Registration lastClosedRegistration = null;
        if(!CollectionUtils.isEmpty(registrationList)) {
            lastClosedRegistration = registrationList.get(0);
            if (registrationList.size() > 1) {

                registrationList.sort(Comparator.comparing(Registration::getEndDate).thenComparing(Registration::getCreatedDate).thenComparing(Registration::getStartDate).reversed());
                lastClosedRegistration = registrationList.get(0);
            }
        } else {
            throw new NotFoundException("no registrations found");
        }
        return lastClosedRegistration;
    }

    @Override
    public Registration reopenEntity(EntityRequest entityRequest, List<Long> iamIds) {
        Registration lastClosedRegistration = getLastClosedRegistration(iamIds);
        updateEndDate(lastClosedRegistration, null);
        AdherenceTechHandler handler = adherenceTechnologyHandlerMap.getHandler(lastClosedRegistration.getAdTechId());
        ScheduleHandler scheduleHandler = scheduleTypeHandlerMap.getHandler(lastClosedRegistration.getScheduleTypeId());
        scheduleHandler.reopenSchedule(lastClosedRegistration);
        handler.postRegistration(lastClosedRegistration, entityRequest, false);
        return lastClosedRegistration;
    }

    @Override
    public List<Long> getIamIdsByAdherenceTech(Long adherenceType, Long size) {
        return registrationRepository
                .getByAdTechIdCustom(adherenceType, size)
                .stream()
                .map(BigInteger::longValue)
                .collect(Collectors.toList());
    }

    @Override
    public AdherenceTechHandler getAdherenceTechHandler(Registration registration) {
        return adherenceTechnologyHandlerMap.getHandler(registration.getAdTechId());
    }

    @Override
    public void validateRegistrationsForDate(List<Registration> registrations, String date) {
        try {
            Date startDate = Utils.convertStringToDate(date);
            if (registrations.stream().anyMatch(f -> f.getEndDate() == null || startDate.before(f.getEndDate()))) {
                throw new ConflictException("Active entity already getKeys");
            }
        } catch (ParseException e) {
            throw new ValidationException("date with invalid format");
        }
    }

    @Override
    public void regenerateAdherenceString(List<Long> iamIds) {
        List<Registration> registrationList = registrationRepository.findByIdIn(iamIds);
        Map<Long, List<Registration>> adherenceCodeRegistrationMap = registrationList.stream().collect(groupingBy(Registration::getAdTechId));
        adherenceCodeRegistrationMap.forEach((adhTechId, registrations) -> {
            AdherenceTechHandler handler = adherenceTechnologyHandlerMap.getHandler(adhTechId);
            Set<Long> iamSet = registrations.stream().map(Registration::getId).collect(Collectors.toSet());
            handler.regenerateAdherence(iamSet);
        });
    }

    public String getEntityAdherenceString(List<AdherenceData> adherentDataList) {
        adherentDataList.sort(Comparator.comparing(AdherenceData::getStartDate).thenComparing(AdherenceData::getCreatedDate));
        boolean nullFlag = false;
        AdherenceData previous =  adherentDataList.get(0);
        String adherenceString = previous.getAdherenceString();
        Date treatmentStartDate = previous.getStartDate();


        // set nullFlag if either treatmentStartDate or adherenceString is null
        if(null == treatmentStartDate || null == adherenceString) nullFlag = true;

        // No case of merging if there is only 1 adherentData or have a null date
        if (adherentDataList.size() > 1 && !nullFlag) {
            List<AdherenceData> toProcessDataList = adherentDataList.subList(1, adherentDataList.size());
            for (AdherenceData data : toProcessDataList) {
                Date prevDate = previous.getEndDate();
                if(null == prevDate || data.getStartDate() == null) {
                    nullFlag = true;
                    break;
                }

                if (Utils.checkIfSameDayByTime(treatmentStartDate, prevDate, data.getStartDate())) {
                    //Compare both the method's adherence codes
                    if (!StringUtils.isEmpty(adherenceString) && !StringUtils.isEmpty(data.getAdherenceString())) {
                        AdherenceCodeEnum codeEnum = AdherenceCodeEnum.getByCode(adherenceString.toCharArray()[adherenceString.length() - 1]);
                        AdherenceCodeEnum newCodeEnum = AdherenceCodeEnum.getByCode(data.getAdherenceString().toCharArray()[0]);

                        AdherenceCodeEnum mergedCodeEnum = AdherenceCodeEnum.mergeCodesByPriority(codeEnum, newCodeEnum);

                        adherenceString = adherenceString.substring(0, adherenceString.length() == 0 ? 0 : adherenceString.length() - 1) + mergedCodeEnum.getCode() + data.getAdherenceString().substring(1);
                    } else if (null != data.getAdherenceString())
                        adherenceString += data.getAdherenceString();
                } else if (null != prevDate){
                    adherenceString += data.getAdherenceString();
                }
                previous = data;
            }
        }

        // if any entity id is faulty, save it to cache for re-porting
        if(nullFlag) {
            Registration registration = registrationRepository.getOne(previous.getIamId());
            AdherenceTechHandler handler = adherenceTechnologyHandlerMap.getHandler(registration.getAdTechId());
            handler.cacheNullDateEntity(registration);
        }

        return adherenceString;
    }

    @Override
    public boolean activeScheduleMappingExistsForEntityIdAndClientId(Long clientId, String entityId, Long scheduleTypeId) {
        ScheduleHandler handler = scheduleTypeHandlerMap.getHandler(scheduleTypeId);
        return !CollectionUtils.isEmpty(handler.findAllIamWithActiveSchedule(clientId, entityId));
    }

    public List<Long> getIamIdsByAdherenceTechAndUniqueIdentifier(String uniqueIdentifier, Long adhTechId)
    {
        AdherenceTechHandler handler = adherenceTechnologyHandlerMap.getHandler(adhTechId);
        return handler.getIamIdsByAdherenceTechAndUniqueIdentifier(uniqueIdentifier);
    }

    @Override
    public boolean hasScheduleMappings(Long scheduleTypeId) {
        return scheduleTypeHandlerMap.getHandler(scheduleTypeId).hasScheduleMapping();
    }

    @Override
    public List<String> getAllConfigNames()
    {
        return Arrays.stream(AdherenceConfig.values())
                .map(Enum::toString)
                .collect(Collectors.toList());
    }

    @Override
    public List<AdherenceCodeConfigResponse> getAdherenceColorCodeConfig() {
        List<AdherenceCodeConfigResponse> adherenceCodeConfigResponseList = new ArrayList<>();
        Arrays.stream(AdherenceCodeEnum.values())
                .forEach(m -> adherenceCodeConfigResponseList.add(
                                new AdherenceCodeConfigResponse(m.getCode(), m.name(), m.getDesign(), m.getDescription())
                        )
                );
        return adherenceCodeConfigResponseList;
    }

    @Override
    public List<GenericConfigResponse> getAdherenceTechConfig()
    {
        List<GenericConfigResponse> adherenceTechConfigResponseList = new ArrayList<>();
        Arrays.stream(AdherenceTech.values())
                .forEach(code -> adherenceTechConfigResponseList.add(
                                new GenericConfigResponse(code.getName(), code.getId())
                        )
                );
        return adherenceTechConfigResponseList;
    }

    @Override
    public List<GenericConfigResponse> getScheduleTypeConfig()
    {
        List<GenericConfigResponse> scheduleTypeConfigResponseList = new ArrayList<>();
        Arrays.stream(ScheduleTypeEnum.values())
                .forEach(code -> scheduleTypeConfigResponseList.add(
                                new GenericConfigResponse(code.getName(), code.getId())
                        )
                );
        return scheduleTypeConfigResponseList;
    }

    @Override
    public AdherenceGlobalConfigResponse getAdherenceGlobalConfig(AdherenceGlobalConfigRequest configRequest) {
        AdherenceGlobalConfigResponse globalConfigResponse = new AdherenceGlobalConfigResponse();
        boolean isConfigNameListEmpty =  CollectionUtils.isEmpty(configRequest.getConfigNameList());
        if (isConfigNameListEmpty || configRequest.getConfigNameList().contains(AdherenceConfig.COLOR_CODE.toString())) {
           globalConfigResponse.setAdherenceCodeConfigResponseList(getAdherenceColorCodeConfig());
        }
        if (isConfigNameListEmpty || configRequest.getConfigNameList().contains(AdherenceConfig.ADHERENCE_TECH.toString())) {
            globalConfigResponse.setAdherenceTechConfigResponseList(getAdherenceTechConfig());
        }
        if (isConfigNameListEmpty || configRequest.getConfigNameList().contains(AdherenceConfig.SCHEDULE_TYPE.toString()))
        {
            globalConfigResponse.setScheduleTypeConfigResponseList(getScheduleTypeConfig());
        }
        return globalConfigResponse;
    }

    @Override
    public boolean isDoseTimeSupportedForScheduleType(Long scheduleTypeId) {
        return scheduleTypeHandlerMap.getHandler(scheduleTypeId).isDoseTimeSupported();
    }

    @Override
    public List<DoseTimeData> getDoseTimeForEntity(Long iamId, Long scheduleTypeId) {
        return scheduleTypeHandlerMap.getHandler(scheduleTypeId).getDoseTimes(iamId);
    }

}