create index if not exists iam_access_mapping_iam_id_client_id_active_idx
    on iam_access_mapping (iam_id, client_id, active);