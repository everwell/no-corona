INSERT INTO public.iam_adhtech (id, created_date, name) VALUES (1, '2019-03-20', '99 Dots') ON CONFLICT DO NOTHING;
INSERT INTO public.iam_adhtech (id, created_date, name) VALUES (2, '2019-03-20', 'Video Dots') ON CONFLICT DO NOTHING;
INSERT INTO public.iam_adhtech (id, created_date, name) VALUES (3, '2019-03-20', 'MERM Technology') ON CONFLICT DO NOTHING;
INSERT INTO public.iam_adhtech (id, created_date, name) VALUES (4, '2019-03-23', 'Operation Asha') ON CONFLICT DO NOTHING;
INSERT INTO public.iam_adhtech (id, created_date, name) VALUES (5, '2019-07-01', 'None') ON CONFLICT DO NOTHING;
INSERT INTO public.iam_adhtech (id, created_date, name) VALUES (6, '2021-08-01', '99 Dots Lite') ON CONFLICT DO NOTHING;