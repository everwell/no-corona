ALTER TABLE iam_caccess ADD if not exists external boolean DEFAULT false;
ALTER TABLE iam_caccess DROP COLUMN if exists external_episode_enabled;


UPDATE iam_caccess
SET external = true
WHERE name = 'Everwell_GMRI';