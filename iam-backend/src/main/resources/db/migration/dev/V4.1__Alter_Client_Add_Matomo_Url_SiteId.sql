ALTER TABLE iam_caccess
  ADD COLUMN IF NOT EXISTS matomo_url varchar(255),
  ADD COLUMN IF NOT EXISTS matomo_site_id int;

/* just for testing purposes */

UPDATE iam_caccess
SET matomo_site_id = 3,
    matomo_url = 'https://matomo.local/matomo.php'
where id = 1;

UPDATE iam_caccess
SET matomo_site_id = 3,
    matomo_url = 'https://matomo.local/matomo.php'
where id = 2;
