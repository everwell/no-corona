# Transition Service

To help store the rules information and use the statemachine to evaluate the entity stages

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

1. Java 8
2. IntelliJ IDEA / Eclipse or any editor capable of running Java
3. Install Maven
4. Add Lombok Plugin
5. Install Postgres (for running a pure development environment)
6. Install RabbitMQ (for running a pure development environment)
7. Docker

The application makes use of the Flyway Migration Manager to automatically do migrations within PostgreSQL database.

While running the project for the first time, add default client from bootstrap.properties in client table of local db.
Add queues 'q.episode.client.add_episode','q.episode.update_tracker' in RabbitMQ if not present.

### Installing and Deployment

A step by step series of examples that tell you how to get a development env running

A dockerfile has been created to easily deploy the application with a specific environment
The dockerfile is able to choose a host of options as present in 
```
docker build MVN_PROFILE=<ENVIRONMENT> -t "transition-v1:transition-service"
```

For the first time installation this would pull all images through the maven and use it internally for building the executable version of the application.

Before running the docker image within the container, please make sure that all dependencies has been started.

To run the image just made, use
```
docker run --net=host -d transition-v1:transition-service
```

The application details as started in the container can be checked as
```
docker logs <CONTAINER_ID>

2021-03-11 05:44:54.386  INFO 1 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat started on port(s): 8787 (http) with context path ''
2021-03-11 05:44:54.406  INFO 1 --- [           main] c.e.transition.TransitionApplication     : Started TransitionApplication
```

Alternatively use the deployment commands for easy startup options.
The options for env are among dev, oncall, beta, nikshay-prod and hub-prod.
```
mvn clean compile -P$env package
java -jar transition-service/target/transition-0.0.1-SNAPSHOT.jar > console.log&
```

The deployment commands builds a new package of the java project and runs the unit tests.
Once the deployment is done, we can look at the log to ascertain the application has started
```
2022-10-26 15:45:00.226  INFO 10012 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat started on port(s): 8787 (http) with context path ''
2022-10-26 15:45:01.710  INFO 10012 --- [           main] s.d.s.w.s.ApiListingReferenceScanner     : Scanning for api listing references
2022-10-26 15:45:03.961  INFO 10012 --- [           main] c.e.transition.TransitionApplication     : Started TransitionApplication in 63.732 seconds (JVM running for 66.04)
```

### Building, Compilation and Packaging

For individual compiling of the project, we run the maven commands to build and compile

```
mvn clean compile
```

For packaging of the project in an executable format.
This would execute all the compilation build and test commands before packaging
```
mvn clean compile package
```

For individual running of the project, we run the maven commands

```
mvn spring-boot:run
```

## Running the tests

Transition Service uses JUnit to run tests

Run the following command to evaluate the entire test suite
```
mvn test
```

Run the following command to evaluate individual unit tests
```
mvn test -Dtest=<testfilename>
```

## Built With

* [Springboot](https://spring.io/projects/spring-boot) - The Java framework used
* [Maven](https://maven.apache.org/) - Dependency Management
* [Mockito](https://github.com/mockito/mockito) - Used to generate mocks for testing independently

## License

This project is licensed under the MIT License

