FROM maven:3.6.3-amazoncorretto-8 as mvncache
WORKDIR /transition-service
COPY pom.xml .
RUN mvn dependency:go-offline
ARG MVN_PROFILE=dev
COPY src/ /transition-service/src/
RUN mvn clean compile -P$MVN_PROFILE -DskipTests=true package

FROM openjdk:8-jdk-alpine as runimage
RUN apk update && \
   apk add ca-certificates curl && \
   update-ca-certificates && \
   rm -rf /var/cache/apk/* && \
   addgroup -S appgroup && \
   adduser -S appuser -G appgroup
USER appuser
WORKDIR /home/appuser
COPY --from=mvncache /transition-service/target/transition-0.0.1-SNAPSHOT.jar /home/appuser/transition-service/target/transition-0.0.1-SNAPSHOT.jar
COPY --from=mvncache /transition-service/src/main/resources/db/migration/ /home/appuser/transition-service/src/main/resources/db/migration/
COPY --from=mvncache /transition-service/src/main/resources/applicationinsights-agent-3.4.4.jar /home/appuser/transition-service/src/main/resources/applicationinsights-agent-3.4.4.jar
EXPOSE 8787
CMD java -javaagent:"transition-service/src/main/resources/applicationinsights-agent-3.4.4.jar" -jar transition-service/target/transition-0.0.1-SNAPSHOT.jar


# The above images are present in our private repo and can be used instead if the build fails in the future
# registry.gitlab.com/everwell/devops/gitlab-ci/maven:3.6.3-ibmjava-8-alpine
# registry.gitlab.com/everwell/devops/gitlab-ci/openjdk:8-jdk-alpine