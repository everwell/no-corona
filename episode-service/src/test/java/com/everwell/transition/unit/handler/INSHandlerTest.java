package com.everwell.transition.unit.handler;

import com.everwell.transition.BaseTest;
import com.everwell.transition.binders.INSBinder;
import com.everwell.transition.constants.NotificationConstants;
import com.everwell.transition.handlers.INSHandler;
import com.everwell.transition.model.dto.GenericEvents;
import com.everwell.transition.model.dto.notification.SmsDetails;
import com.everwell.transition.model.dto.notification.NotificationTriggerDto;
import com.everwell.transition.model.dto.notification.PushNotificationDetails;
import com.everwell.transition.model.dto.notification.PushNotificationTemplateDto;
import com.everwell.transition.model.dto.notification.*;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.messaging.MessageChannel;

import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import java.util.Map;
import java.util.*;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;

public class INSHandlerTest extends BaseTest {

    @Spy
    @InjectMocks
    INSHandler insHandler;

    @Mock
    INSBinder insBinder;

    @Mock
    MessageChannel insNotificationOutput;

    @Mock
    MessageChannel insSmsOutput;

    @Mock
    MessageChannel insEmailOutput;

    @Test
    public void sendSmsToINSTest() {
        Mockito.when(insBinder.insNotificationOutput()).thenReturn(insNotificationOutput);
        NotificationTriggerDto notificationTriggerDto = new NotificationTriggerDto(1L, 1L, 1L, "getPatientsWhoHaveNotMarkedAdherenceForToday", "Asia/Kolkata");
        PushNotificationTemplateDto pushNotificationTemplateDto = new PushNotificationTemplateDto("1", "1", null, null);
        PushNotificationDetails pushNotificationDetails = new PushNotificationDetails(Collections.singletonList(pushNotificationTemplateDto), notificationTriggerDto.getVendorId(), notificationTriggerDto.getTriggerId(), notificationTriggerDto.getTemplateId(), false, null, true);
        GenericEvents<PushNotificationDetails> event = new GenericEvents<>(pushNotificationDetails, "getPatientsWhoHaveNotMarkedAdherenceForToday");
        insHandler.sendNotificationToINS(Collections.singletonList(event), 29L);
        Mockito.verify(insNotificationOutput, Mockito.times(1)).send(any());
    }

    @Test
    public void sendSmstoINSTest2(){
        Mockito.when(insBinder.insSmsOutput()).thenReturn(insSmsOutput);
        List<UserSmsDetails> personList = new ArrayList<>();
        Map<String, String> userParameters = new HashMap<>();
        userParameters.put(NotificationConstants.TEMPLATE_OTP, "9876");
        userParameters.put(NotificationConstants.TEMPLATE_USER, "purposeText");
        userParameters.put(NotificationConstants.NAME_OF_APP, "source");
        personList.add(new UserSmsDetails(0L, userParameters, "9876543210"));
        List<Long> template_ids = Collections.singletonList(1L);
        SmsDetails smsDetails = new SmsDetails(personList,1L,1L,1L,template_ids,true,true,true,"");
        GenericEvents<SmsDetails> event = new GenericEvents<>(smsDetails,"sendRegistrationOtp");
        insHandler.sendSmsToINS(Collections.singletonList(event), 29L);
        Mockito.verify(insSmsOutput, Mockito.times(1)).send(any());
    }

    @Test
    public void sendEmailToINSTest() {
        Mockito.when(insBinder.insEmailOutput()).thenReturn(insEmailOutput);
        EmailRequest emailRequest = new EmailRequest();
        GenericEvents<EmailRequest> event = new GenericEvents<>(emailRequest, "sendEmail");
        insHandler.sendEmailToINS(event, 29L);
        Mockito.verify(insEmailOutput, Mockito.times(1)).send(any());
    }

}
