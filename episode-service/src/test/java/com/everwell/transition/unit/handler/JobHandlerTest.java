package com.everwell.transition.unit.handler;

import com.everwell.transition.BaseTest;
import com.everwell.transition.handlers.JobHandler;
import com.everwell.transition.service.TriggerService;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;

import static org.mockito.Mockito.*;

public class JobHandlerTest extends BaseTest {
    @Spy
    @InjectMocks
    JobHandler jobHandler;

    @Mock
    TriggerService triggerService;

    @Test
    public void testMethodCallToReadTable()
    {
        doNothing().when(triggerService).readTriggerTable();
        jobHandler.everydayJobScheduler();
        verify(triggerService, times(1)).readTriggerTable();
    }

}
