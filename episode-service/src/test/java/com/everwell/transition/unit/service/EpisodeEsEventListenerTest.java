package com.everwell.transition.unit.service;

import com.everwell.transition.BaseTest;
import com.everwell.transition.binders.EpisodeEsInsertUpdateBinder;
import com.everwell.transition.elasticsearch.service.ESEpisodeService;
import com.everwell.transition.elasticsearch.service.Impl.EpisodeEsEventListener;
import com.everwell.transition.model.db.EpisodeTag;
import com.everwell.transition.model.dto.ElasticDocTagsWrapper;
import com.everwell.transition.model.dto.EpisodeEsInsertUpdateDto;
import com.everwell.transition.model.response.EpisodeTagResponse;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.messaging.MessageChannel;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class EpisodeEsEventListenerTest extends BaseTest {

    @InjectMocks
    EpisodeEsEventListener episodeEsEventListener;

    @Mock
    EpisodeEsInsertUpdateBinder episodeEsInsertUpdateBinder;

    @Mock
    MessageChannel stageOutput;

    @Mock
    ESEpisodeService esEpisodeService;

    @Before
    public void init() {
        when(episodeEsInsertUpdateBinder.eventOutput()).thenReturn(stageOutput);
    }

    @Test
    public void addEditEventTest() {
        EpisodeEsInsertUpdateDto episodeEsInsertUpdateDto = new EpisodeEsInsertUpdateDto(1L, 1L, false, true);
        episodeEsEventListener.addEditEvent(episodeEsInsertUpdateDto);
        Mockito.verify(stageOutput, Mockito.times(1)).send(any());
    }

    @Test
    public void tagsListenerTest() {
        ElasticDocTagsWrapper elasticDocTagsWrapper = new ElasticDocTagsWrapper(getEpisodeTags());
        episodeEsEventListener.tagsListener(elasticDocTagsWrapper);
        Mockito.verify(esEpisodeService, Mockito.times(1)).updateBulkElastic(any());
    }

    public static List<EpisodeTagResponse> getEpisodeTags() {
        long episodeId = 12345;
        List<EpisodeTag> tags = Arrays.asList(
                new EpisodeTag(1L, episodeId, "PHONE_UNREACHABLE", LocalDateTime.now(), LocalDateTime.now(), false),
                new EpisodeTag(1L, episodeId, "PHONE_SWITCHED_OFF", LocalDateTime.now(), LocalDateTime.now(), true)
        );
        List<String> currentTags = tags.stream().filter(EpisodeTag::getSticky).map(EpisodeTag::getTagName).collect(Collectors.toList());
        List<EpisodeTag> nonStickyTags = tags.stream().filter(t -> !t.getSticky()).collect(Collectors.toList());
        List<EpisodeTagResponse> list = new ArrayList<>();
        list.add(new EpisodeTagResponse(12345L, nonStickyTags, currentTags));
        return list;
    }

}
