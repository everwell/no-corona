package com.everwell.transition.unit.consumers;

import com.everwell.transition.BaseTest;
import com.everwell.transition.constants.EpisodeTagConstants;
import com.everwell.transition.consumers.RefreshTagsConsumer;
import com.everwell.transition.elasticsearch.ElasticConstants;
import com.everwell.transition.elasticsearch.indices.EpisodeIndex;
import com.everwell.transition.handlers.INSHandler;
import com.everwell.transition.model.dto.EpisodeSearchResult;
import com.everwell.transition.model.request.episodesearch.EpisodeSearchRequest;
import com.everwell.transition.service.EpisodeService;
import com.everwell.transition.service.EpisodeTagService;
import com.everwell.transition.utils.Utils;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.context.ApplicationEventPublisher;

import java.time.LocalDateTime;
import java.util.*;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class RefreshTagsConsumerTest extends BaseTest {
    @Spy
    @InjectMocks
    RefreshTagsConsumer refreshTagsConsumer;

    @Mock
    EpisodeService episodeService;

    @Mock
    EpisodeTagService episodeTagService;

    @Mock
    ApplicationEventPublisher applicationEventPublisher;

    @Mock
    INSHandler insHandler;

    @Test
    public void consumerTestEndDatePassed() {
        String request = getRequestData();
        EpisodeIndex episodeIndex = new EpisodeIndex();
        episodeIndex.setId("1");
        episodeIndex.setCreatedDate("2023-04-01 00:00:00");
        episodeIndex.setEndDate("2023-04-01 00:00:00");
        episodeIndex.setCurrentTags(Collections.singletonList(EpisodeTagConstants.NEW_ENROLLMENT));
        EpisodeSearchResult episodeSearchResult = new EpisodeSearchResult(1, Collections.singletonList(episodeIndex), new HashMap<>());
        doReturn(episodeSearchResult).when(episodeService).elasticSearch(any(EpisodeSearchRequest.class));
        refreshTagsConsumer.consume(new Message(request.getBytes(), new MessageProperties()));
        verify(episodeTagService, times(1)).deleteBulk(any());
        verify(episodeTagService, times(1)).save(any());
    }

    @Test
    public void consumerTestNewEnrollment() {
        String request = getRequestData();
        EpisodeIndex episodeIndex = new EpisodeIndex();
        episodeIndex.setId("1");
        episodeIndex.setCreatedDate(Utils.getFormattedDateNew(LocalDateTime.now().minusDays(2)));
        episodeIndex.setEndDate("2023-04-01 00:00:00");
        EpisodeSearchResult episodeSearchResult = new EpisodeSearchResult(1, Collections.singletonList(episodeIndex), new HashMap<>());
        when(episodeService.elasticSearch(any(EpisodeSearchRequest.class))).thenReturn(episodeSearchResult);
        refreshTagsConsumer.consume(new Message(request.getBytes(), new MessageProperties()));
        verify(episodeTagService, times(1)).deleteBulk(any());
        verify(episodeTagService, times(1)).save(any());
    }

    @Test
    public void consumerTestEndDateNullError() {
        String request = getRequestData();
        EpisodeIndex episodeIndex = new EpisodeIndex();
        episodeIndex.setId("1");
        episodeIndex.setCreatedDate(Utils.getFormattedDateNew(LocalDateTime.now().minusDays(2)));
        episodeIndex.setEndDate(null);
        EpisodeSearchResult episodeSearchResult = new EpisodeSearchResult(1, Collections.singletonList(episodeIndex), new HashMap<>());
        doReturn(episodeSearchResult).when(episodeService).elasticSearch(any(EpisodeSearchRequest.class));
        refreshTagsConsumer.consume(new Message(request.getBytes(), new MessageProperties()));
        verify(episodeTagService, times(0)).deleteBulk(any());
        verify(episodeTagService, times(0)).save(any());
    }

    @Test
    public void consumerTestPartialSuccess() {
        String request = getRequestData();
        EpisodeIndex episodeIndex = new EpisodeIndex();
        episodeIndex.setId("1");
        episodeIndex.setCreatedDate(Utils.getFormattedDateNew(LocalDateTime.now().minusDays(2)));
        episodeIndex.setEndDate(null);
        EpisodeIndex episodeIndex2 = new EpisodeIndex();
        episodeIndex2.setId("1");
        episodeIndex2.setCreatedDate(Utils.getFormattedDateNew(LocalDateTime.now().minusDays(2)));
        episodeIndex2.setEndDate("2023-04-01 00:00:00");
        EpisodeSearchResult episodeSearchResult = new EpisodeSearchResult(1, Arrays.asList(episodeIndex, episodeIndex2), new HashMap<>());
        doReturn(episodeSearchResult).when(episodeService).elasticSearch(any(EpisodeSearchRequest.class));
        refreshTagsConsumer.consume(new Message(request.getBytes(), new MessageProperties()));
        verify(episodeTagService, times(1)).deleteBulk(any());
        verify(episodeTagService, times(1)).save(any());
    }

    @Test
    public void consumerTestElasticBatching() {
        String request = getRequestData();
        EpisodeIndex episodeIndex = new EpisodeIndex();
        episodeIndex.setId("1");
        episodeIndex.setCreatedDate("2023-04-01 00:00:00");
        episodeIndex.setEndDate("2023-04-01 00:00:00");
        episodeIndex.setCurrentTags(Collections.singletonList(EpisodeTagConstants.NEW_ENROLLMENT));
        List<EpisodeIndex> episodeIndexList = new ArrayList<>(Collections.nCopies(ElasticConstants.ELASTIC_BATCH_SIZE + 1, episodeIndex));
        EpisodeSearchResult episodeSearchResult = new EpisodeSearchResult(ElasticConstants.ELASTIC_BATCH_SIZE + 1, episodeIndexList, new HashMap<>());
        EpisodeSearchResult episodeSearchResult1 = new EpisodeSearchResult(ElasticConstants.ELASTIC_BATCH_SIZE + 1, Collections.singletonList(episodeIndex), new HashMap<>());
        when(episodeService.elasticSearch(any())).thenReturn(episodeSearchResult).thenReturn(episodeSearchResult1);
        refreshTagsConsumer.consume(new Message(request.getBytes(), new MessageProperties()));
        verify(episodeTagService, times(2)).deleteBulk(any());
        verify(episodeTagService, times(2)).save(any());
    }

    private String getRequestData() {
        return "{\"episodeSearchRequest\":{\"size\":1,\"page\":0,\"fieldsToShow\":[\"id\",\"endDate\",\"createdDate\",\"currentTags\"],\"typeOfEpisode\":[\"IndiaTbPublic\",\"IndiaTbPrivate\",\"PublicPrivatePartnership\"],\"sortKey\":\"id\",\"sortDirection\":\"ASC\",\"agg\":null,\"search\":{\"must\":{\"clientId\":1,\"deploymentCode\":\"ASCETH\",\"stageKey\":\"ON_TREATMENT\"},\"mustNot\":null},\"requestIdentifier\":null,\"count\":false},\"deploymentCode\":\"ASCETH\",\"vendorId\":1,\"clientId\":1,\"triggerId\":1,\"templateId\":1}";
    }
}
