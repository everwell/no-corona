package com.everwell.transition.unit.service;

import com.everwell.transition.BaseTest;
import com.everwell.transition.exceptions.ValidationException;
import com.everwell.transition.model.db.CourseCompletion;
import com.everwell.transition.model.db.CourseUrlMapping;
import com.everwell.transition.model.dto.CourseDto;
import com.everwell.transition.model.request.course.MarkCourseRequest;
import com.everwell.transition.model.response.CourseResponse;
import com.everwell.transition.model.response.MarkCourseResponse;
import com.everwell.transition.postconstruct.CourseUrlMappingList;
import com.everwell.transition.repositories.CourseCompletionRepository;
import com.everwell.transition.repositories.CourseRepository;
import com.everwell.transition.repositories.CourseUrlMappingRepository;
import com.everwell.transition.service.impl.CourseServiceImpl;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class CourseServiceTest extends BaseTest {

    @Spy
    @InjectMocks
    CourseServiceImpl courseService;

    @Mock
    CourseCompletionRepository courseCompletionRepository;

    @Mock
    CourseRepository courseRepository;

    @Mock
    CourseUrlMappingList courseUrlMappingList;

    @Test
    public void getAllCoursesTest() {
        Long episodeId = 1L;
        when(courseCompletionRepository.getAllByEpisodeId(episodeId)).thenReturn(getCourses());
        when(courseUrlMappingList.getCourseUrlMappingList()).thenReturn(getAllMappings());
        CourseResponse courseResponse = courseService.getAllCourses(episodeId);
        Assertions.assertEquals((int) (int) courseResponse.getCourses().stream().filter(e -> e.getIsComplete()).count(), 2);
    }

    @Test
    public void markAsCompleteTest() {
        MarkCourseRequest markCourseRequest = new MarkCourseRequest(1L,1L);
        when(courseCompletionRepository.save(any())).thenReturn(getCourses().get(0));
        when(courseRepository.count()).thenReturn(2L);
        MarkCourseResponse markCourseResponse = courseService.markCourseAsComplete(markCourseRequest);
        Assertions.assertEquals(markCourseResponse.getCourseId(),getCourses().get(0).getCourseId());
    }

    @Test(expected=Exception.class)
    public void markAsCompleteTest_NullCourseIdTest() {
        MarkCourseRequest markCourseRequest = new MarkCourseRequest(1L,null);
        when(courseCompletionRepository.save(any())).thenReturn(getCourses().get(0));
        when(courseRepository.count()).thenReturn(2L);
        when(courseService.markCourseAsComplete(markCourseRequest)).thenThrow(ValidationException.class) ;
    }

    List<CourseCompletion> getCourses() {

        return Arrays.asList(
                new CourseCompletion(1L,1L,true),
                new CourseCompletion(1L,2L,true)
        );
    }

    List<CourseUrlMapping> getAllMappings() {

        return Arrays.asList(
          new CourseUrlMapping(1L,2L,"English","https://igot.gov.in/explore-course/course/do_31343045368297062411748","2. TB Diagnosis and Treatment"),
          new CourseUrlMapping(2L,1L,"English","https://igot.gov.in/explore-course/course/do_31343045368297062411748","3. Stigma, Myths and Misconceptions related to TB"),
          new CourseUrlMapping(3L,3L,"English","https://igot.gov.in/explore-course/course/do_31343045368297062411748","4. NTEP programme and services")
        );
    }
}
