package com.everwell.transition.unit.service;

import com.everwell.transition.BaseTest;
import com.everwell.transition.constants.Constants;
import com.everwell.transition.constants.FieldConstants;
import com.everwell.transition.elasticsearch.data.ESEpisodeRepository;
import com.everwell.transition.elasticsearch.indices.EpisodeIndex;
import com.everwell.transition.elasticsearch.service.Impl.ESEpisodeServiceImpl;
import com.everwell.transition.elasticsearch.service.Impl.EpisodeEsQueryBuilder;
import com.everwell.transition.exceptions.NotFoundException;
import com.everwell.transition.model.dto.AggregationDto;
import com.everwell.transition.model.dto.EpisodeSearchResult;
import com.everwell.transition.model.request.episodesearch.EpisodeSearchRequest;
import com.everwell.transition.utils.Utils;
import com.fasterxml.jackson.core.JsonFactory;
import org.apache.commons.io.IOUtils;
import org.apache.lucene.search.TotalHits;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.common.ParseField;
import org.elasticsearch.common.bytes.BytesArray;
import org.elasticsearch.common.bytes.BytesReference;
import org.elasticsearch.common.xcontent.ContextParser;
import org.elasticsearch.common.xcontent.DeprecationHandler;
import org.elasticsearch.common.xcontent.NamedXContentRegistry;
import org.elasticsearch.common.xcontent.XContentParser;
import org.elasticsearch.common.xcontent.json.JsonXContentParser;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.AbstractAggregationBuilder;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.bucket.histogram.DateHistogramInterval;
import org.elasticsearch.search.aggregations.bucket.histogram.ParsedDateHistogram;
import org.elasticsearch.search.aggregations.bucket.terms.ParsedStringTerms;
import org.elasticsearch.search.aggregations.bucket.terms.StringTerms;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;
import org.elasticsearch.search.internal.InternalSearchResponse;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.data.elasticsearch.core.*;
import org.springframework.data.elasticsearch.core.clients.elasticsearch7.ElasticsearchAggregations;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.stream.Collectors;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

public class ESEpisodeServiceTest extends BaseTest {

    @Spy
    @InjectMocks
    ESEpisodeServiceImpl esEpisodeService;

    @Mock
    ESEpisodeRepository esEpisodeRepository;

    @Mock
    ElasticsearchOperations elasticsearchOperations;

    @Mock
    EpisodeEsQueryBuilder episodeEsQueryBuilder;

    @Test
    public void saveTest() {
        EpisodeIndex episodeIndex = new EpisodeIndex();
        esEpisodeService.save(episodeIndex);
        Mockito.verify(esEpisodeRepository, Mockito.times(1)).save(any());
    }

    @Test
    public void findByIdAndClientIdTest() {
        EpisodeIndex episodeIndex = new EpisodeIndex();
        episodeIndex.setClientId(1L);
        episodeIndex.setAddedBy(123L);
        episodeIndex.setTypeOfEpisode("Private");
        when(esEpisodeRepository.findAllByIdAndClientId(eq("1"), eq(1L))).thenReturn(Optional.of(episodeIndex));
        EpisodeIndex episodeIndexResponse = esEpisodeService.findByIdAndClientId(1L, 1L);
        Assert.assertEquals("Private", episodeIndexResponse.getTypeOfEpisode());
    }

    @Test(expected = NotFoundException.class)
    public void findByIdAndClientId_NullTest() {
        when(esEpisodeRepository.findAllByIdAndClientId(eq("1"), eq(1L))).thenReturn(Optional.empty());
        esEpisodeService.findByIdAndClientId(1L, 1L);
    }

    @Test
    public void deleteTest() {
        doNothing().when(esEpisodeService).deleteByQuery(any());
        esEpisodeService.delete("1", 1L);
        Mockito.verify(esEpisodeService, Mockito.times(1)).deleteByQuery(any());
    }

    @Test
    public void updateToElastic() {
        doNothing().when(esEpisodeService).updateBulkElastic(any());
        esEpisodeService.updateToElastic(new UpdateRequest());
        Mockito.verify(esEpisodeService, Mockito.times(1)).updateBulkElastic(any());
    }

    @Test
    public void findAllByPersonIdTest() {
        List<EpisodeIndex> episodeIndexList = Arrays.asList(
                new EpisodeIndex(),
                new EpisodeIndex()
        );
        when(esEpisodeRepository.findAllByPersonIdAndClientId(eq(1L), eq(1L))).thenReturn(episodeIndexList);
        List<EpisodeIndex> episodeIndices = esEpisodeService.findAllByPersonId(1L, 1L);
        Assert.assertEquals(2, episodeIndices.size());
    }

    @Test
    public void searchTest() {
        EpisodeSearchRequest episodeSearchRequest = new EpisodeSearchRequest();
        EpisodeSearchResult episodeSearchResult = new EpisodeSearchResult();
        episodeSearchResult.setTotalHits(10);

        Map<String, String> fieldToTableMap = new HashMap<>();
        BoolQueryBuilder queryBuilder = QueryBuilders.boolQuery();

        when(episodeEsQueryBuilder.build(eq(episodeSearchRequest), eq(fieldToTableMap))).thenReturn(queryBuilder);
        when(episodeEsQueryBuilder.convertFieldNames(any(), any(), any())).thenReturn("id");
        doReturn(episodeSearchResult).when(esEpisodeService).performSearch(any());
        EpisodeSearchResult episodeSearchResult1 = esEpisodeService.search(episodeSearchRequest, fieldToTableMap, false);
        Assert.assertEquals(10, episodeSearchResult1.getTotalHits());
    }

    @Test
    public void searchfullDocFalseTest() {
        EpisodeSearchRequest episodeSearchRequest = new EpisodeSearchRequest();
        EpisodeSearchResult episodeSearchResult = new EpisodeSearchResult();
        episodeSearchResult.setTotalHits(10);
        Map<String, String> fieldToTableMap = new HashMap<>();
        BoolQueryBuilder queryBuilder = QueryBuilders.boolQuery();
        when(episodeEsQueryBuilder.build(eq(episodeSearchRequest), eq(fieldToTableMap))).thenReturn(queryBuilder);
        when(episodeEsQueryBuilder.convertFieldNames(any(), any(), any())).thenReturn("id");
        doReturn(episodeSearchResult).when(esEpisodeService).performSearch(any());
        EpisodeSearchResult episodeSearchResult1 = esEpisodeService.search(episodeSearchRequest, fieldToTableMap, true);
        Assert.assertEquals(10, episodeSearchResult1.getTotalHits());
    }

    @Test
    public void searchCountTest() {
        EpisodeSearchRequest episodeSearchRequest = new EpisodeSearchRequest();
        episodeSearchRequest.setCount(true);
        EpisodeSearchResult episodeSearchResult = new EpisodeSearchResult();
        episodeSearchResult.setTotalHits(10);
        Map<String, String> fieldToTableMap = new HashMap<>();
        BoolQueryBuilder queryBuilder = QueryBuilders.boolQuery();
        when(episodeEsQueryBuilder.build(eq(episodeSearchRequest), eq(fieldToTableMap))).thenReturn(queryBuilder);
        when(episodeEsQueryBuilder.convertFieldNames(any(), any(), any())).thenReturn("id");
        doReturn(episodeSearchResult).when(esEpisodeService).performCount(any());
        EpisodeSearchResult episodeSearchResult1 = esEpisodeService.search(episodeSearchRequest, fieldToTableMap, true);
        Assert.assertEquals(10, episodeSearchResult1.getTotalHits());
    }

    @Test
    public void searchWithAggTest() {
        EpisodeSearchRequest episodeSearchRequest = new EpisodeSearchRequest();
        episodeSearchRequest.setAgg(new EpisodeSearchRequest.SupportedAggMethods(Collections.singletonList(new AggregationDto(10, "stageCount", "stageName")), null));
        EpisodeSearchResult episodeSearchResult = new EpisodeSearchResult();
        episodeSearchResult.setTotalHits(10);
        Map<String, String> fieldToTableMap = new HashMap<>();
        BoolQueryBuilder queryBuilder = QueryBuilders.boolQuery();
        when(episodeEsQueryBuilder.build(eq(episodeSearchRequest), eq(fieldToTableMap))).thenReturn(queryBuilder);
        when(episodeEsQueryBuilder.convertFieldNames(any(), eq("stageName"), any())).thenReturn("stageName");
        when(episodeEsQueryBuilder.convertFieldNames(any(), eq("id"), any())).thenReturn("id");
        when(episodeEsQueryBuilder.buildAggregation(any())).thenReturn(new ArrayList<>());
        doReturn(episodeSearchResult).when(esEpisodeService).performSearch(any());
        EpisodeSearchResult episodeSearchResult1 = esEpisodeService.search(episodeSearchRequest, fieldToTableMap, true);
        Assert.assertEquals(10, episodeSearchResult1.getTotalHits());
    }

    @Test
    public void searchWithAggTest_DateAggregation() {
        EpisodeSearchRequest episodeSearchRequest = new EpisodeSearchRequest();
        episodeSearchRequest.setAgg(new EpisodeSearchRequest.SupportedAggMethods(null, Collections.singletonList(new AggregationDto(10, "stageCount", "stageName"))));
        EpisodeSearchResult episodeSearchResult = new EpisodeSearchResult();
        episodeSearchResult.setTotalHits(10);
        Map<String, String> fieldToTableMap = new HashMap<>();
        BoolQueryBuilder queryBuilder = QueryBuilders.boolQuery();
        when(episodeEsQueryBuilder.build(eq(episodeSearchRequest), eq(fieldToTableMap))).thenReturn(queryBuilder);
        when(episodeEsQueryBuilder.convertFieldNames(any(), eq("stageName"), any())).thenReturn("stageName");
        when(episodeEsQueryBuilder.convertFieldNames(any(), eq("id"), any())).thenReturn("id");
        when(episodeEsQueryBuilder.buildAggregation(any())).thenReturn(new ArrayList<>());
        doReturn(episodeSearchResult).when(esEpisodeService).performSearch(any());
        EpisodeSearchResult episodeSearchResult1 = esEpisodeService.search(episodeSearchRequest, fieldToTableMap, true);
        Assert.assertEquals(10, episodeSearchResult1.getTotalHits());
    }

    @Test
    public void searchWithCollapseTest() {
        EpisodeSearchRequest episodeSearchRequest = new EpisodeSearchRequest();
        episodeSearchRequest.setCollapseField(FieldConstants.EPISODE_FIELD_DISEASE_ID);
        episodeSearchRequest.setTrackTotalHits(true);
        episodeSearchRequest.setSearchAfter(Collections.singletonList(1L));
        EpisodeSearchResult episodeSearchResult = new EpisodeSearchResult();
        episodeSearchResult.setTotalHits(10);

        Map<String, String> fieldToTableMap = new HashMap<>();
        BoolQueryBuilder queryBuilder = QueryBuilders.boolQuery();

        when(episodeEsQueryBuilder.build(eq(episodeSearchRequest), eq(fieldToTableMap))).thenReturn(queryBuilder);
        when(episodeEsQueryBuilder.convertFieldNames(any(), any(), any())).thenReturn("id");
        doReturn(episodeSearchResult).when(esEpisodeService).performSearch(any());
        EpisodeSearchResult episodeSearchResult1 = esEpisodeService.search(episodeSearchRequest, fieldToTableMap, false);
        Assert.assertEquals(10, episodeSearchResult1.getTotalHits());
    }

    @Test
    public void searchWithScrollIdTest() {
        EpisodeSearchRequest episodeSearchRequest = new EpisodeSearchRequest();
        episodeSearchRequest.setScrollId("123");
        EpisodeSearchResult episodeSearchResult = new EpisodeSearchResult();
        episodeSearchResult.setTotalHits(10);

        Map<String, String> fieldToTableMap = new HashMap<>();
        BoolQueryBuilder queryBuilder = QueryBuilders.boolQuery();

        when(episodeEsQueryBuilder.build(eq(episodeSearchRequest), eq(fieldToTableMap))).thenReturn(queryBuilder);
        when(episodeEsQueryBuilder.convertFieldNames(any(), any(), any())).thenReturn("id");
        doReturn(null).when(esEpisodeService).scrollByRestClient(any());
        doReturn(episodeSearchResult).when(esEpisodeService).collectDataFromSearchResponse(any());
        EpisodeSearchResult episodeSearchResult1 = esEpisodeService.search(episodeSearchRequest, fieldToTableMap, false);
        Assert.assertEquals(10, episodeSearchResult1.getTotalHits());
    }

    @Test
    public void searchWithScrollTest() {
        EpisodeSearchRequest episodeSearchRequest = new EpisodeSearchRequest();
        episodeSearchRequest.setScroll(true);
        EpisodeSearchResult episodeSearchResult = new EpisodeSearchResult();
        episodeSearchResult.setTotalHits(10);

        Map<String, String> fieldToTableMap = new HashMap<>();
        BoolQueryBuilder queryBuilder = QueryBuilders.boolQuery();

        when(episodeEsQueryBuilder.build(eq(episodeSearchRequest), eq(fieldToTableMap))).thenReturn(queryBuilder);
        when(episodeEsQueryBuilder.convertFieldNames(any(), any(), any())).thenReturn("id");
        doReturn(null).when(esEpisodeService).searchByRestClient(any());
        doReturn(episodeSearchResult).when(esEpisodeService).collectDataFromSearchResponse(any());
        EpisodeSearchResult episodeSearchResult1 = esEpisodeService.search(episodeSearchRequest, fieldToTableMap, false);
        Assert.assertEquals(10, episodeSearchResult1.getTotalHits());
    }

    @Test
    public void performSearchTest() {
        List<SearchHit<EpisodeIndex>> searchHits = new ArrayList<>();
        Map<String, Object> stageData = new HashMap<>();
        stageData.put("personId", 100);
        stageData.put("diseaseId", null);
        searchHits.add(new SearchHit<>(
                "1", "1", "", 1f, new Object[1], new HashMap<>(),
                new EpisodeIndex("1", 1L, 1L, false, 1L, "1,2,3", "2022-01-01 13:00:00", "LOW", 1L, 100L, "", "", "", "", "", "", null, null, stageData, new ArrayList<>())
        ));
        NativeSearchQuery nativeSearchQuery = new NativeSearchQueryBuilder().build();
        when(elasticsearchOperations.search(eq(nativeSearchQuery), eq(EpisodeIndex.class))).thenReturn(
                new SearchHitsImpl<>(10, TotalHitsRelation.EQUAL_TO, 12f, "12", searchHits, null, null)
        );
        EpisodeSearchResult episodeSearchResult = esEpisodeService.performSearch(nativeSearchQuery);
        Assert.assertEquals(10, episodeSearchResult.getTotalHits());
    }

    public static List<NamedXContentRegistry.Entry> getDefaultNamedXContents() {
        Map<String, ContextParser<Object, ? extends Aggregation>> map = new HashMap<>();
        map.put(StringTerms.NAME, (p, c) -> ParsedStringTerms.fromXContent(p, (String) c));
        return map.entrySet()
                .stream()
                .map(entry -> new NamedXContentRegistry.Entry(Aggregation.class, new ParseField(entry.getKey()), entry.getValue()))
                .collect(Collectors.toList());
    }

    @Test
    public void performSearchWithAggTest() throws IOException {
        List<SearchHit<EpisodeIndex>> searchHits = new ArrayList<>();
        Map<String, Object> stageData = new HashMap<>();
        stageData.put("personId", 100);
        stageData.put("diseaseId", null);
        searchHits.add(new SearchHit<>(
                "1", "1", "", 1f, new Object[1], new HashMap<>(),
                new EpisodeIndex("1", 1L, 1L, false, 1L, "1,2,3", "2022-01-01 13:00:00", "LOW", 1L, 100L, "", "", "", "", "", "", null, null, stageData, new ArrayList<>())
        ));
        String jsonString = "{\"buckets\":[{\"key\":\"DIAGNOSED_NOT_ON_TREATMENT\",\"doc_count\":16788}]}";
        NamedXContentRegistry registry = new NamedXContentRegistry(getDefaultNamedXContents());
        XContentParser parser = new JsonXContentParser(registry, DeprecationHandler.THROW_UNSUPPORTED_OPERATION, new JsonFactory().createParser(jsonString));
        Aggregation aggregation = ParsedStringTerms.fromXContent(parser, "stageCount");
        List<AbstractAggregationBuilder<?>> aggregationBuilderList = new ArrayList<>();
        aggregationBuilderList.add(AggregationBuilders
                .terms("stageCount")
                .field("stageKey")
                .size(20));

        Aggregations aggregations = new Aggregations(Collections.singletonList(aggregation));
        ElasticsearchAggregations elasticsearchAggregations = new ElasticsearchAggregations(aggregations);
        SearchHitsImpl<EpisodeIndex> searchResponse = new SearchHitsImpl<>(10, TotalHitsRelation.EQUAL_TO, 12f, "12", searchHits, elasticsearchAggregations, null);
        NativeSearchQueryBuilder nativeSearchQueryBuilder = new NativeSearchQueryBuilder();
        NativeSearchQuery nativeSearchQuery = nativeSearchQueryBuilder.withAggregations(aggregationBuilderList).build();
        when(elasticsearchOperations.search(eq(nativeSearchQuery), eq(EpisodeIndex.class))).thenReturn(
                searchResponse
        );
        EpisodeSearchResult episodeSearchResult = esEpisodeService.performSearch(nativeSearchQuery);
        Assert.assertEquals((Long) 16788L, episodeSearchResult.getAggregationResults().get("stageCount").get("DIAGNOSED_NOT_ON_TREATMENT"));
    }

    @Test
    public void performSearchWithAggTest_DateAggregation() throws IOException {
        List<SearchHit<EpisodeIndex>> searchHits = new ArrayList<>();
        Map<String, Object> stageData = new HashMap<>();
        stageData.put("personId", 100);
        stageData.put("diseaseId", null);
        searchHits.add(new SearchHit<>(
                "1", "1", "", 1f, new Object[1], new HashMap<>(),
                new EpisodeIndex("1", 1L, 1L, false, 1L, "1,2,3", "2022-01-01 13:00:00", "LOW", 1L, 100L, "", "", "", "", "", "", null, null, stageData, new ArrayList<>())
        ));
        String jsonString = "{\"buckets\":[{\"key\":\"1672444800000\",\"key_as_string\": \"2022-12-31\",\"doc_count\":1234}]}";
        NamedXContentRegistry registry = new NamedXContentRegistry(getDefaultNamedXContents());
        XContentParser parser = new JsonXContentParser(registry, DeprecationHandler.THROW_UNSUPPORTED_OPERATION, new JsonFactory().createParser(jsonString));
        Aggregation aggregation = ParsedDateHistogram.fromXContent(parser, "enrollmentCount");
        List<AbstractAggregationBuilder<?>> aggregationBuilderList = new ArrayList<>();
        aggregationBuilderList.add(AggregationBuilders
                .dateHistogram("enrollmentCount")
                .field("stageKey")
                .calendarInterval(DateHistogramInterval.DAY)
                .format(Constants.DATE_FORMAT_WITHOUT_TIME)
        );

        Aggregations aggregations = new Aggregations(Collections.singletonList(aggregation));
        ElasticsearchAggregations elasticsearchAggregations = new ElasticsearchAggregations(aggregations);
        SearchHitsImpl<EpisodeIndex> searchResponse = new SearchHitsImpl<>(10, TotalHitsRelation.EQUAL_TO, 12f, "12", searchHits, elasticsearchAggregations, null);
        NativeSearchQueryBuilder nativeSearchQueryBuilder = new NativeSearchQueryBuilder();
        NativeSearchQuery nativeSearchQuery = nativeSearchQueryBuilder.withAggregations(aggregationBuilderList).build();
        when(elasticsearchOperations.search(eq(nativeSearchQuery), eq(EpisodeIndex.class))).thenReturn(
                searchResponse
        );
        EpisodeSearchResult episodeSearchResult = esEpisodeService.performSearch(nativeSearchQuery);
        Assert.assertEquals((Long) 1234L, episodeSearchResult.getAggregationResults().get("enrollmentCount").get("2022-12-31"));
    }

    @Test
    public void performCountTest() {
        NativeSearchQuery nativeSearchQuery = new NativeSearchQueryBuilder().build();
        when(elasticsearchOperations.count(eq(nativeSearchQuery), eq(EpisodeIndex.class))).thenReturn(22L);
        EpisodeSearchResult episodeSearchResult = esEpisodeService.performCount(nativeSearchQuery);
        Assert.assertEquals(22, episodeSearchResult.getTotalHits());
    }

    @Test
    public void collectDataFromSearchResponse() {
        Map<String, Object> stageData = new HashMap<>();
        stageData.put("personId", 100);
        stageData.put("diseaseId", null);
        String epJson = Utils.asJsonString(new EpisodeIndex("1", 1L, 1L, false, 1L, "1,2,3", "2022-01-01 13:00:00", "LOW", 1L, 100L, "", "", "", "", "", "", null, null, stageData, new ArrayList<>()));
        org.elasticsearch.search.SearchHit hit = new org.elasticsearch.search.SearchHit(
                1
        );
        BytesReference source = new BytesArray(epJson);
        hit.sourceRef(source);
        org.elasticsearch.search.SearchHit[] hitList = new org.elasticsearch.search.SearchHit[]{
                hit
        };
        org.elasticsearch.search.SearchHits hits = new org.elasticsearch.search.SearchHits(hitList, new TotalHits(1, TotalHits.Relation.EQUAL_TO), 10f);
        SearchResponse searchResponse = new SearchResponse(
                new InternalSearchResponse(hits, null, null, null, false, false, 1), "1", 1, 1, 1, 1L, null, null
        );
        EpisodeSearchResult searchResult = esEpisodeService.collectDataFromSearchResponse(searchResponse);
        Assert.assertEquals(searchResult.getTotalHits(), 1);
        Assert.assertEquals(searchResult.getScrollId(), "1");
    } 

    @Test
    public void findAllByPersonIdListTest() {
        Long clientId = 1L;
        List<Long> personIdList = new ArrayList<>();
        personIdList.add(1L);
        List<EpisodeIndex> episodeIndexList = new ArrayList<>();
        EpisodeIndex episodeIndex = new EpisodeIndex();
        episodeIndex.setClientId(1L);
        episodeIndexList.add(episodeIndex);
        when(esEpisodeRepository.findAllByPersonIdInAndClientId(any(), any())).thenReturn(episodeIndexList);
        List<EpisodeIndex> episodeIndexListResult = esEpisodeService.findAllByPersonIdList(personIdList, clientId);
        Assertions.assertArrayEquals(episodeIndexList.toArray(), episodeIndexListResult.toArray());
    }

}
