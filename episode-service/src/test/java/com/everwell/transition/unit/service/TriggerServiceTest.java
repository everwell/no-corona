package com.everwell.transition.unit.service;

import com.everwell.transition.BaseTest;
import com.everwell.transition.constants.Constants;
import com.everwell.transition.model.db.Trigger;
import com.everwell.transition.repositories.TriggerRepository;
import com.everwell.transition.service.NotificationService;
import com.everwell.transition.service.impl.NotificationServiceImpl;
import com.everwell.transition.service.impl.TriggerServiceImpl;
import org.jobrunr.jobs.lambdas.JobLambda;
import org.jobrunr.scheduling.JobScheduler;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;

import java.util.Collections;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

public class TriggerServiceTest extends BaseTest {

    @Spy
    @InjectMocks
    TriggerServiceImpl triggerService;

    @Mock
    NotificationService notificationService;

    @Mock
    JobScheduler jobScheduler;

    @Mock
    TriggerRepository triggerRepository;

    @Test
    public void readTriggerTableTest() throws IllegalAccessException {
        Trigger trigger = new Trigger(1L, null, 29L, 1L, 1L, "1", "getPatientsWhoHaveNotMarkedAdherenceForToday", "getPatientsWhoHaveNotMarkedAdherenceForToday", "10 * * * * *", 1L, Constants.PUSH_NOTIFICATION, "Asia/Kolkata");
        when(triggerRepository.getAllByCronTimeNotNull()).thenReturn(Collections.singletonList(trigger));
        doNothing().when(jobScheduler).delete(eq(""));
        when(jobScheduler.scheduleRecurrently(anyString(), anyString(), (JobLambda) any())).thenReturn("test");
        doNothing().when(notificationService).sendPushNotificationGeneric(any(), eq(171L), eq("sample"));
        triggerService.readTriggerTable();
        verify(jobScheduler, Mockito.times(0)).delete(eq(""));
    }

    @Test
    public void readTriggerTableTest2() throws IllegalAccessException {
        Trigger trigger = new Trigger(1L, null, 29L, 1L, 1L, "1", "getPatientsWhoHaveNotMarkedAdherenceForToday", "getPatientsWhoHaveNotMarkedAdherenceForToday", "10 * * * * *", 1L, "test", "Asia/Kolkata");
        when(triggerRepository.getAllByCronTimeNotNull()).thenReturn(Collections.singletonList(trigger));
        doNothing().when(jobScheduler).delete(eq(""));
        when(jobScheduler.scheduleRecurrently(anyString(), anyString(), (JobLambda) any())).thenReturn("test");
        doNothing().when(notificationService).sendPushNotificationGeneric(any(), eq(171L), eq("sample"));
        triggerService.readTriggerTable();
        verify(jobScheduler, Mockito.times(0)).delete(eq(""));
    }
}
