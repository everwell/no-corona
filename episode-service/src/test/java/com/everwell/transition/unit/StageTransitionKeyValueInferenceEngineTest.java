package com.everwell.transition.unit;

import com.everwell.transition.BaseTest;
import com.everwell.transition.model.Rule;
import com.everwell.transition.model.RuleNamespace;
import com.everwell.transition.model.db.Rules;
import com.everwell.transition.repositories.RulesRepository;
import com.everwell.transition.service.AggregationService;
import com.everwell.transition.service.impl.StageTransitionKeyValueInferenceEngine;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import javax.validation.constraints.AssertTrue;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

public class StageTransitionKeyValueInferenceEngineTest extends BaseTest {

    @InjectMocks
    StageTransitionKeyValueInferenceEngine stageTransitionKeyValueInferenceEngine;

    @Mock
    RulesRepository rulesRepository;

    @Mock
    AggregationService aggregationService;

    private static final Long clientId = 1L;

    @Test
    public void getRuleNameSpace_correctName() {
        Assert.assertEquals(
            stageTransitionKeyValueInferenceEngine.getRuleNamespace(),
            RuleNamespace.STAGE_TRANSITION.name()
        );
    }

    @Test
    public void getRuleNameSpace() {
        when(rulesRepository.findAllByRuleNamespaceAndClientId(RuleNamespace.STAGE_TRANSITION.name(), clientId)).thenReturn(getDbRules());

        List<Rule> rules = stageTransitionKeyValueInferenceEngine.getRules(null, true, clientId);

        rules.forEach(r -> {
            Assert.assertEquals(r.getRuleNamespace(), RuleNamespace.STAGE_TRANSITION);
        });

    }

    List<Rules> getDbRules () {
        List<Rules> rules = Arrays.asList(
            new Rules(1L, RuleNamespace.STAGE_TRANSITION.name(), "var stage = input.get(\"Stage\");var outcome = input.get(\"TreatmentOutcome\");return (stage == null || stage.equals('') || stage.equals('PRESUMPTIVE_OPEN')) && (null == outcome || outcome.equals(''));", "output.put('ToStage', 'PRESUMPTIVE_OPEN');", 1, "TB Stage Transitions to PRESUMPTIVE_OPEN", 1L, 2L, clientId),
            new Rules(1L, RuleNamespace.STAGE_TRANSITION.name(), "var stage = input.get(\"Stage\");var outcome = input.get(\"TreatmentOutcome\");return stage != null && !stage.equals('PRESUMPTIVE_OPEN') && !stage.equals('DIAGNOSED_NOT_ON_TREATMENT') && outcome != null && !outcome.equals('');", "output.put('ToStage', 'DIAGNOSED_OUTCOME_ASSIGNED');", 1, "TB Stage Transitions to DIAGNOSED_OUTCOME_ASSIGNED", 1L, 2L, clientId)
        );
        return rules;
    }

    @Test
    public void test_resolveMissingData_correctness() {
        List<Rule> rules = getDbRules().stream().map(r -> new Rule(
                RuleNamespace.valueOf(r.getRuleNamespace()),
                r.getId(), r.getCondition(), r.getAction(), r.getPriority(), r.getDescription())).collect(Collectors.toList());
        Map<String,Object> aggregationServiceResponse = new HashMap<>();
        aggregationServiceResponse.put("TreatmentOutcome", null);
        when(aggregationService.resolveFields(any(), eq(1L))).thenReturn(aggregationServiceResponse);
        Map<String, Object> response = stageTransitionKeyValueInferenceEngine.resolveMissingData(getInputObject(), rules);
        Assert.assertTrue(response.containsKey("TreatmentOutcome"));
    }

    Map<String, Object> getInputObject() {
        Map<String, Object> input = new HashMap<>();
        input.put("Stage", "Presumptive_Open");
        input.put("EntityId", 1L);
        return input;
    }


}
