package com.everwell.transition.unit.controller;

import com.everwell.transition.BaseTest;
import com.everwell.transition.constants.Constants;
import com.everwell.transition.controller.EpisodeController;
import com.everwell.transition.elasticsearch.indices.EpisodeIndex;
import com.everwell.transition.enums.episode.EpisodeValidation;
import com.everwell.transition.exceptions.CustomExceptionHandler;
import com.everwell.transition.model.db.Client;
import com.everwell.transition.model.db.Episode;
import com.everwell.transition.model.dto.BeneficiaryDetailsDto;
import com.everwell.transition.model.dto.EpisodeDto;
import com.everwell.transition.model.dto.notification.EmptyBankDetailsDto;
import com.everwell.transition.model.dto.notification.UserSmsDetails;
import com.everwell.transition.model.dto.EpisodeSearchResult;
import com.everwell.transition.model.dto.EpisodeUnifiedFilters;
import com.everwell.transition.model.dto.EpisodeUnifiedSearchRequest;
import com.everwell.transition.model.db.EpisodeDocument;
import com.everwell.transition.model.dto.*;
import com.everwell.transition.model.request.AddEpisodeDocumentRequest;
import com.everwell.transition.model.request.EpisodeDocumentSearchRequest;
import com.everwell.transition.model.request.episode.DeleteRequest;
import com.everwell.transition.model.request.episode.EpisodeBulkRequest;
import com.everwell.transition.model.request.episode.SearchEpisodesRequest;
import com.everwell.transition.model.request.episode.UpdateRiskRequest;
import com.everwell.transition.model.request.episodesearch.EpisodeHeaderSearchRequest;
import com.everwell.transition.model.request.episodesearch.EpisodeSearchRequest;
import com.everwell.transition.model.request.episodesearch.ExtraFilterRequest;
import com.everwell.transition.model.request.tasklistfilters.EpisodeTasklistRequest;
import com.everwell.transition.model.request.tasklistfilters.TaskListFilterRequest;
import com.everwell.transition.service.EpisodeService;
import com.everwell.transition.service.impl.ClientServiceImpl;
import com.everwell.transition.utils.Utils;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class EpisodeControllerTest extends BaseTest {

    private MockMvc mockMvc;

    @Mock
    private EpisodeService episodeService;

    @InjectMocks
    private EpisodeController episodeController;

    @Mock
    ApplicationEventPublisher applicationEventPublisher;

    @Mock
    ClientServiceImpl clientService;

    private final String REPORTS_FILTER_ENDPOINT = "/v1/episode/reports";

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(episodeController)
                .setControllerAdvice(new CustomExceptionHandler())
                .build();
    }

    @Test
    public void testGetEpisodeSuccess() throws Exception {
        Long episodeId = 1L;
        Long clientId = 1L;
        String uri = "/v1/episode/" + episodeId;
        when(episodeService.getEpisodeDetails(eq(episodeId), eq(clientId))).thenReturn(episodeDetailsBasic());
        mockMvc
                .perform(MockMvcRequestBuilders
                        .get(uri)
                        .header(Constants.X_CLIENT_ID, clientId)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("true"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.id").value(episodeDetailsBasic().getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.personId").value(episodeDetailsBasic().getPersonId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.disease").value(episodeDetailsBasic().getDisease()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.currentStageId").value(episodeDetailsBasic().getCurrentStageId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.typeOfEpisode").value(episodeDetailsBasic().getTypeOfEpisode()));

    }

    @Test
    public void testGetEpisode_throws_validationException_forNullEpisode() throws Exception {
        Long episodeId = null;
        Long clientId = 1L;
        String uri = "/v1/episode/" + episodeId;
        when(episodeService.getEpisodeDetails(eq(episodeId), eq(clientId) )).thenReturn(null);

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get(uri)
                        .header(Constants.X_CLIENT_ID, clientId)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testCreateEpisode_success() throws Exception {
        long clientId = 1L;
        String uri = "/v1/episode";
        when(episodeService.processCreateEpisode(any(), any())).thenReturn(getEpisodeData());

        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .header(Constants.X_CLIENT_ID, clientId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(Utils.asJsonString(createEpisodeRequestObject()))
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("true"));
    }

    @Test
    public void testUpdateEpisode_success() throws Exception {
        long clientId = 1L;
        String uri = "/v1/episode";
        when(episodeService.processUpdateEpisode(any(), any())).thenReturn(getEpisodeData());

        mockMvc
                .perform(MockMvcRequestBuilders
                        .put(uri)
                        .header(Constants.X_CLIENT_ID, clientId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(Utils.asJsonString(updateEpisodeRequestObject()))
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("true"));
    }

    private Episode getEpisodeData() {
        return new Episode(1L, 1L, "1", null, Utils.getCurrentDateNew(), null, 1L );
    }

    private Map<String, Object> createEpisodeRequestObject() {
        Map<String, Object> map = new HashMap<>();
        map.put("personId", 1L);
        map.put("addedBy", 1L);
        map.put("diseaseId", 1L);
        return map;
    }

    private Map<String, Object> updateEpisodeRequestObject() {
        Map<String, Object> map = new HashMap<>();
        map.put("episodeId", 1L);
        map.put("personId", 1L);
        map.put("addedBy", 1L);
        map.put("diseaseId", 1L);
        return map;
    }

    private EpisodeDto episodeDetailsBasic() {
        return new EpisodeDto(1L, 1L, 1L, true, "DSTB", 1L, null, Utils.getCurrentDateNew(), Utils.getCurrentDateNew(), null, Utils.getCurrentDateNew(), null, null, 1L,"PRESUMPTIVE_OPEN", "Presumptive Open", "IndiaTbPublic", 1L, null, null, null, null, null);
    }

    @Test
    public void testGetEpisodesForPerson_throws_validationException_forNullPersonId() throws Exception {
        Long personId = null;
        Long clientId = 1L;
        Boolean includeDetails = true;
        String uri = "/v1/episodes/personId/" + personId + "?includeDetails=" + includeDetails;
        when(episodeService.getEpisodesForPersonList(eq(new ArrayList<>()), eq(clientId), eq(includeDetails) )).thenReturn(null);

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get(uri)
                        .header(Constants.X_CLIENT_ID, clientId)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testGetEpisodesForPersonSuccess() throws Exception {
        Long personId = 1L;
        Long clientId = 1L;
        List<Long> personIdList = new ArrayList<>();
        personIdList.add(personId);
        Boolean includeDetails = true;
        String uri = "/v1/episodes/personId/" + personId + "?includeDetails=" + includeDetails;
        when(episodeService.getEpisodesForPersonList(eq(personIdList), eq(clientId), eq(includeDetails) )).thenReturn(episodeDtoList_countTwo());

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get(uri)
                        .header(Constants.X_CLIENT_ID, clientId)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("true"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data[0].id").value(episodeDtoList_countTwo().get(0).getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data[0].personId").value(episodeDtoList_countTwo().get(0).getPersonId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data[0].disease").value(episodeDtoList_countTwo().get(0).getDisease()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data[0].currentStageId").value(episodeDtoList_countTwo().get(0).getCurrentStageId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data[0].typeOfEpisode").value(episodeDtoList_countTwo().get(0).getTypeOfEpisode()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data[1].id").value(episodeDtoList_countTwo().get(1).getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data[1].personId").value(episodeDtoList_countTwo().get(1).getPersonId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data[1].disease").value(episodeDtoList_countTwo().get(1).getDisease()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data[1].currentStageId").value(episodeDtoList_countTwo().get(1).getCurrentStageId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data[1].typeOfEpisode").value(episodeDtoList_countTwo().get(1).getTypeOfEpisode()));

    }

    @Test
    public void episodeSearch_validationEx() throws Exception {
        String uri = "/v2/episode/search";
        EpisodeSearchRequest episodeSearchRequest = new EpisodeSearchRequest(null, null);
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(episodeSearchRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("false"));
    }

    @Test
    public void episodeSearch_2_validationEx() throws Exception {
        String uri = "/v2/episode/search";
        EpisodeSearchRequest episodeSearchRequest = new EpisodeSearchRequest(null, null);
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(episodeSearchRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("false"));
    }

    @Test
    public void episodeSearch_2_success() throws Exception {
        String uri = "/v2/episode/search";
        EpisodeSearchRequest episodeSearchRequest = new EpisodeSearchRequest();
        when(clientService.getClientByTokenOrDefault()).thenReturn(new Client(1L, "client", "client", new Date()));
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(episodeSearchRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("true"));
    }

    @Test
    public void episodeSearch_2_filter_success() throws Exception {
        String uri = "/v2/episode/search";
        when(clientService.getClientByTokenOrDefault()).thenReturn(new Client(1L, "client", "client", new Date()));
        EpisodeSearchRequest episodeSearchRequest = new EpisodeSearchRequest();
        ExtraFilterRequest filterRequest = new ExtraFilterRequest();
        filterRequest.setStageKey(Collections.singletonList("ON_TREATMENT"));
        filterRequest.setDoseNotReportedToday(true);
        filterRequest.setEndDateBefore("2022-10-10 18:30:00");
        filterRequest.setRegistrationDateRange("{\"lowerBound\":14,\"upperBound\":20}");
        filterRequest.setForegoBenefits(true);
        filterRequest.setTypeOfPatient("IndiaTbPublic");
        filterRequest.setMonitoringMethodNew(Collections.singletonList("99DOTS"));
        filterRequest.setDisease("DSTB");
        episodeSearchRequest.setFilters(filterRequest);
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(episodeSearchRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("true"));
    }

    @Test
    public void episodeSearchBulk_2_success() throws Exception {
        String uri = "/v2/episode/search/bulk";
        when(clientService.getClientByTokenOrDefault()).thenReturn(new Client(1L, "client", "client", new Date()));
        EpisodeSearchRequest episodeSearchRequest = new EpisodeSearchRequest();
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(Collections.singletonList(episodeSearchRequest)))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("true"));
    }

    @Test
    public void episodeSearchBulkFilter_2_success() throws Exception {
        String uri = "/v2/episode/search/bulk";
        when(clientService.getClientByTokenOrDefault()).thenReturn(new Client(1L, "client", "client", new Date()));
        EpisodeSearchRequest episodeSearchRequest = new EpisodeSearchRequest();
        ExtraFilterRequest filterRequest = new ExtraFilterRequest();
        episodeSearchRequest.setFilters(filterRequest);
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(Collections.singletonList(episodeSearchRequest)))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("true"));
    }

    @Test
    public void episodeHeaderSearch_validationEx() throws Exception {
        String uri = "/v1/episode/header";
        EpisodeHeaderSearchRequest episodeHeaderSearchRequest = new EpisodeHeaderSearchRequest(null, null, null);
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(episodeHeaderSearchRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("false"));
    }

    @Test
    public void episodeHeaderSearch_validationEx_ArrayWithNull() throws Exception {
        String uri = "/v1/episode/header";
        List<Long> list = new ArrayList<>();
        list.add(null); // sometimes client is sending array with null value
        EpisodeHeaderSearchRequest episodeHeaderSearchRequest = new EpisodeHeaderSearchRequest(null, null, list);
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(episodeHeaderSearchRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("false"));
    }

    @Test
    public void episodeHeaderSearch_2_validationEx() throws Exception {
        String uri = "/v1/episode/header";
        EpisodeHeaderSearchRequest episodeHeaderSearchRequest = new EpisodeHeaderSearchRequest("name", null, null);
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(episodeHeaderSearchRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("false"));
    }

    @Test
    public void episodeHeaderSearch_2_success() throws Exception {
        String uri = "/v1/episode/header";
        EpisodeHeaderSearchRequest episodeHeaderSearchRequest = new EpisodeHeaderSearchRequest("name", "key", null);
        when(clientService.getClientByTokenOrDefault()).thenReturn(new Client(1L, "client", "client", new Date()));
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(episodeHeaderSearchRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("true"));
    }

    @Test
    public void episodeHeaderSearch_EpisodeSearchRequest() throws Exception {
        String uri = "/v1/episode/header";
        EpisodeHeaderSearchRequest episodeHeaderSearchRequest = new EpisodeHeaderSearchRequest("phone", "key", null);
        episodeHeaderSearchRequest.setHierarchyIds(Arrays.asList(1L));
        episodeHeaderSearchRequest.setTypeOfEpisode(Arrays.asList("IndiaTbPublic"));
        when(clientService.getClientByTokenOrDefault()).thenReturn(new Client(1L, "client", "client", new Date()));
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(episodeHeaderSearchRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("true"));
    }

    @Test
    public void episodeHeaderSearch_EpisodeSearchRequest_99DotsId() throws Exception {
        String uri = "/v1/episode/header";
        EpisodeHeaderSearchRequest episodeHeaderSearchRequest = new EpisodeHeaderSearchRequest("99dotsId", "key", null);
        episodeHeaderSearchRequest.setHierarchyIds(Arrays.asList(1L));
        episodeHeaderSearchRequest.setTypeOfEpisode(Arrays.asList("IndiaTbPublic"));
        when(clientService.getClientByTokenOrDefault()).thenReturn(new Client(1L, "client", "client", new Date()));
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(episodeHeaderSearchRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("true"));
    }

    @Test
    public void episodeHeaderSearch_EpisodeSearchRequest_Custom() throws Exception {
        String uri = "/v1/episode/header";
        EpisodeHeaderSearchRequest episodeHeaderSearchRequest = new EpisodeHeaderSearchRequest("key", "value", null);
        episodeHeaderSearchRequest.setHierarchyIds(Arrays.asList(1L));
        episodeHeaderSearchRequest.setTypeOfEpisode(Arrays.asList("IndiaTbPublic"));
        when(clientService.getClientByTokenOrDefault()).thenReturn(new Client(1L, "client", "client", new Date()));
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(episodeHeaderSearchRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("true"));
    }

    @Test
    public void testUpdateEpisodeStageAndDetails() throws Exception {
        Long clientId = 1L;
        String uri = "/v1/episode/stage";
        Episode episode = new Episode();
        when(episodeService.processEpisodeRequestInputForStageTransition(any(), any())).thenReturn(episode);

        mockMvc
                .perform(MockMvcRequestBuilders
                        .put(uri)
                        .header(Constants.X_CLIENT_ID, clientId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(Utils.asJsonString(updateEpisodeRequestObject()))
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());

    }

    @Test
    public void fetchPossibleDuplicatesV2Test() throws Exception {
        Long clientId = 1L;
        String uri = "/v1/episode/duplicates";
        Map<String, Object> fetchDuplicateMap = new HashMap<>();
        fetchDuplicateMap.put("scheme", "PRIMARYPHONE_GENDER");
        fetchDuplicateMap.put("primaryPhoneNumber", "9988799887" );
        fetchDuplicateMap.put("gender", "Male");

        when(episodeService.getDuplicateEpisodesForUser(any(), any())).thenReturn(new ArrayList<>());

        mockMvc
                .perform(MockMvcRequestBuilders.post(uri)
                        .header(Constants.X_CLIENT_ID, clientId)
                        .content(Utils.asJsonString(fetchDuplicateMap))
                        .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
    }


    @Test
    public void testFilterEpisodeSuccess() throws Exception {

        SearchEpisodesRequest searchEpisodesRequest = new SearchEpisodesRequest(Collections.singletonList(2L),"01-01-1999","02-01-1999",true,new ArrayList<>(),"current",new ArrayList<>());
        Long clientId = 1L;
        String uri = "/v1/episode/search" ;
        when(episodeService.search(searchEpisodesRequest)).thenReturn(episodeDtoList_countTwo().stream().map(EpisodeDto :: getId).collect(Collectors.toList()));

        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .header(Constants.X_CLIENT_ID, clientId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(Utils.asJsonString(searchEpisodesRequest))
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("true"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data[0]").value(episodeDtoList_countTwo().get(0).getId()));
    }

    @Test
    public void testFilterEpisode_throws_validationException_forNullTreatmentStartDate() throws Exception {

        SearchEpisodesRequest searchEpisodesRequest = new SearchEpisodesRequest(Collections.singletonList(2L),null,null,true,new ArrayList<>(),"current",new ArrayList<>());
        Long clientId = 1L;
        String uri = "/v1/episode/risk" ;
        when(episodeService.search(searchEpisodesRequest)).thenReturn(null);

        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .header(Constants.X_CLIENT_ID, clientId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(Utils.asJsonString(searchEpisodesRequest))
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testFilterEpisode_throws_validationException_forNullHierararchyType() throws Exception {

        SearchEpisodesRequest searchEpisodesRequest = new SearchEpisodesRequest(Collections.singletonList(2L),"01-01-1999","02-01-1999",true,new ArrayList<>(),null,new ArrayList<>());
        Long clientId = 1L;
        String uri = "/v1/episode/risk" ;
        when(episodeService.search(searchEpisodesRequest)).thenReturn(null);

        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .header(Constants.X_CLIENT_ID, clientId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(Utils.asJsonString(searchEpisodesRequest))
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testUpdateEpisodeRiskSuccess() throws Exception {

        UpdateRiskRequest updateRiskRequest = new UpdateRiskRequest(Collections.singletonList(2L),Collections.singletonList(2L),2L);
        Long clientId = 1L;
        String uri = "/v1/episode/risk" ;
        doNothing().when(applicationEventPublisher).publishEvent(any());
        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .header(Constants.X_CLIENT_ID, clientId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(Utils.asJsonString(updateRiskRequest))
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("true"));
    }

    @Test
    public void testBulkEpisode_throws_validationException_forEmptyIdList() throws Exception {
        EpisodeBulkRequest episodeBulkRequest = new EpisodeBulkRequest(new ArrayList<>());
        Long clientId = 1L;
        String uri = "/v1/episode/bulk" ;
        when(episodeService.getEpisodesBulk(episodeBulkRequest)).thenReturn(null);

        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .header(Constants.X_CLIENT_ID, clientId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(Utils.asJsonString(episodeBulkRequest))
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testTagList_throws_validationException_forEmptyIdList() throws Exception {
        EpisodeTasklistRequest episodeTasklistRequest = new EpisodeTasklistRequest(new EpisodeSearchRequest(), new ArrayList<>());
        String uri = "/v1/episode/tasklist" ;
        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(Utils.asJsonString(episodeTasklistRequest))
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testTagList_success() throws Exception {
        EpisodeTasklistRequest episodeTasklistRequest = new EpisodeTasklistRequest(new EpisodeSearchRequest(), Collections.singletonList(new TaskListFilterRequest("test", "test", 1L)));
        EpisodeSearchResult episodeSearchResult = new EpisodeSearchResult(10L, null, null);
        String uri = "/v1/episode/tasklist" ;

        when(episodeService.taskListSearch(any())).thenReturn(episodeSearchResult);
        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(Utils.asJsonString(episodeTasklistRequest))
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.totalHits").value(10L));
    }

    @Test
    public void testUpp_throws_validationException_forEmptyIdList() throws Exception {
        EpisodeUnifiedSearchRequest episodeUnifiedSearchRequest = new EpisodeUnifiedSearchRequest();
        String uri = "/v1/episode/upp" ;
        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(Utils.asJsonString(episodeUnifiedSearchRequest))
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testUpp_success() throws Exception {
        EpisodeUnifiedSearchRequest episodeUnifiedSearchRequest = new EpisodeUnifiedSearchRequest(new EpisodeSearchRequest(), new EpisodeUnifiedFilters());
        EpisodeSearchResult episodeSearchResult = new EpisodeSearchResult(10L, null, null);
        String uri = "/v1/episode/upp" ;

        when(episodeService.episodeUnifiedSearch(any())).thenReturn(episodeSearchResult);
        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(Utils.asJsonString(episodeUnifiedSearchRequest))
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.totalHits").value(10L));
    }

    @Test
    public void testEpisodeBulkSuccess() throws Exception {

        EpisodeBulkRequest episodeBulkRequest = new EpisodeBulkRequest(Collections.singletonList(2L));
        Long clientId = 1L;
        String uri = "/v1/episode/bulk" ;
        when(episodeService.getEpisodesBulk(episodeBulkRequest)).thenReturn(episodeDtoList_countTwo());

        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .header(Constants.X_CLIENT_ID, clientId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(Utils.asJsonString(episodeBulkRequest))
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("true"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data[0].id").value(episodeDtoList_countTwo().get(0).getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data[0].personId").value(episodeDtoList_countTwo().get(0).getPersonId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data[0].disease").value(episodeDtoList_countTwo().get(0).getDisease()));
    }

    @Test
    public void testGetEpisodesForEmptyBankDetails() throws Exception {
        String uri = "/v1/episode/empty-bank";
        EpisodeSearchRequest episodeSearchRequest = new EpisodeSearchRequest();
        when(episodeService.getEpisodesForEmptyBankDetails(any())).thenReturn(new EmptyBankDetailsDto());

        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(Utils.asJsonString(episodeSearchRequest))
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value(true));
    }

    @Test
    public void testGetEpisodesForBenefitsCreatedSuccess() throws Exception {
        String uri = "/v1/episode/benefits";
        EpisodeSearchRequest episodeSearchRequest = new EpisodeSearchRequest();
        when(episodeService.getEpisodesForBenefitCreated(any())).thenReturn(Collections.singletonList(new BeneficiaryDetailsDto()));

        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(Utils.asJsonString(episodeSearchRequest))
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("true"));
    }


    @Test
    public void testGetEpisodesForInvalidBankDetailsSuccess() throws Exception {
        String uri = "/v1/episode/invalid-bank";
        EpisodeSearchRequest episodeSearchRequest = new EpisodeSearchRequest();
        when(episodeService.getEpisodesForInvalidBankDetails(any())).thenReturn(Collections.singletonList(new UserSmsDetails()));

        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(Utils.asJsonString(episodeSearchRequest))
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("true"));
    }


    private List<EpisodeDto> episodeDtoList_countTwo() {
        EpisodeDto episode1 =  new EpisodeDto(1L, 1L, 1L, true, "DSTB", 1L, null, Utils.getCurrentDateNew(), Utils.getCurrentDateNew(), null, Utils.getCurrentDateNew(), null, null, 1L,"PRESUMPTIVE_OPEN", "Presumptive Open", "IndiaTbPublic", 1L, null, null, null, null, null);
        EpisodeDto episode2 =  new EpisodeDto(2L, 1L, 1L, true, "DRTB", 2L, null, Utils.getCurrentDateNew(), Utils.getCurrentDateNew(), null, Utils.getCurrentDateNew(), null, null, 2L,"PRESUMPTIVE_CLOSED", "Presumptive Closed", "IndiaTbPublic", 1L, null, null, null, null, null);
        return Arrays.asList(episode1, episode2);
    }

    @Test
    public void checkIfUserExistsTest() throws Exception {
        String phoneNumber = "9999999999";
        String uri = "/v1/episode/phone/" + phoneNumber;
        Episode episode = new Episode(1L, 1L, 1L, false, null, "1", Utils.getCurrentDateNew(), Utils.getCurrentDateNew(),
                null, Utils.getCurrentDateNew(), null, 1L, null, 1L);
        List<Episode> episodeIdList = new ArrayList<>();
        episodeIdList.add(episode);
        when(episodeService.getEpisodeByPhoneNumber(eq(phoneNumber))).thenReturn(episodeIdList);

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                                .andExpect(MockMvcResultMatchers.jsonPath("$.data.[0].id").value(1L));
    }

    @Test
    public void uploadEpisodeDocumentTestSuccess() throws Exception {
        String type = "type";
        String dateOfRecord = "10-09-2022";
        AddEpisodeDocumentRequest request = new AddEpisodeDocumentRequest(1L, 1L, Collections.singletonList(type), "test", "1111111111", Collections.singletonList(dateOfRecord), "notes", "Asia/Kolkata", "19-10-2022", null, null);
        doNothing().when(episodeService).uploadEpisodeDocument(any(), any());
        ObjectMapper objectMapper = new ObjectMapper();
        MockMultipartFile file
                = new MockMultipartFile(
                "file",
                "hello.pdf",
                MediaType.TEXT_PLAIN_VALUE,
                "Hello, World!".getBytes()
        );
        MockMultipartFile addEpisodeDocument =
                new MockMultipartFile(
                        "addEpisodeDocument",
                        "request",
                        MediaType.APPLICATION_JSON_VALUE,
                        objectMapper.writeValueAsString(request).getBytes(StandardCharsets.UTF_8)
                );

        mockMvc.perform(multipart("/v1/episode/document").file(file)
                        .file(addEpisodeDocument)
                        .accept(MediaType.APPLICATION_JSON))
                        .andExpect(status().isOk());
    }

    @Test
    public void getPersonDocumentDetailsTest() throws Exception {
        long personId = 1L;
        String type = "type";
        String dateOfRecord = "2022-10-09 18:30:00";
        String uri= "/v1/episode/document?personId=" + personId;
        AddEpisodeDocumentRequest request = new AddEpisodeDocumentRequest(1L, 1L, Collections.singletonList(type), "test", "1111111111", Collections.singletonList(dateOfRecord), "notes", "Asia/Kolkata", "19-10-2022", null, null);
        EpisodeDocument episodeDocument = new EpisodeDocument(request);
        EpisodeDocumentResponseDto episodeDocumentResponseDto = new EpisodeDocumentResponseDto(1L, Collections.singletonList(episodeDocument));
        when(episodeService.getPersonDocumentDetails(eq(1L), eq(false))).thenReturn(Collections.singletonList(episodeDocumentResponseDto));

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.[0].episodeId").value(1L));
    }

    @Test
    public void getEpisodeDocumentDetailsTest_throwsValidationException() throws Exception {
        long personId = 1L;
        String type = "type";
        String dateOfRecord = "2022-10-09 18:30:00";
        String uri= "/v1/episode/document";
        AddEpisodeDocumentRequest request = new AddEpisodeDocumentRequest(1L, 1L, Collections.singletonList(type), "test", "1111111111", Collections.singletonList(dateOfRecord), "notes", "Asia/Kolkata", "19-10-2022", null, null);
        EpisodeDocument episodeDocument = new EpisodeDocument(request);
        EpisodeDocumentResponseDto episodeDocumentResponseDto = new EpisodeDocumentResponseDto(1L, Collections.singletonList(episodeDocument));
        when(episodeService.getPersonDocumentDetails(eq(1L), eq(false))).thenReturn(Collections.singletonList(episodeDocumentResponseDto));

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void getEpisodeDocumentDetailsByEpisodeDocumentDetailsIdTest() throws Exception {
        long episodeDocumentDetailsId = 1L;
        String type = "type";
        String dateOfRecord = "2022-10-09 18:30:00";
        String uri= "/v1/episode/document?episodeDocumentDetailsId=" + episodeDocumentDetailsId;
        AddEpisodeDocumentRequest request = new AddEpisodeDocumentRequest(1L, 1L, Collections.singletonList(type), "test", "1111111111", Collections.singletonList(dateOfRecord), "notes", "Asia/Kolkata", "19-10-2022", null, null);
        EpisodeDocument episodeDocument = new EpisodeDocument(request);
        EpisodeDocumentResponseDto episodeDocumentResponseDto = new EpisodeDocumentResponseDto(1L, Collections.singletonList(episodeDocument));
        when(episodeService.getEpisodeDocumentDetailsByEpisodeDocumentDetailsId(eq(1L))).thenReturn(Collections.singletonList(episodeDocumentResponseDto));

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.[0].episodeId").value(1L));
    }

    @Test
    public void deleteEpisode() throws Exception
    {
        Long episodeId = 1L;
        Long clientId = 1L;
        String uri = "/v1/episode/delete";
        Client testClient = new Client(clientId,"test","test",new Date(),1L, "test");
        when(clientService.getClientByTokenOrDefault()).thenReturn(testClient);

        DeleteRequest deleteRequest = new DeleteRequest(episodeId, 1L, "reason", "notes");

        when(episodeService.deleteEpisode(eq(clientId), any(DeleteRequest.class))).thenReturn(new Episode());

        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(Utils.asJsonString(deleteRequest)))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("true"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data").value("Episode deleted successfully"));
    }

    @Test
    public void getEpisodeDocumentDetailsTest() throws Exception {
        long episodeId = 1L;
        String type = "type";
        String dateOfRecord = "2022-10-09 18:30:00";
        String uri= "/v1/episode/search/document";
        EpisodeDocumentSearchRequest searchRequest = new EpisodeDocumentSearchRequest(Collections.singletonList(episodeId), "2022-10-10 18:30:00", "2022-11-10 18:30:00", true);
        AddEpisodeDocumentRequest request = new AddEpisodeDocumentRequest(1L, 1L, Collections.singletonList(type), "test", "1111111111", Collections.singletonList(dateOfRecord), "notes", "Asia/Kolkata", "19-10-2022", 7945L, "Staff");
        EpisodeDocument episodeDocument = new EpisodeDocument(request);
        EpisodeDocumentResponseDto episodeDocumentResponseDto = new EpisodeDocumentResponseDto(1L, Collections.singletonList(episodeDocument));
        when(episodeService.getEpisodeDocumentDetails(any())).thenReturn(Collections.singletonList(episodeDocumentResponseDto));

        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
                        .content(Utils.asJsonString(searchRequest)))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.[0].episodeId").value(1L))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.[0].episodeDocumentList[0].addedById").value(7945L));
    }

    @Test
    public void getEpisodeDocumentDetailsTest_throwsValidationException_forEmptyList() throws Exception {
        String uri= "/v1/episode/search/document";
        EpisodeDocumentSearchRequest searchRequest = new EpisodeDocumentSearchRequest(Collections.EMPTY_LIST, null, null, null);

        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
                        .content(Utils.asJsonString(searchRequest)))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void getEpisodeDocumentDetailsTest_throwsValidationException_forNull() throws Exception {
        String uri= "/v1/episode/search/document";
        EpisodeDocumentSearchRequest searchRequest = new EpisodeDocumentSearchRequest(null, null, null, null);

        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
                        .content(Utils.asJsonString(searchRequest)))
                .andExpect(status().isBadRequest());
    }


    @Test
    public void testEpisodeReportsFilters() throws Exception {
        EpisodeReportsRequest episodeReportsRequest = new EpisodeReportsRequest(new EpisodeSearchRequest(), new EpisodeReportsFilter());
        episodeReportsRequest.getEpisodeReportsFilter().setHierarchyIds(Arrays.asList(1L, 2L));
        EpisodeIndex episodeIndex = new EpisodeIndex();
        episodeIndex.setId("1");
        EpisodeSearchResult episodeSearchResult = new EpisodeSearchResult(1L, Collections.singletonList(episodeIndex), null, "", "");
        when(episodeService.episodeReports(any())).thenReturn(episodeSearchResult);
        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(REPORTS_FILTER_ENDPOINT)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(Utils.asJsonString(episodeReportsRequest)))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("true"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.totalHits").value("1"));
    }

    @Test
    public void testEpisodeReportsFiltersWithException() throws Exception {
        EpisodeReportsRequest episodeReportsRequest = new EpisodeReportsRequest(new EpisodeSearchRequest(), null);
        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(REPORTS_FILTER_ENDPOINT)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(Utils.asJsonString(episodeReportsRequest)))
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("false"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(EpisodeValidation.INVALID_EPISODE_REPORTS_FILTER.getMessage()));
    }

    @Test
    public void updateDocumentStatus() throws Exception {
        String uri = "/v1/episode/document/1?approved=true&validatedBy=7945";

        doNothing().when(episodeService).updateEpisodeDocumentDetails(eq(1L), eq(true), eq(7945L));
        mockMvc
                .perform(MockMvcRequestBuilders
                        .put(uri)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());
    }

    @Test
    public void updateDocumentStatusMissingApproved() throws Exception {
        String uri = "/v1/episode/document/1?validatedBy=7945";

        mockMvc
                .perform(MockMvcRequestBuilders
                        .put(uri)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void updateDocumentStatusMissingValidatedBy() throws Exception {
        String uri = "/v1/episode/document/1?approved=true";

        mockMvc
                .perform(MockMvcRequestBuilders
                        .put(uri)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest());
    }
}


