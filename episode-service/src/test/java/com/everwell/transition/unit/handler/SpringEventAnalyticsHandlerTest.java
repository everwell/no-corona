package com.everwell.transition.unit.handler;

import com.everwell.transition.BaseTest;
import com.everwell.transition.binders.EpisodeEventsBinder;
import com.everwell.transition.handlers.SpringEventsAnalyticsHandler;
import com.everwell.transition.model.db.Client;
import com.everwell.transition.model.db.EpisodeTagStore;
import com.everwell.transition.model.dto.EventStreamingAnalyticsDto;
import com.everwell.transition.model.request.episodetags.EpisodeTagDataRequest;
import com.everwell.transition.model.request.episodetags.EpisodeTagBulkDeletionRequest;
import com.everwell.transition.model.request.episodetags.EpisodeTagRequest;
import com.everwell.transition.model.request.episodetags.SearchTagsRequest;
import com.everwell.transition.postconstruct.EpisodeTagStoreCreation;
import com.everwell.transition.repositories.EpisodeTagStoreRepository;
import com.everwell.transition.service.ClientService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.messaging.MessageChannel;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class SpringEventAnalyticsHandlerTest extends BaseTest {

    @Spy
    @InjectMocks
    SpringEventsAnalyticsHandler springEventsAnalyticsHandler;

    @Mock
    EpisodeEventsBinder episodeEventsBinder;

    @Mock
    EpisodeTagStoreCreation episodeTagStoreCreation;

    @Mock
    ClientService clientService;

    @Mock
    MessageChannel eventOutput;

    @Mock
    EpisodeTagStoreRepository episodeTagStoreRepository;

    private Map<String, EpisodeTagStore> episodeTagStoreMap = new HashMap<>();

    @Before
    public void init() {
        Mockito.when(episodeEventsBinder.eventOutput()).thenReturn(eventOutput);
        List<EpisodeTagStore> episodeTagStoreList = new ArrayList<>();
        EpisodeTagStore episodeTagStore = new EpisodeTagStore(1L,"Phone_inaccessible","Phone_inaccessible",null,"test","Phone_issues");
        episodeTagStoreList.add(episodeTagStore);
        episodeTagStoreMap = episodeTagStoreList.stream().collect(Collectors.toMap(
                EpisodeTagStore::getTagName,
                v -> v
        ));
    }

    @Test
    public void trackDeleteTagsEventTest() {
        Client testClient = new Client(1L,"test","test",new Date(),1L, "test");
        when(clientService.getClientByTokenOrDefault()).thenReturn(testClient);
        EpisodeTagBulkDeletionRequest episodeTagBulkDeletionRequest = new EpisodeTagBulkDeletionRequest(Collections.singletonList(1L), Collections.singletonList("Phone_inaccessible"), Collections.singletonList("2012-12-29 11:30:52"), false);

        when(episodeTagStoreCreation.getEpisodeTagStoreMap()).thenReturn(episodeTagStoreMap);
        springEventsAnalyticsHandler.trackDeleteTagsEvent(episodeTagBulkDeletionRequest);

        Mockito.verify(eventOutput, Mockito.times(1)).send(any());
    }

    @Test
    public void trackSaveTagsEventTest() {
        Client testClient = new Client(1L,"test","test",new Date(),1L, "test");
        when(clientService.getClientByTokenOrDefault()).thenReturn(testClient);
        EpisodeTagRequest episodeTagRequest = new EpisodeTagRequest(Collections.singletonList(new EpisodeTagDataRequest( "Phone_inaccessible", LocalDateTime.now(),1L, true)));

        when(episodeTagStoreCreation.getEpisodeTagStoreMap()).thenReturn(episodeTagStoreMap);
        springEventsAnalyticsHandler.trackSaveTagsEvent(episodeTagRequest);

        Mockito.verify(eventOutput, Mockito.times(1)).send(any());
    }

    @Test
    public void trackSearchTagsEventTest() {
        Client testClient = new Client(1L,"test","test",new Date(),1L,"test");
        when(clientService.getClientByTokenOrDefault()).thenReturn(testClient);
        SearchTagsRequest searchTagsRequest = new SearchTagsRequest(Collections.singletonList(1L),Collections.emptyList());

        springEventsAnalyticsHandler.trackSearchTagsEvent(searchTagsRequest);

        Mockito.verify(eventOutput, Mockito.times(1)).send(any());
    }

    @Test
    public void trackTagsEventTest() {
        Client testClient = new Client(1L,"test","test",new Date(),1L,"test");
        when(clientService.getClientByTokenOrDefault()).thenReturn(testClient);
        EventStreamingAnalyticsDto eventStreamingAnalyticsDto = new EventStreamingAnalyticsDto("test","test","test");

        springEventsAnalyticsHandler.trackTagsEvent(eventStreamingAnalyticsDto);

        Mockito.verify(eventOutput, Mockito.times(1)).send(any());
    }



}
