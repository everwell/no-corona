package com.everwell.transition.unit;

import com.everwell.transition.BaseTest;
import com.everwell.transition.constants.Constants;
import com.everwell.transition.constants.FieldConstants;
import com.everwell.transition.exceptions.NotFoundException;
import com.everwell.transition.model.db.Field;
import com.everwell.transition.service.DiseaseTemplateService;
import com.everwell.transition.exceptions.ValidationException;
import com.everwell.transition.model.db.Client;
import com.everwell.transition.model.response.*;
import com.everwell.transition.service.ClientService;
import com.everwell.transition.service.impl.DataGatewayHelperServiceImpl;
import com.everwell.transition.utils.Utils;
import com.fasterxml.jackson.core.type.TypeReference;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.*;

public class DataGatewayHelperServiceTest extends BaseTest {

    @Spy
    @InjectMocks
    DataGatewayHelperServiceImpl dataGatewayHelperService;

    @Mock
    RestTemplate restTemplate;

    @Mock
    DiseaseTemplateService diseaseTemplateService;

    @Mock
    ClientService clientService;

    @Before
    public void init(){
        ReflectionTestUtils.setField(dataGatewayHelperService, "dataGatewayURL", "http://test:8080/");
        ReflectionTestUtils.setField(dataGatewayHelperService, "username", "test");
        ReflectionTestUtils.setField(dataGatewayHelperService, "password", "test");
        when(clientService.getClientByTokenOrDefault()).thenReturn(new Client(29L,"test", "test", null ));
        dataGatewayHelperService.init();
    }

    @Test
    public void test_authenticate() throws IOException {
        Mockito.when(restTemplate.exchange(ArgumentMatchers.anyString(), ArgumentMatchers.any(HttpMethod.class), ArgumentMatchers.any(),ArgumentMatchers.<Class<Map<String, Object>>>any()))
            .thenReturn(new ResponseEntity<Map<String, Object>>(getAuthenticateResponse(), HttpStatus.OK));
        dataGatewayHelperService.authenticate();
        dataGatewayHelperService.getByEntityIdAndType("nikshayPlatform", 10718428L, "patient");
    }

    @Test
    public void test_authenticate_status_not_ok() throws IOException {
        Mockito.when(restTemplate.exchange(ArgumentMatchers.anyString(), ArgumentMatchers.any(HttpMethod.class), ArgumentMatchers.any(),ArgumentMatchers.<Class<Map<String, Object>>>any()))
            .thenReturn(new ResponseEntity<Map<String, Object>>(getAuthenticateResponse(), HttpStatus.ACCEPTED));
        dataGatewayHelperService.authenticate();
        dataGatewayHelperService.getByEntityIdAndType("nikshayPlatform", 10718428L, "patient");
    }

    @Test
    public void getByEntityIdAndType_success() throws IOException {
        when(restTemplate.exchange(ArgumentMatchers.anyString(), any(HttpMethod.class), any(),ArgumentMatchers.<Class<Map<String, Object>>>any()))
                .thenReturn(new ResponseEntity<>(getValidEntityResponse_Success(), HttpStatus.OK));
        Map<String, Object> response = dataGatewayHelperService.getByEntityIdAndType("nikshayPlatform", 10718428L, "patient");
        assertTrue(response.get("Stage").equals("DIAGNOSED_OUTCOME_ASSIGNED"));
    }

    @Test
    public void getByEntityIdAndType_firstTry_forbiddenCall_invokes_authenticate() throws IOException {
        when(restTemplate.exchange(ArgumentMatchers.anyString(), any(HttpMethod.class), any(),ArgumentMatchers.<Class<Map<String, Object>>>any()))
                .thenThrow(new HttpClientErrorException(HttpStatus.FORBIDDEN))
                .thenReturn(new ResponseEntity<>(getAuthenticateResponse(), HttpStatus.OK), new ResponseEntity<>(getValidEntityResponse_Success(), HttpStatus.OK));
        Map<String, Object> response = dataGatewayHelperService.getByEntityIdAndType("nikshayPlatform", 10718428L, "patient");
        verify(dataGatewayHelperService).authenticate();
        assertTrue(response.get("Stage").equals("DIAGNOSED_OUTCOME_ASSIGNED"));
    }

    @Test
    public void getByEntityIdAndType_exception() throws IOException {
        Mockito.when(restTemplate.exchange(ArgumentMatchers.anyString(), ArgumentMatchers.any(HttpMethod.class), ArgumentMatchers.any(),ArgumentMatchers.<Class<Map<String, Object>>>any()))
            .thenThrow(new HttpClientErrorException(HttpStatus.BAD_GATEWAY))
            .thenReturn(new ResponseEntity<>(getAuthenticateResponse(), HttpStatus.OK), new ResponseEntity<>(getValidEntityResponse_Success(), HttpStatus.OK));
        Map<String, Object> response = dataGatewayHelperService.getByEntityIdAndType("nikshayPlatform", 10718428L, "patient");
        verify(dataGatewayHelperService, Mockito.times(0)).authenticate();
    }

    @Test
    public void resolveFields_success() throws IOException {
        when(restTemplate.exchange(ArgumentMatchers.anyString(), any(HttpMethod.class), any(),ArgumentMatchers.<Class<Map<String, Object>>>any()))
                .thenReturn(new ResponseEntity<>(getValidEntityResponse_Success(), HttpStatus.OK));
        when(diseaseTemplateService.getAllFieldsByNameIn(any())).thenReturn(getFieldEntities());
        Map<String, Object> response = dataGatewayHelperService.resolveFields(fieldsToResolve_count_one(), 10718428L);
        assertTrue(response.get(FieldConstants.FIELD_STAGE).equals("DIAGNOSED_OUTCOME_ASSIGNED"));
    }

    @Test(expected = NotFoundException.class)
    public void resolveFields_throws_notFoundException() throws IOException {
        when(restTemplate.exchange(ArgumentMatchers.anyString(), any(HttpMethod.class), any(),ArgumentMatchers.<Class<Map<String, Object>>>any()))
                .thenReturn(new ResponseEntity<>( HttpStatus.OK));
        when(diseaseTemplateService.getAllFieldsByNameIn(any())).thenReturn(getFieldEntities());
        dataGatewayHelperService.resolveFields(fieldsToResolve_count_one(), 10718428L);
    }

    @Test
    public void resolveFields_success_testCase2() throws IOException {
        when(restTemplate.exchange(ArgumentMatchers.anyString(), any(HttpMethod.class), any(),ArgumentMatchers.<Class<Map<String, Object>>>any()))
                .thenReturn(new ResponseEntity<>(getValidEntityResponse_Success(), HttpStatus.OK));
        when(diseaseTemplateService.getAllFieldsByNameIn(any())).thenReturn(getFieldEntities());
        Map<String, Object> response = dataGatewayHelperService.resolveFields(fieldsToResolve_count_two(), 10718428L);
        assertEquals("DIAGNOSED_OUTCOME_ASSIGNED", response.get(FieldConstants.FIELD_STAGE));
        assertEquals("TREATMENT_COMPLETE", response.get(FieldConstants.FIELD_TREATMENT_OUTCOME));
    }

    private List<Field> getFieldEntities() {
        List<Field> list = new ArrayList<>();
        list.add(new Field (1L, "Stage", "Episode", "Episode"));
        list.add(new Field (2L, "treatmentOutcome", "Episode", "EpisodeStageData"));
        list.add(new Field (2L, "refillDate", "Dispensation", "Episode"));
        return list;
    }

    @Test
    public void resolveFields_success_twoModules_testCase() throws IOException {
        when(restTemplate.exchange(ArgumentMatchers.anyString(), any(HttpMethod.class), any(),ArgumentMatchers.<Class<Map<String, Object>>>any()))
                .thenReturn(new ResponseEntity<>(getValidEntityResponse_Success(), HttpStatus.OK)).thenReturn(new ResponseEntity<>(getValidEntityResponse_Success(), HttpStatus.OK))
                .thenReturn(new ResponseEntity<>(getValidEntityResponse_moduleDispensation(), HttpStatus.OK)).thenReturn(new ResponseEntity<>(getValidEntityResponse_Success(), HttpStatus.OK));
        when(diseaseTemplateService.getAllFieldsByNameIn(any())).thenReturn(getFieldEntities());
        Map<String, Object> response = dataGatewayHelperService.resolveFields(fieldsToResolve_countThree_twoModules(), 10718428L);
        assertEquals("DIAGNOSED_OUTCOME_ASSIGNED", response.get(FieldConstants.FIELD_STAGE));
        assertEquals("TREATMENT_COMPLETE", response.get(FieldConstants.FIELD_TREATMENT_OUTCOME));
        assertEquals("21-02-2021 02:02:02", response.get(FieldConstants.FIELD_REFILL_DATE));
    }

    @Test
    public void invoke_fieldConstant() {
        new FieldConstants();
    }

    @Test
    public void testExchangeSuccess() {
        AdherenceResponse adherenceResponse = new AdherenceResponse(
                Utils.toLocalDateTimeWithNull("02-01-2022 18:30:00"),
                Utils.toLocalDateTimeWithNull("06-01-2022 18:30:00"),
                "654A9"
        );
        ResponseEntity<Response<AdherenceResponse>> response =
                new ResponseEntity<>(
                        new Response<>(
                                adherenceResponse
                        ),
                        HttpStatus.OK
                );
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("client-id", "29");
        HttpEntity<?> requestEntity = new HttpEntity<>(httpHeaders);


        when(restTemplate.exchange(ArgumentMatchers.anyString(), ArgumentMatchers.any(HttpMethod.class), ArgumentMatchers.any(), ArgumentMatchers.any(ParameterizedTypeReference.class)))
                .thenReturn(response);
        when(clientService.getClientByTokenOrDefault()).thenReturn(new Client(29L,"test", "test", null ));

        ResponseEntity<Response<AdherenceResponse>> apiResponse =  dataGatewayHelperService.genericExchange("test", HttpMethod.POST, requestEntity, new ParameterizedTypeReference<Response<AdherenceResponse>>() {});


        Mockito.verify(dataGatewayHelperService, Mockito.times(0)).authenticate();
        Assert.assertTrue(apiResponse.getBody().isSuccess());

        when(clientService.getClientByTokenOrDefault()).thenReturn(new Client(30L,"test", "test", null ));
        ResponseEntity<Response<AdherenceResponse>> apiResponseDifferentClientId =  dataGatewayHelperService.genericExchange("test", HttpMethod.POST, requestEntity, new ParameterizedTypeReference<Response<AdherenceResponse>>() {});

        Mockito.verify(dataGatewayHelperService, Mockito.times(0)).authenticate();
        Assert.assertTrue(apiResponseDifferentClientId.getBody().isSuccess());

    }

    @Test
    public void testExchangeAuthError() {
        HttpClientErrorException forbiddenException = HttpClientErrorException.Forbidden.create(HttpStatus.FORBIDDEN, null, null, null, null);
        when(restTemplate.exchange(ArgumentMatchers.anyString(), ArgumentMatchers.any(HttpMethod.class), ArgumentMatchers.any(), ArgumentMatchers.any(ParameterizedTypeReference.class)))
                .thenThrow(forbiddenException);

        doNothing().when(dataGatewayHelperService).authenticate();

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("client-id", "29");
        HttpEntity<?> requestEntity = new HttpEntity<>(httpHeaders);

        dataGatewayHelperService.genericExchange("test", HttpMethod.POST, requestEntity, new ParameterizedTypeReference<Response<String>>() {});

        Mockito.verify(dataGatewayHelperService, Mockito.times(4)).authenticate();

    }

    @Test
    public void testExchangeCustomError() {
        HttpClientErrorException exception = HttpClientErrorException.BadRequest.create(HttpStatus.BAD_REQUEST, "Bad Request", null, "{\"success\":\"false\"}".getBytes(StandardCharsets.UTF_8), null);
        when(restTemplate.exchange(ArgumentMatchers.anyString(), ArgumentMatchers.any(HttpMethod.class), ArgumentMatchers.any(), ArgumentMatchers.any(ParameterizedTypeReference.class)))
                .thenThrow(exception);

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("client-id", "29");
        HttpEntity<?> requestEntity = new HttpEntity<>(httpHeaders);

        ResponseEntity<Response<AdherenceResponse>> apiResponse = dataGatewayHelperService.genericExchange("TEST", HttpMethod.POST, requestEntity, new ParameterizedTypeReference<Response<String>>() {});
        Assert.assertFalse(apiResponse.getBody().isSuccess());
    }

    @Test(expected = ValidationException.class)
    public void testExchangeIOError() {
        HttpClientErrorException exception = HttpClientErrorException.BadRequest.create(HttpStatus.BAD_REQUEST, "Bad Request", null, null, null);
        when(restTemplate.exchange(ArgumentMatchers.anyString(), ArgumentMatchers.any(HttpMethod.class), ArgumentMatchers.any(), ArgumentMatchers.any(ParameterizedTypeReference.class)))
                .thenThrow(exception);

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("client-id", "29");
        HttpEntity<?> requestEntity = new HttpEntity<>(httpHeaders);

        dataGatewayHelperService.genericExchange("test", HttpMethod.POST, requestEntity, new ParameterizedTypeReference<Response<String>>() {});
    }

    @Test
    public void testPostExchange() {
        AdherenceResponse adherenceResponse = new AdherenceResponse(
                Utils.toLocalDateTimeWithNull("02-01-2022 18:30:00"),
                Utils.toLocalDateTimeWithNull("06-01-2022 18:30:00"),
                "654A9"
        );
        ResponseEntity<Response<AdherenceResponse>> response =
                new ResponseEntity<>(
                        new Response<>(
                                adherenceResponse
                        ),
                        HttpStatus.OK
                );

        when(restTemplate.exchange(ArgumentMatchers.anyString(), ArgumentMatchers.any(HttpMethod.class), ArgumentMatchers.any(), ArgumentMatchers.any(ParameterizedTypeReference.class)))
                .thenReturn(response);

        ResponseEntity<Response<AdherenceResponse>> apiResponse = dataGatewayHelperService.postExchange("test", "test", new ParameterizedTypeReference<Response<AdherenceResponse>>() {});

        Mockito.verify(dataGatewayHelperService, Mockito.times(0)).authenticate();
        Assert.assertTrue(apiResponse.getBody().isSuccess());
    }

    @Test
    public void testPostExchangeWithCustomHeaders() {
        AdherenceResponse adherenceResponse = new AdherenceResponse(
                Utils.toLocalDateTimeWithNull("02-01-2022 18:30:00"),
                Utils.toLocalDateTimeWithNull("06-01-2022 18:30:00"),
                "654A9"
        );
        Map<String,String> customHeaders = new HashMap<>();
        customHeaders.put(Constants.X_PLATFORM_ID, "2");
        customHeaders.put("client-id", "29");
        ResponseEntity<Response<AdherenceResponse>> response =
                new ResponseEntity<>(
                        new Response<>(
                                adherenceResponse
                        ),
                        HttpStatus.OK
                );

        when(restTemplate.exchange(ArgumentMatchers.anyString(), ArgumentMatchers.any(HttpMethod.class), ArgumentMatchers.any(), ArgumentMatchers.any(ParameterizedTypeReference.class)))
                .thenReturn(response);

        ResponseEntity<Response<AdherenceResponse>> apiResponse = dataGatewayHelperService.postExchange("test", "test", new ParameterizedTypeReference<Response<AdherenceResponse>>() {}, customHeaders);

        Mockito.verify(dataGatewayHelperService, Mockito.times(0)).authenticate();
        Assert.assertTrue(apiResponse.getBody().isSuccess());
    }

    @Test
    public void testPutExchange() {
        AdherenceResponse adherenceResponse = new AdherenceResponse(
                Utils.toLocalDateTimeWithNull("02-01-2022 18:30:00"),
                Utils.toLocalDateTimeWithNull("06-01-2022 18:30:00"),
                "654A9"
        );
        ResponseEntity<Response<AdherenceResponse>> response =
                new ResponseEntity<>(
                        new Response<>(
                                adherenceResponse
                        ),
                        HttpStatus.OK
                );

        when(restTemplate.exchange(ArgumentMatchers.anyString(), ArgumentMatchers.any(HttpMethod.class), ArgumentMatchers.any(), ArgumentMatchers.any(ParameterizedTypeReference.class)))
                .thenReturn(response);

        ResponseEntity<Response<AdherenceResponse>> apiResponse = dataGatewayHelperService.putExchange("test", "test", new ParameterizedTypeReference<Response<AdherenceResponse>>() {});

        Mockito.verify(dataGatewayHelperService, Mockito.times(0)).authenticate();
        Assert.assertTrue(apiResponse.getBody().isSuccess());
    }

    @Test
    public void testGetExchange() {
        Map<String,Object> queryParams = new HashMap<>();
        queryParams.put("test", "test@123");
        AdherenceResponse adherenceResponse = new AdherenceResponse(
                Utils.toLocalDateTimeWithNull("02-01-2022 18:30:00"),
                Utils.toLocalDateTimeWithNull("06-01-2022 18:30:00"),
                "654A9"
        );
        ResponseEntity<Response<AdherenceResponse>> response =
                new ResponseEntity<>(
                        new Response<>(
                                adherenceResponse
                        ),
                        HttpStatus.OK
                );

        when(restTemplate.exchange(ArgumentMatchers.anyString(), ArgumentMatchers.any(HttpMethod.class), ArgumentMatchers.any(), ArgumentMatchers.any(ParameterizedTypeReference.class)))
                .thenReturn(response);

        ResponseEntity<Response<AdherenceResponse>> apiResponseNoQuery = dataGatewayHelperService.getExchange("test", new ParameterizedTypeReference<Response<AdherenceResponse>>() {}, 1L, null);
        Mockito.verify(dataGatewayHelperService, Mockito.times(0)).authenticate();
        Assert.assertTrue(apiResponseNoQuery.getBody().isSuccess());

        ResponseEntity<Response<AdherenceResponse>> apiResponseNoPath = dataGatewayHelperService.getExchange("test", new ParameterizedTypeReference<Response<AdherenceResponse>>() {}, null, queryParams);
        Mockito.verify(dataGatewayHelperService, Mockito.times(0)).authenticate();
        Assert.assertTrue(apiResponseNoPath.getBody().isSuccess());

    }

    @Test
    public void testDeleteExchange() {
        Map<String,Object> queryParams = new HashMap<>();
        queryParams.put("test", "test@123");
        AdherenceResponse adherenceResponse = new AdherenceResponse(
                Utils.toLocalDateTimeWithNull("02-01-2022 18:30:00"),
                Utils.toLocalDateTimeWithNull("06-01-2022 18:30:00"),
                "654A9"
        );
        ResponseEntity<Response<AdherenceResponse>> response =
                new ResponseEntity<>(
                        new Response<>(
                                adherenceResponse
                        ),
                        HttpStatus.OK
                );

        when(restTemplate.exchange(ArgumentMatchers.anyString(), ArgumentMatchers.any(HttpMethod.class), ArgumentMatchers.any(), ArgumentMatchers.any(ParameterizedTypeReference.class)))
                .thenReturn(response);

        ResponseEntity<Response<AdherenceResponse>> apiResponsePath = dataGatewayHelperService.deleteExchange("test", new ParameterizedTypeReference<Response<AdherenceResponse>>() {}, 1L, null, null);
        Mockito.verify(dataGatewayHelperService, Mockito.times(0)).authenticate();
        Assert.assertTrue(apiResponsePath.getBody().isSuccess());

        ResponseEntity<Response<AdherenceResponse>> apiResponseNoPath = dataGatewayHelperService.deleteExchange("test", new ParameterizedTypeReference<Response<AdherenceResponse>>() {}, null, null, null);
        Mockito.verify(dataGatewayHelperService, Mockito.times(0)).authenticate();
        Assert.assertTrue(apiResponseNoPath.getBody().isSuccess());

    }

    private List<String> fieldsToResolve_count_one() {
        List<String> fields = new LinkedList<>();
        fields.add(FieldConstants.FIELD_STAGE);
        return fields;
    }

    private List<String> fieldsToResolve_count_two() {
        List<String> fields = new LinkedList<>();
        fields.add(FieldConstants.FIELD_STAGE);
        fields.add(FieldConstants.FIELD_TREATMENT_OUTCOME);
        return fields;
    }

    private List<String> fieldsToResolve_countThree_twoModules() {
        List<String> fields = new LinkedList<>();
        fields.add(FieldConstants.FIELD_STAGE);
        fields.add(FieldConstants.FIELD_TREATMENT_OUTCOME);
        fields.add(FieldConstants.FIELD_REFILL_DATE);
        return fields;
    }

    private Map<String, Object> getAuthenticateResponse() throws IOException {
        Map<String, Object> map = new HashMap<>();
        String jsonData = "{\"data\":\"eteaiuraaaxarrarrtieacffr\", \"code\": \"200 OK\",\"message\": \"Successful Response.\"}";
        return Utils.jsonToObject(jsonData, new TypeReference<Map<String, Object>>() {});
    }

    private Map<String, Object> getValidEntityResponse_Success() throws IOException{
        Map<String, Object> map = new HashMap<>();
        String jsonData = "{\"WeightBand\": null, \"Address\": \"32\", \"TbCategory\": null, \"Gender\": \"Male\",\"Stage\": \"DIAGNOSED_OUTCOME_ASSIGNED\", \"FirstName\": \"dispensation\", \"treatmentOutcome\": \"TREATMENT_COMPLETE\" }";
        return Utils.jsonToObject(jsonData, new TypeReference<Map<String, Object>>() {});
    }

    private Map<String, Object> getValidEntityResponse_moduleDispensation() throws IOException {
        Map<String, Object> map = new HashMap<>();
        String jsonData = "{\"entityId\": \"1\", \"refillDate\": \"21-02-2021 02:02:02\", \"totalDispensedDrugs\": \"20\" }";
        return Utils.jsonToObject(jsonData, new TypeReference<Map<String, Object>>() {});
    }
}
