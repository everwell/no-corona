package com.everwell.transition.unit;

import com.everwell.transition.BaseTest;
import com.everwell.transition.model.dto.GenericEvents;
import com.everwell.transition.model.dto.notification.NotificationTriggerDto;
import com.everwell.transition.processor.NotificationProcessor;
import com.everwell.transition.service.NotificationService;
import com.everwell.transition.utils.Utils;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.GenericMessage;

import java.io.IOException;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;

public class NotificationProcessorTest extends BaseTest {

    @InjectMocks
    NotificationProcessor notificationProcessor;

    @Mock
    NotificationService notificationService;

    @Test
    public void testNotificationHandler() throws IOException, IllegalAccessException {
        NotificationTriggerDto notificationTriggerDto = new NotificationTriggerDto();
        GenericEvents<NotificationTriggerDto> event = new GenericEvents<>(notificationTriggerDto, "test");
        String payload = Utils.asJsonString(event);
        Message message = new GenericMessage<>(payload);

        doNothing().when(notificationService).sendSms(any(), eq("29"), eq("test"));

        notificationProcessor.notificationHandler(message, "29");
        Mockito.verify(notificationService, Mockito.times(1)).sendSms(any(), eq("29"), eq("test"));
    }
}
