package com.everwell.transition.unit;

import com.everwell.transition.constants.Constants;
import com.everwell.transition.enums.SupportedRelationType;
import com.everwell.transition.exceptions.IllegalArgumentException;
import com.everwell.transition.exceptions.ValidationException;
import com.everwell.transition.model.response.AdherenceResponse;
import com.everwell.transition.model.response.Response;
import com.everwell.transition.model.response.SSO.DeviceIdsMapResponse;
import com.everwell.transition.utils.Utils;
import com.fasterxml.jackson.core.type.TypeReference;
import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.io.InputStream;
import java.time.*;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class UtilsTest {

    @Test
    public void readRequestBody_inputStreamNull() throws IOException {
        InputStream stubInputStream = null;
        Assert.assertNull(Utils.readRequestBody(stubInputStream));
    }

    @Test
    public void readRequestBody() throws IOException {
        String testingData = "some test data for input stream";

        InputStream stubInputStream =
            IOUtils.toInputStream(testingData, "UTF-8");

        Assert.assertEquals(testingData, Utils.readRequestBody(stubInputStream));
    }

    @Test(expected = RuntimeException.class)
    public void asJsonString_correctInput() {
        //if a class has no public fields/method, we can see exception from jackson
        Utils.asJsonString(new ClassThatJacksonCannotSerialize());
    }

    @Test
    public void utils_constructorInvocation() {
        Utils utils = new Utils();
    }

    private static class ClassThatJacksonCannotSerialize {}

    @Test(expected = IllegalArgumentException.class)
    public void formatDate_incorrectDate() {
        Utils.convertStringToDate("x", "yydd");
    }

    @Test
    public void toLocalDateTest() {
        String date = "12-10-2021";
        LocalDate localDate = Utils.toLocalDate(date);
        Assert.assertEquals(localDate.getYear(), 2021);
        Assert.assertEquals(localDate.getMonth().getValue(), 10);
    }

    @Test
    public void toLocalDateTest_CustomFormat() {
        String date = "10-2021-12";
        LocalDate localDate = Utils.toLocalDate(date, "MM-yyyy-dd");
        Assert.assertEquals(localDate.getYear(), 2021);
        Assert.assertEquals(localDate.getMonth().getValue(), 10);
    }

    @Test
    public void toLocalDateTest_CustomFormat_with_null() {
        String date = "14-10-2021 18:30:00";
        LocalDateTime localDateTime = Utils.toLocalDateTimeWithNull(date);
        Assert.assertEquals(localDateTime.getYear(), 2021);
        Assert.assertEquals(localDateTime.getMonth().getValue(), 10);
    }

    @Test
    public void toLocalDateTest_CustomFormat_null_date() {
        String date = null;
        LocalDateTime localDateTime = Utils.toLocalDateTimeWithNull(date, "dd-MM-yyyy");
        Assert.assertNull(localDateTime);
    }

    @Test
    public void toLocalDateTest_CustomFormat_empty_date() {
        String date = "";
        LocalDateTime localDateTime = Utils.toLocalDateTimeWithNull(date, "dd-MM-yyyy");
        Assert.assertNull(localDateTime);
    }

    @Test
    public void getSupportedRelationTypeEnum() {
        String association = SupportedRelationType.ASSOCIATION.name();
        String hierarchy = SupportedRelationType.HIERARCHY.name();

        Assert.assertEquals("HIERARCHY", hierarchy);
        Assert.assertEquals("ASSOCIATION", association);
    }

    @Test
    public void compareLocalDateNull() {
        Assert.assertFalse(Utils.isDateBefore(null, LocalDateTime.now()));
        Assert.assertFalse(Utils.isDateBefore(LocalDateTime.now(), null));
        Assert.assertFalse(Utils.isDateBefore(null, null));
    }

    @Test
    public void compaareLocalDateNotNull() {
        Assert.assertFalse(Utils.isDateBefore(LocalDateTime.now().plusDays(1),LocalDateTime.now()));
        Assert.assertTrue(Utils.isDateBefore(LocalDateTime.now(), LocalDateTime.now().plusDays(1)));
    }

    @Test
    public void isEmpty() {
        String str = null;
        Assert.assertTrue(Utils.isEmpty(str));
        str = "s";
        Assert.assertFalse(Utils.isEmpty(str));
        List<String> list = null;
        Assert.assertTrue(Utils.isEmpty(list));
        list = new ArrayList<>();
        Assert.assertTrue(Utils.isEmpty(list));
        list.add("Str");
        Assert.assertFalse(Utils.isEmpty(list));
    }

    @Test
    public void processResponseEntity() {
        Map<String, List<String>> userDeviceIdsMap = new HashMap<>();
        List<String> deviceIdList = new ArrayList<>();
        deviceIdList.add("1");
        userDeviceIdsMap.put("userNames", deviceIdList);
        userDeviceIdsMap.put("1",deviceIdList);
        DeviceIdsMapResponse deviceIdsMapResponse = new DeviceIdsMapResponse(userDeviceIdsMap);
        ResponseEntity<Response<DeviceIdsMapResponse>> deviceIdResponse = new ResponseEntity<>(new Response<>(true, deviceIdsMapResponse), HttpStatus.OK);
        Map<String, List<String>> response = Utils.processResponseEntity(deviceIdResponse, "test").getUserDeviceIdsMap();
        Assert.assertEquals("1", response.get("userNames").get(0));
    }

    @Test
    public void processResponseEntityThrowsErrorWithCustomMessage() {
        ResponseEntity responseEntity = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        ValidationException exception = assertThrows(
                ValidationException.class,
                () -> Utils.processResponseEntity(responseEntity, "custom exception")
        );

        assertEquals("custom exception", exception.getMessage());
    }

    @Test
    public void processResponseEntityThrowsErrorWithIncomingMessage() {
        ResponseEntity<Response<DeviceIdsMapResponse>> responseEntity = new ResponseEntity<>(new Response<>(false, null, "incoming message"), HttpStatus.OK);
        ValidationException exception = assertThrows(
                ValidationException.class,
                () -> Utils.processResponseEntity(responseEntity, "custom exception")
        );

        assertEquals("incoming message", exception.getMessage());
    }

    @Test
    public void isResponseValidTrueTest() {
        ResponseEntity<Response<Map<String, Object>>> responseEntity = new ResponseEntity<>(new Response<>(true, new HashMap<>(), "Successful !"), HttpStatus.OK);
        boolean response = Utils.isResponseValid(responseEntity);
        Assert.assertTrue(response);
    }

    @Test
    public void isResponseValidForNullResponseTest() {
        boolean response = Utils.isResponseValid(null);
        Assert.assertFalse(response);
    }

    @Test
    public void isResponseValidForNullBodyResponseTest() {
        ResponseEntity<Response<Map<String, Object>>> responseEntity = new ResponseEntity<>(null, HttpStatus.OK);
        boolean response = Utils.isResponseValid(responseEntity);
        Assert.assertFalse(response);
    }

    @Test
    public void isResponseValidFalseTest() {
        ResponseEntity<Response<Map<String, Object>>> responseEntity = new ResponseEntity<>(new Response<>(false, new HashMap<>(), "Error !"), HttpStatus.OK);
        boolean response = Utils.isResponseValid(responseEntity);
        Assert.assertFalse(response);
    }

    @Test
    public void getCurrentDateInGivenTimeZoneTest() {
        LocalDate today = LocalDate.now(ZoneId.of(Constants.IST_TIMEZONE));
        LocalDate response = Utils.getCurrentDateInGivenTimeZone(Constants.IST_TIMEZONE);
        Assert.assertEquals(today, response);
    }

    @Test
    public void convertDateTimeToSpecificZoneDateTimeTest() {
        LocalDate today = Utils.getCurrentDateInGivenTimeZone(Constants.IST_TIMEZONE);
        ZonedDateTime test = today.atStartOfDay(ZoneId.of(Constants.IST_TIMEZONE));
        String response = Utils.convertDateTimeToSpecificZoneDateTime(test, ZoneOffset.UTC);
        Assert.assertEquals(today.minusDays(1).toString(), response.substring(0, 10));
    }

    @Test
    public void  convertLocalDateTimeToGivenTimeZone() {
        LocalDateTime today = LocalDateTime.now(ZoneId.of("UTC"));
        String response = Utils.convertLocalDateTimeToGivenTimeZone(today, "UTC");
        Assert.assertEquals(today.toLocalDate().toString(), response);
    }

    @Test
    public void convertObjectToString() {
        String response = Utils.convertObjectToStringWithNull("test");
        Assert.assertEquals("test", response);
        String nullResponse = Utils.convertObjectToStringWithNull(null);
        Assert.assertNull(nullResponse);
    }

    @Test
    public void convertDateFromCurrentToExpectedFormatWithNullTest() {
        String date = "2023-04-01 11:12:15";
        String response = Utils.convertDateFromCurrentToExpectedFormatWithNull(date, Constants.DATE_FORMAT, Constants.DATE_TIME_FORMAT);
        Assert.assertEquals("1-4-2023 11:12:15", response);
    }

    @Test
    public void convertDateFromCurrentToExpectedFormatWithNullTestNullDate() {
        String response = Utils.convertDateFromCurrentToExpectedFormatWithNull(null, Constants.DATE_FORMAT, Constants.DATE_TIME_FORMAT);
        Assert.assertNull(response);
    }

    // function added to generate time in utc to use in flyway migration script as version number
    // to avoid version conflicts with parallel development
    @Test
    public void getUTCTimeNow() {
        LocalDateTime today = LocalDateTime.now(ZoneId.of("UTC"));
        System.out.println(Utils.getFormattedDateNew(today, "yyyyMMddHHmmss"));
    }

}
