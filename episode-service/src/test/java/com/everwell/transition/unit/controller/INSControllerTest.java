package com.everwell.transition.unit.controller;

import com.everwell.transition.BaseTest;
import com.everwell.transition.constants.FieldConstants;
import com.everwell.transition.controller.INSController;
import com.everwell.transition.enums.episode.EpisodeField;
import com.everwell.transition.exceptions.CustomExceptionHandler;
import com.everwell.transition.model.db.Client;
import com.everwell.transition.model.dto.EngagementDto;
import com.everwell.transition.model.dto.EpisodeDto;
import com.everwell.transition.model.request.EngagementRequest;
import com.everwell.transition.model.request.ins.NotificationLogRequest;
import com.everwell.transition.model.response.EngagementResponse;
import com.everwell.transition.service.ClientService;
import com.everwell.transition.service.EpisodeLogService;
import com.everwell.transition.service.EpisodeService;
import com.everwell.transition.service.INSService;
import com.everwell.transition.utils.Utils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.HashMap;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class INSControllerTest extends BaseTest {

    private MockMvc mockMvc;

    @Spy
    @InjectMocks
    private INSController insController;

    @Mock
    private INSService insService;

    @Mock
    private EpisodeService episodeService;

    @Mock
    private ClientService clientService;

    @Mock
    private EpisodeLogService episodeLogService;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(insController)
                .setControllerAdvice(new CustomExceptionHandler())
                .build();
    }

    @Test
    public void getEngagementNotExists() throws Exception {
        String uri = "/v1/engagement?episodeId=123";
        Map<String, Object> stageData = new HashMap<>();
        Map<String, Object> hierarchyMappings = new HashMap<>();
        hierarchyMappings.put(FieldConstants.FIELD_CURRENT, 157);
        EpisodeDto episodeDto = new EpisodeDto();
        episodeDto.setStageData(stageData);
        episodeDto.setHierarchyMappings(hierarchyMappings);

        when(clientService.getClientByTokenOrDefault()).thenReturn(new Client(29L, "test", "test", null));
        when(episodeService.getEpisodeDetails(eq(123L), eq(29L))).thenReturn(episodeDto);
        when(insService.getEngagement(any())).thenReturn(null);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.currentHierarchyId").value(157));
    }

    @Test
    public void getEngagementSuccess() throws Exception {
        String uri = "/v1/engagement?episodeId=123";
        Map<String, Object> stageData = new HashMap<>();
        stageData.put(EpisodeField.CALL_CENTER_CALLS.getFieldName(), false);
        stageData.put(EpisodeField.HOUSEHOLD_VISITS.getFieldName(), true);
        Map<String, Object> hierarchyMappings = new HashMap<>();
        hierarchyMappings.put(FieldConstants.FIELD_CURRENT, 157);
        EpisodeDto episodeDto = new EpisodeDto();
        episodeDto.setStageData(stageData);
        episodeDto.setHierarchyMappings(hierarchyMappings);

        when(clientService.getClientByTokenOrDefault()).thenReturn(new Client(29L, "test", "test", null));
        when(episodeService.getEpisodeDetails(eq(123L), eq(29L))).thenReturn(episodeDto);
        when(insService.getEngagement(any())).thenReturn(null);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.callCenterCalls").value(false))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.householdVisits").value(true));
    }

    @Test
    public void saveEngagementNoEpisodeId() throws Exception {
        String uri = "/v1/engagement";
        EngagementRequest request = new EngagementRequest();

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .content(Utils.asJsonString(request))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("episode Id cannot be null"));
    }

    @Test
    public void saveEngagementSuccess() throws Exception {
        String uri = "/v1/engagement";
        EngagementRequest request = new EngagementRequest();
        request.setEpisodeId(123L);
        EpisodeDto episodeDto = new EpisodeDto();

        when(clientService.getClientByTokenOrDefault()).thenReturn(new Client(29L, "test", "test", null));
        when(episodeService.getEpisodeDetails(eq(123L), eq(29L))).thenReturn(episodeDto);
        when(insService.saveEngagement(any(), any())).thenReturn(new EngagementDto());
        when(episodeLogService.save(any())).thenReturn(null);
        when(episodeService.processUpdateEpisode(eq(29L), any())).thenReturn(null);


        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .content(Utils.asJsonString(request))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk());
    }

    @Test
    public void testNotificationLogsSuccess() throws Exception{
        String uri = "/v1/notification/logs";
        NotificationLogRequest notificationLogRequest = new NotificationLogRequest();
        notificationLogRequest.setEpisodeId(1L);
        notificationLogRequest.setEventName("getMorningDoseReminderPNDtosForClientIdAndSchedule");

        when(clientService.getClientByTokenOrDefault()).thenReturn(new Client(29L, "test", "test", null));
        when(insService.addNotificationLog(notificationLogRequest, 29L)).thenReturn("added!");

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .content(Utils.asJsonString(notificationLogRequest))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk());
    }

    @Test
    public void testNotificationLogsFailureWithoutEpisodeId() throws Exception{
        String uri = "/v1/notification/logs";
        NotificationLogRequest notificationLogRequest = new NotificationLogRequest();
        notificationLogRequest.setEventName("getMorningDoseReminderPNDtosForClientIdAndSchedule");

        when(clientService.getClientByTokenOrDefault()).thenReturn(new Client(29L, "test", "test", null));
        when(insService.addNotificationLog(notificationLogRequest, 29L)).thenReturn("added!");

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .content(Utils.asJsonString(notificationLogRequest))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Episode Id and Event Name cannot be empty!"));
    }

    @Test
    public void testNotificationLogsFailureWithoutEventName() throws Exception{
        String uri = "/v1/notification/logs";
        NotificationLogRequest notificationLogRequest = new NotificationLogRequest();
        notificationLogRequest.setEpisodeId(1L);

        when(clientService.getClientByTokenOrDefault()).thenReturn(new Client(29L, "test", "test", null));
        when(insService.addNotificationLog(notificationLogRequest, 29L)).thenReturn("added!");

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .content(Utils.asJsonString(notificationLogRequest))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Episode Id and Event Name cannot be empty!"));

    }
}
