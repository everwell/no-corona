package com.everwell.transition.unit.controller;

import com.everwell.transition.BaseTest;
import com.everwell.transition.controller.DiseaseController;
import com.everwell.transition.exceptions.CustomExceptionHandler;
import com.everwell.transition.model.db.Client;
import com.everwell.transition.model.db.DiseaseTemplate;
import com.everwell.transition.model.response.DiseaseTemplateResponse;
import com.everwell.transition.service.ClientService;
import com.everwell.transition.service.DiseaseTemplateService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class DiseaseControllerTest extends BaseTest {

    private MockMvc mockMvc;

    @InjectMocks
    DiseaseController diseaseController;

    @Mock
    DiseaseTemplateService diseaseTemplateService;

    @Mock
    ClientService clientService;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(diseaseController)
                .setControllerAdvice(new CustomExceptionHandler())
                .build();
    }

    @Test
    public void getDiseasesTest() throws Exception {
        String uri = "/v1/disease";
        when(clientService.getClientByTokenOrDefault()).thenReturn(new Client(29L, "test", "test", null));
        when(diseaseTemplateService.getDiseasesForClient(29L)).thenReturn(getDiseases());
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.diseases[0].id").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.diseases[0].diseaseName").value("TB"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.diseases[0].clientId").value(29));
    }

    DiseaseTemplateResponse getDiseases() {
        List<DiseaseTemplate> diseases  = Collections.singletonList(new DiseaseTemplate(1L, "TB", 29L, false, "TB"));
        return new DiseaseTemplateResponse(diseases);
    }

    @Test
    public void defaultDiseaseForClientIdTest() throws Exception {
        when(diseaseTemplateService.getDefaultDiseaseForClientId(eq(1L))).thenReturn(new DiseaseTemplate(1L, "disease", 1L, true, "disease"));
        when(clientService.getClientByTokenOrDefault()).thenReturn(new Client(29L, "test", "test", null));
        String uri = "/v1/disease/default";
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk());

    }
}
