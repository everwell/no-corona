package com.everwell.transition.unit.controller;

import com.everwell.transition.BaseTest;
import com.everwell.transition.controller.UserController;
import com.everwell.transition.enums.notification.NotificationField;
import com.everwell.transition.enums.user.UserServiceField;
import com.everwell.transition.exceptions.CustomExceptionHandler;
import com.everwell.transition.model.db.Client;
import com.everwell.transition.model.dto.EpisodeDto;
import com.everwell.transition.model.response.Response;
import com.everwell.transition.service.ClientService;
import com.everwell.transition.service.EpisodeService;
import com.everwell.transition.service.NotificationService;
import com.everwell.transition.service.UserService;
import com.everwell.transition.utils.Utils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Collections;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class UserControllerTest extends BaseTest {

    private MockMvc mockMvc;

    @Spy
    @InjectMocks
    private UserController userController;

    @Mock
    private UserService userService;

    @Mock
    private EpisodeService episodeService;

    @Mock
    private NotificationService notificationService;

    @Mock
    private ClientService clientService;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(userController)
                .setControllerAdvice(new CustomExceptionHandler())
                .build();
    }

    @Test
    public void testGetUserRelations() throws Exception {
        String uri = "/v1/user/relation?userId=1&status=ACCEPT";
        when(userService.getUserRelations(eq(1L), eq("ACCEPT"))).thenReturn(new ResponseEntity<>(new Response(true, null), HttpStatus.OK));

        mockMvc.perform(MockMvcRequestBuilders.get(uri)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());
    }

    @Test
    public void testGetUserRelationByPhone() throws Exception {
        String uri = "/v1/users/relation?phoneNumber=123&primaryUserId=1";
        when(userService.getUserRelationsPrimaryPhone(eq("123"), eq(1L))).thenReturn(new ResponseEntity<>(new Response(true, null), HttpStatus.OK));

        mockMvc.perform(MockMvcRequestBuilders.get(uri)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());
    }

    @Test
    public void testAddRelation() throws Exception {
        String uri = "/v1/user/relation";
        doNothing().when(userService).addUserRelation(any());
        when(clientService.getClientByTokenOrDefault()).thenReturn(new Client(171L, "twsr", "est", null));
        EpisodeDto episodeDto =  new EpisodeDto(1L, 1L, 1L, true, "DSTB", 1L, null, Utils.getCurrentDateNew(), Utils.getCurrentDateNew(), null, Utils.getCurrentDateNew(), null, null, 1L,"PRESUMPTIVE_OPEN", "Presumptive Open", "IndiaTbPublic", 1L, null, null, null, null, null);
        List<EpisodeDto> episodeDtos = Collections.singletonList(episodeDto);
        doReturn(episodeDtos).when(episodeService).getEpisodesForPersonList(any(), any(), anyBoolean());

        doNothing().when(notificationService).sendPushNotificationForEpisodeWithExtraData(any(), any(), any());
        Map<String, Object> userRelationDto = new HashMap<>();
        userRelationDto.put(UserServiceField.SECONDARY_USER_ID.getFieldName(), "123");

        mockMvc.perform(MockMvcRequestBuilders.post(uri)
                        .content(Utils.asJsonString(userRelationDto))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void testUpdateRelation() throws Exception {
        String uri = "/v1/user/relation";
        doNothing().when(userService).updateUserRelation(any());
        Map<String, Object> userRelationDto = new HashMap<>();
        userRelationDto.put(NotificationField.NOTIFICATION_ID.getFieldName(), "1");
        userRelationDto.put(NotificationField.STATUS.getFieldName(), "accept");
        doReturn(new ResponseEntity<>(new Response(true), HttpStatus.OK)).when(notificationService).updateNotification(anyLong(), anyBoolean(), any());

        mockMvc.perform(MockMvcRequestBuilders.put(uri)
                        .content(Utils.asJsonString(userRelationDto))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void testUpdateRelationNoNotification() throws Exception {
        String uri = "/v1/user/relation";
        doNothing().when(userService).updateUserRelation(any());
        Map<String, Object> userRelationDto = new HashMap<>();

        mockMvc.perform(MockMvcRequestBuilders.put(uri)
                        .content(Utils.asJsonString(userRelationDto))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void testDeleteRelation() throws Exception {
        String uri = "/v1/user/relation";
        Map<String, Object> userRelationDto = new HashMap<>();
        doNothing().when(userService).deleteUserRelation(any());
        doReturn(new ResponseEntity<>(new Response(true), HttpStatus.OK)).when(notificationService).deleteNotification(any());

        mockMvc.perform(MockMvcRequestBuilders.delete(uri)
                        .content(Utils.asJsonString(userRelationDto))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void testGetAllVitals() throws Exception {
        String uri = "/v1/vitals";
        ResponseEntity responseEntity = new ResponseEntity<>(new Response(true, new ArrayList<>()), HttpStatus.OK);
        when(userService.getAllVitals()).thenReturn(responseEntity);

        mockMvc.perform(MockMvcRequestBuilders.get(uri)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void testGetVitalsById() throws Exception {
        String uri = "/v1/vitals/1";
        ResponseEntity responseEntity = new ResponseEntity<>(new Response(true, new HashMap<>()), HttpStatus.OK);
        when(userService.getVitalsById(eq(1L))).thenReturn(responseEntity);

        mockMvc.perform(MockMvcRequestBuilders.get(uri)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void testSaveVitals() throws Exception {
        String uri = "/v1/vitals";
        ResponseEntity responseEntity = new ResponseEntity<>(new Response(true, new ArrayList<>()), HttpStatus.CREATED);
        Map<String, Object> vitalBulkRequest = new HashMap<>();
        when(userService.saveVitals(eq(vitalBulkRequest))).thenReturn(responseEntity);

        mockMvc.perform(MockMvcRequestBuilders.post(uri)
                        .content(Utils.asJsonString(vitalBulkRequest))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    public void testUpdateVitalsById() throws Exception {
        String uri = "/v1/vitals/1";
        ResponseEntity responseEntity = new ResponseEntity<>(new Response(true, new HashMap<>()), HttpStatus.OK);
        Map<String, Object> vitalBulkRequest = new HashMap<>();
        when(userService.updateVitalsById(eq(1L), eq(vitalBulkRequest))).thenReturn(responseEntity);

        mockMvc.perform(MockMvcRequestBuilders.put(uri)
                        .content(Utils.asJsonString(vitalBulkRequest))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void testDeleteVitalsById() throws Exception {
        String uri = "/v1/vitals/1";
        ResponseEntity responseEntity = new ResponseEntity<>(new Response(true, null), HttpStatus.OK);
        when(userService.deleteVitalsById(eq(1L))).thenReturn(responseEntity);

        mockMvc.perform(MockMvcRequestBuilders.delete(uri)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void testGetVitalsGoalByUserId() throws Exception {
        String uri = "/v1/users/1/vitals-goal";
        ResponseEntity responseEntity = new ResponseEntity<>(new Response(true, new ArrayList<>()), HttpStatus.OK);
        when(userService.getVitalsGoalByUserId(eq(1L))).thenReturn(responseEntity);

        mockMvc.perform(MockMvcRequestBuilders.get(uri)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void testSaveUserVitalsGoal() throws Exception {
        String uri = "/v1/users/1/vitals-goal";
        ResponseEntity responseEntity = new ResponseEntity<>(new Response(true, null), HttpStatus.CREATED);
        Map<String, Object> vitalGoalRequest = new HashMap<>();
        when(userService.saveUserVitalsGoal(eq(1L), eq(vitalGoalRequest))).thenReturn(responseEntity);

        mockMvc.perform(MockMvcRequestBuilders.post(uri)
                        .content(Utils.asJsonString(vitalGoalRequest))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    public void testUpdateUserVitalsGoal() throws Exception {
        String uri = "/v1/users/1/vitals-goal";
        ResponseEntity responseEntity = new ResponseEntity<>(new Response(true, null), HttpStatus.OK);
        Map<String, Object> vitalGoalRequest = new HashMap<>();
        when(userService.updateUserVitalsGoal(eq(1L), eq(vitalGoalRequest))).thenReturn(responseEntity);

        mockMvc.perform(MockMvcRequestBuilders.put(uri)
                        .content(Utils.asJsonString(vitalGoalRequest))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void testDeleteUserVitalsGoal() throws Exception {
        String uri = "/v1/users/1/vitals-goal";
        ResponseEntity responseEntity = new ResponseEntity<>(new Response(true, null), HttpStatus.OK);
        when(userService.deleteUserVitalsGoal(eq(1L))).thenReturn(responseEntity);

        mockMvc.perform(MockMvcRequestBuilders.delete(uri)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void getVitalsDetailsByUserId() throws Exception {
        String uri = "/v1/users/vitals/bulk";
        ResponseEntity responseEntity = new ResponseEntity<>(new Response(true, new HashMap<>()), HttpStatus.OK);
        Map<String, Object> vitalDetailsRequest = new HashMap<>();
        when(userService.getUserVitalsDetails(eq(vitalDetailsRequest))).thenReturn(responseEntity);

        mockMvc.perform(MockMvcRequestBuilders.post(uri)
                        .content(Utils.asJsonString(vitalDetailsRequest))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void saveUserVitalsDetails() throws Exception {
        String uri = "/v1/users/1/vitals";
        ResponseEntity responseEntity = new ResponseEntity<>(new Response(true, null), HttpStatus.CREATED);
        Map<String, Object> addVitalDetailsRequest = new HashMap<>();
        when(userService.saveUserVitalsDetails(eq(1L), eq(addVitalDetailsRequest))).thenReturn(responseEntity);

        mockMvc.perform(MockMvcRequestBuilders.post(uri)
                        .content(Utils.asJsonString(addVitalDetailsRequest))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }
}
