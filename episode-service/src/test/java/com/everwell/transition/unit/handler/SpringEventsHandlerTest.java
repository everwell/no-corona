package com.everwell.transition.unit.handler;

import com.everwell.transition.BaseTest;
import com.everwell.transition.handlers.SpringEventsHandler;
import com.everwell.transition.model.db.EpisodeLog;
import com.everwell.transition.model.request.episode.UpdateRiskRequest;
import com.everwell.transition.model.response.EpisodeLogResponse;
import com.everwell.transition.repositories.EpisodeLogRepository;
import com.everwell.transition.service.EpisodeLogService;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;

import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;

public class SpringEventsHandlerTest extends BaseTest {

    @Spy
    @InjectMocks
    SpringEventsHandler springEventsHandler;

    @Mock
    EpisodeLogService episodeLogService;

    @Test
    public void addHighRiskLogsTest() {
        UpdateRiskRequest updateRiskRequest = new UpdateRiskRequest(Collections.singletonList(1L),Collections.singletonList(1L),2L);
        springEventsHandler.updateRiskLogs(updateRiskRequest);
        Mockito.verify(episodeLogService,Mockito.times(1)).save(any());
    }

}
