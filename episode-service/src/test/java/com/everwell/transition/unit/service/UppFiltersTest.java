package com.everwell.transition.unit.service;

import com.everwell.transition.BaseTest;
import com.everwell.transition.constants.FieldConstants;
import com.everwell.transition.constants.UppFieldsConstants;
import com.everwell.transition.elasticsearch.service.Impl.UppFiltersToEpisodeSearchFields;
import com.everwell.transition.model.dto.EpisodeUnifiedFilters;
import com.everwell.transition.model.dto.EpisodeUnifiedSearchRequest;
import com.everwell.transition.model.request.episodesearch.EpisodeSearchRequest;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Spy;

import java.util.Collections;
import java.util.HashMap;

public class UppFiltersTest extends BaseTest {

    @Spy
    @InjectMocks
    UppFiltersToEpisodeSearchFields uppFiltersToEpisodeSearchFields;
    

    @Test
    public void transformDateTypeNotificationDate() {
        EpisodeUnifiedFilters episodeUnifiedFilters = new EpisodeUnifiedFilters();
        episodeUnifiedFilters.setDateType(UppFieldsConstants.FIELD_NOTIFICATION_DATE);
        episodeUnifiedFilters.setDateStart("2022-10-22 18:30:00");
        episodeUnifiedFilters.setDateEnd("2022-10-24 18:30:00");
        EpisodeSearchRequest episodeSearchRequest = new EpisodeSearchRequest(new EpisodeSearchRequest.SupportedSearchMethods(new HashMap<>(), new HashMap<>(), new HashMap<>()), null);
        EpisodeUnifiedSearchRequest episodeUnifiedSearchRequest = new EpisodeUnifiedSearchRequest(episodeSearchRequest, episodeUnifiedFilters);

        EpisodeSearchRequest searchRequest = uppFiltersToEpisodeSearchFields.convert(episodeUnifiedSearchRequest);
        Assert.assertNotNull(searchRequest.getRange().get(FieldConstants.EPISODE_FIELD_DIAGNOSIS_DATE));
    }

    @Test
    public void transformDateTypeRegistrationDate() {
        EpisodeUnifiedFilters episodeUnifiedFilters = new EpisodeUnifiedFilters();
        episodeUnifiedFilters.setDateType(UppFieldsConstants.FIELD_REGISTRATION_DATE);
        episodeUnifiedFilters.setDateStart("2022-10-22 18:30:00");
        episodeUnifiedFilters.setDateEnd("2022-10-24 18:30:00");
        EpisodeUnifiedSearchRequest episodeUnifiedSearchRequest = new EpisodeUnifiedSearchRequest(new EpisodeSearchRequest(), episodeUnifiedFilters);

        EpisodeSearchRequest searchRequest = uppFiltersToEpisodeSearchFields.convert(episodeUnifiedSearchRequest);
        Assert.assertNotNull(searchRequest.getRange().get(FieldConstants.EPISODE_FIELD_CREATED_DATE));
    }

    @Test
    public void transformDateTypeStartDate() {
        EpisodeUnifiedFilters episodeUnifiedFilters = new EpisodeUnifiedFilters();
        episodeUnifiedFilters.setDateType(FieldConstants.TB_TREATMENT_START_DATE);
        episodeUnifiedFilters.setDateStart("2022-10-22 18:30:00");
        episodeUnifiedFilters.setDateEnd("2022-10-24 18:30:00");
        EpisodeUnifiedSearchRequest episodeUnifiedSearchRequest = new EpisodeUnifiedSearchRequest(new EpisodeSearchRequest(), episodeUnifiedFilters);

        EpisodeSearchRequest searchRequest = uppFiltersToEpisodeSearchFields.convert(episodeUnifiedSearchRequest);
        Assert.assertNotNull(searchRequest.getRange().get(FieldConstants.EPISODE_FIELD_START_DATE));
    }

    @Test
    public void transformMonitoringMethod99DOTS() {
        EpisodeUnifiedFilters episodeUnifiedFilters = new EpisodeUnifiedFilters();
        episodeUnifiedFilters.setMonitoringMethod(Collections.singletonList(UppFieldsConstants.MONITORING_METHOD_99DOTS_DISPLAY));
        EpisodeUnifiedSearchRequest episodeUnifiedSearchRequest = new EpisodeUnifiedSearchRequest(new EpisodeSearchRequest(), episodeUnifiedFilters);

        EpisodeSearchRequest searchRequest = uppFiltersToEpisodeSearchFields.convert(episodeUnifiedSearchRequest);
        Assert.assertEquals(Collections.singletonList(UppFieldsConstants.MONITORING_METHOD_99DOTS), searchRequest.getSearch().getMust().get(FieldConstants.MONITORING_METHOD));
    }

    @Test
    public void transformMonitoringMethod99DOTSLite() {
        EpisodeUnifiedFilters episodeUnifiedFilters = new EpisodeUnifiedFilters();
        episodeUnifiedFilters.setMonitoringMethod(Collections.singletonList(UppFieldsConstants.MONITORING_METHOD_99DOTS_LITE));
        EpisodeUnifiedSearchRequest episodeUnifiedSearchRequest = new EpisodeUnifiedSearchRequest(new EpisodeSearchRequest(), episodeUnifiedFilters);

        EpisodeSearchRequest searchRequest = uppFiltersToEpisodeSearchFields.convert(episodeUnifiedSearchRequest);
        Assert.assertEquals(Collections.singletonList(UppFieldsConstants.MONITORING_METHOD_99DOTS_LITE), searchRequest.getSearch().getMust().get(FieldConstants.MONITORING_METHOD));
    }

    @Test
    public void transformType() {
        EpisodeUnifiedFilters episodeUnifiedFilters = new EpisodeUnifiedFilters();
        episodeUnifiedFilters.setTypes(Collections.singletonList("IndiaTbPrivate"));
        EpisodeUnifiedSearchRequest episodeUnifiedSearchRequest = new EpisodeUnifiedSearchRequest(new EpisodeSearchRequest(), episodeUnifiedFilters);

        EpisodeSearchRequest searchRequest = uppFiltersToEpisodeSearchFields.convert(episodeUnifiedSearchRequest);
        Assert.assertEquals(Collections.singletonList("IndiaTbPrivate"), searchRequest.getSearch().getMust().get(FieldConstants.EPISODE_FIELD_TYPE_OF_EPISODE));
    }

    @Test
    public void transformDiseaseId() {
        EpisodeUnifiedFilters episodeUnifiedFilters = new EpisodeUnifiedFilters();
        episodeUnifiedFilters.setDiseaseId(Collections.singletonList(1L));
        EpisodeUnifiedSearchRequest episodeUnifiedSearchRequest = new EpisodeUnifiedSearchRequest(new EpisodeSearchRequest(), episodeUnifiedFilters);

        EpisodeSearchRequest searchRequest = uppFiltersToEpisodeSearchFields.convert(episodeUnifiedSearchRequest);
        Assert.assertEquals(Collections.singletonList(1L), searchRequest.getSearch().getMust().get(FieldConstants.EPISODE_FIELD_DISEASE_ID));
    }

    @Test
    public void transformDiseaseIdOptions() {
        EpisodeUnifiedFilters episodeUnifiedFilters = new EpisodeUnifiedFilters();
        episodeUnifiedFilters.setDiseaseIdOptions("1,2");
        EpisodeUnifiedSearchRequest episodeUnifiedSearchRequest = new EpisodeUnifiedSearchRequest(new EpisodeSearchRequest(), episodeUnifiedFilters);

        EpisodeSearchRequest searchRequest = uppFiltersToEpisodeSearchFields.convert(episodeUnifiedSearchRequest);
        Assert.assertEquals("1,2", searchRequest.getSearch().getMust().get(FieldConstants.EPISODE_FIELD_DISEASE_ID_OPTIONS));
    }

    @Test
    public void transformDateTypeTreatmentStartTimeStamp() {
        EpisodeUnifiedFilters episodeUnifiedFilters = new EpisodeUnifiedFilters();
        episodeUnifiedFilters.setDateType(FieldConstants.EPISODE_FIELD_TREATMENT_START_TIME_STAMP);
        episodeUnifiedFilters.setDateStart("2022-10-22 18:30:00");
        episodeUnifiedFilters.setDateEnd("2022-10-24 18:30:00");
        EpisodeUnifiedSearchRequest episodeUnifiedSearchRequest = new EpisodeUnifiedSearchRequest(new EpisodeSearchRequest(), episodeUnifiedFilters);

        EpisodeSearchRequest searchRequest = uppFiltersToEpisodeSearchFields.convert(episodeUnifiedSearchRequest);
        Assert.assertNotNull(searchRequest.getRange().get(FieldConstants.EPISODE_FIELD_TREATMENT_START_TIME_STAMP));
    }

}
