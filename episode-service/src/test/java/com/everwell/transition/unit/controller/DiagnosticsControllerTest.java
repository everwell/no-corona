package com.everwell.transition.unit.controller;

import com.everwell.transition.BaseTest;
import com.everwell.transition.constants.Constants;
import com.everwell.transition.constants.diagnostics.RequestConstants;
import com.everwell.transition.controller.DiagnosticsController;
import com.everwell.transition.enums.diagnostics.TestTypeEnum;
import com.everwell.transition.exceptions.CustomExceptionHandler;
import com.everwell.transition.exceptions.ValidationException;
import com.everwell.transition.model.db.Client;
import com.everwell.transition.model.dto.diagnostics.TypeRequest;
import com.everwell.transition.model.request.diagnostics.TestRequestV2;
import com.everwell.transition.model.response.Response;
import com.everwell.transition.model.response.diagnostics.AddTestResponseV2;
import com.everwell.transition.model.request.diagnostics.GetSampleIdListRequest;
import com.everwell.transition.model.response.Response;
import com.everwell.transition.model.response.diagnostics.EpisodeTestsResponse;
import com.everwell.transition.model.response.diagnostics.TestResponse;
import com.everwell.transition.service.DiagnosticsService;
import com.everwell.transition.service.impl.ClientServiceImpl;
import com.everwell.transition.utils.Utils;
import net.minidev.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import springfox.documentation.spring.web.json.Json;

import java.util.*;
import java.util.HashMap;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class DiagnosticsControllerTest extends BaseTest {

    private MockMvc mockMvc;

    @Spy
    @InjectMocks
    private DiagnosticsController diagnosticsController;

    @Mock
    DiagnosticsService diagnosticsService;

    @Mock
    ClientServiceImpl clientService;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(diagnosticsController)
                .setControllerAdvice(new CustomExceptionHandler())
                .build();
    }

    @Test
    public void testTestConnection() throws Exception {
        String uri = "/diagnostics/test-connection";
        String successfulResponse = "Successful Response.";
        ResponseEntity<Response<String>> response
                = new ResponseEntity<>(
                new Response<>(successfulResponse, true),
                HttpStatus.OK
        );

        when(diagnosticsService.testConnection()).thenReturn(response);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(uri)
                )
                .andExpect(status().isOk());
    }

    @Test
    public void testGetTestsByEpisodeId() throws Exception {
        long episodeId = 123L;
        String uri = "/v2/tests?episodeId=" + episodeId;
        EpisodeTestsResponse episodeTestsResponse = new EpisodeTestsResponse(null, "test", 12, "123456", "Male");

        when(diagnosticsService.getTestsByEpisode(eq(123L))).thenReturn(episodeTestsResponse);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers
                        .jsonPath("$.data.name")
                        .value("test"));
    }

    @Test
    public void testGetTestsById() throws Exception {
        long testRequestId = 123L;
        String uri = "/v2/test?testRequestId=" + testRequestId;
        TestResponse testResponse = new TestResponse();

        when(diagnosticsService.getTestById(any())).thenReturn(testResponse);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(uri)
                ).andExpect(status().isOk());
    }

    @Test
    public void kpiFilter() throws Exception {
        String kpiUri = "/v1/diagnostics/kpi";
        String input = "1";
        Map<String, Object> kpiFilterRequest = new HashMap<>();
        kpiFilterRequest.put("testRequestIds", input);
        Map<String, Object> kpiResponse = new HashMap<>();
        ResponseEntity<Response<Map<String, Object>>> response
                = new ResponseEntity<>(new Response<>(true, kpiResponse), HttpStatus.OK);
        when(diagnosticsService.filterKPI(any())).thenReturn(response);
        mockMvc.
                perform(
                        MockMvcRequestBuilders.
                                post(kpiUri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(kpiFilterRequest))
                )
                .andExpect(status().isOk());
    }

    @Test
    public void testGetTestsByIdNoTestRequestId() throws Exception {
        String uri = "/v2/test?testRequestId=";

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(uri)
                )
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testEditTest() throws Exception {
        String uri = "/v2/test";
        Map<String, Object> requestInput = new HashMap<>();
        Map<String, Object> response = new HashMap<>();
        when(diagnosticsService.editTest(any())).thenReturn(response);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .put(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(requestInput))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk());
    }
    
    @Test
    public void getQRCodeValidation() throws Exception {
        String qrUri = "/v1/diagnostics/qr-code";
        String testQRCode = "A123";
        Map<String, Object> qrResponse = new HashMap<>();
        ResponseEntity<Response<Map<String, Object>>> response
                = new ResponseEntity<>(new Response<>(true, qrResponse), HttpStatus.OK);
        when(diagnosticsService.qrCodeValidation(any())).thenReturn(response);
        mockMvc.
                perform(
                        MockMvcRequestBuilders.
                                get(qrUri)
                                .queryParam(RequestConstants.QUERY_PARAM_QR_CODE, testQRCode)
                )
                .andExpect(status().isOk());
    }

    @Test
    public void addTest() throws Exception {
        String uri = "/v2/test";
        TestRequestV2 testRequest = new TestRequestV2();
        testRequest.setEntityId("1");
        testRequest.setClientId(1L);
        testRequest.setTypeRequestMap(new HashMap<TestTypeEnum, TypeRequest>() {
        });
        AddTestResponseV2 testResponse = new AddTestResponseV2();
        testResponse.setStatus("Results Available");
        ResponseEntity<Response<AddTestResponseV2>> response
                = new ResponseEntity<>(new Response<>(true, testResponse), HttpStatus.OK);
        when(clientService.getClientByTokenOrDefault()).thenReturn(new Client(1L, "client", "client", new Date()));
        when(diagnosticsService.addTest(any(), any())).thenReturn(response);

        mockMvc.
                perform(
                        MockMvcRequestBuilders.
                                post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(testRequest))
                )
                .andExpect(status().isOk());
        }

    @Test
    public void addTestBadRequest() throws Exception {
        String uri = "/v2/test";
        TestRequestV2 testRequest = new TestRequestV2();
        testRequest.setClientId(1L);
        testRequest.setTypeRequestMap(new HashMap<TestTypeEnum, TypeRequest>() {
        });

        mockMvc.
                perform(
                        MockMvcRequestBuilders.
                                post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(testRequest))
                )
                .andExpect(status().isBadRequest());
    }
    @Test
    public void getSampleBySampleId() throws Exception {
        String qrUri = "/v1/diagnostics/sample";
        Map<String, Object> sampleResponse = new HashMap<>();
        ResponseEntity<Response<Map<String, Object>>> response
                = new ResponseEntity<>(new Response<>(true, sampleResponse), HttpStatus.OK);
        when(diagnosticsService.getSampleByIdOrQRCode(any(), any(), any(), any())).thenReturn(response);
        mockMvc.
                perform(
                        MockMvcRequestBuilders.
                                get(qrUri)
                                .param(RequestConstants.QUERY_PARAM_BY, "sampleId")
                                .param(RequestConstants.QUERY_PARAM_SAMPLE_ID, "1")
                                .param(RequestConstants.QUERY_PARAM_JOURNEY_DETAILS,String.valueOf(false))
                )
                .andExpect(status().isOk());
    }

    @Test
    public void getSampleByQRCode() throws Exception {
        String qrUri = "/v1/diagnostics/sample";
        Map<String, Object> sampleResponse = new HashMap<>();
        ResponseEntity<Response<Map<String, Object>>> response
                = new ResponseEntity<>(new Response<>(true, sampleResponse), HttpStatus.OK);
        when(diagnosticsService.getSampleByIdOrQRCode(any(), any(), any(), any())).thenReturn(response);
        mockMvc.
                perform(
                        MockMvcRequestBuilders.
                                get(qrUri)
                                .param(RequestConstants.QUERY_PARAM_BY, "qrCode")
                                .param(RequestConstants.QUERY_PARAM_QR_CODE, "KKAh1o")
                                .param(RequestConstants.QUERY_PARAM_JOURNEY_DETAILS,String.valueOf(false))
                )
                .andExpect(status().isOk());
    }

    @Test
    public void generateQrPdf() throws Exception {
        String uri = "/v1/diagnostics/qr-code/generate-pdf";
        Map<String, Object> request = new HashMap<>();
        Map<String, Object> response = new HashMap<>();
        ResponseEntity<Response<Map<String, Object>>> diagnosticsResponse
                = new ResponseEntity<>(new Response<>(true, response), HttpStatus.OK);
        when(diagnosticsService.generateQrPdf(any())).thenReturn(diagnosticsResponse);
        mockMvc.
                perform(
                        MockMvcRequestBuilders.
                                post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(request))
                )
                .andExpect(status().isOk());
    }

    @Test
    public void testGetSampleIdList() throws Exception {
        String uri = "/v1/sampleIds";
        GetSampleIdListRequest request = new GetSampleIdListRequest();
        EpisodeTestsResponse episodeTestsResponse = new EpisodeTestsResponse(null, "test", 12, "123456", "Male");

        when(diagnosticsService.getSampleIdListByEpisodeId(any())).thenReturn(Collections.singletonList(1L));

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(request))
                )
                .andExpect(status().isOk());
    }

    @Test
    public void testDeleteTest() throws Exception {
        String uri = "/v1/test?testRequestId=123";
        String response = "Test Deleted successfully!";
        when(diagnosticsService.deleteTest(eq(123L))).thenReturn(response);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .delete(uri)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers
                        .jsonPath("$.data")
                        .value("Test Deleted successfully!"));
    }
}
