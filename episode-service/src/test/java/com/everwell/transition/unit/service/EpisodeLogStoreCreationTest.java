package com.everwell.transition.unit.service;

import com.everwell.transition.BaseTest;
import com.everwell.transition.model.db.EpisodeLogStore;
import com.everwell.transition.postconstruct.EpisodeLogStoreCreation;
import com.everwell.transition.repositories.EpisodeLogStoreRepository;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.when;

public class EpisodeLogStoreCreationTest extends BaseTest {

    @InjectMocks
    private EpisodeLogStoreCreation episodeLogStoreCreation;

    @Mock
    private EpisodeLogStoreRepository episodeLogStoreRepository;

    @Test
    public void test() {
        when(episodeLogStoreRepository.findAll()).thenReturn(getStore());
        episodeLogStoreCreation.initLogStore();
        List<EpisodeLogStore> config = new ArrayList<>(episodeLogStoreCreation.getEpisodeLogStoreMap().values());
        Assertions.assertEquals(config.get(0).getCategory(), getStore().get(0).getCategory());
        Assertions.assertEquals(config.get(0).getCategoryGroup(), getStore().get(0).getCategoryGroup());
    }

    List<EpisodeLogStore> getStore() {
        List<EpisodeLogStore> config = Arrays.asList(
            new EpisodeLogStore(1L, "Manual_Doses_Added", "Dosage level changes")
        );
        return config;
    }
}
