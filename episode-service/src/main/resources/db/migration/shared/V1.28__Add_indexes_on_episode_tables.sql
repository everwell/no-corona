create index if not exists dskm_disease_Stage_ids_index
    on disease_stage_key_mapping(disease_stage_id);

create index if not exists dsm_diseaseId_index
    on disease_stage_mapping(disease_template_id);

create index if not exists dsm_stageId_index
    on disease_stage_mapping(stage_id);

create index if not exists ea_episodeId_index
    on episode_association(episode_id);

create index if not exists ehl_episodeId_index
    on episode_hierarchy_linkage(episode_id);

create index if not exists episode_id_clientId_deleted_index
    on episode(id, client_id, deleted);

create index if not exists esd_episodeStageId_index
    on episode_stage_data(episode_stage_id);

create index if not exists es_episodeId_index
    on episode_stage(episode_id);

create index if not exists tp_diseaseStageMapId_index
    on tab_permission(disease_stage_mapping_id);

-- Index for get by person Id
create index if not exists episode_personId_clientId_deleted_index
    on episode(person_id, client_id, deleted);