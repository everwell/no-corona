-- Episode table
drop table if exists episode;

create table if not exists episode
(
	id bigserial not null
		constraint episode_pkey
			primary key,
	client_id integer,
	person_id integer,
	deleted boolean,
	disease_id integer,
	disease_id_options text,
	created_date timestamp,
	start_date timestamp,
	end_date timestamp,
	last_activity_date timestamp,
	current_tags text,
	risk_status text,
	current_stage_id integer,
	added_by bigint,
	type_of_episode varchar(255)
);

create index if not exists episode_id_clientid_deleted_index
	on episode (id, client_id, deleted);

create index if not exists episode_personid_clientid_deleted_index
	on episode (person_id, client_id, deleted);

-- Episode Stage table

Drop table if exists episode_stage;

create table if not exists episode_stage
(
	id bigserial not null
		constraint episode_stages_pkey
			primary key,
	episode_id integer,
	stage_id integer,
	start_date timestamp,
	end_date timestamp
);

create index if not exists es_episodeid_index
	on episode_stage (episode_id);

-- Episode stage data table

drop table if exists episode_stage_data;

create table if not exists episode_stage_data
(
	id bigserial not null
		constraint episode_stage_data_pkey
			primary key,
	episode_stage_id integer,
	value varchar(255),
	field_id integer
);

create index if not exists esd_episodestageid_index
	on episode_stage_data (episode_stage_id);

-- Episode Association table

drop table if exists episode_association;

create table if not exists episode_association
(
	id bigserial not null
		constraint episode_associations_pkey
			primary key,
	episode_id integer,
	value varchar(255),
	association_id integer
);

create index if not exists ea_episodeid_index
	on episode_association (episode_id);

-- Episode HierarchyMap table

drop table if exists episode_hierarchy_linkage;

create table if not exists episode_hierarchy_linkage
(
	id bigserial not null
		constraint episode_hierarchy_linkages_pkey
			primary key,
	episode_id integer,
	hierarchy_id integer,
	relation_id integer
);

create index if not exists ehl_episodeid_index
	on episode_hierarchy_linkage (episode_id);



