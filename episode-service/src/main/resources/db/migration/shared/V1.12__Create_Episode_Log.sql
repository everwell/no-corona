create table if not exists episode_log
(
    id bigserial not null
    constraint episode_log_pkey
    primary key,
    action_taken text,
    added_by bigint,
    added_on timestamp,
    category text,
    comments text,
    episode_id bigint
);

create index if not exists episode_log_category_index
    on episode_log (category);

create index if not exists episode_log_episode_id_index
    on episode_log (episode_id);
