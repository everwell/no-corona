CREATE TABLE if not exists rules (
  id bigint NOT NULL,
  action character varying(255) NULL,
  condition character varying(255) NULL,
  description character varying(255) NULL,
  priority integer NULL,
  rule_namespace character varying(255) NULL
);