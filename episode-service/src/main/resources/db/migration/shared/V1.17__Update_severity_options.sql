UPDATE adverse_reaction SET severity = 'Mild symptoms - causing no or minimal interference with usual social & functional activities with intervention not indicated'
where severity = 'Mild symptoms -  causing no or minimal interference with usual activities';

UPDATE adverse_reaction SET severity = 'Moderate symptoms - causing greater than minimal interference with usual social & functional activities with intervention indicated'
where severity = 'Moderate symptoms - causing greater than minimal interference with usual activities';

UPDATE adverse_reaction SET severity = 'Severe symptoms - causing inability to perform usual social and functional activities with intervention or hospitalization indicated'
where severity = 'Severe symptoms - causing inability to perform usual activities';

UPDATE adverse_reaction SET severity = 'Potentially life-threatening symptoms - causing inability to perform basic self- care functions with interventions indicated to prevent permanent impairment, persistent disability or death'
where severity = 'Fatal or life threatening';

