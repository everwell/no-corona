alter table disease_stage_key_mapping
    drop column if exists field_id;

alter table if exists disease_stage_key_mapping
    rename column field_key_id to field_id;