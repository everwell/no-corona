DO $$

DECLARE
	v_d2c_client_id INTEGER;
	v_new_disease_id INTEGER;
	v_new_disease_ids INTEGER [];
	old_disease_ids INTEGER[];
	v_new_stage_ids INTEGER[];
	v_new_DS_mapping_ids INTEGER[];
	v_new_field_ids INTEGER[];
	v_required_field_both_stages INTEGER[];
	v_field_ids_both_stages INTEGER[];
	DS_id INTEGER;
	new_field_id INTEGER;
	existing_field_id INTEGER;
	on_treatement_stage INTEGER;
	off_treatement_stage INTEGER;
	v_new_DS_mapping_id_on_treatment INTEGER;
	v_new_DS_mapping_id_off_treatment INTEGER;
	_disease_template_name_list varchar[] := array['F2 Disease Template'];

BEGIN
    FOR i in 1 .. array_upper(_disease_template_name_list, 1)
        LOOP
        SELECT id INTO v_d2c_client_id FROM client WHERE  id=63;
        IF v_d2c_client_id > 0 THEN
            SELECT id INTO v_new_disease_id FROM disease_template where disease_name=_disease_template_name_list[i] and client_id=63;
            IF v_new_disease_id  is NULL THEN
            -- New diseases
                WITH new_diseases AS (
                    INSERT INTO disease_template (id, disease_name, client_id)
                        VALUES
                            ((select COALESCE(MAX(id), 0) + 1 from disease_template),_disease_template_name_list[i], v_d2c_client_id)
                        RETURNING id)
                    SELECT id INTO v_new_disease_id FROM new_diseases;
            END IF;
            SELECT id into on_treatement_stage FROM stages where stage_name = 'ON_TREATMENT' and stage_display_name ='Treatment';
            IF on_treatement_stage  is NULL THEN
                WITH new_stage_1 AS (
                    INSERT INTO stages (id, stage_name, stage_display_name) VALUES
                    ((select COALESCE(MAX(id), 0) + 1 from stages),'ON_TREATMENT', 'Treatment')
                    RETURNING id)
                SELECT id INTO on_treatement_stage from new_stage_1;
            END IF;
            SELECT id into off_treatement_stage FROM stages where stage_name = 'OFF_TREATMENT' and stage_display_name ='Off Treatment';
            IF off_treatement_stage  is NULL THEN
                WITH new_stage_2 AS (
                    INSERT INTO stages (id, stage_name, stage_display_name) VALUES
                    ((select COALESCE(MAX(id), 0) + 1 from stages),'OFF_TREATMENT', 'Off Treatment')
                    RETURNING id)
                SELECT id INTO off_treatement_stage from new_stage_2;
            END IF;

            -- Add disease_stage_mapping with newly created diseases and stage
            SELECT id into v_new_DS_mapping_id_on_treatment from disease_stage_mapping where disease_template_id = v_new_disease_id and stage_id = on_treatement_stage;
            IF v_new_DS_mapping_id_on_treatment  is NULL THEN
                WITH new_DS_mapping_1 AS (
                    INSERT INTO disease_stage_mapping (id, disease_template_id, stage_id)
                    VALUES
                        ((select COALESCE(MAX(id), 0) + 1 from disease_stage_mapping), v_new_disease_id, on_treatement_stage)
                    RETURNING id)
                SELECT id INTO v_new_DS_mapping_id_on_treatment FROM new_DS_mapping_1;
            END IF;

            SELECT id into v_new_DS_mapping_id_off_treatment from disease_stage_mapping where disease_template_id = v_new_disease_id and stage_id = off_treatement_stage;
            IF v_new_DS_mapping_id_off_treatment  is NULL  THEN
                WITH new_DS_mapping_2 AS (
                    INSERT INTO disease_stage_mapping (id, disease_template_id, stage_id)
                    VALUES
                        ((select COALESCE(MAX(id), 0) + 1 from disease_stage_mapping), v_new_disease_id, off_treatement_stage)
                    RETURNING id)
                SELECT id INTO v_new_DS_mapping_id_off_treatment FROM new_DS_mapping_2;
            END IF;

            SELECT array_agg(id) INTO v_field_ids_both_stages FROM field where key in ('lastName', 'isDeleted', 'emailList', 'gender', 'mobileList', 'dateOfBirth', 'treatmentOutcome', 'address', 'city', 'pincode', 'contactPersonName', 'contactPersonAddress', 'contactPersonPhone', 'maritalStatus', 'langugage', 'iamStartDate');

            SELECT array_agg(id) INTO v_required_field_both_stages FROM field where key in ('firstName', 'primaryPhoneNumber');

            FOREACH existing_field_id IN ARRAY v_field_ids_both_stages
                LOOP
                    IF NOT EXISTS(SELECT id from disease_stage_key_mapping where disease_stage_id=v_new_DS_mapping_id_on_treatment and field_id=existing_field_id) THEN
                        INSERT INTO disease_stage_key_mapping (id, disease_stage_id, required, deleted, default_value, field_id)
                        VALUES ((select COALESCE(MAX(id), 0) + 1 from disease_stage_key_mapping), v_new_DS_mapping_id_on_treatment, false, false, null, existing_field_id);
                    END IF;
                    IF NOT EXISTS(SELECT id from disease_stage_key_mapping where disease_stage_id=v_new_DS_mapping_id_off_treatment and field_id=existing_field_id) THEN
                        INSERT INTO disease_stage_key_mapping (id, disease_stage_id, required, deleted, default_value, field_id)
                        VALUES ((select COALESCE(MAX(id), 0) + 1 from disease_stage_key_mapping), v_new_DS_mapping_id_off_treatment, false, false, null, existing_field_id);
                    END IF;
                END LOOP;


            FOREACH existing_field_id IN ARRAY v_required_field_both_stages
                LOOP
                    IF NOT EXISTS(SELECT id from disease_stage_key_mapping where disease_stage_id=v_new_DS_mapping_id_on_treatment and field_id=existing_field_id) THEN
                        INSERT INTO disease_stage_key_mapping (id, disease_stage_id, required, deleted, default_value, field_id)
                        VALUES ((select COALESCE(MAX(id), 0) + 1 from disease_stage_key_mapping), v_new_DS_mapping_id_on_treatment, true, false, null, existing_field_id);
                    END IF;
                    IF NOT EXISTS(SELECT id from disease_stage_key_mapping where disease_stage_id=v_new_DS_mapping_id_off_treatment and field_id=existing_field_id) THEN
                        INSERT INTO disease_stage_key_mapping (id, disease_stage_id, required, deleted, default_value, field_id)
                        VALUES ((select COALESCE(MAX(id), 0) + 1 from disease_stage_key_mapping), v_new_DS_mapping_id_off_treatment, true, false, null, existing_field_id);
                    END IF;
                END LOOP;
        END IF;
    END LOOP;
END $$;

