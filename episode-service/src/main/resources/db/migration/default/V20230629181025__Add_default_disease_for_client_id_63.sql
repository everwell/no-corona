UPDATE disease_template
SET is_default = false;

UPDATE disease_template
SET is_default = true
where disease_name = 'F2 Disease Template';