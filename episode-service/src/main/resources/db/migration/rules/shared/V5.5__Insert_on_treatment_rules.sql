INSERT INTO public.rules (id, action, condition, description, priority, rule_namespace)
VALUES (12, 'output.put("ToStage", "DIAGNOSED_ON_TREATMENT");',
        'return null != input.get("Stage") && input.get("Stage").equals("DIAGNOSED_NOT_ON_TREATMENT") ' ||
        '&& (null == input.get("TreatmentOutcome") || input.get("TreatmentOutcome").equals(""));',
        'TB Stage Transitions from DIAGNOSED_NOT_ON_TREATMENT to DIAGNOSED_ON_TREATMENT', 1,
        'STAGE_TRANSITION')
ON CONFLICT DO NOTHING;

INSERT INTO public.rules (id, action, condition, description, priority, rule_namespace)
VALUES (13, 'output.put("ToStage", "DIAGNOSED_ON_TREATMENT");',
        'return null != input.get("Stage") && input.get("Stage").equals("PRESUMPTIVE_OPEN") ' ||
        '&& (null != input.get("DiagnosisDate"));',
        'TB Stage Transitions from PRESUMPTIVE_OPEN to DIAGNOSED_ON_TREATMENT', 0,
        'STAGE_TRANSITION')
ON CONFLICT DO NOTHING;