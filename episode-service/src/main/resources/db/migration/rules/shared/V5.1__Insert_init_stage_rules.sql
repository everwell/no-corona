INSERT INTO public.rules (id, action, condition, description, priority, rule_namespace)
VALUES (1,
        'output.put("ToStage", "PRESUMPTIVE_OPEN");',
        'var stage = input.get("Stage");' ||
        'var outcome = input.get("TreatmentOutcome");' ||
        'return (stage == null || stage.equals("") || stage.equals("PRESUMPTIVE_OPEN")) && (null == outcome || outcome.equals(""));',
        'TB Stage Transitions to PRESUMPTIVE_OPEN', 1,
        'STAGE_TRANSITION')
        ON CONFLICT DO NOTHING;

INSERT INTO public.rules (id, action, condition, description, priority, rule_namespace)
VALUES (2,
        'output.put("ToStage", "PRESUMPTIVE_CLOSED");',
        'var stage = input.get("Stage");' ||
        'var outcome = input.get("TreatmentOutcome");' ||
        'return null != stage && stage.equals("PRESUMPTIVE_OPEN") && null != outcome && !outcome.equals("");',
        'TB Stage Transitions from PRESUMPTIVE_OPEN to PRESUMPTIVE_CLOSED', 1,
        'STAGE_TRANSITION')
        ON CONFLICT DO NOTHING;
