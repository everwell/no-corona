--- Fixing the mappings for weight and height

--- Delete the existing height and weight mappings

-- delete for disease stage ids where disease template = 1, 2, 3

delete  from disease_stage_key_mapping where field_id = (select id from field where key= 'height' and module = 'Person' limit 1)
and disease_stage_id in (
select id from disease_stage_mapping where disease_template_id in
(select id  from disease_template where disease_name in ('Drug Resistant Tuberculosis', 'Drug Sensitive Tuberculosis', 'Latent Tuberculosis Infection')));


delete from disease_stage_key_mapping where field_id = (select id from field where key= 'weight' and module = 'Person' limit 1)
and disease_stage_id in (
select id from disease_stage_mapping where disease_template_id in
(select id  from disease_template where disease_name in ('Drug Resistant Tuberculosis', 'Drug Sensitive Tuberculosis', 'Latent Tuberculosis Infection')));


--- Add the mappings for tpt case which for public patients can only happen through presumptive open
DO
$do$
    declare
        disease_stage_ids int[] := ARRAY(SELECT id FROM disease_stage_mapping
                                         where stage_id in (SELECT id FROM stages where stage_name in ('PRESUMPTIVE_OPEN_PUBLIC'))
                                         and disease_template_id in (select id from disease_template dt where disease_name in ('Latent Tuberculosis Infection')));

        var int;
    BEGIN
        foreach var in array disease_stage_ids loop
        IF NOT EXISTS (SELECT * from disease_stage_key_mapping where disease_stage_id = var and field_id = (select id from field where key = 'weight' and module = 'Person' limit 1) ) THEN
          insert into disease_stage_key_mapping (id, default_value, deleted, disease_stage_id, required, field_id) VALUES ((select max(id) + 1 from disease_stage_key_mapping), null, false, var, true, (select id from field where key = 'weight' and module = 'Person' limit 1));
        end if;

        IF NOT EXISTS (SELECT * from disease_stage_key_mapping where disease_stage_id = var and field_id = (select id from field where key = 'height' and module = 'Person' limit 1) ) THEN
          insert into disease_stage_key_mapping (id, default_value, deleted, disease_stage_id, required, field_id) VALUES ((select max(id) + 1 from disease_stage_key_mapping), null, false, var, true, (select id from field where key = 'height' and module = 'Person' limit 1));
        end if;


    end loop ;
end;
$do$;


--- Add the mappings for rest of the stages and diseases
DO
$do$
    declare
        disease_stage_ids int[] := ARRAY(SELECT id FROM disease_stage_mapping
                                         where stage_id in (SELECT id FROM stages where stage_name in ('PRESUMPTIVE_OPEN_PRIVATE', 'DIAGNOSED_NOT_ON_TREATMENT', 'DIAGNOSED_ON_TREATMENT_PRIVATE', 'DIAGNOSED_ON_TREATMENT_PUBLIC'  ))
                                         and disease_template_id in (select id from disease_template dt where disease_name in ('Drug Resistant Tuberculosis', 'Drug Sensitive Tuberculosis', 'Latent Tuberculosis Infection')));

        var int;
    BEGIN
        foreach var in array disease_stage_ids loop
        IF NOT EXISTS (SELECT * from disease_stage_key_mapping where disease_stage_id = var and field_id = (select id from field where key = 'weight' and module = 'Person' limit 1) ) THEN
          insert into disease_stage_key_mapping (id, default_value, deleted, disease_stage_id, required, field_id) VALUES ((select max(id) + 1 from disease_stage_key_mapping), null, false, var, true, (select id from field where key = 'weight' and module = 'Person' limit 1));
        end if;

        IF NOT EXISTS (SELECT * from disease_stage_key_mapping where disease_stage_id = var and field_id = (select id from field where key = 'height' and module = 'Person' limit 1) ) THEN
          insert into disease_stage_key_mapping (id, default_value, deleted, disease_stage_id, required, field_id) VALUES ((select max(id) + 1 from disease_stage_key_mapping), null, false, var, true, (select id from field where key = 'height' and module = 'Person' limit 1));
        end if;


    end loop ;
end;
$do$;