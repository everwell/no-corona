

INSERT INTO public.trigger(client_id, default_template_id, event_name, function_name, notification_type, template_ids, trigger_id, vendor_id, time_zone)
	VALUES (
	 171,
	 23,
	 'addUserRelationNotification',
	 'getAddUserRelationPnDtos',
	 'PushNotification',
	 23,
	 20,
	 20,
	 'Asia/Kolkata');

SELECT setval('trigger_id_seq', (SELECT max(id) FROM trigger));