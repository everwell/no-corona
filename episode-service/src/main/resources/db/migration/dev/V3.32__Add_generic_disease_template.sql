---Add new generic disease template with presumptive open and close

DO
$do$
    DEClARE
        _client_id_list int[] := array[171];
        _client_id int;
        _disease_template_name_list varchar[] := array['Generic Disease'];
        _disease_template_id int;
        _stage_name_list varchar[] := array['PRESUMPTIVE_OPEN', 'PRESUMPTIVE_CLOSED'];
        _stage_display_name_list varchar[] := array['Presumptive (Open)', 'Presumptive (Closed)'];
        _stage_id int;
        _stage_id_list int[] DEFAULT '{}';
        _disease_stage_id_list int[] DEFAULT '{}';
        _disease_stage_id int;
        _required_fields varchar[] := array['firstName', 'mobileList'];
        _not_required_fields varchar[] := array['lastName', 'gender', 'address','dateOfBirth', 'maritalStatus', 'height', 'weight', 'emailList'];
        _required_field_ids int[];
        _not_required_field_ids int[];
        _field_id int;
    BEGIN
        FOR i IN 1 .. array_upper(_stage_name_list, 1)
            LOOP
                 SELECT id into _stage_id from stages where stage_name = _stage_name_list[i] and stage_display_name = _stage_display_name_list[i];
                    IF _stage_id  is NULL THEN
                        WITH new_stage AS (
                            INSERT INTO stages (id, stage_name, stage_display_name) VALUES
                            ((select max(id) + 1 from stages),_stage_name_list[i], _stage_display_name_list[i])
                            RETURNING id)
                        SELECT id INTO _stage_id from new_stage;
                    END IF;
                  _stage_id_list := _stage_id_list || _stage_id;
            END LOOP;
         foreach _client_id  in array _client_id_list loop
              FOR i IN 1 .. array_upper(_disease_template_name_list, 1) loop
                SELECT id INTO _disease_template_id FROM disease_template WHERE disease_name = _disease_template_name_list[i] AND client_id = _client_id;
                IF _disease_template_id is NULL THEN
                        With new_disease_template As (
                            INSERT  INTO disease_template (id, client_id, disease_name) values ((select max(id) +1 from disease_template), _client_id, _disease_template_name_list[i])
                            RETURNING  id)
                    SELECT id INTO _disease_template_id FROM new_disease_template;
                END IF;
               FOREACH  _stage_id in array _stage_id_list LOOP
                   SELECT id INTO _disease_stage_id FROM disease_stage_mapping WHERE stage_id = _stage_id AND disease_template_id = _disease_template_id;
                   IF _disease_stage_id is NULL THEN
                       With new_disease_stage As (
                           INSERT INTO disease_stage_mapping(id, disease_template_id, stage_id) VALUES
                               ((select max(id) +1 from disease_stage_mapping), _disease_template_id, _stage_id)
                           RETURNING id
                       )
                       select id INTO _disease_stage_id FROM new_disease_stage;
                   end if;
                   _disease_stage_id_list := _disease_stage_id_list || _disease_stage_id;
              END LOOP ;

              SELECT array_agg(id) INTO _not_required_field_ids FROM field where key = ANY(_not_required_fields);

              SELECT array_agg(id) INTO _required_field_ids FROM field where key = ANY(_required_fields);

              FOREACH  _disease_stage_id IN ARRAY _disease_stage_id_list LOOP
                  FOREACH _field_id IN ARRAY _not_required_field_ids LOOP
                    IF NOT EXISTS(SELECT id from disease_stage_key_mapping where disease_stage_id = _disease_stage_id and field_id = _field_id) THEN
                        INSERT INTO disease_stage_key_mapping (id, disease_stage_id, required, deleted, default_value, field_id)
                        VALUES ((select max(id)+1 from disease_stage_key_mapping), _disease_stage_id, false, false, null, _field_id);
                    END IF;
                  END LOOP;
                  FOREACH _field_id IN ARRAY _required_field_ids LOOP
                    IF NOT EXISTS(SELECT id from disease_stage_key_mapping where disease_stage_id = _disease_stage_id and field_id = _field_id) THEN
                        INSERT INTO disease_stage_key_mapping (id, disease_stage_id, required, deleted, default_value, field_id)
                        VALUES ((select max(id)+1 from disease_stage_key_mapping), _disease_stage_id, true, false, null, _field_id);
                    END IF;
                  END LOOP;
              END LOOP;
              end loop;
        end loop;
    END

$do$;
