INSERT INTO public.trigger(client_id, default_template_id, event_name, function_name, notification_type, template_ids, trigger_id, vendor_id, time_zone)
	VALUES (
	 171,
	 200,
	 'sendRegistrationOtp',
	 'sendRegistrationOtp',
	 'SMS',
	 200,
	 58,
	 3,
	 'Asia/Kolkata');

SELECT setval('trigger_id_seq', (SELECT max(id) FROM trigger));