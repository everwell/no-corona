package com.everwell.transition.postconstruct;

import com.everwell.transition.model.db.EpisodeTagStore;
import com.everwell.transition.repositories.EpisodeTagStoreRepository;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class EpisodeTagStoreCreation {

    @Autowired
    private EpisodeTagStoreRepository episodeTagStoreRepository;

    @Getter
    private Map<String, EpisodeTagStore> episodeTagStoreMap = new HashMap<>();

    @PostConstruct
    public void initTagStore() {
        List<EpisodeTagStore> episodeTagStoreList = episodeTagStoreRepository.findAll();
        episodeTagStoreMap = episodeTagStoreList.stream().collect(Collectors.toMap(
                EpisodeTagStore::getTagName,
                v -> v
        ));
    }

}
