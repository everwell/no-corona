package com.everwell.transition.processor;

import com.everwell.transition.binders.EpisodeNotificationBinder;
import com.everwell.transition.constants.Constants;
import com.everwell.transition.model.dto.GenericEvents;
import com.everwell.transition.model.dto.notification.NotificationTriggerDto;
import com.everwell.transition.model.dto.notification.NotificationTriggerEvent;
import com.everwell.transition.service.NotificationService;
import com.everwell.transition.utils.Utils;
import com.fasterxml.jackson.core.type.TypeReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.Message;
import org.springframework.messaging.handler.annotation.Header;

import java.io.IOException;

@EnableBinding(EpisodeNotificationBinder.class)
public class NotificationProcessor {

    @Autowired
    private EpisodeNotificationBinder episodeNotificationBinder;

    @Autowired
    private NotificationService notificationService;

    private static Logger LOGGER = LoggerFactory.getLogger(NotificationProcessor.class);

    @StreamListener(EpisodeNotificationBinder.EPISODE_NOTIFICATION_INPUT)
    public void notificationHandler(Message message, @Header(Constants.EVENT_CLIENT_ID) String idHeader) throws IOException, IllegalAccessException {
        String body = message.getPayload().toString();
        LOGGER.info("[notificationHandler] message received with body: " + body);
        GenericEvents<NotificationTriggerEvent> genericEventDto = Utils.jsonToObject(body, new TypeReference<GenericEvents<NotificationTriggerEvent>>(){});
        NotificationTriggerEvent notificationTriggerEvent = genericEventDto.getField();
        LOGGER.info("[notificationHandler] trigger received with details " + notificationTriggerEvent.toString());
        notificationService.sendSms(notificationTriggerEvent, idHeader, genericEventDto.getEventName());
    }
}
