package com.everwell.transition.binders;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface EpisodeEsInsertUpdateBinder {
    String EPISODE_ELASTIC_EXCHANGE = "ex.topic.elastic-add";

    @Output(EPISODE_ELASTIC_EXCHANGE)
    MessageChannel eventOutput();
}
