package com.everwell.transition.repositories;

import com.everwell.transition.model.db.AdverseEvent;
import com.everwell.transition.model.db.VendorConfig;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface VendorConfigRepository extends JpaRepository<VendorConfig, Long> {

    VendorConfig findByClientIdAndVendor(String clientId, String vendor);

}
