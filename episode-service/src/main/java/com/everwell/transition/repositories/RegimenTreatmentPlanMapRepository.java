package com.everwell.transition.repositories;

import com.everwell.transition.model.db.RegimenTreatmentPlanMap;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RegimenTreatmentPlanMapRepository extends JpaRepository<RegimenTreatmentPlanMap, Long> {

    List<RegimenTreatmentPlanMap> findAllByTreatmentPlanId(Long treatmentPlanId);

}
