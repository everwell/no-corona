package com.everwell.transition.repositories;

import com.everwell.transition.model.db.EpisodeAppConfig;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface EpisodeAppConfigRepository extends JpaRepository<EpisodeAppConfig, Long> {

    @Query(value = "SELECT e FROM EpisodeAppConfig e WHERE e.technology = :technology and e.deployment = :deployment")
    EpisodeAppConfig getAllByTechnologyAndDeployment(String technology,String deployment);
}
