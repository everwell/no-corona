package com.everwell.transition.repositories;

import com.everwell.transition.model.db.EpisodeStageData;
import com.everwell.transition.model.dto.dtoInterface.EpisodeFieldIdValueDtoInterface;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface EpisodeStageDataRepository extends JpaRepository<EpisodeStageData, Long> {

    List<EpisodeStageData> findAllByEpisodeStageIdIn(List<Long> episodeStageIds);

    @Query(value =  "Select e.Id as episodeId, esd.field_id as fieldId, esd.value as fieldValue " +
            "from (episode e join episode_stage es on e.Id = es.episode_id) " +
            "join episode_stage_data esd on es.id = esd.episode_stage_id " +
            "where esd.field_id IN :fieldIds and e.deleted != true and e.client_id = :clientId order by e.Id", nativeQuery = true)
    List<EpisodeFieldIdValueDtoInterface> getEpisodeFieldIdValueMapForFieldIdsInAndClientId(List<Long> fieldIds, Long clientId);
}
