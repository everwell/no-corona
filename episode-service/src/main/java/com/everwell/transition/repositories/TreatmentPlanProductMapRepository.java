package com.everwell.transition.repositories;

import com.everwell.transition.model.db.TreatmentPlanProductMap;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDateTime;
import java.util.List;

public interface TreatmentPlanProductMapRepository extends JpaRepository<TreatmentPlanProductMap, Long> {

    List<TreatmentPlanProductMap> findAllByTreatmentPlanId(Long treatmentPlanId);

    List<TreatmentPlanProductMap> findAllByTreatmentPlanIdIn(List<Long> treatmentPlanIds);

    @Query(value = "SELECT * FROM treatment_plan_product_map WHERE refill_due_date  >= :refillStartDate and refill_due_date <= :refillEndDate and end_date is null", nativeQuery = true)
    List<TreatmentPlanProductMap> findAllActiveInRefillDateRange(LocalDateTime refillStartDate, LocalDateTime refillEndDate);
}
