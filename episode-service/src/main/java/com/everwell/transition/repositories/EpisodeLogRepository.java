package com.everwell.transition.repositories;

import com.everwell.transition.model.db.EpisodeLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

public interface EpisodeLogRepository extends JpaRepository<EpisodeLog, Long> {

    List<EpisodeLog> getAllByEpisodeIdOrderByAddedOnDesc(Long episodeId);

    List<EpisodeLog> getAllByEpisodeIdInOrderByAddedOnDesc(List<Long> episodeIds);

    List<EpisodeLog> getAllByEpisodeIdInAndAddedByOrderByAddedOnDesc(List<Long> episodeIds, Long addedBy);

    List<EpisodeLog> getAllByEpisodeIdInAndCategoryInOrderByAddedOnDesc(List<Long> episodeIds, List<String> categories);

    List<EpisodeLog> getAllByEpisodeIdInAndCategoryInAndAddedByOrderByAddedOnDesc(List<Long> episodeIds, List<String> categories, Long addedBy);

    List<EpisodeLog> getAllByEpisodeIdAndCategoryInAndAddedOnAfterOrderByAddedOnAsc(Long episodeId, List<String> categories, LocalDateTime addedOnAfter);

    @Modifying
    @Transactional
    @Query(value = "DELETE from episode_log where id in (SELECT id FROM episode_log e WHERE e.episode_id = :episodeId and e.category in :categories)", nativeQuery = true)
    void deleteLogs(Long episodeId, List<String> categories);

    @Modifying
    @Transactional
    @Query(value = "DELETE from episode_log where id in (SELECT id FROM episode_log e WHERE e.episode_id = :episodeId and e.category in :categories and e.added_on > :deleteAllAfter)", nativeQuery = true)
    void deleteLogsAfterDate(Long episodeId, List<String> categories, LocalDateTime deleteAllAfter);

    List<EpisodeLog> getAllByIdIn(List<Long> ids);

    List<EpisodeLog> getAllByEpisodeIdAndCategoryIn(Long episodeId, List<String> categoryList);

}
