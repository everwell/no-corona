package com.everwell.transition.repositories;

import com.everwell.transition.model.db.EpisodeDocument;
import com.everwell.transition.model.db.EpisodeDocumentDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;
import java.util.List;

public interface EpisodeDocumentRepository extends JpaRepository<EpisodeDocument, Long> {

    @Query(value = "select * from episode_document ed inner join episode_document_details edd on edd.episode_document_id = ed.id where ed.episode_id in :episodeId", nativeQuery = true)
    List<EpisodeDocument> findAllByEpisodeIdCustom(List<Long> episodeId);

    @Query(value = "select * from (select * from episode_document ed where ed.episode_id in :episodeId and ed.added_on >= :fromDate and ed.added_on <= :endDate) eds inner join episode_document_details edd on edd.episode_document_id = eds.id where edd.is_approved = :approved", nativeQuery = true)
    List<EpisodeDocument> findAllByEpisodeIdAndAddedOnBetweenAndApprovedCustom(List<Long> episodeId, LocalDateTime fromDate, LocalDateTime endDate, Boolean approved);

    @Query(value = "select * from (select * from episode_document ed where ed.episode_id in :episodeId and ed.added_on >= :fromDate and ed.added_on <= :endDate) eds inner join episode_document_details edd on edd.episode_document_id = eds.id", nativeQuery = true)
    List<EpisodeDocument> findAllByEpisodeIdAndAddedOnBetweenCustom(List<Long> episodeId, LocalDateTime fromDate, LocalDateTime endDate);

}
