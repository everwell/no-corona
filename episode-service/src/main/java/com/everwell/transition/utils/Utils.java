package com.everwell.transition.utils;

import com.everwell.transition.constants.Constants;
import com.everwell.transition.exceptions.IllegalArgumentException;
import com.everwell.transition.exceptions.ValidationException;
import com.everwell.transition.model.response.Response;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.IOUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.*;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.apache.commons.io.IOUtils;
import org.springframework.http.ResponseEntity;
import java.util.stream.Collectors;

import static com.everwell.transition.constants.Constants.EMPTY_STRING;

public class Utils {

    private static ObjectMapper objectMapper = new ObjectMapper().registerModule(new JavaTimeModule());


    public static String asJsonString(final Object obj) {
        try {
            return objectMapper.writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    //for generic classes
    public static <T> T jsonToObject (String json, TypeReference<T> typeReference) throws IOException {
        //Disabling the FAIL_ON_UNKNOWN_PROPERTIES so that in case the input json has extra properties
        //other than the properties in the Class that needs to be converted to, jackson mapper doesn't throw exception
        objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        return objectMapper.readValue(json, typeReference);
    }

    public static Map<String, Object> objectToMap(Object obj) {
        Map<String, Object> map = new HashMap<>();
        if(null != obj) {
            map = objectMapper.convertValue(obj, Map.class);
        }
        return map;
    }

    public static String getFormattedDate(Date date, String format) {
        DateFormat dateFormat = new SimpleDateFormat(format);
        return dateFormat.format(date);
    }

    public static String getFormattedDateNew(LocalDateTime localDateTime, String format) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
        return localDateTime.format(formatter);
    }

    public static Date getCurrentDate() {
        return Utils.formatDate(new Date());
    }

    public static LocalDateTime getCurrentDateNew() {
        return formatDateNew(LocalDateTime.ofInstant(getCurrentDate().toInstant(), ZoneId.systemDefault()));
    }

    public static Date formatDate(Date date) {
        return formatDate(date, Constants.DATE_FORMAT);
    }

    public static LocalDateTime formatDateNew(LocalDateTime localDateTime) {
        return formatDateNew(localDateTime, Constants.DATE_FORMAT);
    }

    public static Date formatDate(Date date, String format) {
        return convertStringToDate(getFormattedDate(date, format), format);
    }

    public static LocalDateTime formatDateNew(LocalDateTime localDateTime, String format) {
        return convertStringToDateNew(getFormattedDateNew(localDateTime, format), format);
    }

    public static Date convertStringToDate(String date, String format) {
        try {
            SimpleDateFormat df = new SimpleDateFormat(format);
            df.setLenient(false);
            return df.parse(date);
        } catch (ParseException e) {
            throw new IllegalArgumentException("[convertStringToDate] Unable to parse date: " + date + " format: " + format);
        }
    }

    public static LocalDateTime convertStringToDateNew(String date, String format) {
        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
            return LocalDateTime.parse(date, formatter);
        } catch (DateTimeParseException ex) {
            throw new IllegalArgumentException("[convertStringToDateNew] Unable to parse date: " + date + " format: " + format);
        }
    }

    public static Date convertStringToDate(String date) throws ParseException {
        return convertStringToDate(date, Constants.DATE_FORMAT);
    }

    public static LocalDateTime convertStringToDateNew(String date) {
        return convertStringToDateNew(date, Constants.DATE_FORMAT);
    }

    public static String getFormattedDate(Date date) {
        return getFormattedDate(date, Constants.DATE_FORMAT);
    }

    public static String getFormattedDateNew(LocalDateTime localDateTime) {
        if (localDateTime == null) return "";
        return getFormattedDateNew(localDateTime, Constants.DATE_FORMAT);
    }

    public static String getFormattedDateNewEmptyAsNull(LocalDateTime localDateTime) {
        if (localDateTime == null) return null;
        return getFormattedDateNew(localDateTime, Constants.DATE_FORMAT);
    }

    public static String readRequestBody(InputStream inputStream) throws IOException {
        StringWriter writer = null;
        if (inputStream != null) {
            writer = new StringWriter();
            IOUtils.copy(inputStream, writer, "UTF-8");
        }
        return writer != null ? writer.toString() : null;
    }

    public static LocalDate toLocalDate(String date) {
        return toLocalDate(date, "dd-MM-yyyy");
    }

    public static LocalDate toLocalDate(String date, String format) {
        return LocalDate.parse(date, DateTimeFormatter.ofPattern(format));
    }


    public static LocalDateTime toLocalDateTimeWithNull(String date) {
        return toLocalDateTimeWithNull(date,"d-M-yyyy HH:mm:ss");
    }

    public static LocalDateTime toLocalDateTimeWithNull(String date, String format) {
        if(null == date || date.isEmpty()) return null;
        return LocalDateTime.parse(date, DateTimeFormatter.ofPattern(format));
    }

    public static boolean isDateBefore(LocalDateTime date1, LocalDateTime date2)
    {
        if (date1 == null || date2 == null)
            return false;
        return date1.isBefore(date2);
    }

    public static boolean isEmpty(Collection obj) {
        return obj == null || obj.isEmpty();
    }

    public static boolean isEmpty(String string) {
        return string == null || string.isEmpty();
    }

    public static boolean isEmpty(Object obj) {
        return obj == null || obj.toString().trim().isEmpty();
    }

    public static <T> List<List<T>> getBatches(List<T> collection, int batchSize) {
        List<List<T>> batches = new ArrayList<>();
        for (int i = 0; i < collection.size(); i += batchSize) {
            batches.add(collection.subList(i, Math.min(i + batchSize, collection.size())));
        }
        return batches;
    }

    public static <T> T processResponseEntity(ResponseEntity<Response<T>> responseEntity, String errorResponse) {
        if (null == responseEntity.getBody() || !responseEntity.getBody().isSuccess()){
            String message = responseEntity.getBody() != null ? responseEntity.getBody().getMessage() : null;
            if (StringUtils.hasLength(message)) {
                errorResponse = message;
            }
            throw new ValidationException(errorResponse);
        }
        return responseEntity.getBody().getData();
    }

    public static <T> boolean isResponseValid(ResponseEntity<Response<T>> responseEntity) {
        boolean isResponseValid = true;
        if (null == responseEntity || null == responseEntity.getBody() || !responseEntity.getBody().isSuccess()) {
            isResponseValid = false;
        }
        return isResponseValid;
    }

    public static LocalDate getCurrentDateInGivenTimeZone(String timeZone) {
        ZoneId zoneId = ZoneId.of(timeZone);
        return LocalDate.now(zoneId);
    }

    public static String convertDateTimeToSpecificZoneDateTime(ZonedDateTime zonedDateTime, ZoneOffset offset) {
        return convertDateTimeToSpecificZoneLocalDateTime(zonedDateTime, offset).format(DateTimeFormatter.ofPattern(Constants.DATE_FORMAT));
    }

    public static LocalDateTime convertDateTimeToSpecificZoneLocalDateTime(ZonedDateTime zonedDateTime, ZoneOffset offset) {
        return Instant.parse(zonedDateTime.toInstant().toString()).atOffset(offset).toLocalDateTime();
    }

    public static <T> Object invokeMethod(T classObject, T param1, T param2, String invokeMethod) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        return classObject.getClass().getMethod(invokeMethod, param1.getClass(), param2.getClass()).invoke(classObject, param1, param2);
    }
    
    public static LocalDate convertMilliSecToDateNew(Long millisecs) {
        Date date = new Date(millisecs);
        return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    }

    public static LocalDateTime getCurrentLocalDateUTCInGivenTimeZone(String timeZone) {
        LocalDate today = getCurrentDateInGivenTimeZone(timeZone);
        ZonedDateTime dayStartTime = today.atStartOfDay(ZoneId.of(timeZone));
        return Instant.parse(dayStartTime.toInstant().toString()).atOffset(ZoneOffset.UTC).toLocalDateTime();
    }

    public static List<String> getClassFieldName(Class<?> klass) {
        return Arrays.stream(klass.getDeclaredFields()).map(Field::getName).collect(Collectors.toList());
    }

    public static String convertLocalDateTimeToGivenTimeZone(LocalDateTime localDateTime, String timeZone) {
        ZonedDateTime zonedDateTime = localDateTime.atZone(ZoneId.systemDefault());
        ZoneOffset zoneOffset = ZoneId.of(timeZone).getRules().getOffset(localDateTime);
        return Instant.parse(zonedDateTime.toInstant().toString()).atOffset(zoneOffset).format(DateTimeFormatter.ofPattern(Constants.DATE_FORMAT_WITHOUT_TIME));
    }

    public static <T> boolean isEmptyOrAllNull(List<T> list) {
        return CollectionUtils.isEmpty(list) || list.stream().noneMatch(Objects::nonNull);
    }

    public static <T> boolean isEmptyOrAnyNull(List<T> list) {
        return CollectionUtils.isEmpty(list) || list.stream().anyMatch(Objects::isNull);
    }

    public static String convertObjectToStringWithNull(Object obj) {
        String value = null != obj ? String.valueOf(obj) : null;
        return value;
    }

    public static String convertObjectToStringWithEmpty(Object obj) {
        return null != obj ? String.valueOf(obj) : EMPTY_STRING;
    }

    public static String convertEpochToDate(Long epoch) {
        LocalDateTime localDateTime = Instant.ofEpochMilli(epoch).atZone(ZoneId.systemDefault()).toLocalDateTime();
        return getFormattedDateNew(localDateTime, Constants.DATE_FORMAT);
    }

    /**
     * This method converts the date from current format to expected format
     * @param date - date which needs to be converted
     * @param currentFormat - the format in which date is received
     * @param expectedFormat - the format in which date is expected
     * @return converted date string
     */
    public static String convertDateFromCurrentToExpectedFormatWithNull(String date, String currentFormat, String expectedFormat) {
        LocalDateTime localDateTime = toLocalDateTimeWithNull(date, currentFormat);
        String convertedDate = null;
        if(null != localDateTime) {
            convertedDate = getFormattedDateNew(localDateTime, expectedFormat);
        }
        return convertedDate;
    }
}
