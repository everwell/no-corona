package com.everwell.transition.handlers;

import com.everwell.transition.binders.EpisodeEventsBinder;
import com.everwell.transition.model.db.Client;
import com.everwell.transition.model.dto.EventDto;
import com.everwell.transition.model.dto.EventStreamingDto;
import com.everwell.transition.service.ClientService;
import com.everwell.transition.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;


import static com.everwell.transition.constants.Constants.ROUTING_KEY;
import static com.everwell.transition.constants.UserServiceConstants.X_US_CLIENT_ID;

@Component
@EnableBinding(EpisodeEventsBinder.class)
public class EventHandler {

    @Autowired
    EpisodeEventsBinder episodeEventsBinder;

    @Autowired
    ClientService clientService;

    @EventListener
    public <T> void emitEvent(EventDto<T> eventDto) {
        EventStreamingDto<T> eventStreamingDto = new EventStreamingDto<>(eventDto.getEventDetails().getRoutingKey(), eventDto.getMessage());
        Client client = clientService.getClientByTokenOrDefault();
        if (null != client && null != client.getId()) {
            if (eventDto.getEventDetails().getExchangeName().equals(EpisodeEventsBinder.USER_SERVICE_EXCHANGE)) {
                    episodeEventsBinder.userServiceEventOutput()
                            .send(MessageBuilder.withPayload(Utils.asJsonString(eventStreamingDto))
                            .setHeader(ROUTING_KEY, eventStreamingDto.getEventName()).
                            setHeader( X_US_CLIENT_ID, String.valueOf(client.getId())).build());
            }
        }
    }

}
