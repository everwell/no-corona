package com.everwell.transition.handlers;

import com.everwell.transition.service.TriggerService;
import org.jobrunr.jobs.annotations.Job;
import org.jobrunr.scheduling.JobScheduler;
import org.jobrunr.spring.annotations.Recurring;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class JobHandler {
    @Autowired
    TriggerService triggerService;

    @Autowired
    JobScheduler jobScheduler;

    @Autowired
    JobHandler jobHandler;

    @PostConstruct
    public void triggerJobScheduler ()
    {
        // Every time, application is deployed, we want to trigger job scheduler
        // It will read trigger table and scheduler job accordingly
        jobScheduler.enqueue(() -> jobHandler.everydayJobScheduler());
    }

    // This Job will read trigger table and schedule all the jobs according to their cron times
    // Keeping it recurring, so that changes in trigger table can be synced with jobs if any.
    @Recurring(id = "everyday-job-scheduler", cron = "0 0 * * *")
    @Job
    public void everydayJobScheduler()
    {
        triggerService.readTriggerTable();
    }


}
