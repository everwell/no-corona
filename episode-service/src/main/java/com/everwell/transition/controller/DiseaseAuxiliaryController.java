package com.everwell.transition.controller;

import com.everwell.transition.exceptions.ValidationException;
import com.everwell.transition.model.db.Regimen;
import com.everwell.transition.model.response.Response;
import com.everwell.transition.service.ClientService;
import com.everwell.transition.service.DiseaseTemplateService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class DiseaseAuxiliaryController {

    private static Logger LOGGER = LoggerFactory.getLogger(DiseaseAuxiliaryController.class);

    @Autowired
    private DiseaseTemplateService diseaseTemplateService;

    @Autowired
    ClientService clientService;

    @RequestMapping(value = "/v1/regimen/disease", method = RequestMethod.GET)
    public ResponseEntity<Response> getRegimenForDisease(
            @RequestParam(name = "diseaseId", required = false) Long diseaseId,
            @RequestParam(name = "diseaseName", required = false) String diseaseName) {
        LOGGER.info("[getRegimenForDiseaseId] getRegimenForDisease request received for diseaseId " + diseaseId + ", diseaseName " + diseaseName);
        if(null == diseaseId && null == diseaseName) {
            throw new ValidationException("both diseaseId and diseaseName cannot be null");
        }
        Long clientId = clientService.getClientByTokenOrDefault().getId();
        List<Regimen> regimens = diseaseTemplateService.getRegimensForDisease(diseaseId, diseaseName, clientId);
        return new ResponseEntity<>(new Response<>(regimens), HttpStatus.OK);
    }

}
