package com.everwell.transition.controller;

import com.everwell.transition.constants.NotificationConstants;
import com.everwell.transition.model.dto.EpisodeDto;
import com.everwell.transition.model.db.Client;
import com.everwell.transition.model.response.EpisodeUnreadNotificationResponse;
import com.everwell.transition.model.response.Response;
import com.everwell.transition.service.ClientService;
import com.everwell.transition.service.EpisodeService;
import com.everwell.transition.service.NotificationService;
import org.springframework.beans.factory.annotation.Value;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import javax.xml.bind.ValidationException;
import java.util.*;
import java.util.stream.Collectors;

@RestController
public class NotificationController {
    private static Logger LOGGER = LoggerFactory.getLogger(NotificationController.class);

    @Autowired
    NotificationService notificationService;

    @Autowired
    ClientService clientService;

    @Autowired
    private EpisodeService episodeService;

    @GetMapping("/v1/notification/send-otp/{phoneNumber}")
    @ApiOperation(
            value = "Sends otp",
            notes = "Sends an otp for the given phone number"
    )
    public ResponseEntity<Response<String>> sendOtp(@PathVariable("phoneNumber") String phoneNumber){
        LOGGER.info("[sendOtp] request received with phone number: " + phoneNumber);
        Client client = clientService.getClientByTokenOrDefault();
        notificationService.sendRegistrationOtp(client.getId(), phoneNumber, client.getHashcodeAndroid(), client.getName());
        return new ResponseEntity<>(new Response<>( true, "Otp Sent"), HttpStatus.OK);
    }

    @GetMapping("/v1/notification/validate-otp")
    @ApiOperation(
            value = "Validates otp",
            notes = "Validates otp with the phone number from cache"
    )
    public ResponseEntity<Response<String>> validateOtp(@RequestParam String phoneNumber, @RequestParam String otp){
        boolean otpValidated = notificationService.validateOtp(phoneNumber, otp);
        if (otpValidated) {
            return new ResponseEntity<>(new Response<>(true, "OTP Validated"), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(new Response<>(false,"OTP INVALID" ,"Wrong OTP / OTP has expired"), HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(
            value = "Get Person Notifications",
            notes = "Get Person Notifications"
    )
    @GetMapping(value = "/v1/notification/{personId}")
    public ResponseEntity<Response<List<Map<String, Object>>>> getEpisodeNotifications(@PathVariable Long personId) {
        LOGGER.info("[getEpisodeNotifications] request accepted");
        Long clientId = clientService.getClientByTokenOrDefault().getId();
        List<EpisodeDto> episodeDtoList = episodeService.getEpisodesForPersonList(Collections.singletonList(personId), clientId, false);
        List<Long> episodeIdList = episodeDtoList.stream().map(EpisodeDto::getId).collect(Collectors.toList());
        return notificationService.getEpisodeNotifications(episodeIdList);
    }
    
    @ApiOperation(
            value = "Update Notification Bulk",
            notes = "Update Notification Bulk"
    )
    @PutMapping(value = "/v1/notification/{personId}")
    public ResponseEntity<Response<String>> updateNotificationBulk(@PathVariable Long personId) {
        LOGGER.info("[updateNotificationBulk] request accepted");
        Long clientId = clientService.getClientByTokenOrDefault().getId();
        List<EpisodeDto> episodeDtoList = episodeService.getEpisodesForPersonList(Collections.singletonList(personId), clientId, false);
        List<Long> episodeIdList = episodeDtoList.stream().map(EpisodeDto::getId).collect(Collectors.toList());
        return notificationService.updateNotificationBulk(episodeIdList);
    }

    @ApiOperation(
            value = "Get Unread Person Notifications",
            notes = "Get Unread Person Notifications"
    )
    @GetMapping(value = "/v1/notification/unread/{personId}/count/{count}")
    public ResponseEntity<Response<EpisodeUnreadNotificationResponse>> getUnreadEpisodeNotifications(@PathVariable Long personId, @PathVariable Boolean count) {
        LOGGER.info("[getUnreadEpisodeNotifications] request accepted");
        Long clientId = clientService.getClientByTokenOrDefault().getId();
        List<EpisodeDto> episodeDtoList = episodeService.getEpisodesForPersonList(Collections.singletonList(personId), clientId, false);
        List<Long> episodeIdList = episodeDtoList.stream().map(EpisodeDto::getId).collect(Collectors.toList());
        return notificationService.getUnreadEpisodeNotifications(episodeIdList, count);
    }
}
