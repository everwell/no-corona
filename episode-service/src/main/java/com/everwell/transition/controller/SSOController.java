package com.everwell.transition.controller;

import com.everwell.transition.constants.Constants;
import com.everwell.transition.model.response.Response;
import com.everwell.transition.service.ClientService;
import com.everwell.transition.service.SSOService;
import com.everwell.transition.utils.CacheUtils;
import io.jsonwebtoken.Jwt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;


@RestController
public class SSOController {

    private static Logger LOGGER = LoggerFactory.getLogger(SSOController.class);

    @Autowired
    SSOService ssoService;

    @RequestMapping(value="v1/sso/login", method = RequestMethod.POST)
    public ResponseEntity<Response<Map<String, Object>>> login(@RequestHeader(name = Constants.X_PLATFORM_ID) Long platformId , @RequestBody Map<String, Object> loginRequest) {
        LOGGER.info("[login] login request accepted : " + loginRequest);
        return ssoService.login(loginRequest, platformId);
    }

    @RequestMapping(value="v1/sso/logout", method = RequestMethod.POST)
    public ResponseEntity<Response<Void>> logout(@RequestHeader HttpHeaders headers, @RequestBody Map<String, Object> logoutRequest) {
        LOGGER.info("[logout] logout request accepted : " + logoutRequest);
        //assumption is that token will always be present before reaching this route.
        String token = headers.get(Constants.AUTHORIZATION).get(0).split(" ")[1];
        CacheUtils.putIntoCache(Constants.BLACKLIST_TOKEN + token, "1", 84600L);
        return ssoService.logout(logoutRequest);
    }

    @RequestMapping(value="v1/sso/device-id", method = RequestMethod.PUT)
    public ResponseEntity<Response<Void>> refreshDeviceIdAndLastActivityDate(@RequestBody Map<String, Object> updateDeviceIdRequest) {
        LOGGER.info("[refreshDeviceIdAndLastActivityDate] refreshDeviceId request accepted :" + updateDeviceIdRequest);
        return ssoService.refreshDeviceIdAndLastActivityDate(updateDeviceIdRequest);
    }

}
