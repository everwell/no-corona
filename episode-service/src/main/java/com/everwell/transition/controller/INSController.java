package com.everwell.transition.controller;

import com.everwell.transition.constants.FieldConstants;
import com.everwell.transition.enums.episode.EpisodeField;
import com.everwell.transition.model.dto.EngagementDto;
import com.everwell.transition.model.dto.EpisodeDto;
import com.everwell.transition.model.request.EngagementRequest;
import com.everwell.transition.model.request.ins.NotificationLogRequest;
import com.everwell.transition.model.response.EngagementResponse;
import com.everwell.transition.model.response.Response;
import com.everwell.transition.repositories.TriggerRepository;
import com.everwell.transition.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;


@RestController
public class INSController {
    private static final Logger LOGGER = LoggerFactory.getLogger(INSController.class);

    @Autowired
    private EpisodeService episodeService;

    @Autowired
    private INSService insService;

    @Autowired
    private ClientService clientService;

    @Autowired
    private EpisodeLogService episodeLogService;

    @GetMapping(value = "/v1/engagement")
    public ResponseEntity<Response<EngagementResponse>> episodeEngagement(@RequestParam Long episodeId) {
        LOGGER.info("[episodeEngagement] received for episodeId" + episodeId);
        Long clientId = clientService.getClientByTokenOrDefault().getId();
        EpisodeDto episodeDto = episodeService.getEpisodeDetails(episodeId, clientId);
        List<Map<String, Object>> insEngagementResponse = insService.getEngagement(episodeDto);
        Map<String, Object> stageData = episodeDto.getStageData();
        Boolean householdVisits = Boolean.valueOf(String.valueOf(stageData.getOrDefault(EpisodeField.HOUSEHOLD_VISITS.getFieldName(), null)));
        Boolean callCenterCalls = Boolean.valueOf(String.valueOf(stageData.getOrDefault(EpisodeField.CALL_CENTER_CALLS.getFieldName(), null)));
        Long hierarchyId = Long.parseLong(String.valueOf(episodeDto.getHierarchyMappings().getOrDefault(FieldConstants.FIELD_CURRENT, null)));
        EngagementResponse engagementResponse = new EngagementResponse(insEngagementResponse, callCenterCalls, householdVisits, hierarchyId);
        return new ResponseEntity<>(new Response<>(engagementResponse), HttpStatus.OK);
    }

    @PostMapping(value = "/v1/engagement")
    public ResponseEntity<Response<String>> saveEpisodeEngagement(@RequestBody EngagementRequest engagementRequest) {
        LOGGER.info("[saveEpisodeEngagement] received with request" + engagementRequest.toString());
        engagementRequest.validate();
        Long clientId = clientService.getClientByTokenOrDefault().getId();
        EpisodeDto episodeDto = episodeService.getEpisodeDetails(engagementRequest.getEpisodeId(), clientId);
        EngagementDto engagementDto = insService.saveEngagement(engagementRequest, episodeDto);
        episodeLogService.save(engagementDto.getEpisodeLogDataRequest());
        episodeService.processUpdateEpisode(clientId, engagementDto.getUpdateRequest());
        return new ResponseEntity<>(new Response<>(true ,"Engagement Saved!"), HttpStatus.OK);
    }

    @PostMapping(value = "/v1/notification/logs")
    public ResponseEntity<Response<String>> addNotificationLogs(@RequestBody NotificationLogRequest notificationLogRequest) {
        LOGGER.info("[saveEpisodeEngagement] received with request" + notificationLogRequest.toString());
        notificationLogRequest.validate();
        Long clientId = clientService.getClientByTokenOrDefault().getId();
        String response = insService.addNotificationLog(notificationLogRequest, clientId);
        return new ResponseEntity<>(new Response<>(true ,response), HttpStatus.OK);
    }
}
