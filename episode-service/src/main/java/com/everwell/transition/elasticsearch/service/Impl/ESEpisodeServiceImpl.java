package com.everwell.transition.elasticsearch.service.Impl;

import com.everwell.transition.constants.FieldConstants;
import com.everwell.transition.elasticsearch.ElasticConstants;
import com.everwell.transition.elasticsearch.data.ESEpisodeRepository;
import com.everwell.transition.elasticsearch.indices.EpisodeIndex;
import com.everwell.transition.elasticsearch.service.ESEpisodeService;
import com.everwell.transition.exceptions.InternalServerErrorException;
import com.everwell.transition.exceptions.NotFoundException;
import com.everwell.transition.model.dto.EpisodeSearchResult;
import com.everwell.transition.model.request.episodesearch.EpisodeSearchRequest;
import com.everwell.transition.utils.Utils;
import com.fasterxml.jackson.core.type.TypeReference;
import io.sentry.Sentry;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchScrollRequest;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.reindex.DeleteByQueryRequest;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.ParsedMultiBucketAggregation;
import org.elasticsearch.search.aggregations.ParsedMultiBucketAggregation.ParsedBucket;
import org.elasticsearch.search.aggregations.bucket.histogram.ParsedDateHistogram;
import org.elasticsearch.search.aggregations.bucket.terms.ParsedTerms;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.query.FetchSourceFilter;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import static org.elasticsearch.index.query.QueryBuilders.termQuery;

@Service
public class ESEpisodeServiceImpl implements ESEpisodeService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ESEpisodeServiceImpl.class);

    @Autowired
    private ESEpisodeRepository esEpisodeRepository;

    @Autowired
    private ElasticsearchOperations elasticsearchOperations;

    @Autowired
    private EpisodeEsQueryBuilder episodeEsQueryBuilder;

    @Autowired
    RestHighLevelClient restHighLevelClient;

    @Override
    public EpisodeIndex save (EpisodeIndex episodeIndex) {
        return esEpisodeRepository.save(episodeIndex);
    }

    @Override
    public EpisodeIndex findByIdAndClientId(Long id, Long clientId) {
        EpisodeIndex episodeIndex = esEpisodeRepository.findAllByIdAndClientId(String.valueOf(id), clientId).orElse(null);
        if(null == episodeIndex) {
            throw new NotFoundException("No episode found for Id : " + id + " for client: " + clientId);
        }
        return episodeIndex;
    }

    @Override
    public List<EpisodeIndex> findAllByPersonId (Long id, Long clientId) {
        return esEpisodeRepository.findAllByPersonIdAndClientId(id, clientId);
    }

    @Override
    public List<EpisodeIndex> findAllByPersonIdList(List<Long> personIdList, Long clientId) {
        return esEpisodeRepository.findAllByPersonIdInAndClientId(personIdList, clientId);
    }

    @Override
    public List<EpisodeIndex> findAllByEpisodeIdInAndClientId(List<String> episodeIdList, Long clientId) {
        return esEpisodeRepository.findAllByIdInAndClientId(episodeIdList, clientId);
    }

    @Override
    public EpisodeSearchResult search(EpisodeSearchRequest searchReq, Map<String, String> fieldToTableMap, boolean fetchFullDoc) {
        EpisodeSearchResult result;
        if (StringUtils.hasLength(searchReq.getScrollId())) {
            return searchWithScrollId(searchReq.getScrollId(), searchReq.getScrollTimeOutInSeconds());
        }
        NativeSearchQueryBuilder searchQueryBuilder = new NativeSearchQueryBuilder()
                .withQuery(episodeEsQueryBuilder.build(searchReq, fieldToTableMap))
                .withPageable(PageRequest.of(searchReq.getPage(), searchReq.getSize(), searchReq.getSortDirection(), episodeEsQueryBuilder.convertFieldNames(fieldToTableMap, searchReq.getSortKey(), searchReq.getKeywordFields())));
        if (searchReq.getTrackTotalHits())
            searchQueryBuilder.withTrackTotalHits(searchReq.getTrackTotalHits());
        if (searchReq.isScroll()) {
            return searchWithScroll(searchReq, searchQueryBuilder, fetchFullDoc, fieldToTableMap);
        }
        if (!fetchFullDoc) {
            List<String> formattedFields = new ArrayList<>();
            searchReq.getFieldsToShow().forEach(key -> {
                formattedFields.add(fieldToTableMap.containsKey(key) ? fieldToTableMap.get(key) + "." + key : key);
            });
            searchQueryBuilder.withSourceFilter(new FetchSourceFilter(formattedFields.toArray(new String[0]), null));
        }
        if (StringUtils.hasLength(searchReq.getCollapseField())) {
            searchQueryBuilder.withCollapseField(episodeEsQueryBuilder.convertFieldNames(fieldToTableMap, searchReq.getCollapseField(), searchReq.getKeywordFields()));
        }
        if (searchReq.isCount()) {
            result = performCount(searchQueryBuilder.build());
        } else {
            if (null != searchReq.getAgg()) {
                EpisodeSearchRequest.SupportedAggMethods aggMethods = searchReq.getAgg();
                if(null != aggMethods.getTerms()) {
                    aggMethods.getTerms().forEach(a -> a.setFieldName(episodeEsQueryBuilder.convertFieldNames(fieldToTableMap, a.getFieldName(), searchReq.getKeywordFields())));
                }
                if(null != aggMethods.getDate()) {
                    aggMethods.getDate().forEach(a -> a.setFieldName(episodeEsQueryBuilder.convertFieldNames(fieldToTableMap, a.getFieldName(), searchReq.getKeywordFields())));
                }
                searchQueryBuilder.withAggregations(episodeEsQueryBuilder.buildAggregation(aggMethods)).withMaxResults(0);
            }
            result = performSearch(searchQueryBuilder.build());
        }
        result.setRequestIdentifier(searchReq.getRequestIdentifier());
        return result;
    }

    public EpisodeSearchResult searchWithScrollId(String scrollId, Long scrollTimeOutInSeconds) {
        SearchScrollRequest scrollRequest = new SearchScrollRequest(scrollId);
        scrollRequest.scroll(TimeValue.timeValueSeconds(scrollTimeOutInSeconds));
        return collectDataFromSearchResponse(scrollByRestClient(scrollRequest));
    }

    public EpisodeSearchResult searchWithScroll(EpisodeSearchRequest episodeSearchRequest, NativeSearchQueryBuilder nativeSearchQueryBuilder, boolean fetchFullDoc, Map<String, String> fieldToTableMap) {
        SearchRequest searchRequest = new SearchRequest(ElasticConstants.EPISODE_INDEX);
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(nativeSearchQueryBuilder.build().getQuery());
        searchSourceBuilder.size(episodeSearchRequest.getSize());
        if (!fetchFullDoc) {
            String[] formattedFields = new String[episodeSearchRequest.getFieldsToShow().size()];
            for (int i = 0; i < episodeSearchRequest.getFieldsToShow().size(); i++) {
                String key = episodeSearchRequest.getFieldsToShow().get(i);
                formattedFields[i] = fieldToTableMap.containsKey(key) ? fieldToTableMap.get(key) + "." + key : key;
            }
            searchSourceBuilder.fetchSource(formattedFields, null);
        }
        searchRequest.source(searchSourceBuilder);
        searchRequest.scroll(TimeValue.timeValueSeconds(episodeSearchRequest.getScrollTimeOutInSeconds()));
        return collectDataFromSearchResponse(searchByRestClient(searchRequest));
    }

    @Override
    public EpisodeSearchResult collectDataFromSearchResponse(SearchResponse searchResponse) {
        String scrollId = searchResponse.getScrollId();
        org.elasticsearch.search.SearchHits searchHit = searchResponse.getHits();
        List<EpisodeIndex> episodeIndices = new ArrayList<>();
        for (SearchHit hit: searchHit.getHits()) {
            try {
                episodeIndices.add(Utils.jsonToObject(hit.getSourceAsString(), new TypeReference<EpisodeIndex>() {}));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        EpisodeSearchResult episodeSearchResult = new EpisodeSearchResult(searchHit.getTotalHits().value, episodeIndices, null);
        episodeSearchResult.setScrollId(scrollId);
        return episodeSearchResult;
    }

    @Override
    public EpisodeSearchResult performSearch(NativeSearchQuery nativeSearchQuery) {
        LOGGER.info(Utils.asJsonString(nativeSearchQuery));
        SearchHits<EpisodeIndex> searchHit = elasticsearchOperations.search(nativeSearchQuery, EpisodeIndex.class);
        Map<String, Map<String, Long>> aggregationResults = new HashMap<>();
        if (!CollectionUtils.isEmpty(nativeSearchQuery.getAggregations())) {
            Aggregations aggregations = (Aggregations) searchHit.getAggregations().aggregations();
            Map<String, Aggregation> aggregationsMap = aggregations.getAsMap();
            aggregationsMap.forEach((identifier, aggregation) -> {
                ParsedMultiBucketAggregation<? extends ParsedBucket> value;
                Map<String, Long> fieldCountMap = new HashMap<>();
                if(aggregation.getType().equals(ElasticConstants.AGGREGATION_TYPE_DATE_HISTOGRAM)) {
                    value = (ParsedDateHistogram) aggregation;
                    fieldCountMap = new LinkedHashMap<>(); //using LinkedHashMap to maintain insertion order of dates
                } else {
                    value = (ParsedTerms) aggregation;
                }
                Map<String, Long> finalFieldCountMap = fieldCountMap;
                value.getBuckets().forEach(bucket -> {
                    String fieldKey = bucket.getKeyAsString();
                    Long count = bucket.getDocCount();
                    finalFieldCountMap.put(fieldKey, count);
                });
                aggregationResults.put(identifier, fieldCountMap);
            });
        }
        List<EpisodeIndex> episodeIndexList = searchHit.getSearchHits().stream().map(m -> {
            EpisodeIndex episodeIndex = m.getContent();
            episodeIndex.clean();
            return episodeIndex;
        }).collect(Collectors.toList());
        return new EpisodeSearchResult(searchHit.getTotalHits(), episodeIndexList, aggregationResults);
    }

    @Override
    public EpisodeSearchResult performCount(NativeSearchQuery nativeSearchQuery) {
        long count = elasticsearchOperations.count(nativeSearchQuery, EpisodeIndex.class);
        return new EpisodeSearchResult(count, null, null);
    }

    @Override
    public void updateToElastic(UpdateRequest updateRequest) {
        BulkRequest bulkRequest = new BulkRequest();
        bulkRequest.add(updateRequest);
        updateBulkElastic(bulkRequest);
    }

    @Override
    public void updateBulkElastic(BulkRequest bulkRequest) {
        try {
            restHighLevelClient.bulk(bulkRequest, RequestOptions.DEFAULT);
        } catch (Exception e) {
            Sentry.capture(e);
            throw new InternalServerErrorException("[updateBulkElastic] Unable to process request");
        }
    }

    public SearchResponse searchByRestClient(SearchRequest searchRequest) {
        try {
            return restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
        } catch (Exception e) {
            Sentry.capture(e);
            throw new InternalServerErrorException("[searchByRestClient] Unable to process request");
        }
    }

    public SearchResponse scrollByRestClient(SearchScrollRequest scrollRequest) {
        try {
            return restHighLevelClient.scroll(scrollRequest, RequestOptions.DEFAULT);
        } catch (Exception e) {
            Sentry.capture(e);
            throw new InternalServerErrorException("[scrollByRestClient] Unable to process request!");
        }
    }

    @Override
    public void delete (String id, Long clientId) {
        DeleteByQueryRequest request = new DeleteByQueryRequest(ElasticConstants.EPISODE_INDEX);
        QueryBuilder queryBuilder = QueryBuilders.boolQuery()
                .filter(termQuery(FieldConstants.EPISODE_FIELD_ID, id))
                .filter(termQuery(FieldConstants.CLIENT_ID, clientId));
        request.setQuery(queryBuilder);
        deleteByQuery(request);
    }

    @Override
    public void deleteByQuery(DeleteByQueryRequest request) {
        try {
            restHighLevelClient.deleteByQuery(request, RequestOptions.DEFAULT);
        } catch (IOException e) {
            throw new InternalServerErrorException("[delete] Unable to process request");
        }
    }

}
