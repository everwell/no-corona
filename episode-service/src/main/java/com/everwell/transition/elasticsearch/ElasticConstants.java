package com.everwell.transition.elasticsearch;

import java.util.Collections;
import java.util.List;

public class ElasticConstants {

    public static final String EPISODE_INDEX = "episode";
    public static final String HIERARCHY_MAPPING_CLIENT_NOMENCLATURE = "hierarchyMapping_";
    public static final String HIERARCHY_MAPPING_ELASTIC_NOMENCLATURE = "hierarchyMappings";
    public static final String HIERARCHY_MAPPING_ALL_ELASTIC_NOMENCLATURE = "_all";
    public static final String ASSOCIATIONS_ELASTIC_NOMENCLATURE = "associations";
    public static final String STAGE_DATA_ELASTIC_NOMENCLATURE = "stageData";
    public static final String ELASTIC_SEARCH_KEYWORD_KEYWORD = "keyword";
    public static final String ELASTIC_SEARCH_KEYWORD_MUST = "must";
    public static final String ELASTIC_SEARCH_KEYWORD_MUST_NOT = "must_not";
    public static final String ELASTIC_SEARCH_RANGE_LOWER_THAN = "lt";
    public static final String ELASTIC_SEARCH_RANGE_GREATER_THAN = "gt";
    public static final String ELASTIC_SEARCH_RANGE_LOWER_THAN_EQUAL = "lte";
    public static final String ELASTIC_SEARCH_RANGE_GREATER_THAN_EQUAL = "gte";

    public static final List<String> ELASTIC_NO_PREFIX_FIELDS = Collections.singletonList("addedBy");

    public static final String AGGREGATION_TYPE_DATE_HISTOGRAM = "date_histogram";
    public static final String ELASTIC_SEARCH_KEYWORD_CONTAINS = "contains";
    public static final String ASTERISK = "*";
    public static final Integer ELASTIC_BATCH_SIZE = 5000;

}
