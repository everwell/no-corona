package com.everwell.transition.enums.iam;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum IAMEndpoint {

    ADHERENCE_CONFIG("/iam/v1/adherence-config"),
    GET_ADHERENCE("/iam/v1/adherence"),
    GET_ADHERENCE_BULK("/iam/v1/adherence/search"),
    RECORD_ADHERENCE("/iam/v1/adherence/record"),
    UPDATE_ENTITY("/iam/v1/entity"),
    DELETE_ENTITY("/iam/v1/entity"),
    IMEI("/iam/v1/imei"),
    IMEI_ENTITY("/iam/v1/imei/entity"),
    IMEI_BULK("/iam/v1/imei/bulk"),
    VOT_VIDEO_STATUS("/iam/v1/vot/video/status"),
    POSITIVE_EVENT("/iam/v1/adherence/positive-events"),
    AVERAGE_ADHERENCE("/iam/v1/adherence/average"),
    EDIT_PHONE("/iam/v1/phone"),
    RECORD_ADHERENCE_BULK("/iam/v1/adherence/record/bulk"),
    ADHERENCE_GLOBAL_CONFIG("/iam/v1/adherence-global-config"),
    REGISTER_ENTITY("/iam/v1/entity"),
    CLOSE_ENTITY("/iam/v1/entity"),
    ENTITY_REOPEN("/iam/v1/entity/reopen");

    @Getter
    private final String endpointUrl;
}
