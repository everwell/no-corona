package com.everwell.transition.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum AdherenceTypeMappingEnum {

    NNDOTS("99DOTS",1L),
    VOT("VOT",2L),
    MERM("MERM",3L),
    OPASHA("OpAsha",4L),
    NONE("NONE",5L),
    NNDLITE("99DOTSLite",6L);

    @Getter
    private final String name;

    @Getter
    private final Long id;

    public static Long getMatch(String monitoringMethod) {
        Long adherenceType = null;
        for(AdherenceTypeMappingEnum tech: values()) {
            if(tech.name.toLowerCase().equalsIgnoreCase(monitoringMethod)) {
                adherenceType = tech.id;
                break;
            }
        }
        return adherenceType;
    }
}
