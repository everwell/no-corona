package com.everwell.transition.enums.dispensation;

public enum DispensationRangeFilterType {
    NEXT_REFILL_DATE,
    NUMBER_OF_DISPENSATIONS
}
