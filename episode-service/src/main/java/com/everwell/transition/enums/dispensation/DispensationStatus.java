package com.everwell.transition.enums.dispensation;

public enum DispensationStatus {
    ISSUED,
    RETURNED,
    PARTIALLY_RETURNED
}
