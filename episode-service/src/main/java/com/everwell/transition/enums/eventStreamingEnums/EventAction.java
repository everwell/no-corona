package com.everwell.transition.enums.eventStreamingEnums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum EventAction {
    TAGS_SEARCH("Search Tags"),
    TAGS_GET("Get Tags"),
    TAGS_GET_CONFIG("Get Config"),
    EPISODE_DELETE("Delete Episode");

    @Getter
    private final String name;
}
