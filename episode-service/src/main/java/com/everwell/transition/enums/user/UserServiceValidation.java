package com.everwell.transition.enums.user;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum UserServiceValidation
{
    GET_USER_ERROR("error in get user"),
    CREATE_USER_ERROR("error in create user"),
    FETCH_DUPLICATE_USER_ERROR("error in fetching duplicates user"),
    UPDATE_USER_ERROR("error in update user");
    @Getter
    private final String message;

}
