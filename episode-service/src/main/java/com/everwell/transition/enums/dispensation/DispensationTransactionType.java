package com.everwell.transition.enums.dispensation;

public enum DispensationTransactionType {
    ISSUED,
    RETURNED,
    CREDIT,
    DEBIT
}
