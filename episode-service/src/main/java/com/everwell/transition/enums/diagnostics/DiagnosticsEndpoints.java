package com.everwell.transition.enums.diagnostics;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum DiagnosticsEndpoints {

    TEST_CONNECTION("/diagnostics/test-connection"),
    ADD_TEST("/diagnostics/v2/test"),
    KPI("/diagnostics/v1/kpi"),
    QR_VALIDATION("/diagnostics/v1/validateQr"),
    GENERATE_QR_PDF("/diagnostics/v1/generateQrPdf"),
    SAMPLE_BY_ID_OR_QRCODE("/diagnostics/v1/sample"),
    FILTER("/diagnostics/v1/tests/all");

    @Getter
    private final String endpointUrl;
}
