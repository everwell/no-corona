package com.everwell.transition.enums.user;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum UserServiceEndpoint
{
    GET_USER( "/userservice/v1/user"),
    CREATE_USER("/userservice/v1/users"),
    GET_USER_BY_PRIMARY_PHONE("/userservice/v1/users/primaryPhone"),
    FETCH_DUPLICATE_USER("/userservice/v1/users/duplicates"),
    ADD_RELATION("/userservice/v1/user/relation"),
    UPDATE_RELATION("/userservice/v1/user/relation"),
    DELETE_RELATION("/userservice/v1/user/relation"),
    GET_RELATIONS("/userservice/v1/user/relation"),
    GET_RELATIONS_FOR_PHONE("/userservice/v1/users/relation"),
    GET_VITALS("/userservice/v1/vitals"),
    GET_VITALS_BY_ID("/userservice/v1/vitals"),
    ADD_VITALS("/userservice/v1/vitals"),
    UPDATE_VITALS_BY_ID("/userservice/v1/vitals/%s"),
    DELETE_VITALS_BY_ID("/userservice/v1/vitals"),
    GET_USER_VITALS_GOAL("/userservice/v1/users/%s/vitals-goal"),
    ADD_USER_VITALS_GOAL("/userservice/v1/users/%s/vitals-goal"),
    UPDATE_USER_VITALS_GOAL("/userservice/v1/users/%s/vitals-goal"),
    DELETE_USER_VITALS_GOAL("/userservice/v1/users/%s/vitals-goal"),
    GET_USER_VITALS_DETAILS("/userservice/v1/users/vitals/bulk"),
    ADD_USER_VITALS_DETAILS("/userservice/v1/users/%s/vitals"),
    UPDATE_USER("/userservice/v1/users");

    @Getter
    private final String endpointUrl;
}
