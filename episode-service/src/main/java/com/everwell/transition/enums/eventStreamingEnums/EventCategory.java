package com.everwell.transition.enums.eventStreamingEnums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum EventCategory {

    EPISODE("Episode"),
    EPISODE_TAGS("Episode Tags");

    @Getter
    private final String name;
}
