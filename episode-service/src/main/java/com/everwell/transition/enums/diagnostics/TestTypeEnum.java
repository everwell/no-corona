package com.everwell.transition.enums.diagnostics;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum TestTypeEnum {
    CBNAAT("CBNAAT"),
    MICROSCOPY_ZN("Microscopy ZN"),
    CHEST_X_RAY("Chest X Ray"),
    TRUENAT_MTB("Truenat (MTB)"),
    TRUENAT_MTB_RIF("Truenat (MTB-RIF)"),
    FLLPA("FLLPA"),
    SLLPA("SLLPA"),
    DST("DST"),
    CULTURE("Culture"),
    OTHER("Other"),
    MICROSCOPY_FLUORESCENT("Microscopy Fluorescent"),
    TST("TST"),
    IGRA("IGRA"),
    CYTOPATHOLOGY("Cytopathology"),
    HISTOPATHOLOGY("Histopathology"),
    GENE_SEQUENCING("Gene Sequencing");

    @Getter
    private final String name;

}