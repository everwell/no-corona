package com.everwell.transition.model.response.appointment;

import com.everwell.transition.model.db.AppointmentAnswer;
import com.everwell.transition.model.dto.appointment.AppointmentDetails;
import com.everwell.transition.model.dto.appointment.Document;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetAppointmentResponse {
    private AppointmentDetails appointment;
    private List<AppointmentAnswer> answers;
//    private List<Document> documents;
}
