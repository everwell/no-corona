package com.everwell.transition.model.request.episodelogs;

import com.everwell.transition.exceptions.ValidationException;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class EpisodeLogDataRequest {
    Long episodeId;
    String category;
    String subCategory;
    Long addedBy;
    String actionTaken;
    String comments;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    LocalDateTime dateOfAction;

    public void validate() {
        if (!StringUtils.hasText(category))
            throw new ValidationException("Category cannot be empty!");
        if (episodeId == null)
            throw new ValidationException("Episode Id cannot be empty!");
        if (addedBy == null)
            throw  new ValidationException("Added By cannot be null");
    }

    public EpisodeLogDataRequest(Long episodeId, String category, Long addedBy, String actionTaken, String comments)
    {
        this.episodeId = episodeId;
        this.category = category;
        this.addedBy = addedBy;
        this.actionTaken = actionTaken;
        this.comments = comments;
    }
}
