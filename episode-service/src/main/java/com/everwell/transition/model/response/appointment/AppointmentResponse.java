package com.everwell.transition.model.response.appointment;

import org.joda.time.LocalDateTime;

public class AppointmentResponse {
    private Integer id;
    private Long episodeId;
    private String date;
    private String time;
    private LocalDateTime deletedAt;
    private Integer deletedById;
    private Integer visitNumber;
    private LocalDateTime followUpDate;
    private String status;
}
