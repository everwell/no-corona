package com.everwell.transition.model.db;

import com.everwell.transition.model.request.aers.OutcomeRequest;
import com.everwell.transition.utils.Utils;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.lang.reflect.Field;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Table(name = "aers_outcome")
@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class AersOutcome {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "outcome")
    private String outcome;

    @Column(name = "date_of_resolution")
    private LocalDateTime dateOfResolution;

    @Column(name = "date_of_death")
    private LocalDateTime dateOfDeath;

    @Column(name = "cause_of_death")
    private String causeOfDeath;

    @Column(name = "autopsy_performed")
    private String autopsyPerformed;

    @Column(name = "hospital_admission_date")
    private LocalDateTime hospitalAdmissionDate;

    @Column(name = "hospital_discharge_data")
    private LocalDateTime hospitalDischargeDate;

    @Column(name = "created_on")
    private LocalDateTime createdOn;

    @Column(name = "updated_on")
    private LocalDateTime updatedOn;

    public AersOutcome update(AersOutcome other) {
        Class klass = getClass();
        for (Field field : klass.getDeclaredFields()) {
            try {
                if (field.get(other) != null) {
                    field.set(this, field.get(other));
                }
            } catch (IllegalAccessException e) {
            }
        }
        return this;
    }

    public AersOutcome(OutcomeRequest outcomeRequest) {
        this.outcome = outcomeRequest.getOutcome();
        this.autopsyPerformed = outcomeRequest.getAutopsyPerformed();
        this.causeOfDeath = outcomeRequest.getCauseOfDeath();
        this.dateOfResolution =  Utils.toLocalDateTimeWithNull(outcomeRequest.getDateOfResolution());
        this.dateOfDeath = Utils.toLocalDateTimeWithNull(outcomeRequest.getDateOfDeath());
        this.hospitalAdmissionDate = Utils.toLocalDateTimeWithNull(outcomeRequest.getHospitalAdmissionDate());
        this.hospitalDischargeDate = Utils.toLocalDateTimeWithNull(outcomeRequest.getHospitalDischargeDate());
    }

    public AersOutcome(String outcome, String causeOfDeath)
    {
        this.outcome = outcome;
        this.causeOfDeath = causeOfDeath;
    }

    public AersOutcome(Long id, String outcome)
    {
        this.id = id;
        this.outcome = outcome;
    }

    public AersOutcome(String outcome, String causeOfDeath, String date)
    {
        this.outcome = outcome;
        this.causeOfDeath = causeOfDeath;
        this.dateOfDeath = Utils.toLocalDateTimeWithNull(date);
        this.dateOfResolution = Utils.toLocalDateTimeWithNull(date);
        this.hospitalAdmissionDate = Utils.toLocalDateTimeWithNull(date);
        this.hospitalDischargeDate = Utils.toLocalDateTimeWithNull(date);
    }
}
