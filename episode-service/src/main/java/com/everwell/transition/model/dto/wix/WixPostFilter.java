package com.everwell.transition.model.dto.wix;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class WixPostFilter {
    Boolean featured;
    List<String> categoryIds;
    String language;

    public WixPostFilter(Boolean featured, List<String> categoryIds) {
        this.featured = featured;
        this.categoryIds = categoryIds;
        this.language = "en";
    }
}
