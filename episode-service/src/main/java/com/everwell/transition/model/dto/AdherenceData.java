package com.everwell.transition.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@JsonInclude(JsonInclude.Include.NON_NULL)
@ToString
public class AdherenceData {
    @Setter
    public Long iamId;

    @Setter
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    private LocalDateTime startDate;

    @Setter
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    private LocalDateTime endDate;

    @Setter
    private Long adherenceType;

    private String adherenceTypeString;

    @Setter
    private Long scheduleType;

    @Setter
    private String scheduleTypeString;

    @Setter
    private String currentSchedule;

    @Setter
    private String uniqueIdentifier;

    @Setter
    private String adherenceString;

    @Getter
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    private LocalDateTime createdDate;

    @Setter
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    private LocalDateTime lastDosage;

    @Setter
    private int technologyDoses;

    @Setter
    private int manualDoses;

    @Setter
    private int totalDoses;

    @Setter
    List<AdhTechLogsData> adhTechLogsData;

    public AdherenceData(String adherenceString, String adherenceTypeString) {
        this.adherenceString = adherenceString;
        this.adherenceTypeString = adherenceTypeString;
    }

}
