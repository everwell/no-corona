package com.everwell.transition.model.request.adherence;

import com.everwell.transition.constants.IAMConstants;
import com.everwell.transition.enums.iam.IAMField;
import com.everwell.transition.enums.iam.IAMValidation;
import com.everwell.transition.exceptions.ValidationException;
import com.everwell.transition.utils.Utils;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EntityRequest {

    private String entityId;

    private Integer adherenceType;

    private String uniqueIdentifier;

    private String startDate;

    private Long scheduleTypeId;

    private String scheduleString;

    private Long positiveSensitivity;

    private Long negativeSensitivity;

    private Long firstDoseOffset;

    private Long clientId;

    private Object doseDetails;

    private List<PhoneRequest.PhoneNumbers> phoneNumbers;

    private String imei;

    public EntityRequest(String entityId, Integer adherenceType, String uniqueIdentifier, String startDate, Long scheduleTypeId, String scheduleString, Long positiveSensitivity,
                         Long negativeSensitivity, Long firstDoseOffset, Long clientId, Object doseDetails) {
        this.entityId = entityId;
        this.adherenceType = adherenceType;
        this.uniqueIdentifier = uniqueIdentifier;
        this.startDate = startDate;
        this.scheduleTypeId = scheduleTypeId;
        this.scheduleString = scheduleString;
        this.positiveSensitivity = positiveSensitivity;
        this.negativeSensitivity = negativeSensitivity;
        this.firstDoseOffset = firstDoseOffset;
        this.clientId = clientId;
        this.doseDetails = doseDetails;
    }

    public EntityRequest(Long episodeId, Map<String, Object> input, Integer adherenceTypeMapping, Integer scheduleTypeMapping) {
        this.entityId = String.valueOf(episodeId);
        this.adherenceType = adherenceTypeMapping;
        this.startDate = Utils.convertObjectToStringWithNull(input.get(IAMField.IAM_START_DATE.getFieldName()));
        this.scheduleTypeId = scheduleTypeMapping.longValue();
        this.scheduleString =  Utils.convertObjectToStringWithNull(input.get(IAMField.SCHEDULE_STRING.getFieldName()));
        this.positiveSensitivity = NumberUtils.toLong(Utils.convertObjectToStringWithNull(input.get(IAMField.POSITIVE_SENSITIVITY.getFieldName())), IAMConstants.DEFAULT_POSITIVE_SENSITIVITY);
        this.negativeSensitivity = NumberUtils.toLong(Utils.convertObjectToStringWithNull(input.get(IAMField.NEGATIVE_SENSITIVITY.getFieldName())), IAMConstants.DEFAULT_NEGATIVE_SENSITIVITY);
        this.firstDoseOffset = NumberUtils.toLong(Utils.convertObjectToStringWithNull(input.get(IAMField.FIRST_DOSE_OFFSET.getFieldName())), IAMConstants.FIRST_DOSE_OFFSET);
        this.uniqueIdentifier = String.valueOf(episodeId);
    }

    public void validate() {
        if (!StringUtils.hasText(startDate)) {
            throw new ValidationException(IAMValidation.START_DATE_VALIDATION.getMessage());
        }
    }

}
