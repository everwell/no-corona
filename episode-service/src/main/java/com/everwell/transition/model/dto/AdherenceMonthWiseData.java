package com.everwell.transition.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class AdherenceMonthWiseData {
    String monthName;
    int monthNumber;
    int daysInMonth;
    int year;
    List<AdherenceDataForCalendar> adherenceDataForCalendar;
}
