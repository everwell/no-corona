package com.everwell.transition.model.db;

import com.everwell.transition.enums.SupportedRelationType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "supported_relation")
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class SupportedRelation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "description", columnDefinition = "TEXT")
    private String description;

    @Column(name = "type")
    private SupportedRelationType type;

}
