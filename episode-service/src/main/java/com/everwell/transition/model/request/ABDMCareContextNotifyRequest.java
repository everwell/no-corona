package com.everwell.transition.model.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class ABDMCareContextNotifyRequest {
    private long episodeId;
    private long personId;
    private String hiTypes;
    private String personName;
    private String abhaAddress;
}
