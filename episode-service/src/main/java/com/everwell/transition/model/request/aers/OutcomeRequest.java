package com.everwell.transition.model.request.aers;

import com.everwell.transition.exceptions.ValidationException;
import com.everwell.transition.utils.Utils;
import lombok.*;
import org.springframework.util.StringUtils;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class OutcomeRequest {
    Long adverseEventId;
    Long episodeId;
    String outcome;
    String dateOfResolution;
    String dateOfDeath;
    String causeOfDeath;
    String autopsyPerformed;
    String hospitalAdmissionDate;
    String hospitalDischargeDate;


    public OutcomeRequest(Long adverseEventId, Long episodeId) {
        this.adverseEventId = adverseEventId;
        this.episodeId = episodeId;
    }

    public OutcomeRequest(Long adverseEventId, Long episodeId, String outcome) {
        this.adverseEventId = adverseEventId;
        this.episodeId = episodeId;
        this.outcome = outcome;
    }

    public OutcomeRequest(Long adverseEventId, Long episodeId, String outcome, String dateOfDeath) {
        this.adverseEventId = adverseEventId;
        this.episodeId = episodeId;
        this.outcome = outcome;
        this.dateOfDeath = dateOfDeath;
    }

    public void validate(OutcomeRequest outcomeRequest) {
        if (outcomeRequest.getEpisodeId() == null) {
            throw new ValidationException("episode id is required");
        }

        if (outcomeRequest.getAdverseEventId() == null) {
            throw new ValidationException("Adverse Event Id is required for Outcome");
        }

        if (!StringUtils.hasText(outcomeRequest.getOutcome())) {
            throw new ValidationException("Outcome is required");
        }
    }
}
