package com.everwell.transition.model.request.adherence;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;
import java.util.Map;

@Getter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class AverageAdherenceRequest {
    private List<String> entityIdList;
    private boolean patientDetailsRequired;
    private List<Map<String, Object>> averageAdherenceIntervals;

    public AverageAdherenceRequest(List<String> entityIdList, boolean patientDetailsRequired) {
        this.entityIdList = entityIdList;
        this.patientDetailsRequired = patientDetailsRequired;
    }
}
