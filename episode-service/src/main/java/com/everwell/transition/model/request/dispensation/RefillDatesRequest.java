package com.everwell.transition.model.request.dispensation;

import com.everwell.transition.exceptions.ValidationException;
import lombok.*;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.List;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class RefillDatesRequest {
    List<Long> episodeIds;
    String fromDate;
    String toDate;
}
