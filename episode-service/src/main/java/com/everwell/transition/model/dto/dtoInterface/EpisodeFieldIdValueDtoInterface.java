package com.everwell.transition.model.dto.dtoInterface;

public interface EpisodeFieldIdValueDtoInterface {

    Long getEpisodeId();

    Long getFieldId();

    String getFieldValue();
}
