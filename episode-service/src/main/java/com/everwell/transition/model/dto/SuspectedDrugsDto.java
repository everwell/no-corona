package com.everwell.transition.model.dto;

import com.everwell.transition.model.request.aers.CausalityManagementRequest;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@ToString
@NoArgsConstructor
@Getter
@Setter
public class SuspectedDrugsDto {
    String isoniazid ;
    String rifampicin ;
    String pyrazinamide ;
    String ethambutol ;
    String kanamycin ;
    String amikacin ;
    String capreomycin ;
    String levofloxacin ;
    String moxifloxacin ;
    String cycloserine ;
    String naPAS ;
    String linezolid ;
    String clofazimine ;
    String amxClv ;
    String clarithromycin ;
    String bedaquiline ;
    String delamanid ;
    String pretomonid ;
    String rifapentine ;
    String prothionamide;
    String ethionamde ;

    public SuspectedDrugsDto(CausalityManagementRequest causalityManagementRequest)
    {
        this.amikacin = causalityManagementRequest.getAmikacin();
        this.amxClv = causalityManagementRequest.getAmxClv();
        this.bedaquiline = causalityManagementRequest.getBedaquiline();
        this.capreomycin = causalityManagementRequest.getCapreomycin();
        this.clarithromycin = causalityManagementRequest.getClarithromycin();
        this.clofazimine = causalityManagementRequest.getClofazimine();
        this.cycloserine = causalityManagementRequest.getCycloserine();
        this.delamanid = causalityManagementRequest.getDelamanid();
        this.ethambutol = causalityManagementRequest.getEthambutol();
        this.ethionamde = causalityManagementRequest.getEthionamde();
        this.isoniazid = causalityManagementRequest.getIsoniazid();
        this.kanamycin = causalityManagementRequest.getKanamycin();
        this.levofloxacin = causalityManagementRequest.getLevofloxacin();
        this.linezolid = causalityManagementRequest.getLinezolid();
        this.moxifloxacin = causalityManagementRequest.getMoxifloxacin();
        this.naPAS = causalityManagementRequest.getNaPAS();
        this.pretomonid = causalityManagementRequest.getPretomonid();
        this.prothionamide = causalityManagementRequest.getProthionamide();
        this.pyrazinamide = causalityManagementRequest.getPyrazinamide();
        this.rifampicin = causalityManagementRequest.getRifampicin();
        this.rifapentine = causalityManagementRequest.getRifapentine();
    }
}
