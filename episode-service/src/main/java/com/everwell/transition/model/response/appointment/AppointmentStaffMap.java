package com.everwell.transition.model.response.appointment;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AppointmentStaffMap {
    private Integer id;
    private Long staffId;
    private Integer appointmentId;
}
