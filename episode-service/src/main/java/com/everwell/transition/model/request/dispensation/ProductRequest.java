package com.everwell.transition.model.request.dispensation;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Getter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class ProductRequest {

    public String name;

    public String dosage;

    public String drugCode;

    public String dosageForm;

    public String manufacturer;

    public String composition;

}
