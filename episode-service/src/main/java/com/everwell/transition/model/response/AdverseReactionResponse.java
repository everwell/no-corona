package com.everwell.transition.model.response;

import com.everwell.transition.model.db.AdverseReaction;
import com.everwell.transition.model.dto.KeyValuePair;
import com.everwell.transition.utils.Utils;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.type.TypeReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@AllArgsConstructor
@ToString
@Getter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AdverseReactionResponse {
    Long adverseEventId;
    Long episodeId;
    String currentWeight;
    String reportType;
    String tbType;
    String typeOfCase;
    String drtbType;
    List<KeyValuePair> listOfDrugs;
    String previousExposure;
    String pregnancyStatus;
    String breastfeedingInfant;
    String bdqExposure;
    String linezolidExposure;
    String injectionDrugUse;
    String alcoholUse;
    String tobaccoUse;
    String hivReactive;
    String onsetDate;
    List<KeyValuePair> predominantCondition;
    String otherPredominantCondition;
    String reporterNarrative;
    String severity;
    String daidsGrading;
    String adrSeriousness;
    String dateOfDeath;
    String causeOfDeath;
    String autopsyPerformed;
    String hospitalAdmissionDate;
    String hospitalDischargeDate;

    public AdverseReactionResponse(AdverseReaction adverseReaction, Long episodeId, Long adverseEventId) throws IOException {
        this.episodeId = episodeId;
        this.adverseEventId = adverseEventId;
        this.currentWeight = adverseReaction.getCurrentWeight();
        this.reportType = adverseReaction.getReportType();
        this.typeOfCase = adverseReaction.getTypeOfCase();
        this.tbType = adverseReaction.getTbType();
        this.drtbType = adverseReaction.getDrtbType();
        this.listOfDrugs = adverseReaction.getListOfDrugs() == null ? null : Utils.jsonToObject(adverseReaction.getListOfDrugs(),new TypeReference<List<KeyValuePair>>(){});
        this.previousExposure = adverseReaction.getPreviousExposure();
        this.pregnancyStatus = adverseReaction.getPregnancyStatus();
        this.breastfeedingInfant = adverseReaction.getBreastfeedingInfant();
        this.bdqExposure = adverseReaction.getBdqExposure();
        this.linezolidExposure = adverseReaction.getLinezolidExposure();
        this.injectionDrugUse = adverseReaction.getInjectionDrugUse();
        this.alcoholUse = adverseReaction.getAlcoholUse();
        this.tobaccoUse = adverseReaction.getTobaccoUse();
        this.hivReactive = adverseReaction.getHivReactive();
        this.onsetDate = adverseReaction.getOnsetDate() == null ? null : adverseReaction.getOnsetDate().toString();
        this.predominantCondition =  adverseReaction.getPredominantCondition() == null ? null : Utils.jsonToObject(adverseReaction.getPredominantCondition(),new TypeReference<List<KeyValuePair>>(){});
        this.otherPredominantCondition = adverseReaction.getOtherPredominantCondition();
        this.reporterNarrative = adverseReaction.getReporterNarrative();
        this.severity = adverseReaction.getSeverity();
        this.daidsGrading = adverseReaction.getDaidsGrading();
        this.adrSeriousness = adverseReaction.getAdrSeriousness();
        this.dateOfDeath = adverseReaction.getDateOfDeath() == null ? null : adverseReaction.getDateOfDeath().toString();
        this.autopsyPerformed = adverseReaction.getAutopsyPerformed();
        this.causeOfDeath = adverseReaction.getCauseOfDeath();
        this.hospitalAdmissionDate = adverseReaction.getHospitalAdmissionDate() == null ? null : adverseReaction.getHospitalAdmissionDate().toString();
        this.hospitalDischargeDate = adverseReaction.getHospitalDischargeDate() == null ? null : adverseReaction.getHospitalDischargeDate().toString();

    }
}
