package com.everwell.transition.model.db;

import com.everwell.transition.constants.Constants;
import com.everwell.transition.model.dto.treatmentPlan.TreatmentPlanConfig;
import com.everwell.transition.utils.Utils;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Map;

@Table(name = "treatment_plan")
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class TreatmentPlan {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "client_id")
    @Setter
    private Long clientId;

    @Column(name = "name")
    @Setter
    private String name;

    @Column(name = "config")
    @Setter
    private String config;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.DATE_FORMAT)
    @CreationTimestamp
    @Column(name = "created_on")
    private LocalDateTime createdOn;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.DATE_FORMAT)
    @UpdateTimestamp
    @Column(name = "updated_on")
    private LocalDateTime updatedOn;

    public TreatmentPlan(Long id, Long clientId, String name, String config) {
        this.id = id;
        this.clientId = clientId;
        this.name = name;
        this.config = config;
    }

    public TreatmentPlanConfig getConfig() {
        TreatmentPlanConfig config = null;
        try {
            config = Utils.jsonToObject(this.config, new TypeReference<TreatmentPlanConfig>(){});
        } catch (Exception ignored) {

        }
        return null == config ? new TreatmentPlanConfig() : config;
    }

    public void setConfig(TreatmentPlanConfig config) {
        this.config = Utils.asJsonString(config);
    }

    public TreatmentPlan(Long clientId, String name) {
        this.clientId = clientId;
        this.name = name;
        this.config = Utils.asJsonString(new TreatmentPlanConfig());
    }
}
