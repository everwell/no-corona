package com.everwell.transition.model.request.episodesearch;

import com.everwell.transition.constants.FieldConstants;
import com.everwell.transition.elasticsearch.ElasticConstants;
import com.everwell.transition.exceptions.ValidationException;
import com.everwell.transition.utils.Utils;
import lombok.*;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class EpisodeHeaderSearchRequest extends BaseElasticSearchRequest {
    String searchKey;
    String searchValue;
    List<Long> patientIds;

    public EpisodeSearchRequest convert() {
        EpisodeSearchRequest episodeSearchRequest = new EpisodeSearchRequest();
        if (!Utils.isEmptyOrAllNull(this.hierarchyIds))
            episodeSearchRequest.getSearch().getMust().put(this.typeOfHierarchy + ElasticConstants.HIERARCHY_MAPPING_ALL_ELASTIC_NOMENCLATURE, this.hierarchyIds);
        if (!Utils.isEmptyOrAllNull(this.typeOfEpisode))
            episodeSearchRequest.getSearch().getMust().put(FieldConstants.EPISODE_FIELD_TYPE_OF_EPISODE, this.typeOfEpisode);
        if (!Utils.isEmptyOrAllNull(this.patientIds)) {
            episodeSearchRequest.getSearch().getMust().put(FieldConstants.EPISODE_FIELD_ID, this.patientIds);
        }
        if (searchKey.equalsIgnoreCase(FieldConstants.FIELD_PHONE)) {
            episodeSearchRequest.getSearch().getMust().put(FieldConstants.PERSON_PRIMARY_PHONE, searchValue);
        } else if (searchKey.equalsIgnoreCase(FieldConstants.PERSON_FIELD_NAME)) {
            episodeSearchRequest.getSearch().getMust().put(FieldConstants.PERSON_FIELD_NAME, searchValue);
        } else if (searchKey.equalsIgnoreCase(FieldConstants.FIELD_99DOTS_ID)) {
            episodeSearchRequest.getSearch().getMust().put(FieldConstants.EPISODE_FIELD_ID, searchValue.replaceAll("[^0-9]",""));
        } else {
            episodeSearchRequest.getSearch().getMust().put(searchKey, searchValue);
        }
        episodeSearchRequest.setPage(this.page);
        episodeSearchRequest.setFieldsToShow(this.fieldsToShow);
        episodeSearchRequest.setSortKey(this.sortKey);
        episodeSearchRequest.setSize(this.pageSize);
        episodeSearchRequest.setSortDirection(this.sortDirection);
        if (StringUtils.hasLength(this.collapseField))
            episodeSearchRequest.setCollapseField(this.collapseField);
        return episodeSearchRequest;
    }

    public void validate() {
        if (StringUtils.isEmpty(this.searchKey) || StringUtils.isEmpty(this.searchValue))
            throw new ValidationException("Search key and value must be present!");
    }
}
