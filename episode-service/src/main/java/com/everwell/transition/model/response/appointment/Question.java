package com.everwell.transition.model.response.appointment;

import lombok.Data;

@Data
public class Question {
    private Long id;
    private String type;
    private Long order;
    private String question;
    private String options;
    private String validationList;
}
