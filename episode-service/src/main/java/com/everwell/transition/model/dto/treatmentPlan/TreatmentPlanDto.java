package com.everwell.transition.model.dto.treatmentPlan;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Data
public class TreatmentPlanDto {

    private Long id;

    private String name;

    private List<TreatmentPlanProductMapDto> mappedProducts;

    private List<RegimenTreatmentPlanDto> mappedToRegimens;

    private String mergedAdherence;
}
