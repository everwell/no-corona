package com.everwell.transition.model.response.diagnostics;

import com.everwell.transition.model.dto.EpisodeDto;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TestResponse {

    private Map<String, Object> test;

    private EpisodeDto episodeDto;
}
