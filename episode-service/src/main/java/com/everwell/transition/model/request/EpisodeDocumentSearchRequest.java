package com.everwell.transition.model.request;

import com.everwell.transition.exceptions.ValidationException;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class EpisodeDocumentSearchRequest {

    @Setter
    private List<Long> episodeIds;

    String fromDate;

    String endDate;

    Boolean approved;
    
    public void validate() {
        if(CollectionUtils.isEmpty(this.episodeIds)) {
            throw new ValidationException("episodeIds list cannot be null or empty");
        }
    }
}
