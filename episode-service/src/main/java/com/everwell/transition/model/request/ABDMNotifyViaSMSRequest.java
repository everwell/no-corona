package com.everwell.transition.model.request;

import com.everwell.transition.constants.Constants;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

@Getter
@Setter
public class ABDMNotifyViaSMSRequest {
    public String requestId;
    public String timestamp;
    public ABDMNotifyViaSMSNotificationDto notification;

    public ABDMNotifyViaSMSRequest(String phoneNo)
    {
        DateTimeFormatter formatter =  DateTimeFormatter.ofPattern(Constants.TIMESTAMP_FORMAT);
        LocalDateTime utcNow =  LocalDateTime.now(ZoneOffset.UTC);
        this.requestId = UUID.randomUUID().toString();
        this.timestamp = formatter.format(utcNow);
        this.notification = new ABDMNotifyViaSMSNotificationDto(phoneNo);
    }

    @Getter
    @Setter
    public static class ABDMNotifyViaSMSNotificationDto {
        public String phoneNo;
        public ABDMNotifyViaSMSHipDto hip;

        public ABDMNotifyViaSMSNotificationDto(String phoneNo)
        {
            this.phoneNo = "+91-"+phoneNo;
            this.hip = new ABDMNotifyViaSMSHipDto();
        }

        @Getter
        @Setter
        @NoArgsConstructor
        public static class ABDMNotifyViaSMSHipDto {
            public String name = Constants.ABDM_HIP_NAME;
            public String id = Constants.ABDM_HIP_ID;
        }
    }
}
