package com.everwell.transition.model.request.episodesearch;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Sort;

import java.util.Arrays;
import java.util.List;

@Data
@NoArgsConstructor
public class BaseElasticSearchRequest {
    List<Long> hierarchyIds;
    String typeOfHierarchy;
    List<String> typeOfEpisode;
    List<String> fieldsToShow = Arrays.asList(
        "id", "typeOfEpisode", "FirstName"
    );
    int page = 0;
    int pageSize = 10;
    String sortKey = "id";
    Sort.Direction sortDirection = Sort.Direction.ASC;
    String collapseField;
}
