package com.everwell.transition.model.request.dispensation;

import com.everwell.transition.model.dto.dispensation.DispensationRangeValueFilters;
import lombok.*;

import java.util.List;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class SearchDispensationRequest {
    public List<Long> entityIds;
    public List<DispensationRangeValueFilters> filters;
}
