package com.everwell.transition.model.dto;

import com.everwell.transition.enums.RangeFilterType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RangeFilters {
  private RangeFilterType type;
  private String from;
  private String to;
}
