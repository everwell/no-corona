package com.everwell.transition.model.request;

import com.everwell.transition.exceptions.ValidationException;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class EngagementRequest {
    Long episodeId;
    Boolean receiveSms;
    String timeOfDay;
    Long languageId;
    Boolean householdVisits;
    Boolean callCenterCalls;
    Long userId;

    public void validate() {
        if (null == episodeId) {
            throw new ValidationException("episode Id cannot be null");
        }
    }
}
