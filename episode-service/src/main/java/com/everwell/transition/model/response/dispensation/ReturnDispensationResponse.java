package com.everwell.transition.model.response.dispensation;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ReturnDispensationResponse {

    public List<Long> returnDispensationIds;
}
