package com.everwell.transition.model.dto.dispensation;

import com.everwell.transition.enums.dispensation.DispensationStatus;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;


import java.util.List;

@ToString
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class DispensationData {
    public Long dispensationId;

    public Long entityId;
    
    public String addedDate;

    public String dateOfPrescription;

    public Long weight;

    public String weightBand;

    public String phase;

    public Long drugDispensedForDays;

    public DispensationStatus status;

    public String issuedDate;
    
    public String dosingStartDate;

    public Long issuingFacility;
    
    public String refillDate;

    public Boolean deleted;

    public List<DispensationProduct> productList;

    public String notes;

    public  DispensationData( Long dispensationId, Long entityId, String dosingStartDate, String refillDate, List<DispensationProduct> productList) {
        this.dispensationId= dispensationId;
        this.entityId = entityId;
        this.dosingStartDate = dosingStartDate;
        this.refillDate = refillDate;
        this.productList = productList;
    }
}
