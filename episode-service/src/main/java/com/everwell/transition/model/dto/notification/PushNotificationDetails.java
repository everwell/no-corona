package com.everwell.transition.model.dto.notification;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.util.List;
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class PushNotificationDetails {
    List<PushNotificationTemplateDto> personList;
    Long vendorId;
    Long triggerId;
    Long templateId;
    Boolean sendNotificationData;
    String notificationExtraData;
    Boolean isMandatory;
}
