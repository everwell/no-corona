package com.everwell.transition.model.dto;

import com.everwell.transition.constants.Constants;
import com.everwell.transition.model.db.DiseaseTemplate;
import com.everwell.transition.model.db.Episode;
import com.everwell.transition.model.db.Stage;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class EpisodeDto {

    private Long id;

    private Long clientId;

    private Long personId;

    private boolean deleted;

    private String disease;

    @Setter
    private Long diseaseId;

    private String diseaseIdOptions;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime createdDate;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime startDate;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime endDate;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime lastActivityDate;

    private List<String> currentTags;

    private String riskStatus;

    private Long currentStageId;

    private String stageKey;

    private String stageName;

    private String typeOfEpisode;

    private Long addedBy;

    private Map<String, Object> associations;

    @Setter
    private Map<String, Object> hierarchyMappings;

    private List<TabPermissionDto> tabPermissions;

    private List<EpisodeStageDto> stageDetails;

    @Setter
    private Map<String, Object> stageData = new HashMap<>();

    public EpisodeDto(Episode episode, List<String> currentTags, Stage stage,
                      DiseaseTemplate diseaseTemplate,
                      List<EpisodeStageDto> episodeStageDtos,
                      Map<String, Object> associations,
                      Map<String, Object>  hierarchyMappings,
                      List<TabPermissionDto> tabPermissions,
                      Map<String, Object> stageData) {
        this(episode.getId(),
                episode.getClientId(),
                episode.getPersonId(),
                episode.isDeleted(),
                null != diseaseTemplate ? diseaseTemplate.getDiseaseName() : null,
                episode.getDiseaseId(),
                episode.getDiseaseIdOptions(),
                episode.getCreatedDate(),
                episode.getStartDate(),
                episode.getEndDate(),
                episode.getLastActivityDate(),
                currentTags,
                episode.getRiskStatus(),
                episode.getCurrentStageId(),
                null != stage ? stage.getStageName() : null,
                null != stage ? stage.getStageDisplayName() : null,
                episode.getTypeOfEpisode(),
                episode.getAddedBy(),
                associations,
                hierarchyMappings,
                tabPermissions,
                episodeStageDtos,
                stageData);
    }

    public EpisodeDto(Episode episode, List<String> currentTags, Stage stage) {
        this(episode, currentTags, stage, null, new ArrayList<>(), new HashMap<>(), new HashMap<>(), new ArrayList<>(), new HashMap<>());
    }
}