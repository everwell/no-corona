package com.everwell.transition.model.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@AllArgsConstructor
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
public class AdherenceStatisticsResponse implements Serializable {
  private String from;
  private String to;
  private int technologyDoses;
  private int manualDoses;
  private int totalDoses;
}
