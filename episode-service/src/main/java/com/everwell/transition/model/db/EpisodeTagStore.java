package com.everwell.transition.model.db;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "episode_tag_store")
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class EpisodeTagStore {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "tag_name")
    String tagName;

    @Column(name = "tag_display_name")
    String tagDisplayName;

    @Column(name = "tag_icon")
    String tagIcon;

    @Column(name = "tag_description")
    String tagDescription;

    @Column(name = "tag_group")
    String tagGroup;
}
