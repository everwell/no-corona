package com.everwell.transition.model.dto.notification;

import lombok.*;

import java.util.Map;
@AllArgsConstructor
@NoArgsConstructor
@Data
public class PushNotificationTemplateDto {
    String id;
    String deviceId;
    Map<String, String> headingParameters;
    Map<String, String> contentParameters;
}
