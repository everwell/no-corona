package com.everwell.transition.model.request.adherence;

import lombok.*;

import java.util.Map;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class RecordAdherenceRequest {

    String entityId;

    Long adherenceType;

    Map<String, Character> datesToValueMapList;

}
