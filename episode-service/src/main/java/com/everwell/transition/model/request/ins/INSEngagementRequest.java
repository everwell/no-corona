package com.everwell.transition.model.request.ins;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class INSEngagementRequest {
    String entityId;
    Boolean consent;
    String timeOfDay;
    Long languageId;
    Long typeId;
}
