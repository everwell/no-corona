package com.everwell.transition.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;

@Data
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class EventStreamingAnalyticsDto {

    private String eventCategory;

    private String eventName;

    private String eventAction;

    private Long eventValue;

    private Long eventTime;

    public EventStreamingAnalyticsDto(String eventCategory,String eventName, String eventAction) {
        this.eventName = eventName;
        this.eventAction = eventAction;
        this.eventCategory = eventCategory;
        this.eventTime = new Date().getTime();
    }

}
