package com.everwell.transition.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class AdherenceDataForCalendar {
    char code;
    String dayName;
    List<String> tagsForDay;
}

