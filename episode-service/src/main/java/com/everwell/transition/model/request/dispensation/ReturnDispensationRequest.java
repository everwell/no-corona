package com.everwell.transition.model.request.dispensation;

import lombok.*;

import java.util.List;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class ReturnDispensationRequest {
    public Long dispensationId;
    public List<ReturnDispensationData> returnDispensationProductDetails;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class ReturnDispensationData {

        Long transactionId;
        Long returnQuantity;
        String reasonForReturn;
        Long updatedBy;
        String batchNumber;
        String expiryDate;
        Long pInventoryId;
    }

}
