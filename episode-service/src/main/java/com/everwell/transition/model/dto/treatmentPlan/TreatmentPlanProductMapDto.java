package com.everwell.transition.model.dto.treatmentPlan;

import com.everwell.transition.constants.Constants;
import com.everwell.transition.model.request.treatmentPlan.DoseDetails;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class TreatmentPlanProductMapDto {

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.DATE_FORMAT)
    private LocalDateTime createdDate;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.DATE_FORMAT)
    private LocalDateTime startDate;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.DATE_FORMAT)
    private LocalDateTime endDate;

    private Long id;

    private String name;

    private String schedule;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.DATE_FORMAT)
    private LocalDateTime refillDueDate;

    String adherenceString;

    List<Map<String, Object>> doseDetailsList;

    private String otherDrugName;

    private Long treatmentPlanProductMapId;

    private String otherDosageForm;

    private Boolean isSos;

    public TreatmentPlanProductMapDto(LocalDateTime createdDate, LocalDateTime startDate, LocalDateTime endDate, Long id, String schedule, LocalDateTime refillDueDate, String otherDrugName, Long treatmentPlanProductMapId, String otherDosageForm, Boolean isSos) {
        this.createdDate = createdDate;
        this.startDate = startDate;
        this.endDate = endDate;
        this.id = id;
        this.schedule = schedule;
        this.refillDueDate = refillDueDate;
        this.otherDrugName = otherDrugName;
        this.treatmentPlanProductMapId = treatmentPlanProductMapId;
        this.otherDosageForm = otherDosageForm;
        this.isSos = isSos;
    }

}
