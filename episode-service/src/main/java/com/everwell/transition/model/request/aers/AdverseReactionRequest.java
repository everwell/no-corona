package com.everwell.transition.model.request.aers;

import com.everwell.transition.exceptions.ValidationException;
import com.everwell.transition.model.dto.KeyValuePair;
import lombok.*;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class AdverseReactionRequest {
        Long adverseEventId;
        Long episodeId;
        String currentWeight;
        String reportType;
        String typeOfCase;
        String tbType;
        String drtbType;
        List<KeyValuePair> listOfDrugs;
        String previousExposure;
        String pregnancyStatus;
        String breastfeedingInfant;
        String bdqExposure;
        String linezolidExposure;
        String injectionDrugUse;
        String alcoholUse;
        String tobaccoUse;
        String hivReactive;
        String onsetDate;
        List<KeyValuePair> predominantCondition;
        String otherPredominantCondition;
        String reporterNarrative;
        String severity;
        String daidsGrading;
        String adrSeriousness;
        String dateOfDeath;
        String causeOfDeath;
        String autopsyPerformed;
        String hospitalAdmissionDate;
        String hospitalDischargeDate;

        public AdverseReactionRequest(Long adverseEventId, Long episodeId) {
                this.adverseEventId = adverseEventId;
                this.episodeId = episodeId;
        }

        public AdverseReactionRequest(Long adverseEventId, Long episodeId, String reportType, String onsetDate, List<KeyValuePair> predominantCondition, String severity) {
                this.adverseEventId = adverseEventId;
                this.episodeId = episodeId;
                this.reportType = reportType;
                this.onsetDate = onsetDate;
                this.predominantCondition = predominantCondition;
                this.severity = severity;
        }

        public void validate(AdverseReactionRequest adverseReactionRequest) {
                if (adverseReactionRequest.getEpisodeId() == null) {
                        throw new ValidationException("episode id is required");
                }

                if(!StringUtils.hasText(adverseReactionRequest.getReportType())) {
                        throw new ValidationException("Report type is required");
                }

                if(!StringUtils.hasText(adverseReactionRequest.getOnsetDate())) {
                        throw new ValidationException("Onset date is required");
                }

                if(!StringUtils.hasText(adverseReactionRequest.getSeverity())) {
                        throw new ValidationException("Severity is required");
                }

                if(CollectionUtils.isEmpty(adverseReactionRequest.getPredominantCondition())) {
                        throw new ValidationException("Predominant condition is required");
                }
        }
}
