package com.everwell.transition.model.db;

import com.everwell.transition.constants.Constants;
import com.everwell.transition.utils.Utils;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Table(name = "tb_champion_activity")
@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class TBChampionActivity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "episode_id")
    Long episodeId;

    @Column(name = "created_on")
    private LocalDate createdOn;

    @Column(name = "patient_visits")
    Long patientVisits;

    @Column(name = "meeting_count")
    Long meetingCount;

    @Column(name = "other_acitivity")
    String otherActivity;

    @Column(name = "file_uploaded")
    private String fileUploaded;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.DATE_FORMAT)
    @UpdateTimestamp
    @Column(name = "updated_on")
    private LocalDateTime updatedOn;

    public TBChampionActivity(String date,Long episodeId, Long patientVisits, Long meetingCount, String otherActivity,String fileUploaded) {
        this.createdOn = Utils.toLocalDate(date,"dd/MM/yyyy");
        this.episodeId = episodeId;
        this.patientVisits = patientVisits;
        this.meetingCount = meetingCount;
        this.otherActivity = otherActivity;
        this.fileUploaded = fileUploaded;
    }

}
