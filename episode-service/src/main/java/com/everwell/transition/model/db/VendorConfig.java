package com.everwell.transition.model.db;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "vendor_config")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class VendorConfig {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "client_id")
    private String clientId;

    @Column(name = "config", columnDefinition = "TEXT")
    private String config;

    @Column(name = "credentials", columnDefinition = "TEXT")
    private String credentials;

    @Column(name = "vendor")
    private String vendor;

}
