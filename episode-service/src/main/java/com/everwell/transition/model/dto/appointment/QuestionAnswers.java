package com.everwell.transition.model.dto.appointment;

import lombok.*;

@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class QuestionAnswers {
    private Long fieldId;
    private String answer;
}
