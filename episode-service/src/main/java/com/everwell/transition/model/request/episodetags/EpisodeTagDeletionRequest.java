package com.everwell.transition.model.request.episodetags;

import com.everwell.transition.exceptions.ValidationException;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EpisodeTagDeletionRequest {
    Long episodeId;
    List<String> tagList;
    List<String> tagDates;

    public void validate() {
        if (episodeId == null)
            throw new ValidationException("EpisodeId must be present");
        if (CollectionUtils.isEmpty(tagList))
            throw new ValidationException("TagList must be present");
        if (CollectionUtils.isEmpty(tagDates))
            throw new ValidationException("TagDates must be present");
    }


}
