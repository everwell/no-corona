package com.everwell.transition.model.db;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "tab_permission")
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class TabPermission {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "disease_stage_mapping_id")
    private Long diseaseStageMappingId;

    @Column(name = "tab_id")
    private Long tabId;

    @Column(name = "add")
    private boolean add;

    @Column(name = "edit")
    private boolean edit;

    @Column(name = "delete")
    private boolean delete;

    @Column(name = "view")
    private boolean view;

    @Column(name = "tab_order")
    private Long tabOrder;
}
