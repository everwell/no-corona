package com.everwell.transition.model.response;

import com.everwell.transition.model.dto.AdherenceMonthWiseData;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
@AllArgsConstructor
public class AdherenceDetailsWithMonths {

    Long episodeId;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    private LocalDateTime lastDosage;

    private int technologyDoses;

    private int manualDoses;

    private int totalDoses;

    List<AdherenceMonthWiseData> adherenceMonthWiseData;

}
