package com.everwell.transition.model.dto;

import com.everwell.transition.elasticsearch.indices.EpisodeIndex;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EpisodeSearchResult {
    long totalHits;
    List<EpisodeIndex> episodeIndexList;
    Map<String, Map<String, Long>> aggregationResults;
    String requestIdentifier;
    String scrollId;

    public EpisodeSearchResult(long totalHits, List<EpisodeIndex> episodeIndexList, Map<String, Map<String, Long>> aggregationResults) {
        this.totalHits = totalHits;
        this.episodeIndexList = episodeIndexList;
        this.aggregationResults = aggregationResults;
    }
}
