package com.everwell.transition.model.request.episodelogs;

import com.everwell.transition.exceptions.ValidationException;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SyncLogsRequest {

    Long episodeId;
    String endDate;

    public void validate()
    {
        if (episodeId == null || episodeId == 0)
            throw new ValidationException("episodeId must not be null!");
        if (!StringUtils.hasText(endDate))
            throw new ValidationException("End Date cannot be empty!");
    }
}
