package com.everwell.transition.model.response.diagnostics;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.util.Map;


@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddTestResponseV2 {


    Long testRequestId;

    String reason;

    String status;

    Map<String, ResultSampleResponse> typeMap;
}
