package com.everwell.transition.model.dto.appointment;

import com.everwell.transition.constants.AppointmentConstants;
import com.everwell.transition.model.db.Appointment;
import com.everwell.transition.model.request.appointment.AppointmentRequest;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AppointmentDetails {
    private Integer id;
    private Long episodeId;
    private String firstName;
    private String lastName;
    private List<String> emailIds;
    private String gender;
    private Integer age;
    private LocalDateTime appointmentSlot;
    private String appointmentStatus;
    private String appointmentType;
    private Integer visitCount;
    private LocalDateTime addedOn;
}
