package com.everwell.transition.model.dto;

import com.everwell.transition.model.response.EpisodeTagResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ElasticDocTagsWrapper {
    List<EpisodeTagResponse> episodeTagResponse;
}
