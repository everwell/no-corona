package com.everwell.transition.model.db;

import com.everwell.transition.constants.Constants;
import com.everwell.transition.utils.Utils;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Table(name = "regimen_treatment_plan_map")
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class RegimenTreatmentPlanMap {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.DATE_FORMAT)
    @Column(name = "created_date")
    @Getter
    private LocalDateTime createdDate;

    @Getter
    @Column(name = "regimen_id")
    private Long regimenId;

    @Column(name = "treatment_plan_id")
    private Long treatmentPlanId;

    public RegimenTreatmentPlanMap(Long regimenId, Long treatmentPlanId) {
        this.createdDate = Utils.getCurrentDateNew();
        this.regimenId = regimenId;
        this.treatmentPlanId = treatmentPlanId;
    }
}
