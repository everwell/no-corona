package com.everwell.transition.model.response;

import com.everwell.transition.model.dto.AdherenceData;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Data
public class AdherenceResponse {

    @Setter
    @Getter
    public String entityId;

    @Setter
    @Getter
    public List<AdherenceData> adherenceData;

    @Setter
    @Getter
    private String adherenceString;

    @Setter
    @Getter
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    private LocalDateTime lastDosage;

    @Setter
    @Getter
    private int technologyDoses;

    @Setter
    @Getter
    private int manualDoses;

    @Setter
    @Getter
    private int totalDoses;

    @Setter
    @Getter
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    private LocalDateTime startDate;

    @Setter
    @Getter
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    private LocalDateTime endDate;

    @Setter
    @Getter
    public AdherenceStatisticsResponse adherenceStatistics;

    @Setter
    @Getter
    private long consecutiveMissedDoses;

    @Setter
    @Getter
    private long consecutiveMissedDosesFromYesterday;

    private List<Map<String, Object>> doseTimeList;

    private String monitoringMethod;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    private LocalDateTime lastMissedDosage;

    public AdherenceResponse(LocalDateTime startDate, LocalDateTime endDate, String adherenceString) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.adherenceString = adherenceString;
    }

}
