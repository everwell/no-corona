package com.everwell.transition.model.request.diagnostics;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GetSampleIdListRequest {
    Long episodeId;

    List<String> stagesToExclude;
}
