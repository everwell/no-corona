package com.everwell.transition.model.request.episodetags;

import com.everwell.transition.exceptions.ValidationException;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SearchTagsRequest {
    List<Long> episodeIdList;
    List<String> tagsToFilter;

    public void validate() {
        if (episodeIdList == null)
            throw new ValidationException("Episode Id List cannot be empty!");
        if (episodeIdList.isEmpty()) {
            throw new ValidationException("Episode Id List cannot be empty!");
        }
    }

}
