package com.everwell.transition.constants;

public class INSConstants {

    public static final String FIELD_ENGAGEMENT_LIST = "engagementRequestList";

    public static final String FIELD_TYPE_ID = "typeId";

    public static final String FIELD_TYPE = "type";

    public static final String TYPE_SMS = "SMS";
    public final static String VENDOR_ID = "vendorId";
    public final static String TRIGGER_ID = "triggerId";
    public final static String TEMPLATE_ID = "templateId";
}
