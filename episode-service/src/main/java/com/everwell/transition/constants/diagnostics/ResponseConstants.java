package com.everwell.transition.constants.diagnostics;

public class ResponseConstants {
    public static final String PATIENT_NAME = "patientName";
    public static final String ENTITY_ID = "entityId";
    public static final String ERROR_MESSAGE_FOR_GET_SAMPLE = "Unable to get sample details from diagnostics";
    public static final String ERROR_MESSAGE_FOR_ADD_TEST = "Unable to add test in diagnostics";
}
