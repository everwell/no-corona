package com.everwell.transition.constants.diagnostics;

public class RequestConstants {
    public static final String QUERY_PARAM_BY = "by";
    public static final String QUERY_PARAM_SAMPLE_ID = "sampleId";
    public static final String QUERY_PARAM_QR_CODE = "qrCode";
    public static final String QUERY_PARAM_JOURNEY_DETAILS = "journeyDetails";

    public static final String FILTER_PARAM_POSITIVE = "positive";
}
