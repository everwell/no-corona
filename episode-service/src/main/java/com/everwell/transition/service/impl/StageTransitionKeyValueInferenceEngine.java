package com.everwell.transition.service.impl;

import com.everwell.transition.model.Rule;
import com.everwell.transition.model.RuleNamespace;
import com.everwell.transition.service.KeyValueInferenceEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class StageTransitionKeyValueInferenceEngine extends KeyValueInferenceEngine {

    @Override
    public String getRuleNamespace() {
        return RuleNamespace.STAGE_TRANSITION.name();
    }

    @Override
    protected List<String> getRequiredFields(Rule rule) {
        String input = rule.getCondition();
        String pattern = "(?i)input\\.get\\(\"(.*?)\"\\)";
        Pattern r = Pattern.compile(pattern);
        Matcher m = r.matcher(input);
        String matchedString = "";
        List<String> fields = new ArrayList<>();
        while(m.find()) {
            matchedString = m.group();
            matchedString = matchedString.replaceAll("input\\.get\\(\"", "");
            matchedString = matchedString.replaceAll("\"\\)","");
            fields.add(matchedString);
        }
        return fields;
    }
}
