package com.everwell.transition.service.impl;

import com.everwell.transition.exceptions.ValidationException;
import com.everwell.transition.model.dto.notification.EpisodeListDto;
import com.everwell.transition.model.response.Response;
import com.everwell.transition.service.NikshayService;
import com.everwell.transition.utils.Utils;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@Service
public class NikshayServiceImpl extends DataGatewayHelperServiceImpl implements NikshayService {

    public ResponseEntity<Response<EpisodeListDto>> getEpisodesWithContactTracing(EpisodeListDto episodeListDto) {
        String endpoint = "/nikshay/api/Patients/GetEpisodesWithContactTracing";
        return postExchange(endpoint, episodeListDto, new ParameterizedTypeReference<EpisodeListDto>() {});
    }

    @Override
    public List<Long> getEpisodesWithContactTracingFilter(List<Long> episodeIds) {
        List<CompletableFuture<ResponseEntity<Response<EpisodeListDto>>>> allFutures = new ArrayList<>();
        List<List<Long>> episodeIdBatches = Utils.getBatches(episodeIds, 1000);
        episodeIdBatches.forEach(e -> {
            EpisodeListDto episodeListDto = new EpisodeListDto(e);
            allFutures.add(
                    CompletableFuture.supplyAsync(() -> getEpisodesWithContactTracing(episodeListDto))
            );
        });
        CompletableFuture.allOf(allFutures.toArray(new CompletableFuture[0])).join();
        List<Long> episodeIdWithContactTracingAdded = new ArrayList<>();
        for (int i = 0; i < episodeIdBatches.size(); i++) {
            try {
                ResponseEntity<Response<EpisodeListDto>> partialResponse = allFutures.get(i).get();
                if (partialResponse.getBody() != null && partialResponse.getBody().isSuccess()) {
                    episodeIdWithContactTracingAdded.addAll(partialResponse.getBody().getData().getEpisodeIds());
                }
            } catch (Exception e) {
                throw new ValidationException(e.toString());
            }
        }
        return episodeIdWithContactTracingAdded;
    }
}

