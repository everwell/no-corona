package com.everwell.transition.service.impl;

import com.everwell.transition.exceptions.InternalServerErrorException;
import com.everwell.transition.model.db.VendorConfig;
import com.everwell.transition.model.dto.wix.WixPostFilter;
import com.everwell.transition.model.request.wix.WixPostResponse;
import com.everwell.transition.repositories.VendorConfigRepository;
import com.everwell.transition.service.ClientService;
import com.everwell.transition.service.WixApiIntegrationService;
import com.everwell.transition.utils.Utils;
import com.fasterxml.jackson.core.type.TypeReference;
import io.sentry.Sentry;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class WixApiIntegrationServiceImpl implements WixApiIntegrationService {

    @Autowired
    VendorConfigRepository vendorConfigRepository;
    @Autowired
    ClientService clientService;

    public RestTemplate restTemplate = new RestTemplate();
    public String BASE_WIX_URL = "https://www.wixapis.com";

    //Once refresh token is obtained, the original auth api starts to give error, so that is why
    //we have hard coded this value in DB config as well. Next time onwards refresh token is used
    //to obtain auth_token, and refresh token remains same.
    @Setter
    private String refreshToken;
    private String authToken = null;
    private String BASE_SITE_URL = "";
    @Setter
    private Map<String, String> credentialsMap = new HashMap<>();
    private final String MY_VENDOR = "wix";

    @Override
    public void setAuthToken() {

        if (credentialsMap.isEmpty()) {
            setCredentials(clientService.getClientByTokenOrDefault().getId(), MY_VENDOR);
        }

        if (StringUtils.hasLength(refreshToken)) {
            refreshAuthToken();
            return;
        }

        String endPoint = "/oauth/access";
        String requestUrl = BASE_WIX_URL + endPoint;

        Map<String, String> jsonBody = new HashMap<>();

        jsonBody.put("grant_type", "authorization_code");
        jsonBody.put("client_id", credentialsMap.get("client_id"));
        jsonBody.put("client_secret", credentialsMap.get("client_secret"));
        jsonBody.put("code", credentialsMap.get("code"));

        HttpEntity<Map<String, String>> requestEntity = new HttpEntity<>(jsonBody);

        ResponseEntity<Map<String, Object>> response = restTemplate.exchange(
                requestUrl,
                HttpMethod.POST,
                requestEntity,
                new ParameterizedTypeReference<Map<String, Object>>() {}
        );

        if (response.getStatusCode() == HttpStatus.OK) {
            Map<String, Object> responseBody = response.getBody();
            refreshToken = String.valueOf(responseBody.get("refresh_token"));
            authToken = String.valueOf(responseBody.get("access_token"));
        }
    }

    private void setCredentials(Long clientId, String vendor) {
        VendorConfig config = vendorConfigRepository.findByClientIdAndVendor(String.valueOf(clientId), vendor);
        try {
            Map<String, String> credentials = Utils.jsonToObject(config.getCredentials(), new TypeReference<Map<String, String>>() {});
            credentialsMap.put("client_id", credentials.get("client_id"));
            credentialsMap.put("client_secret", credentials.get("client_secret"));
            credentialsMap.put("refresh_token", credentials.get("refresh_token"));
            credentialsMap.put("code", credentials.get("code"));
            refreshToken = credentials.get("refresh_token");
            BASE_SITE_URL = credentials.get("base_site_url");
        } catch (IOException e) {
            Sentry.capture(e);
            throw new InternalServerErrorException("[setCredentials] unable to parse config, vendor: " + vendor);
        }
    }

    @Override
    public void refreshAuthToken() {
        boolean retry = false;
        int retryCount = 3;

        String endPoint = "/oauth/access";
        String requestUrl = BASE_WIX_URL + endPoint;

        Map<String, String> jsonBody = new HashMap<>();

        jsonBody.put("grant_type", "refresh_token");
        jsonBody.put("client_id", credentialsMap.get("client_id"));
        jsonBody.put("client_secret", credentialsMap.get("client_secret"));
        jsonBody.put("refresh_token", credentialsMap.get("refresh_token"));

        MultiValueMap<String, String> headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");

        do {
            HttpEntity<Map<String, String>> requestEntity = new HttpEntity<>(jsonBody, headers);
            try {
                ResponseEntity<Map<String, Object>> response = restTemplate.exchange(
                        requestUrl,
                        HttpMethod.POST,
                        requestEntity,
                        new ParameterizedTypeReference<Map<String, Object>>() {
                        }
                );
                Map<String, Object> responseBody = response.getBody();
                refreshToken = String.valueOf(responseBody.get("refresh_token"));
                authToken = String.valueOf(responseBody.get("access_token"));
            } catch (HttpClientErrorException ex) {
                if (ex.getStatusCode() == HttpStatus.FORBIDDEN) {
                    refreshToken = null;
                    setAuthToken();
                }
                retry = true;
                retryCount--;
            }
        } while (retry && retryCount > 0);
    }

    @Override
    public Map<String, WixPostResponse> listCategoriesFilter(Map<String, String> filters) {
        Map<String, WixPostResponse> categoryIdToDetails = new HashMap<>();
        int retryCount = 3;
        String endPoint = "/blog/v3/categories/query";
        String requestUrl = BASE_WIX_URL  + endPoint;
        boolean retry = false;
        do {
            MultiValueMap<String, String> headers = new HttpHeaders();
            headers.add("Content-Type", "application/json");
            headers.add("Authorization", authToken);

            Map<String, Object> jsonBody = new HashMap<>();
            jsonBody.put("filter", filters);

            HttpEntity<Map<String, Object>> requestEntity = new HttpEntity<>(jsonBody, headers);
            try {
                ResponseEntity<Map<String, Object>> response = restTemplate.exchange(
                        requestUrl,
                        HttpMethod.POST,
                        requestEntity,
                        new ParameterizedTypeReference<Map<String, Object>>() {}
                );
                if (response.getStatusCode() == HttpStatus.OK) {
                    Map<String, Object> responseBody = response.getBody();
                    List<Map<String, Object>> categories = (List<Map<String, Object>>) responseBody.get("categories");
                    for (Map<String, Object> category: categories) {
                        WixPostResponse wixPostResponse = new WixPostResponse();
                        wixPostResponse.setTitle(String.valueOf(category.get("title")));
                        Map<String, Object> imageObj = (Map<String, Object>) ((Map<String, Object>) category.get("coverMedia")).get("image");
                        if (imageObj != null)
                            wixPostResponse.setImageUrl(String.valueOf(imageObj.getOrDefault("url", "")));
                        wixPostResponse.setCategory(String.valueOf(category.get("label")));
                        wixPostResponse.setLanguage(String.valueOf(category.get("language")));
                        categoryIdToDetails.put(String.valueOf(category.get("id")), wixPostResponse);
                        wixPostResponse.setPostUrl(BASE_SITE_URL + "/" + filters.getOrDefault("language", "en") + "/knowledge/categories/" + String.valueOf(category.get("slug")));
                    }
                }
                retry = false;
            } catch (HttpClientErrorException ex) {
                if (ex.getStatusCode() == HttpStatus.FORBIDDEN) {
                    setAuthToken();
                }
                retry = true;
                retryCount--;
            }
        } while (retry && retryCount > 0);
        return categoryIdToDetails;
    }

    @Override
    public List<WixPostResponse> listPosts(WixPostFilter wixPostFilter) {
        int retryCount = 3;
        String endPoint = "/blog/v3/posts";
        String requestUrl = BASE_WIX_URL + endPoint;
        requestUrl = requestUrl + "?language=" + wixPostFilter.getLanguage();
        if (wixPostFilter.getFeatured()) {
            requestUrl = requestUrl + "&featured=true";
        }
        if (!CollectionUtils.isEmpty(wixPostFilter.getCategoryIds())) {
            requestUrl = requestUrl + "&categoryIds=" + String.join(",", wixPostFilter.getCategoryIds());
        }
        boolean retry = false;
        List<WixPostResponse> wixPostResponseList = new ArrayList<>();
        do {
            MultiValueMap<String, String> headers = new HttpHeaders();
            headers.add("Content-Type", "application/json");
            headers.add("Authorization", authToken);
            HttpEntity<Map<String, String>> requestEntity = new HttpEntity<>(null, headers);
            try {
                ResponseEntity<Map<String, Object>> response = restTemplate.exchange(
                        requestUrl,
                        HttpMethod.GET,
                        requestEntity,
                        new ParameterizedTypeReference<Map<String, Object>>() {}
                );
                if (response.getStatusCode() == HttpStatus.OK) {
                    Map<String, Object> responseBody = response.getBody();
                    List<Map<String, Object>> posts = (List<Map<String, Object>>) responseBody.get("posts");
                    for (Map<String, Object> post: posts) {
                        WixPostResponse wixPostResponse = new WixPostResponse();
                        wixPostResponse.setTitle(String.valueOf(post.get("title")));
                        Map<String, Object> imageObj = (Map<String, Object>) ((Map<String, Object>) post.get("coverMedia")).get("image");
                        wixPostResponse.setImageUrl(String.valueOf(imageObj.get("url")));
                        wixPostResponse.setPostUrl(BASE_SITE_URL + "/" + wixPostFilter.getLanguage() + "/post/" + String.valueOf(post.get("slug")));
                        wixPostResponse.setLanguage(String.valueOf(post.get("language")));
                        wixPostResponseList.add(wixPostResponse);
                    }
                }
                retry = false;
            } catch (HttpClientErrorException ex) {
                if (ex.getStatusCode() == HttpStatus.FORBIDDEN) {
                    setAuthToken();
                }
                retry = true;
                retryCount--;
            }
        } while (retry && retryCount > 0);
        return wixPostResponseList;
    }

}
