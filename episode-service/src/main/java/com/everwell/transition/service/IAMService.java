package com.everwell.transition.service;

import com.everwell.transition.model.dto.IAMScheduleDto;
import com.everwell.transition.model.request.adherence.*;
import com.everwell.transition.model.request.adherence.EntityRequest;
import com.everwell.transition.model.request.adherence.RecordAdherenceBulkRequest;
import com.everwell.transition.model.request.adherence.RecordAdherenceRequest;
import com.everwell.transition.model.request.adherence.SearchAdherenceRequest;
import com.everwell.transition.model.response.AdherenceCodeConfigResponse;
import com.everwell.transition.model.response.AdherenceResponse;
import com.everwell.transition.model.response.AllAdherenceResponse;
import com.everwell.transition.model.response.Response;
import com.everwell.transition.model.response.adherence.AllAvgAdherenceResponse;
import com.everwell.transition.model.response.adherence.PositiveCountResponse;
import com.everwell.transition.model.response.adherence.AdherenceGlobalConfigResponse;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Map;

public interface IAMService {

    ResponseEntity<Response<List<AdherenceCodeConfigResponse>>> getAdherenceConfig();

    ResponseEntity<Response<AdherenceGlobalConfigResponse>> getAdherenceGlobalConfig(Map<String, Object> configRequest);

    ResponseEntity<Response<AdherenceResponse>> getAdherence(Long episodeId, Boolean logsRequired);

    ResponseEntity<Response<AdherenceResponse>> getAdherence(String episodeId, Boolean logsRequired);

    ResponseEntity<Response<AllAdherenceResponse>> getAdherenceBulk(SearchAdherenceRequest adherenceRequest, Boolean merge);

    ResponseEntity<Response<AllAdherenceResponse>> getAdherenceBulk(SearchAdherenceRequest adherenceRequest);

    ResponseEntity<Response<Void>> recordAdherenceForMultipleDates(RecordAdherenceRequest recordAdherenceRequest);

    ResponseEntity<Response<Void>> recordAdherenceBulk(RecordAdherenceBulkRequest recordAdherenceBulkRequest);

    Boolean registerEntity(String entityId, String monitoringMethod, String uniqueIdentifier, String startDate, IAMScheduleDto scheduleInfo, Object doseDetails);

    ResponseEntity<Response<Map<String, Object>>> registerEntity(EntityRequest entityRequest);

    ResponseEntity<Response<Map<String, Object>>> updateEntity(EntityRequest entityRequest);

    ResponseEntity<Response<Map<String, Object>>> deleteEntity(String entityId);

    ResponseEntity<Response<Map<String, Object>>> deleteEntity(String entityId, String endDate, Boolean updateEndDate, Boolean deleteIfRequired);

    ResponseEntity<Response<PositiveCountResponse>> getPositiveEventCount(PositiveAdherenceRequest positiveAdherenceRequest);

    ResponseEntity<Response<AllAvgAdherenceResponse>> getAverageAdherence(AverageAdherenceRequest averageAdherenceRequest);

    ResponseEntity<Response<List<Map<String, Object>>>> getImei(String entityId, boolean active);

    ResponseEntity<Response<List<Map<String, Object>>>> getImeiBulk(Map<String, Object> imeiRequest);

    ResponseEntity<Response<List<Map<String, Object>>>> getActiveEntityFromImei(String imei);

    ResponseEntity<Response<Map<String, Object>>> updateVideoStatus(Map<String, Object> reviewAdherenceRequest);

    ResponseEntity<Response<Long>> reallocateIMEI(Map<String, Object> imeiEditRequest);

    ResponseEntity<Response<String>> editPhone(Map<String, Object> phoneEditDto);
    
    ResponseEntity<Response<String>> closeCase(Long episodeId, String endDate, Boolean deleteIfRequired, Boolean updateEndDate);

    ResponseEntity<Response<Map<String, Object>>> reopenEntity(EntityRequest entityRequest);

}
