package com.everwell.transition.service.impl;

import com.everwell.transition.constants.Constants;
import com.everwell.transition.model.db.Trigger;
import com.everwell.transition.model.dto.notification.NotificationTriggerDto;
import com.everwell.transition.model.dto.notification.PushNotificationTemplateDto;
import com.everwell.transition.repositories.TriggerRepository;
import com.everwell.transition.service.ClientService;
import com.everwell.transition.service.NotificationService;
import com.everwell.transition.service.TriggerService;
import org.jobrunr.scheduling.JobScheduler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class TriggerServiceImpl implements TriggerService {

    @Autowired
    TriggerRepository triggerRepository;

    @Autowired
    JobScheduler jobScheduler;

    @Autowired
    NotificationService notificationService;

    @Scheduled
    public void readTriggerTable()
    {
        List<Trigger> triggers = triggerRepository.getAllByCronTimeNotNull();
        triggers.forEach(trigger -> {
            NotificationTriggerDto notificationTriggerDto = new NotificationTriggerDto(trigger.getVendorId(), trigger.getDefaultTemplateId(), trigger.getTriggerId(), trigger.getFunctionName(), trigger.getTimeZone());
            jobScheduler.delete(trigger.getEventName());
            if(trigger.getNotificationType().equals(Constants.PUSH_NOTIFICATION)) {
                jobScheduler.scheduleRecurrently(trigger.getEventName(), trigger.getCronTime(), () -> notificationService.sendPushNotificationGeneric(notificationTriggerDto, trigger.getClientId(), trigger.getEventName()));
            }
        });
    }

}
