package com.everwell.transition.service;

import com.everwell.transition.model.db.DiseaseTemplate;
import com.everwell.transition.model.db.Field;
import com.everwell.transition.model.db.Regimen;
import com.everwell.transition.model.dto.DiseaseStageFieldMapDto;
import com.everwell.transition.model.dto.TabPermissionDto;
import com.everwell.transition.model.response.DiseaseTemplateResponse;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface DiseaseTemplateService {

    Map<Long, List<DiseaseStageFieldMapDto>> getDefinedStageIdKeyMappingsListMap(Set<Long> diseaseIds, List<Long> stageIds);

    Map<Long, List<TabPermissionDto>> getStageToTabPermissionsListMapForStageIds(Set<Long> diseaseIds, List<Long> stageIds, Map<String, Object> episodeStageData);

    Map<Long, DiseaseTemplate> getDiseaseIdTemplateMap(List<Long> diseaseIds);

    List<Field> getAllFieldsByNameIn(List<String> fieldNames);

    List<Field> getAllFields();

    List<Regimen> getRegimensForDisease(Long diseaseId, String diseaseName, Long clientId);

    DiseaseTemplateResponse getDiseasesForClient(Long clientId);

    DiseaseTemplate getDefaultDiseaseForClientId(Long clientId);
}