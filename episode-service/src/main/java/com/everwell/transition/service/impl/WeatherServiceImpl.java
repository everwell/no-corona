package com.everwell.transition.service.impl;

import com.everwell.transition.model.db.VendorConfig;
import com.everwell.transition.model.response.WeatherResponse;
import com.everwell.transition.repositories.VendorConfigRepository;
import com.everwell.transition.service.WeatherService;
import com.everwell.transition.utils.Utils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class WeatherServiceImpl implements WeatherService {

    private static final String VENDOR = "openweathermap";
    private static final String SECRET_KEY = "client_secret";
    private static final String BASE_URL = "base_site_url";
    private static final String IMAGE_URL = "image_url";

    @Autowired
    private VendorConfigRepository configRepository;

    private final RestTemplate restTemplate = new RestTemplate();

    @Override
    public WeatherResponse getCompleteWeather(Double lat, Double lon, Long clientId) {
        WeatherResponse weatherResponse = new WeatherResponse();
        VendorConfig config = configRepository.findByClientIdAndVendor(String.valueOf(clientId), VENDOR);
        try {
            Map<String, Object> vendorConfig = Utils.jsonToObject(config.getCredentials(), new TypeReference<Map<String, Object>>() {});
            Map<String, Object> response = getWeather(lat, lon,  vendorConfig.get(BASE_URL) + "weather", vendorConfig.get(SECRET_KEY).toString());
            if (response.containsKey("coord")) {
                Map<String, Object> coord = (Map<String, Object>) response.get("coord");
                weatherResponse.setCoord(new WeatherResponse.Coord(coord.get("lon").toString(), coord.get("lat").toString()));
            }
            if (response.containsKey("main")) {
                Map<String, Object> mainMap = (Map<String, Object>) response.get("main");
                weatherResponse.setMain(new WeatherResponse.Main(
                        mainMap.get("temp").toString(),
                        mainMap.get("feels_like").toString(),
                        mainMap.get("temp_min").toString(),
                        mainMap.get("temp_max").toString(),
                        mainMap.get("pressure").toString(),
                        mainMap.get("humidity").toString()
                ));
            }
            if (response.containsKey("weather")) {
                List<Map<String, Object>> weatherMap = (List<Map<String, Object>>) response.get("weather");
                List<WeatherResponse.Weather> weatherList = new ArrayList<>();
                weatherMap.forEach(w -> {
                    weatherList.add(new WeatherResponse.Weather(
                            w.get("main").toString(),
                            w.get("description").toString(),
                            w.get("icon").toString(),
                            String.format(vendorConfig.get(IMAGE_URL).toString(), w.get("icon").toString())
                    ));
                });
                weatherResponse.setWeather(weatherList);
            }
            Map<String, Object> airPollutionResponse = getWeather(lat, lon, vendorConfig.get(BASE_URL) + "air_pollution", vendorConfig.get(SECRET_KEY).toString());
            if (airPollutionResponse.containsKey("list")) {
                List<Map<String, Object>> airPollutionMap = (List<Map<String, Object>>) airPollutionResponse.get("list");
                List<WeatherResponse.Aqi> aqiList = new ArrayList<>();
                airPollutionMap.forEach(a -> {
                    String aqi = "";
                    if (a.containsKey("main")) {
                        Map<String, Object> aqiMap = (Map<String, Object>) a.get("main");
                        aqi = aqiMap.get("aqi").toString();
                    }
                    Map<String, Object> componentMap = new HashMap<>();
                    if (a.containsKey("components")) {
                        componentMap = (Map<String, Object>) a.get("components");
                    }
                    aqiList.add(new WeatherResponse.Aqi(
                            aqi,
                            componentMap.get("co").toString(),
                            componentMap.get("no").toString(),
                            componentMap.get("no2").toString(),
                            componentMap.get("o3").toString(),
                            componentMap.get("so2").toString(),
                            componentMap.get("pm2_5").toString(),
                            componentMap.get("pm10").toString(),
                            componentMap.get("nh3").toString()
                    ));
                });
                weatherResponse.setAqi(aqiList);
            }
        } catch (Exception ignored) {

        }
        return weatherResponse;
    }

    public Map<String, Object> getWeather(Double lat, Double lon, String url, String secretKey) {
        Map<String, Object> body = null;
        int retryCount = 3;
        String endPoint = UriComponentsBuilder.fromUriString(url)
                .queryParam("lat", lat)
                .queryParam("lon", lon)
                .queryParam("appid", secretKey)
                .queryParam("units", "metric")
                .toUriString();
        boolean retry;
        do {
            try {
                ResponseEntity<Map<String, Object>> response = restTemplate.exchange(
                        endPoint,
                        HttpMethod.GET,
                        null,
                        new ParameterizedTypeReference<Map<String, Object>>() {}
                );
                if (response.getStatusCode() == HttpStatus.OK) {
                    body = response.getBody();
                }
                retry = false;
            } catch (HttpClientErrorException ex) {
                retry = true;
                retryCount--;
            }
        } while (retry && retryCount > 0);
        return body;
    }
}
