package com.everwell.transition.service.impl;

import com.everwell.transition.constants.AppointmentConstants;
import com.everwell.transition.constants.Constants;
import com.everwell.transition.constants.FieldConstants;
import com.everwell.transition.enums.user.UserServiceField;
import com.everwell.transition.exceptions.InternalServerErrorException;
import com.everwell.transition.exceptions.NotFoundException;
import com.everwell.transition.model.db.Appointment;
import com.everwell.transition.model.db.AppointmentAnswer;
import com.everwell.transition.model.dto.EpisodeDto;
import com.everwell.transition.model.dto.appointment.AppointmentData;
import com.everwell.transition.model.dto.appointment.AppointmentDetails;
import com.everwell.transition.model.request.appointment.*;
import com.everwell.transition.model.response.appointment.AppointmentStaffMap;
import com.everwell.transition.model.response.appointment.GetAppointmentResponse;
import com.everwell.transition.repositories.AppointmentAnswerRepository;
import com.everwell.transition.repositories.AppointmentRepository;
import com.everwell.transition.service.AppointmentService;
import com.everwell.transition.service.ClientService;
import com.everwell.transition.service.EpisodeService;
import com.everwell.transition.service.RegistryService;
import com.nimbusds.oauth2.sdk.util.CollectionUtils;
import io.micrometer.core.instrument.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class AppointmentServiceImpl implements AppointmentService {

    @Autowired
    private AppointmentRepository appointmentRepository;

    @Autowired
    private AppointmentAnswerRepository appointmentAnswerRepository;

    @Autowired
    private RegistryService registryService;

    @Autowired
    private EpisodeService episodeService;

    @Autowired
    private ClientService clientService;

    Appointment getAppointment(Integer appointmentId) {
        Appointment appointment = appointmentRepository.findByIdAndDeletedAtIsNull(appointmentId);
        if (appointment == null) {
            throw new NotFoundException(AppointmentConstants.APPOINTMENT_NOT_EXIST);
        }
        return appointment;
    }

    Appointment updateAppointmentStatus(Integer appointmentId, String status) {
        Appointment appointmentToUpdate = getAppointment(appointmentId);
        appointmentToUpdate.setStatus(status);
        appointmentRepository.save(appointmentToUpdate);
        return appointmentToUpdate;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public AppointmentData addAppointment(AppointmentData appointmentDetails) {
        Boolean addedByPatient = Constants.PATIENT.toUpperCase().equals(appointmentDetails.getAddedBy());
        Appointment appointmentToSave = new Appointment(appointmentDetails.getAppointment(),
                addedByPatient ? AppointmentConstants.DEFAULT_STATUS : AppointmentConstants.STATUS_CONFIRMED);
        Integer appointmentId = appointmentRepository.save(appointmentToSave).getId();
        appointmentDetails.getAppointment().setId(appointmentId);

        if (!CollectionUtils.isEmpty(appointmentDetails.getQuestionnaire())) {
            List<AppointmentAnswer> answersToSave = new ArrayList<>();
            appointmentDetails.getQuestionnaire().forEach(answer ->
                    answersToSave.add(new AppointmentAnswer(null, appointmentId, answer.getFieldId(), answer.getAnswer()))
            );
            appointmentAnswerRepository.saveAll(answersToSave);
        }

        if (addedByPatient) {
            StaffMapRequest map = new StaffMapRequest(appointmentDetails.getStaffId(), appointmentId);
            AppointmentStaffMap registryResponse = registryService.addAppointmentStaffMap(map);
            if (registryResponse == null || registryResponse.getId() == null) {
                // rolling back by @Transactional
                throw new InternalServerErrorException("Error: Could not save at registry.");
            }
        }

        // TODO: Save documents

        return appointmentDetails;
    }

    @Override
    public GetAppointmentResponse getAppointmentResponse(Integer appointmentId) {
        Appointment appointment = getAppointment(appointmentId);
        EpisodeDto patientDetails = episodeService.getEpisodeDetails(appointment.getEpisodeId(),
                clientService.getClientByTokenOrDefault().getId());

        return new GetAppointmentResponse(createAppointmentDetail(appointment, patientDetails),
                appointmentAnswerRepository.getAllByAppointmentId(appointment.getId()));
    }

    @Override
    public List<AppointmentDetails> getAppointments(AppointmentListRequest appointmentListRequest) {
        List<AppointmentDetails> result = new ArrayList<>();
        List<Appointment> appointments;
        if (appointmentListRequest.getEpisodeId() != null) {
            appointments = appointmentRepository
                    .findAllByEpisodeIdAndDeletedAtIsNull(appointmentListRequest.getEpisodeId());
        } else {
            appointments = appointmentRepository
                    .findAllByIdInAndDeletedAtIsNull(appointmentListRequest.getAppointmentIds());
        }
        appointments = appointments.stream()
                .filter(appointment -> {
                    if (!StringUtils.isEmpty(appointmentListRequest.getStatus())
                            && !Objects.equals(appointment.getStatus(), appointmentListRequest.getStatus())) {
                        return false;
                    }
                    if (appointmentListRequest.getDateRangeStarting() != null
                            && !appointment.getDate().isAfter(appointmentListRequest.getDateRangeStarting())) {
                        return false;
                    }
                    if (appointmentListRequest.getDateRangeEnding() != null
                            && !appointment.getDate().isBefore(appointmentListRequest.getDateRangeEnding())) {
                        return false;
                    }
                    return true;
                })
                .collect(Collectors.toList());
        appointments.sort(Comparator.comparing(Appointment::getDate).reversed());
        for (Appointment appointment : appointments) {
            EpisodeDto patientDetails = episodeService.getEpisodeDetails(appointment.getEpisodeId(),
                    clientService.getClientByTokenOrDefault().getId());
            result.add(createAppointmentDetail(appointment, patientDetails));
        }
        return result;
    }

    @Override
    public AppointmentDetails createAppointmentDetail(Appointment appointment, EpisodeDto episode) {
        Map<String, Object> episodeStageData = episode.getStageData();
        return new AppointmentDetails(
                appointment.getId(),
                appointment.getEpisodeId(),
                (String) episodeStageData.get(FieldConstants.PERSON_FIELD_FIRST_NAME),
                (String) episodeStageData.getOrDefault(FieldConstants.PERSON_FIELD_LAST_NAME, ""),
                (List<String>) episodeStageData.get(UserServiceField.EMAIL_LIST.getFieldName()),
                (String) episodeStageData.get(UserServiceField.GENDER.getFieldName()),
                (Integer) episodeStageData.getOrDefault(UserServiceField.AGE.getFieldName(), null),
                appointment.getDate(),
                appointment.getStatus(),
                appointment.getVisitNumber() > 1 ? AppointmentConstants.TYPE_FOLLOW_UP : AppointmentConstants.TYPE_NEW,
                appointment.getVisitNumber(),
                appointment.getAdded_on()
        );
    }

    @Override
    public Appointment confirmAppointment(Integer appointmentId) {
        return updateAppointmentStatus(appointmentId, AppointmentConstants.STATUS_CONFIRMED);
    }

    @Override
    public String endAppointment(Integer appointmentId) {
        updateAppointmentStatus(appointmentId, AppointmentConstants.STATUS_ENDED);
        return AppointmentConstants.END_SUCCESS;
    }

    @Override
    public Appointment createFollowUpAppointment(FollowUpRequest followUpRequest) {
        Appointment appointment = getAppointment(followUpRequest.getAppointmentId());
        //creating new appointment for follow-up
        Appointment followUpAppointment = new Appointment(
                appointment.getId(), appointment.getEpisodeId(),
                followUpRequest.getFollowUpDate(), appointment.getVisitNumber() + 1,
                AppointmentConstants.STATUS_CONFIRMED
        );
        appointment.setFollowUpDate(followUpRequest.getFollowUpDate());
        appointmentRepository.saveAll(Arrays.asList(appointment, followUpAppointment));
        return followUpAppointment;
    }

    @Override
    public Appointment rescheduleAppointment(RescheduleRequest rescheduleRequest) {
        Appointment appointment = getAppointment(rescheduleRequest.getAppointmentId());
        appointment.setDate(rescheduleRequest.getDate());
        appointmentRepository.save(appointment);
        return appointment;
    }

    @Override
    public void deleteAppointment(DeleteAppointmentRequest deleteRequest) {
        Appointment appointmentToUpdate = getAppointment(deleteRequest.getAppointmentId());
        appointmentToUpdate.setDeletedAt(LocalDateTime.now());
        appointmentToUpdate.setDeletedById(deleteRequest.getDeletedById());
        appointmentRepository.save(appointmentToUpdate);
    }


}
