package com.everwell.transition.service.impl;

import com.everwell.transition.model.db.TBChampionActivity;
import com.everwell.transition.model.dto.ActivityDto;
import com.everwell.transition.model.request.AddActivityRequest;
import com.everwell.transition.model.response.TBChampionActivityResponse;
import com.everwell.transition.repositories.TBChampionActivityRepository;
import com.everwell.transition.service.TBChampionActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TBChampionActivityServiceImpl implements TBChampionActivityService {

    @Autowired
    TBChampionActivityRepository tbChampionActivityRepository;

    @Override
    public TBChampionActivityResponse getActivities(Long episodeId) {
        return new TBChampionActivityResponse(episodeId,tbChampionActivityRepository.getAllByEpisodeIdOrderByCreatedOnDesc(episodeId));
    }

    @Override
    public void saveActivity(AddActivityRequest addActivityRequest) {
        ActivityDto activityDto = addActivityRequest.getActivity();
        TBChampionActivity tbChampionActivity = new TBChampionActivity(activityDto.getDate(), addActivityRequest.getEpisodeId(), activityDto.getPatientVisits(),activityDto.getMeetingCount(),activityDto.getOtherActivity(),activityDto.getUploadedFile());
        tbChampionActivityRepository.save(tbChampionActivity);
    }
}
