package com.everwell.transition.service;

import com.everwell.transition.elasticsearch.indices.EpisodeIndex;
import com.everwell.transition.model.dto.EpisodeDto;
import com.everwell.transition.model.dto.dispensation.DispensationData;
import com.everwell.transition.model.dto.dispensation.ProductStockData;
import com.everwell.transition.model.dto.treatmentPlan.TreatmentPlanDto;
import com.everwell.transition.model.request.dispensation.*;
import com.everwell.transition.model.response.ProductResponseWithScheduleDto;
import com.everwell.transition.model.response.dispensation.*;
import com.everwell.transition.model.response.Response;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Map;

public interface DispensationService {

    ResponseEntity<Response<List<ProductResponse>>> searchProducts(ProductSearchRequest productSearchRequest);

    ResponseEntity<Response<List<ProductStockData>>> productStockSearch(ProductStockSearchRequest productStockSearchRequest);

    ResponseEntity<Response<EntityDispensationResponse>> getAllDispensationDataForEntity(Long entityId, boolean includeProductLog);

    ResponseEntity<Response<DispensationData>> getDispensation(Long id);

    ResponseEntity<Response<DispensationResponse>> createDispensation(DispensationRequest dispensationRequest);

    ResponseEntity<Response<DispensationResponse>> createDispensationInventory(DispensationRequest dispensationRequest);

    ResponseEntity<Response<DispensationResponse>> deleteDispensation(Long id);

    ResponseEntity<Response<List<EntityDispensationResponse>>> searchDispensationsForEntity(SearchDispensationRequest searchDispensationRequest);

    ResponseEntity<Response<ReturnDispensationResponse>> returnDispensation(ReturnDispensationRequest returnDispensationRequest);

    ResponseEntity<Response<ReturnDispensationResponse>> returnDispensationInventory(ReturnDispensationRequest returnDispensationRequest);

    ResponseEntity<Response<ProductResponse>> addProduct(ProductRequest productRequest);

    ResponseEntity<Response<List<ProductResponse>>> addProductBulk(ProductBulkRequest productBulkRequest);

    ResponseEntity<Response<ProductResponse>> getProduct(Long id);

    ResponseEntity<Response<Void>> stockCredit(StockRequest stockRequest);

    ResponseEntity<Response<Void>> stockDebit(StockRequest stockRequest);

    ResponseEntity<Response<DispensationResponse>> createDispensationEpisode(DispensationRequest dispensationRequest, EpisodeDto episodeDto);

    ResponseEntity<Response<EntityDispensationResponse>> getEpisodeDispensationData(Long dispensationId);

    DispensationWeightBandResponse getWeightBandForEpisode(Long episodeId);

    Map<Long, String> getEpisodeIdsWithRefillDueDate(RefillDatesRequest refillDatesRequest);

    ResponseEntity<Response<Void>> stockUpdate(Map<String,Object> stockRequest);

    List<ProductResponseWithScheduleDto> getProductWithSchedulesFromTreatmentPlan(TreatmentPlanDto treatmentDetails);

}
