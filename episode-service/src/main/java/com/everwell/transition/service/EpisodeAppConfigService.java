package com.everwell.transition.service;

import com.everwell.transition.model.response.EpisodeAppConfigResponse;
import com.everwell.transition.model.response.EpisodeLogResponse;
import org.springframework.stereotype.Service;

@Service
public interface EpisodeAppConfigService {

    EpisodeAppConfigResponse getEpisodeAppConfig(Long episodeId,String technology, String Deployment);
}
