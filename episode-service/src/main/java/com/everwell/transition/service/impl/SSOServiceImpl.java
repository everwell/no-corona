package com.everwell.transition.service.impl;

import com.everwell.transition.constants.Constants;
import com.everwell.transition.constants.SSOConstants;
import com.everwell.transition.enums.SSO.SSOEndpoint;
import com.everwell.transition.model.request.SSO.*;
import com.everwell.transition.model.response.Response;
import com.everwell.transition.model.response.SSO.DeviceIdsMapResponse;
import com.everwell.transition.service.SSOService;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class SSOServiceImpl extends DataGatewayHelperServiceImpl implements SSOService {

    @Override
    public ResponseEntity<Response<Map<String, Object>>> login(Map<String, Object> loginRequest, Long PlatformId) {
        String endpoint = SSOEndpoint.LOGIN.getEndpointUrl();
        Map<String,String> headers = new HashMap<>();
        headers.put(Constants.X_PLATFORM_ID, PlatformId.toString());
        return postExchange(endpoint, loginRequest, new ParameterizedTypeReference<Response<Map<String, Object>>>() {}, headers);
    }

    public Boolean registration(RegistrationRequest registrationRequest){
        String endpoint = SSOEndpoint.REGISTRATION.getEndpointUrl();
        ResponseEntity<Response<Void>> response = postExchange(endpoint, registrationRequest, new ParameterizedTypeReference<Response<Void>>() {});
        return response.getBody().isSuccess();
    }

    public ResponseEntity<Response<Void>> logout(Map<String, Object> logoutRequest){
        String endpoint = SSOEndpoint.LOGOUT.getEndpointUrl();
        return postExchange(endpoint, logoutRequest, new ParameterizedTypeReference<Response<Void>>() {});
    }

    public ResponseEntity<Response<Void>> refreshDeviceIdAndLastActivityDate(Map<String, Object> updateDeviceIdRequest) {
        String endpoint = SSOEndpoint.UPDATE_DEVICE_ID.getEndpointUrl();
        return putExchange(endpoint, updateDeviceIdRequest, new ParameterizedTypeReference<Response<Void>>() {});
    }

    public ResponseEntity<Response<DeviceIdsMapResponse>> fetchDeviceIdUserMap(List<String> userNames) {
        String endpoint = SSOEndpoint.GET_DEVICE_ID.getEndpointUrl();
        String url = "?";
        for(String params : userNames){
            url += "userNames" + "=" + params + "&";
        }
        url = url.substring(0,url.length()-1);
        endpoint += url;
        return getExchange(endpoint, new ParameterizedTypeReference<Response<DeviceIdsMapResponse>>() {}, null, null);
    }
}
