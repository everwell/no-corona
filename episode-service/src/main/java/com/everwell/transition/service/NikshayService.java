package com.everwell.transition.service;

import com.everwell.transition.model.dto.notification.EpisodeListDto;
import com.everwell.transition.model.response.Response;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface NikshayService {

    List<Long> getEpisodesWithContactTracingFilter(List<Long> episodeIds);
}
