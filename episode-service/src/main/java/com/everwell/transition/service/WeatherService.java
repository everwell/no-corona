package com.everwell.transition.service;

import com.everwell.transition.model.response.WeatherResponse;

import java.util.Map;

public interface WeatherService {
    public WeatherResponse getCompleteWeather(Double lat, Double lon, Long clientId);
}
