package com.everwell.transition.service;

import com.everwell.transition.model.Rule;
import com.everwell.transition.parser.RuleParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public abstract class InferenceEngine <I, O> {

    @Autowired
    private RuleParser<I, O> ruleParser;

    O run(List<Rule> listOfRule, I conditionInputData, Object inputDataAction) {
        List<Rule> conflictSet = match(listOfRule, conditionInputData);
        Rule resolvedRule = resolve(conflictSet);
        if (null == resolvedRule){
            return null;
        }
        return executeRule(resolvedRule, inputDataAction);
    }

    private List<Rule> match(List<Rule> listOfRule, I conditionInputData){
        return
            listOfRule
                .stream()
                .filter(rule -> ruleParser.parseCondition(rule.getCondition(), conditionInputData))
                .collect(Collectors.toList());
    }

    private Rule resolve(List<Rule> conflictSet){
        Optional<Rule> rule = conflictSet.stream().sorted(Comparator.comparing(Rule::getPriority)).findFirst();
        return rule.orElse(null);
    }

    private O executeRule(Rule rule, Object inputDataAction){
        O outputResult = initializeOutputResult();
        return ruleParser.parseAction(rule.getAction(), outputResult, inputDataAction);
    }

    public abstract I resolveMissingData(I input, List<Rule> rules);

    protected abstract List<String> getRequiredFields(Rule rules);

    protected abstract O initializeOutputResult();
    protected abstract List<Rule> getRules(Long fromId, boolean shouldEvaluateAll, Long clientId);
    protected abstract String getRuleNamespace();
}
