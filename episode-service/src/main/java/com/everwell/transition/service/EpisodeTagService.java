package com.everwell.transition.service;

import com.everwell.transition.model.db.EpisodeTag;
import com.everwell.transition.model.request.episodetags.EpisodeTagBulkDeletionRequest;
import com.everwell.transition.model.request.episodetags.EpisodeTagDataRequest;
import com.everwell.transition.model.request.episodetags.EpisodeTagDeletionRequest;
import com.everwell.transition.model.request.episodetags.SearchTagsRequest;
import com.everwell.transition.model.response.EpisodeTagResponse;
import com.everwell.transition.model.response.TagsConfigResponse;

import java.util.List;

public interface EpisodeTagService {

    EpisodeTagResponse getEpisodeTags(Long episodeId);
    List<EpisodeTagResponse> save(List<EpisodeTagDataRequest> episodeTagRequest);
    List<EpisodeTagResponse> search(SearchTagsRequest searchTagsRequest);
    void delete(EpisodeTagDeletionRequest episodeTagDeletionRequest);
    void deleteBulk(EpisodeTagBulkDeletionRequest episodeTagBulkDeletionRequest);
    List<TagsConfigResponse> getEpisodeConfig();
    List<EpisodeTagResponse> groupTags(List<EpisodeTag> tagList);
    void deleteAllTagsAfterEndDate(Long episodeId, String endDate);

}
