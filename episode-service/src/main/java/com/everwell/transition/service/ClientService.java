package com.everwell.transition.service;

import com.everwell.transition.model.db.Client;
import com.everwell.transition.model.request.RegisterClientRequest;
import com.everwell.transition.model.response.ClientResponse;
import org.springframework.stereotype.Service;

@Service
public interface ClientService {

    ClientResponse registerClient(RegisterClientRequest clientRequest);

    ClientResponse getClientWithToken(Long id);

    Client getClient(Long id);

    Client getClientByTokenOrDefault();
}