package com.everwell.transition.service.impl;

import com.everwell.transition.constants.Constants;
import com.everwell.transition.model.db.Client;
import com.everwell.transition.model.dto.EpisodeDto;
import com.everwell.transition.model.request.diagnostics.TestRequestV2;
import com.everwell.transition.constants.DiagnosticsConstants;
import com.everwell.transition.constants.FieldConstants;
import com.everwell.transition.constants.NotificationConstants;
import com.everwell.transition.constants.diagnostics.RequestConstants;
import com.everwell.transition.constants.diagnostics.ResponseConstants;
import com.everwell.transition.enums.diagnostics.DiagnosticsEndpoint;
import com.everwell.transition.exceptions.NotFoundException;
import com.everwell.transition.exceptions.ValidationException;
import com.everwell.transition.enums.diagnostics.DiagnosticsField;
import com.everwell.transition.enums.user.UserServiceField;
import com.everwell.transition.model.dto.EpisodeDto;
import com.everwell.transition.model.request.diagnostics.GetSampleIdListRequest;
import com.everwell.transition.enums.diagnostics.DiagnosticsEndpoints;
import com.everwell.transition.exceptions.ValidationException;
import com.everwell.transition.model.dto.EpisodeDto;
import com.everwell.transition.model.request.diagnostics.DiagnosticsFilterDataRequest;
import com.everwell.transition.model.response.Response;
import com.everwell.transition.model.response.diagnostics.DiagnosticsFilterDataResponse;
import com.everwell.transition.model.response.diagnostics.AddTestResponseV2;
import com.everwell.transition.service.DiagnosticsService;
import com.everwell.transition.service.EpisodeService;
import com.everwell.transition.model.response.diagnostics.EpisodeTestsResponse;
import com.everwell.transition.model.response.diagnostics.TestResponse;
import com.everwell.transition.enums.diagnostics.DiagnosticsEndpoints;
import com.everwell.transition.service.ClientService;
import com.everwell.transition.service.DiagnosticsService;
import com.everwell.transition.service.EpisodeService;
import com.everwell.transition.utils.Utils;
import com.everwell.transition.exceptions.ValidationException;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;
import java.util.*;

@Service
public class DiagnosticsServiceImpl extends DataGatewayHelperServiceImpl implements DiagnosticsService {

    private static Logger LOGGER = LoggerFactory.getLogger(DiagnosticsServiceImpl.class);
    @Autowired
    EpisodeService episodeService;

    @Autowired
    private ClientService clientService;


    @Override
    public ResponseEntity<Response<String>> testConnection() {
        String endPoint = DiagnosticsEndpoints.TEST_CONNECTION.getEndpointUrl();
        return
                getExchange(endPoint,
                        new ParameterizedTypeReference<Response<String>>() {
                        },
                        null,
                        new HashMap<>()
                );
    }

    @Override
    public ResponseEntity<Response<AddTestResponseV2>> addTest(TestRequestV2 genericTestRequest, Long clientId) {
        EpisodeDto episode = episodeService.getEpisodeDetails(Long.valueOf(genericTestRequest.getEntityId()), clientId, false,false,false,false);
        LOGGER.info("[addTest] Episode Stage: " + episode.getStageKey());
        LOGGER.info("[addTest] Is Episode Enabled for current episode: " + episode.getStageData().getOrDefault(FieldConstants.IS_EPISODE_ENABLED,false));
        if (genericTestRequest.getStagesToExclude().contains(episode.getStageKey())) {
            throw new ValidationException(String.format("This is a %s case. Please re-open the case to add test or add a new episode and proceed with adding the test", episode.getStageKey()));
        }
        String endPoint = DiagnosticsEndpoints.ADD_TEST.getEndpointUrl();
        ResponseEntity<Response<AddTestResponseV2>> response = postExchange(endPoint, genericTestRequest, new ParameterizedTypeReference<Response<AddTestResponseV2>>() {
        });

        AddTestResponseV2 addTestResponseV2 = Utils.processResponseEntity(response, ResponseConstants.ERROR_MESSAGE_FOR_ADD_TEST);
        LOGGER.info("[addTest] Diagnostics AddTestResponseV2: " + addTestResponseV2);
        if(DiagnosticsConstants.RESULT_AVAILABLE.equals(addTestResponseV2.getStatus()) &&
                Boolean.parseBoolean(String.valueOf(episode.getStageData().getOrDefault(FieldConstants.IS_EPISODE_ENABLED,false)))
            && genericTestRequest.isPositive()
            && genericTestRequest.getReason().startsWith(Constants.DIAGNOSIS)) {
            Map<String, Object> fieldObject = Utils.objectToMap(addTestResponseV2);
            fieldObject.put(FieldConstants.EPISODE_FIELD_EPISODE_ID, NumberUtils.toLong(genericTestRequest.getEntityId()));
            fieldObject.put(FieldConstants.EPISODE_FIELD_DIAGNOSIS_BASIS, genericTestRequest.getType());
            fieldObject.put(FieldConstants.EPISODE_FIELD_DIAGNOSIS_DATE, genericTestRequest.getDateReported());
            if(genericTestRequest.getPreviousATT().equals(Constants.YES))
                fieldObject.put(FieldConstants.EPISODE_FIELD_TYPE_OF_CASE, Constants.RETREATMENT_OTHERS);
            else
                fieldObject.put(FieldConstants.EPISODE_FIELD_TYPE_OF_CASE, Constants.NEW);
            fieldObject.put(FieldConstants.EPISODE_FIELD_DRUG_RESISTANCE, Constants.UNKNOWN);
            if(genericTestRequest.getTestingFacilityHierarchyId()>0)
                fieldObject.put(FieldConstants.EPISODE_FIELD_DIAGNOSED, genericTestRequest.getTestingFacilityHierarchyId());
            else
                fieldObject.put(FieldConstants.EPISODE_FIELD_DIAGNOSED, episode.getHierarchyMappings().getOrDefault(FieldConstants.FIELD_CURRENT, null));
            try {
                episodeService.processEpisodeRequestInputForStageTransition(clientId, fieldObject, false);
            } catch (Exception ignored){}
        }
        return response;
    }
    
    @Override
    public EpisodeTestsResponse getTestsByEpisode(Long episodeId) {
        EpisodeDto episodeDto;
        try {
            episodeDto = episodeService.getEpisodeDetails(episodeId, clientService.getClientByTokenOrDefault().getId(), false, false, false, false);
        } catch (NotFoundException e) {
            throw new NotFoundException("Invalid Patient Id");
        }
        String endpoint = DiagnosticsEndpoint.GET_TESTS_BY_ENTITY.getEndpointUrl();
        Map<String, Object> queryParams = new HashMap<>();
        queryParams.put(DiagnosticsField.ENTITY_ID.getFieldName(), episodeId);
        ResponseEntity<Response<List<Map<String, Object>>>> diagnosticTestResponse = getExchange(endpoint, new ParameterizedTypeReference<Response<List<Map<String, Object>>>>() {}, null, queryParams);
        List<Map<String,Object>> testData = Utils.processResponseEntity(diagnosticTestResponse, "Unable to fetch tests for episode " + episodeId);
        Map<String, Object> episodeStageData = episodeDto.getStageData();
        Integer age = null == episodeStageData.get(UserServiceField.AGE.getFieldName()) ? null : Integer.parseInt(String.valueOf(episodeStageData.get(UserServiceField.AGE.getFieldName())));
        return new EpisodeTestsResponse(testData, Utils.convertObjectToStringWithNull(episodeStageData.getOrDefault(FieldConstants.PERSON_FIELD_NAME, null)), age, Utils.convertObjectToStringWithNull(episodeStageData.getOrDefault(UserServiceField.PRIMARY_PHONE.getFieldName(), null)), Utils.convertObjectToStringWithNull(episodeStageData.getOrDefault(UserServiceField.GENDER.getFieldName(), null)));
    }

    @Override
    public ResponseEntity<Response<Map<String, Object>>> filterKPI(@RequestBody Map<String, Object> kpiFilterRequest) {
        String endPoint = DiagnosticsEndpoints.KPI.getEndpointUrl();
        return
                postExchange(endPoint,
                        kpiFilterRequest,
                        new ParameterizedTypeReference<Response<Map<String, Object>>>() {
                        }
                );
    }

    @Override
    public ResponseEntity<Response<Map<String, Object>>> qrCodeValidation(String qrCode) {
        String endPoint = DiagnosticsEndpoints.QR_VALIDATION.getEndpointUrl();
        Map<String, Object> queryParams = new HashMap<>();
        queryParams.put("qrCode", qrCode);
        return
                getExchange(endPoint,
                        new ParameterizedTypeReference<Response<Map<String, Object>>>() {
                        },
                        null,
                        queryParams
                );
    }

    @Override
    public ResponseEntity<Response<Map<String, Object>>> generateQrPdf(@RequestBody Map<String, Object> request) {
        String endPoint = DiagnosticsEndpoints.GENERATE_QR_PDF.getEndpointUrl();
        return
                postExchange(endPoint,
                        request,
                        new ParameterizedTypeReference<Response<Map<String, Object>>>() {}
                );
    }
    
    @Override
    public ResponseEntity<Response<Map<String, Object>>> getSampleByIdOrQRCode(String by, Long sampleId, String qrCode, Boolean journeyDetails) {
        String endPoint = DiagnosticsEndpoints.SAMPLE_BY_ID_OR_QRCODE.getEndpointUrl();
        Map<String, Object> queryParams = new HashMap<>();
        queryParams.put(RequestConstants.QUERY_PARAM_BY, by);
        queryParams.put(RequestConstants.QUERY_PARAM_SAMPLE_ID, sampleId);
        queryParams.put(RequestConstants.QUERY_PARAM_QR_CODE, qrCode);
        queryParams.put(RequestConstants.QUERY_PARAM_JOURNEY_DETAILS, journeyDetails);
        ResponseEntity<Response<Map<String, Object>>> response =
                getExchange(endPoint,
                        new ParameterizedTypeReference<Response<Map<String, Object>>>() {
                        },
                        null,
                        queryParams
                );
        if(journeyDetails) { 
            //diagnostics does not send entityId if journeyDetails = false
            //patientName is not required when journeyDetails = false
            Map<String, Object> formattedResponse = Utils.processResponseEntity(response, ResponseConstants.ERROR_MESSAGE_FOR_GET_SAMPLE);
            Long entityId = Long.valueOf(String.valueOf(formattedResponse.get(ResponseConstants.ENTITY_ID)));
            EpisodeDto patientDetails = episodeService.getEpisodeDetails(entityId, clientService.getClientByTokenOrDefault().getId(), false, false, false, false);
            Map<String, Object> episodeStageData = patientDetails.getStageData();
            String patientName = episodeStageData.get(FieldConstants.PERSON_FIELD_FIRST_NAME) + " " + episodeStageData.getOrDefault(FieldConstants.PERSON_FIELD_LAST_NAME, "");
            response.getBody().getData().put(ResponseConstants.PATIENT_NAME, patientName);
        }
        return response;
    }

    @Override
    public ResponseEntity<Response<DiagnosticsFilterDataResponse>> filterTest(DiagnosticsFilterDataRequest diagnosticsFilterDataRequest) {
        String endPoint = DiagnosticsEndpoints.FILTER.getEndpointUrl();
        return
                postExchange(endPoint,
                        diagnosticsFilterDataRequest,
                        new ParameterizedTypeReference<Response<DiagnosticsFilterDataResponse>>() {}
                );
    }

    @Override
    public List<Long> episodesWithTbTest(List<Long> episodeIds) {
        List<List<Long>> episodeIdBatches = Utils.getBatches(episodeIds, 500);
        List<CompletableFuture<ResponseEntity<Response<DiagnosticsFilterDataResponse>>>> allFutures = new ArrayList<>();
        Map<String, Object> filters = new HashMap<>();
        filters.put(RequestConstants.FILTER_PARAM_POSITIVE, true);
        episodeIdBatches.forEach(e -> {
            DiagnosticsFilterDataRequest diagnosticsFilterDataRequest = new DiagnosticsFilterDataRequest(filters, e, new ArrayList<>());
            allFutures.add(
                    CompletableFuture.supplyAsync(() -> filterTest(diagnosticsFilterDataRequest))
            );
        });
        CompletableFuture.allOf(allFutures.toArray(new CompletableFuture[0])).join();
        List<DiagnosticsFilterDataResponse.FilterResponse> filterTestList = new ArrayList<>();
        for (int i = 0; i < episodeIdBatches.size(); i++) {
            try {
                ResponseEntity<Response<DiagnosticsFilterDataResponse>> partialResponse = allFutures.get(i).get();
                if (partialResponse.getBody() != null && partialResponse.getBody().isSuccess()) {
                    filterTestList.addAll(partialResponse.getBody().getData().getTestList());
                }
            } catch (Exception e) {
                throw new ValidationException(e.toString());
            }
        }
        List<Long> episodeIdsWithTbTest = filterTestList.stream().filter(t -> StringUtils.hasLength(t.getSubReason()) && t.getSubReason().toLowerCase().contains(NotificationConstants.TESTED_FOR_TB_SUBREASON)).map(DiagnosticsFilterDataResponse.FilterResponse::getPatientId).distinct().collect(Collectors.toList());
        return episodeIdsWithTbTest;
    }

    @Override
    public List<Long> getSampleIdListByEpisodeId(GetSampleIdListRequest sampleIdListRequest) {
        EpisodeDto episodeDto = episodeService.getEpisodeDetails(sampleIdListRequest.getEpisodeId(), clientService.getClientByTokenOrDefault().getId(), false, false, false, false);
        if (sampleIdListRequest.getStagesToExclude().contains(episodeDto.getStageName())) {
            throw new ValidationException("This is a Presumptive Closed case. Please re-open the case to add test or add a new episode and proceed with adding the test");
        }
        String endpoint = DiagnosticsEndpoint.GET_SAMPLE_ID_LIST_BY_ENTITY.getEndpointUrl();
        Map<String, Object> queryParams = new HashMap<>();
        queryParams.put(DiagnosticsField.ENTITY_ID.getFieldName(), sampleIdListRequest.getEpisodeId());
        ResponseEntity<Response<List<Long>>> response = getExchange(endpoint, new ParameterizedTypeReference<Response<List<Long>>>() {}, null, queryParams);
        return Utils.processResponseEntity(response, "Unable to fetch sample id from diagnostics for episode " + sampleIdListRequest.getEpisodeId());
    }

    @Override
    public TestResponse getTestById(Long testRequestId) {
        String endpoint = DiagnosticsEndpoint.GET_TEST_BY_ID.getEndpointUrl();
        Map<String, Object> queryParams = new HashMap<>();
        queryParams.put(DiagnosticsConstants.FIELD_TEST_REQUEST_ID, testRequestId);
        ResponseEntity<Response<Map<String, Object>>> testResponse = getExchange(endpoint, new ParameterizedTypeReference<Response<Map<String, Object>>>() {}, null, queryParams);
        Map<String, Object> test = Utils.processResponseEntity(testResponse, "Unable to fetch test for id " + testRequestId);
        Long episodeId = Long.parseLong(String.valueOf(test.get(DiagnosticsConstants.FIELD_EPISODE_ID)));
        EpisodeDto episodeDto = episodeService.getEpisodeDetails(episodeId, clientService.getClientByTokenOrDefault().getId(), false, false, false, false);
        return new TestResponse(test, episodeDto);
    }

    @Override
    public Map<String, Object> editTest(Map<String, Object> requestInput) {
        String episodeIdInput = String.valueOf(requestInput.getOrDefault(DiagnosticsConstants.FIELD_EPISODE_ID, null));
        if ("null".equals(episodeIdInput)) {
            throw new ValidationException("episode id cannot be null");
        }
        Long episodeId = Long.parseLong(episodeIdInput);
        EpisodeDto episodeDto = episodeService.getEpisodeDetails(episodeId, clientService.getClientByTokenOrDefault().getId(), false, false, false, false);
        List<String> nonEditableStages = (List<String>) requestInput.getOrDefault(DiagnosticsConstants.FIELD_NON_EDITABLE_STAGES, new ArrayList<String>());
        String stage = (String) episodeDto.getStageData().get(FieldConstants.EPISODE_FIELD_STAGE_KEY);
        if (nonEditableStages.contains(stage)) {
            throw new ValidationException(String.format(DiagnosticsConstants.INVALID_STAGE_EDIT, stage));
        }
        String endpoint = DiagnosticsEndpoint.EDIT_TEST.getEndpointUrl();
        ResponseEntity<Response<Map<String, Object>>> response = putExchange(endpoint, requestInput, new ParameterizedTypeReference<Response<Map<String, Object>>>() {});
        Map<String, Object> responseMap = Utils.processResponseEntity(response, "Unable to edit test for testRequestId " + requestInput.get(DiagnosticsConstants.FIELD_TEST_REQUEST_ID));
        LOGGER.info("[editTest] Diagnostics Response Received: " + responseMap.toString());
        if(DiagnosticsConstants.RESULT_AVAILABLE.equals(responseMap.getOrDefault(DiagnosticsConstants.FIELD_STATUS, null)) && Boolean.parseBoolean(String.valueOf(episodeDto.getStageData().getOrDefault(FieldConstants.IS_EPISODE_ENABLED,false)))) {
            Client client = clientService.getClientByTokenOrDefault();
            responseMap.put(FieldConstants.EPISODE_FIELD_EPISODE_ID, episodeId);
            try {
                episodeService.processEpisodeRequestInputForStageTransition(client.getId(), responseMap, false);
            } catch (Exception ignored){}
        }
        return responseMap;
    }

    @Override
    public String deleteTest(Long testRequestId) {
        String endpoint = DiagnosticsEndpoint.DELETE_TEST.getEndpointUrl();
        Map<String, Object> queryParams = new HashMap<>();
        queryParams.put(DiagnosticsConstants.FIELD_TEST_REQUEST_ID, testRequestId);
        ResponseEntity<Response<String>> response = deleteExchange(endpoint, new ParameterizedTypeReference<Response<String>>() {}, null, queryParams, null);
        return Utils.processResponseEntity(response, "Unable to delete test for testRequestId " + testRequestId);
    }
}
