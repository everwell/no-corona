package com.everwell.transition.service;

import com.everwell.transition.model.request.aers.AdverseReactionRequest;
import com.everwell.transition.model.request.aers.CausalityManagementRequest;
import com.everwell.transition.model.request.aers.OutcomeRequest;
import com.everwell.transition.model.response.AdverseReactionResponse;
import com.everwell.transition.model.response.CausalityManagementResponse;
import com.everwell.transition.model.response.FormConfigResponse;
import com.everwell.transition.model.response.OutcomeResponse;

import java.io.IOException;
import java.util.List;

public interface AERSService {

    void addAdverseReaction(AdverseReactionRequest adverseReactionRequest);
    void updateAdverseReaction(AdverseReactionRequest adverseReactionRequest);
    void addCausalityManagement(CausalityManagementRequest causalityManagementRequest);
    void updateCausalityManagement(CausalityManagementRequest causalityManagementRequest);
    void addOutcome(OutcomeRequest outcomeRequest);
    void updateOutcome(OutcomeRequest outcomeRequest);
    AdverseReactionResponse getAdverseReaction(Long adverseEventId) throws IOException;
    CausalityManagementResponse getCausalityManagement(Long adverseEventId) throws IOException;
    OutcomeResponse getOutcome(Long adverseEventId);
    List<FormConfigResponse> getStatus(Long episodeId);

}
