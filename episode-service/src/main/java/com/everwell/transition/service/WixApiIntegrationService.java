package com.everwell.transition.service;

import com.everwell.transition.model.dto.wix.WixPostFilter;
import com.everwell.transition.model.request.wix.WixPostResponse;

import java.util.List;
import java.util.Map;

public interface WixApiIntegrationService {
    void setAuthToken();

    void refreshAuthToken();

    Map<String, WixPostResponse> listCategoriesFilter(Map<String, String> filters);

    List<WixPostResponse> listPosts(WixPostFilter wixPostFilter);

}
