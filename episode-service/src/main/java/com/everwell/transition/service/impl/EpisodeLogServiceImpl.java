package com.everwell.transition.service.impl;

import com.everwell.transition.constants.Constants;
import com.everwell.transition.enums.AdherenceCodeEnum;
import com.everwell.transition.model.db.EpisodeLog;
import com.everwell.transition.model.db.EpisodeLogStore;
import com.everwell.transition.model.request.adherence.EditDosesRequest;
import com.everwell.transition.model.request.episode.DeleteRequest;
import com.everwell.transition.model.request.episodelogs.EpisodeLogDataRequest;
import com.everwell.transition.model.request.episodelogs.EpisodeLogDeletionRequest;
import com.everwell.transition.model.request.episodelogs.SearchLogsRequest;
import com.everwell.transition.model.request.episodelogs.*;
import com.everwell.transition.model.response.EpisodeLogResponse;
import com.everwell.transition.model.response.LogsConfigResponse;
import com.everwell.transition.postconstruct.EpisodeLogStoreCreation;
import com.everwell.transition.repositories.EpisodeLogRepository;
import com.everwell.transition.service.EpisodeLogService;
import com.everwell.transition.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class EpisodeLogServiceImpl implements EpisodeLogService {

    @Autowired
    private EpisodeLogRepository episodeLogRepository;

    @Autowired
    private EpisodeLogStoreCreation episodeLogStoreCreation;

    @Override
    public EpisodeLogResponse getEpisodeLogs(Long episodeId) {
        return new EpisodeLogResponse(episodeId, episodeLogRepository.getAllByEpisodeIdOrderByAddedOnDesc(episodeId));
    }

    @Override
    public List<EpisodeLogResponse> save(List<EpisodeLogDataRequest> episodeLogRequest) {
        List<EpisodeLog> episodeLogs = episodeLogRequest
            .stream()
            .map(e->  new EpisodeLog(e.getEpisodeId(), e.getAddedBy(), e.getCategory(), e.getSubCategory(), e.getActionTaken(), e.getComments(), e.getDateOfAction()))
            .collect(Collectors.toList());
        return groupLogs(episodeLogRepository.saveAll(episodeLogs));
    }

    @Override
    public List<EpisodeLogResponse> search(SearchLogsRequest searchLogsRequest) {
        List<EpisodeLogResponse> episodeLogResponse;
        if (CollectionUtils.isEmpty(searchLogsRequest.getCategoriesToFilter())) {
            if(searchLogsRequest.getAddedBy() == null)
                episodeLogResponse = groupLogs(episodeLogRepository.getAllByEpisodeIdInOrderByAddedOnDesc(searchLogsRequest.getEpisodeIdList()));
            else
                episodeLogResponse = groupLogs(episodeLogRepository.getAllByEpisodeIdInAndAddedByOrderByAddedOnDesc(searchLogsRequest.getEpisodeIdList(), searchLogsRequest.getAddedBy()));
        } else {
            if (searchLogsRequest.getAddedBy() == null)
                episodeLogResponse = groupLogs(episodeLogRepository.getAllByEpisodeIdInAndCategoryInOrderByAddedOnDesc(searchLogsRequest.getEpisodeIdList(),searchLogsRequest.getCategoriesToFilter()));
            else
                episodeLogResponse = groupLogs(episodeLogRepository.getAllByEpisodeIdInAndCategoryInAndAddedByOrderByAddedOnDesc(searchLogsRequest.getEpisodeIdList(), searchLogsRequest.getCategoriesToFilter(), searchLogsRequest.getAddedBy()));
        }
        return episodeLogResponse;
    }

    @Override
    public void delete(EpisodeLogDeletionRequest episodeLogDeletionRequest) {
        if (episodeLogDeletionRequest.getDeleteAllAfter() == null) {
            episodeLogRepository.deleteLogs(episodeLogDeletionRequest.getEpisodeId(), episodeLogDeletionRequest.getCategories());
        } else {
            episodeLogRepository.deleteLogsAfterDate(episodeLogDeletionRequest.getEpisodeId(), episodeLogDeletionRequest.getCategories(), Utils.toLocalDateTimeWithNull(episodeLogDeletionRequest.getDeleteAllAfter()));
        }
    }

    @Override
    public List<LogsConfigResponse> getEpisodeLogsConfig() {
        List<EpisodeLogStore> config = new ArrayList<>(episodeLogStoreCreation.getEpisodeLogStoreMap().values());
        List<LogsConfigResponse> configResponse = new ArrayList<>();
        config.forEach(s -> configResponse.add(new LogsConfigResponse(s.getCategory(), s.getCategoryGroup())));
        return configResponse;
    }

    @Override
    public List<EpisodeLogResponse> groupLogs(List<EpisodeLog> logList) {
        Map<Long, List<EpisodeLog>> map = new HashMap<>();
        logList.forEach(t -> {
            map.putIfAbsent(t.getEpisodeId(), new ArrayList<>());
            map.get(t.getEpisodeId()).add(t);
        });
        List<EpisodeLogResponse> list = new ArrayList<>();
        map.forEach((k, v) -> list.add(new EpisodeLogResponse(k, v)));
        return list;
    }

    public Pair<String, List<LocalDate>> getActionTakenWithDatesFromLog(String actionTaken, LocalDateTime endDateTime)
    {
        List<LocalDate> logList = new ArrayList<>();
        String[] date = actionTaken.split(" ");
        int dateIndex = date[0].equals(Constants.PATIENT) ? 5 : 4;
        String actionTakenText = Arrays.stream(date, 0, dateIndex).collect(Collectors.joining(" "));
        while (dateIndex < date.length)
        {
            String getDate = date[dateIndex];
            if (getDate.contains(","))
            {
                getDate = getDate.replaceAll(",", "");
            }
            LocalDate logDate = Utils.toLocalDate(getDate, "d/M/yyyy");
            if (logDate.isBefore(endDateTime.toLocalDate()))
                logList.add(logDate);
            dateIndex++;
        }
        return Pair.of(actionTakenText, logList);
    }

    @Override
    public void syncLogs(Long episodeId, String endDate) {
        LocalDateTime endDateTime = Utils.toLocalDateTimeWithNull(endDate);
        List<String> categories = episodeLogStoreCreation.getEpisodeLogStoreMap().values().stream().filter(e -> e.getCategoryGroup().equals(Constants.ADHERENCE_CHANGES_LOG)).map(EpisodeLogStore::getCategory).collect(Collectors.toList());
        List<EpisodeLog> switchLogsAfterEndDate = episodeLogRepository.getAllByEpisodeIdAndCategoryInAndAddedOnAfterOrderByAddedOnAsc(episodeId, categories, endDateTime);
        if (!CollectionUtils.isEmpty(switchLogsAfterEndDate)) {
            LocalDateTime firstSwitchDateAfterEndDate = switchLogsAfterEndDate.get(0).getAddedOn();
            categories.clear();
            categories = episodeLogStoreCreation.getEpisodeLogStoreMap().values().stream().filter(e -> e.getCategoryGroup().equals(Constants.DOSAGE_CHANGES_LOG)).map(EpisodeLogStore::getCategory).collect(Collectors.toList());
            List<EpisodeLog> logsToUpdate = episodeLogRepository.getAllByEpisodeIdAndCategoryInAndAddedOnAfterOrderByAddedOnAsc(episodeId, categories, firstSwitchDateAfterEndDate);
            List<EpisodeLog> logsToDelete = new ArrayList<>();
            logsToUpdate.forEach(f -> {
                Pair<String, List<LocalDate>> actionTakenWithDates = getActionTakenWithDatesFromLog(f.getActionTaken(), endDateTime);
                if (actionTakenWithDates.getSecond().size() > 0) {
                    List<String> date = actionTakenWithDates.getSecond().stream().map(d -> d.format(DateTimeFormatter.ofPattern("d/M/yyyy"))).collect(Collectors.toList());
                    f.setActionTaken(actionTakenWithDates.getFirst() + " " + String.join(", ", date));
                } else {
                    logsToDelete.add(f);
                }
            });
            logsToDelete.addAll(switchLogsAfterEndDate);

            episodeLogRepository.saveAll(logsToUpdate);
            episodeLogRepository.deleteAll(logsToDelete);
        }
    }

    @Override
    public EpisodeLog addAdherenceLogs(EditDosesRequest editDosesRequest, List<LocalDateTime> dates, ZoneId zoneId) {
        String category;
        String actionTakenPrefix;

        if (editDosesRequest.getAddDoses()) {
            if (AdherenceCodeEnum.getAdherenceCode(editDosesRequest.getCode()) == AdherenceCodeEnum.PATIENT_MANUAL.getAdherenceCode()) {
                category = "Patient_Manual_Doses_Added";
                actionTakenPrefix =  "Patient manual doses marked for ";
            } else if (AdherenceCodeEnum.getAdherenceCode(editDosesRequest.getCode()) == AdherenceCodeEnum.PATIENT_MISSED.getAdherenceCode()) {
                category = "Patient_Missed_Doses_Added";
                actionTakenPrefix = "Patient missed doses marked for ";
            } else if (AdherenceCodeEnum.getAdherenceCode(editDosesRequest.getCode()) == AdherenceCodeEnum.MANUAL.getAdherenceCode()) {
                category = "Manual_Doses_Added";
                actionTakenPrefix = "Manual doses marked for ";
                editDosesRequest.setFetchAdherence(true);
            } else {
                category = "Missed_Doses_Added";
                actionTakenPrefix = "Missed doses marked for ";
                editDosesRequest.setFetchAdherence(true);
            }
        } else {
            if (AdherenceCodeEnum.getAdherenceCode(editDosesRequest.getCode()) == AdherenceCodeEnum.PATIENT_MANUAL.getAdherenceCode()) {
                category = "Patient_Manual_Doses_Removed";
                actionTakenPrefix =  "Patient Unmarked manual doses for ";
            } else if (AdherenceCodeEnum.getAdherenceCode(editDosesRequest.getCode()) == AdherenceCodeEnum.PATIENT_MISSED.getAdherenceCode()) {
                category = "Patient_Missed_Doses_Removed";
                actionTakenPrefix = "Patient Unmarked missed doses for ";
            } else if (AdherenceCodeEnum.getAdherenceCode(editDosesRequest.getCode()) == AdherenceCodeEnum.MANUAL.getAdherenceCode()) {
                category = "Manual_Doses_Removed";
                actionTakenPrefix = "Unmarked manual doses for ";
                editDosesRequest.setFetchAdherence(true);
            } else {
                category = "Missed_Doses_Removed";
                actionTakenPrefix = "Unmarked missed doses for ";
                editDosesRequest.setFetchAdherence(true);
            }
        }
        List<String> localDates = new ArrayList<>();
        dates.forEach( d -> {
            String date = ZonedDateTime.of(d, ZoneOffset.UTC).toOffsetDateTime().atZoneSameInstant(zoneId).toLocalDateTime().format(DateTimeFormatter.ofPattern("d/M/yyyy HH:mm:ss"));
            localDates.add(date);
        });

        String actionTaken = actionTakenPrefix + String.join(", ", localDates);
        EpisodeLog episodeLog = new EpisodeLog(editDosesRequest.getEpisodeId(), editDosesRequest.getUserId(), category, actionTaken, editDosesRequest.getAddedNote());
        episodeLogRepository.save(episodeLog);
        return episodeLog;
    }

    @Override
    public EpisodeLog addEpisodeDeletionLog(DeleteRequest episodeDeleteRequest) {
        String category = "Patient_Deleted";
        String actionTaken = "Reason for Deletion: " + episodeDeleteRequest.getReason();
        EpisodeLog episodeLog = new EpisodeLog(episodeDeleteRequest.getEpisodeId(), episodeDeleteRequest.getDeletedBy(), category, actionTaken, episodeDeleteRequest.getAdditionalNotes());
        episodeLogRepository.save(episodeLog);
        return episodeLog;
    }

    /**
     * Update existing support actions
     * @param updateSupportActionsRequest - request payload
     */
    @Override
    public void updateSupportActions(UpdateSupportActionsRequest updateSupportActionsRequest) {
        updateSupportActionsRequest.validate();
        List<String> categories = episodeLogStoreCreation.getEpisodeLogStoreMap().values().stream().filter(e -> Constants.SUPPORT_ACTION_LOGS.equals(e.getCategoryGroup())).map(EpisodeLogStore::getCategory).collect(Collectors.toList());
        Map<Long, EpisodeLog> existingLogMap = episodeLogRepository.getAllByEpisodeIdAndCategoryIn(updateSupportActionsRequest.getEpisodeId(), categories)
                .stream()
                .collect(Collectors.toMap(EpisodeLog::getId, e -> e));
        List<EpisodeLog> actionsToUpdate = new ArrayList<>();
        List<EpisodeLog> actionsToDelete = new ArrayList<>();
        for (UpdateSupportActionsDataRequest logDataRequest : updateSupportActionsRequest.getEpisodeLogData()) {
            if (existingLogMap.containsKey(logDataRequest.getId())) {
                EpisodeLog existingLog = existingLogMap.get(logDataRequest.getId());
                if (!logDataRequest.equal(existingLog)) {
                    EpisodeLog updatedLog = new EpisodeLog(existingLog.getEpisodeId(), existingLog.getAddedBy(), logDataRequest.getCategory(), logDataRequest.getSubCategory(), logDataRequest.getActionTaken(), logDataRequest.getComments(), logDataRequest.getDateOfAction());
                    actionsToUpdate.add(updatedLog);
                    actionsToDelete.add(existingLog);
                }
            }
        }
        Set<Long> updateSupportActionLogIds = updateSupportActionsRequest.getEpisodeLogData().stream().map(UpdateSupportActionsDataRequest::getId).collect(Collectors.toSet());
        for (Map.Entry<Long, EpisodeLog> episodeLogEntry : existingLogMap.entrySet()) {
            if (!updateSupportActionLogIds.contains(episodeLogEntry.getKey())) {
                actionsToDelete.add(episodeLogEntry.getValue());
            }
        }
        episodeLogRepository.deleteAll(actionsToDelete);
        episodeLogRepository.saveAll(actionsToUpdate);
    }
}
