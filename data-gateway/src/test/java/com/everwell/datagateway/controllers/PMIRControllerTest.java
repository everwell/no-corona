package com.everwell.datagateway.controllers;

import com.everwell.datagateway.config.CustomExceptionHandler;
import com.everwell.datagateway.models.response.ApiResponse;
import com.everwell.datagateway.models.response.Episode;
import com.everwell.datagateway.service.EpisodeServiceRestService;
import com.everwell.datagateway.service.PMIRService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.HashMap;
import java.util.Map;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class PMIRControllerTest extends BaseControllerTest {
    private MockMvc mockMvc;
    @Mock
    private PMIRService pmirService;

    @Mock
    private EpisodeServiceRestService episodeService;

    @InjectMocks
    private PMIRController pmirController;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(pmirController)
                .setControllerAdvice(new CustomExceptionHandler())
                .build();
    }

    @Test
    public void addPatientTest() throws Exception {
        Map<String, Object> createEpisodeRequest= new HashMap<>();
        createEpisodeRequest.put("firstName", "test");
        mockGetClientWithEventAccessCheck();
        when(pmirService.createUpdateEpisodeRequest(Mockito.any(), Mockito.anyBoolean(), Mockito.anyLong())).thenReturn(createEpisodeRequest);
        when(episodeService.createEpisode(Mockito.any(), Mockito.anyLong())).thenReturn(new ApiResponse<>("true", new Episode(), null));
        String request = getPatientRequest();
        mockMvc.
                perform(
                        MockMvcRequestBuilders.
                                post("/v1/patient")
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                                .content(request)
                )
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.Success").value("true"));
    }

    @Test
    public void updatePatientTest() throws Exception {
        Map<String, Object> createEpisodeRequest= new HashMap<>();
        createEpisodeRequest.put("firstName", "test");
        mockGetClientWithEventAccessCheck();
        when(episodeService.updateEpisode(Mockito.any(), Mockito.anyLong())).thenReturn(new ApiResponse<>("true", new Episode(), null));
        when(pmirService.createUpdateEpisodeRequest(Mockito.any(), Mockito.anyBoolean(), Mockito.anyLong())).thenReturn(createEpisodeRequest);
        String request = getPatientRequest();
        mockMvc.
                perform(
                        MockMvcRequestBuilders.
                                put("/v1/patient")
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                                .content(request)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.Success").value("true"));
    }

    @Test
    public void deletePatientTest() throws Exception {
        String request = "{\n" +
                "  \"resourceType\": \"Patient\",\n" +
                "  \"id\": \"1\"\n" +
                "}";
        when(episodeService.deleteEpisode(Mockito.anyLong(), Mockito.anyLong())).thenReturn(new ApiResponse<>("true", "test", null));
        mockGetClientWithEventAccessCheck();
        mockMvc.
                perform(
                        MockMvcRequestBuilders.
                                delete("/v1/patient")
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                                .content(request)
                )
                .andExpect(status().isAccepted());
    }

    @Test
    public void addMultiplePatientsTest() throws Exception {
        Map<String, Object> createEpisodeRequest= new HashMap<>();
        createEpisodeRequest.put("firstName", "test");
        mockGetClientWithEventAccessCheck();
        when(pmirService.createUpdateEpisodeRequest(Mockito.any(), Mockito.anyBoolean(), Mockito.anyLong())).thenReturn(createEpisodeRequest);
        when(episodeService.createEpisode(Mockito.any(), Mockito.anyLong())).thenReturn(new ApiResponse<>("true", new Episode(), null));
        String request = getBundleRequest();
        mockMvc.
                perform(
                        MockMvcRequestBuilders.
                                post("/v1/patients")
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                                .content(request)
                )
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.Success").value("true"));
    }

    @Test
    public void updateMultiplePatientsTest() throws Exception {
        Map<String, Object> createEpisodeRequest= new HashMap<>();
        createEpisodeRequest.put("firstName", "test");
        mockGetClientWithEventAccessCheck();
        when(pmirService.createUpdateEpisodeRequest(Mockito.any(), Mockito.anyBoolean(), Mockito.anyLong())).thenReturn(createEpisodeRequest);
        when(episodeService.updateEpisode(Mockito.any(), Mockito.anyLong())).thenReturn(new ApiResponse<>("true", new Episode(), null));
        String request = getBundleRequest();
        mockMvc.
                perform(
                        MockMvcRequestBuilders.
                                put("/v1/patients")
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                                .content(request)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.Success").value("true"));
    }

    private String getPatientRequest() {
        String request = "{\"resourceType\": \"Patient\", \"id\": \"1\", \"active\": true, \"name\": " +
                "[{\"use\": \"official\", \"family\": \"last\", \"given\": [\"first\", \"name\"]}], \"telecom\": " +
                "[{\"system\": \"phone\", \"value\": \"+1-734-942-9512\", \"use\": \"work\"}, {\"system\": \"email\", \"value\": \"DavidARiegel@jourrapide.com\", \"use\": \"work\"}], \"gender\": \"male\", " +
                "\"birthDate\": \"1990-01-01\", \"address\": [{\"use\": \"home\", \"type\": \"both\", \"text\": \"4512 Bombardier Way\\nRomulus, MI 48174\", " +
                "\"line\": [\"4512 Bombardier Way\"], \"city\": \"Romulus\", \"state\": \"MI\", \"postalCode\": \"48174\", \"country\": \"US\"}], " +
                "\"maritalStatus\": {\"coding\": [{\"system\": \"http://hl7.org/fhir/v3/MaritalStatus\", \"code\": \"M\", \"display\": \"Married\"}]}, \"contact\": [{\"relationship\": [{\"coding\": [{\"system\": \"http://terminology.hl7.org/CodeSystem/v2-0131\", " +
                "\"code\": \"NOK\", \"display\": \"Next Of Kin\"}]}], \"name\": {\"family\": \"Doe\", \"given\": " +
                "[\"John\"]}, \"telecom\": [{\"system\": \"phone\", \"value\": \"1234567890\", \"use\": \"home\"}] }], \"communication\": [{\"language\": {\"coding\": [{\"system\": \"urn:ietf:bcp:47\", \"code\": \"en\", \"display\": \"English\"}]}}] }";
        return  request;
    }

    private String getBundleRequest() {
        String request = "{\"resourceType\":\"Bundle\",\"id\":\"ex-PMIRBundleCreate\",\"type\":\"message\",\"entry\":[{\"resource\":{\"resourceType\":\"MessageHeader\",\"id\":\"ex-messageheader-create\"}},{\"fullUrl\":\"Bundle/ex-bundle-history-create\",\"resource\":{\"resourceType\":\"Bundle\",\"id\":\"ex-bundle-history-create\",\"type\":\"history\",\"entry\":[{\"resource\":{\"resourceType\":\"Patient\",\"id\":\"ex-patient-create1\",\"active\":true,\"name\":[{\"use\":\"official\",\"family\":\"Riegel\",\"given\":[\"David\",\"A.\"]}],\"telecom\":[{\"system\":\"phone\",\"value\":\"+1-734-942-9512\",\"use\":\"work\"},{\"system\":\"email\",\"value\":\"DavidARiegel@jourrapide.com\",\"use\":\"work\"}],\"gender\":\"male\",\"birthDate\":\"1985-07-12\"},\"request\":{\"method\":\"POST\",\"url\":\"Patient\"},\"response\":{\"status\":\"201\",\"location\":\"Patient/ex-patient-create1/_history/1\"}},{\"fullUrl\":\"urn:uuid:95e1726c-9c3c-4de3-bebc-15daed6b6d34\",\"resource\":{\"resourceType\":\"Patient\",\"id\":\"ex-patient-create2\",\"active\":true,\"name\":[{\"use\":\"official\",\"family\":\"Wooten\",\"given\":[\"Lucille\",\"T.\"]}],\"telecom\":[{\"system\":\"phone\",\"value\":\"+1-570-508-4343\",\"use\":\"work\"},{\"system\":\"email\",\"value\":\"LucilleTWooten@teleworm.us\",\"use\":\"work\"}],\"gender\":\"female\",\"birthDate\":\"1971-12-14\"},\"request\":{\"method\":\"POST\",\"url\":\"Patient\"},\"response\":{\"status\":\"201\",\"location\":\"Patient/ex-patient-create2/_history/1\"}}]}}]}";
        return request;
    }

}
