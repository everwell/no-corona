package com.everwell.datagateway.handlers.impl.publishers;

import com.everwell.datagateway.enums.EventEnum;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

class GetTestEventIdHandlerTest {

    @Test
    void getEventEnumerator() {
        GetTestEventIdHandler getTestEventIdHandler = new GetTestEventIdHandler();
        assertTrue(getTestEventIdHandler.getEventEnumerator().equals(EventEnum.GET_TEST_ID));
    }
}