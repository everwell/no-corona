package com.everwell.datagateway.handlers;

import com.everwell.datagateway.BaseTest;
import com.everwell.datagateway.service.TriggerService;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;

import static org.mockito.Mockito.*;

class JobHandlerTest extends BaseTest {
    @Spy
    @InjectMocks
    JobHandler jobHandler;

    @Mock
    TriggerService triggerService;

    @Test
    public void testMethodCallToReadTable()
    {
        doNothing().when(triggerService).readTriggerTable();
        jobHandler.everydayJobScheduler();
        verify(triggerService, times(1)).readTriggerTable();
    }
}