package com.everwell.datagateway.utils;

import com.everwell.datagateway.BaseTest;
import com.everwell.datagateway.exceptions.RedisException;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

class CacheUtilsTest extends BaseTest {

    CacheUtils cacheUtils;

    @Mock
    RedisTemplate<String, Object> redisTemplate;

    private String DUMMY_KEY = "key";


    @Mock
    private ValueOperations valueOperations;

    @BeforeEach
    public void init() {
        cacheUtils = new CacheUtils(redisTemplate);
    }


    @Test
    void getFromCache() {
        Object DUMMY_VAL = new Object();
        when(redisTemplate.opsForValue()).thenReturn(valueOperations);
        when(valueOperations.get(eq(DUMMY_KEY))).thenReturn(DUMMY_VAL);
        Object obj = cacheUtils.getFromCache(DUMMY_KEY);
        Assert.assertEquals(obj, DUMMY_VAL);
    }

    @Test
    void getFromCacheNull() {
        Object DUMMY_VAL = null;
        when(redisTemplate.opsForValue()).thenReturn(valueOperations);
        when(valueOperations.get(eq(DUMMY_KEY))).thenReturn(DUMMY_VAL);
        Object obj = cacheUtils.getFromCache(DUMMY_KEY);
        Assert.assertNull(obj);
    }

    @Test
    void getFromCacheException() {
        when(redisTemplate.opsForValue()).thenReturn(null);
        Assertions.assertThrows(RedisException.class, () -> {
            CacheUtils.getFromCache(DUMMY_KEY);
        });
    }

    @Test
    void hasKey() {
        when(redisTemplate.hasKey(eq(DUMMY_KEY))).thenReturn(true);
        boolean ans = cacheUtils.hasKey(DUMMY_KEY);
        Assert.assertTrue(ans);
    }

    @Test
    void hasKeyException() {
        when(redisTemplate.hasKey(eq(DUMMY_KEY))).thenReturn(null);   //In case of any exception
        Assertions.assertThrows(RedisException.class, () -> {
            CacheUtils.hasKey(DUMMY_KEY);
        });


    }


    @Test
    void updateCacheByValueValid() {
        Long value = 4L;
        long incrementBy = 2;
        when(redisTemplate.opsForValue()).thenReturn(valueOperations);
        when(valueOperations.increment((DUMMY_KEY), incrementBy)).thenReturn(value);
        Long returnedValue = CacheUtils.updateCacheByValue(DUMMY_KEY, incrementBy);
        Assert.assertTrue(returnedValue.equals(value));
    }

    @Test
    void updateCacheByValueException() {
        long incrementBy = 2;
        when(redisTemplate.opsForValue()).thenReturn(null);
        Assertions.assertThrows(RedisException.class, () -> {
            CacheUtils.updateCacheByValue(DUMMY_KEY, incrementBy);
        });
    }

    @Test
    void putIntoCacheException() {
        when(redisTemplate.opsForValue()).thenReturn(null);
        Assertions.assertThrows(RedisException.class, () -> {
            CacheUtils.putIntoCache(DUMMY_KEY, new Object());
        });

        Assertions.assertThrows(RedisException.class, () -> {
            CacheUtils.putIntoCache(DUMMY_KEY, new Object(), 1);
        });
    }


    @Test
    void putIntoCache() {
        when(redisTemplate.opsForValue()).thenReturn(valueOperations);
        Mockito.doNothing().when(valueOperations).set(anyString(), anyObject());
        CacheUtils.putIntoCache(DUMMY_KEY, new Object());
    }


}