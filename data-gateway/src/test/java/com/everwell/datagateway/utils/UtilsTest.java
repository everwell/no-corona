package com.everwell.datagateway.utils;

import com.everwell.datagateway.exceptions.ValidationException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.netflix.zuul.context.RequestContext;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.zip.GZIPOutputStream;

import static org.junit.jupiter.api.Assertions.*;

class UtilsTest {


    @Test
    void readRequestBodyNullInput() throws Exception {
        assertNull(Utils.readRequestBody((InputStream) null));
    }

    @Test
    void readRequestBodyValidInput() throws Exception {
        String input = "test";
        InputStream inputStream = new ByteArrayInputStream(input.getBytes());
        String result = Utils.readRequestBody(inputStream);
        assertTrue(result.equals(input));
    }

    @Test
    void getRemainingTime() {
        Calendar c = Calendar.getInstance();
        Integer answer = (86400 - ((c.get(Calendar.HOUR_OF_DAY) * 60 + c.get(Calendar.MINUTE)) * 60));
        Integer answerReturned = Utils.getRemainingTime();
        assertEquals(answerReturned, answer);
    }

    @Test
    void processResponseEntityNullResponseEntityTest() throws JsonProcessingException {
        ValidationException exception = Assertions.assertThrows(ValidationException.class, () -> {
            Utils.processResponseEntity(null, 200, String.class, "errorMessage");
        });
        assertEquals("errorMessage", exception.getMessage());
    }

    @Test
    void processResponseEntityNullBodyTest() {
        ResponseEntity<String> responseEntity = new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        ValidationException exception = Assertions.assertThrows(ValidationException.class, () -> {
            Utils.processResponseEntity(responseEntity, 200, String.class, "notFound");
        });
        assertEquals("notFound", exception.getMessage());
    }

    @Test
    void processResponseEntityUnsuccessfulResponseTest() {
        ResponseEntity<String> responseEntity = new ResponseEntity<>("Not found response", HttpStatus.NOT_FOUND);
        ValidationException exception = Assertions.assertThrows(ValidationException.class, () -> {
            Utils.processResponseEntity(responseEntity, 200, String.class, "notFound");
        });
        assertEquals("Not found response", exception.getMessage());
    }

    @Test
    void processResponseEntityTest() throws JsonProcessingException {
        ResponseEntity<String> responseEntity = new ResponseEntity<>("{\"test\":\"value\"}", HttpStatus.OK);
        Map<String, String> result = Utils.processResponseEntity(responseEntity, 200, Map.class, "errorMessage");
        assertEquals(1, result.size());
        assertEquals("value", result.get("test"));
    }

    @Test
    void buildURIStringTest() {
        String url = "http://test.com/api/test";
        Map<String, Object> params = new HashMap<>();
        params.put("param1", "value");
        String uri = Utils.buildURIString(url, params);
        assertEquals("http://test.com/api/test?param1=value", uri);
    }

    @Test
    void buildURIStringTestWithoutParams() {
        String url = "http://test.com/api";
        Map<String, Object> params = new HashMap<>();
        String uri = Utils.buildURIString(url, params);
        assertEquals("http://test.com/api", uri);
    }

    @Test
    void encryptDataTest() throws Exception {
        String encryptedContent = Utils.encryptData("{\"name\":\"John Doe\",\"age\":30,\"city\":\"New York\",\"email\":\"john.doe@example.com\"}","x7XRQBZ32nZ3l8je5qXTLwHeiHnJ1GFMIsNwf0kfXns=");
        assertEquals("vM7gbb5480o50UoUFDb4vapH/ixLkA7+9Wfab4tdHW4dEqzGx1J1RWuzdj2cWmRnvC/ny9cAD+kSjszvfhf/rSBuw9W2qGq/lAS5lGWct5M=",encryptedContent);
    }

    @Test
    void formatIntoRequestBodySchemaTest() throws Exception {
        Map<String,Object> schema = Utils.convertStrToObject("{\"scheme-details\":[\"location\",\"is_data_awaited\",\"beneficiary-details\",\"fundtransfer-cash\",\"transaction-details\"],\"location\":[\"state_code\",\"state_name\",\"district_code\",\"district_name\"],\"beneficiary-details\":[\"beneficiaries_total_x\",\"beneficiaries_state_additional_y\",\"beneficiaries_total\",\"beneficiaries_digitised\",\"beneficiaries_aadhaar_authenticated\",\"beneficiaries_mobile_seeded\"],\"fundtransfer-cash\":[\"fund_cash_centre\",\"fund_cash_state\",\"fund_cash_additional_state_x\",\"fund_cash_state_y\",\"fund_cash_total\",\"fund_cash_electronic_centre\",\"fund_cash_electronic_state\",\"fund_cash_electronic_additional_state_x\",\"fund_cash_electronic_state_y\",\"fund_cash_electronic_apb\",\"fund_cash_electronic_non_apb\",\"fund_cash_electronic\",\"fund_cash_non_electronic\"],\"transaction-details\":[\"transaction_cash_electronic_apb\",\"transaction_cash_electronic_non_apb\",\"transaction_cash_electronic\",\"transaction_cash_non_electronic\"]}",new TypeReference<Map<String, Object>>() {});
        Map<String,Object> dataMap = Utils.convertStrToObject("{\"fund_cash_centre\":0.0,\"fund_cash_additional_state_x\":0.0,\"fund_cash_electronic_apb\":0.0,\"year\":2023,\"beneficiaries_state_additional_y\":0,\"expenditure_inkind_additional_state_x\":0.0,\"expenditure_inkind_centre\":0.0,\"transaction_cash_non_electronic\":0,\"expenditure_inkind_state_y\":0.0,\"transaction_cash_electronic_non_apb\":0,\"expenditure_inkind_authenticated\":0.0,\"schemecode\":\"BOGCU\",\"beneficiaries_aadhaar_authenticated\":0,\"expenditure_inkind_state\":0.0,\"district_name\":\"NICOBARS\",\"beneficiaries_mobile_seeded\":0,\"fund_cash_electronic_centre\":0.0,\"expenditure_inkind_authenticated_state_y\":0.0,\"state_name\":\"ANDAMAN AND NICOBAR ISLANDS\",\"quantity_transferred\":0,\"additional_parameter3\":0.0,\"expenditure_inkind_total\":0.0,\"additional_parameter1\":0.0,\"additional_parameter2\":0.0,\"fund_cash_electronic_state_y\":0.0,\"fund_cash_electronic_non_apb\":0.0,\"fund_cash_total\":0.0,\"beneficiaries_total_x\":0,\"fund_cash_electronic_additional_state_x\":0.0,\"transaction_inkind_authenticated\":0,\"expenditure_inkind_authenticated_centre\":0.0,\"beneficiaries_total\":0,\"fund_cash_non_electronic\":0.0,\"number_of_groups_shg\":0,\"expenditure_inkind_authenticated_state\":0.0,\"fund_cash_electronic_state\":0.0,\"transaction_cash_electronic_apb\":0,\"is_data_awaited\":0,\"month\":4,\"district_code\":603,\"fund_cash_state_y\":0.0,\"beneficiaries_digitised\":0,\"fund_cash_state\":0.0,\"state_code\":35,\"expenditure_inkind_authenticated_additional_state_x\":0.0,\"fund_cash_electronic\":0.0,\"transaction_cash_electronic\":0}",  new TypeReference<Map<String, Object>>() {});
        List<Map<String,Object>> kpiList = new ArrayList<>();
        kpiList.add(dataMap);
        Map<String,Object> kpiData = Utils.formatIntoRequestBodySchema(kpiList,schema);
        Assertions.assertEquals(Utils.asJsonString(kpiData),"{\"scheme-details\":[{\"fundtransfer-cash\":{\"fund_cash_centre\":0.0,\"fund_cash_additional_state_x\":0.0,\"fund_cash_electronic_apb\":0.0,\"fund_cash_total\":0.0,\"fund_cash_electronic_additional_state_x\":0.0,\"fund_cash_non_electronic\":0.0,\"fund_cash_electronic_state\":0.0,\"fund_cash_electronic_centre\":0.0,\"fund_cash_state_y\":0.0,\"fund_cash_state\":0.0,\"fund_cash_electronic_state_y\":0.0,\"fund_cash_electronic_non_apb\":0.0,\"fund_cash_electronic\":0.0},\"is_data_awaited\":0,\"transaction-details\":{\"transaction_cash_electronic_apb\":0,\"transaction_cash_non_electronic\":0,\"transaction_cash_electronic_non_apb\":0,\"transaction_cash_electronic\":0},\"location\":{\"district_name\":\"NICOBARS\",\"state_name\":\"ANDAMAN AND NICOBAR ISLANDS\",\"district_code\":603,\"state_code\":35},\"beneficiary-details\":{\"beneficiaries_aadhaar_authenticated\":0,\"beneficiaries_mobile_seeded\":0,\"beneficiaries_total_x\":0,\"beneficiaries_state_additional_y\":0,\"beneficiaries_total\":0,\"beneficiaries_digitised\":0}}]}");
    }


    @Test
    public void testReadGzipRequestBody() throws IOException {
        RequestContext ctx = new RequestContext();
        String originalData = "This is the data to be compressed.";
        byte[] compressedData = compressData(originalData);
        InputStream inputStream = new ByteArrayInputStream(compressedData);
        ctx.setResponseDataStream(inputStream);
        String requestBody = Utils.readGzipRequestBody(ctx);
        assertEquals(originalData, requestBody);
    }

    private byte[] compressData(String data) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try (GZIPOutputStream gzipOutputStream = new GZIPOutputStream(byteArrayOutputStream)) {
            gzipOutputStream.write(data.getBytes(StandardCharsets.UTF_8));
        }
        return byteArrayOutputStream.toByteArray();
    }
    @Test
    public void testConvertAndFormatDateStringForTaskerWithInvalidInput() {
        String invalidInputDate = "invalid_date";
        String outputFormat = "yyyy-MM-dd HH:mm:ss";
        String result = Utils.convertAndFormatDateStringForTasker(invalidInputDate, outputFormat);
        assertEquals(invalidInputDate, result);
    }

    @Test
    public void testConvertAndFormatDateStringForTasker() {
        String inputDate = "2022-03-01T12:34:56.789Z";
        String outputFormat = "yyyy-MM-dd HH:mm:ss";
        String expectedOutput = "2022-03-01 12:34:56";
        String result = Utils.convertAndFormatDateStringForTasker(inputDate, outputFormat);
        assertEquals(expectedOutput, result);
    }


}