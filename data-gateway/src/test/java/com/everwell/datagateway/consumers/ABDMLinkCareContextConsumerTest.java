package com.everwell.datagateway.consumers;

import com.everwell.datagateway.BaseTest;
import com.everwell.datagateway.models.dto.EventStreamingDTO;
import com.everwell.datagateway.models.request.abdm.ABDMLinkCareContextEvent;
import com.everwell.datagateway.service.ABDMIntegrationService;
import com.everwell.datagateway.utils.Utils;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageBuilder;

import static com.everwell.datagateway.constants.QueueConstants.ABDM_LINK_CARE_CONTEXT_QUEUE;
import static org.mockito.ArgumentMatchers.any;

class ABDMLinkCareContextConsumerTest extends BaseTest {

    @Spy
    @InjectMocks
    ABDMLinkCareContextConsumer abdmLinkCareContextConsumer;

    @Mock
    ABDMIntegrationService abdmHelper;

    @Test
    void consume() {
        String input = "{'episodeId':'5555555','personId':'12345','hiTypes':'Prescription','personName':'Ankur Everwell','enrollmentDate':'29/04/2022','identifierValue':'+919903246033','gender':'M','identifierType':'MOBILE','yearOfBirth':'1990','abhaAddress':'ankureverwell@sbx'}";

        Message msg = MessageBuilder.withBody(input.getBytes()).build();
        abdmLinkCareContextConsumer.consume(msg);
        Mockito.verify(abdmLinkCareContextConsumer, Mockito.times(1)).outgoingWebhook(ABDM_LINK_CARE_CONTEXT_QUEUE, msg);
    }

    @Test
    void outgoingWebhook() {
        ABDMLinkCareContextEvent abdmLinkCareContextEvent = new ABDMLinkCareContextEvent(40964616,34192,"Prescription","Test","2023-06-01","8186079679",'M',"MOBILE","mohan.test59@sbx","1999","IN2910000210");
        EventStreamingDTO eventStreamingDTO = new EventStreamingDTO();
        eventStreamingDTO.setEventName("link-careContext");
        eventStreamingDTO.setField(abdmLinkCareContextEvent);
        String input = Utils.asJsonString(eventStreamingDTO);
        Message msg = MessageBuilder.withBody(input.getBytes()).build();
        abdmLinkCareContextConsumer.outgoingWebhook(ABDM_LINK_CARE_CONTEXT_QUEUE, msg);
        Mockito.verify(abdmHelper, Mockito.times(1)).initiateDemographicAuth(any());
    }

    @Test
    void outgoingWebhookException() {
        String input = "{'episodeId':'5555555','personId':'12345','hiTypes':'Prescription','personName':'Ankur Everwell','enrollmentDate':'29/04/2022','identifierValue':'+919903246033','gender':'M','identifierType':'MOBILE','yearOfBirth':'1990','abhaAddress':'ankureverwell@sbx'}";

        Message msg = MessageBuilder.withBody(input.getBytes()).build();
        abdmLinkCareContextConsumer.outgoingWebhook(ABDM_LINK_CARE_CONTEXT_QUEUE, null);
    }
}