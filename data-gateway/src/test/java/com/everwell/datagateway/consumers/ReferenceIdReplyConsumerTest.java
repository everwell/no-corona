package com.everwell.datagateway.consumers;

import com.everwell.datagateway.BaseTest;
import com.everwell.datagateway.entities.Client;
import com.everwell.datagateway.entities.Event;
import com.everwell.datagateway.exceptions.NotFoundException;
import com.everwell.datagateway.exceptions.RedisException;
import com.everwell.datagateway.exceptions.RestTemplateException;
import com.everwell.datagateway.exceptions.URLDisabledException;
import com.everwell.datagateway.service.ClientService;
import com.everwell.datagateway.service.ConsumerService;
import com.everwell.datagateway.service.EventService;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class ReferenceIdReplyConsumerTest extends BaseTest {

    @InjectMocks
    ReferenceIdReplyConsumer referenceIdReplyConsumer;

    @Mock
    private ClientService clientService;

    @Mock
    private EventService eventService;

    @Mock
    ConsumerService consumerService;


    ReferenceIdReplyConsumer referenceIdReplyConsumerObject = new ReferenceIdReplyConsumer();



    @Test
    void consume() {
        byte[] byteArray = {'T', 'E', 'S', 'T'};
        ReferenceIdReplyConsumer referenceIdReplyConsumer = Mockito.spy(ReferenceIdReplyConsumer.class);
        doNothing().when(referenceIdReplyConsumer).outgoingWebhook(anyString(), (Message) any());
        referenceIdReplyConsumer.consume(new Message(byteArray, new MessageProperties()));
        verify(referenceIdReplyConsumer, times(1)).outgoingWebhook(any(), (Message) any());
    }



}