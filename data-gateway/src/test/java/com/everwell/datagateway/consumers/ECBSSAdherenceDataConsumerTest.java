package com.everwell.datagateway.consumers;

import com.everwell.datagateway.BaseTest;
import com.everwell.datagateway.constants.Constants;
import com.everwell.datagateway.constants.QueueConstants;
import com.everwell.datagateway.models.dto.PublishToSubscriberUrlDTO;
import com.everwell.datagateway.models.response.ecbss.TrackedEntityInstancesResponse;
import com.everwell.datagateway.publishers.RMQPublisher;
import com.everwell.datagateway.service.ConsumerService;
import com.everwell.datagateway.service.ECBSSHelperService;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class ECBSSAdherenceDataConsumerTest extends BaseTest {

    @InjectMocks
    ECBSSAdherenceDataConsumer ecbssAdherenceDataConsumer;

    @Mock
    ECBSSHelperService eCBSSHelper;

    @Mock
    RMQPublisher rabbitMQPublisherService;

    @Mock
    ConsumerService consumerService;

    @Test
    void consume() {
        byte[] byteArray = {'T', 'E', 'S', 'T'};
        ECBSSAdherenceDataConsumer ecbssAdherenceDataConsumer = Mockito.spy(ECBSSAdherenceDataConsumer.class);
        doNothing().when(ecbssAdherenceDataConsumer).outgoingWebhook(any(), (Message) any());
        ecbssAdherenceDataConsumer.consume(new Message(byteArray, new MessageProperties()));
        verify(ecbssAdherenceDataConsumer, times(1)).outgoingWebhook(any(), (Message) any());
    }

    @Test
    public void consumerTestClientNotFound() {
        String test = "Test";
        ecbssAdherenceDataConsumer.outgoingWebhook(QueueConstants.ECBSS_ADHERENCE_DATA_QUEUE, new Message(test.getBytes(), new MessageProperties()));
        verify(eCBSSHelper, times(0)).getTrackedEntityInstance(anyString(), anyString());
        verify(rabbitMQPublisherService, times(0)).send(any(), any(), any(), any());
        verify(consumerService, times(0)).publishToSubscriberUrl(any(PublishToSubscriberUrlDTO.class));
    }

    @Test
    void outgoingWebhookTestNullExternalId2() {
        String adherenceDataRequest = getAdherenceDataRequestNullExternalId2();
        byte[] byteArray = adherenceDataRequest.getBytes();
        when(eCBSSHelper.getTrackedEntityInstance(any(), any())).thenReturn(new TrackedEntityInstancesResponse());
        ecbssAdherenceDataConsumer.outgoingWebhook(QueueConstants.ECBSS_ADHERENCE_DATA_QUEUE, new Message(byteArray, getMessageProperties()));
        verify(eCBSSHelper, times(1)).getTrackedEntityInstance(anyString(), anyString());
        verify(rabbitMQPublisherService, times(0)).send(any(), any(), any(), any());
        verify(consumerService, times(0)).publishToSubscriberUrl(any(PublishToSubscriberUrlDTO.class));
    }

    @Test
    void outgoingWebhookTestTrackedEntityInstanceNull() {
        String adherenceDataRequest = getAdherenceDataRequestNullExternalId2();
        byte[] byteArray = adherenceDataRequest.getBytes();
        when(eCBSSHelper.getTrackedEntityInstance(any(), any())).thenReturn(null);
        ecbssAdherenceDataConsumer.outgoingWebhook(QueueConstants.ECBSS_ADHERENCE_DATA_QUEUE, new Message(byteArray, getMessageProperties()));
        verify(eCBSSHelper, times(1)).getTrackedEntityInstance(anyString(), anyString());
        verify(rabbitMQPublisherService, times(0)).send(any(), any(), any(), any());
        verify(consumerService, times(0)).publishToSubscriberUrl(any(PublishToSubscriberUrlDTO.class));
    }

    @Test
    void outgoingWebhookTestException() {
        String adherenceDataRequest = getAdherenceDataRequestNullExternalId2();
        byte[] byteArray = adherenceDataRequest.getBytes();
        doThrow(NullPointerException.class).when(eCBSSHelper).getTrackedEntityInstance(any(), any());
        ecbssAdherenceDataConsumer.outgoingWebhook(QueueConstants.ECBSS_ADHERENCE_DATA_QUEUE, new Message(byteArray, getMessageProperties()));
        verify(eCBSSHelper, times(1)).getTrackedEntityInstance(anyString(), anyString());
        verify(rabbitMQPublisherService, times(0)).send(any(), any(), any(), any());
        verify(consumerService, times(0)).publishToSubscriberUrl(any(PublishToSubscriberUrlDTO.class));
    }

    @Test
    void outgoingWebhookTest() {
        String adherenceDataRequest = getAdherenceDataRequest();
        byte[] byteArray = adherenceDataRequest.getBytes();
        ecbssAdherenceDataConsumer.outgoingWebhook(QueueConstants.ECBSS_ADHERENCE_DATA_QUEUE, new Message(byteArray, getMessageProperties()));
        verify(eCBSSHelper, times(0)).getTrackedEntityInstance(anyString(), anyString());
        verify(rabbitMQPublisherService, times(0)).send(any(), any(), any(), any());
        verify(consumerService, times(1)).publishToSubscriberUrl(any(PublishToSubscriberUrlDTO.class));
    }

    @Test
    void outgoingWebhook_SendAdherence() {
        String adherenceDataRequest = getAdherenceDataRequestNullExternalId2();
        TrackedEntityInstancesResponse.TrackedEntityInstance trackedEntityInstance = new TrackedEntityInstancesResponse.TrackedEntityInstance();
        trackedEntityInstance.setTrackedEntityInstance("123");
        TrackedEntityInstancesResponse response = new TrackedEntityInstancesResponse();
        response.setTrackedEntityInstances(new ArrayList<>(Collections.singletonList(trackedEntityInstance)));
        byte[] byteArray = adherenceDataRequest.getBytes();
        Map<Integer, String> map2 = new HashMap<>();
        map2.put(200, "Test");
        when(eCBSSHelper.getTrackedEntityInstance(any(), any())).thenReturn(response);
        doNothing().when(rabbitMQPublisherService).send(any(), any(), any(), any());
        ecbssAdherenceDataConsumer.outgoingWebhook(QueueConstants.ECBSS_ADHERENCE_DATA_QUEUE, new Message(byteArray, getMessageProperties()));
        verify(rabbitMQPublisherService, times(1)).send(any(), any(), any(), any());
        verify(consumerService, times(1)).publishToSubscriberUrl(any(PublishToSubscriberUrlDTO.class));
    }

    private String getAdherenceDataRequestNullExternalId2() {
        return "{\"entityId\":\"320219\",\"externalId1\":\"123\",\"externalId2\":null,\"ecbssAdherenceDetailsDto\":{\"orgUnit\":\"goFnHxlDGzD\",\"program\":\"wfd9K4dQVDR\",\"programStage\":\"pQjHJmDXg4j\",\"eventDate\":\"2023-06-08\",\"trackedEntityInstance\":null,\"dataValues\":[{\"dataElement\":\"PYXkMAh7Fu4\",\"value\":\"88\"},{\"dataElement\":\"a07BsgpwFan\",\"value\":\"1\"},{\"dataElement\":\"U4jSUZPF0HH\",\"value\":\"Month 3\"}]}}";
    }

    private String getAdherenceDataRequest() {
        return "{\"entityId\":\"320219\",\"externalId1\":\"123\",\"externalId2\":\"1\",\"ecbssAdherenceDetailsDto\":{\"orgUnit\":\"goFnHxlDGzD\",\"program\":\"wfd9K4dQVDR\",\"programStage\":\"pQjHJmDXg4j\",\"eventDate\":\"2023-06-08\",\"trackedEntityInstance\":\"1\",\"dataValues\":[{\"dataElement\":\"PYXkMAh7Fu4\",\"value\":\"88\"},{\"dataElement\":\"a07BsgpwFan\",\"value\":\"1\"},{\"dataElement\":\"U4jSUZPF0HH\",\"value\":\"Month 3\"}]}}";
    }

    private MessageProperties getMessageProperties() {
        MessageProperties messageProperties = new MessageProperties();
        messageProperties.setHeader(Constants.RMQ_HEADER_CLIENT_ID, "63");
        return messageProperties;
    }

}