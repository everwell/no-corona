package com.everwell.datagateway.zuul

import com.everwell.datagateway.component.AppProperties
import com.everwell.datagateway.filters.zuul.userService.UserServiceZuulRouteFilter
import com.everwell.datagateway.service.UserServiceRestService
import com.netflix.zuul.ExecutionStatus
import com.netflix.zuul.context.RequestContext
import com.netflix.zuul.monitoring.MonitoringHelper
import com.nhaarman.mockitokotlin2.mock
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import org.springframework.mock.web.MockHttpServletRequest
import kotlin.test.assertEquals
import com.nhaarman.mockitokotlin2.any
import com.everwell.datagateway.constants.Constants.CLIENT_ID

@RunWith(MockitoJUnitRunner::class)
class UserServiceZuulFilterTest {

    private lateinit var userServiceZuulFilter: UserServiceZuulRouteFilter
    private val userServiceRestService: UserServiceRestService = mock()

    private val authToken = "abcd1234"

    @Before
    fun before() {
        MockitoAnnotations.initMocks(this)
        MonitoringHelper.initMocks()
        userServiceZuulFilter = UserServiceZuulRouteFilter()
        userServiceZuulFilter.userServiceRestService = userServiceRestService
        Mockito.`when`(userServiceRestService.getAuthToken(any())).thenAnswer { authToken }
        val appProperties = AppProperties()
        userServiceZuulFilter.appProperties = appProperties
    }

    @Test
    fun testZuulRouteFilterSuccess() {
        val request = MockHttpServletRequest("GET", "/userservice/v1/user?id=1")
        request.addHeader(CLIENT_ID, "29")
        val context = RequestContext()
        context.request = request
        RequestContext.testSetCurrentContext(context)
        val result = userServiceZuulFilter.runFilter();
        assertEquals(context["requestURI"], "/v1/user?id=1")
        assertEquals(ExecutionStatus.SUCCESS, result.status)
    }

}