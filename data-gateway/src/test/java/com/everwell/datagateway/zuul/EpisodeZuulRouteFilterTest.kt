package com.everwell.datagateway.zuul

import com.everwell.datagateway.component.AppProperties
import com.everwell.datagateway.entities.Client
import com.everwell.datagateway.exceptions.ValidationException
import com.everwell.datagateway.filters.zuul.episode.EpisodeZuulRouteFilter
import com.everwell.datagateway.service.AuthenticationService
import com.everwell.datagateway.service.ClientService
import com.everwell.datagateway.service.EpisodeServiceRestService
import com.netflix.zuul.ExecutionStatus
import com.netflix.zuul.context.RequestContext
import com.netflix.zuul.monitoring.MonitoringHelper
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import org.springframework.mock.web.MockHttpServletRequest
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import kotlin.test.assertEquals
import kotlin.test.fail

@RunWith(MockitoJUnitRunner::class)
class EpisodeZuulRouteFilterTest {

    private lateinit var episodeZuulRouteFilter: EpisodeZuulRouteFilter
    val episodeRestService: EpisodeServiceRestService = mock()

    private val authToken = "abcd1234"

    @Before
    fun before() {
        MockitoAnnotations.initMocks(this)
        MonitoringHelper.initMocks()
        episodeZuulRouteFilter = EpisodeZuulRouteFilter()
        episodeZuulRouteFilter.episodeRestService = episodeRestService
        Mockito.`when`(episodeRestService.getAuthToken(any())).thenAnswer { authToken }
        val appProperties = AppProperties()
        episodeZuulRouteFilter.appProperties = appProperties
    }

    @Test
    fun testZuulRouteFilterSuccessWithRequestClientId() {
        val request = MockHttpServletRequest("GET", "/episode/v1/episode/1743783")
        request.addHeader("client-id", "29")
        val context = RequestContext()
        context.request = request
        RequestContext.testSetCurrentContext(context)

        val result = episodeZuulRouteFilter.runFilter();
        assertEquals(context["requestURI"], "/v1/episode/1743783")
        assertEquals(ExecutionStatus.SUCCESS, result.status)
    }

}