package com.everwell.datagateway.zuul

import com.everwell.datagateway.entities.Client
import com.everwell.datagateway.entities.EventClient
import com.everwell.datagateway.entities.SubscriberUrl
import com.everwell.datagateway.filters.zuul.APIAccessZuulPreFilter
import com.everwell.datagateway.service.AuthenticationService
import com.everwell.datagateway.service.ClientService
import com.everwell.datagateway.service.EventClientService
import com.everwell.datagateway.service.SubscriberUrlService
import com.netflix.zuul.ExecutionStatus
import com.netflix.zuul.context.RequestContext
import com.netflix.zuul.monitoring.MonitoringHelper
import com.nhaarman.mockitokotlin2.anyOrNull
import com.nhaarman.mockitokotlin2.mock
import org.json.simple.JSONObject
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import org.springframework.mock.web.MockHttpServletRequest
import javax.servlet.http.HttpServletRequest
import kotlin.test.assertEquals


@RunWith(MockitoJUnitRunner.Silent::class)
class APIAccessZuulPreFilterTest {

    private lateinit var APIAccessPreFilter: APIAccessZuulPreFilter


    val clientService : ClientService = mock()
    val authenticationService : AuthenticationService = mock()
    val subscriberUrlService: SubscriberUrlService = mock()
    val eventClientService: EventClientService = mock()

    @Mock
    private val request: HttpServletRequest? = null

    @Before
    fun before() {
        MockitoAnnotations.initMocks(this)
        MonitoringHelper.initMocks()
        APIAccessPreFilter = APIAccessZuulPreFilter()

        APIAccessPreFilter.authenticationService = authenticationService
        APIAccessPreFilter.clientService = clientService
        APIAccessPreFilter.subscriberUrlService = subscriberUrlService
        APIAccessPreFilter.eventClientService = eventClientService

        val subscriberUrl = SubscriberUrl()
        subscriberUrl.eventClient = EventClient()
        subscriberUrl.eventClient.clientId = 77

        val eventClient = EventClient()
        eventClient.id = 16

        val client: Client = Client()
        client.username = "umang"
        client.id = 77

        val successJson = JSONObject()
        successJson.put("status","success")
        val uri = "/umang/tbInformation"
        subscriberUrl.url = uri

        Mockito.`when`(clientService.findByUsername(anyOrNull())).thenAnswer { client }
        Mockito.`when`(authenticationService.getCurrentUsername(anyOrNull())).thenAnswer { "umang" }
        Mockito.`when`(eventClientService.getEventClientByClientId(anyOrNull())).thenAnswer { listOf(eventClient) }
        Mockito.`when`(subscriberUrlService.findUrlsByEventClientId(anyOrNull())).thenAnswer { listOf(subscriberUrl) }
    }

    @Test
    fun testZuulPreFilterSuccess(){
        val request = MockHttpServletRequest("GET", "/umang/tbInformation")
        val context = RequestContext()
        context.setRequest(request)
        RequestContext.testSetCurrentContext(context)
        val result = APIAccessPreFilter.runFilter();
        assertEquals(ExecutionStatus.SUCCESS, result.getStatus());
    }

    @Test
    fun testZuulPreFilterFailure(){
        val request = MockHttpServletRequest("GET", "/umang/unknownAPICall")
        val context = RequestContext()
        context.setRequest(request)
        RequestContext.testSetCurrentContext(context)
        val result = APIAccessPreFilter.runFilter();
        assertEquals(ExecutionStatus.FAILED, result.getStatus());
    }

}