package com.everwell.datagateway.zuul

import com.everwell.datagateway.component.AppProperties
import com.everwell.datagateway.constants.Constants
import com.everwell.datagateway.filters.zuul.nikshay.NikshayZuulRouteFilter
import com.everwell.datagateway.service.NikshayRestService
import com.netflix.zuul.ExecutionStatus
import com.netflix.zuul.context.RequestContext
import com.netflix.zuul.monitoring.MonitoringHelper
import com.nhaarman.mockitokotlin2.mock
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import org.springframework.mock.web.MockHttpServletRequest
import kotlin.test.assertEquals
import com.nhaarman.mockitokotlin2.any

@RunWith(MockitoJUnitRunner::class)
class NikshayZuulRouteFilterTest {
    private lateinit var nikshayZuulRouteFilter: NikshayZuulRouteFilter
    val nikshayRestService: NikshayRestService = mock()

    private val nikshayAuthToken = "abcd1234"

    @Before
    fun before() {
        MockitoAnnotations.initMocks(this)
        MonitoringHelper.initMocks()
        nikshayZuulRouteFilter = NikshayZuulRouteFilter()
        nikshayZuulRouteFilter.nikshayRestService = nikshayRestService
        Mockito.`when`(nikshayRestService.getAuthToken(null)).thenAnswer { nikshayAuthToken }
        val appProperties = AppProperties()
        nikshayZuulRouteFilter.appProperties = appProperties
    }

    @Test
    fun testZuulRouteFilterSuccess(){
        val request = MockHttpServletRequest("GET", "/nikshay/tbInformation")
        val context = RequestContext()
        request.addHeader(Constants.CLIENT_ID, "29")
        context.setRequest(request)
        RequestContext.testSetCurrentContext(context)
        val result = nikshayZuulRouteFilter.runFilter();
        assertEquals(context["requestURI"], "/tbInformation")
        assertEquals(ExecutionStatus.SUCCESS, result.getStatus())
    }
}