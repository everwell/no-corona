package com.everwell.datagateway.zuul

import com.everwell.datagateway.component.AppProperties
import com.everwell.datagateway.constants.Constants
import com.everwell.datagateway.filters.zuul.vot.VotZuulRouteFilter
import com.everwell.datagateway.service.VotService
import com.netflix.zuul.ExecutionStatus
import com.netflix.zuul.context.RequestContext
import com.netflix.zuul.monitoring.MonitoringHelper
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import org.springframework.mock.web.MockHttpServletRequest
import kotlin.test.assertEquals

@RunWith(MockitoJUnitRunner::class)
class VotZuulRouteFilterTest {
    private lateinit var votZuulRouteFilter: VotZuulRouteFilter
    private val votService: VotService = mock()

    private val authToken = "abcd1234"

    @Before
    fun before() {
        MockitoAnnotations.initMocks(this)
        MonitoringHelper.initMocks()
        votZuulRouteFilter = VotZuulRouteFilter()
        votZuulRouteFilter.votService = votService
        var votResponse: MutableMap<String, Any> = mutableMapOf()
        votResponse["token"] = authToken
        Mockito.`when`(votService.getAuthToken(any())).thenAnswer { authToken }
        val appProperties = AppProperties()
        votZuulRouteFilter.appProperties = appProperties
        appProperties.votServerUrl = "https://video-server.dev.everwell.org"
        appProperties.votPassword = "password"
        appProperties.votUsername = "username"
    }

    @Test
    fun testZuulRouteFilterSuccess() {
        val request = MockHttpServletRequest("POST", "/vot/api-token-auth/")
        request.addHeader(Constants.CLIENT_ID, "29")
        val context = RequestContext()
        context.request = request
        RequestContext.testSetCurrentContext(context)
        val result = votZuulRouteFilter.runFilter();
        assertEquals(context["requestURI"], "/api-token-auth/")
    }
}