package com.everwell.datagateway.zuul

import com.everwell.datagateway.filters.zuul.hub.HubZuulRouteFilter
import com.everwell.datagateway.service.HubRestService
import com.netflix.zuul.ExecutionStatus
import com.netflix.zuul.context.RequestContext
import com.netflix.zuul.monitoring.MonitoringHelper
import org.junit.runner.RunWith
import com.nhaarman.mockitokotlin2.mock
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import org.springframework.mock.web.MockHttpServletRequest
import kotlin.test.assertEquals
import com.nhaarman.mockitokotlin2.any

@RunWith(MockitoJUnitRunner::class)
class HubZuulRouteFilterTest {
    private lateinit var hubZuulRouteFilter: HubZuulRouteFilter

    val hubRestService :HubRestService = mock()
    private val hubAuthToken = "abcd1234"

    @Before
    fun before() {
        MockitoAnnotations.initMocks(this)
        MonitoringHelper.initMocks()
        hubZuulRouteFilter = HubZuulRouteFilter()
        hubZuulRouteFilter.hubRestService = hubRestService
        Mockito.`when`(hubRestService.getAuthToken(any())).thenAnswer { hubAuthToken }
    }

    @Test
    fun testZuulRouteFilterSuccess(){
        val request = MockHttpServletRequest("GET", "/hub/missedAdherence")
        val context = RequestContext()
        context.setRequest(request)
        RequestContext.testSetCurrentContext(context)
        val result = hubZuulRouteFilter.runFilter();
        assertEquals(context["requestURI"], "/api/Patients/CurrentMonthMissedAdherence")
        assertEquals(ExecutionStatus.SUCCESS, result.getStatus())
    }

    @Test
    fun testZuulRouteFilterFailure(){
        val request = MockHttpServletRequest("GET", "/hub/unknownAPICall")
        val context = RequestContext()
        context.setRequest(request)
        RequestContext.testSetCurrentContext(context)
        val result = hubZuulRouteFilter.runFilter()
        assertEquals(context["requestURI"], null)
        assertEquals(ExecutionStatus.FAILED, result.getStatus())
    }

}