package com.everwell.datagateway.zuul

import com.everwell.datagateway.component.AppProperties
import com.everwell.datagateway.constants.Constants
import com.everwell.datagateway.entities.Client
import com.everwell.datagateway.filters.zuul.iam.IamZuulRouteFilter
import com.everwell.datagateway.filters.zuul.registry.RegistryZuulRouteFilter
import com.everwell.datagateway.service.AuthenticationService
import com.everwell.datagateway.service.ClientService
import com.everwell.datagateway.service.IamRestService
import com.everwell.datagateway.service.RegistryRestService
import com.netflix.zuul.ExecutionStatus
import com.netflix.zuul.context.RequestContext
import com.netflix.zuul.monitoring.MonitoringHelper
import com.nhaarman.mockitokotlin2.mock
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import org.springframework.mock.web.MockHttpServletRequest
import kotlin.test.assertEquals
import com.nhaarman.mockitokotlin2.any
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder

@RunWith(MockitoJUnitRunner::class)
class RegistryZuulRouteFilterTest {

    private lateinit var registryZuulRouteFilter: RegistryZuulRouteFilter
    val registryRestService: RegistryRestService = mock()
    val clientService: ClientService = mock()
    val authenticationService: AuthenticationService = mock()

    private val authToken = "abcd1234"

    @Before
    fun before() {
        MockitoAnnotations.initMocks(this)
        MonitoringHelper.initMocks()
        registryZuulRouteFilter = RegistryZuulRouteFilter()
        registryZuulRouteFilter.registryRestService = registryRestService
        registryZuulRouteFilter.authenticationService = authenticationService
        registryZuulRouteFilter.clientService = clientService
        Mockito.`when`(registryRestService.getAuthToken(any())).thenAnswer { authToken }
        Mockito.`when`(registryRestService.getUserAccessClientToken(any(), any())).thenAnswer { authToken }
        val appProperties = AppProperties()
        registryZuulRouteFilter.appProperties = appProperties
    }

    @Test
    fun testZuulRouteFilterSuccess() {
        val request = MockHttpServletRequest("GET", "/registry/v1/config/OverviewComponents")
        val clientName = "external-client"
        SecurityContextHolder.getContext().authentication = UsernamePasswordAuthenticationToken(clientName, null)
        val client = Client()
        client.accessibleClient = null
        client.username = clientName

        Mockito.`when`(authenticationService.getCurrentUsername(any())).thenAnswer { clientName }
        Mockito.`when`(clientService.findByUsername(clientName)).thenAnswer { client }
        request.addHeader("client-id", "29")
        val context = RequestContext()
        context.request = request
        RequestContext.testSetCurrentContext(context)
        val result = registryZuulRouteFilter.runFilter()
        assertEquals(context["requestURI"], "/v1/config/OverviewComponents")
        assertEquals(ExecutionStatus.SUCCESS, result.getStatus())
    }

    @Test
    fun testZuulRouteFilterSuccessWithAccessibleClient() {
        val request = MockHttpServletRequest("GET", "/registry/v1/config/OverviewComponents")
        val clientName = "external-client"
        SecurityContextHolder.getContext().authentication = UsernamePasswordAuthenticationToken(clientName, null)
        val client = Client()
        client.accessibleClient = 29
        client.username = clientName

        Mockito.`when`(authenticationService.getCurrentUsername(any())).thenAnswer { clientName }
        Mockito.`when`(clientService.findByUsername(clientName)).thenAnswer { client }
        val context = RequestContext()
        context[Constants.GENERATE_USER_ACCESS_TOKEN] = true
        context.request = request
        RequestContext.testSetCurrentContext(context)
        val result = registryZuulRouteFilter.runFilter()
        assertEquals(context["requestURI"], "/v1/config/OverviewComponents")
        assertEquals(ExecutionStatus.SUCCESS, result.getStatus())
    }

}