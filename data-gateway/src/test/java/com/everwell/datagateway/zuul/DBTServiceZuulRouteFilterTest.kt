package com.everwell.datagateway.zuul

import com.everwell.datagateway.component.AppProperties
import com.everwell.datagateway.filters.zuul.dbtservice.DBTServiceZuulRouteFilter
import com.everwell.datagateway.service.DBTServiceRestService
import com.netflix.zuul.ExecutionStatus
import com.netflix.zuul.context.RequestContext
import com.netflix.zuul.monitoring.MonitoringHelper
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations
import org.springframework.mock.web.MockHttpServletRequest

@RunWith(MockitoJUnitRunner::class)
class DBTServiceZuulRouteFilterTest {

    private lateinit var dbtServiceZuulRouteFilter: DBTServiceZuulRouteFilter
    val dbtServiceRestService: DBTServiceRestService = mock()

    private val authToken = "abcd1234"

    @Before
    fun before() {
        MockitoAnnotations.initMocks(this)
        MonitoringHelper.initMocks()
        dbtServiceZuulRouteFilter = DBTServiceZuulRouteFilter()
        dbtServiceZuulRouteFilter.dbtServiceRestService = dbtServiceRestService
        `when`(dbtServiceRestService.getAuthToken(any())).thenAnswer{ authToken }
        val appProperties = AppProperties()
        dbtServiceZuulRouteFilter.appProperties = appProperties
    }

    @Test
    fun testZuulRouteFilterSuccessWithRequestClientId() {
        val request = MockHttpServletRequest("POST", "/dbtservice/v1/beneficiary/bulk")
        request.addHeader("client-id", "29")
        val context = RequestContext()
        context.request = request
        RequestContext.testSetCurrentContext(context)

        val result = dbtServiceZuulRouteFilter.runFilter()
        assertEquals(context["requestURI"], "/v1/beneficiary/bulk")
        assertEquals(ExecutionStatus.SUCCESS, result.status)
    }
}