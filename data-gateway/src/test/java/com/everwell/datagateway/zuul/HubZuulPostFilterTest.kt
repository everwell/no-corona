package com.everwell.datagateway.zuul

import com.everwell.datagateway.filters.zuul.hub.HubZuulPostFilter
import com.everwell.datagateway.service.HubRestService
import com.netflix.zuul.ExecutionStatus
import com.netflix.zuul.context.RequestContext
import com.netflix.zuul.monitoring.MonitoringHelper
import com.nhaarman.mockitokotlin2.mock
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import org.springframework.cloud.netflix.zuul.filters.ProxyRequestHelper
import org.springframework.cloud.netflix.zuul.filters.ZuulProperties
import org.springframework.http.HttpStatus
import org.springframework.mock.web.MockHttpServletRequest
import org.springframework.mock.web.MockHttpServletResponse
import org.springframework.web.client.RestTemplate
import java.net.URL
import kotlin.test.assertEquals
import com.nhaarman.mockitokotlin2.any

@RunWith(MockitoJUnitRunner::class)
class HubZuulPostFilterTest {
    private lateinit var hubZuulPostFilter: HubZuulPostFilter

    val mockRestTemplate: RestTemplate = mock()
    val mockHubRestService: HubRestService = mock()
    val proxyRequestHelper: ProxyRequestHelper = ProxyRequestHelper(ZuulProperties())
    private val nikshayAuthToken = "abcd1234"

    @Before
    fun before() {
        MockitoAnnotations.initMocks(this)
        MonitoringHelper.initMocks()
        hubZuulPostFilter = HubZuulPostFilter()
        hubZuulPostFilter.helper = proxyRequestHelper
        hubZuulPostFilter.hubRestService = mockHubRestService
        hubZuulPostFilter.restTemplateForApi = mockRestTemplate
        Mockito.`when`(mockHubRestService.getAuthToken(any())).thenAnswer { nikshayAuthToken }
    }


    @Test
    fun testZuulPostFilterForAuthorised(){
        val successBodyString = "Success"
        val request = MockHttpServletRequest("GET", "/hub/missedAdherence?page=0")
        val response = MockHttpServletResponse()
        val context = RequestContext()
        context.setRequest(request)
        context.response = response
        RequestContext.testSetCurrentContext(context)
        context.routeHost = URL("https://beta-hub.everwell.org")
        context["requestURI"] ="/api/Patients/CurrentMonthMissedAdherence?page=0"
        context.setResponseBody(successBodyString)
        context.setResponseStatusCode(HttpStatus.OK.value())
        val result = hubZuulPostFilter.runFilter()
        assertEquals(ExecutionStatus.SUCCESS, result.getStatus())
        assertEquals(context.responseBody, successBodyString)
    }
}