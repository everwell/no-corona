package com.everwell.datagateway.zuul

import com.everwell.datagateway.component.AppProperties
import com.everwell.datagateway.filters.zuul.dispensation.DispensationZuulRouteFilter
import com.everwell.datagateway.service.DispensationRestService
import com.netflix.zuul.ExecutionStatus
import com.netflix.zuul.context.RequestContext
import com.netflix.zuul.monitoring.MonitoringHelper
import com.nhaarman.mockitokotlin2.mock
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import org.springframework.mock.web.MockHttpServletRequest
import kotlin.test.assertEquals
import com.nhaarman.mockitokotlin2.any
import com.everwell.datagateway.constants.Constants.CLIENT_ID

@RunWith(MockitoJUnitRunner::class)
class DispensationZuulRouteFilterTest {

    private lateinit var dispensationZuulRouteFilter: DispensationZuulRouteFilter
    val dispensationRestService: DispensationRestService = mock()

    private val authToken = "abcd1234"

    @Before
    fun before() {
        MockitoAnnotations.initMocks(this)
        MonitoringHelper.initMocks()
        dispensationZuulRouteFilter = DispensationZuulRouteFilter()
        dispensationZuulRouteFilter.dispensationRestService = dispensationRestService
        Mockito.`when`(dispensationRestService.getAuthToken(any())).thenAnswer { authToken }
        val appProperties = AppProperties()
        dispensationZuulRouteFilter.appProperties = appProperties
    }

    @Test
    fun testZuulRouteFilterSuccess() {
        val request = MockHttpServletRequest("GET", "/dispensation/v1/entity/search")
        request.addHeader(CLIENT_ID, "63")
        val context = RequestContext()
        context.setRequest(request)
        RequestContext.testSetCurrentContext(context)
        val result = dispensationZuulRouteFilter.runFilter();
        assertEquals(context["requestURI"], "/v1/entity/search")
        assertEquals(ExecutionStatus.SUCCESS, result.getStatus())
    }

}