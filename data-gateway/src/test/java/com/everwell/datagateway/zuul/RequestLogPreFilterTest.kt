package com.everwell.datagateway.zuul

import com.everwell.datagateway.constants.Constants
import com.everwell.datagateway.constants.Constants.INTERNAL_CLIENT_USERNAME
import com.everwell.datagateway.entities.Client
import com.everwell.datagateway.entities.Event
import com.everwell.datagateway.entities.EventClient
import com.everwell.datagateway.entities.SubscriberUrl
import com.everwell.datagateway.filters.zuul.RequestLogPreFilter
import com.everwell.datagateway.service.*
import com.netflix.zuul.ExecutionStatus
import com.netflix.zuul.context.RequestContext
import com.netflix.zuul.monitoring.MonitoringHelper
import com.nhaarman.mockitokotlin2.any
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import com.nhaarman.mockitokotlin2.mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import org.springframework.mock.web.MockHttpServletRequest
import org.springframework.mock.web.MockHttpServletResponse
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import kotlin.test.assertEquals

@RunWith(MockitoJUnitRunner::class)
class RequestLogPreFilterTest {
    private lateinit var requestLogPreFilter: RequestLogPreFilter

    val clientService: ClientService = mock()

    private val clientRequestsService: ClientRequestsService = mock()

    private val subscriberUrlService: SubscriberUrlService = mock()

    val eventService: EventService = mock()

    val authenticationService: AuthenticationService = mock()

    @Before
    fun before() {
        MockitoAnnotations.initMocks(this)
        MonitoringHelper.initMocks()
        requestLogPreFilter = RequestLogPreFilter()
        requestLogPreFilter.clientService = clientService
        requestLogPreFilter.clientRequestsService = clientRequestsService
        requestLogPreFilter.subscriberUrlService = subscriberUrlService
        requestLogPreFilter.eventService = eventService
        requestLogPreFilter.authenticationService = authenticationService
    }

    @Test
    fun testRun() {
        val url = "/kpi/v1/kpi/data/1"
        val request = MockHttpServletRequest("POST", url)
        val response = MockHttpServletResponse()
        val context = RequestContext()
        context.request = request
        context.response = response

        context.set(Constants.REQUEST_LOG_DATA, mapOf("clientId" to 1L, "eventId" to 1L, "subscriberUrlId" to 1L))

        RequestContext.testSetCurrentContext(context)

        val clientName = "external-client"
        SecurityContextHolder.getContext().authentication = UsernamePasswordAuthenticationToken(clientName, null)
        Mockito.`when`(authenticationService.getCurrentUsername(any())).thenAnswer { clientName }

        val client = Client()
        client.username = clientName
        client.id = 1
        Mockito.`when`(clientService.findByUsername(clientName)).thenAnswer { client }

        val subscriberUrl1 = SubscriberUrl()
        val eventClient1 = EventClient()
        eventClient1.clientId = 2
        val eventId = 1L
        eventClient1.eventId = eventId
        subscriberUrl1.eventClient = eventClient1

        val subscriberUrl2 = SubscriberUrl()
        val eventClient2 = EventClient()
        eventClient2.clientId = client.id
        eventClient2.eventId = eventId
        subscriberUrl2.eventClient = eventClient2

        Mockito.`when`(subscriberUrlService.findByUrl(url)).thenAnswer { listOf(subscriberUrl1, subscriberUrl2) }

        val event = Event()
        Mockito.`when`(eventService.getEventById(eventId)).thenAnswer { event }



        val result = requestLogPreFilter.runFilter()
        assertEquals(ExecutionStatus.SUCCESS, result.status)
        Mockito.verify(clientRequestsService, Mockito.times(1)).saveClientRequest(any())
    }

    @Test
    fun testShouldFilter_InternalClient() {
        val url = "/kpi/v1/kpi/data/1"
        val request = MockHttpServletRequest("POST", url)
        val response = MockHttpServletResponse()
        val context = RequestContext()
        context.request = request
        context.response = response
        RequestContext.testSetCurrentContext(context)

        val clientName = INTERNAL_CLIENT_USERNAME
        SecurityContextHolder.getContext().authentication = UsernamePasswordAuthenticationToken(clientName, null)
        Mockito.`when`(authenticationService.getCurrentUsername(any())).thenAnswer { clientName }

        val result = requestLogPreFilter.runFilter()
        assertEquals(ExecutionStatus.SKIPPED, result.status)
    }
}