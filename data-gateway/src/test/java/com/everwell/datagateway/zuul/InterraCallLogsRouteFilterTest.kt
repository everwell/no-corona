package com.everwell.datagateway.zuul

import com.everwell.datagateway.component.AppProperties
import com.everwell.datagateway.enums.LegacyRoutesEnum
import com.everwell.datagateway.filters.zuul.callLogs.CallLogsZuulRouteFilter
import com.everwell.datagateway.filters.zuul.interraCallLogs.InterraCallLogsZuulRouteFilter
import com.everwell.datagateway.service.RegistryRestService
import com.netflix.zuul.ExecutionStatus
import com.netflix.zuul.context.RequestContext
import com.netflix.zuul.monitoring.MonitoringHelper
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import org.springframework.http.HttpMethod
import org.springframework.mock.web.MockHttpServletRequest
import org.springframework.web.client.RestTemplate
import java.net.URL
import kotlin.test.assertEquals

@RunWith(MockitoJUnitRunner::class)
class InterraCallLogsRouteFilterTest {

    private lateinit var interraCallLogsZuulRouteFilter: InterraCallLogsZuulRouteFilter
    val registryRestService: RegistryRestService = mock()

    val mockRestTemplate: RestTemplate = mock()
    private val authToken = "abcd1234"

    @Before
    fun before() {
        MockitoAnnotations.initMocks(this)
        MonitoringHelper.initMocks()
        interraCallLogsZuulRouteFilter = InterraCallLogsZuulRouteFilter()
        interraCallLogsZuulRouteFilter.registryRestService = registryRestService
        Mockito.`when`(registryRestService.getAuthToken(any())).thenAnswer { authToken }
        interraCallLogsZuulRouteFilter.restTemplateForApi = mockRestTemplate
    }

    @Test
    fun testZuulRouteFilterSuccessTestFetchClientIdFromRegistry() {
        val appProperties = AppProperties()
        appProperties.registryServerUrl = "http://localhost:9210"
        appProperties.clientIdInterraCallLogs = "63"
        interraCallLogsZuulRouteFilter.appProperties = appProperties
        val request = MockHttpServletRequest(HttpMethod.POST.name, LegacyRoutesEnum.INTERRA_CALL_LOGS.path)
        val context = RequestContext()
        context.setRequest(request)
        context.routeHost = URL("http://localhost:9210")
        RequestContext.testSetCurrentContext(context)
        val result = interraCallLogsZuulRouteFilter.runFilter();
        assertEquals(context["requestURI"], "/v1/deployment-call-back")
        assertEquals(ExecutionStatus.SUCCESS, result.getStatus())
    }

}