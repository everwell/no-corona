package com.everwell.datagateway.service

import com.everwell.datagateway.BaseTest
import com.everwell.datagateway.component.AppProperties
import com.everwell.datagateway.constants.Constants
import com.everwell.datagateway.models.request.KpiDataRequest
import com.everwell.datagateway.models.response.ApiResponse
import com.everwell.datagateway.models.response.KpiDataResponse
import com.everwell.datagateway.models.response.MicroServiceGenericAuthResponse
import com.nhaarman.mockitokotlin2.*
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.util.LinkedMultiValueMap
import org.springframework.web.client.RestTemplate
import java.util.*

@RunWith(MockitoJUnitRunner::class)
class KpiRestServiceTest : BaseTest() {

    @InjectMocks
    lateinit var kpiRestService: KpiRestServiceImpl

    @Test
    fun testAuthentication() {
        val appProperties = AppProperties()
        appProperties.kpiServerUrl = "http://localhost:9310"

        val restTemplate : RestTemplate = mock()
        val restService : RestService = mock()

        kpiRestService.restService = restService
        kpiRestService.restTemplateForAuthentication = restTemplate
        kpiRestService.appProperties = appProperties

        val genericAuthResponseMock = ApiResponse(
                "true", MicroServiceGenericAuthResponse(1L, "kpi", "auth_token", 1234L), "");
        val responseEntity = ResponseEntity(genericAuthResponseMock, HttpStatus.OK)
        val headers = mutableMapOf<String, String>()
        headers["X-Client-Id"] = "29"
        val params: LinkedMultiValueMap<String, String> = LinkedMultiValueMap()
        Mockito.`when`(restTemplate.exchange(appProperties.kpiServerUrl + "/v1/client", HttpMethod.GET, kpiRestService.getHttpEntity(headers, params), object : ParameterizedTypeReference<ApiResponse<MicroServiceGenericAuthResponse>>() {}))
                .thenAnswer { responseEntity }
        val genericAuthResponse = kpiRestService.getAuthToken("29")
        Assert.assertEquals(genericAuthResponseMock.data?.authToken, genericAuthResponse);
    }

    @Test
    fun testAuthentication_NoMultipleApiCalls() {
        val appProperties = AppProperties()
        appProperties.kpiServerUrl = "http://localhost:9310"

        val restTemplate : RestTemplate = mock()
        val restService : RestService = mock()

        kpiRestService.restService = restService
        kpiRestService.restTemplateForAuthentication = restTemplate
        kpiRestService.appProperties = appProperties

        val genericAuthResponseMock = ApiResponse(
                "true", MicroServiceGenericAuthResponse(1L, "kpi", "auth_token", Date().time + 2 * 3600 * 1000), "");
        val responseEntity = ResponseEntity(genericAuthResponseMock, HttpStatus.OK)
        val headers = mutableMapOf<String, String>()
        headers["X-Client-Id"] = "29"
        val params: LinkedMultiValueMap<String, String> = LinkedMultiValueMap()
        Mockito.`when`(restTemplate.exchange(appProperties.kpiServerUrl + "/v1/client", HttpMethod.GET, kpiRestService.getHttpEntity(headers, params), object : ParameterizedTypeReference<ApiResponse<MicroServiceGenericAuthResponse>>() {}))
                .thenAnswer { responseEntity }
        val genericAuthResponse1 = kpiRestService.getAuthToken("29")
        Assert.assertEquals(genericAuthResponseMock.data?.authToken, genericAuthResponse1)

        val genericAuthResponse2 = kpiRestService.getAuthToken("29")
        Assert.assertEquals(genericAuthResponseMock.data?.authToken, genericAuthResponse2)

        Mockito.verify(restTemplate, times(1)).exchange(appProperties.kpiServerUrl + "/v1/client", HttpMethod.GET, kpiRestService.getHttpEntity(headers, params), object : ParameterizedTypeReference<ApiResponse<MicroServiceGenericAuthResponse>>() {})
    }

    @Test
    fun testAuthentication_MultipleApiCallsForExpiredToken() {
        val appProperties = AppProperties()
        appProperties.kpiServerUrl = "http://localhost:9310"

        val restTemplate : RestTemplate = mock()
        val restService : RestService = mock()

        kpiRestService.restService = restService
        kpiRestService.restTemplateForAuthentication = restTemplate
        kpiRestService.appProperties = appProperties

        val genericAuthResponseMock = ApiResponse(
                "true", MicroServiceGenericAuthResponse(1L, "kpi", "auth_token", Date().time - 2 * 3600 * 1000), "");
        val responseEntity = ResponseEntity(genericAuthResponseMock, HttpStatus.OK)
        val headers = mutableMapOf<String, String>()
        headers["X-Client-Id"] = "29"
        val params: LinkedMultiValueMap<String, String> = LinkedMultiValueMap()
        Mockito.`when`(restTemplate.exchange(appProperties.kpiServerUrl + "/v1/client", HttpMethod.GET, kpiRestService.getHttpEntity(headers, params), object : ParameterizedTypeReference<ApiResponse<MicroServiceGenericAuthResponse>>() {}))
                .thenAnswer { responseEntity }
        val genericAuthResponse1 = kpiRestService.getAuthToken("29")
        Assert.assertEquals(genericAuthResponseMock.data?.authToken, genericAuthResponse1)

        val genericAuthResponse2 = kpiRestService.getAuthToken("29")
        Assert.assertEquals(genericAuthResponseMock.data?.authToken, genericAuthResponse2)

        Mockito.verify(restTemplate, times(2)).exchange(appProperties.kpiServerUrl + "/v1/client", HttpMethod.GET, kpiRestService.getHttpEntity(headers, params), object : ParameterizedTypeReference<ApiResponse<MicroServiceGenericAuthResponse>>() {})
    }

    @Test
    fun getKPIDataTest() {
        val appProperties = AppProperties()
        appProperties.kpiServerUrl = "http://localhost:9310"

        val restTemplate : RestTemplate = mock()
        val restService : RestService = mock()

        kpiRestService.restService = restService
        kpiRestService.restTemplateForAuthentication = restTemplate
        kpiRestService.appProperties = appProperties
        val genericAuthResponseMock = ApiResponse(
                "true", MicroServiceGenericAuthResponse(1L, "kpi", "auth_token", Date().time - 2 * 3600 * 1000), "");
        val getTokenResponseEntity = ResponseEntity(genericAuthResponseMock, HttpStatus.OK)
        val getTokenHeaders = mutableMapOf<String, String>()
        getTokenHeaders["X-Client-Id"] = "1"
        val getTokenParams: LinkedMultiValueMap<String, String> = LinkedMultiValueMap()
        val response = ApiResponse<KpiDataResponse>(
                "true", KpiDataResponse(), "");
        val responseEntity = ResponseEntity(response, HttpStatus.OK)
        val headers = mutableMapOf<String, String>()
        headers[Constants.AUTHORIZATION] = Constants.BEARER + " " + "auth_token"
        val body = KpiDataRequest(mutableMapOf<String, Any>())
        Mockito.`when`(restTemplate.exchange(appProperties.kpiServerUrl + "/v1/client", HttpMethod.GET, kpiRestService.getHttpEntity(getTokenHeaders, getTokenParams), object : ParameterizedTypeReference<ApiResponse<MicroServiceGenericAuthResponse>>() {}))
                .thenAnswer { getTokenResponseEntity }
        val responseType: ParameterizedTypeReference<ApiResponse<KpiDataResponse>> =
                object:  ParameterizedTypeReference<ApiResponse<KpiDataResponse>>() {}
        Mockito.`when`(restTemplate.exchange(eq(appProperties.kpiServerUrl + "/v1/kpi/data/1"), eq(HttpMethod.POST), anyVararg(), eq(responseType)))
                .thenAnswer { responseEntity }
        val kpiDataResponse = kpiRestService.getKPIData(KpiDataRequest(mutableMapOf<String, Any>()), 1,"1" )
        Assert.assertEquals(kpiDataResponse.success, "true")
    }
}