package com.everwell.datagateway.service;

import com.everwell.datagateway.BaseTest;
import com.everwell.datagateway.entities.Client;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class UserDetailsServiceImplTest extends BaseTest {

    @Mock
    ClientService clientService;

    @InjectMocks
    UserDetailsServiceImpl userDetailsService;

    @Test
    void loadUserByUsername() {
        //For invalid or null input throw exception
         assertThrows(UsernameNotFoundException.class, () -> userDetailsService.loadUserByUsername(null));
    }

    @Test
    void loadByUsernameValidInput() {
        Client client = new Client();
        client.setUsername("junit");
        client.setPassword("junit");
        when(clientService.findByUsername(any())).thenReturn(client);
        UserDetails userDetails = userDetailsService.loadUserByUsername("junit");
        assertEquals(client.getUsername(), userDetails.getUsername());
    }
}