package com.everwell.datagateway.service;

import com.everwell.datagateway.BaseTest;
import com.everwell.datagateway.exceptions.RestTemplateException;
import com.everwell.datagateway.models.request.AbdmClientDto.HipOnRequestDto;
import com.everwell.datagateway.models.request.abdm.ABDMConsentInitiationRequest;
import com.everwell.datagateway.service.impl.ABDMHelperServiceImpl;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

public class ABDMHelperServiceImplTest extends BaseTest {

    @InjectMocks
    ABDMHelperServiceImpl abdmHelperService;

    @Mock
    RestTemplate client;

    @Test
    void exchangeTest() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("X-CM-ID","1");
        when(client.postForEntity(anyString(),any(),any())).thenReturn(new ResponseEntity<>("data", HttpStatus.ACCEPTED));
        HttpEntity<String> request = new HttpEntity<>("data", headers);
        Map<Integer,String> restServiceResponse = abdmHelperService.exchange("https://www.google.com/",request);
        assertEquals(restServiceResponse.entrySet().iterator().next().getKey(), 202);
    }

    @Test
    void getTokenTest() {
        Map<String,String> response = new HashMap<>();
        response.put("accessToken","abcd");
        when(client.postForEntity(anyString(),any(),any())).thenReturn(new ResponseEntity<>(response, HttpStatus.ACCEPTED));
        abdmHelperService.getToken();
    }

    @Test
    void hipOnRequestCallTest() {
        Map<String,String> response = new HashMap<>();
        response.put("accessToken","abcd");
        when(client.postForEntity(anyString(),any(),any())).thenReturn(new ResponseEntity<>(response, HttpStatus.ACCEPTED));
        abdmHelperService.hipOnRequestCall(new HipOnRequestDto("test","test",null));
    }

    @Test
    void hipOnRequestCallTest_Unauthorised() {
        Map<String,String> response = new HashMap<>();
        response.put("accessToken","abcd");
        when(client.postForEntity(anyString(),any(),any())).thenReturn(new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED));
        abdmHelperService.hipOnRequestCall(new HipOnRequestDto("test","test",null));
    }

    @Test()
     void hipOnRequestCallTest_Exception() {
        Map<String,String> response = new HashMap<>();
        response.put("accessToken","abcd");
        when(client.postForEntity(anyString(),any(),any())).thenReturn(new ResponseEntity<>(response, HttpStatus.BAD_REQUEST));
        assertThrows(RestTemplateException.class,() -> abdmHelperService.hipOnRequestCall(new HipOnRequestDto("test","test",null)));
    }

    @Test
    void pushToCallBackUrlTest() {
        Map<String,String> response = new HashMap<>();
        response.put("accessToken","abcd");
        when(client.postForEntity(anyString(),any(),any())).thenReturn(new ResponseEntity<>(response, HttpStatus.ACCEPTED));
        abdmHelperService.pushToCallBackUrl("https://www.google.com","test");
    }

    @Test()
    void pushToCallBackUrl_Exception() {
        when(client.postForEntity(anyString(),any(),any())).thenReturn(new ResponseEntity<>("error", HttpStatus.BAD_REQUEST));
        assertThrows(RestTemplateException.class,() -> abdmHelperService.pushToCallBackUrl("https://www.google.com","test"));
    }

    @Test
    void hipHiTransferNotifyTest() {
        Map<String,String> response = new HashMap<>();
        response.put("accessToken","abcd");
        when(client.postForEntity(anyString(),any(),any())).thenReturn(new ResponseEntity<>(response, HttpStatus.ACCEPTED));
        abdmHelperService.hipHiTransferNotify("Test");
    }

    @Test
    void hipHiTransferNotifyTest_Unauthorised() {
        Map<String,String> response = new HashMap<>();
        response.put("accessToken","abcd");
        when(client.postForEntity(anyString(),any(),any())).thenReturn(new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED));
        abdmHelperService.hipHiTransferNotify("Test");
    }

    @Test()
    void hipHiTransferNotifyTest_Exception() {
        Map<String,String> response = new HashMap<>();
        response.put("accessToken","abcd");
        when(client.postForEntity(anyString(),any(),any())).thenReturn(new ResponseEntity<>(response, HttpStatus.BAD_REQUEST));
        assertThrows(RestTemplateException.class,() -> abdmHelperService.hipHiTransferNotify("Test"));
    }

    @Test
    void consentRequestInitTest() {
        Map<String,String> response = new HashMap<>();
        response.put("accessToken","abcd");
        when(client.postForEntity(anyString(),any(),any())).thenReturn(new ResponseEntity<>(response, HttpStatus.ACCEPTED));
        ABDMConsentInitiationRequest abdmConsentInitiationRequest = new ABDMConsentInitiationRequest();
        abdmHelperService.consentRequestInit(abdmConsentInitiationRequest);
    }

    @Test
    void consentRequestInitTest_Unauthorised() {
        Map<String,String> response = new HashMap<>();
        response.put("accessToken","abcd");
        when(client.postForEntity(anyString(),any(),any())).thenReturn(new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED));
        ABDMConsentInitiationRequest abdmConsentInitiationRequest = new ABDMConsentInitiationRequest();
        abdmHelperService.consentRequestInit(abdmConsentInitiationRequest);
    }

    @Test()
    void consentRequestInitTest_Exception() {
        Map<String,String> response = new HashMap<>();
        response.put("accessToken","abcd");
        when(client.postForEntity(anyString(),any(),any())).thenReturn(new ResponseEntity<>(response, HttpStatus.BAD_REQUEST));
        ABDMConsentInitiationRequest abdmConsentInitiationRequest = new ABDMConsentInitiationRequest();
        assertThrows(RestTemplateException.class,() -> abdmHelperService.consentRequestInit(abdmConsentInitiationRequest));
    }

}
