package com.everwell.datagateway.service

import com.everwell.datagateway.component.AppProperties
import com.everwell.datagateway.exceptions.ValidationException
import com.everwell.datagateway.models.response.GenericAuthResponse
import com.nhaarman.mockitokotlin2.mock
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.HttpEntity
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.util.LinkedMultiValueMap
import org.springframework.web.client.RestTemplate
import kotlin.reflect.jvm.internal.impl.load.kotlin.JvmType

@RunWith(MockitoJUnitRunner.Silent::class)
class SmartPaymentsRestServiceTest {
    private var smartPaymentsRestServiceImp: SmartPaymentsRestServiceImp = SmartPaymentsRestServiceImp()


    @Test
    fun testAuthentication(){
        val clientService : ClientService = mock()
        val restTemplate : RestTemplate = mock()
        val restService : RestService = mock()
        val httpEntity : HttpEntity<JvmType.Object> = mock()
        val authenticationService : AuthenticationService = mock()

        val appProperties = AppProperties()
        appProperties.smartPaymentsUrl = "http://localhost:8072"
        appProperties.smartPaymentUserGrantType = "client_credentials"
        appProperties.smartPaymentsClientIdMap = mapOf("29" to "eef7b53e-42bf-490a-8421-a12a12b21212")
        appProperties.smartPaymentsClientIdSecretMap = mapOf("29" to "TEST_PWD_123")
        smartPaymentsRestServiceImp.appProperties = appProperties
        smartPaymentsRestServiceImp.restService = restService
        smartPaymentsRestServiceImp.restTemplateForAuthentication = restTemplate
        smartPaymentsRestServiceImp.clientService = clientService
        smartPaymentsRestServiceImp.authenticationService = authenticationService

        val genericAuthResponseMock = GenericAuthResponse("abcded1234", null)
        val responseEntity = ResponseEntity(genericAuthResponseMock, HttpStatus.OK)
        Mockito.`when`(restService.buildHeaderAndRequest(ArgumentMatchers.anyMap())).thenAnswer { httpEntity }
        Mockito.`when`(restTemplate.postForObject("http://localhost:8072/oauth2/token", httpEntity, GenericAuthResponse::class.java)).thenAnswer{genericAuthResponseMock}
        val params: LinkedMultiValueMap<String, String> = LinkedMultiValueMap()
        params["grant_type"] = appProperties.smartPaymentUserGrantType
        params["client_id"] = "eef7b53e-42bf-490a-8421-a12a12b21212"
        params["client_secret"] = "TEST_PWD_123"
        Mockito.`when`(restTemplate.exchange(appProperties.smartPaymentsUrl + "/oauth2/token", HttpMethod.POST, smartPaymentsRestServiceImp.getHttpEntity(HashMap(), params), object : ParameterizedTypeReference<GenericAuthResponse>() {}))
            .thenAnswer { responseEntity }
        val genericAuthResponse = smartPaymentsRestServiceImp.getAuthToken("29")
        Assert.assertEquals(genericAuthResponseMock.access_token, genericAuthResponse);
    }

}