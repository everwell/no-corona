package com.everwell.datagateway.service;

import com.everwell.datagateway.BaseTest;
import com.everwell.datagateway.apiHelpers.ABDMHelper;
import com.everwell.datagateway.component.AppProperties;
import com.everwell.datagateway.entities.Client;
import com.everwell.datagateway.entities.ClientRequests;
import com.everwell.datagateway.models.dto.ConsentFetchRedisDto;
import com.everwell.datagateway.entities.Event;
import com.everwell.datagateway.models.dto.LinkConfirmRedisDto;
import com.everwell.datagateway.models.request.AbdmClientDto.DataPullRequest;
import com.everwell.datagateway.models.request.AbdmClientDto.DataTransferRequest;
import com.everwell.datagateway.models.request.abdm.*;
import com.everwell.datagateway.models.request.abdm.ABDMLinkInitiationRequest.*;
import com.everwell.datagateway.models.response.ApiResponse;
import com.everwell.datagateway.models.response.GetEpisodeByPersonResponse;
import com.everwell.datagateway.models.response.GetRecordsByTxnIdResponse;
import com.everwell.datagateway.models.response.LinkInitiationOtpResponse;
import com.everwell.datagateway.publishers.RMQPublisher;
import com.everwell.datagateway.utils.CacheUtils;
import com.everwell.datagateway.utils.Utils;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;


import java.util.Collections;
import java.util.Date;

import static com.everwell.datagateway.constants.Constants.ABDM_DATA_REQUEST_CACHE_PREFIX;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

public class ABDMIntegrationServiceTest extends BaseTest {

    @Spy
    @InjectMocks
    ABDMIntegrationService abdmIntegrationService;

    @InjectMocks
    CacheUtils cacheUtils;

    @Mock
    RedisTemplate<String, Object> redisTemplate;

    @Mock
    private ValueOperations valueOperations;

    @Mock
    private EventService eventService;

    @Mock
    private AuthenticationService authenticationService;

    @Mock
    private ClientService clientService;

    @Mock
    private ClientRequestsService clientRequestsService;

    @Mock
    private ABDMHelper abdmHelper;

    @Mock
    private NikshayRestService nikshayRestService;

    @Mock
    private RMQPublisher rmqPublisher;

    @Mock
    private EpisodeServiceRestService episodeServiceRestService;

    @Mock
    private AppProperties appProperties;

    @Mock
    private UserServiceRestService userServiceRestService;

    public void init() {
        cacheUtils = new CacheUtils(redisTemplate);
        when(redisTemplate.opsForValue()).thenReturn(valueOperations);
    }

    @Test
    public void processLinkInitiationRequestTest() throws JsonProcessingException {
        init();
        ABDMLinkInitiationRequest abdmLinkInitiationRequest = new ABDMLinkInitiationRequest();
        abdmLinkInitiationRequest.setTransactionId("test");
        Patient patient = new Patient();
        patient.setReferenceNumber("58420");
        CareContext careContext = new CareContext();
        careContext.setReferenceNumber("10724488");
        patient.setCareContexts(Collections.singletonList(careContext));
        abdmLinkInitiationRequest.setPatient(patient);
        when(redisTemplate.opsForValue().get(eq("ON_DISCOVERY_REQUEST:test"))).thenReturn(Utils.asJsonString(getRedisDto().getAbdmOnDiscoveryRequest()));
        when(cacheUtils.hasKey(eq("ON_DISCOVERY_REQUEST:test"))).thenReturn(true);
        when(nikshayRestService.getAuthToken(any())).thenReturn("token");
        when(nikshayRestService.sendOtpLinkInitiation(any(),any())).thenReturn(new ApiResponse<>("true",new LinkInitiationOtpResponse("453652","Otp sent"),null));
        when(abdmHelper.OnLinkInit(any())).thenReturn(true);
        when(clientRequestsService.getClientRequestsById(any())).thenReturn(new ClientRequests());
        when(eventService.findEventByEventName(any())).thenReturn(new Event());
        when(eventService.findEventByEventName(any())).thenReturn(new Event());
        when(clientService.findByUsername(any())).thenReturn(new Client());
        abdmIntegrationService.processLinkInitiationRequest(abdmLinkInitiationRequest);
        Mockito.verify(nikshayRestService,Mockito.times(1)).getAuthToken(any());
        Mockito.verify(nikshayRestService,Mockito.times(1)).sendOtpLinkInitiation(any(),any());
    }

    @Test
    public void processLinkConfirmationRequestTest() {
        init();
        ABDMLinkConfirmationRequest abdmLinkConfirmationRequest = new ABDMLinkConfirmationRequest();
        abdmLinkConfirmationRequest.setConfirmation(new ABDMLinkConfirmationRequest.Confirmation("58420","123564"));
        when(cacheUtils.hasKey(eq("LINK_INITIATION_REQUEST:58420"))).thenReturn(true);
        when(redisTemplate.opsForValue().get(eq("LINK_INITIATION_REQUEST:58420"))).thenReturn(Utils.asJsonString(getRedisDto()));
        when(episodeServiceRestService.updateEpisodeDetails(any(),any())).thenReturn(new ApiResponse<>("true",new GetEpisodeByPersonResponse(1234,new Date().toString()),null));
        when(episodeServiceRestService.getAuthToken(any())).thenReturn("134");
        when(abdmHelper.OnLinkConfirm(any())).thenReturn(true);
        when(clientRequestsService.getClientRequestsById(any())).thenReturn(new ClientRequests());
        when(eventService.findEventByEventName(any())).thenReturn(new Event());
        when(clientService.findByUsername(any())).thenReturn(new Client());
        abdmIntegrationService.processLinkConfirmationRequest(abdmLinkConfirmationRequest);
    }

    @Test
    public void processLinkConfirmationRequestTest_cacheKeyNotFound() {
        ABDMLinkConfirmationRequest abdmLinkConfirmationRequest = new ABDMLinkConfirmationRequest();
        abdmLinkConfirmationRequest.setConfirmation(new ABDMLinkConfirmationRequest.Confirmation("58420","123564"));
        when(cacheUtils.hasKey(eq("LINK_INITIATION_REQUEST:58420"))).thenReturn(false);
        when(abdmHelper.OnLinkConfirm(any())).thenReturn(true);
        when(clientRequestsService.getClientRequestsById(any())).thenReturn(new ClientRequests());
        when(eventService.findEventByEventName(any())).thenReturn(new Event());
        when(clientService.findByUsername(any())).thenReturn(new Client());
        abdmIntegrationService.processLinkConfirmationRequest(abdmLinkConfirmationRequest);
    }

    @Test
    public void processLinkConfirmationRequestTest_invalidOtp() {
        init();
        ABDMLinkConfirmationRequest abdmLinkConfirmationRequest = new ABDMLinkConfirmationRequest();
        abdmLinkConfirmationRequest.setConfirmation(new ABDMLinkConfirmationRequest.Confirmation("58420","123564"));
        when(cacheUtils.hasKey(eq("LINK_INITIATION_REQUEST:58420"))).thenReturn(true);
        when(redisTemplate.opsForValue().get(eq("LINK_INITIATION_REQUEST:58420"))).thenReturn(Utils.asJsonString(getRedisDto()));
        when(episodeServiceRestService.getAuthToken(any())).thenReturn(null);
        when(abdmHelper.OnLinkConfirm(any())).thenReturn(true);
        when(clientRequestsService.getClientRequestsById(any())).thenReturn(new ClientRequests());
        when(eventService.findEventByEventName(any())).thenReturn(new Event());
        when(clientService.findByUsername(any())).thenReturn(new Client());
        abdmIntegrationService.processLinkConfirmationRequest(abdmLinkConfirmationRequest);
    }

    @Test
    public void processLinkConfirmationRequestTest_invalidToken() {
        init();
        ABDMLinkConfirmationRequest abdmLinkConfirmationRequest = new ABDMLinkConfirmationRequest();
        abdmLinkConfirmationRequest.setConfirmation(new ABDMLinkConfirmationRequest.Confirmation("58420","123565"));
        when(cacheUtils.hasKey(eq("LINK_INITIATION_REQUEST:58420"))).thenReturn(true);
        when(redisTemplate.opsForValue().get(eq("LINK_INITIATION_REQUEST:58420"))).thenReturn(Utils.asJsonString(getRedisDto()));
        when(abdmHelper.OnLinkConfirm(any())).thenReturn(true);
        when(clientRequestsService.getClientRequestsById(any())).thenReturn(new ClientRequests());
        when(eventService.findEventByEventName(any())).thenReturn(new Event());
        when(clientService.findByUsername(any())).thenReturn(new Client());
        abdmIntegrationService.processLinkConfirmationRequest(abdmLinkConfirmationRequest);
    }

    @Test
    public void processLinkConfirmationRequestTest_throwException() {
        init();
        ABDMLinkConfirmationRequest abdmLinkConfirmationRequest = new ABDMLinkConfirmationRequest();
        abdmLinkConfirmationRequest.setConfirmation(new ABDMLinkConfirmationRequest.Confirmation("58420","123565"));
        when(cacheUtils.hasKey(eq("LINK_INITIATION_REQUEST:58420"))).thenReturn(true);
        when(redisTemplate.opsForValue().get(eq("LINK_INITIATION_REQUEST:58420"))).thenThrow(NullPointerException.class);
        when(abdmHelper.OnLinkConfirm(any())).thenReturn(true);
        when(clientRequestsService.getClientRequestsById(any())).thenReturn(new ClientRequests());
        when(eventService.findEventByEventName(any())).thenReturn(new Event());
        when(clientService.findByUsername(any())).thenReturn(new Client());
        abdmIntegrationService.processLinkConfirmationRequest(abdmLinkConfirmationRequest);
    }

    LinkConfirmRedisDto getRedisDto() {
        LinkConfirmRedisDto linkConfirmRedisDto = new LinkConfirmRedisDto();
        ABDMOnDiscoveryRequest abdmOnDiscoveryRequest = new ABDMOnDiscoveryRequest();
        abdmOnDiscoveryRequest.setPatient(new ABDMCareContextDTO("58420","10724488","test",new Date().toString()));
        linkConfirmRedisDto.setAbdmOnDiscoveryRequest(abdmOnDiscoveryRequest);
        linkConfirmRedisDto.setOtp("123564");
        linkConfirmRedisDto.setAbhaId("911-823-85969");
        linkConfirmRedisDto.setAbhaAddress("test@sbx");
        return linkConfirmRedisDto;
    }

    @Test
    public void processConsentInitiationTest() {
        init();
        ABDMConsentRequestOnInitiation abdmConsentRequestOnInitiation = new ABDMConsentRequestOnInitiation();
        ABDMConsentRequestOnInitiation.Resp resp = new ABDMConsentRequestOnInitiation.Resp("3847cbb1-1302-47b5-a8ff-777e427ce38c");
        abdmConsentRequestOnInitiation.setConsentRequest(new ABDMConsentRequestOnInitiation.ConsentRequest("45cd0c41-c502-4ab3-acaa-3ee87773e362"));
        abdmConsentRequestOnInitiation.setResp(resp);
        when(cacheUtils.hasKey(eq("CONSENT_INITIATION_REQUEST:3847cbb1-1302-47b5-a8ff-777e427ce38c"))).thenReturn(true);
        when(redisTemplate.opsForValue().get(eq("CONSENT_INITIATION_REQUEST:3847cbb1-1302-47b5-a8ff-777e427ce38c"))).thenReturn(Utils.asJsonString(getRedisDto()));
        doNothing().when(rmqPublisher).send(any(),any(),any(),any());
        when(eventService.findEventByEventName(any())).thenReturn(new Event());
        when(clientService.findByUsername(any())).thenReturn(new Client());
        abdmIntegrationService.processConsentInitiation(abdmConsentRequestOnInitiation);
    }
    @Test
    public void addCareContextDirectAuthTest() throws JsonProcessingException {
        init();
        ABDMAuthNotifyRequest abdmAuthNotifyRequest = Utils.convertStrToObject(getAuthNotifyJson(),ABDMAuthNotifyRequest.class);
        when(redisTemplate.opsForValue().get(eq("19d38558-1ef6-44c5-bc0e-6f8feee50f7a"))).thenReturn("kmk@sbx");
        when(redisTemplate.opsForValue().get(eq("kmk@sbx"))).thenReturn(Utils.asJsonString(Utils.convertStrToObject(getABDMLinkCareContextEvent(),ABDMLinkCareContextEvent.class)));
        doNothing().when(abdmHelper).authOnNotify(any());
        when(abdmHelper.addCareContextRequest(any())).thenReturn("");
        Assertions.assertTrue(abdmIntegrationService.addCareContextDirectAuth(abdmAuthNotifyRequest));
    }

    @Test
    public void notifyCareContextTest() {
        ABDMNotifyCareContextEvent abdmNotifyCareContextEvent = new ABDMNotifyCareContextEvent(1L,1L,"Prescription","Test","Test@sbx","NIKSHAY-HIP");
        when(episodeServiceRestService.updateEpisodeDetails(any(),any())).thenReturn(new ApiResponse<>("true",new GetEpisodeByPersonResponse(1234,new Date().toString()),null));
        when(episodeServiceRestService.getAuthToken(any())).thenReturn("134");
        when(abdmHelper.notifyCareContext(any())).thenReturn("");
        Assertions.assertTrue(abdmIntegrationService.notifyCareContext(abdmNotifyCareContextEvent));
    }

    @Test
    public void processConsentNotificationTest() throws JsonProcessingException {
        ABDMConsentNotification abdmConsentNotification = Utils.convertStrToObject(getConsentNotifyJson(),ABDMConsentNotification.class);
        when(eventService.findEventByEventName(any())).thenReturn(new Event());
        when(clientService.findByUsername(any())).thenReturn(new Client());
        abdmIntegrationService.processConsentNotification(abdmConsentNotification);
    }

    @Test
    public void dataPullRequestInitiationTest() throws Exception {
        init();
        ABDMOnConsentFetchRequest abdmOnConsentFetchRequest = Utils.convertStrToObject(getConsentArtefactJson(),ABDMOnConsentFetchRequest.class);
        when(eventService.findEventByEventName(any())).thenReturn(new Event());
        when(clientService.findByUsername(any())).thenReturn(new Client());
        abdmIntegrationService.dataPullRequestInitiation(abdmOnConsentFetchRequest);
    }

    @Test
    public void processOnHIDataRequestTest_RequestIdNotCached() throws Exception {
        ABDMOnHIDataRequest abdmOnHIDataRequest = Utils.convertStrToObject(getHIOnDataRequestJson(),ABDMOnHIDataRequest.class);
        when(eventService.findEventByEventName(any())).thenReturn(new Event());
        when(clientService.findByUsername(any())).thenReturn(new Client());
        boolean success = abdmIntegrationService.processOnHIDataRequest(abdmOnHIDataRequest);
        Assertions.assertFalse(success);
    }

    @Test
    public void processOnHIDataRequestTest_Success() throws Exception {
        init();
        ABDMOnHIDataRequest abdmOnHIDataRequest = Utils.convertStrToObject(getHIOnDataRequestJson(),ABDMOnHIDataRequest.class);
        when(cacheUtils.hasKey(eq(ABDM_DATA_REQUEST_CACHE_PREFIX +"d15a82ae-427d-4c70-9e9d-7c0d6b1cbdaa"))).thenReturn(true);
        when(redisTemplate.opsForValue().get(eq(ABDM_DATA_REQUEST_CACHE_PREFIX +"d15a82ae-427d-4c70-9e9d-7c0d6b1cbdaa"))).thenReturn(Utils.asJsonString(new ConsentFetchRedisDto(new HIDataRequest("4491ea54-43b8-4813-8c8a-8fb72eab0974",null,null,null,null,null,null),"NIKSHAY-HIP","")));
        when(eventService.findEventByEventName(any())).thenReturn(new Event());
        when(clientService.findByUsername(any())).thenReturn(new Client());
        boolean success = abdmIntegrationService.processOnHIDataRequest(abdmOnHIDataRequest);
        Assertions.assertTrue(success);
    }

    @Test
    public void processHITransferTest_Success() throws Exception {
        DataPullRequest.KeyMaterial keyMaterial = new DataPullRequest.KeyMaterial();
        keyMaterial.setDhPublicKey(new DataPullRequest.DhPublicKey());
        DataTransferRequest dataTransferRequest = new DataTransferRequest("BHAyI0JKsSboFAR+kAySRkp7SWYjKjhuSrQzixFcpiqFN8tnUPAbX0E1B+Z358sdb5vigG6BU9VwBqIAHPbiGrU=","6SOlbhxIAm/snY3DvVCPL1N9Pd5jn4e3RXaLwQo8inc=",keyMaterial,"3fa85f64-5717-4562-b3fc-2c963f66afa6",Collections.singletonList(new DataTransferRequest.Entry("BVZVM6Ptd/o1Fd8Zmw5BDuZXGpTyW7100jlcj0ld6L8=","10724488")));
        when(userServiceRestService.getAuthToken(any())).thenReturn("token");
        when(userServiceRestService.getRecordsByTxnId(eq("token"),eq("3fa85f64-5717-4562-b3fc-2c963f66afa6"))).thenReturn(new ApiResponse<>("True", new GetRecordsByTxnIdResponse("test","test"),null));
        when(eventService.findEventByEventName(any())).thenReturn(new Event());
        abdmIntegrationService.processHITransfer(dataTransferRequest);
        Mockito.verify(rmqPublisher,Mockito.times(1)).send(any(),any(),any(),any());
        Mockito.verify(abdmHelper,Mockito.times(1)).hiuTransferNotify(any());
    }

    private String getAuthNotifyJson() {
        return "{\"requestId\":\"baf556d0-6e37-4cb5-a63e-f4a59725f3fe\",\"timestamp\":\"2023-01-03T11:40:04.765923\",\"auth\":{\"transactionId\":\"19d38558-1ef6-44c5-bc0e-6f8feee50f7a\",\"status\":\"GRANTED\",\"accessToken\":\"eyJhbGciOiJSUzUxMiJ9.eyJzdWIiOiJtb2hhbmtyaXNobmExOTk5QHNieCIsInJlcXVlc3RlclR5cGUiOiJISVAiLCJyZXF1ZXN0ZXJJZCI6Ik5JS1NIQVktSElQIiwicGF0aWVudElkIjoibW9oYW5rcmlzaG5hMTk5OUBzYngiLCJzZXNzaW9uSWQiOiJkYjQ0MGJkMi05NTc3LTQ3NDQtOWNhYi1iNGNhNDUxMzZhMTIiLCJleHAiOjE2NzI4MzI0MDQsImlhdCI6MTY3Mjc0NjAwNH0.bsAk4F2VDNjHOLFOtdvTgN28X2-Yn0BGzRLAGn8TUHdDz7b7LlzCivZzDdZszPWnx3Qw8huuf6muUmqBgH7ajhlmfdmHPbC3Bz6QGqemMPJfGxjFDKz5hQyeEPeOdbkBpf4NMrfRqPmxws8O6SsbWOoOW-yMoldQaArua_qO5ojonofSG-pFtt0Jty2cAQJ-HAgiRXZ1gEkLS-jfYk-UCX2KXE20fewykbi0LEifcRF8aE5vgnDZ40zSlTDe7Rm6eIvr3BvyoMxvkiI0BJJw4o6goZqebDkN_mx0mxk3xFagiqpMF6VZhHgFT8SP2DUCSkW5n0LVa6c3FrjQgV2NWg\",\"validity\":{\"purpose\":\"LINK\",\"requester\":{\"type\":\"HIP\",\"id\":\"NIKSHAY-HIP\",\"name\":null},\"expiry\":\"2024-01-03T11:40:04.765873\",\"limit\":1},\"patient\":null}}";
    }

    private String getABDMLinkCareContextEvent() {
        return "{\"episodeId\":\"10750687\",\"personId\":\"41777\",\"hiTypes\":\"Prescription\",\"personName\":\"Ankur Mandal\",\"enrollmentDate\":\"24/06/2021\",\"identifierValue\":\"+919903246033\",\"gender\":\"M\",\"identifierType\":\"MOBILE\",\"yearOfBirth\":\"1996\",\"abhaAddress\":\"ankur.test2@sbx\"}";
    }

    private String getConsentNotifyJson() {
        return "{\"requestId\":\"5f7a535d-a3fd-416b-b069-c97d021fbacd\",\"timestamp\":\"1970-01-01T00:00:00.000Z\",\"notification\":{\"consentRequestId\":\"5f7a535d-a3fd-416b-b069-c97d021fbacd\",\"status\":\"GRANTED\",\"consentArtefacts\":[{\"id\":\"5f7a535d-a3fd-416b-b069-c97d021fbacd\"}]}}";
    }

    private String getConsentArtefactJson() {
        return "{\"requestId\":\"a3b6c134-12f1-4629-b75a-a35a7fdc7027\",\"timestamp\":\"2023-05-12T04:56:22.798624599\",\"consent\":{\"status\":\"GRANTED\",\"consentDetail\":{\"schemaVersion\":\"v0.5\",\"consentId\":\"4491ea54-43b8-4813-8c8a-8fb72eab0974\",\"createdAt\":\"2023-05-12T04:55:33.508756572\",\"patient\":{\"id\":\"91187171704708@sbx\"},\"careContexts\":[{\"patientReference\":\"1087608\",\"careContextReference\":\"40956499\"},{\"patientReference\":\"1087608\",\"careContextReference\":\"50001127\"},{\"patientReference\":\"1087608\",\"careContextReference\":\"40958006\"},{\"patientReference\":\"1087608\",\"careContextReference\":\"50002366\"},{\"patientReference\":\"1087608\",\"careContextReference\":\"50002340\"},{\"patientReference\":\"1087608\",\"careContextReference\":\"50000803\"}],\"purpose\":{\"text\":\"Self requested\",\"code\":\"CAREMGT\",\"refUri\":\"string\"},\"hip\":{\"id\":\"NIKSHAY-HIP\",\"name\":\"NIKSHAY\"},\"hiu\":{\"id\":\"NIKSHAY_HIU\",\"name\":null},\"consentManager\":{\"id\":\"sbx\"},\"requester\":{\"name\":\"NIKSHAY\",\"identifier\":{\"value\":\"324\",\"type\":\"DTO\",\"system\":\"https://nikshay.in\"}},\"hiTypes\":[\"DiagnosticReport\",\"DischargeSummary\",\"HealthDocumentRecord\",\"ImmunizationRecord\",\"OPConsultation\",\"Prescription\",\"WellnessRecord\"],\"permission\":{\"accessMode\":\"VIEW\",\"dateRange\":{\"from\":\"2022-06-21T11:05:00.442\",\"to\":\"2022-06-23T11:05:00.442\"},\"dataEraseAt\":\"2023-06-23T11:05:00.442\",\"frequency\":{\"unit\":\"HOUR\",\"value\":1,\"repeats\":1}}},\"signature\":\"SqToN5wkhn5/NC6+BiQ29S24sw9KoMzu5BTCulS1AIK30mY5lfZiNIRTCdv0IIdhtv3bzp/SQOIdG/MJEQbmw7SnSG1Fvp2zDFY8LzZz0cm2z7EnQOJLM4pVl8m6R5iByu7I9BQk4moNxL0C/g06SoVksBSev2+tGPpweYklpPTp2nuS+zEtMVcVYebz0hTMhMb05qbcN1p1BvkdQVH/+K6E6E6lEoBBz5HBnNZNi5XWAF63KOeidvIAlPuZ5ApigDO/oVRur5VetPoxgR/2l+CJ6GSo73Nohp8Yly8ftV6Djolh393IODMz/qERthou6qNcXuyVPNDIo7bP16HUGg==\"},\"error\":null,\"resp\":{\"requestId\":\"1ed0ff2d-4584-45f1-a978-87d387cdc6b5\"}}";
    }

    private String getHIOnDataRequestJson() {
        return "{\"requestId\":\"3d6ba80a-5320-475e-bceb-f567269dc582\",\"timestamp\":\"2023-05-12T04:57:33.165315254\",\"hiRequest\":{\"transactionId\":\"75eb699e-0bab-4460-927d-b6e6ab4d792d\",\"sessionStatus\":\"REQUESTED\"},\"error\":null,\"resp\":{\"requestId\":\"d15a82ae-427d-4c70-9e9d-7c0d6b1cbdaa\"}}";
    }

}
