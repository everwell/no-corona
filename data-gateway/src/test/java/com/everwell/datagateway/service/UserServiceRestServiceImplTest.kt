package com.everwell.datagateway.service;
import com.everwell.datagateway.BaseTest
import com.everwell.datagateway.component.AppProperties
import com.everwell.datagateway.models.response.ApiResponse
import com.everwell.datagateway.models.response.GetRecordsByTxnIdResponse
import com.everwell.datagateway.models.response.MicroServiceGenericAuthResponse
import com.everwell.datagateway.models.response.UserServiceCreateResponse
import com.everwell.datagateway.models.response.UserServiceSearchResponse
import com.nhaarman.mockitokotlin2.mock
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.util.LinkedMultiValueMap
import org.springframework.web.client.RestTemplate



@RunWith(MockitoJUnitRunner::class)
class UserServiceRestServiceImplTest : BaseTest() {

    @InjectMocks
    lateinit var userServiceRestServiceImpl: UserServiceRestServiceImpl

    @Test
    fun testAuthentication() {
        val appProperties = AppProperties()
        appProperties.userServiceUrl = "http://localhost:9090"

        val restTemplate : RestTemplate = mock()
        val restService : RestService = mock()

        userServiceRestServiceImpl.restService = restService
        userServiceRestServiceImpl.restTemplateForAuthentication = restTemplate
        userServiceRestServiceImpl.appProperties = appProperties

        val genericAuthResponseMock = ApiResponse<MicroServiceGenericAuthResponse>(
    "true", MicroServiceGenericAuthResponse(1L, "userService", "auth_token", 1234L), "");
        val responseEntity = ResponseEntity(genericAuthResponseMock, HttpStatus.OK)
        val headers = mutableMapOf<String, String>()
        headers["X-US-Client-Id"] = "29"
        val params: LinkedMultiValueMap<String, String> = LinkedMultiValueMap()
        Mockito.`when`(restTemplate.exchange(appProperties.userServiceUrl + "/v1/client", HttpMethod.GET, userServiceRestServiceImpl.getHttpEntity(headers, params), object : ParameterizedTypeReference<ApiResponse<MicroServiceGenericAuthResponse>>() {}))
        .thenAnswer { responseEntity }
        val genericAuthResponse = userServiceRestServiceImpl.getAuthToken("29")
        Assert.assertEquals(genericAuthResponseMock.data?.authToken, genericAuthResponse);
    }

    @Test
    fun testSearchUser() {
        val appProperties = AppProperties()
        appProperties.userServiceUrl = "http://localhost:9090"
        appProperties.genericClientId = "29"

        val restTemplate : RestTemplate = mock()
        val restService : RestService = mock()

        userServiceRestServiceImpl.restService = restService
        userServiceRestServiceImpl.restTemplateForAuthentication = restTemplate
        userServiceRestServiceImpl.appProperties = appProperties

        val response = ApiResponse<UserServiceSearchResponse>(
                "true", UserServiceSearchResponse(1), "");
        val responseEntity = ResponseEntity(response, HttpStatus.OK)
        val headers = mutableMapOf<String, String>()
        headers["X-US-Client-Id"] = "29"
        headers["X-US-Access-Token"] = "29"
        val data = mutableMapOf<String,String>()
        data["test"] = "test"
        Mockito.`when`(restTemplate.exchange(appProperties.userServiceUrl + "/v1/users/search", HttpMethod.POST, userServiceRestServiceImpl.getHttpEntity(headers,data), object : ParameterizedTypeReference<ApiResponse<UserServiceSearchResponse>>() {}))
                .thenAnswer { responseEntity }
        val userSearchResponse = userServiceRestServiceImpl.searchUser("29", data)
        Assert.assertEquals(response.data?.id, userSearchResponse.data);
    }

    @Test
    fun testCreateUser() {
        val appProperties = AppProperties()
        appProperties.userServiceUrl = "http://localhost:9090"
        appProperties.genericClientId = "1"

        val restTemplate : RestTemplate = mock()
        val restService : RestService = mock()

        userServiceRestServiceImpl.restService = restService
        userServiceRestServiceImpl.restTemplateForAuthentication = restTemplate
        userServiceRestServiceImpl.appProperties = appProperties

        val response = ApiResponse<UserServiceCreateResponse>(
                "true", UserServiceCreateResponse(1), "");
        val responseEntity = ResponseEntity(response, HttpStatus.OK)

        val headers = mutableMapOf<String, String>()
        headers["X-US-Client-Id"] = "29"
        headers["X-US-Access-Token"] = "auth_token"
        val data = mutableMapOf<String,Any>()
        data["test"] = "test"

        val genericAuthResponseMock = ApiResponse<MicroServiceGenericAuthResponse>(
                "true", MicroServiceGenericAuthResponse(1L, "userService", "auth_token", 1234L), "");
        val authResponseEntity = ResponseEntity(genericAuthResponseMock, HttpStatus.OK)
        val authHeaders = mutableMapOf<String, String>()
        authHeaders["X-US-Client-Id"] = "29"
        val params: LinkedMultiValueMap<String, String> = LinkedMultiValueMap()
        Mockito.`when`(restTemplate.exchange(appProperties.userServiceUrl + "/v1/client", HttpMethod.GET, userServiceRestServiceImpl.getHttpEntity(authHeaders, params), object : ParameterizedTypeReference<ApiResponse<MicroServiceGenericAuthResponse>>() {}))
                .thenAnswer { authResponseEntity }

        Mockito.`when`(restTemplate.exchange(appProperties.userServiceUrl + "/v1/users", HttpMethod.POST, userServiceRestServiceImpl.getHttpEntity(headers,data), object : ParameterizedTypeReference<ApiResponse<UserServiceCreateResponse>>() {}))
                .thenAnswer { responseEntity }
        val createUserResponse = userServiceRestServiceImpl.createUser(appProperties.userServiceUrl, 29, data)
        Assert.assertEquals(response.data?.id, createUserResponse.data?.id);
    }

     @Test
    fun testGetRecordsByTxnId() {
        val appProperties = AppProperties()
        appProperties.userServiceUrl = "http://localhost:9090"
        appProperties.genericClientId = "29"

        val restTemplate : RestTemplate = mock()
        val restService : RestService = mock()

        userServiceRestServiceImpl.restService = restService
        userServiceRestServiceImpl.restTemplateForAuthentication = restTemplate
        userServiceRestServiceImpl.appProperties = appProperties

        val response = ApiResponse(
                "true", GetRecordsByTxnIdResponse("test","test"), "");
        val responseEntity = ResponseEntity(response, HttpStatus.OK)
        val headers = mutableMapOf<String, String>()
        headers["X-US-Client-Id"] = "29"
        headers["X-US-Access-Token"] = "29"
        val params: LinkedMultiValueMap<String, String> = LinkedMultiValueMap()
        Mockito.`when`(restTemplate.exchange(appProperties.userServiceUrl + "/v1/user-records/transactionId/txnId", HttpMethod.GET, userServiceRestServiceImpl.getHttpEntity(headers,params), object : ParameterizedTypeReference<ApiResponse<GetRecordsByTxnIdResponse>>() {}))
                .thenAnswer { responseEntity }
        val getRecordsByTxnIdResponse = userServiceRestServiceImpl.getRecordsByTxnId("29", "txnId")
        Assert.assertEquals(response.data?.consentId, getRecordsByTxnIdResponse.data?.consentId);
    }
}