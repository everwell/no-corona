package com.everwell.datagateway.service

import com.everwell.datagateway.BaseTest
import com.everwell.datagateway.component.AppProperties
import com.everwell.datagateway.models.response.ApiResponse
import com.everwell.datagateway.models.response.GenericAuthResponse
import com.everwell.datagateway.models.response.MicroServiceGenericAuthResponse
import com.nhaarman.mockitokotlin2.mock
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.util.LinkedMultiValueMap
import org.springframework.web.client.RestTemplate

@RunWith(MockitoJUnitRunner::class)
class IamRestServiceImpTest : BaseTest() {

    @InjectMocks
    lateinit var iamRestServiceImpl: IamRestServiceImp

    @Test
    fun testAuthentication() {
        val appProperties = AppProperties()
        appProperties.iamServerUrl = "http://localhost:9090"

        val restTemplate : RestTemplate = mock()
        val restService : RestService = mock()

        iamRestServiceImpl.restService = restService
        iamRestServiceImpl.restTemplateForAuthentication = restTemplate
        iamRestServiceImpl.appProperties = appProperties

        val genericAuthResponseMock = ApiResponse<MicroServiceGenericAuthResponse>(
                "true", MicroServiceGenericAuthResponse(1L, "iam", "auth_token", 1234L), "");
        val responseEntity = ResponseEntity(genericAuthResponseMock, HttpStatus.OK)
        val headers = mutableMapOf<String, String>()
        headers["X-IAM-Client-Id"] = "29"
        val params: LinkedMultiValueMap<String, String> = LinkedMultiValueMap()
        Mockito.`when`(restTemplate.exchange(appProperties.iamServerUrl + "/v1/client", HttpMethod.GET, iamRestServiceImpl.getHttpEntity(headers, params), object : ParameterizedTypeReference<ApiResponse<MicroServiceGenericAuthResponse>>() {}))
                .thenAnswer { responseEntity }
        val genericAuthResponse = iamRestServiceImpl.getAuthToken("29")
        Assert.assertEquals(genericAuthResponseMock.data?.authToken, genericAuthResponse);
    }
}