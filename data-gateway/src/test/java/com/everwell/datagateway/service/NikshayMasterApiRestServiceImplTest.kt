package com.everwell.datagateway.service

import com.everwell.datagateway.component.AppProperties
import com.everwell.datagateway.entities.Client
import com.everwell.datagateway.models.response.GenericAuthResponse
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import org.junit.Assert
import org.junit.Test
import org.mockito.ArgumentMatchers
import org.mockito.Mockito
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.HttpEntity
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.util.LinkedMultiValueMap
import org.springframework.web.client.RestTemplate
import kotlin.reflect.jvm.internal.impl.load.kotlin.JvmType

class NikshayMasterApiRestServiceImplTest {

    private var nikshayMasterApiRestServiceImp: NikshayMasterApiRestServiceImp = NikshayMasterApiRestServiceImp()

    @Test
    fun testAuthentication(){
        val clientService : ClientService = mock()
        val restTemplate : RestTemplate = mock()
        val restService : RestService = mock()
        val client : Client = mock()
        val httpEntity : HttpEntity<JvmType.Object> = mock()
        val authenticationService : AuthenticationService = mock()

        val appProperties = AppProperties()
        appProperties.nikshayMasterApiServerUrl = "http://localhost.com/"
        appProperties.nikshayMasterApiUserGrantType="password"
        appProperties.nikshayMasterApiUserName= "username"
        appProperties.nikshayMasterApiPassword = "password"

        nikshayMasterApiRestServiceImp.appProperties = appProperties
        nikshayMasterApiRestServiceImp.restService = restService
        nikshayMasterApiRestServiceImp.restTemplateForAuthentication = restTemplate
        nikshayMasterApiRestServiceImp.clientService = clientService
        nikshayMasterApiRestServiceImp.authenticationService = authenticationService

        val genericAuthResponseMock = GenericAuthResponse("abcded1234",25999)
        val responseEntity = ResponseEntity(genericAuthResponseMock, HttpStatus.OK)
        Mockito.`when`(restService.buildHeaderAndRequest(ArgumentMatchers.anyMap())).thenAnswer { httpEntity }
        Mockito.`when`(clientService.findByUsername("username")).thenAnswer { client }
        Mockito.`when`(authenticationService.getCurrentUsername(any())).thenAnswer { ArgumentMatchers.anyString() }
        Mockito.`when`(restTemplate.postForObject("http://localhost.com/Token", httpEntity, GenericAuthResponse::class.java)).thenAnswer{genericAuthResponseMock}
        val params: LinkedMultiValueMap<String, String> = LinkedMultiValueMap()
        params["grant_type"] = appProperties.nikshayMasterApiUserGrantType
        params["username"] = appProperties.nikshayMasterApiUserName
        params["password"] = appProperties.nikshayMasterApiPassword
        Mockito.`when`(restTemplate.exchange(appProperties.nikshayMasterApiServerUrl + "Token", HttpMethod.POST, nikshayMasterApiRestServiceImp.getHttpEntity(HashMap(), params), object : ParameterizedTypeReference<GenericAuthResponse>() {}))
                .thenAnswer { responseEntity }
        val genericAuthResponse = nikshayMasterApiRestServiceImp.getAuthToken(null)
        Assert.assertEquals(genericAuthResponseMock.access_token, genericAuthResponse);
    }
}