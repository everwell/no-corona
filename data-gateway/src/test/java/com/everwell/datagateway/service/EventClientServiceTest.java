package com.everwell.datagateway.service;

import com.everwell.datagateway.BaseTest;
import com.everwell.datagateway.entities.EventClient;
import com.everwell.datagateway.repositories.EventClientRepository;
import com.everwell.datagateway.service.impl.EventClientServiceImpl;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

class EventClientServiceTest extends BaseTest {

    @Mock
    private EventClientRepository eventClientRepository;

    @InjectMocks
    EventClientServiceImpl eventClientService;

    @Test
    void findEventClientByClientId() {
        EventClient eventClient = new EventClient(1L, 2L, 159L, "JSON", true);
        when(eventClientRepository.findAllByClientId(159L)).thenReturn(Collections.singletonList(eventClient));
        List<EventClient> eventClientList = eventClientService.getEventClientByClientId(159L);
        assertEquals(eventClientList.size(), 1);
        assertEquals(eventClientList.get(0).getId(), eventClient.getId());
    }

}