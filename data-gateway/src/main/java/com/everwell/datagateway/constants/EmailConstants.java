package com.everwell.datagateway.constants;

public class EmailConstants {


    public static final String FAILURE_EMAIL_SUBJECT = "Data Gateway: Message not published";
    public static final String DLQ_EMAIL_SUBJECT = "Data Gateway: DLQ Messages";
    public static final int BULK_MESSAGES_LIMIT = 100;

}
