package com.everwell.datagateway.constants;

public class ECBSSConstants {
    public static final String PROGRAM = "program";
    public static final String FILTER = "filter";
    public static final String OUMODE = "SELECTED";
    public static final String TRACKEDENTITYINSTANCE_EVENT = "FetchTrackedEntityInstance";
    public static final String MULTIPLE_TRACKED_ENTITY_INSTANCE_RECEIVED = "TrackedEntityInstancesResponse is null or Multiple TrackedEntityInstances are received ";
    public static final String ERROR_MESSAGE_TRACKED_ENTITY_INSTANCE = "Error while fetching tracked entity instance";
    public static final String OU_KEY = "ou";
    public static final String FILTER_KEY = "filter";
    public static final String PROGRAM_KEY = "program";
    public static final String OU_MODE_KEY = "ouMode";
    public static final String GET_TRACKED_ENTITY_ENDPOINT = "/api/trackedEntityInstances.json";
}
