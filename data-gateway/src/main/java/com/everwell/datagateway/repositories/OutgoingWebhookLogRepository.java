package com.everwell.datagateway.repositories;

import com.everwell.datagateway.entities.OutgoingWebhookLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OutgoingWebhookLogRepository extends JpaRepository<OutgoingWebhookLog, Long> {
}
