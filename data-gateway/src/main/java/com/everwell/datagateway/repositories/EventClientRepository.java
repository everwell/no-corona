package com.everwell.datagateway.repositories;

import com.everwell.datagateway.entities.EventClient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EventClientRepository extends JpaRepository<EventClient, Long> {

    EventClient findByClientIdAndEventId(Long clientId, Long eventId);

    EventClient findByClientIdAndEventIdAndActiveTrue(Long clientId, Long eventId);

    List<EventClient> findAllByClientId(Long clientId);
}
