package com.everwell.datagateway.component

import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component

@Component
open class AppProperties {
    @Value("\${client.nikshay.server.url}")
    lateinit var nikshayServerUrl: String

    @Value("\${client.nikshay.server.username}")
    lateinit var nikshayUserName: String

    @Value("\${client.nikshay.server.password}")
    lateinit var nikshayUserPassword: String

    @Value("\${client.nikshay.server.granttype}")
    lateinit var nikshayUserGrantType: String


    @Value("\${client.reports.nikshay.server.url}")
    lateinit var nikshayReportsServerUrl: String

    @Value("\${client.reports.nikshay.server.username}")
    lateinit var nikshayReportsUserName: String

    @Value("\${client.reports.nikshay.server.password}")
    lateinit var nikshayReportsUserPassword: String

    @Value("\${client.reports.nikshay.server.granttype}")
    lateinit var nikshayReportsUserGrantType: String

    @Value("\${client.hub.server.url}")
    lateinit var hubServerUrl: String

    @Value("\${client.hub.server.username}")
    lateinit var hubUserName: String

    @Value("\${client.hub.server.password}")
    lateinit var hubUserPassword: String

    @Value("\${client.hub.server.granttype}")
    lateinit var hubUserGrantType: String

    @Value("\${zuul.routes.iam.url}")
    lateinit var iamServerUrl: String
    
    @Value("\${zuul.routes.ins.url}")
    lateinit var insServerUrl: String

    @Value("\${zuul.routes.registry.url}")
    lateinit var registryServerUrl: String

    @Value("\${zuul.routes.dispensation.url}")
    lateinit var dispensationServerUrl: String

    @Value("\${zuul.routes.diagnostics.url}")
    lateinit var diagnosticsServerUrl: String
    
    @Value("\${zuul.routes.userService.url}")
    lateinit var userServiceUrl: String

    @Value("\${zuul.routes.sso.url}")
    lateinit var ssoServerUrl: String

    @Value("#{\${sso.client.id.url.map}}")
    lateinit var ssoClientIdUrlMap:Map<String,String>

    @Value("\${zuul.routes.episode.url}")
    lateinit var episodeServiceUrl: String

    @Value("\${default.service.client_id}")
    lateinit var genericClientId : String

    @Value("#{\${zuul.routes.merm.default.client.id}}")
    lateinit var registryDefaultClientId: String

    @Value("\${zuul.routes.smartPayments.url}")
    lateinit var smartPaymentsUrl: String

    @Value("\${client.smartPayment.server.grantType}")
    lateinit var smartPaymentUserGrantType: String

    @Value("#{\${smartPayment.client.id.map}}")
    lateinit var smartPaymentsClientIdMap:Map<String,String>

    @Value("#{\${smartPayment.client.id.secret.map}}")
    lateinit var smartPaymentsClientIdSecretMap:Map<String,String>
    
    @Value("\${client.lims.nikshay.server.username}")
    lateinit var limsNikshayUserName: String

    @Value("\${client.lims.nikshay.server.password}")
    lateinit var limsNikshayUserPassword: String

    @Value("\${client.lims.nikshay.server.granttype}")
    lateinit var limsNikshayUserGrantType: String
    @Value("\${zuul.routes.hubReports.url}")
    lateinit var hubReportsUrl: String

    @Value("\${clientId.callLogs}")
    lateinit var clientIdCallLogs: String

    @Value("\${clientId.lpa}")
    lateinit var clientIdLpa: String

    @Value("\${client.nikshayMasterApi.server.grantType}")
    lateinit var nikshayMasterApiUserGrantType: String

    @Value("\${client.nikshayMasterApi.server.username}")
    lateinit var nikshayMasterApiUserName: String

    @Value("\${client.nikshayMasterApi.server.password}")
    lateinit var nikshayMasterApiPassword: String

    @Value("\${zuul.routes.nikshayMasterApi.url}")
    lateinit var nikshayMasterApiServerUrl: String

    @Value("\${zuul.routes.kpi.url}")
    lateinit var kpiServerUrl: String

    @Value("\${clientId.interraCallLogs}")
    lateinit var clientIdInterraCallLogs: String

    @Value("\${zuul.routes.egs.url}")
    lateinit var egsUrl: String
    
    @Value("\${zuul.routes.vot.url}")
    lateinit var votServerUrl: String

    @Value("\${vot.username}")
    lateinit var votUsername: String

    @Value("\${vot.password}")
    lateinit var votPassword: String

    @Value("\${zuul.routes.dbtservice.url}")
    lateinit var dbtServiceUrl: String

    @Value("\${clientId.globeLabs}")
    lateinit var clientIdGlobeLabsCallLogs: String
}