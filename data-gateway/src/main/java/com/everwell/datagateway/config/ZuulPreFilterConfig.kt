package com.everwell.datagateway.config
import com.everwell.datagateway.filters.zuul.APIAccessZuulPreFilter
import com.everwell.datagateway.filters.zuul.RequestLogPreFilter
import com.everwell.datagateway.filters.zuul.callLogs.CallLogsZuulPreFilter
import com.everwell.datagateway.filters.zuul.globalLabsCallLogs.GlobeLabsCallLogsZuulPreFilter
import com.everwell.datagateway.filters.zuul.interraCallLogs.InterraCallLogsZuulPreFilter
import com.everwell.datagateway.filters.zuul.registry.RegistryZuulPreFilter
import com.everwell.datagateway.filters.zuul.gmri.GmriZuulPreFilter
import com.everwell.datagateway.filters.zuul.lpa.LpaZuulPreFilter
import com.everwell.datagateway.filters.zuul.mermConfig.MermConfigZuulPreFilter
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
open class ZuulPreFilterConfig {
    @Bean
    open fun zuulAPIAccessPreFilter() : APIAccessZuulPreFilter {
        return APIAccessZuulPreFilter()
    }

    @Bean
    open fun registryZuulPreFilter() : RegistryZuulPreFilter {
        return RegistryZuulPreFilter()
    }

    @Bean
    open fun gmriZuulPreFilter() : GmriZuulPreFilter {
        return GmriZuulPreFilter()
    }

    @Bean
    open fun callLogsZuulPreFilter() : CallLogsZuulPreFilter {
        return CallLogsZuulPreFilter()
    }

    @Bean
    open fun mermConfigZuulPreFilter() : MermConfigZuulPreFilter {
        return MermConfigZuulPreFilter()
    }

    @Bean
    open fun lpaZuulPreFilter() : LpaZuulPreFilter {
        return LpaZuulPreFilter()
    }

    @Bean
    open fun requestLogPreFilter() : RequestLogPreFilter {
        return RequestLogPreFilter()
    }

    @Bean
    open fun callLogsInterraZuulPreFilter() : InterraCallLogsZuulPreFilter {
        return InterraCallLogsZuulPreFilter()
    }

    @Bean
    open fun callLogsGlobeLabsZuulPreFilter() : GlobeLabsCallLogsZuulPreFilter {
        return GlobeLabsCallLogsZuulPreFilter()
    }
}