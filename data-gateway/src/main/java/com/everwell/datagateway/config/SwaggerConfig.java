package com.everwell.datagateway.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.http.ResponseEntity;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.Contact;
import springfox.documentation.service.VendorExtension;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Configuration
@EnableSwagger2
@Profile({ "local","beta"})
public class SwaggerConfig {

    @Bean
    public Docket api() {

        Contact contact = new Contact(
                "Everwell Health Solutions",
                "https://www.everwell.org/",
                "developers@everwell.org");

        List<VendorExtension> vext = new ArrayList<>();
        ApiInfo apiInfo = new ApiInfo(
                "Data Gateway",
                "The Everwell Data Gateway is designed for external vendor integration of \n" +
                        "1)Data produced as a result of business events.\n" +
                        "2)Data consumed by the Hub for triggering certain business events.\n",
                "1.1.1",
                "",
                contact,
                "",
                "",
                vext);


        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.everwell.datagateway"))
                .paths(PathSelectors.any())
                .build()
                .securitySchemes(Arrays.asList(apiKey()))
                .genericModelSubstitutes(ResponseEntity.class);
    }


    private ApiKey apiKey() {
        return new ApiKey("jwtToken", "Authorization", "header");
    }


}
