package com.everwell.datagateway.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "publisher_queue_info")
@Getter
@Setter
@NoArgsConstructor
public class PublisherQueueInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String queueName;

    private String eventName;

    private String exchange;

    private String routingKey;

    private String replyToRoutingKey;
}
