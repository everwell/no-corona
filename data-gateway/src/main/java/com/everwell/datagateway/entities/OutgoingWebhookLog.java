package com.everwell.datagateway.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "outgoing_webhook_log")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class OutgoingWebhookLog {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long subscriberUrlId;
    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    private boolean resultDelivered;
    @Column(columnDefinition = "TEXT")
    private String data;
    private Long eventId;
    private Long clientId;
    private Date deliveredAt;
    private Integer callbackResponseCode;
    @Column(columnDefinition = "TEXT")
    private String response;
    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_at")
    private Date updatedAt;
}
