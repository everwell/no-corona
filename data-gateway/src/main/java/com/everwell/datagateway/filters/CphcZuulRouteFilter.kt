package com.everwell.datagateway.filters

import com.everwell.datagateway.filters.zuul.base.BaseZuulRouteFilter
import com.everwell.datagateway.service.BaseRestService
import com.everwell.datagateway.service.NikshayRestService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants.PRE_DECORATION_FILTER_ORDER
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants.ROUTE_TYPE


open class CphcZuulRouteFilter(

) : BaseZuulRouteFilter() {


    @Autowired
    lateinit var nikshayRestService: NikshayRestService

    override var thisURI: String
        get() = "/cphc"
        set(value) {}

    init {
        /// keep adding the url mapping here as CPHC know which API to be called
        urlMapping.put("cphc/getPatientDetails", "api/Patients/Get/{patientId}")
    }

    override fun getRestService(): BaseRestService {
        return nikshayRestService
    }


    override fun filterType(): String {
        return ROUTE_TYPE
    }

    override fun filterOrder(): Int {
        return PRE_DECORATION_FILTER_ORDER + 6
    }

}