package com.everwell.datagateway.filters.zuul.umang

import com.everwell.datagateway.filters.zuul.base.BaseZuulPostFilter
import com.everwell.datagateway.service.BaseRestService
import com.everwell.datagateway.service.NikshayRestService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants.POST_TYPE
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants.SEND_RESPONSE_FILTER_ORDER


open class UmangZuulPostFilter(

) : BaseZuulPostFilter(){

    @Autowired
    lateinit var nikshayRestService: NikshayRestService

    override var thisURI: String
        get() = "/umang"
        set(value) {}

    override fun getRestService(): BaseRestService {
        return nikshayRestService
    }

    override fun filterType(): String {
        return POST_TYPE
    }

    override fun filterOrder(): Int {
        return SEND_RESPONSE_FILTER_ORDER-1
    }
}