package com.everwell.datagateway.filters.zuul.registry

import com.everwell.datagateway.config.CacheConfig
import com.everwell.datagateway.utils.Utils
import com.netflix.zuul.ZuulFilter
import com.netflix.zuul.context.RequestContext
import org.apache.http.client.utils.URLEncodedUtils
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.MediaType
import org.springframework.http.client.BufferingClientHttpRequestFactory
import org.springframework.http.client.SimpleClientHttpRequestFactory
import org.springframework.web.client.RestTemplate
import java.nio.charset.Charset

open class RegistryZuulPreFilter : ZuulFilter() {

    var restTemplateForApi: RestTemplate = RestTemplate(BufferingClientHttpRequestFactory(SimpleClientHttpRequestFactory()))

    private val LOGGER = LoggerFactory.getLogger(CacheConfig::class.java)

    @Value("\${nikshay.web.app.url}")
    lateinit var nikshayWebAppURL: String

    // Filter only for process merm event
    override fun shouldFilter():Boolean{
        val request = RequestContext.getCurrentContext().request
        return (request.requestURI.contains("registry/v1/merm/event",true) || request.requestURI.contains("process-merm-event",true)) && request.method.equals(HttpMethod.POST.name)
    }

    override fun filterType(): String {
        return FilterConstants.PRE_TYPE
    }

    override fun filterOrder(): Int {
        return 0
    }

    override fun run(): Any? {
        val ctx: RequestContext = RequestContext.getCurrentContext()
        val queryString = URLEncodedUtils.parse(ctx.request.queryString, Charset.forName("UTF-8"))
        for(query in queryString){
            if(query.name.equals("noforward")){
                if(!query.value.equals("1")){
                    // Redirect to Nikshay if noforward is not equal to 1
                    val request = Utils.readRequestBody(ctx.request.inputStream)
                    val headers = HttpHeaders()
                    headers.add("Content-Type", MediaType.APPLICATION_JSON.toString())
                    try {
                        restTemplateForApi.exchange(
                            "$nikshayWebAppURL/MERM.aspx",
                            HttpMethod.POST,
                            HttpEntity<Any>(request, headers),
                            String::class.java
                        )
                    } catch (e: Exception){
                        LOGGER.debug(e.message)
                    }
                    return null
                }
                break;
            }
        }
        return null
    }
}