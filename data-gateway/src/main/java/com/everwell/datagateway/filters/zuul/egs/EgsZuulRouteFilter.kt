package com.everwell.datagateway.filters.zuul.egs

import com.everwell.datagateway.filters.zuul.base.BaseZuulRouteFilter
import com.everwell.datagateway.service.BaseRestService
import com.everwell.datagateway.service.EgsRestService
import com.netflix.zuul.context.RequestContext
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants

open class EgsZuulRouteFilter() : BaseZuulRouteFilter() {

    @Autowired
    lateinit var egsRestService: EgsRestService

    override fun getRestService(): BaseRestService {
        return egsRestService
    }

    override fun getHeadersForAuth(context: RequestContext): MutableMap<String, String> {
        return mutableMapOf()
    }

    override fun getUrlMapping(context: RequestContext): String? {
        return context.request.requestURI.substring(thisURI.length)
    }

    override var thisURI: String
        get() = "/egs"
        set(value) {}

    override fun filterType(): String {
        return FilterConstants.ROUTE_TYPE
    }

    override fun filterOrder(): Int {
        return FilterConstants.PRE_DECORATION_FILTER_ORDER + 10
    }
}