package com.everwell.datagateway.filters.zuul.vot

import com.everwell.datagateway.filters.zuul.base.BaseZuulRouteFilter
import com.everwell.datagateway.service.BaseRestService
import com.everwell.datagateway.service.VotService
import com.netflix.zuul.context.RequestContext
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants
import java.net.URL

class VotZuulRouteFilter: BaseZuulRouteFilter() {

    @Autowired
    lateinit var votService: VotService

    override var thisURI: String
        get() = "/vot"
        set(value) {}

    override fun run(): Any? {
        val ctx: RequestContext = RequestContext.getCurrentContext()
        val votUrl = URL(appProperties.votServerUrl)
        ctx.routeHost = votUrl
        super.run()
        return null
    }

    override fun getHeadersForAuth(context: RequestContext): MutableMap<String, String> {
        val map = mutableMapOf<String, String>()
        map["Authorization"] = "JWT " + votService.getAuthToken(null)!!
        return map
    }

    override fun getUrlMapping(context: RequestContext): String? {
        return context.request.requestURI.substring(thisURI.length)
    }

    override fun getRestService(): BaseRestService {
        return votService
    }

    override fun filterType(): String {
        return FilterConstants.ROUTE_TYPE
    }

    override fun filterOrder(): Int {
        return FilterConstants.PRE_DECORATION_FILTER_ORDER + 10
    }
}