package com.everwell.datagateway.filters.zuul

import com.everwell.datagateway.constants.Constants
import com.everwell.datagateway.entities.Client
import com.everwell.datagateway.entities.ClientRequests
import com.everwell.datagateway.entities.Event
import com.everwell.datagateway.entities.SubscriberUrl
import com.everwell.datagateway.service.*
import com.everwell.datagateway.utils.Utils
import com.fasterxml.jackson.databind.ObjectMapper
import com.netflix.zuul.context.RequestContext
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants
import org.springframework.security.core.context.SecurityContextHolder

class RequestLogPreFilter: RequestLogFilterBase() {
    @Autowired
    lateinit var clientService: ClientService

    @Autowired
    lateinit var subscriberUrlService: SubscriberUrlService

    @Autowired
    lateinit var eventService: EventService

    override fun filterOrder(): Int {
        return 2 //ensuring this filter gets executed after APIAccessZuulPreFilter
    }

    override fun filterType(): String {
        return FilterConstants.PRE_TYPE
    }

    override fun run(): Any? {
        val ctx: RequestContext = RequestContext.getCurrentContext()
        val request = ctx.request
        val payload = Utils.readRequestBody(request.inputStream)
        val uri = request.requestURI + (request.queryString?.let { "?$it" } ?: "")
        val requestHeaders = mutableMapOf<String, String>()
        val headerNames = request.headerNames
        while (headerNames.hasMoreElements()) {
            val headerName = headerNames.nextElement()
            requestHeaders[headerName] = request.getHeader(headerName)
        }
        val objectMapper = ObjectMapper()
        val requestHeadersString = objectMapper.writeValueAsString(requestHeaders)

        var clientId: Long? = null
        var subscriberUrlId: Long? = null
        var eventId: Long? = null
        if (ctx.containsKey(Constants.REQUEST_LOG_DATA)) {
            val requestLogData: Map<String, Long> = ctx[Constants.REQUEST_LOG_DATA] as Map<String, Long>
            clientId = requestLogData["clientId"]
            eventId = requestLogData["eventId"]
            subscriberUrlId = requestLogData["subscriberUrlId"]
        }
        val clientRequests = ClientRequests(clientId, payload, eventId, subscriberUrlId,uri, requestHeadersString)
        val createdId = clientRequestsService.saveClientRequest(clientRequests)
        ctx.set(Constants.REQUEST_LOG_ID_KEY, createdId)

        return null
    }
}