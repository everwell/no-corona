package com.everwell.datagateway.filters.zuul.dispensation;

import com.everwell.datagateway.filters.zuul.base.BaseZuulRouteFilter
import com.everwell.datagateway.service.BaseRestService
import com.everwell.datagateway.service.DispensationRestService
import com.netflix.zuul.context.RequestContext
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants
import com.everwell.datagateway.constants.Constants.CLIENT_ID

open class DispensationZuulRouteFilter (

): BaseZuulRouteFilter() {

    @Autowired
    lateinit var dispensationRestService: DispensationRestService

    override var thisURI: String
        get() = "/dispensation"
        set(value) {}

    override fun getHeadersForAuth(context: RequestContext): MutableMap<String, String> {
        val map = mutableMapOf<String, String>()
        map["X-DS-Access-Token"] = dispensationRestService.getAuthToken(context.request.getHeader(CLIENT_ID))!!
        map["X-DS-Client-Id"] = context.request.getHeader(CLIENT_ID)
        return map
    }

    override fun getUrlMapping(context: RequestContext): String? {
        return context.request.requestURI.substring(thisURI.length)
    }

    override fun getRestService(): BaseRestService {
        return dispensationRestService
    }

    override fun filterType(): String {
        return FilterConstants.ROUTE_TYPE
    }

    override fun filterOrder(): Int {
        return FilterConstants.PRE_DECORATION_FILTER_ORDER + 10
    }
}