package com.everwell.datagateway.filters.zuul

import com.everwell.datagateway.constants.Constants
import com.everwell.datagateway.entities.SubscriberUrl
import com.everwell.datagateway.enums.LegacyRoutesEnum
import com.everwell.datagateway.service.AuthenticationService
import com.everwell.datagateway.service.ClientService
import com.everwell.datagateway.service.EventClientService
import com.everwell.datagateway.service.SubscriberUrlService
import com.fasterxml.jackson.databind.ObjectMapper
import com.netflix.zuul.ZuulFilter
import com.netflix.zuul.context.RequestContext
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants.PRE_TYPE
import org.springframework.http.HttpStatus
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.util.AntPathMatcher
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import kotlin.streams.toList


open class APIAccessZuulPreFilter(

) : ZuulFilter(){

    @Autowired
    lateinit var clientService: ClientService

    @Autowired
    lateinit var authenticationService: AuthenticationService

    @Autowired
    lateinit var subscriberUrlService: SubscriberUrlService

    @Autowired
    lateinit var eventClientService: EventClientService

    override fun run(): Any? {
        val ctx: RequestContext = RequestContext.getCurrentContext()
        val requestServlet: HttpServletRequest = ctx.getRequest()
        val pathMatcher = AntPathMatcher()
        val clientId = clientService.findByUsername(getUserName()).id
        val eventClientIds : List<Long> = eventClientService.getEventClientByClientId(clientId).stream().map { ec -> ec.id }.toList()
        val subscriberUrls: List<SubscriberUrl> = subscriberUrlService.findUrlsByEventClientId(eventClientIds)
        for(subscriberUrl in subscriberUrls){
            if(pathMatcher.match(subscriberUrl.url, requestServlet.requestURI)) {
                val requestLogData = mapOf<String, Long>("clientId" to clientId, "eventId" to subscriberUrl.eventClient.eventId, "subscriberUrlId" to subscriberUrl.id)
                ctx.set(Constants.REQUEST_LOG_DATA, requestLogData)
                ctx.set(Constants.GENERATE_USER_ACCESS_TOKEN, true )
                return null
            }
        }

        // User dont have access so requested URI
        setResponseUnauthorized(ctx, requestServlet)
        return null
    }

    private fun setResponseUnauthorized(ctx: RequestContext, requestServlet: HttpServletRequest) {
        ctx.setSendZuulResponse(false)
        ctx.unset()
        val message = "Not authorised to call '" + requestServlet.requestURI +"' route"
        ctx.responseStatusCode = HttpStatus.UNAUTHORIZED.value()
        val response: HttpServletResponse = ctx.response
        val responseBody = mapOf("success" to false, "message" to message)
        val objectMapper = ObjectMapper()
        val responseBodyStr = objectMapper.writeValueAsString(responseBody)
        ctx.responseBody = responseBodyStr
        response.contentType = "application/json"
        response.characterEncoding = "UTF-8"
        response.writer.write(ctx.responseBody)
    }

    override fun shouldFilter(): Boolean {
        if (LegacyRoutesEnum.isLegacyRoute(RequestContext.getCurrentContext().request.requestURI)){
            return false;
        }
        return getUserName()?.toLowerCase() != Constants.INTERNAL_CLIENT_USERNAME
    }

    override fun filterType(): String {
        return PRE_TYPE
    }

    override fun filterOrder(): Int {
       return 1
    }

    fun getUserName():String?{
        val authentication = SecurityContextHolder.getContext().authentication
        return authenticationService.getCurrentUsername(authentication)
    }

}