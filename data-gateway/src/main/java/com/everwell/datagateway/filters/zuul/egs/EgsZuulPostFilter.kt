package com.everwell.datagateway.filters.zuul.egs

import com.everwell.datagateway.filters.zuul.base.BaseZuulPostFilter
import com.everwell.datagateway.service.BaseRestService
import com.everwell.datagateway.service.EgsRestService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants

class EgsZuulPostFilter() : BaseZuulPostFilter() {

    @Autowired
    lateinit var egsRestService: EgsRestService

    override fun getRestService(): BaseRestService {
        return egsRestService
    }

    override var thisURI: String
        get() = "/egs"
        set(value) {}

    override fun filterType(): String {
        return FilterConstants.POST_TYPE
    }

    override fun filterOrder(): Int {
        return FilterConstants.SEND_RESPONSE_FILTER_ORDER -2
    }
}