package com.everwell.datagateway.filters.zuul

import com.everwell.datagateway.filters.zuul.base.BaseZuulRouteFilter
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants

abstract class GenericZuulRouteFilter() : BaseZuulRouteFilter() {

    override fun filterType(): String {
        return FilterConstants.ROUTE_TYPE
    }

    override fun filterOrder(): Int {
        return FilterConstants.PRE_DECORATION_FILTER_ORDER +2
    }
}