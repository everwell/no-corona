package com.everwell.datagateway.filters.zuul

import com.everwell.datagateway.constants.Constants
import com.everwell.datagateway.utils.Utils
import com.netflix.zuul.context.RequestContext
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants

class RequestLogPostFilter: RequestLogFilterBase() {
    override fun filterOrder(): Int {
        return 1000
    }

    override fun filterType(): String {
        return FilterConstants.POST_TYPE
    }

    override fun run(): Any? {
        val ctx: RequestContext = RequestContext.getCurrentContext()
        var response = ""
        if(ctx.containsKey(Constants.REQUEST_LOG_RESPONSE))
        {
            response = ctx.get(Constants.REQUEST_LOG_RESPONSE).toString()
        }
        else
        {
            response = Utils.readResponseBody(ctx)
            ctx.responseBody = response
        }

        if (ctx.containsKey(Constants.REQUEST_LOG_ID_KEY))
        {
            val requestLogId = ctx[Constants.REQUEST_LOG_ID_KEY]
            clientRequestsService.updateResponseData(requestLogId as Long?, response)
        }

        return null
    }
}