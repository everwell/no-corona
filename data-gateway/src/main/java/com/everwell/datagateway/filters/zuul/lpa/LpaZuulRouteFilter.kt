package com.everwell.datagateway.filters.zuul.lpa

import com.everwell.datagateway.constants.SecurityConstants
import com.everwell.datagateway.filters.zuul.base.BaseZuulRouteFilter
import com.everwell.datagateway.service.BaseRestService
import com.everwell.datagateway.service.RegistryRestService
import com.netflix.zuul.context.RequestContext
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants

open class LpaZuulRouteFilter() : BaseZuulRouteFilter() {

    @Autowired
    lateinit var registryRestService : RegistryRestService


    override var thisURI: String
        get() = "/lpa"
        set(value) {}

    init {
        // ToDo: Add / Change endpoints as LPA knows which endpoints to be called
        urlMapping.put("/lpa/v1/users","/v1/users/{ssoId}")
        urlMapping.put("/lpa/v1/user/validate-episode","/v1/user/validate-episode")
        urlMapping.put("/lpa/v1/available-lpa-lab", "/v1/hierarchy/children/lab")
    }

    override fun getHeadersForAuth(context: RequestContext): MutableMap<String, String> {
        val map = mutableMapOf<String, String>()
        map[SecurityConstants.HEADER_STRING] = SecurityConstants.TOKEN_PREFIX + " " + registryRestService.getAuthToken(appProperties.clientIdLpa)!!
        return map
    }

    override fun getRestService(): BaseRestService {
        return registryRestService
    }

    override fun filterType(): String {
        return FilterConstants.ROUTE_TYPE
    }

    override fun filterOrder(): Int {
        return FilterConstants.PRE_DECORATION_FILTER_ORDER + 12
    }
}