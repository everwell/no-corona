package com.everwell.datagateway.filters;


import com.everwell.datagateway.entities.Client;
import com.everwell.datagateway.service.ClientService;
import com.everwell.datagateway.utils.JWTUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.FilterChain;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.everwell.datagateway.constants.SecurityConstants.HEADER_STRING;
import static com.everwell.datagateway.constants.SecurityConstants.TOKEN_PREFIX;

public class JWTAuthorizationFilter extends BasicAuthenticationFilter {

    private static final Logger LOGGER = LoggerFactory.getLogger(JWTAuthorizationFilter.class);

    static Map<String, String> CLIENT_IP_CACHE = new HashMap<String, String>();

    @Autowired
    private ClientService clientService;

    public JWTAuthorizationFilter(AuthenticationManager authManager) {
        super(authManager);
    }

    @Override
    protected void doFilterInternal(HttpServletRequest req,
                                    HttpServletResponse res,
                                    FilterChain chain) throws IOException, ServletException {
        if(clientService==null){
            ServletContext servletContext = req.getServletContext();
            WebApplicationContext webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
            clientService = webApplicationContext.getBean(ClientService.class);
        }


        String header = req.getHeader(HEADER_STRING);

        if (header == null || !header.startsWith(TOKEN_PREFIX)) {
            chain.doFilter(req, res);
            return;
        }

        UsernamePasswordAuthenticationToken authentication = getAuthentication(req);

        SecurityContextHolder.getContext().setAuthentication(authentication);
        chain.doFilter(req, res);
    }

    private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest request) {
        LOGGER.info("Client IP :: " + request.getRemoteAddr());
        String token = request.getHeader(HEADER_STRING);
        String user = getClientFromRequestIP(request.getRemoteAddr());
        if (user == null && token != null) {
            // parse the token.
            user = JWTUtil.parseJWT(token);
        }
        LOGGER.info("Client Username :: "+user);
        return (user != null) ? new UsernamePasswordAuthenticationToken(user, null, new ArrayList<>()) : null;
    }

    private String getClientFromRequestIP(String referer) {
        String IP_KEY_PREFIX = "CLIENT_IP_";
        String userName = null;
        if (referer != null) {
            userName = CLIENT_IP_CACHE.get(IP_KEY_PREFIX + referer);
            if (userName == null) {
                Client client = clientService.findByIp(referer);
                if (client != null) {
                    userName = client.getUsername();
                    CLIENT_IP_CACHE.put(IP_KEY_PREFIX + referer, userName);
                }
            }
        }
        return userName;
    }
}
