package com.everwell.datagateway.filters.zuul.hubReports

import com.everwell.datagateway.constants.Constants
import com.everwell.datagateway.constants.GenericConstants
import com.everwell.datagateway.constants.ZuulConstants
import com.everwell.datagateway.filters.zuul.base.BaseZuulRouteFilter
import com.everwell.datagateway.service.BaseRestService
import com.everwell.datagateway.service.HubReportsRestService
import com.netflix.zuul.context.RequestContext
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants

open class HubReportsZuulRouteFilter : BaseZuulRouteFilter() {

    @Autowired
    lateinit var hubReportsRestService: HubReportsRestService

    override var thisURI: String
        get() = "/reports"
        set(value) {}

    override fun getHeadersForAuth(context: RequestContext): MutableMap<String, String> {
        val map = mutableMapOf<String, String>()
        map[GenericConstants.auth_header] = GenericConstants.auth_type + hubReportsRestService.getAuthToken(context.request.getHeader(Constants.CLIENT_ID))!!
        map[ZuulConstants.CLIENT_ID_HUB_REPORTS_IDENTIFIER] = context.request.getHeader(Constants.CLIENT_ID)
        return map
    }

    override fun getUrlMapping(context: RequestContext): String? {
        return context.request.requestURI.substring(thisURI.length)
    }

    override fun getRestService(): BaseRestService {
        return hubReportsRestService
    }

    override fun filterType(): String {
        return FilterConstants.ROUTE_TYPE
    }

    override fun filterOrder(): Int {
        return FilterConstants.PRE_DECORATION_FILTER_ORDER + 10
    }
}