package com.everwell.datagateway.controllers;


import com.everwell.datagateway.enums.AuthenticationCredentialTypeEnum;
import com.everwell.datagateway.enums.StatusCodeEnum;
import com.everwell.datagateway.models.dto.AuthenticationDetailsDto;
import com.everwell.datagateway.models.request.SubscribeEventRequest;
import com.everwell.datagateway.models.response.MessageApiResponse;
import com.everwell.datagateway.service.ClientService;
import com.everwell.datagateway.service.ConsumerService;
import com.everwell.datagateway.service.EventService;
import com.everwell.datagateway.service.RestService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@CrossOrigin
public class ClientController {

    @Autowired
    private ClientService clientService;

    @Autowired
    private EventService eventService;


    @GetMapping("/test-connection")
    public ResponseEntity<MessageApiResponse<String>> testConnection() throws Exception {
        StatusCodeEnum success = StatusCodeEnum.SUCCESS;
        return new ResponseEntity<>(new MessageApiResponse<>(success.getMessage()), success.getCode());
    }

    @ApiOperation(value = "Subscribe to event", authorizations = { @Authorization(value="jwtToken") })
    @PostMapping("/subscribe-event")
    public ResponseEntity<MessageApiResponse<String>> subscribeEvent(@RequestBody @Valid SubscribeEventRequest subscribeEventRequest)  {
        subscribeEventRequest.validate();
        if (!eventService.isEventExists(subscribeEventRequest.getEventName())) {  //Event doesn't Exists
            StatusCodeEnum eventNotFound = StatusCodeEnum.EVENT_NOT_FOUND;
            return new ResponseEntity<>(new MessageApiResponse<>(eventNotFound.getMessage()), eventNotFound.getCode());
        } else {
            if (clientService.subscribeEvent(subscribeEventRequest)) {    //Event Exists
                StatusCodeEnum success = StatusCodeEnum.SUCCESS;
                return new ResponseEntity<>(new MessageApiResponse<>(success.getMessage()), success.getCode());
            } else { //Event Exists but already Subscribed to that event
                StatusCodeEnum cannotSubscribeToEvent = StatusCodeEnum.CANNOT_SUBSCRIBE_TO_EVENT;
                return new ResponseEntity<>(new MessageApiResponse<>(cannotSubscribeToEvent.getMessage()), cannotSubscribeToEvent.getCode());
            }
        }
    }

}
