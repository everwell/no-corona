package com.everwell.datagateway.controllers

import com.everwell.datagateway.constants.Constants.WIX_CLIENT_NAME
import com.everwell.datagateway.entities.Client
import com.everwell.datagateway.enums.EventEnum
import com.everwell.datagateway.service.ClientService
import com.everwell.datagateway.service.PublisherService
import com.everwell.datagateway.utils.WixUtils
import io.swagger.annotations.ApiOperation
import lombok.Setter
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import java.util.*



@RestController
open class WixController {
    private val LOGGER = LoggerFactory.getLogger(WixController::class.java)

    @Autowired
    lateinit var clientService:ClientService

    @Autowired
    lateinit var publisherService: PublisherService


    @Setter
    @Value("\${wix.publicKey:}")
    lateinit var wixPublicKey: String


    @ApiOperation(value = "Receive new post/blog creation event from wix")
    @PostMapping("/event/new/blog")
    @Throws(Exception::class)
    open fun newBlogEvent(@RequestBody rawRequest: String): ResponseEntity<Boolean>{
        LOGGER.info("[newBlogEvent] request received: $rawRequest")
        val chunks = rawRequest.split(".")
        var status=false
        var httpStatus: HttpStatus = HttpStatus.OK
        if (chunks.size==3 && WixUtils.validateWixSignature(wixPublicKey,chunks.get(0), chunks.get(1), chunks.get(2))) {
//            val header = String(Base64.getUrlDecoder().decode(chunks.get(0))) // First chunk is alway header
            val payload = String(Base64.getUrlDecoder().decode(chunks.get(1)))
            val client = clientService.findByUsername(WIX_CLIENT_NAME)
            if(null != client) {
                setAuthentication(client)
                status = publisherService.publishRequestForWix(payload, EventEnum.WIX_NEW_BLOG_POST_EVENT.eventName)
            } else {
                httpStatus = HttpStatus.NOT_FOUND
            }

        } else {
            httpStatus = HttpStatus.BAD_REQUEST
        }
        return ResponseEntity<Boolean>(status, httpStatus)

    }

    private fun setAuthentication(client: Client) {
        val ctx = SecurityContextHolder.createEmptyContext()
        ctx.authentication = UsernamePasswordAuthenticationToken(client.username, null, emptyList())
        SecurityContextHolder.setContext(ctx)
    }
}