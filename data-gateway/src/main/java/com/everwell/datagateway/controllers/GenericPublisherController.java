package com.everwell.datagateway.controllers;

import com.everwell.datagateway.enums.StatusCodeEnum;
import com.everwell.datagateway.exceptions.NotFoundException;
import com.everwell.datagateway.models.request.GenericPublishRequest;
import com.everwell.datagateway.models.request.RefIdStatusRequest;
import com.everwell.datagateway.models.response.PublisherResponse;
import com.everwell.datagateway.models.response.RefIdStatusResponse;
import com.everwell.datagateway.models.response.Response;
import com.everwell.datagateway.service.ClientRequestsService;
import com.everwell.datagateway.service.EventService;
import com.everwell.datagateway.service.PublisherService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class GenericPublisherController {

    private static Logger LOGGER = LoggerFactory.getLogger(GenericPublisherController.class);

    @Autowired
    private EventService eventService;

    @Autowired
    private PublisherService publisherService;

    @Autowired
    private ClientRequestsService clientRequestsService;

    @ApiOperation(value = "Publish Event", authorizations = {@Authorization(value = "jwtToken")})
    @PostMapping("/generic-publish")
    public ResponseEntity genericPublisher(@Valid @RequestBody GenericPublishRequest genericPublishRequest) {
        LOGGER.debug("[genericPublisher] request received - " + genericPublishRequest);
        String eventName = genericPublishRequest.getEventName();
        if (!eventService.isEventExists(eventName)) {
            throw new NotFoundException(StatusCodeEnum.EVENT_NOT_FOUND);
        }
        PublisherResponse publisherResponse = publisherService.publishRequest(genericPublishRequest.getData(), eventName);
        return new ResponseEntity(new Response<>(true, publisherResponse), HttpStatus.OK);
    }

    @ApiOperation(value = "Get Status of Reference Id", authorizations = {@Authorization(value = "jwtToken")})
    @PostMapping("/get-status")
    public ResponseEntity getStatusOfEvent(@Valid @RequestBody RefIdStatusRequest refIdStatusRequest) {
        RefIdStatusResponse refIdStatusResponse = clientRequestsService.getStatusByRefId(refIdStatusRequest);
        return new ResponseEntity(new Response<>(true, refIdStatusResponse), HttpStatus.OK);
    }
}
