package com.everwell.datagateway.controllers;

import com.everwell.datagateway.constants.Constants;
import com.everwell.datagateway.enums.EventEnum;
import com.everwell.datagateway.enums.StatusCodeEnum;
import com.everwell.datagateway.models.request.CallRequest;
import com.everwell.datagateway.models.response.MessageApiResponse;
import com.everwell.datagateway.service.PublisherService;
import com.everwell.datagateway.utils.SentryUtils;
import com.everwell.datagateway.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class CallController extends BaseController {
    private static Logger LOGGER = LoggerFactory.getLogger(CallController.class);

    @Autowired
    private PublisherService publisherService;

    @GetMapping("/process-call")
    public ResponseEntity<MessageApiResponse<String>> processCall(@Valid CallRequest callRequest) {
        try {
            setAuthentication(Constants.PROCESS_CALL_CLIENT_NAME);
            callRequest.formatRequest();
            LOGGER.debug("Call request received from: " + callRequest.getDeployment() + " and query: " + callRequest);
            callRequest.setQueryParamsString(callRequest.toString());
            publisherService.publishRequest(Utils.asJsonString(callRequest), EventEnum.REGISTRY_PROCESS_CALL.getEventName());
            return new ResponseEntity<>(new MessageApiResponse<>("SUCCESS"), StatusCodeEnum.SUCCESS.getCode());
        } catch (Exception ex) {
            SentryUtils.captureException(ex);
            return new ResponseEntity<>(new MessageApiResponse<>("FAILURE"), StatusCodeEnum.INTERNAL_SERVER_ERROR.getCode());
        }
    }
}
