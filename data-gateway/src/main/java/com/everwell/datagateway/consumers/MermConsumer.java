package com.everwell.datagateway.consumers;

import com.everwell.datagateway.constants.QueueConstants;
import com.everwell.datagateway.enums.ConsumerQueueInfoEnum;
import com.everwell.datagateway.exceptions.NotFoundException;
import com.everwell.datagateway.exceptions.RedisException;
import com.everwell.datagateway.exceptions.RestTemplateException;
import com.everwell.datagateway.exceptions.URLDisabledException;
import com.everwell.datagateway.models.dto.EventStreamingDTO;
import com.everwell.datagateway.models.dto.PublishToSubscriberUrlDTO;
import com.everwell.datagateway.service.ConsumerService;
import com.everwell.datagateway.utils.SentryUtils;
import com.everwell.datagateway.utils.Utils;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class MermConsumer implements Consumer, ConsumerWebhook {

    private static Logger LOGGER = LoggerFactory.getLogger(MermConsumer.class);

    @Autowired
    private ConsumerService consumerService;

    @Override
    @RabbitListener(queues = QueueConstants.SHARE_MERM_EVENT, containerFactory = "rmqPrefetchCount20", concurrency = "3")
    public void consume(Message message) {
        LOGGER.debug("Message from queue is " + new String(message.getBody()));
        outgoingWebhook(QueueConstants.SHARE_MERM_EVENT, message);
    }

    @Override
    public void outgoingWebhook(String consumer, Message message) {
        try {
            EventStreamingDTO eventStreamingDTO = Utils.convertStrToObject(new String(message.getBody()), EventStreamingDTO.class);
            String eventName = ConsumerQueueInfoEnum.getEventName(consumer, eventStreamingDTO.getEventName());
            String clientName = ConsumerQueueInfoEnum.getClientName(consumer);
            String data = Utils.asJsonString(eventStreamingDTO.getField());
            consumerService.publishToSubscriberUrl(new PublishToSubscriberUrlDTO(clientName, eventName, data, null, true));
        } catch (NotFoundException | URLDisabledException e) {
            LOGGER.warn("NotFound or URLDisabled exception thrown");
        }  catch (RestTemplateException e) {
            SentryUtils.captureException(e);
            throw e;
        } catch (RedisException e) {
            SentryUtils.captureException(e);
            throw e;
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}
