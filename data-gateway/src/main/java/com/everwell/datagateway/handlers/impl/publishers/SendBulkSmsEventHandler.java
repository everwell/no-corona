package com.everwell.datagateway.handlers.impl.publishers;

import com.everwell.datagateway.constants.Constants;
import com.everwell.datagateway.entities.PublisherQueueInfo;
import com.everwell.datagateway.enums.EventEnum;
import com.everwell.datagateway.enums.StatusCodeEnum;
import com.everwell.datagateway.exceptions.ForbiddenException;
import com.everwell.datagateway.handlers.EventHandler;
import com.everwell.datagateway.publishers.RMQPublisher;
import com.everwell.datagateway.utils.SentryUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class SendBulkSmsEventHandler extends EventHandler {

    @Autowired
    private RMQPublisher rmqPublisher;
    @Override
    public EventEnum getEventEnumerator() {
        return EventEnum.SEND_BULK_SMS;
    }

    public void publishEvent(Object data, PublisherQueueInfo publisherQueueInfo, Long clientId) {
        try {
            if(null == clientId)
                throw new ForbiddenException(StatusCodeEnum.ACCESSIBLE_CLIENT_ID_REQUIRED.getMessage());
            Map<String, Object> headers = new HashMap<>();
            headers.put(Constants.RMQ_HEADER_CLIENT_ID_INS_FIELD_NAME,clientId.toString());
            rmqPublisher.send(data,publisherQueueInfo.getRoutingKey(),publisherQueueInfo.getExchange(),headers);
        } catch (Exception e) {
            SentryUtils.captureException(e);
        }
    }

}
