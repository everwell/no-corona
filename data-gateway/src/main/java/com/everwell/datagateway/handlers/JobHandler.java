package com.everwell.datagateway.handlers;

import com.everwell.datagateway.service.TriggerService;
import org.jobrunr.jobs.annotations.Job;
import org.jobrunr.spring.annotations.Recurring;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class JobHandler {
    @Autowired
    TriggerService triggerService;

    @Recurring(id = "everyday-job-scheduler", cron = "0 0 * * *")
    @Job
    public void everydayJobScheduler()
    {
        triggerService.readTriggerTable();
    }
}
