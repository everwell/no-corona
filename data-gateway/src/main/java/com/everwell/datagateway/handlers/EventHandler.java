package com.everwell.datagateway.handlers;

import com.everwell.datagateway.constants.Constants;
import com.everwell.datagateway.entities.PublisherQueueInfo;
import com.everwell.datagateway.enums.EventEnum;
import com.everwell.datagateway.models.dto.PublisherData;
import com.everwell.datagateway.models.request.ReferenceIdRMQRequest;
import com.everwell.datagateway.publishers.RMQPublisher;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.Map;

public abstract class EventHandler {

    @Autowired
    private RMQPublisher rmqPublisher;

    public abstract EventEnum getEventEnumerator();

    public void publishEvent(PublisherData publisherData, PublisherQueueInfo publisherQueueInfo) {
        ReferenceIdRMQRequest referenceIdRMQRequest = new ReferenceIdRMQRequest();
        referenceIdRMQRequest.setUserName(publisherData.getClientName());
        referenceIdRMQRequest.setUserId(publisherData.getClientId());
        referenceIdRMQRequest.setData(publisherData.getData());
        referenceIdRMQRequest.setReferenceId(publisherData.getReferenceId());
        referenceIdRMQRequest.setMicroServiceClientId(publisherData.getMicroServiceClientId());
        rmqPublisher.publish(referenceIdRMQRequest, publisherQueueInfo.getExchange(), publisherQueueInfo.getRoutingKey(), publisherQueueInfo.getReplyToRoutingKey(), publisherData.getReferenceId().toString()); // check for true false here
    }

    public void publishEvent(Object data, PublisherQueueInfo publisherQueueInfo,Long accessibleClient) {
        Map<String, Object> headers = new HashMap<>();
        headers.put(Constants.RMQ_HEADER_CLIENT_ID_INS_FIELD_NAME,accessibleClient);
        rmqPublisher.send(data,publisherQueueInfo.getRoutingKey(),publisherQueueInfo.getExchange(),headers);
    }
}
