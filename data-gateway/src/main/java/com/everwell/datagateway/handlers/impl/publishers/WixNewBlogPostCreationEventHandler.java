package com.everwell.datagateway.handlers.impl.publishers;

import com.everwell.datagateway.constants.Constants;
import com.everwell.datagateway.entities.PublisherQueueInfo;
import com.everwell.datagateway.enums.EventEnum;
import com.everwell.datagateway.handlers.EventHandler;
import com.everwell.datagateway.publishers.RMQPublisher;
import com.everwell.datagateway.utils.SentryUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class WixNewBlogPostCreationEventHandler extends EventHandler {

    @Autowired
    private RMQPublisher rmqPublisher;
    @Override
    public EventEnum getEventEnumerator() {
        return EventEnum.WIX_NEW_BLOG_POST_EVENT;
    }
}
