package com.everwell.datagateway.handlers.impl.publishers;

import com.everwell.datagateway.enums.EventEnum;
import com.everwell.datagateway.handlers.EventHandler;
import org.springframework.stereotype.Component;

@Component
public class AddMermEntityMappingHandler extends EventHandler {

    @Override
    public EventEnum getEventEnumerator() {
        return EventEnum.ADD_MERM_ENTITY_MAPPING;
    }

}
