package com.everwell.datagateway.exceptions;

public class ForbiddenException extends RuntimeException{
    public ForbiddenException(String exception) {
        super(exception);
    }
}
