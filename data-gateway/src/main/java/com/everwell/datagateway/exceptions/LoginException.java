package com.everwell.datagateway.exceptions;


import com.everwell.datagateway.enums.StatusCodeEnum;
import lombok.Getter;
import lombok.Setter;

public class LoginException extends RuntimeException {

    @Getter
    @Setter
    private StatusCodeEnum statusCodeEnum;
    public LoginException(){}
    public LoginException(StatusCodeEnum statusCodeEnum)
    {
        super(statusCodeEnum.getMessage());
        this.statusCodeEnum=statusCodeEnum;
    }




}
