package com.everwell.datagateway.utils;

import com.everwell.datagateway.exceptions.ValidationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.security.spec.KeySpec;
import java.util.Base64;

public class EncryptUtil {

    private static final String ENCRYPT_ALGO = "AES/GCM/NoPadding";
    private static final String SECRET_KEY_FACTORY_ALGORITHM = "PBKDF2WithHmacSHA256";
    private static final String SECRET_KEY_ALGORITHM = "AES";
    private static final String PASSWORD_RANDOM = "JBjndk";

    private static final int TAG_LENGTH_BIT = 128;
    private static final int IV_LENGTH_BYTE = 12;
    private static final int SALT_LENGTH_BYTE = 16;
    private static final int ITERATION_COUNT = 1000;
    private static final Charset UTF_8 = StandardCharsets.UTF_8;

    private static final Logger LOGGER = LoggerFactory.getLogger(EncryptUtil.class);

    public static byte[] getRandomNonce(int numBytes) {
        try{
            byte[] nonce = new byte[numBytes];
            new SecureRandom().nextBytes(nonce);
            return nonce;
        }catch (Exception ex) {
            LOGGER.error("[getRandomNonce] unable to  get random nonce");
            throw new ValidationException("Unable to get random nonce" + ex);
        }
    }

    public static SecretKey getSecretKeyFromPassword(char[] password, byte[] salt) {
        try{
            SecretKeyFactory factory = SecretKeyFactory.getInstance(SECRET_KEY_FACTORY_ALGORITHM);
            KeySpec spec = new PBEKeySpec(password, salt, ITERATION_COUNT, 256);
            return new SecretKeySpec(factory.generateSecret(spec).getEncoded(), SECRET_KEY_ALGORITHM);
        }catch (Exception ex) {
            LOGGER.error("[getSecretKeyFromPassword] unable to get secret key from password");
            throw new ValidationException("unable to get secret key from password" + ex);
        }
    }

    public static String encrypt(String plainText, String password) throws Exception {
        return encrypt(plainText.getBytes(UTF_8), password+PASSWORD_RANDOM);
    }

    public static String encrypt(byte[] pText, String password) {
        try {
            byte[] salt = getRandomNonce(SALT_LENGTH_BYTE);
            byte[] iv = getRandomNonce(IV_LENGTH_BYTE);
            SecretKey aesKeyFromPassword = getSecretKeyFromPassword(password.toCharArray(), salt);
            Cipher cipher = Cipher.getInstance(ENCRYPT_ALGO);
            cipher.init(Cipher.ENCRYPT_MODE, aesKeyFromPassword, new GCMParameterSpec(TAG_LENGTH_BIT, iv));
            byte[] cipherText = cipher.doFinal(pText);
            byte[] cipherTextWithIvSalt = ByteBuffer.allocate(iv.length + salt.length + cipherText.length)
                    .put(iv)
                    .put(salt)
                    .put(cipherText)
                    .array();
            return Base64.getEncoder().encodeToString(cipherTextWithIvSalt);
        }catch (Exception ex) {
            LOGGER.error("[encrypt] unable to encrypt data");
            throw new ValidationException("Unable to encrypt data" + ex);
        }
    }

    public static String decrypt(String cText, String password) {
        try {
            password = password + PASSWORD_RANDOM;
            byte[] decode = Base64.getDecoder().decode(cText.getBytes(UTF_8));
            ByteBuffer bb = ByteBuffer.wrap(decode);
            byte[] iv = new byte[IV_LENGTH_BYTE];
            bb.get(iv);
            byte[] salt = new byte[SALT_LENGTH_BYTE];
            bb.get(salt);
            byte[] cipherText = new byte[bb.remaining()];
            bb.get(cipherText);
            SecretKey aesKeyFromPassword = getSecretKeyFromPassword(password.toCharArray(), salt);
            Cipher cipher = Cipher.getInstance(ENCRYPT_ALGO);
            cipher.init(Cipher.DECRYPT_MODE, aesKeyFromPassword, new GCMParameterSpec(TAG_LENGTH_BIT, iv));
            byte[] plainText = cipher.doFinal(cipherText);
            return new String(plainText, UTF_8);
        }catch (Exception ex) {
            LOGGER.error("[decrypt] Unable to decrypt data");
            throw new ValidationException("Unable to decrypt data" + ex);
        }
    }

}
