package com.everwell.datagateway.utils;


import com.everwell.datagateway.constants.Constants;
import io.sentry.Sentry;
import io.sentry.SentryEvent;
import io.sentry.protocol.Request;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Component
public class SentryUtils {

    private static String releaseVersion;

    @Value("${data.gateway.sentry.release.version}")
    public void setVersion(String version) {
        this.releaseVersion = version;
    }

    static {
        init();
    }

    /**
     * Method to initialize sentry options which enables sentry to pick configs from sentry.properties
     */
    public static void init() {
        Sentry.init(options -> {
            options.setEnableExternalConfiguration(true);
        });
    }

    /**
     * Method to build SentryEvent which can be then be captured
     *
     * @param ex - Exception which needs to be captured in sentry event
     * @return sentry event corresponding to given error
     */
    private static SentryEvent eventBuilder(Exception ex) {
        SentryEvent sentryEvent = new SentryEvent();
        sentryEvent.setThrowable(ex);
        sentryEvent.setRelease(releaseVersion);
        return sentryEvent;
    }

    /**
     * Method to capture exception along with the web request
     *
     * @param e       - Exception which is to be captured
     * @param request - webRequest which is received
     * @throws IOException - throws IOException in case of reading invalid data
     */
    public static void captureException(Exception e, WebRequest request) throws IOException {
        SentryEvent sentryEvent = eventBuilder(e);
        sentryEvent.setRequest(formatWebRequest(request));
        Sentry.captureEvent(sentryEvent);
    }

    /**
     * Method to format webRequest to sentry Request object
     *
     * @param webRequest - webRequest which is received
     * @return - Converted Request object
     * @throws IOException - throws IOException in case of reading invalid data
     */
    private static Request formatWebRequest(WebRequest webRequest) throws IOException {
        HttpServletRequest httpServletRequest = ((ServletWebRequest) webRequest).getRequest();
        Request request = new Request();
        Map<String, String> headers = new HashMap<>();
        webRequest.getHeaderNames().forEachRemaining(h -> headers.put(h, webRequest.getHeader(h)));
        request.setUrl(httpServletRequest.getRequestURI());
        request.setQueryString(httpServletRequest.getQueryString());
        request.setHeaders(headers);
        request.setMethod(httpServletRequest.getMethod());
        request.setData(Utils.readRequestBody(httpServletRequest.getInputStream()));
        return request;
    }

    /**
     * Method to capture exception
     *
     * @param e - Exception which is to be captured
     */
    public static void captureException(Exception e) {
        SentryEvent builder = eventBuilder(e);
        Sentry.captureEvent(builder);
    }

    /**
     * Method to capture exception along with extra data
     *
     * @param ex   - Exception which is to be captured
     * @param data - Extra data to be captured along with exception
     */
    public static void captureException(Exception ex, String data) {
        SentryEvent sentryEvent = eventBuilder(ex);
        if (StringUtils.hasText(data)) {
            sentryEvent.setExtra(Constants.DATA, data);
        }
        Sentry.captureEvent(sentryEvent);
    }

}
