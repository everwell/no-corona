package com.everwell.datagateway.utils

import io.jsonwebtoken.SignatureAlgorithm
import io.jsonwebtoken.impl.crypto.DefaultJwtSignatureValidator
import java.security.KeyFactory
import java.security.spec.X509EncodedKeySpec
import java.util.*

object WixUtils {
    fun validateWixSignature(wixPublicKey:String, header:String, payload: String, signature: String):Boolean{
        val sa = SignatureAlgorithm.RS256
        val decoded = Base64.getDecoder().decode(wixPublicKey)
        val kf = KeyFactory.getInstance("RSA")
        val generatedPublic = kf.generatePublic(X509EncodedKeySpec(decoded))
        val tokenWithoutSignature = header + "." + payload
        val validator = DefaultJwtSignatureValidator(sa, generatedPublic)
         return validator.isValid(tokenWithoutSignature, signature)
    }
}