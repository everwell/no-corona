package com.everwell.datagateway.utils;


import com.everwell.datagateway.constants.Constants;
import com.everwell.datagateway.enums.FHIRProfileEnum;
import com.everwell.datagateway.enums.FHIRStructureEnum;
import com.everwell.datagateway.models.dto.SharedResourceDto;
import org.hl7.fhir.r4.model.*;
import org.hl7.fhir.r4.model.DiagnosticReport.DiagnosticReportStatus;
import org.hl7.fhir.r4.model.Enumerations.AdministrativeGender;
import org.hl7.fhir.r4.model.MedicationRequest.MedicationRequestIntent;
import org.hl7.fhir.r4.model.MedicationRequest.MedicationRequestStatus;
import org.hl7.fhir.r4.model.Observation.ObservationStatus;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static com.everwell.datagateway.constants.Constants.FHIR_IDENTIFIER_SYSTEM_URL;
import static com.everwell.datagateway.enums.FHIRFieldMappingEnum.*;

/**
 * The FhirResourcePopulator class populates all the FHIR resources
 */
public class FHIRResourceUtil {

	public static Resource populateResource(FHIRProfileEnum profile, SharedResourceDto sharedResourceDto){

		switch (profile){

			case PATIENT: {
				Patient patient = new Patient();
				patient.setId(String.valueOf(sharedResourceDto.episodeId));
				Meta meta = new Meta();
				meta.setVersionId(String.valueOf(sharedResourceDto.episodeId));
				meta.setProfile(Collections.singletonList(new CanonicalType( profile.getProfileUrl())));
				patient.setMeta(meta);
				patient.addIdentifier()
						.setType(new CodeableConcept().setText(profile.getIdentifier()))
						.setValue(String.valueOf(sharedResourceDto.episodeId));
				AdministrativeGender gender = sharedResourceDto.gender == null ? null :
						sharedResourceDto.gender.equalsIgnoreCase(Constants.FHIR_FIELD_MALE) ? AdministrativeGender.MALE :
								sharedResourceDto.gender.equalsIgnoreCase(Constants.FHIR_FIELD_FEMALE) ? AdministrativeGender.FEMALE :
										AdministrativeGender.OTHER;
				patient.setGender(gender);
				List<HumanName> humanNames = new ArrayList<>();
				HumanName humanName = new HumanName().setText(sharedResourceDto.getName());
				humanNames.add(humanName);
				patient.setName(humanNames);
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				try{
					patient.setBirthDate(formatter.parse(sharedResourceDto.getDateOfBirth()));
				}
				catch (ParseException ignored){
				}
				return patient;
			}
			case PRACTITIONER: {
				Practitioner practitioner = new Practitioner();
				practitioner.setId(String.valueOf(sharedResourceDto.userId));
				Meta meta = new Meta();
				meta.setVersionId(String.valueOf(sharedResourceDto.episodeId));
				meta.setProfile(Collections.singletonList(new CanonicalType( profile.getProfileUrl())));
				practitioner.setMeta(meta);
				practitioner.addIdentifier()
						.setType(new CodeableConcept().setText(profile.getIdentifier())).setValue(String.valueOf(sharedResourceDto.userId));
				return practitioner;
			}
			case DIAGNOSTIC_REPORT: {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
				DiagnosticReport diagnosticReportLab = new DiagnosticReport();
				diagnosticReportLab.setId(sharedResourceDto.diagnosticsEntry.getKey());
				Meta meta = new Meta();
				meta.setVersionId(String.valueOf(sharedResourceDto.episodeId));
				meta.setProfile(Collections.singletonList(new CanonicalType( profile.getProfileUrl())));
				diagnosticReportLab.setMeta(meta);
				diagnosticReportLab.setStatus(DiagnosticReportStatus.FINAL);
				diagnosticReportLab.setCode(
						new CodeableConcept().setText(sharedResourceDto.diagnosticsEntry.getValue().getTestType()));
				diagnosticReportLab.setResultsInterpreter(
						Collections.singletonList(new Reference().setReference(FHIRProfileEnum.ORGANIZATION.getReferenceText() + sharedResourceDto.hierarchyId)));
				try {
					diagnosticReportLab.setIssued(sdf.parse(sharedResourceDto.diagnosticsEntry.getValue().requestDate));
					diagnosticReportLab.setIssuedElement(new InstantType(sdf.parse(sharedResourceDto.diagnosticsEntry.getValue().requestDate)));
				} catch (ParseException ignored) {
				}
				diagnosticReportLab.setResult(Collections.singletonList(new Reference().setReference(FHIRProfileEnum.OBSERVATION.getReferenceText() + sharedResourceDto.diagnosticsEntry.getKey())));
				diagnosticReportLab.setPerformer(Collections.singletonList(new Reference().setReference(FHIRProfileEnum.ORGANIZATION.getReferenceText() + sharedResourceDto.hierarchyId)));
				diagnosticReportLab.setConclusion(sharedResourceDto.diagnosticsEntry.getValue().getFinalInterpretation());
				return diagnosticReportLab;
			}
			case MEDICATION_REQUEST: {
				MedicationRequest medicationRequest = new MedicationRequest();
				medicationRequest.setId(sharedResourceDto.dispensationEntry.getKey());
				Meta meta = new Meta();
				meta.setVersionId(String.valueOf(sharedResourceDto.getEpisodeId()));
				meta.setProfile(Collections.singletonList(new CanonicalType(profile.getProfileUrl())));
				medicationRequest.setMeta(meta);
				medicationRequest.setStatus(sharedResourceDto.dispensationEntry.getValue().isActive() ? MedicationRequestStatus.ACTIVE : MedicationRequestStatus.COMPLETED);
				medicationRequest.setIntent(MedicationRequestIntent.ORIGINALORDER);
				medicationRequest.setMedication(new CodeableConcept().setText(sharedResourceDto.dispensationEntry.getValue().getProductName()));
				medicationRequest.setSubject(new Reference().setReference(FHIRProfileEnum.PATIENT.getReferenceText()+sharedResourceDto.episodeId));
				medicationRequest.setAuthoredOnElement(new DateTimeType(sharedResourceDto.dispensationEntry.getValue().getTimeStamp().split(" ")[0]));
				medicationRequest
						.setRequester(new Reference().setReference(FHIRProfileEnum.ORGANIZATION.getReferenceText()+sharedResourceDto.hierarchyId));
				populateDosageInstructions(medicationRequest,sharedResourceDto);
				return medicationRequest;
			}

			case OBSERVATION: {
				Observation observation = new Observation();
				observation.setId(sharedResourceDto.diagnosticsEntry.getKey());
				Meta meta = new Meta();
				meta.setVersionId(String.valueOf(sharedResourceDto.episodeId));
				meta.setProfile(Collections.singletonList(new CanonicalType( profile.getProfileUrl())));
				observation.setMeta(meta);
				observation.setStatus(ObservationStatus.FINAL);
				observation.setCode(new CodeableConcept()
						.setText(sharedResourceDto.diagnosticsEntry.getValue().getReasonForTesting()));
				observation.setSubject(new Reference().setReference(FHIRProfileEnum.PATIENT.getReferenceText()+sharedResourceDto.getEpisodeId()));
				observation.addPerformer().setReference(FHIRProfileEnum.ORGANIZATION.getReferenceText()+sharedResourceDto.getHierarchyId());
				return observation;
			}

			case ORGANIZATION: {
				Organization organization = new Organization();
				organization.setId(String.valueOf(sharedResourceDto.hierarchyId));
				Meta meta = new Meta();
				meta.setVersionId(String.valueOf(sharedResourceDto.getEpisodeId()));
				meta.setProfile(Collections.singletonList(new CanonicalType(profile.getProfileUrl())));
				organization.setMeta(meta);
				organization.addIdentifier()
						.setType(new CodeableConcept().setText(profile.getIdentifier()))
						.setSystem(FHIR_IDENTIFIER_SYSTEM_URL)
						.setValue(String.valueOf(sharedResourceDto.hierarchyId));
				organization.setName(sharedResourceDto.hierarchyName);
				return organization;
			}

			case ENCOUNTER: {
				Encounter encounter = new Encounter();
				encounter.setId(String.valueOf(sharedResourceDto.episodeId));
				Meta meta = new Meta();
				meta.setVersionId(String.valueOf(sharedResourceDto.getEpisodeId()));
				meta.setProfile(Collections.singletonList(new CanonicalType(profile.getProfileUrl())));
				encounter.setMeta(meta);
				encounter.setStatus(Encounter.EncounterStatus.FINISHED);
				encounter.setSubject(new Reference().setReference(FHIRProfileEnum.PATIENT.getReferenceText()+sharedResourceDto.getEpisodeId()));
				encounter.setServiceProvider(new Reference().setReference(FHIRProfileEnum.ORGANIZATION.getReferenceText()+sharedResourceDto.getHierarchyId()));
				encounter.setClass_(new Coding().setDisplay(null == sharedResourceDto.diagnosticsEntry ? FHIRStructureEnum.DISPENSATION.getTitle() : FHIRStructureEnum.DIAGNOSTICS.getTitle()));
				return encounter;
			}
		}

		return null;
	}

	private static void populateDosageInstructions(MedicationRequest medicationRequest, SharedResourceDto sharedResourceDto) {
		medicationRequest.addDosageInstruction(new Dosage().setText(DOSES_GIVEN.getLabel() + sharedResourceDto.dispensationEntry.getValue().dosesGiven));
		medicationRequest.addDosageInstruction(new Dosage().setText(DOSING_DATE.getLabel() + sharedResourceDto.dispensationEntry.getValue().dosingDate));
		medicationRequest.addDosageInstruction(new Dosage().setText(DOSING_DAYS.getLabel() + sharedResourceDto.dispensationEntry.getValue().noOfDays));
		medicationRequest.addDosageInstruction(new Dosage().setText(CASE_TYPE.getLabel() + sharedResourceDto.getTypeOfCase()));
		medicationRequest.addDosageInstruction(new Dosage().setText(REGIMEN_TYPE.getLabel() + sharedResourceDto.dispensationEntry.getValue().typeOfRegimen));
		medicationRequest.addDosageInstruction(new Dosage().setText(DIAGNOSIS_DATE.getLabel() + sharedResourceDto.getDiagnosisDate()));
		medicationRequest.addDosageInstruction(new Dosage().setText(DIAGNOSIS_BASIS.getLabel() +  sharedResourceDto.getBasisOfDiagnosis()));
		medicationRequest.addDosageInstruction(new Dosage().setText(TREATMENT_PHASE.getLabel() + sharedResourceDto.getTreatmentPhase()));
	}
}
