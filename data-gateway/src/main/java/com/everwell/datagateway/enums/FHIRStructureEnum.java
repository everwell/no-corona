package com.everwell.datagateway.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum FHIRStructureEnum {

    DIAGNOSTICS("Diagnostics","Diagnostic studies report","http://snomed.info/sct","721981007","3cf54fc4-0178-4127-bb99-b20711404881","https://nrces.in/ndhm/fhir/r4/StructureDefinition/DiagnosticReportRecord"),
    DISPENSATION("Dispensation","Prescription record","http://snomed.info/sct","440545006","bc3c6c57-2053-4d0e-ac40-139ccccff645","https://nrces.in/ndhm/fhir/r4/StructureDefinition/PrescriptionRecord");

    @Getter
    private final String name;

    @Getter
    private final String title;

    @Getter
    private final String codingSystem;

    @Getter
    private final String code;

    @Getter
    private final String value;

    @Getter
    private final String profileUrl;

    public static FHIRStructureEnum getEnumByName(String fhirType) {
        for(FHIRStructureEnum fse : FHIRStructureEnum.values()) {
            if(fse.name.equals(fhirType)){
                return fse;
            }
        }
        return null;
    }
}
