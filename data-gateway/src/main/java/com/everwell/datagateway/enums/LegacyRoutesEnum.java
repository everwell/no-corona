package com.everwell.datagateway.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

@Getter
@AllArgsConstructor
public enum LegacyRoutesEnum {
    PROCESS_MERM_EVENT("/process-merm-event"),
    GET_MERM_CONFIG("/get-merm-config"),
    CALL_LOGS("/call-logs"),
    INTERRA_CALL_LOGS("/interra/call-logs"),
    GLOBAL_LABS_CALL_LOGS("/global-labs-call-logs");

    public final String path;

    public static boolean isLegacyRoute(String route) {
        return Arrays.stream(values()).anyMatch(legacyRoutesEnum -> legacyRoutesEnum.getPath().equals(route));
    }

    public static String[] getAllPaths() {
        return Arrays.stream(values())
                .map(LegacyRoutesEnum::getPath)
                .toArray(String[]::new);
    }
}
