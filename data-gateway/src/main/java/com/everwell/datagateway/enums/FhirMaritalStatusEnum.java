package com.everwell.datagateway.enums;

import com.everwell.datagateway.constants.Constants;

public enum FhirMaritalStatusEnum {
    ANNULLED("A", "Annulled"),
    DIVORCED("D", "Divorced"),
    INTERLOCUTORY("I", "Interlocutory"),
    LEGALLY_SEPARATED("L", "Legally Separated"),
    MARRIED("M", "Married"),
    COMMON_LAW("C", "Common Law"),
    POLYGAMOUS("P", "Polygamous"),
    DOMESTIC_PARTNER("T", "Domestic partner"),
    UNMARRIED("U", "Unmarried"),
    NEVER_MARRIED("S", "Never Married"),
    WIDOWED("W", "Widowed");

    private final String code;
    private final String value;

    FhirMaritalStatusEnum(String code, String value) {
        this.code = code;
        this.value = value;
    }

    public String getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }

    public static String getValueByCode(String code) {
        for (FhirMaritalStatusEnum status : FhirMaritalStatusEnum.values()) {
            if (status.code.equalsIgnoreCase(code)) {
                return status.value;
            }
        }
        return Constants.UNKNOWN;
    }

    public static String getCodeByValue(String value) {
        for (FhirMaritalStatusEnum status : FhirMaritalStatusEnum.values()) {
            if (status.value.equalsIgnoreCase(value)) {
                return status.code;
            }
        }
        return Constants.UNKNOWN;
    }
}

