package com.everwell.datagateway.enums;

import com.everwell.datagateway.constants.ECBSSConstants;
import com.everwell.datagateway.constants.QueueConstants;
import lombok.Getter;

import static com.everwell.datagateway.constants.Constants.*;
import static com.everwell.datagateway.constants.QueueConstants.PATIENT_ENROLLMENT_GRAMENER_QUEUE;

public enum ConsumerQueueInfoEnum {


    PATIENT_ENROLLMENT_QUEUE(PATIENT_ENROLLMENT_GRAMENER_QUEUE, "gramener", "patient_enrollment"),
    REF_ID_REPLY_QUEUE(QueueConstants.REF_ID_REPLY_QUEUE),
    SHARE_DISPENSATION_QUEUE(QueueConstants.SHARE_DISPENSATION_QUEUE, "aushadi", "dispensation-share"),
    MERM_OpenMerm_QUEUE(QueueConstants.SHARE_MERM_EVENT, "everwell_gmri", "OpenMERM" ),
    MERM_HEARTBEAT_QUEUE(QueueConstants.SHARE_MERM_EVENT, "everwell_gmri", "Heartbeat" ),
    ECBSS_TRACKEDENTITYINSTANCE_QUEUE(QueueConstants.ECBSS_ADHERENCE_DATA_QUEUE, RTC_UGANDA_CLIENT, ECBSSConstants.TRACKEDENTITYINSTANCE_EVENT),
    ECBSS_SEND_ADHERENCE_DATA_QUEUE(QueueConstants.ECBSS_ADHERENCE_DATA_QUEUE, RTC_UGANDA_CLIENT, SEND_ADHERENCE_DATA_EVENT);
    @Getter
    private String queueName;
    @Getter
    private String clientName;
    @Getter
    private String eventName;


    ConsumerQueueInfoEnum(String queueName, String clientName, String eventName) {
        this.queueName = queueName;
        this.clientName = clientName;
        this.eventName = eventName;
    }

    ConsumerQueueInfoEnum(String queueName) {
        this.queueName = queueName;
    }

    public static String getClientName(String queueName) {
        for (ConsumerQueueInfoEnum consumerQueueInfoEnum : ConsumerQueueInfoEnum.values()) {
            if (consumerQueueInfoEnum.queueName.equals(queueName))
                return consumerQueueInfoEnum.clientName;
        }

        return null;
    }

    public static String getEventName(String queueName) {
        for (ConsumerQueueInfoEnum consumerQueueInfoEnum : ConsumerQueueInfoEnum.values()) {
            if (consumerQueueInfoEnum.queueName.equals(queueName))
                return consumerQueueInfoEnum.eventName;
        }

        return null;
    }

    public static String getEventName(String queueName, String eventName) {
        for (ConsumerQueueInfoEnum consumerQueueInfoEnum : ConsumerQueueInfoEnum.values()) {
            if(consumerQueueInfoEnum.queueName.equals(queueName) && consumerQueueInfoEnum.eventName.equals(eventName))
                return consumerQueueInfoEnum.eventName;
        }
        return null;
    }
}
