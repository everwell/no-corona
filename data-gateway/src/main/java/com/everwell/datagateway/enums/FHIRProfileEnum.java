package com.everwell.datagateway.enums;

import com.everwell.datagateway.models.dto.DiagnosticsFHIRDto;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum FHIRProfileEnum {

    PATIENT("Patient","Patient","Patient/","episodeId","https://nrces.in/ndhm/fhir/r4/StructureDefinition/Patient"),
    PRACTITIONER("Practitioner","Practitioner","Practitioner/","userId","https://nrces.in/ndhm/fhir/r4/StructureDefinition/Practitioner"),
    DIAGNOSTIC_REPORT("DiagnosticReport","","DiagnosticReport/","testId","https://nrces.in/ndhm/fhir/r4/StructureDefinition/DiagnosticReportLab"),
    MEDICATION_REQUEST("MedicationRequest","","MedicationRequest/","","https://nrces.in/ndhm/fhir/r4/StructureDefinition/MedicationRequest"),
    OBSERVATION("Observation","","Observation/","testId","https://nrces.in/ndhm/fhir/r4/StructureDefinition/Observation"),
    ORGANIZATION("Organization","Hierarchy","Organization/","","https://nrces.in/ndhm/fhir/r4/StructureDefinition/Organization"),
    COMPOSITION("DiagnosticReport","","Composition/","episodeId","https://nrces.in/ndhm/fhir/r4/StructureDefinition/DocumentBundle"),
    ENCOUNTER("Encounter","","Encounter/","episodeId","https://nrces.in/ndhm/fhir/r4/StructureDefinition/Encounter");

    @Getter
    private final String name;

    @Getter
    private final String identifier;

    @Getter
    private final String referenceText;

    @Getter
    private final String key;

    @Getter
    private final String profileUrl;
}
