package com.everwell.datagateway.models.request;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class UpdateUserMedicalRecordsRequest {

    public String consentId;
    private String transactionId;
    private String hiuPrivateKey;
    private String hiuNonce;
    private String encryptedJson;
    private String hipName;

    public UpdateUserMedicalRecordsRequest(String transactionId, String consentId, String hiuPrivateKey, String hiuNonce, String hipName) {
        this.transactionId = transactionId;
        this.consentId = consentId;
        this.hiuPrivateKey = hiuPrivateKey;
        this.hiuNonce = hiuNonce;
        this.hipName = hipName;
    }
}
