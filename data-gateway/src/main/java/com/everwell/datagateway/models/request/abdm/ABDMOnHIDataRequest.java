package com.everwell.datagateway.models.request.abdm;

import lombok.Data;

@Data
public class ABDMOnHIDataRequest {
    public String requestId;
    public String timestamp;
    public HiRequest hiRequest;
    public Error error;
    public Resp resp;

    @Data
    public static class Error{
        public int code;
        public String message;
    }

    @Data
    public static class HiRequest{
        public String transactionId;
        public String sessionStatus;
    }

    @Data
    public static class Resp{
        public String requestId;
    }
}
