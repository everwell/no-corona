package com.everwell.datagateway.models.request;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@Data
public class ABDMAddUpdateConsentRequest {
    @NotNull
    public ABDMConsentNotification notification;
    @NotBlank
    public String requestId;
    public String timestamp;


    @Getter
    @Setter
    public static class ABDMConsentNotification {
        public Object consentDetail;
        @NotBlank
        public String status;
        public String signature;
        @NotBlank
        public String consentId;
        public boolean grantAcknowledgement;
    }

}
