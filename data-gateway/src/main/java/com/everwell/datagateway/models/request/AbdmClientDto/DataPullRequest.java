package com.everwell.datagateway.models.request.AbdmClientDto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Getter
@Setter
@Data
public class DataPullRequest{
    @NotBlank
    public String transactionId;
    @NotBlank
    public String requestId;
    public Date timestamp;
    @NotNull
    public HiRequest hiRequest;

    @Getter
    @Setter
    public static class Consent {
        @NotBlank
        public String id;
    }
    @Getter
    @Setter
    public static class DateRange {
        public String from;
        @JsonProperty("to")
        public String till;
    }

    @Getter
    @Setter
    public static class DhPublicKey {
        public String expiry;
        public String parameters;
        @NotBlank
        public String keyValue;
    }

    @Getter
    @Setter
    public static class HiRequest {
        public Consent consent;
        public DateRange dateRange;
        public String dataPushUrl;
        public KeyMaterial keyMaterial;
    }

    @Getter
    @Setter
    public static class KeyMaterial {
        public String cryptoAlg;
        public String curve;
        public DhPublicKey dhPublicKey;
        @NotBlank
        public String nonce;
    }

}
