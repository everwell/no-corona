package com.everwell.datagateway.models.dto;

import com.everwell.datagateway.models.request.abdm.HIDataRequest;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ConsentFetchRedisDto {

    HIDataRequest hiDataRequest;
    String hipName;
    String privateKey;
}
