package com.everwell.datagateway.models.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PublisherResponse {
    @JsonProperty("RefId")
    private Long referenceId;
}
