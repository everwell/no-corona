package com.everwell.datagateway.models.dto.ecbss;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AdherenceDetailsDto {
    public String orgUnit;
    public String program;
    public String programStage;
    public String eventDate;
    public String trackedEntityInstance;
    public ArrayList<DataValue> dataValues;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class DataValue {
        public String dataElement;
        public String value;
    }
}
