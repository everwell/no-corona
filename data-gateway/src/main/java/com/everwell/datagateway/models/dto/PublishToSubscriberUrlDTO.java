package com.everwell.datagateway.models.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class PublishToSubscriberUrlDTO {
    private String clientName;
    private String eventName;
    private String message;
    private Long referenceId;
    private boolean saveOutgoingWebhookLog;
}
