package com.everwell.datagateway.models.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EpisodeSearchResult {
    private long totalHits;
    private List<EpisodeIndex> episodeIndexList;
    private Map<String, Map<String, Long>> aggregationResults;
    private String requestIdentifier;
    private String scrollId;

    public EpisodeSearchResult(List<EpisodeIndex> episodeIndexList) {
        this.episodeIndexList = episodeIndexList;
        this.totalHits = episodeIndexList.size();
    }

    public EpisodeSearchResult(Map<String, Map<String, Long>> aggregationResults) {
        this.aggregationResults = aggregationResults;
    }

    public EpisodeSearchResult(long totalHits, String requestIdentifier) {
        this.totalHits = totalHits;
        this.requestIdentifier = requestIdentifier;
    }
}
