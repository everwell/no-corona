package com.everwell.datagateway.models.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class RefIdStatusRequest {
    @NotNull
    @Min(value = 1, message = "Please enter valid value")
    private Long referenceId;
}
