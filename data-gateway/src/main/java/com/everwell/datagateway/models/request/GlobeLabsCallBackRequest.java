package com.everwell.datagateway.models.request;

import com.everwell.datagateway.constants.CallConstants;
import com.everwell.datagateway.utils.Utils;
import com.fasterxml.jackson.core.type.TypeReference;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@NoArgsConstructor
@Getter
@Setter
public class GlobeLabsCallBackRequest extends DeploymentCallBackRequest{
    public GlobeLabsCallBackRequest(String requestSMSList) throws IOException {
        Map<String, Object> inboundSMSMessageInput = Utils.convertStrToObject(requestSMSList, new TypeReference<Map<String, Map<String, Object>>>() {
        }).get(CallConstants.INBOUND_SMS_MESSAGE_LIST);
        List<Map<String, String>> inboundSMSMessageList = (List<Map<String, String>>) inboundSMSMessageInput.get(CallConstants.INBOUND_SMS_MESSAGE);
        if(!CollectionUtils.isEmpty(inboundSMSMessageList)){
            Map<String, String> inboundSMSMessage = inboundSMSMessageList.get(0);
            this.date = Utils.convertDateStringFormat(inboundSMSMessage.get(CallConstants.DATE_TIME), CallConstants.DATE_TIME_FORMAT_3, CallConstants.DATE_TIME_FORMAT_2);
            this.to = !StringUtils.isEmpty(inboundSMSMessage.get(CallConstants.DESTINATION_ADDRESS)) ?
                    inboundSMSMessage.get(CallConstants.DESTINATION_ADDRESS).replace("tel:", "") :
                    "";
            this.from = !StringUtils.isEmpty(inboundSMSMessage.get(CallConstants.SENDER_ADDRESS)) ?
                    inboundSMSMessage.get(CallConstants.SENDER_ADDRESS).replace("tel:", "") :
                    "";
            this.deploymentCode = CallConstants.GLOBE_LABS_DEPLOYMENT_CODE; // if only one deployment uses globe labs
            this.linkId = inboundSMSMessage.get(CallConstants.MESSAGE_ID);
            this.text = inboundSMSMessage.get(CallConstants.MESSAGE);
            this.id = inboundSMSMessage.get(CallConstants.ID);
            this.body = inboundSMSMessage.entrySet().stream()
                    .map(entry -> entry.getKey() + "=" + entry.getValue())
                    .collect(Collectors.joining("&"));
        }
    }
}
