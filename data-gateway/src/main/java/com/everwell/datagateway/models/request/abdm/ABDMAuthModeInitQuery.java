package com.everwell.datagateway.models.request.abdm;
import lombok.Getter;

import static com.everwell.datagateway.constants.Constants.ABDM_TRANSFER_NOTIFIER_TYPE;

@Getter
public class ABDMAuthModeInitQuery {
    public String id;
    public String purpose;
    public String authMode;
    public ABDMAuthModesRequester requester;

    public ABDMAuthModeInitQuery(String abhaAddress, String mode,String hipId)
    {
        this.id = abhaAddress;
        this.purpose = "LINK";
        this.authMode = mode;
        this.requester =  new ABDMAuthModesRequester(ABDM_TRANSFER_NOTIFIER_TYPE,hipId);
    }
}
