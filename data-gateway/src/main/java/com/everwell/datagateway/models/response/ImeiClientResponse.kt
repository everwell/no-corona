package com.everwell.datagateway.models.response

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
data class ImeiClientResponse (
        @JsonProperty("clientId")
        val clientId: Long
)