package com.everwell.datagateway.models.request.abdm;

import com.everwell.datagateway.models.response.abdm.Error;
import com.everwell.datagateway.models.response.abdm.Response;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

import static com.everwell.datagateway.constants.Constants.ABDM_TIMESTAMP_FORMAT;

@Getter
@Setter
@Data
public class ABDMOnLinkInitiationRequest {
    public String requestId;
    public String timestamp;
    public String transactionId;
    public Link link;
    private Response resp;
    private Error error;

    public ABDMOnLinkInitiationRequest(String transactionId, String requestId, String referenceNumber,String message,String expiry) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(ABDM_TIMESTAMP_FORMAT);
        LocalDateTime utcNow = LocalDateTime.now(ZoneOffset.UTC);
        this.requestId = UUID.randomUUID().toString();
        this.timestamp = formatter.format(utcNow);
        this.transactionId = transactionId;
        this.resp = new Response(requestId);
        this.link = new Link(referenceNumber,"DIRECT",new Meta("MOBILE",message,expiry));
    }

    public ABDMOnLinkInitiationRequest(String transactionID, String requestId,String message) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(ABDM_TIMESTAMP_FORMAT);
        LocalDateTime utcNow = LocalDateTime.now(ZoneOffset.UTC);
        this.requestId = UUID.randomUUID().toString();
        this.timestamp = formatter.format(utcNow);
        this.transactionId = transactionID;
        this.resp = new Response(requestId);
        this.error = new Error(1000,message);

    }

    @Getter
    @Setter
    @AllArgsConstructor
    public static class Link {
        public String referenceNumber;
        public String authenticationType;
        public Meta meta;
    }

    @Getter
    @Setter
    @AllArgsConstructor
    public static class Meta {
        public String communicationMedium;
        public String communicationHint;
        public String communicationExpiry;
    }
}
