package com.everwell.datagateway.models.dto;

import com.everwell.datagateway.enums.FHIRProfileEnum;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import com.everwell.datagateway.models.dto.DiagnosticsFHIRDto.*;
import com.everwell.datagateway.models.dto.DispensationFHIRDto.*;

import java.util.HashMap;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SharedResourceDto {

    public Map.Entry<String, TestDetails> diagnosticsEntry;
    public Map.Entry<String, DispensationDetails> dispensationEntry;
    public int episodeId;
    public int hierarchyId;
    public int userId;
    public String typeOfCase;
    public String diagnosisDate;
    public String basisOfDiagnosis;
    public String treatmentPhase;
    public String hierarchyName;
    public Map<String,String> profileReferenceMap;
    public String name;
    public String gender;
    public String dateOfBirth;

    public SharedResourceDto(Map.Entry<String, TestDetails> entry, DiagnosticsFHIRDto diagnosticsFHIRDto) {
        this.diagnosticsEntry = entry;
        this.episodeId = diagnosticsFHIRDto.episodeId;
        this.hierarchyId = diagnosticsFHIRDto.hierarchyId;
        this.userId = diagnosticsFHIRDto.userId;
        this.hierarchyName = diagnosticsFHIRDto.hierarchyName;
        this.name = diagnosticsFHIRDto.getName();
        this.gender = diagnosticsFHIRDto.getGender();
        this.dateOfBirth = diagnosticsFHIRDto.getDateOfBirth();
        this.profileReferenceMap = new HashMap<>();
        profileReferenceMap.put(FHIRProfileEnum.PATIENT.getKey(),String.valueOf(this.episodeId));
        profileReferenceMap.put(FHIRProfileEnum.PRACTITIONER.getKey(), String.valueOf(this.userId));
        profileReferenceMap.put(FHIRProfileEnum.DIAGNOSTIC_REPORT.getKey(), this.diagnosticsEntry.getKey());
        profileReferenceMap.put(FHIRProfileEnum.ORGANIZATION.getKey(),String.valueOf(this.hierarchyId));
    }

    public SharedResourceDto(DispensationFHIRDto dispensationFHIRDto) {
        this.episodeId = dispensationFHIRDto.episodeId;
        this.hierarchyId = dispensationFHIRDto.hierarchyId;
        this.userId = dispensationFHIRDto.userId;
        this.hierarchyName = dispensationFHIRDto.hierarchyName;
        this.typeOfCase = dispensationFHIRDto.typeOfCase;
        this.diagnosisDate = dispensationFHIRDto.diagnosisDate;
        this.basisOfDiagnosis = dispensationFHIRDto.basisOfDiagnosis;
        this.treatmentPhase = dispensationFHIRDto.treatmentPhase;
        this.name = dispensationFHIRDto.getName();
        this.gender = dispensationFHIRDto.getGender();
        this.dateOfBirth = dispensationFHIRDto.getDateOfBirth();
        this.profileReferenceMap = new HashMap<>();
        profileReferenceMap.put(FHIRProfileEnum.PATIENT.getKey(),String.valueOf(this.episodeId));
        profileReferenceMap.put(FHIRProfileEnum.ORGANIZATION.getKey(), String.valueOf(this.hierarchyId));
    }
}
