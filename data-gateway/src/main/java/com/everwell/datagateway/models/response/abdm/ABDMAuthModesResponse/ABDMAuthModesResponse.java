package com.everwell.datagateway.models.response.abdm.ABDMAuthModesResponse;

import com.everwell.datagateway.models.response.abdm.Response;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ABDMAuthModesResponse{
    public String requestId;
    public String timestamp;
    public Auth auth;
    public Object error;
    public Response resp;
}
