package com.everwell.datagateway.models.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ABDMConsumerResponse {
    public Long refId;
    public String username;
    public String response;
    public boolean success;
}
