package com.everwell.datagateway.models.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProcessCallDto {
    private String numberDialed;
    private String caller;
    private String utcDateTime;
}
