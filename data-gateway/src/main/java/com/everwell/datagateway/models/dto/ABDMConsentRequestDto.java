package com.everwell.datagateway.models.dto;

import com.everwell.datagateway.models.request.abdm.ABDMConsentInitiationRequest;
import com.everwell.datagateway.models.request.abdm.ABDMConsentNotification;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class ABDMConsentRequestDto {
    public Long userId;
    public String consentRequestId;
    public String consentId;
    public ABDMConsentInitiationRequest abdmConsentInitiationRequest;
    public ABDMConsentNotification abdmConsentNotification;
}
