package com.everwell.datagateway.models.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.*;

import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonNaming(PropertyNamingStrategy.UpperCamelCaseStrategy.class)
public class DispensationFHIRDto {

    public int episodeId;

    public int hierarchyId;

    public int userId;

    public String hierarchyName;

    public String typeOfCase;

    public String diagnosisDate;

    public String basisOfDiagnosis;

    public String treatmentPhase;
    public String name;
    public String gender;
    public String dateOfBirth;

    public Map<String, DispensationDetails> dispensationMap;

    public DispensationFHIRDto(int episodeId, int hierarchyId, int userId, String hierarchyName, String dob,Map<String, DispensationDetails> dispensationDetailsMap) {
        this.episodeId = episodeId;
        this.hierarchyId = hierarchyId;
        this.hierarchyName = hierarchyName;
        this.userId = userId;
        this.dispensationMap = dispensationDetailsMap;
        this.dateOfBirth = dob;
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @JsonNaming(PropertyNamingStrategy.UpperCamelCaseStrategy.class)
    static
    public class DispensationDetails
    {
        public boolean active;
        public String productName;
        public String timeStamp;
        public String dosesGiven;
        public String dosingDate;
        public String noOfDays;
        public String weightband;
        public String typeOfRegimen;
        public String refillDate;

        public DispensationDetails(boolean active, String productName, String timeStamp) {
            this.active = active;
            this.productName = productName;
            this.timeStamp = timeStamp;
        }
    }
}
