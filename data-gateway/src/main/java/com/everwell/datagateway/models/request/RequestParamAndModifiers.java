package com.everwell.datagateway.models.request;

import com.everwell.datagateway.models.dto.ParamPrefixAndValue;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@AllArgsConstructor
@Data
public class RequestParamAndModifiers {
    String paramName;
    String paramValues[];
    String paramModifier;
    List<ParamPrefixAndValue> prefixAndValueList;
}
