package com.everwell.datagateway.models.request.abdm;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import static com.everwell.datagateway.constants.Constants.X_HIP_ID_VALUE;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class ABDMAuthModesRequester {
    public String type;
    public String id;
}
