package com.everwell.datagateway.models.dto.ecbss;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EpisodeDataDto {
    public String entityId;
    public String externalId2;
}
