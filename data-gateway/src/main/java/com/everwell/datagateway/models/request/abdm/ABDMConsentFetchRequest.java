package com.everwell.datagateway.models.request.abdm;

import lombok.Data;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

import static com.everwell.datagateway.constants.Constants.ABDM_TIMESTAMP_FORMAT;

@Data
public class ABDMConsentFetchRequest {
    public String requestId;
    public String timestamp;
    public String consentId;

    public ABDMConsentFetchRequest(String consentId) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(ABDM_TIMESTAMP_FORMAT);
        LocalDateTime utcNow = LocalDateTime.now(ZoneOffset.UTC);
        this.requestId = UUID.randomUUID().toString();
        this.timestamp = formatter.format(utcNow);
        this.consentId = consentId;
    }
}
