package com.everwell.datagateway.models.dto.ecbss;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TrackedEntityInstanceDto {
    String ou;
    String filter;
    String program;
    String ouMode;
}
