package com.everwell.datagateway.service

import com.everwell.datagateway.component.AppProperties
import com.everwell.datagateway.constants.Constants.NIKSHAY_REFRESH_EXPIRY
import com.everwell.datagateway.entities.Client
import com.everwell.datagateway.models.response.ApiResponse
import com.everwell.datagateway.models.response.GenericAuthResponse
import com.everwell.datagateway.models.response.LinkInitiationOtpResponse
import com.everwell.datagateway.utils.SentryUtils
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.HttpMethod
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service
import org.springframework.util.LinkedMultiValueMap
import org.springframework.web.client.RestClientResponseException
import org.springframework.web.client.RestTemplate
import java.util.*
import kotlin.collections.HashMap


@Service
class NikshayMasterApiRestServiceImp : NikshayMasterApiRestService() {

    private val logger: Logger = LoggerFactory.getLogger(NikshayMasterApiRestServiceImp::class.simpleName)
    override fun getAuthToken(client_id: String?): String? {
        val client = provideCurrentClient()
        if(accessToken!=null && client!=null && client.authTokenForProxy==null){
            client.authTokenForProxy = accessToken
            clientService.saveClient(client)
        } else {
            val params: LinkedMultiValueMap<String, String> = LinkedMultiValueMap()
            params["grant_type"] = appProperties.nikshayMasterApiUserGrantType
            params["username"] = appProperties.nikshayMasterApiUserName
            params["password"] = appProperties.nikshayMasterApiPassword
            val resp = authenticate(
                    appProperties.nikshayMasterApiServerUrl + "Token",
                    HashMap(),
                    params,
                    object : ParameterizedTypeReference<GenericAuthResponse>() {})
            accessToken = resp.body?.access_token
            saveAccessToken(accessToken)
        }
        return accessToken
    }
}