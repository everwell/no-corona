package com.everwell.datagateway.service;

import com.everwell.datagateway.entities.Event;
import com.everwell.datagateway.repositories.EventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EventServiceImpl implements EventService {

    @Autowired
    private EventRepository eventRepository;

    @Override
    public Event findEventByEventName(String eventName) {
        return eventRepository.findByEventName(eventName).orElse(null);
    }

    @Override
    public Boolean isEventExists(String eventName) {
        return findEventByEventName(eventName) != null;
    }

    @Override
    public Event getEventById(Long eventId) {
        return eventRepository.findTopById(eventId);
    }


}
