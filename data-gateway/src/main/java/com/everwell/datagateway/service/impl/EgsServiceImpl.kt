package com.everwell.datagateway.service.impl

import com.everwell.datagateway.service.EgsRestService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.web.client.RestTemplate

@Service
open class EgsServiceImpl : EgsRestService() {
    @Autowired
    override fun getAuthToken(client_id: String?): String? {
        restTemplateForAuthentication = RestTemplate()
        return null
    }
}