package com.everwell.datagateway.service.impl;

import com.everwell.datagateway.entities.Client;
import com.everwell.datagateway.entities.Event;
import com.everwell.datagateway.entities.Trigger;
import com.everwell.datagateway.exceptions.ForbiddenException;
import com.everwell.datagateway.models.dto.PublishToSubscriberUrlDTO;
import com.everwell.datagateway.models.request.GetDateRequest;
import com.everwell.datagateway.models.request.KpiDataRequest;
import com.everwell.datagateway.models.response.ApiResponse;
import com.everwell.datagateway.models.response.GetDateResponse;
import com.everwell.datagateway.models.response.KpiDataResponse;
import com.everwell.datagateway.repositories.TriggerRepository;
import com.everwell.datagateway.service.*;
import com.everwell.datagateway.utils.SentryUtils;
import com.everwell.datagateway.utils.Utils;
import com.fasterxml.jackson.core.type.TypeReference;
import io.sentry.Sentry;
import org.jobrunr.scheduling.JobScheduler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.*;

import static com.everwell.datagateway.constants.Constants.*;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

@Service
public class TriggerServiceImpl implements TriggerService {

    @Autowired
    TriggerRepository triggerRepository;

    @Autowired
    JobScheduler jobScheduler;

    @Autowired
    KpiRestService kpiRestService;

    @Autowired
    ConsumerService consumerService;

    @Autowired
    EventService eventService;

    @Autowired
    ClientService clientService;
    private final RestTemplate restTemplate = new RestTemplate();
    Logger LOGGER = LoggerFactory.getLogger(TriggerServiceImpl.class);

    @Override
    public void readTriggerTable() {
        List<Trigger> triggers = triggerRepository.getAllByCronTimeNotNullAndActiveTrue();
        triggers.forEach(trigger -> {
                jobScheduler.delete(trigger.getEventName());
                jobScheduler.scheduleRecurrently(trigger.getEventName(), trigger.getCronTime(), () -> invokeFuncFromFunctionName(trigger));
        });
    }
    public void invokeFuncFromFunctionName(Trigger trigger) throws Exception {
        try {
            Class<?> classs = Class.forName(this.getClass().getName());
            Method method = classs.getDeclaredMethod(trigger.getFunctionName(), Trigger.class);
            method.invoke(this, trigger);
        }
        catch (Exception e) {
            SentryUtils.captureException(e);
            throw e;
        }
    }

    public void SampleJob(Trigger trigger) throws IOException {
        LOGGER.info("Starting Sample Job");
        Map<String,Object> paramMap = Utils.convertStrToObject(trigger.getExtraParams(),  new TypeReference<Map<String, Object>>() {});
        KpiDataRequest kpiDataRequest = new KpiDataRequest(paramMap);
        int datasetId = (int) kpiDataRequest.getParams().get(DATASET_ID);
        ApiResponse<KpiDataResponse> response = kpiRestService.getKPIData(kpiDataRequest,datasetId, trigger.getClientId().toString());
        LOGGER.info(Utils.asJsonString(response));
        LOGGER.info("Sample Job Ended");
    }

    public void pushDbtBharatSchemeData(Trigger trigger) throws Exception {
        String year,month;
        Map<String,Object> paramMap = Utils.convertStrToObject(trigger.getExtraParams(),  new TypeReference<Map<String, Object>>() {});
        int datasetId = (int) paramMap.get(DATASET_ID);
        String schemeCode = (String) paramMap.get(SCHEME_CODE);
        String getDateUrl = (String) paramMap.get(GET_DATE_URL);
        String privateKey = (String) paramMap.get(PRIVATE_KEY);
        String ksClientId = (String) paramMap.get(KS_CLIENT_ID);
        GetDateRequest getDateRequest = new GetDateRequest(schemeCode);
        String pendingDate = getPendingDate(getDateRequest,getDateUrl);
        if(null != pendingDate) {
          String[] parts = pendingDate.split("-");
          year = parts[0];
          month = parts[1];
        }
        else{
            throw new ForbiddenException("No data is pending to be pushed.");
        }
        KpiDataRequest fetchDataRequest = new KpiDataRequest();
        Map<String,Object> requestParams = new LinkedHashMap<>();
        requestParams.put(START_YEAR,year);
        requestParams.put(END_YEAR, year);
        requestParams.put(START_MONTH, month);
        requestParams.put(END_MONTH, month);
        requestParams.put(SCHEME_CODE,schemeCode);
        fetchDataRequest.setParams(requestParams);
        ApiResponse<KpiDataResponse> response = kpiRestService.getKPIData(fetchDataRequest,datasetId,ksClientId);
        Map<String,Object> schema = (Map<String, Object>) paramMap.get(SCHEMA);
        Map<String,Object> formattedResponse = Utils.formatIntoRequestBodySchema(response.getData().getKpiData(),schema);
        formattedResponse.put(FIELD_SCHEME_CODE_SMALL,schemeCode);
        formattedResponse.put(FIELD_YEAR,Integer.valueOf(year));
        formattedResponse.put(FIELD_MONTH,Integer.valueOf(month));
        String encryptedContent = Utils.encryptData(Utils.asJsonString(formattedResponse),privateKey);
        Map<String,Object> requestBody = new HashMap<>();
        requestBody.put(FIELD_SCHEME_CODE,schemeCode);
        requestBody.put(FIELD_ENCRYPTED_DATA,encryptedContent);
        Event event = eventService.getEventById(trigger.getEventId());
        Client client = clientService.getClientById(trigger.getClientId());
        consumerService.publishToSubscriberUrl(new PublishToSubscriberUrlDTO(client.getUsername(),event.getEventName(),Utils.asJsonString(requestBody),null,true));
    }

    public String getPendingDate(GetDateRequest request, String getDateUrl) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<GetDateRequest> requestEntity = new HttpEntity<>(request, headers);

        ResponseEntity<GetDateResponse> responseEntity = restTemplate.exchange(
                getDateUrl,
                HttpMethod.POST,
                requestEntity,
                GetDateResponse.class);

        if (responseEntity.getStatusCode() == HttpStatus.OK) {
            GetDateResponse apiResponse = responseEntity.getBody();
            if (null != apiResponse && String.valueOf(HttpStatus.OK.value()).equals(apiResponse.getStatusCode())) {
                List<String> pendingDates = apiResponse.getPendingDates();
                if (!pendingDates.isEmpty()) {
                    return pendingDates.get(0);
                }
            }
        }
        return null;
    }
}
