package com.everwell.datagateway.service;

import com.everwell.datagateway.entities.Client;
import com.everwell.datagateway.models.request.SubscribeEventRequest;

public interface ClientService {

    Client findByUsername(String username);

    Client findByIp(String IP);

    Boolean isClientExists(String username);

    void saveClient(Client client);

    Boolean subscribeEvent(SubscribeEventRequest subscribeEventRequest);

    Client getClientById(Long id);

    Boolean isClientSubscribedToEvent(Long clientId,Long eventId);


}
