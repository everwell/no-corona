package com.everwell.datagateway.service

abstract class RegistryRestService : BaseRestService() {
    abstract fun getUserAccessClientToken(loginRequestDto: Any, clientId: String ?): String
}