package com.everwell.datagateway.service

import com.everwell.datagateway.models.request.KpiDataRequest
import com.everwell.datagateway.models.response.ApiResponse
import com.everwell.datagateway.models.response.KpiDataResponse

abstract class KpiRestService: BaseRestService() {
    abstract fun getKPIData(kpiDataRequest: KpiDataRequest, datasetId: Int, clientId: String): ApiResponse<KpiDataResponse>

}