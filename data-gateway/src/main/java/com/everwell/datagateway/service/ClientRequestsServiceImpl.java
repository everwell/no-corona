package com.everwell.datagateway.service;

import com.everwell.datagateway.entities.*;
import com.everwell.datagateway.enums.RefIdStatusEnum;
import com.everwell.datagateway.exceptions.RedisException;
import com.everwell.datagateway.exceptions.RestTemplateException;
import com.everwell.datagateway.models.dto.PublishToSubscriberUrlDTO;
import com.everwell.datagateway.models.request.RefIdStatusRequest;
import com.everwell.datagateway.models.response.RefIdStatusResponse;
import com.everwell.datagateway.repositories.*;
import com.everwell.datagateway.utils.SentryUtils;
import io.sentry.Sentry;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Service
public class ClientRequestsServiceImpl implements ClientRequestsService {

    private static Logger LOGGER = LoggerFactory.getLogger(ClientRequestsServiceImpl.class);

    @Autowired
    private ClientRequestsRepository clientRequestsRepository;

    @Autowired
    private ClientRequestsArchiveRepository clientRequestsArchiveRepository;
    @Autowired
    private SubscriberUrlService subscriberUrlService;

    @Autowired
    private EventClientRepository eventClientRepository;

    @Autowired
    private ConsumerService consumerService;

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private EventRepository eventRepository;

    @Override
    public Long saveClientRequest(ClientRequests clientRequests) {
        return clientRequestsRepository.save(clientRequests).getId();
    }

    @Override
    public Long updateClientRequest(ClientRequests clientRequests) {
        return clientRequestsRepository.save(clientRequests).getId();
    }

    @Override
    public void setIsDeliveredTrueAndDeliveredAtTime(Long id, Date deliveredAt) {
        ClientRequests clientRequests = clientRequestsRepository.findTopById(id);
        if (null == clientRequests) {
            LOGGER.error("The response was sent to subscriber url, but id doesn't exist :" + id);
        }
        else {
            clientRequests.setResultDelivered(true);
            clientRequests.setDeliveredAt(deliveredAt);
            clientRequestsRepository.save(clientRequests);
        }
    }

    @Override
    public ClientRequests getClientRequestsById(Long id) {
        return clientRequestsRepository.findTopById(id);
    }

    @Override
    public RefIdStatusResponse getStatusByRefId(RefIdStatusRequest refIdStatusRequest) {
        ClientRequests clientRequests = getClientRequestsById(refIdStatusRequest.getReferenceId());
        String status = "";
        RefIdStatusResponse refIdStatusResponse = new RefIdStatusResponse();
        if(null == clientRequests)
        {
            status = RefIdStatusEnum.REQUEST_COMPLETED_OR_NOT_FOUND.getStatus();
        }
        else if(clientRequests.getResultDelivered())
        {
            refIdStatusResponse.setPayload(clientRequests.getResponseData());
            status = RefIdStatusEnum.REQUEST_COMPLETED_AND_NOTIFIED.getStatus();
        }
        else {
            refIdStatusResponse.setPayload(clientRequests.getResponseData());
            status = RefIdStatusEnum.REQUEST_IN_PROCESS.getStatus();
        }
        refIdStatusResponse.setReferenceId(refIdStatusRequest.getReferenceId());
        refIdStatusResponse.setStatus(status);
        return refIdStatusResponse;
    }
    @Value("${client.archive.enable}")
    @Setter
    private boolean clientRequestArchivalEnabled;

    @Override
    @Scheduled(fixedDelay = 24 * 60 * 60 * 1000)
    public void archiveClientRequestData(){
        if(clientRequestArchivalEnabled) {
            Date deliveredAtDateTime = Date.from(ZonedDateTime.now().minusMonths(1).toInstant());
            List<ClientRequests> clientRequests = clientRequestsRepository.findByDeliveredAtLessThanEqual(deliveredAtDateTime);
            List<ClientRequestsArchive> clientRequestsArchives = Collections.synchronizedList(new ArrayList<>());
            clientRequests.parallelStream().forEach(request -> {
                if(request.getEventId()!=null && request.getSubscriberUrlId()!=null && request.getClientId()!=null) {
                    ClientRequestsArchive clientRequestsArchive = new ClientRequestsArchive(
                            request.getId(), request.getCreatedAt(), request.getResultDelivered(),
                            request.getClientId(), request.getEventId(), request.getSubscriberUrlId(),
                            request.getResponseData(), request.getRequestData(), request.getDeliveredAt(),
                            request.getRefIdDeliveredAt(), request.getCallbackResponseCode()
                    );
                    clientRequestsArchives.add(clientRequestsArchive);
                }
            });
            clientRequestsArchiveRepository.saveAll(clientRequestsArchives);
            clientRequestsRepository.deleteByDeliveredAtLessThanEqual(deliveredAtDateTime);
        } else {
            LOGGER.error("Client Request Archival Job is not Enabled");
        }
    }
    @Override
    public ClientRequests updateClientRequestbasedonRefId(Long refId, String data)
    {
        ClientRequests clientRequests = getClientRequestsById(refId);
        EventClient eventClient = eventClientRepository.findByClientIdAndEventId(clientRequests.getClientId(), clientRequests.getEventId());
        SubscriberUrl subscriberUrl = subscriberUrlService.findByEventClientId(eventClient);
        clientRequests.setSubscriberUrlId(subscriberUrl.getId());
        clientRequests.setResponseData(data);
        updateClientRequest(clientRequests);
        return clientRequests;
    }

    @Value("${client.retrigger.enable}")
    @Setter
    private boolean clientRequestRetriggerEnabled;

    @Override
    @Scheduled(cron = "0 0 0 * * *")
    public void retriggerRequestsJob(){
        
        LOGGER.info("Re-trigger Job started");

        try{
            if(clientRequestRetriggerEnabled) {
                Date date = Date.from(ZonedDateTime.now().minusDays(1).toInstant());
                List<ClientRequests> clientRequests = clientRequestsRepository.findByCreatedAtGreaterThanEqualAndResultDeliveredFalse(date);

                clientRequests.forEach(request -> {
                    if(request.getEventId()!=null && request.getSubscriberUrlId()!=null && request.getClientId()!=null && request.getId()!=null && request.getResponseData()!=null) {
                        Client client = clientRepository.findById(request.getClientId()).orElse(null);
                        Event event = eventRepository.findById(request.getEventId()).orElse(null);

                        String clientName = client.getUsername();
                        String eventName = event.getEventName();

                        consumerService.publishToSubscriberUrl(new PublishToSubscriberUrlDTO(clientName, eventName, request.getResponseData(), request.getId(), false));
                        setIsDeliveredTrueAndDeliveredAtTime(request.getId(), new java.util.Date());
                    }
                });
            } else {
                LOGGER.error("Re-trigger Job is Disabled");
            }
        } catch (RestTemplateException | RedisException | IllegalArgumentException e) {
            LOGGER.warn("RestTemplate|Redis|IllegalArgumentException Exception thrown :" + e.getMessage());
            SentryUtils.captureException(e);
            throw e;
        } catch (Exception e) {
            LOGGER.warn("Exception thrown :" + e.getMessage());
            SentryUtils.captureException(e);
        }

        LOGGER.info("Re-trigger Job ended");

    }

    @Override
    public int updateResponseData(Long id, String responseData) {
        return clientRequestsRepository.updateResponseData(id, responseData);
    }
}
