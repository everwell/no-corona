package com.everwell.datagateway.service;

import com.everwell.datagateway.models.response.PublisherResponse;

import java.util.Map;

public interface PublisherService {

    PublisherResponse publishRequest(String data, String eventName);

    Map<String, String> publishRequestForINS(Object data, String eventName);

    Map<String, String> publishRequestForABDM(String data, String eventName);

    Boolean publishRequestForWix(String data, String eventName);
}
