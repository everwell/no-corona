package com.everwell.datagateway.service;

import com.everwell.datagateway.constants.Constants;
import com.everwell.datagateway.entities.Client;
import com.everwell.datagateway.entities.ClientRequests;
import com.everwell.datagateway.entities.Event;
import com.everwell.datagateway.entities.PublisherQueueInfo;
import com.everwell.datagateway.enums.EventEnum;
import com.everwell.datagateway.enums.StatusCodeEnum;
import com.everwell.datagateway.exceptions.ForbiddenException;
import com.everwell.datagateway.exceptions.NotFoundException;
import com.everwell.datagateway.handlers.EventHandler;
import com.everwell.datagateway.handlers.EventHandlerMap;
import com.everwell.datagateway.models.dto.PublisherData;
import com.everwell.datagateway.models.response.PublisherResponse;
import com.everwell.datagateway.repositories.PublisherQueueInfoRepository;
import com.everwell.datagateway.utils.SentryUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class PublisherServiceImpl implements PublisherService {


    @Autowired
    private ClientRequestsService clientRequestsService;

    @Autowired
    private EventService eventService;

    @Autowired
    private ClientService clientService;

    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private EventHandlerMap eventHandlerMap;

    @Autowired
    private PublisherQueueInfoRepository publisherQueueInfoRepository;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    private static Logger LOGGER = LoggerFactory.getLogger(PublisherServiceImpl.class);

    @Override
    public PublisherResponse publishRequest(String data, String eventName) {
        Map<String,Object> response = validateEventDetails(eventName);
        PublisherQueueInfo publisherQueueInfo = (PublisherQueueInfo) response.get(Constants.PUBLISHER_QUEUE_INFO);
        Event event = (Event) response.get(Constants.EVENT);
        Client client = (Client) response.get(Constants.CLIENT);
        ClientRequests clientRequests = new ClientRequests();
        clientRequests.setEventId(event.getId());
        clientRequests.setClientId(client.getId());
        clientRequests.setRequestData(data);
        Long referenceId = clientRequestsService.saveClientRequest(clientRequests);
        PublisherData publisherData = PublisherData.builder()
                .referenceId(referenceId)
                .clientName((String) response.get(Constants.CLIENT_NAME))
                .clientId(client.getId())
                .data(data)
                .microServiceClientId(client.getAccessibleClient())
                .build();
        EventHandler eventHandler = (EventHandler) response.get(Constants.EVENT_HANDLER);
        eventHandler.publishEvent(publisherData, publisherQueueInfo);
        PublisherResponse publisherResponse = new PublisherResponse();
        publisherResponse.setReferenceId(referenceId);
        clientRequests.setRefIdDeliveredAt(new java.util.Date());
        clientRequestsService.updateClientRequest(clientRequests);
        return publisherResponse;
    }

    @Override
    public Map<String, String> publishRequestForINS(Object data, String eventName) {
        LOGGER.info("publishRequestForINS: " + eventName);
        Map<String, String> map = new HashMap<>();
        try {
            Map<String,Object> response = validateEventDetails(eventName);
            EventHandler  eventHandler = (EventHandler) response.get(Constants.EVENT_HANDLER);
            PublisherQueueInfo publisherQueueInfo = (PublisherQueueInfo) response.get(Constants.PUBLISHER_QUEUE_INFO);
            Client client = (Client) response.get(Constants.CLIENT);
            Map<String, Object> msgToSend = new HashMap<>();
            msgToSend.put(Constants.EVENT_NAME, EventEnum.SEND_BULK_SMS.getEventName());
            msgToSend.put(Constants.FIELD,data);
            eventHandler.publishEvent(msgToSend, publisherQueueInfo, client.getAccessibleClient());
            LOGGER.info("publishRequestForINS Success");
            map.put(Constants.SUCCESS, "publisherResponse");
        } catch (NotFoundException | ForbiddenException ex) {
            LOGGER.error("Error during Publish INS SMS Request " + ex);
            SentryUtils.captureException(ex);
            map.put(Constants.ERROR, ex.getMessage());
        }
        return map;
    }

    @Override
    public Map<String, String> publishRequestForABDM(String data, String eventName) {
        LOGGER.info("publishRequestForABDM: " + eventName);
        Map<String, String> map = new HashMap<String, String>();
        try {
            PublisherResponse publisherResponse = publishRequest(data, eventName);
            LOGGER.info("publishRequestForABDM Success ");
            map.put(Constants.SUCCESS, "publisherResponse");
        } catch (NotFoundException | ForbiddenException ex) {
            LOGGER.error("Error during Publish ABDM Consent Request " + ex);
            SentryUtils.captureException(ex);
            map.put(Constants.ERROR, ex.getMessage());
        }
        return map;
    }


    @Override
    public Boolean publishRequestForWix(String data, String eventName) {
        LOGGER.info("publishRequestForEpisode: " + eventName);
        Map<String,Object> response = validateEventDetails(eventName);
        EventHandler  eventHandler = (EventHandler) response.get(Constants.EVENT_HANDLER);
        PublisherQueueInfo publisherQueueInfo = (PublisherQueueInfo) response.get(Constants.PUBLISHER_QUEUE_INFO);
        Map<String, Object> msgToSend = new HashMap<>();
        msgToSend.put(Constants.EVENT_NAME, EventEnum.WIX_NEW_BLOG_POST_EVENT.getEventName());
        msgToSend.put(Constants.FIELD,data);
        eventHandler.publishEvent(msgToSend, publisherQueueInfo,((Client) response.get(Constants.CLIENT)).accessibleClient);
        return true;
    }

    private Map<String,Object> validateEventDetails(String eventName){
        Map<String,Object> eventDetails = new HashMap<>();
        Event event = eventService.findEventByEventName(eventName);
        PublisherQueueInfo publisherQueueInfo = publisherQueueInfoRepository.findFirstByEventName(eventName);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String clientName = authenticationService.getCurrentUsername(authentication);
        Client client = clientService.findByUsername(clientName);
        eventDetails.put(Constants.PUBLISHER_QUEUE_INFO,publisherQueueInfo);
        if (null == event || client == null) {
            StatusCodeEnum statusCodeEnum = StatusCodeEnum.CLIENT_OR_EVENT_NOT_FOUND;
            throw new NotFoundException(statusCodeEnum.getCode().value(), statusCodeEnum.getMessage());
        }
        eventDetails.put(Constants.CLIENT, client);
        eventDetails.put(Constants.CLIENT_NAME, clientName);
        if (!clientService.isClientSubscribedToEvent(client.getId(), event.getId())) {
            throw new ForbiddenException(StatusCodeEnum.CLIENT_NOT_SUBSCRIBED_TO_EVENT_OR_DISABLED.getMessage());
        }
        EventHandler eventHandler = eventHandlerMap.getHandler(eventName);
        if (null == eventHandler) {
            throw new NotFoundException(StatusCodeEnum.NOT_FOUND_HANDLER);
        }
        eventDetails.put(Constants.EVENT, event);
        eventDetails.put(Constants.EVENT_HANDLER, eventHandler);
        return eventDetails;

    }
}
