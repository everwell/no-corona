package com.everwell.datagateway.service;

import com.everwell.datagateway.apiHelpers.ABDMHelper;
import com.everwell.datagateway.component.AppProperties;
import com.everwell.datagateway.constants.Constants;
import com.everwell.datagateway.entities.Client;
import com.everwell.datagateway.entities.ClientRequests;
import com.everwell.datagateway.entities.Event;
import com.everwell.datagateway.models.DiscoveryCacheDto;
import com.everwell.datagateway.models.dto.*;
import com.everwell.datagateway.models.request.AbdmClientDto.DataTransferRequest;
import com.everwell.datagateway.models.request.ExternalIdRequest;
import com.everwell.datagateway.models.request.UpdateUserMedicalRecordsRequest;
import com.everwell.datagateway.models.request.abdm.*;
import com.everwell.datagateway.models.response.ApiResponse;
import com.everwell.datagateway.models.response.GetEpisodeByPersonResponse;
import com.everwell.datagateway.models.response.GetRecordsByTxnIdResponse;
import com.everwell.datagateway.models.response.LinkInitiationOtpResponse;
import com.everwell.datagateway.models.response.abdm.Error;
import com.everwell.datagateway.publishers.RMQPublisher;
import com.everwell.datagateway.utils.CacheUtils;
import com.everwell.datagateway.utils.EncryptionUtil;
import com.everwell.datagateway.utils.SentryUtils;
import com.everwell.datagateway.utils.Utils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import io.sentry.Sentry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;


import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

import static com.everwell.datagateway.constants.Constants.*;
import static com.everwell.datagateway.constants.QueueConstants.UPDATE_CONSENT_REQUEST_ROUTING_KEY;
import static com.everwell.datagateway.constants.QueueConstants.UPDATE_MEDICAL_RECORDS_ROUTING_KEY;

@Service
public class ABDMIntegrationService {

    @Value("${NDHM_HIP_NAME:}")
    private String HIP;
    @Autowired
    private ABDMHelper abdmApiHelper;
    @Autowired
    private PublisherService publisherService;
    @Autowired
    private EmailService emailService;

    @Autowired
    private UserServiceRestService userServiceRestService;

    @Autowired
    private EpisodeServiceRestService episodeServiceRestService;

    @Autowired
    private EventService eventService;

    @Autowired
    private ClientService clientService;

    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private ClientRequestsService clientRequestsService;

    @Autowired
    private AppProperties appProperties;

    @Autowired
    private NikshayRestService nikshayRestService;

    @Autowired
    private RMQPublisher rmqPublisher;

    @Value("${abdm.m3.callback.url}")
    private  String callbackUrl;

    Logger logger = LoggerFactory.getLogger(ABDMIntegrationService.class);

    @Value("${abdm.m2.direct.auth:}")
    boolean directAuthEnabled;

    @Value("${NDHM_HIU_NAME:}")
    String hiuId;

    private static final Logger LOGGER = LoggerFactory.getLogger(ABDMIntegrationService.class);

    public String initiateDemographicAuth(ABDMLinkCareContextEvent data) {
        ABDMInitateAuthRequest req = directAuthEnabled ? new ABDMInitateAuthRequest(data.getAbhaAddress(), DIRECT_AUTH_MODE,data.getHipId()) : new ABDMInitateAuthRequest(data.getAbhaAddress(), DEMOGRAPHICS_AUTH_MODE,data.getHipId());
        Gson gson = new GsonBuilder().create();
        String jsonCareContextDetails = gson.toJson(data);
        CacheUtils.putIntoCache(data.abhaAddress, jsonCareContextDetails, 7200);
        CacheUtils.putIntoCache(req.requestId, req.query.id, 7200);
        return abdmApiHelper.authInitRequest(req);
    }

    public boolean getCareContextLinkingToken(String requestId, String transactionId) {
        try
        {
            ABDMLinkCareContextEvent careContextDetails = getCareContextLinkingDetailsFor(requestId);
            ABDMDemographicLinkingTokenRequest.Credential cred = new ABDMDemographicLinkingTokenRequest.Credential(
                    careContextDetails.personName,
                    Character.toString(careContextDetails.gender),
                    careContextDetails.yearOfBirth,
                    careContextDetails.identifierType,
                    careContextDetails.identifierValue);
            ABDMDemographicLinkingTokenRequest linkingTokenReq = new ABDMDemographicLinkingTokenRequest(transactionId, cred);
            LOGGER.info("linkingTokenReq " + Utils.asJsonString(linkingTokenReq));
            if(directAuthEnabled){
                CacheUtils.putIntoCache(transactionId, careContextDetails.abhaAddress, 7200);
                return true;
            }
            CacheUtils.putIntoCache(linkingTokenReq.requestId, careContextDetails.abhaAddress, 7200);
            String response = abdmApiHelper.authConfirmRequest(linkingTokenReq);
            return null != response;
        }
        catch(Exception ex)
        {
            SentryUtils.captureException(ex);
        }
        return false;
    }

    public boolean addCareContext(String requestId, String accessToken) {
        try{
            ABDMLinkCareContextEvent careContextDetails = getCareContextLinkingDetailsFor(requestId);
            LOGGER.info("ABDMLinkCareContextEvent "+ Utils.asJsonString(careContextDetails));
            ABDMLinkCareContextRequest linkingRequest = new ABDMLinkCareContextRequest(
                    accessToken,
                    Long.toString(careContextDetails.personId),
                    Long.toString(careContextDetails.episodeId),
                    careContextDetails.personName,
                    careContextDetails.enrollmentDate);
            CacheUtils.putIntoCache(linkingRequest.requestId, careContextDetails.abhaAddress, 7200);
            String response = abdmApiHelper.addCareContextRequest(linkingRequest);
            if (null != response) {
                return true;
            }
        }catch(Exception ex)
        {
            SentryUtils.captureException(ex);
        }
        return false;
    }

    public boolean notifyCareContext(ABDMNotifyCareContextEvent event) {
        String episodeServiceToken = episodeServiceRestService.getAuthToken(appProperties.genericClientId);
        boolean success = true;
        Map<String,Object> map = new HashMap<>();
        map.put(EPISODE_ASSOCIATION_CARE_CONTEXT, Boolean.TRUE);
        map.put("episodeId",event.episodeId);
        if(episodeServiceToken != null) {
            ApiResponse<GetEpisodeByPersonResponse> episodeResponse = episodeServiceRestService.updateEpisodeDetails(episodeServiceToken,map);
            if(!Boolean.parseBoolean(episodeResponse.getSuccess())) {
                success = false;
            }
        }
        ABDMNotifyCareContextRequest req = new ABDMNotifyCareContextRequest(event, event.getHipId());
        String response = abdmApiHelper.notifyCareContext(req);
        if(null == response){
            success = false;
        }
        return success;
    }

    public ABDMLinkCareContextEvent getCareContextLinkingDetailsFor(String requestId) {
        Gson gson = new GsonBuilder().create();
        String abhaAddress = CacheUtils.getFromCache(requestId);
        String careContextDetailsString = CacheUtils.getFromCache(abhaAddress);
        ABDMLinkCareContextEvent careContextDetails = gson.fromJson(careContextDetailsString, ABDMLinkCareContextEvent.class);
        return careContextDetails;
    }

    public DiscoveryCacheDto getOnDiscoveryRequestDetailsFor(String key) {
        Gson gson = new GsonBuilder().create();
        String reqString = CacheUtils.getFromCache(key);
        DiscoveryCacheDto req = gson.fromJson(reqString, DiscoveryCacheDto.class);
        return req;
    }

    public void processDiscoveryRequest(ABDMDiscoveryRequest request) {

        Long refId = saveClientDetails(USER_DISCOVERY_EVENT,Utils.asJsonString(request));
        Map<String,String> matcherMap = getMatcherMapFrom(request);
        ABDMOnDiscoveryRequest data = getEpisodeDetailsWith(
                request.getTransactionId(),
                request.getRequestId(),
                getMatcherMapFrom(request));
        DiscoveryCacheDto cacheDto = new DiscoveryCacheDto(data,matcherMap.get(HEALTH_NUMBER_IN_USER_SERVICE));
        CacheUtils.putIntoCache(ABDM_ON_DISCOVERY_CACHE_PREFIX + data.getTransactionId(), Utils.asJsonString(cacheDto), ABDM_CACHE_DISCOVERY_REQUEST_TTL);
        logger.info("[ABDMOnDiscoveryRequest]" + Utils.asJsonString(data));
        boolean success = abdmApiHelper.onDiscovery(data);
        if(success) {
            ClientRequests clientRequests = clientRequestsService.getClientRequestsById(refId);
            String payload = Utils.asJsonString(data);
            clientRequests.setResponseData(payload.substring(0, Math.min(payload.length(), 255)));
            clientRequests.setResultDelivered(true);
            clientRequestsService.updateClientRequest(clientRequests);
        }
    }

    private Map<String, String> getMatcherMapFrom(ABDMDiscoveryRequest request) {
        Map<String, String> reqData = new HashMap<>();

        reqData.put(NAME, request.getPatient().name);
        reqData.put(YEAR_OF_BIRTH, request.getPatient().yearOfBirth);

        switch (request.getPatient().gender) {
            case "M":
                reqData.put(GENDER, GENDER_MALE);
                break;
            case "F":
                reqData.put(GENDER, GENDER_FEMALE);
                break;
            default:
                reqData.put(GENDER, GENDER_TRANSGENDER);
        }

        for (ABDMIdentifiers i : request.getPatient().getVerifiedIdentifiers()) {
            if (i.getType().equalsIgnoreCase(ABDM_IDENTIFIER_MOBILE))
                reqData.put(ABDM_IDENTIFIER_MOBILE, removeINDPhonePrefix(i.getValue()));
            if (i.getType().equalsIgnoreCase(Constants.ABDM_IDENTIFIER_HEALTH_NUMBER))
                reqData.put(Constants.HEALTH_NUMBER_IN_USER_SERVICE, i.getValue());
            if (i.getType().equalsIgnoreCase(ABDM_IDENTIFIER_HEALTH_ID))
                reqData.put(HEALTH_ID_IN_USER_SERVICE, i.getValue());
        }
        return reqData;
    }

    private String removeINDPhonePrefix(String number) {
        if (number.startsWith("+91-"))
            return number.substring(4);
        return number;
    }

    private ABDMOnDiscoveryRequest getEpisodeDetailsWith(String transactionId, String requestId, Map<String,String> data) {
        ApiResponse<Integer>  userResponse = null;
        ApiResponse<List<GetEpisodeByPersonResponse>> episodeResponse = null;
        ABDMOnDiscoveryRequest req;

        String userServiceToken = userServiceRestService.getAuthToken(appProperties.genericClientId);
        logger.info("[userServiceToken]" + userServiceToken);
        if(userServiceToken != null) {
            userResponse =  userServiceRestService.searchUser(userServiceToken,data);
            logger.info(Utils.asJsonString(userResponse));
        }

        String episodeServiceToken = episodeServiceRestService.getAuthToken(appProperties.genericClientId);
        logger.info("[episodeServiceToken]" + episodeServiceToken);
        if(episodeServiceToken != null && userResponse != null && Boolean.parseBoolean(userResponse.getSuccess())) {
            episodeResponse = episodeServiceRestService.getEpisodesFromPerson(episodeServiceToken,userResponse.getData());
            logger.info(Utils.asJsonString(episodeResponse));
        }
        else if(userResponse != null && !Boolean.parseBoolean(userResponse.getSuccess())) {
           req = new ABDMOnDiscoveryRequest(
                    transactionId,
                    userResponse.getMessage(),requestId);
           return req;
        }

        Map<String, String> resp = new HashMap<>();

        if(episodeResponse != null && Boolean.parseBoolean(episodeResponse.getSuccess())) {
            for(GetEpisodeByPersonResponse episode : episodeResponse.getData()) {
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                LocalDateTime dateTime = LocalDateTime.parse(episode.getCreatedDate(),formatter);
                OffsetDateTime offsetDateTime =  dateTime.atOffset(ZoneOffset.UTC);
                offsetDateTime = offsetDateTime.withOffsetSameInstant(ZoneOffset.ofHoursMinutes(5,30));
                resp.put(String.valueOf(episode.getId()),offsetDateTime.format(formatter));
            }
            req = new ABDMOnDiscoveryRequest(
                    transactionId,
                    userResponse.getData().toString(),
                    data.get(NAME),
                    requestId,
                    resp);
            return req;
        }
        req = new ABDMOnDiscoveryRequest(transactionId,GENERIC_ERROR,requestId);

        Gson gson = new GsonBuilder().create();

        return req;
    }

    Long saveClientDetails(String eventName,String requestData) {
        Event event = eventService.findEventByEventName(eventName);
        ClientRequests clientRequests = new ClientRequests();
        clientRequests.setEventId(event.getId());
        if(!HI_TRANSFER_REQUEST_EVENT.equals(eventName)) {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            String clientName = authenticationService.getCurrentUsername(authentication);
            Client client = clientService.findByUsername(clientName);
            clientRequests.setClientId(client.getId());
        }
        clientRequests.setRequestData(requestData);
        return clientRequestsService.saveClientRequest(clientRequests);
    }

    public void processLinkInitiationRequest(ABDMLinkInitiationRequest request) throws JsonProcessingException {
        Long refId = saveClientDetails(USER_LINK_INITIATION_EVENT,Utils.asJsonString(request));
        ABDMOnLinkInitiationRequest abdmOnLinkInitiationRequest = sendOtpSms(request);
        boolean success = abdmApiHelper.OnLinkInit(abdmOnLinkInitiationRequest);
       logger.info("[ABDMOnLinkInitiationRequest]"+Utils.asJsonString(abdmOnLinkInitiationRequest));
        if(success) {
            ClientRequests clientRequests = clientRequestsService.getClientRequestsById(refId);
            String payload = Utils.asJsonString(abdmOnLinkInitiationRequest);
            clientRequests.setResponseData(payload.substring(0, Math.min(payload.length(), 255)));
            clientRequests.setResultDelivered(true);
            clientRequestsService.updateClientRequest(clientRequests);
        }
    }

    public ABDMOnLinkInitiationRequest sendOtpSms(ABDMLinkInitiationRequest request) {
        ApiResponse<LinkInitiationOtpResponse> response = null;
        ABDMOnLinkInitiationRequest abdmOnLinkInitiationRequest;
        try {
            String userId = request.patient.getReferenceNumber();
            logger.info("[ON_DISCOVERY_REQUEST]" + CacheUtils.hasKey(ABDM_ON_DISCOVERY_CACHE_PREFIX + request.getTransactionId()));
            if(CacheUtils.hasKey(ABDM_ON_DISCOVERY_CACHE_PREFIX + request.getTransactionId())){
                String token = nikshayRestService.getAuthToken(null);
                if(null != token && null != userId) {
                    response = nikshayRestService.sendOtpLinkInitiation(token,userId);
                }
            }

            if(response != null) {
                if(Boolean.parseBoolean(response.getSuccess())) {
                    abdmOnLinkInitiationRequest = new ABDMOnLinkInitiationRequest(request.transactionId,request.requestId,userId,response.getData().getMessage(),Utils.getTimeinFormat(ABDM_TIMESTAMP_FORMAT,600L));
                    cacheLinkRequest(request,response.getData());
                }
                else {
                    abdmOnLinkInitiationRequest = new ABDMOnLinkInitiationRequest(request.transactionId,request.requestId,response.getMessage());
                }
            }
            else {
                abdmOnLinkInitiationRequest = new ABDMOnLinkInitiationRequest(request.transactionId,request.requestId,GENERIC_ERROR);
            }
        }
        catch (Exception ex) {
            SentryUtils.captureException(ex);
            abdmOnLinkInitiationRequest = new ABDMOnLinkInitiationRequest(request.transactionId,request.requestId,GENERIC_ERROR);
        }
        return abdmOnLinkInitiationRequest;
    }

    public void processLinkConfirmationRequest(ABDMLinkConfirmationRequest abdmLinkConfirmationRequest) {
        Long refId = saveClientDetails(USER_LINK_CONFIRMATION_EVENT,Utils.asJsonString(abdmLinkConfirmationRequest));
       ABDMOnLinkConfirmationRequest onLinkConfirmRequest = validateOtpAndAddCareContexts(abdmLinkConfirmationRequest);
        boolean success = abdmApiHelper.OnLinkConfirm(onLinkConfirmRequest);
        logger.info("[ABDMOnLinkConfirmationRequest]"+Utils.asJsonString(onLinkConfirmRequest));
        if(success) {
            ClientRequests clientRequests = clientRequestsService.getClientRequestsById(refId);
            String payload = Utils.asJsonString(onLinkConfirmRequest);
            clientRequests.setResponseData(payload.substring(0, Math.min(payload.length(), 255)));
            clientRequests.setResultDelivered(true);
            clientRequestsService.updateClientRequest(clientRequests);
        }
    }

    private void cacheLinkRequest(ABDMLinkInitiationRequest linkInitiationRequest, LinkInitiationOtpResponse otpResponse) {
        DiscoveryCacheDto discoveryCacheDto = getOnDiscoveryRequestDetailsFor(ABDM_ON_DISCOVERY_CACHE_PREFIX +linkInitiationRequest.transactionId);
        ABDMOnDiscoveryRequest onDiscoveryRequest = discoveryCacheDto.getAbdmOnDiscoveryRequest();
        List<String> episodeIds = linkInitiationRequest.patient.getCareContexts().stream().map(p -> p.referenceNumber).collect(Collectors.toList());
        Map<String,String> episodeIdEnrollmentDateMap  = new HashMap<>();
        for(ABDMCareContextDTO.CareContext careContext : onDiscoveryRequest.getPatient().getCareContexts()) {
            if(episodeIds.contains(careContext.getReferenceNumber())) {
                episodeIdEnrollmentDateMap.put(careContext.getReferenceNumber(),careContext.getDisplay());
            }
        }
        String personId = onDiscoveryRequest.getPatient().getReferenceNumber();
        String name = onDiscoveryRequest.getPatient().getDisplay();
        String abhaAddress = linkInitiationRequest.patient.getId();
        onDiscoveryRequest.setPatient(new ABDMCareContextDTO(personId,name,episodeIdEnrollmentDateMap));
        LinkConfirmRedisDto linkConfirmRedisDto = new LinkConfirmRedisDto(onDiscoveryRequest, otpResponse.getOtp(),abhaAddress,discoveryCacheDto.getHealthNumber());
        CacheUtils.putIntoCache(ABDM_LINK_INITIATION_CACHE_PREFIX +personId,Utils.asJsonString(linkConfirmRedisDto),ABDM_CACHE_DISCOVERY_REQUEST_TTL);
    }

    public ABDMOnLinkConfirmationRequest validateOtpAndAddCareContexts(ABDMLinkConfirmationRequest abdmLinkConfirmationRequest) {
        try {
            String personId = abdmLinkConfirmationRequest.getConfirmation().getLinkRefNumber();
            if (!CacheUtils.hasKey(ABDM_LINK_INITIATION_CACHE_PREFIX + personId)) {
                return new ABDMOnLinkConfirmationRequest(abdmLinkConfirmationRequest.requestId, GENERIC_ERROR);
            }

            Gson gson = new GsonBuilder().create();
            String json = CacheUtils.getFromCache(ABDM_LINK_INITIATION_CACHE_PREFIX + personId);
            LinkConfirmRedisDto redisDto = gson.fromJson(json, LinkConfirmRedisDto.class);
            logger.info("[LinkConfirmRedisDto]" + Utils.asJsonString(redisDto));

            String token = abdmLinkConfirmationRequest.confirmation.getToken();
            if (!token.equals(redisDto.getOtp())) {
                return new ABDMOnLinkConfirmationRequest(abdmLinkConfirmationRequest.getRequestId(), ABDM_INVALID_OTP_ERROR);
            }

            String episodeServiceToken = episodeServiceRestService.getAuthToken(appProperties.genericClientId);
            if (episodeServiceToken == null) {
                return new ABDMOnLinkConfirmationRequest(abdmLinkConfirmationRequest.getRequestId(), GENERIC_ERROR);
            }

            List<ABDMCareContextDTO.CareContext> episodeDto = redisDto.getAbdmOnDiscoveryRequest().getPatient().getCareContexts();
            boolean success = true;
            for (ABDMCareContextDTO.CareContext episode : episodeDto) {
                // Add Care context for episode
                Map<String,Object> map = new HashMap<>();
                map.put(EPISODE_ASSOCIATION_CARE_CONTEXT, Boolean.TRUE);
                map.put(EPISODE_ID, episode.getReferenceNumber());
                ApiResponse<GetEpisodeByPersonResponse> episodeResponse = episodeServiceRestService.updateEpisodeDetails(episodeServiceToken,map);
                if (!Boolean.parseBoolean(episodeResponse.getSuccess())) {
                    success = false;
                    break;
                }
                logger.info(Utils.asJsonString(episodeResponse));
            }

            if (success) {
                // Update user's abha address
                Map<String, Object> requestMap = new HashMap<>();
                List<ExternalIdRequest> externalIds = new ArrayList<>();
                if (redisDto.getAbhaAddress() != null) {
                    externalIds.add(new ExternalIdRequest(HEALTH_ADDRESS, redisDto.getAbhaAddress()));
                }
                if (redisDto.getAbhaId() != null) {
                    externalIds.add(new ExternalIdRequest(HEALTH_ID, redisDto.getAbhaId()));
                }
                requestMap.put(FIELD_EXTERNAL_IDS, externalIds);
                if (!episodeDto.isEmpty()) {
                    requestMap.put(EPISODE_ID, episodeDto.get(0).getReferenceNumber());
                }
                episodeServiceToken = episodeServiceRestService.getAuthToken(appProperties.genericClientId);
                if (episodeServiceToken != null) {
                    episodeServiceRestService.updateEpisodeDetails(episodeServiceToken, requestMap);
                    return new ABDMOnLinkConfirmationRequest(redisDto.getAbdmOnDiscoveryRequest().getPatient(), abdmLinkConfirmationRequest.getRequestId());
                }
            }
        } catch (Exception e) {
            SentryUtils.captureException(e);
        }
        return new ABDMOnLinkConfirmationRequest(abdmLinkConfirmationRequest.getRequestId(), GENERIC_ERROR);
    }

    public boolean processConsentInitiation(ABDMConsentRequestOnInitiation abdmConsentRequestOnInitiation) {
        saveClientDetails(CONSENT_INITIATION_EVENT,Utils.asJsonString(abdmConsentRequestOnInitiation));
        Gson gson = new GsonBuilder().create();
        String requestId = abdmConsentRequestOnInitiation.getResp().getRequestId();
        if(!CacheUtils.hasKey(ABDM_CONSENT_INITIATION_CACHE_PREFIX + requestId)) {
            return false;
        }

        String json = CacheUtils.getFromCache(ABDM_CONSENT_INITIATION_CACHE_PREFIX + requestId);
        ABDMConsentRequestDto abdmConsentRequestDto = gson.fromJson(json,ABDMConsentRequestDto.class);
        String consentRequestId = abdmConsentRequestOnInitiation.getConsentRequest().getId();
        abdmConsentRequestDto.setConsentRequestId(consentRequestId);
        rmqPublisher.send(abdmConsentRequestDto,UPDATE_CONSENT_REQUEST_ROUTING_KEY ,UPDATE_USER_EXCHANGE,new HashMap<>());

        return true;
    }
    public boolean addCareContextDirectAuth(ABDMAuthNotifyRequest abdmAuthNotifyRequest) {
        try{
            ABDMLinkCareContextEvent careContextDetails = getCareContextLinkingDetailsFor(abdmAuthNotifyRequest.getAuth().transactionId);
            LOGGER.info("ABDMLinkCareContextEvent "+ Utils.asJsonString(careContextDetails));
            ABDMAuthOnNotifyRequest abdmAuthOnNotifyRequest = new ABDMAuthOnNotifyRequest(abdmAuthNotifyRequest.requestId);
            abdmApiHelper.authOnNotify(abdmAuthOnNotifyRequest);
            if(!abdmAuthNotifyRequest.getAuth().status.equals("GRANTED")) {
                return true;
            }
            ABDMLinkCareContextRequest linkingRequest = new ABDMLinkCareContextRequest(
                    abdmAuthNotifyRequest.getAuth().accessToken,
                    Long.toString(careContextDetails.personId),
                    Long.toString(careContextDetails.episodeId),
                    careContextDetails.personName,
                    careContextDetails.enrollmentDate);
            CacheUtils.putIntoCache(linkingRequest.requestId, careContextDetails.abhaAddress, 7200);
            String response = abdmApiHelper.addCareContextRequest(linkingRequest);
            if (null != response) {
                return true;
            }
        }catch(Exception ex)
        {
            SentryUtils.captureException(ex);
        }
        return false;
    }

    public void processConsentNotification(ABDMConsentNotification abdmConsentNotification) {
        saveClientDetails(CONSENT_NOTIFICATION_EVENT,Utils.asJsonString(abdmConsentNotification));
        ABDMConsentRequestDto abdmConsentRequestDto = new ABDMConsentRequestDto();
        abdmConsentRequestDto.setAbdmConsentNotification(abdmConsentNotification);
        abdmConsentRequestDto.setConsentRequestId(abdmConsentNotification.getNotification().getConsentRequestId());
        rmqPublisher.send(abdmConsentRequestDto,UPDATE_CONSENT_REQUEST_ROUTING_KEY ,UPDATE_USER_EXCHANGE,new HashMap<>());
        ABDMConsentOnNotifyRequest abdmConsentOnNotifyRequest = new ABDMConsentOnNotifyRequest(abdmConsentNotification.getRequestId(),abdmConsentNotification.getNotification().getConsentArtefacts());
        abdmApiHelper.hiuConsentOnNotify(abdmConsentOnNotifyRequest);
        // fetch consent artefacts for all Ids
        if(CONSENT_GRANTED.equals(abdmConsentNotification.getNotification().getStatus())) {
            abdmConsentNotification.getNotification().getConsentArtefacts().forEach(consentArtefact -> abdmApiHelper.hiuConsentFetch(new ABDMConsentFetchRequest(consentArtefact.getId())));
        }
    }

    public void dataPullRequestInitiation(ABDMOnConsentFetchRequest abdmOnConsentFetchRequest) throws Exception {
        saveClientDetails(CONSENT_FETCH_EVENT,Utils.asJsonString(abdmOnConsentFetchRequest));
        if(null == abdmOnConsentFetchRequest.getError() && CONSENT_GRANTED.equals(abdmOnConsentFetchRequest.getConsent().getStatus())) {
            String consentId = abdmOnConsentFetchRequest.getConsent().getConsentDetail().getConsentId();
            ABDMOnConsentFetchRequest.Permission permission = abdmOnConsentFetchRequest.getConsent().getConsentDetail().getPermission();
            ABDMOnConsentFetchRequest.DateRange dateRange = permission.getDateRange();
            KeyMaterial keyMaterial = EncryptionUtil.generate();
            HIDataRequest hiDataRequest = new HIDataRequest(consentId,dateRange.getFrom(),dateRange.getTo(),callbackUrl + DATA_PUSH_URL,keyMaterial.getPublicKey(),permission.getDataEraseAt(),keyMaterial.getNonce());
            String hipName = abdmOnConsentFetchRequest.getConsent().getConsentDetail().getHip().getId();
            ConsentFetchRedisDto consentFetchRedisDto = new ConsentFetchRedisDto(hiDataRequest,hipName,keyMaterial.getPrivateKey());
            CacheUtils.putIntoCache(ABDM_DATA_REQUEST_CACHE_PREFIX +hiDataRequest.requestId,Utils.asJsonString(consentFetchRedisDto),ABDM_CACHE_DISCOVERY_REQUEST_TTL);
            abdmApiHelper.hiuDataRequest(hiDataRequest);
        }
    }

    public boolean processOnHIDataRequest(ABDMOnHIDataRequest abdmOnHIDataRequest) {
        saveClientDetails(HI_DATA_REQUEST_EVENT,Utils.asJsonString(abdmOnHIDataRequest));
        if(null == abdmOnHIDataRequest.getError()) {
            Gson gson = new GsonBuilder().create();
            String requestId = abdmOnHIDataRequest.getResp().getRequestId();
            if(!CacheUtils.hasKey(ABDM_DATA_REQUEST_CACHE_PREFIX + requestId)) {
                return false;
            }
            String json = CacheUtils.getFromCache(ABDM_DATA_REQUEST_CACHE_PREFIX + requestId);
            ConsentFetchRedisDto consentFetchRedisDto = gson.fromJson(json,ConsentFetchRedisDto.class);
            HIDataRequest.HiRequest hiRequest = consentFetchRedisDto.getHiDataRequest().hiRequest;
            HIDataRequest.KeyMaterial keyMaterial = hiRequest.keyMaterial;
            String hipName = consentFetchRedisDto.getHipName();
            UpdateUserMedicalRecordsRequest userMedicalRecordsRequest = new UpdateUserMedicalRecordsRequest(abdmOnHIDataRequest.getHiRequest().getTransactionId(),hiRequest.consent.id,consentFetchRedisDto.getPrivateKey(),keyMaterial.nonce,hipName);
            rmqPublisher.send(userMedicalRecordsRequest,UPDATE_MEDICAL_RECORDS_ROUTING_KEY ,UPDATE_USER_EXCHANGE,new HashMap<>());
            return true;
        }
        return false;
    }

    public Error processHITransfer(DataTransferRequest dataTransferRequest) {
        saveClientDetails(HI_TRANSFER_REQUEST_EVENT,Utils.asJsonString(dataTransferRequest));
        ApiResponse<GetRecordsByTxnIdResponse>  userResponse = null;
        String hipId = null;
        String consentId = null;
        HiuHITransferNotifyRequest hiuHITransferNotifyRequest;
        List<String> careContexts = dataTransferRequest.getEntries().stream().map(DataTransferRequest.Entry::getCareContextReference).collect(Collectors.toList());
        String userServiceToken = userServiceRestService.getAuthToken(appProperties.genericClientId);
        logger.info("[userServiceToken]" + userServiceToken);
        if(userServiceToken != null) {
            userResponse =  userServiceRestService.getRecordsByTxnId(userServiceToken,dataTransferRequest.getTransactionId());
            logger.info(Utils.asJsonString(userResponse));
        }
        if(null != userResponse) {
            if( Boolean.parseBoolean(userResponse.getSuccess())){
                hipId = userResponse.getData().getHipId();
                consentId = userResponse.getData().getConsentId();
            }
            else {
                return new Error(1000,userResponse.getMessage());
            }
        }

        UpdateUserMedicalRecordsRequest userMedicalRecordsRequest = new UpdateUserMedicalRecordsRequest();
        userMedicalRecordsRequest.setEncryptedJson(Utils.asJsonString(dataTransferRequest));
        userMedicalRecordsRequest.setTransactionId(dataTransferRequest.getTransactionId());
        rmqPublisher.send(userMedicalRecordsRequest,UPDATE_MEDICAL_RECORDS_ROUTING_KEY ,UPDATE_USER_EXCHANGE,new HashMap<>());
        hiuHITransferNotifyRequest = new HiuHITransferNotifyRequest(dataTransferRequest.getTransactionId(),consentId,careContexts,hipId,ABDM_HI_TRANSFER_SESSION_STATUS,hiuId);
        abdmApiHelper.hiuTransferNotify(hiuHITransferNotifyRequest);

        return null;
    }

}
