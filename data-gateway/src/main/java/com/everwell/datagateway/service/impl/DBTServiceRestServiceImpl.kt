package com.everwell.datagateway.service.impl;

import com.everwell.datagateway.constants.Constants
import com.everwell.datagateway.models.response.ApiResponse
import com.everwell.datagateway.models.response.MicroServiceGenericAuthResponse
import com.everwell.datagateway.service.DBTServiceRestService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.core.ParameterizedTypeReference
import org.springframework.stereotype.Service;
import java.util.*

@Service
open class DBTServiceRestServiceImpl : DBTServiceRestService() {

    private val logger : Logger = LoggerFactory.getLogger(DBTServiceRestServiceImpl::class.simpleName)

    override fun getAuthToken(client_id: String?): String? {
        return getAuthToken(client_id, appProperties.dbtServiceUrl)
    }

    fun getAuthToken(client_id: String?, url: String): String? {
        if(!clientIdAccessTokenMap.contains(client_id) || Date().time >= clientIdRefreshTimeMap[client_id]!!){
            logger.info("[DBTServiceRestServiceImpl] Fetching auth token...")
            val headers = mutableMapOf<String, String>()
            headers["X-Client-Id"] = client_id!!
            val response = authenticate(
                url + "/v1/client",
                headers,
                object:  ParameterizedTypeReference<ApiResponse<MicroServiceGenericAuthResponse>>() {})

            clientIdAccessTokenMap[client_id] = response.body?.data?.authToken !!
            if (response.body?.data?.nextRefresh == null ||  response.body?.data?.nextRefresh!! == 0L)
                clientIdRefreshTimeMap[client_id] = Date().time + Constants.MICROSERVICE_REFRESH_EXPIRY
            else
                clientIdRefreshTimeMap[client_id] = response.body?.data?.nextRefresh!!
        }
        return clientIdAccessTokenMap[client_id]
    }

}
