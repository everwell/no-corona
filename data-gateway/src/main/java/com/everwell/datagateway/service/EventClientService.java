package com.everwell.datagateway.service;

import com.everwell.datagateway.entities.EventClient;

import java.util.List;

public interface EventClientService {
    List<EventClient> getEventClientByClientId(Long clientId);
}
