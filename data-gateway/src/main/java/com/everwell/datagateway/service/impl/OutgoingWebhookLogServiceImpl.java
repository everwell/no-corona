package com.everwell.datagateway.service.impl;

import com.everwell.datagateway.entities.OutgoingWebhookLog;
import com.everwell.datagateway.mappers.OutgoingWebhookLogMapper;
import com.everwell.datagateway.models.dto.OutgoingWebhookLogDTO;
import com.everwell.datagateway.repositories.OutgoingWebhookLogRepository;
import com.everwell.datagateway.service.OutgoingWebhookLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class OutgoingWebhookLogServiceImpl implements OutgoingWebhookLogService {

    @Autowired
    OutgoingWebhookLogRepository outgoingWebhookLogRepository;

    /**
     * Method to save outgoingWebhookLog for given details
     * @param outgoingWebhookLogDTO - Details of outgoingWebhook
     * @return - Id of saved outgoing webhook log
     */
    @Override
    public Long saveOutgoingWebhookLog(OutgoingWebhookLogDTO outgoingWebhookLogDTO) {
        Long id = null;
        OutgoingWebhookLog outgoingWebhookLogs = OutgoingWebhookLogMapper.INSTANCE.outgoingWebhookLogDTOToOutgoingWebhookLog(outgoingWebhookLogDTO);
        if(outgoingWebhookLogs != null) {
            id = outgoingWebhookLogRepository.save(outgoingWebhookLogs).getId();
        }
        return id;
    }

    /**
     * Method to update outgoingWebhookLog
     * @param id - Id of log which is to be updated
     * @param outgoingWebhookLogDTO - Details of outgoingWebhook
     */
    @Override
    public void updateOutgoingWebhookLog(Long id, OutgoingWebhookLogDTO outgoingWebhookLogDTO) {
        OutgoingWebhookLog outgoingWebhookLogs = getOutgoingWebhookLog(id);
        if(null != outgoingWebhookLogs) {
            outgoingWebhookLogs.setClientId(outgoingWebhookLogDTO.getClientId());
            outgoingWebhookLogs.setEventId(outgoingWebhookLogDTO.getEventId());
            outgoingWebhookLogs.setSubscriberUrlId(outgoingWebhookLogDTO.getSubscriberUrlId());
            outgoingWebhookLogs.setCallbackResponseCode(outgoingWebhookLogDTO.getCallbackResponseCode());
            outgoingWebhookLogs.setResponse(outgoingWebhookLogDTO.getResponse());
            if (outgoingWebhookLogDTO.getCallbackResponseCode() == 200) {
                outgoingWebhookLogs.setResultDelivered(true);
                outgoingWebhookLogs.setDeliveredAt(outgoingWebhookLogDTO.getDeliveredAt());
            }
            outgoingWebhookLogRepository.save(outgoingWebhookLogs);
        }
    }

    /**
     * Method to fetch outgoingWebhookLog for given Id
     * @param id - Id of log which is to be fetched
     * @return = outgoingWebhookLog for given Id
     */
    private OutgoingWebhookLog getOutgoingWebhookLog(Long id){
        return outgoingWebhookLogRepository.findById(id).orElse(null);
    }

}
