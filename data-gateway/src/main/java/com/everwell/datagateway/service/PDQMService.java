package com.everwell.datagateway.service;

import com.everwell.datagateway.exceptions.PDQMCustomException;
import com.everwell.datagateway.models.request.EpisodeSearchRequest;
import com.everwell.datagateway.models.request.RequestParamAndModifiers;
import com.everwell.datagateway.models.response.EpisodeSearchResult;
import com.fasterxml.jackson.databind.JsonNode;
import org.hl7.fhir.r4.model.CapabilityStatement;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

public interface PDQMService {
    void createEpisodeSearchRequest(EpisodeSearchRequest episodeSearchRequest, RequestParamAndModifiers paramAndModifiers);

    EpisodeSearchRequest getEpisodeSearchRequestFromRequest(Map<String, Object> parameterMap) throws PDQMCustomException;

    JsonNode getPatientBundle(HttpServletRequest request, EpisodeSearchRequest episodeSearchRequest, EpisodeSearchResult episodeSearchResult);

    JsonNode getOperationOutcomeForException(Exception exception);

    JsonNode getOperationOutcomeForPDQMException(PDQMCustomException exception);

    String getCapabilityStatement();

}
