package com.everwell.datagateway.service

import com.everwell.datagateway.models.response.ApiResponse
import com.everwell.datagateway.models.response.UserServiceCreateResponse
import com.everwell.datagateway.models.response.GetRecordsByTxnIdResponse

abstract class UserServiceRestService : BaseRestService() {
    abstract fun searchUser(token: String,data: Map<String,String>) : ApiResponse<Int>
    abstract fun createUser(url: String, clientId: Long, data: Map<String, Any>) : ApiResponse<UserServiceCreateResponse>
    abstract  fun getRecordsByTxnId(token: String, transactionId: String): ApiResponse<GetRecordsByTxnIdResponse>
}