-- Add new columns only if they do not exist
DO $$
BEGIN
    IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name='client_requests' AND column_name='request_url') THEN
        ALTER TABLE client_requests ADD COLUMN request_url TEXT;
    END IF;
    IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name='client_requests' AND column_name='request_headers') THEN
        ALTER TABLE client_requests ADD COLUMN request_headers TEXT;
    END IF;
END $$;
