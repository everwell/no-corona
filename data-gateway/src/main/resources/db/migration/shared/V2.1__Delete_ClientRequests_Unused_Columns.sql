ALTER TABLE public.client_requests
DROP COLUMN IF EXISTS reference_id;

ALTER TABLE public.client_requests
DROP COLUMN IF EXISTS queue_task_id;