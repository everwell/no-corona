package com.everwell.registryservice.mappers;

import com.everwell.registryservice.models.db.SidebarItem;
import com.everwell.registryservice.models.dto.SidebarItemResponse;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface SidebarItemMapper {

    SidebarItemMapper INSTANCE = Mappers.getMapper(SidebarItemMapper.class);

    List<SidebarItemResponse> sidebarItemListToSidebarItemResponseList(List<SidebarItem> sidebarItem);

    SidebarItemResponse sidebarItemToSidebarItemResponse(SidebarItem sidebarItem);

}
