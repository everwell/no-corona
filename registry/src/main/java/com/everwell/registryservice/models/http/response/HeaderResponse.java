package com.everwell.registryservice.models.http.response;

import com.everwell.registryservice.constants.Constants;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import org.springframework.data.util.Pair;
import org.springframework.util.CollectionUtils;

import java.util.HashMap;
import java.util.Map;

@NoArgsConstructor
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class HeaderResponse {
    @JsonProperty(value = "HeaderLinks")
    private Map<String, Map<String, String>> headerLinks;
    @JsonProperty(value = "SearchCriteria")
    private Map<String, Map<String, String>> searchCriteria;
    @JsonProperty(value = "DropDownItems")
    private Map<String, Map<String, String>> dropdownItems;
    private boolean isUserLoggedIn = false;

    public void addHeaderLink(Pair<String, String> newPair) {
        if(CollectionUtils.isEmpty(headerLinks)) {
            headerLinks = new HashMap<>();
        }
        Map<String, String> option = new HashMap<>();
        option.put(Constants.KEY, newPair.getFirst());
        option.put(Constants.VALUE, newPair.getSecond());
        headerLinks.put(String.valueOf(headerLinks.keySet().size() + 1), option);
    }

    public void addSearchCriteriaOption(Pair<String, String> newPair) {
        if(CollectionUtils.isEmpty(searchCriteria)) {
            searchCriteria = new HashMap<>();
        }
        Map<String, String> option = new HashMap<>();
        option.put(Constants.KEY, newPair.getFirst());
        option.put(Constants.VALUE, newPair.getSecond());
        searchCriteria.put(String.valueOf(searchCriteria.keySet().size()+1), option);
    }

    public void addDropdownOption(Pair<String, String> newPair) {
        if(CollectionUtils.isEmpty(dropdownItems)) {
            dropdownItems = new HashMap<>();
        }
        Map<String, String> option = new HashMap<>();
        option.put(Constants.KEY, newPair.getFirst());
        option.put(Constants.VALUE, newPair.getSecond());
        dropdownItems.put(String.valueOf(dropdownItems.keySet().size()+1), option);
    }
}
