package com.everwell.registryservice.models.db;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "deployment_language_map")
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class DeploymentLanguageMap {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long deploymentId;

    private Long languageId;

    @JsonFormat(shape = JsonFormat.Shape.NUMBER)
    private LocalDateTime createdAt;

    @JsonFormat(shape = JsonFormat.Shape.NUMBER)
    private LocalDateTime stoppedAt;

    public DeploymentLanguageMap(Long deploymentId, Long languageId, LocalDateTime createdAt)
    {
        this.deploymentId = deploymentId;
        this.languageId = languageId;
        this.createdAt = createdAt;
    }
}
