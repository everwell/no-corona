package com.everwell.registryservice.models.db;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "hierarchy")
@Getter
@Setter
@NoArgsConstructor
public class Hierarchy {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long level;
    private String name;
    private String type;
    private String code;
    @Column(name = "parent_id")
    private Long parentId;
    @Column(name = "level_1_id")
    private Long level1Id;
    @Column(name = "level_1_name")
    private String level1Name;
    @Column(name = "level_1_type")
    private String level1Type;
    @Column(name = "level_1_code")
    private String level1Code;
    @Column(name = "level_2_id")
    private Long level2Id;
    @Column(name = "level_2_name")
    private String level2Name;
    @Column(name = "level_2_type")
    private String level2Type;
    @Column(name = "level_2_code")
    private String level2Code;
    @Column(name = "level_3_id")
    private Long level3Id;
    @Column(name = "level_3_name")
    private String level3Name;
    @Column(name = "level_3_type")
    private String level3Type;
    @Column(name = "level_3_code")
    private String level3Code;
    @Column(name = "level_4_id")
    private Long level4Id;
    @Column(name = "level_4_name")
    private String level4Name;
    @Column(name = "level_4_type")
    private String level4Type;
    @Column(name = "level_4_code")
    private String level4Code;
    @Column(name = "level_5_id")
    private Long level5Id;
    @Column(name = "level_5_name")
    private String level5Name;
    @Column(name = "level_5_type")
    private String level5Type;
    @Column(name = "level_5_code")
    private String level5Code;
    @Column(name = "level_6_id")
    private Long level6Id;
    @Column(name = "level_6_name")
    private String level6Name;
    @Column(name = "level_6_type")
    private String level6Type;
    @Column(name = "level_6_code")
    private String level6Code;
    private boolean hasChildren;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    private LocalDateTime startDate;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    private LocalDateTime endDate;
    private boolean active;
    @Column(name = "created_at")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    private LocalDateTime createdAt;
    @Column(name = "updated_at")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    private LocalDateTime updatedAt;
    @Column(name = "merge_status")
    private String mergeStatus;
    @Column(name = "client_id")
    private Long clientId;
    @Column(name = "deployment_id")
    private Long deploymentId;
}
