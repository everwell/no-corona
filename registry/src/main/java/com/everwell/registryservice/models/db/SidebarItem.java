package com.everwell.registryservice.models.db;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "sidebar_item")
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class SidebarItem {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String icon;

    private String link;

    @Column(name = "relative_path")
    private Boolean relativePath;
}
