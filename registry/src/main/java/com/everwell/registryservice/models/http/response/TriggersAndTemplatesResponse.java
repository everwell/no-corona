package com.everwell.registryservice.models.http.response;

import com.everwell.registryservice.models.db.Trigger;
import com.everwell.registryservice.models.http.response.ins.TemplateResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class TriggersAndTemplatesResponse {
    private Long id;
    private Long triggerId;
    private Long hierarchyId;
    private Long clientId;
    private Boolean timeRelated;
    private String module;
    private Boolean active;
    private String deploymentCode;
    private String cronTime;
    private String eventName;
    private String functionName;
    private boolean isActive;
    private boolean mandatory;
    private boolean entityTimeRelated;
    private String notificationType;
    private TemplateResponse defaultTemplate;
    private List<TemplateResponse> templates;

    public TriggersAndTemplatesResponse(Trigger trigger, String deploymentCode, TemplateResponse defaultTemplate, List<TemplateResponse> templates)
    {
        this.id = trigger.getId();
        this.triggerId = trigger.getTriggerId();
        this.hierarchyId = trigger.getHierarchyId();
        this.clientId = trigger.getClientId();
        this.timeRelated = trigger.getTimeRelated();
        this.module = trigger.getModule();
        this.active = trigger.getActive();
        this.deploymentCode = deploymentCode;
        this.cronTime = trigger.getCronTime();
        this.eventName = trigger.getEventName();
        this.functionName = trigger.getFunctionName();
        this.isActive = trigger.getActive();
        this.mandatory = trigger.getMandatory();
        this.entityTimeRelated = trigger.getTimeRelated();
        this.notificationType = trigger.getNotificationType();
        this.defaultTemplate = defaultTemplate;
        this.templates = templates;
    }
}
