package com.everwell.registryservice.models.dto;

import com.everwell.registryservice.exceptions.ValidationException;
import com.everwell.registryservice.models.http.requests.TaskListItemRequest;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@ToString
public class TaskListItem implements Serializable {
    private String deploymentCode;
    private Long taskListId;
    private String displayColumns;
    private Long displayOrder;

    public void validate(TaskListItem taskListItem) {
        if (StringUtils.isEmpty(taskListItem.getDeploymentCode()))
            throw new ValidationException("deployment code is required");
        if (StringUtils.isEmpty(taskListItem.getDisplayColumns()))
            throw new ValidationException("display columns are required");
    }
}
