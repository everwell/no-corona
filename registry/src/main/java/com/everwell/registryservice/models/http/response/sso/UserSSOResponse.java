package com.everwell.registryservice.models.http.response.sso;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.Date;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserSSOResponse {
    private Long userId;

    private String username;

    private Date createdAt;

    private Date updatedAt;

    private Date passwordLastUpdatedDate;
}
