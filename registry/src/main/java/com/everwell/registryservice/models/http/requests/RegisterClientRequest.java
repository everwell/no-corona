package com.everwell.registryservice.models.http.requests;

import com.everwell.registryservice.exceptions.ValidationException;
import lombok.*;
import org.apache.commons.lang3.StringUtils;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class RegisterClientRequest {

    private String name;

    private String password;

    private Long eventFlowId;

    private String askForHelpUrl;

    public void validate() {
        if (StringUtils.isEmpty(name))
            throw new ValidationException("name is required");
        if (StringUtils.isEmpty(password))
            throw new ValidationException("password is required");
    }
}
