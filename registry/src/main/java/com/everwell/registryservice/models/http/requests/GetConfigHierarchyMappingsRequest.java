package com.everwell.registryservice.models.http.requests;

import com.everwell.registryservice.constants.FieldValidationMessages;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetConfigHierarchyMappingsRequest {
    @NotEmpty(message = FieldValidationMessages.REQUEST_MISSING_DATA)
    private Map<Long, List<String>> hierarchies;
}
