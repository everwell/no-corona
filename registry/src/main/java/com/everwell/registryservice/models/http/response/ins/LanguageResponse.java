package com.everwell.registryservice.models.http.response.ins;

import com.everwell.registryservice.models.dto.LanguageMapping;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class LanguageResponse {
    List<LanguageMapping> languageMappings;
}
