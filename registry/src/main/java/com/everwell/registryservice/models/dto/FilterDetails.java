package com.everwell.registryservice.models.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@ToString
public class FilterDetails implements Serializable {
    private String filterName;
    private String value;
    private Long filterValueId;
}
