package com.everwell.registryservice.models.http.response;

import com.everwell.registryservice.models.db.Client;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Getter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ClientResponse {
    Long id;
    String name;
    String authToken;
    Long nextRefresh;
    String askForHelpUrl;

    public ClientResponse(Client client, String authToken, Long nextRefresh, String url) {
        this.id = client.getId();
        this.name = client.getName();
        this.authToken = authToken;
        this.nextRefresh = nextRefresh;
        this.askForHelpUrl = url;
    }

    public ClientResponse(Client client) {
        this.id = client.getId();
        this.name = client.getName();
        this.askForHelpUrl = client.getAskforhelpurl();
    }

}
