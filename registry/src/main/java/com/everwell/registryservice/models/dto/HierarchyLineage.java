package com.everwell.registryservice.models.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class HierarchyLineage {
    private Long id;
    private Map<Long, Long> ancestralLevelMap;
}
