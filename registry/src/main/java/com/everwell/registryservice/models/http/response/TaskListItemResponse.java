package com.everwell.registryservice.models.http.response;

import com.everwell.registryservice.models.db.DeploymentTaskList;
import com.everwell.registryservice.models.db.TaskList;
import com.everwell.registryservice.models.db.TaskListColumn;
import com.everwell.registryservice.models.db.TaskListFilterMap;
import com.everwell.registryservice.models.dto.FilterDetails;
import lombok.*;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class TaskListItemResponse {
    private Long id;
    private Long deploymentId;
    private String name;
    private String webIcon;
    private String appIcon;
    private String description;
    private List<TaskListColumn> displayColumns;
    private String extraData;
    private Long displayOrder;
    private List<FilterDetails> filters;

    public TaskListItemResponse(DeploymentTaskList taskList, TaskList taskListItem)
    {
        this.id = taskList.getId();
        this.deploymentId = taskList.getDeploymentId();
        this.name = taskListItem.getName();
        this.webIcon = taskListItem.getWebIcon();
        this.appIcon = taskListItem.getAppIcon();
        this.description = taskListItem.getDescription();
        this.extraData = taskListItem.getExtraData();
        this.displayOrder = taskList.getDisplayOrder();
    }
}

