package com.everwell.registryservice.models.db;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "merm_management")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MermManagement {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "imei")
    private String IMEI;
    @Column(name = "last_battery")
    private String lastBattery;
    @Column(name = "last_seen")
    private LocalDateTime lastSeen;
    @Column(name = "last_opened")
    private LocalDateTime lastOpened;
    private boolean active;
    @Column(name = "deactivated_on")
    private LocalDateTime deactivatedOn;
    @Column(name = "alarm_enabled")
    private boolean alarmEnabled;
    @Column(name = "alarm_time")
    private LocalDateTime alarmTime;
    @Column(name = "refill_alarm_enabled")
    private boolean refillAlarmEnabled;
    @Column(name = "refill_date")
    private LocalDateTime refillDate;
    @Column(name = "rt_hours")
    private Long RTHours;
    @Column(name = "hierarchy_mapping")
    private Long hierarchyMapping;
    @Column(name = "lid_feedback")
    private Long lidFeedback;
    @Column(name = "current_status")
    private String currentStatus;
}
