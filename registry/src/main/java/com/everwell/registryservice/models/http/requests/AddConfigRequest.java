package com.everwell.registryservice.models.http.requests;

import com.everwell.registryservice.constants.FieldValidationMessages;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class AddConfigRequest {
    @NotBlank(message = FieldValidationMessages.CONFIG_NAME_MANDATORY)
    private String name;
    @NotBlank(message = FieldValidationMessages.CONFIG_VALUE_MANDATORY)
    private String defaultValue;
    @NotBlank(message = FieldValidationMessages.CONFIG_TYPE_MANDATORY)
    private String type;
}
