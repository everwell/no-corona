package com.everwell.registryservice.models.http.requests;

import com.everwell.registryservice.constants.FieldValidationMessages;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ConfigHierarchyMappingRequest {
    @NotNull(message = FieldValidationMessages.HIERARCHY_ID_MANDATORY)
    private Long hierarchyId;
    @NotBlank(message = FieldValidationMessages.CONFIG_NAME_MANDATORY)
    private String configName;
}
