package com.everwell.registryservice.controller;

import com.everwell.registryservice.constants.ValidationStatusEnum;
import com.everwell.registryservice.exceptions.UnauthorizedException;
import com.everwell.registryservice.mappers.HierarchyMapper;
import com.everwell.registryservice.models.db.Client;
import com.everwell.registryservice.models.db.UserAccess;
import com.everwell.registryservice.models.dto.*;
import com.everwell.registryservice.models.http.requests.AddHierarchyRequest;
import com.everwell.registryservice.models.http.requests.UpdateHierarchyRequest;
import com.everwell.registryservice.models.http.requests.UserUpdateHierarchyRequest;
import com.everwell.registryservice.models.http.response.HierarchyResponse;
import com.everwell.registryservice.models.http.response.HierarchySummaryResponseData;
import com.everwell.registryservice.models.http.response.Response;
import com.everwell.registryservice.service.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

/**
 * Class Type - Rest Controller
 * Purpose - Provide endpoints and routes for hierarchy management
 * Scope - Modules interacting with Hierarchies through REST APIs
 *
 * @author Ashish Shrivastava
 */
@RestController
public class HierarchyController extends BaseController {

    @Autowired
    private HierarchyService hierarchyService;

    @Autowired
    private ConfigService configService;

    @Autowired
    private AssociationsService associationsService;

    @Autowired
    private DeploymentService deploymentService;

    @Autowired
    private UserService userService;

    /**
     * API to add new hierarchy in DB
     *
     * @param addHierarchyRequest - Details of hierarchy to be added
     * @return Added Hierarchy Details
     */
    @RequestMapping(value = "v1/hierarchy", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response<HierarchyResponse>> addNewHierarchy(@Valid @RequestBody AddHierarchyRequest addHierarchyRequest) {
        // Call Associations Service to validate associations passed in request,
        // internally throw validation error if input association does not exist in master table
        associationsService.validateInputAssociations(addHierarchyRequest.getAssociations());
        HierarchyRequestData inputHierarchy = addHierarchyRequest.getHierarchy();
        // For a level 1 hierarchy the deployment id will be the id in deployment table corresponding to input code
        Long deploymentId = null;
        if (inputHierarchy.getLevel() == 1L) {
            deploymentId = deploymentService.getByCode(inputHierarchy.getCode()).getId();
        }
        // Call service to perform Add hierarchy operation
        HierarchyResponseData newHierarchy = hierarchyService.addNewHierarchy(inputHierarchy, getClientId(), deploymentId);
        // Prepare Hierarchy Response
        HierarchyResponse hierarchyResponse = new HierarchyResponse();
        // Map associations if present in Add Hierarchy Request and add to response
        if (!CollectionUtils.isEmpty(addHierarchyRequest.getAssociations())) {
            hierarchyResponse.setAssociations(associationsService.addHierarchyAssociations(newHierarchy.getId(), addHierarchyRequest.getAssociations()));
        }
        // Map Configs if present in Add Hierarchy Request
        if (!CollectionUtils.isEmpty(addHierarchyRequest.getConfigs())) {
            configService.mapConfigsWithHierarchy(newHierarchy.getId(), addHierarchyRequest.getConfigs());
        }
        // Add Hierarchy Data to response
        hierarchyResponse.setHierarchy(newHierarchy);
        // Send Response to Subscriber
        return new ResponseEntity<>(new Response<>(true, hierarchyResponse), HttpStatus.CREATED);
    }

    /**
     * API to fetch Hierarchy details from DB by Hierarchy ID
     *
     * @param hierarchyId         - Hierarchy ID to be searched
     * @param includeAssociations - If hierarchy associations are also to be fetched
     * @return Hierarchy Data along with Configs and Associations as requested
     */
    @RequestMapping(value = "v1/hierarchy/{hierarchyId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response<HierarchyResponse>> getHierarchyDetails(@PathVariable Long hierarchyId, @RequestParam(required = false) boolean includeAssociations) {
        // Fetch Hierarchy Details from DB
        HierarchyResponseData hierarchy = hierarchyService.fetchHierarchyById(hierarchyId, getClientId());
        // Prepare Hierarchy Response
        HierarchyResponse hierarchyResponse = new HierarchyResponse();
        // Add Hierarchy Data to response
        hierarchyResponse.setHierarchy(hierarchy);
        // Add Hierarchy Associations to response if requested
        if (includeAssociations) {
            hierarchyResponse.setAssociations(associationsService.fetchHierarchyAssociations(hierarchy.getId()));
        }
        // Send Response to Subscriber
        return new ResponseEntity<>(new Response<>(true, hierarchyResponse), HttpStatus.OK);
    }

    /**
     * API to fetch Hierarchy details from DB by Hierarchy ID including the ancestors of Hierarchy
     *
     * @param hierarchyId         - Hierarchy ID to be searched
     * @param includeAssociations - If hierarchy associations are also to be fetched
     * @return Hierarchy Data along with Configs and Associations as requested
     */
    @RequestMapping(value = "v1/hierarchy/with-ancestors/{hierarchyId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response<List<HierarchyResponse>>> getHierarchyDetailsWithAncestors(@PathVariable Long hierarchyId, @RequestParam(required = false) boolean includeAssociations) {
        // Fetch Hierarchy Details from DB with its ancestors
        List<HierarchyResponse> hierarchies = hierarchyService.fetchHierarchyByIdWithAncestors(hierarchyId, getClientId());
        // Check if associations data is requested by user
        if (includeAssociations) {
            // Call associations service to populate associations for each hierarchy
            associationsService.populateAssociationsForHierarchies(hierarchies);
        }
        // Send Response to Subscriber
        return new ResponseEntity<>(new Response<>(true, hierarchies), HttpStatus.OK);
    }

    /**
     * API to fetch Hierarchy details from DB by Hierarchy ID including the immediate descendants of Hierarchy
     *
     * @param hierarchyId         - Hierarchy ID to be searched
     * @param includeAssociations - If hierarchy associations are also to be fetched
     * @return Hierarchy Data along with Configs and Associations as requested
     */
    @RequestMapping(value = "v1/hierarchy/with-descendants/{hierarchyId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response<List<HierarchyResponse>>> getHierarchyDetailsWithDescendants(@PathVariable Long hierarchyId, @RequestParam(required = false) boolean includeAssociations, @RequestParam(required = false) boolean fetchFullTree) {
        // Fetch Hierarchy Details from DB with its ancestors
        List<HierarchyResponse> hierarchies = hierarchyService.fetchHierarchyByIdWithDescendants(hierarchyId, fetchFullTree, getClientId());
        // Check if associations data is requested by user
        if (includeAssociations) {
            // Call associations service to populate associations for each hierarchy
            associationsService.populateAssociationsForHierarchies(hierarchies);
        }
        // Send Response to Subscriber
        return new ResponseEntity<>(new Response<>(true, hierarchies), HttpStatus.OK);
    }

    /**
     * API to update details of an existing hierarchy
     *
     * @param updateHierarchyRequest - Details of Hierarchy to be updated
     * @return Updated Hierarchy Details
     */
    @RequestMapping(value = "v1/hierarchy", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response<HierarchyResponse>> updateHierarchyDetails(@Valid @RequestBody UpdateHierarchyRequest updateHierarchyRequest) {
        // Call Associations Service to validate associations passed in request,
        // internally throw validation error if input association does not exist in master table
        associationsService.validateInputAssociations(updateHierarchyRequest.getAssociations());
        // Call Hierarchy service to perform Update hierarchy operation
        HierarchyResponseData updatedHierarchy = hierarchyService.updateHierarchy(updateHierarchyRequest.getHierarchy(), getClientId());
        // Prepare Hierarchy Response
        HierarchyResponse hierarchyResponse = new HierarchyResponse();
        // Map associations if present in Update Hierarchy Request
        if (!CollectionUtils.isEmpty(updateHierarchyRequest.getAssociations())) {
            hierarchyResponse.setAssociations(associationsService.updateHierarchyAssociations(updatedHierarchy.getId(), updateHierarchyRequest.getAssociations()));
        }
        // Add Hierarchy Data to response
        hierarchyResponse.setHierarchy(updatedHierarchy);
        // If hierarchyResponse does not have associations, then make explicit call to fetch associations
        if (null == hierarchyResponse.getAssociations()) {
            // Add Hierarchy Associations to response
            hierarchyResponse.setAssociations(associationsService.fetchHierarchyAssociations(updatedHierarchy.getId()));
        }
        // Send Response to Subscriber
        return new ResponseEntity<>(new Response<>(true, hierarchyResponse), HttpStatus.OK);
    }

    /**
     * API to delete a particular Hierarchy (soft delete only i.e. making active flag false)
     *
     * @param hierarchyId - Hierarchy to be deleted
     * @return Whether deletion is successful
     */
    @RequestMapping(value = "v1/hierarchy/{hierarchyId}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response<String>> deleteHierarchy(@PathVariable Long hierarchyId) {
        Boolean hierarchyDeleted = hierarchyService.deleteHierarchy(hierarchyId, getClientId());
        ResponseEntity<Response<String>> response;
        if (hierarchyDeleted) {
            response = new ResponseEntity<>(new Response<>(true, null, "Deletion of hierarchy " + hierarchyId + " Successful"), HttpStatus.OK);
        } else {
            response = new ResponseEntity<>(new Response<>("Deletion of hierarchy " + hierarchyId + " Failed"), HttpStatus.OK);
        }
        return response;
    }


    @Operation(summary = "Get all hierarchies and its descendants visible to user",
            responses = {
                    @ApiResponse(description = "a list of hierarchy response objects mapped based on user mapping",
                            content = @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = List.class))),
                    @ApiResponse(responseCode = "404", description = "The user does not exist", content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
                    @ApiResponse(responseCode = "401", description = "User session not found", content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
                    @ApiResponse(responseCode = "404", description = "The Hierarchy does not exist", content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class)))})
    @RequestMapping(value = "v1/hierarchies/user", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response<List<MinHierarchyDetails>>> getHierarchies() {
        Pair<Client, UserAccess> user = userService.getUserClientByToken();
        UserAccess definedUser = user.getSecond();
        List<HierarchyResponse> hierarchyResponseList = hierarchyService.fetchHierarchyByIdWithDescendants(definedUser.getHierarchyId(), true, user.getFirst().getId());
        List<MinHierarchyDetails> minHierarchyDetailsList = HierarchyMapper.INSTANCE.hierarchyResponseListToMinHierarchyDetailsList(hierarchyResponseList);
        return new ResponseEntity<>(new Response<>(true, minHierarchyDetailsList), HttpStatus.OK);
    }


    @Operation(summary = "Get the user and hierarchy summary response",
            responses = {
                    @ApiResponse(description = "hierarchy details along with list of user data mapped to the hierarchy",
                            content = @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = Response.class))),
                    @ApiResponse(responseCode = "404", description = "The user does not exist", content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
                    @ApiResponse(responseCode = "401", description = "User session not found", content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
                    @ApiResponse(responseCode = "404", description = "The Hierarchy does not exist", content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class)))})
    @RequestMapping(value = "v1/hierarchy/summary/user", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response<UserHierarchySummaryResponse>> getHierarchySummary(Long hierarchyId) {
        Pair<Client, UserAccess> user = userService.getUserClientByToken();
        UserAccess definedUser = user.getSecond();
        Client client = user.getFirst();
        boolean hasHierarchyAccess = hierarchyService.validateIsDescendant(definedUser.getHierarchyId(), client.getId(), hierarchyId);
        if(!hasHierarchyAccess) {
            throw new UnauthorizedException(ValidationStatusEnum.UNAUTHORIZED_HIERARCHY_ACCESS);
        }
        HierarchySummaryResponseData hierarchySummaryResponseData = hierarchyService.fetchHierarchySummaryById(hierarchyId, user.getFirst().getId());
        List<UserSummaryDto> summaryDtoList = userService.getUserSummaryResponseByHierarchyId(hierarchyId);

        UserHierarchySummaryResponse summaryResponse = new UserHierarchySummaryResponse(hierarchySummaryResponseData, summaryDtoList);
        return new ResponseEntity<>(new Response<>(true, summaryResponse), HttpStatus.OK);
    }

    @Operation(summary = "Update the hierarchy details as acted upon by the user",
            responses = {
                    @ApiResponse(description = "the updated hierarchy summary response",
                            content = @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = Response.class))),
                    @ApiResponse(responseCode = "404", description = "The user does not exist", content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
                    @ApiResponse(responseCode = "401", description = "User session not found", content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
                    @ApiResponse(responseCode = "404", description = "The Hierarchy does not exist", content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class)))})
    @RequestMapping(value = "v1/hierarchy/user", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response<HierarchySummaryResponseData>> updateHierarchyDetails(@RequestBody @Valid UserUpdateHierarchyRequest updateHierarchyRequest) {
        Pair<Client, UserAccess> user = userService.getUserClientByToken();
        UserAccess definedUser = user.getSecond();
        Client client = user.getFirst();
        boolean hasHierarchyAccess = hierarchyService.validateIsDescendant(definedUser.getHierarchyId(), client.getId(), updateHierarchyRequest.getHierarchyId());
        if(!hasHierarchyAccess) {
            throw new UnauthorizedException(ValidationStatusEnum.UNAUTHORIZED_HIERARCHY_ACCESS);
        }
        HierarchySummaryResponseData hierarchyResponseData = hierarchyService.userUpdateHierarchy(updateHierarchyRequest, client.getId());
        return new ResponseEntity<>(new Response<>(true, hierarchyResponseData), HttpStatus.OK);
    }


}
