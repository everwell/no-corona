package com.everwell.registryservice.controller;

import com.everwell.registryservice.constants.ValidationStatusEnum;
import com.everwell.registryservice.models.dto.EngagementDetails;
import com.everwell.registryservice.models.http.requests.AddEngagementRequest;
import com.everwell.registryservice.models.http.requests.UpdateLanguageRequest;
import com.everwell.registryservice.models.http.response.LanguagesResponse;
import com.everwell.registryservice.models.http.response.Response;
import com.everwell.registryservice.service.EngagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@RestController
public class EngagementController {

    @Autowired
    EngagementService engagementService;

    /**
     * API to add new hierarchy in DB
     *
     * @param hierarchyId - id for which engagement is expected
     * @return engagement
     */
    @RequestMapping(value = "/v1/engagement", method = RequestMethod.GET)
    public ResponseEntity<Response<EngagementDetails>> getEngagementByHierarchyId(@RequestParam Long hierarchyId) {
        EngagementDetails response = engagementService.getEngagementByHierarchy(hierarchyId, true);
        if (null == response)
            return new ResponseEntity<>(new Response<>(false, null, ValidationStatusEnum.HIERARCHY_ENGAGEMENT_NOT_FOUND.getMessage()), HttpStatus.NOT_FOUND);
        return ResponseEntity.ok(new Response<>(true, response));
    }

    /**
     * API to add new hierarchy in DB
     *
     * @param addEngagementRequest - engagement details to be added
     * @return failed list of engagements
     */
    @RequestMapping(value = "/v1/engagement", method = RequestMethod.POST)
    public ResponseEntity<Response<List<EngagementDetails>>> addEngagement(@Valid @RequestBody AddEngagementRequest addEngagementRequest) {
        List<EngagementDetails> failedList = engagementService.addEngagement(addEngagementRequest.getEngagementDetails());
        return new ResponseEntity<>(new Response<>(true, failedList), HttpStatus.CREATED);
    }

    @RequestMapping(value = "/v1/engagement/languages", method = RequestMethod.PUT)
    public ResponseEntity<Response<String>> updateLanguages(@Valid @RequestBody UpdateLanguageRequest updateLanguageRequest){
        Boolean updateLanguageSuccess = engagementService.updateLanguages(updateLanguageRequest);
        if(updateLanguageSuccess) {
            return new ResponseEntity<>(new Response<>(true, null), HttpStatus.OK);
        }
        return new ResponseEntity<>(new Response<>(false, null,"Something went wrong"), HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/v1/engagement/languages", method = RequestMethod.GET)
    public ResponseEntity<Response<LanguagesResponse>> getLanguages(@RequestParam Long hierarchyId){
        LanguagesResponse response = engagementService.getLanguages(hierarchyId);
        if(null == response)
            return new ResponseEntity<>(new Response<>(false, null, "Something went wrong"), HttpStatus.NOT_FOUND);
        return ResponseEntity.ok(new Response<>(true, response));
    }
}
