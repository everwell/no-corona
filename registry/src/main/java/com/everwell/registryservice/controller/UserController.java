package com.everwell.registryservice.controller;

import com.everwell.registryservice.constants.Constants;
import com.everwell.registryservice.constants.ValidationStatusEnum;
import com.everwell.registryservice.exceptions.ConflictException;
import com.everwell.registryservice.exceptions.NotFoundException;
import com.everwell.registryservice.exceptions.UnauthorizedException;
import com.everwell.registryservice.mappers.UserMapper;
import com.everwell.registryservice.models.db.*;
import com.everwell.registryservice.models.dto.HierarchyResponseData;
import com.everwell.registryservice.models.dto.SidebarItemResponse;
import com.everwell.registryservice.models.dto.UnifiedListFilterResponse;
import com.everwell.registryservice.models.http.requests.CreateUserRequest;
import com.everwell.registryservice.models.http.requests.RedirectionRequest;
import com.everwell.registryservice.models.http.requests.sso.RegistrationRequest;
import com.everwell.registryservice.models.http.response.*;
import com.everwell.registryservice.models.http.response.adherence.AdherenceTechnologyDto;
import com.everwell.registryservice.models.http.response.sso.UserSSOResponse;
import com.everwell.registryservice.service.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Class Type - Rest Controller
 * Purpose - Provide endpoints and routes for user management
 * Scope - Modules interacting with user management through REST APIs
 *
 * @author Ashish Shrivastava
 */
@RestController
public class UserController extends BaseController {

    @Autowired
    private UserService userService;

    @Autowired
    private ClientService clientService;

    @Autowired
    private SSOService ssoService;

    @Autowired
    private HierarchyService hierarchyService;

    @Autowired
    private DeploymentService deploymentService;

    @Autowired
    private AssociationsService associationsService;

    @Autowired
    private LanguageService languageService;

    @Autowired
    private AdherenceService adherenceService;

    @Autowired
    private TaskListService taskListService;

    /**
     * API to add a new user in DB
     *
     * @param createUserRequest - Data of new user to be created
     * @return Details of newly created user
     */
    @RequestMapping(value = "v1/users", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response<UserDetailsResponse>> addNewUser(@Valid @RequestBody CreateUserRequest createUserRequest) {
        //Validate if sso creation is to be done internally
        createUserRequest.validateSsoCreation();
        Long clientId = getClientId();
        if (createUserRequest.isSsoUserToBeCreated()) {
            UserSSOResponse ssoResponse = ssoService.registration(clientId.toString(), new RegistrationRequest(createUserRequest.getUsername(), createUserRequest.getPassword()));
            createUserRequest.setSsoId(ssoResponse.getUserId());
        }
        // Validate if hierarchy to be mapped exists, internal validation to check if hierarchy is present
        HierarchyResponseData mappedHierarchy = hierarchyService.fetchHierarchyById(createUserRequest.getHierarchyId(), clientId);
        // Call User Service to validate request data and create new user login accordingly
        UserAccess newUser = userService.addNewUser(createUserRequest, clientId);
        // If hierarchies found, fetch their associations and prepare response list
        Map<String, String> mappedHierarchyAssociations = associationsService.fetchHierarchyAssociations(mappedHierarchy.getId());
        // Prepare Mapped Hierarchy Data
        HierarchyResponse mappedHierarchyDetails = new HierarchyResponse(mappedHierarchy, mappedHierarchyAssociations);
        // Prepare List of Mapped Hierarchies for Response
        List<HierarchyResponse> associatedHierarchiesDetails = new ArrayList<>(1);
        // Add Mapped Hierarchy Details to List
        associatedHierarchiesDetails.add(mappedHierarchyDetails);
        // Prepare user response object
        UserDetailsResponse userDetailsResponse = new UserDetailsResponse(UserMapper.INSTANCE.userAccessToUserDetails(newUser), associatedHierarchiesDetails);
        // Return Response
        return new ResponseEntity<>(new Response<>(true, userDetailsResponse), HttpStatus.CREATED);
    }

    /**
     * API to fetch the details of a user from DB
     *
     * @param ssoId             - SSO ID to be searched
     * @param includeAllDetails - Whether all user related details like mapped hierarchies and associations to be retrieved
     * @return Required user details
     */
    @RequestMapping(value = "v1/users/{ssoId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response<UserDetailsResponse>> getUserDetails(@PathVariable Long ssoId, @RequestParam(required = false) boolean includeAllDetails) {
        // Fetch user data from DB
        UserAccess user = userService.fetchUserData(ssoId);
        UserDetailsResponse userDetailsResponse = new UserDetailsResponse();
        userDetailsResponse.setUser(UserMapper.INSTANCE.userAccessToUserDetails(user));
        if (null != user && includeAllDetails) {
            // Fetch mapped Hierarchy data for the user
            List<Long> associatedHierarchyIds = userService.fetchHierarchiesAssociatedWithUser(user.getId());
            if (!CollectionUtils.isEmpty(associatedHierarchyIds)) {
                // Fetch All Active Hierarchies by their IDs
                List<HierarchyResponse> associatedHierarchies = hierarchyService.fetchAllHierarchies(associatedHierarchyIds, getClientId());
                if (!CollectionUtils.isEmpty(associatedHierarchies)) {
                    // If hierarchies found, fetch their associations
                    associationsService.populateAssociationsForHierarchies(associatedHierarchies);
                    // Add Hierarchies data to response
                    userDetailsResponse.setHierarchy(associatedHierarchies);
                }
            }
        }
        // Return Response
        return new ResponseEntity<>(new Response<>(true, userDetailsResponse), HttpStatus.OK);
    }

    /**
     * API to delete a user from DB
     *
     * @param ssoId - SSO ID to be removed from DB
     * @return Whether deletion of user is successful
     */
    @RequestMapping(value = "v1/users/{ssoId}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response<String>> deleteUser(@PathVariable Long ssoId) {
        boolean userDeleted = userService.deleteUser(ssoId);
        ResponseEntity<Response<String>> response;
        if (userDeleted) {
            response = new ResponseEntity<>(new Response<>(true, null, "Deletion of user " + ssoId + " Successful"), HttpStatus.OK);
        } else {
            response = new ResponseEntity<>(new Response<>("Deletion of user " + ssoId + " Failed"), HttpStatus.OK);
        }
        return response;
    }

    @Operation(summary = "Get user authentication status",
            responses = {
                    @ApiResponse(description = "response of user authenticated status",
                            content = @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = UserAuthStatusResponse.class)))})
    @RequestMapping(value = "v1/user/auth-status", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response<UserAuthStatusResponse>> validateUserAuthentication() {
        Pair<Client, UserAccess> user = null;
        UserAuthStatusResponse userAuthResponse = new UserAuthStatusResponse();
        try {
            // Fetch client + user data from DB
            user = userService.getUserClientByToken();
            if (null != user) {
                String username = user.getSecond().getUsername();
                userAuthResponse.setUserName(username);
                userAuthResponse.setUserAuthenticated(true);
            }
        } catch (UnauthorizedException | NotFoundException unE) {
            userAuthResponse.setUserAuthenticated(false);
        }
        // Return Response
        return new ResponseEntity<>(new Response<>(true, userAuthResponse), HttpStatus.OK);
    }

    @Operation(summary = "Get header details by user",
            responses = {
                    @ApiResponse(description = "response of header information that is received",
                            content = @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = HeaderResponse.class))),
                    @ApiResponse(responseCode = "404", description = "The user does not exist", content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
                    @ApiResponse(responseCode = "401", description = "User session not found", content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
                    @ApiResponse(responseCode = "404", description = "Deployment code not found", content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
                    @ApiResponse(responseCode = "404", description = "Language not found", content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class)))})
    @RequestMapping(value = "v1/user/header", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response<HeaderResponse>> getHeaderDetails() {
        HeaderResponse headerResponse = new HeaderResponse();
        Pair<Client, UserAccess> user = null;

        String askForHelpUrl = null;
        try {
            // Fetch client + user data from DB
            user = userService.getUserClientByToken();
            if (null != user && null != user.getSecond()) {
                UserAccess definiedUser = user.getSecond();
                //Todo: Define relationship between user, deployment, hierarchy and client
                HierarchyResponseData responseData = hierarchyService.fetchHierarchyById(definiedUser.getHierarchyId(), definiedUser.getClientId());
                Deployment deployment = deploymentService.get(Long.parseLong(responseData.getDeploymentId()));
                if (null != deployment) {
                    askForHelpUrl = deployment.getAskForHelpUrl();
                }
                headerResponse = userService.buildUserHeaderDetails(definiedUser, headerResponse);
            }
        } catch (UnauthorizedException | NotFoundException unE) {
            askForHelpUrl = clientService.getDefaultAskForUrl();
        }

        if (StringUtils.isBlank(askForHelpUrl)) {
            askForHelpUrl = clientService.getDefaultAskForUrl();
        }
        headerResponse.addHeaderLink(Pair.of(Constants.ASK_FOR_HELP, askForHelpUrl));
        headerResponse.addHeaderLink(Pair.of(Constants.FAQS, clientService.getFaqUrl()));
        // Return Response
        return new ResponseEntity<>(new Response<>(true, headerResponse), HttpStatus.OK);
    }

    @Operation(summary = "Post redirect url for login",
            responses = {
                    @ApiResponse(description = "Redirect url",
                            content = @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = RedirectionRequest.class))),
                    @ApiResponse(responseCode = "404", description = "The user does not exist", content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
                    @ApiResponse(responseCode = "401", description = "User session not found", content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class)))})
    @RequestMapping(value = "v1/user/login", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response<RedirectionResponse>> validateLogin(@Valid @RequestBody RedirectionRequest requestBody) {
        RedirectionResponse response = new RedirectionResponse();

        try {
            // Fetch client + user data from token
            Pair<Client, UserAccess> user = userService.getUserClientByToken();
            // If a user is already logged in redirection is not required
            if (null != user) {
                throw new ConflictException(ValidationStatusEnum.USER_ALREADY_LOGGED_IN);
            }
        } catch (UnauthorizedException unE) {
            // As the user is unauthorized check the token based on client to redirect to sso
            response.setRedirectUrl(clientService.getRedirectUrl(requestBody.getReturnUrl()));
        }
        // Return Response
        return new ResponseEntity<>(new Response<>(true, response), HttpStatus.OK);
    }

    @Operation(summary = "Get languages by user",
            responses = {
                    @ApiResponse(description = "List of languages and metadata",
                            content = @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = Map.class))),
                    @ApiResponse(responseCode = "404", description = "The user does not exist", content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
                    @ApiResponse(responseCode = "401", description = "User session not found", content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
                    @ApiResponse(responseCode = "404", description = "Deployment code not found", content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
                    @ApiResponse(responseCode = "404", description = "Language not found", content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class)))})
    @RequestMapping(value = "v1/user/languages", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response<Map<String, String>>> getLanguages() {
        Pair<Client, UserAccess> user = userService.getUserClientByToken();
        UserAccess definedUser = user.getSecond();
        HierarchyResponseData responseData = hierarchyService.fetchHierarchyById(definedUser.getHierarchyId(), user.getFirst().getId());
        Deployment deployment = deploymentService.get(Long.parseLong(responseData.getDeploymentId()));

        // Todo: define relationship between deployment and language service to abstract implementation details
        List<Language> languages = languageService.getAll();
        Map<Long, Language> languageByIdMap = languages.stream().collect(Collectors.toMap(Language::getId, language -> language));

        List<DeploymentLanguageMap> languageMaps = this.languageService.getMappings(deployment.getId());
        Map<String, String> allowedLanguageMap = new HashMap<>();
        if (!CollectionUtils.isEmpty(languageMaps)) {
            for (DeploymentLanguageMap languageMap : languageMaps) {
                if (null != languageByIdMap.get(languageMap.getLanguageId())) {
                    Language lang = languageByIdMap.get(languageMap.getLanguageId());
                    allowedLanguageMap.put(lang.getTranslationKey(), lang.getDisplayName());
                }
            }
        }
        return new ResponseEntity<>(new Response<>(true, allowedLanguageMap), HttpStatus.OK);
    }

    /**
     * API to fetch all sidebar options mapped for the user
     *
     * @return a list of sidebar item objects mapped based on the permission module
     */
    @RequestMapping(value = "v1/user/sidebar-items", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response<SidebarResponse>> getSidebarItems() {
        Pair<Client, UserAccess> user = userService.getUserClientByToken();
        UserAccess definedUser = user.getSecond();
        HierarchyResponseData responseData = hierarchyService.fetchHierarchyById(definedUser.getHierarchyId(), user.getFirst().getId());
        Deployment deployment = deploymentService.get(Long.parseLong(responseData.getDeploymentId()));
        List<SidebarItemResponse> sidebarItems = userService.getSidebarItems(definedUser, deployment);

        SidebarResponse sidebarResponse = new SidebarResponse(sidebarItems);
        return new ResponseEntity<>(new Response<>(true, sidebarResponse), HttpStatus.OK);
    }

    @Operation(summary = "Get the filter options for the logged in user",
            responses = {
                    @ApiResponse(description = "response of the filter options",
                            content = @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = Map.class))),
                    @ApiResponse(responseCode = "404", description = "The user does not exist", content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
                    @ApiResponse(responseCode = "401", description = "User session not found", content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
                    @ApiResponse(responseCode = "404", description = "The Hierarchy does not exist", content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class)))})
    @RequestMapping(value = "v1/user/upp-filters", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response<UnifiedListFilterResponse>> getUPPFilterOptions() {
        Pair<Client, UserAccess> user = userService.getUserClientByToken();
        UserAccess definedUser = user.getSecond();
        Client client = user.getFirst();
        List<AdherenceTechnologyDto> allAdherenceTechnologies = adherenceService.getAllTechnologyOptions();
        UnifiedListFilterResponse summaryResponse = userService.getUppFilterOptions(definedUser, allAdherenceTechnologies);
        summaryResponse = hierarchyService.addHierarchyOptionsToUppFilter(definedUser.getHierarchyId(), client.getId(), summaryResponse);
        return new ResponseEntity<>(new Response<>(true, summaryResponse), HttpStatus.OK);
    }


    @Operation(summary = "Get the tasklist details for the logged in user",
            responses = {
                    @ApiResponse(description = "response of list of items containing the tasklist description, icons and extra data details",
                            content = @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = List.class))),
                    @ApiResponse(responseCode = "404", description = "The user does not exist", content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
                    @ApiResponse(responseCode = "401", description = "User session not found", content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
                    @ApiResponse(responseCode = "404", description = "The Hierarchy does not exist", content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class)))})
    @RequestMapping(value = "v1/user/tasklist/details", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response<List<TaskListItemResponse>>> getTaskListDetails() {
        Pair<Client, UserAccess> user = userService.getUserClientByToken();
        UserAccess definedUser = user.getSecond();
        HierarchyResponseData responseData = hierarchyService.fetchHierarchyById(definedUser.getHierarchyId(), user.getFirst().getId());
        Deployment deployment = deploymentService.get(Long.parseLong(responseData.getDeploymentId()));

        List<TaskListItemResponse> taskListItemResponse = taskListService.getTaskListFilterResponseByDeploymentId(deployment.getId(), false);
        return new ResponseEntity<>(new Response<>(true, taskListItemResponse), HttpStatus.OK);
    }

    @Operation(summary = "Logout user session",
            responses = {
                    @ApiResponse(description = "Redirection Url",
                            content = @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = RedirectionResponse.class)))})
    @RequestMapping(value = "v1/user/logout", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response<RedirectionResponse>> logoutUser(@Valid @RequestBody RedirectionRequest request) {
        RedirectionResponse response = new RedirectionResponse();
        try {
            // Fetch client + user data from token
            Pair<Client, UserAccess> user = userService.getUserClientByToken();
            // If a user is logged in redirection to sso for logout is required
            if (null != user) {
                response.setRedirectUrl(clientService.getLogoutUrl(request.getReturnUrl()));
            }
        } catch (UnauthorizedException | NotFoundException e) {
            // As the user is not logged in system can directly redirect to user provided redirection Url
            response.setRedirectUrl(request.getReturnUrl());
        }
        // Return Response
        return new ResponseEntity<>(new Response<>(true, response), HttpStatus.OK);
    }
}
