package com.everwell.registryservice.service.impl;

import com.everwell.registryservice.constants.ValidationStatusEnum;
import com.everwell.registryservice.exceptions.ValidationException;
import com.everwell.registryservice.models.db.VendorMapping;
import com.everwell.registryservice.repository.VendorRepository;
import com.everwell.registryservice.service.VendorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VendorServiceImpl implements VendorService {

    @Autowired
    VendorRepository vendorRepository;

    @Override
    public long getVendorIdByDeploymentIdAndTriggerId(long deploymentId, long triggerId)
    {
        VendorMapping vendorMapping = vendorRepository.findFirstByDeploymentIdAndTriggerRegistryId(deploymentId, triggerId);
        if (null == vendorMapping)
            throw new ValidationException(ValidationStatusEnum.TRIGGER_VENDOR_MAPPING_NOT_FOUND);

        return vendorMapping.getVendorId();
    }
}
