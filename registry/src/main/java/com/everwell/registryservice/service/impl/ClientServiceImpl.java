package com.everwell.registryservice.service.impl;

import com.everwell.registryservice.exceptions.NotFoundException;
import com.everwell.registryservice.exceptions.UnauthorizedException;
import com.everwell.registryservice.exceptions.ValidationException;
import com.everwell.registryservice.models.db.Client;
import com.everwell.registryservice.models.http.requests.RegisterClientRequest;
import com.everwell.registryservice.models.http.response.ClientResponse;
import com.everwell.registryservice.repository.ClientRepository;
import com.everwell.registryservice.service.ClientService;
import com.everwell.registryservice.utils.JwtUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class ClientServiceImpl implements ClientService {

    private static Logger LOGGER = LoggerFactory.getLogger(ClientServiceImpl.class);

    @Autowired
    ClientRepository clientRepository;

    @Autowired
    JwtUtils jwtUtils;

    @Value("${client.ask-for-help-url.default}")
    private String defaultAskForHelpUrl;

    @Value("${client.faq-url.default:https://everwellhub.atlassian.net/wiki/spaces/EH/pages/292192308/Frequently+Asked+Questions+-+test+page}")
    private String faqUrl;

    @Value("${default.client.id}")
    private Long defaultClientId;

    @Override
    public ClientResponse registerClient(RegisterClientRequest clientRequest) {
        Client existingClient = clientRepository.findByName(clientRequest.getName());
        if (null != existingClient) {
            LOGGER.error("[registerClient] User already exists");
            throw new ValidationException("User already exists");
        }
        String encodedPassword = new BCryptPasswordEncoder().encode(clientRequest.getPassword());
        Client newClient = new Client(clientRequest.getName(), encodedPassword, clientRequest.getEventFlowId(), clientRequest.getAskForHelpUrl());
        clientRepository.save(newClient);
        return new ClientResponse(newClient);
    }

    @Override
    public ClientResponse getClientWithToken(Long id) {
        Client client = getClient(id);
        String jwtToken = jwtUtils.generateToken(String.valueOf(id));
        return new ClientResponse(client, jwtToken, jwtUtils.getExpirationDateFromToken(jwtToken).getTime(), client.getAskforhelpurl());
    }

    @Override
    public Client getClient(Long id) {
        if (null == id) {
            LOGGER.error("[getClient] id cannot be null");
            throw new ValidationException("id cannot be null");
        }
        Client client = clientRepository.findById(id).orElse(null);
        if (null == client) {
            LOGGER.error("[getClient] No client found with id:" + id);
            throw new NotFoundException("No client found with id:" + id);
        }
        return client;
    }

    @Override
    public Client getClientByToken() {
        try {
            return (Client) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        } catch (Exception e) {
            throw new UnauthorizedException("Client not found");
        }
    }

    @Override
    public Client getClientByTokenOrDefault() {
        try {
            return (Client) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        } catch (Exception e) {
            return clientRepository.findById(defaultClientId).orElse(null);
        }
    }

    @Override
    public String getRedirectUrl(String returnUrl) {
        Client client = getClientByToken();
        return client.getSsoUrl() + "/v1/sso/login?returnUrl=" + returnUrl + "&clientId=" + client.getId();
    }

    @Override
    public String getDefaultAskForUrl() {
        return getDefaultAskForHelpUrl(getClientByToken());
    }

    private String getDefaultAskForHelpUrl(Client client) {
        return (null != client && StringUtils.isNotBlank(client.getAskforhelpurl())) ? client.getAskforhelpurl() : defaultAskForHelpUrl;
    }

    @Override
    public String getFaqUrl() {
        return faqUrl;
    }

    @Override
    public Client getClientByCookieName(String cookieName) {
        if (null == cookieName) {
            LOGGER.error("[getClientByCookieName] cookie name cannot be null");
            throw new ValidationException("cookie name cannot be null");
        }
        Client client = clientRepository.findBySsoAuthorizationCookieName(cookieName).orElse(null);
        return client;
    }

    /**
     * API to get sso logout URL for redirection
     *
     * @param returnUrl - Return URL from redirection from SSO after logout
     * @return - SSO logout redirection URL with appropriate query params
     */
    @Override
    public String getLogoutUrl(String returnUrl) {
        Client client = getClientByToken();
        return client.getSsoUrl() + "/v1/sso/logout?returnUrl=" + returnUrl + "&clientId=" + client.getId();
    }
}
