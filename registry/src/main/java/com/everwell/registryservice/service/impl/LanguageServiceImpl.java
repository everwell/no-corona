package com.everwell.registryservice.service.impl;

import com.everwell.registryservice.exceptions.NotFoundException;
import com.everwell.registryservice.models.db.DeploymentLanguageMap;
import com.everwell.registryservice.models.db.Language;
import com.everwell.registryservice.repository.DeploymentLanguageRepository;
import com.everwell.registryservice.repository.LanguageRepository;
import com.everwell.registryservice.service.LanguageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class LanguageServiceImpl implements LanguageService
{
    private static Logger LOGGER = LoggerFactory.getLogger(LanguageServiceImpl.class);

    @Autowired
    private LanguageRepository languageRepository;

    @Autowired
    private DeploymentLanguageRepository deploymentLanguageRepository;

    public List<Language> getAll()
    {
        return this.languageRepository.findAll();
    }

    @Override
    public Language get(Long id)
    {
        Optional<Language> language = this.languageRepository.findById(id);

        if(language.isEmpty())
        {
            throw new NotFoundException("Language not found");
        }

        return language.get();
    }

    @Override
    public List<DeploymentLanguageMap> getMappings(Long deploymentId)
    {
        return this.deploymentLanguageRepository.getAllByDeploymentIdAndStoppedAtIsNull(deploymentId);
    }
}
