package com.everwell.registryservice.service;

import com.everwell.registryservice.models.db.DeploymentLanguageMap;
import com.everwell.registryservice.models.db.Language;

import java.util.List;

public interface LanguageService
{
    List<Language> getAll();

    Language get(Long id);

    List<DeploymentLanguageMap> getMappings(Long deploymentId);
}
