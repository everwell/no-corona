package com.everwell.registryservice.service;

import com.everwell.registryservice.models.dto.*;
import com.everwell.registryservice.models.http.requests.UserUpdateHierarchyRequest;
import com.everwell.registryservice.models.http.response.HierarchyResponse;
import com.everwell.registryservice.models.http.response.HierarchySummaryResponseData;

import java.util.List;

/**
 * Class Type - Service Interface
 * Purpose - Provide business logic for hierarchy management
 * Scope - Modules interacting with hierarchies
 *
 * @author Ashish Shrivastava
 */
public interface HierarchyService {

    /**
     * API to add new Hierarchy, and it's related mappings in DB
     *
     * @param hierarchyRequestData - Details of Hierarchy to be added
     * @param clientId             - Client ID fetched from request
     * @param deploymentId         - Deployment ID corresponding to input code for level 1 hierarchy
     * @return Added Hierarchy Details
     */
    HierarchyResponseData addNewHierarchy(HierarchyRequestData hierarchyRequestData, Long clientId, Long deploymentId);

    /**
     * API to fetch Hierarchy details by Hierarchy ID
     *
     * @param hierarchyId - Hierarchy ID to be searched
     * @param clientId    - Client Id fetched from request
     * @return Hierarchy Details
     */
    HierarchyResponseData fetchHierarchyById(Long hierarchyId, Long clientId);

    /**
     * API to fetch Hierarchy details by Hierarchy ID along with details of its ancestors
     *
     * @param hierarchyId - Hierarchy ID to be searched
     * @param clientId    - Client Id fetched from request
     * @return Hierarchy and its ancestors
     */
    List<HierarchyResponse> fetchHierarchyByIdWithAncestors(Long hierarchyId, Long clientId);

    /**
     * API to fetch Hierarchy details by Hierarchy ID along with details of its descendants
     *
     * @param hierarchyId   - Hierarchy ID to be searched
     * @param fetchFullTree - If full tree of hierarchies to be fetched with input hierarchyId as root
     * @param clientId      - Client Id fetched from request
     * @return Hierarchy and it's descendants Details
     */
    List<HierarchyResponse> fetchHierarchyByIdWithDescendants(Long hierarchyId, boolean fetchFullTree, Long clientId);

    /**
     * API to update existing Hierarchy, and it's associated configs and mappings in DB
     *
     * @param hierarchyUpdateData - Hierarchy details to be updated
     * @param clientId            - Client Id fetched from request
     * @return Updated Hierarchy details
     */
    HierarchyResponseData updateHierarchy(HierarchyUpdateData hierarchyUpdateData, Long clientId);

    /**
     * API to delete a particular Hierarchy from DB
     *
     * @param hierarchyId - Hierarchy to be deleted
     * @param clientId    - Client Id fetched from request
     * @return if delete hierarchy operation is successful
     */
    Boolean deleteHierarchy(Long hierarchyId, Long clientId);

    /**
     * API to fetch all hierarchies by provided hierarchyId list
     *
     * @param hierarchyIdList - Input hierarchy id list
     * @param clientId        - Client Id fetched from request
     * @return List of hierarchies
     */
    List<HierarchyResponse> fetchAllHierarchies(List<Long> hierarchyIdList, Long clientId);

    /**
     * API to fetch the lineage of all hierarchies by provided hierarchyId list
     *
     * @param hierarchyIdList - Input hierarchy id list
     * @param clientId        - Client Id fetched from request
     * @return List of hierarchies lineage data
     */
    List<HierarchyLineage> fetchAllHierarchiesLineages(List<Long> hierarchyIdList, Long clientId);

    /**
     * API to fetch Hierarchy Summary by Hierarchy ID
     *
     * @param hierarchyId - Hierarchy ID to be searched
     * @param clientId    - Client Id fetched from request
     * @return Hierarchy Summary Information including cases within
     */
    HierarchySummaryResponseData fetchHierarchySummaryById(Long hierarchyId, Long clientId);

    /**
     * API to validate if the hierarchyId2 passed is within the jurisdiction of hierarchyId1
     *
     * @param hierarchyId1 - Hierarchy Id under which the other should contain
     * @param clientId - Client Id under which the hierarchy would be contained
     * @param hierarchyId2 - Hierarchy Id which is to be contained
     * @return Boolean response of whether it is contained.
     */
    boolean validateIsDescendant(Long hierarchyId1, Long clientId, Long hierarchyId2);

    /**
     * API for logged in user to update existing hierarchy
     *
     * @param hierarchyUpdateData - Hierarchy details to be updated
     * @param clientId            - Client Id fetched from request
     * @return response containing the updated hierarchy details
     */
    HierarchySummaryResponseData userUpdateHierarchy(UserUpdateHierarchyRequest hierarchyUpdateData, Long clientId);

    /**
     * API to add hierarchy options of the hierarchy specified to the upp filter
     *
     * @param hierarchyId - Hierarchy for which details are to be populated
     * @param clientId     - Client Id fetched from request
     * @param filterResponse - Partially built unified filter response into which hierarchy options are to be added
     */
    UnifiedListFilterResponse addHierarchyOptionsToUppFilter(Long hierarchyId, Long clientId, UnifiedListFilterResponse filterResponse);
}
