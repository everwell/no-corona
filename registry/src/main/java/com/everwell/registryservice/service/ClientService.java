package com.everwell.registryservice.service;

import com.everwell.registryservice.models.db.Client;
import com.everwell.registryservice.models.http.requests.RegisterClientRequest;
import com.everwell.registryservice.models.http.response.ClientResponse;
import org.springframework.stereotype.Service;

@Service
public interface ClientService {

    ClientResponse registerClient(RegisterClientRequest clientRequest);

    ClientResponse getClientWithToken(Long id);

    Client getClient(Long id);

    Client getClientByToken();

    Client getClientByTokenOrDefault();

    /**
     * Function to get the configured sso url for redirection
     *
     * @param returnUrl for sso to redirect after logging in / resetting password
     * @return A string with the redirect url
     */
    String getRedirectUrl(String returnUrl);

    /**
     * Function to get ask for help url based on client
     *
     * @return A string with the ask for help url
     */
    String getDefaultAskForUrl();

    /**
     * Function to get faq url
     *
     * @return A string with the faq url
     */
    String getFaqUrl();

    /**
     * Function to get client
     *
     * @param cookieName for fetching the client record
     * @return The matching client object
     */
    Client getClientByCookieName(String cookieName);

    /**
     * API to get sso logout URL for redirection
     *
     * @param returnUrl - Return URL from redirection from SSO after logout
     * @return - SSO logout redirection URL with appropriate query params
     */
    String getLogoutUrl(String returnUrl);
}
