package com.everwell.registryservice.service.impl;

import com.everwell.registryservice.constants.ValidationStatusEnum;
import com.everwell.registryservice.exceptions.NotFoundException;
import com.everwell.registryservice.exceptions.ValidationException;
import com.everwell.registryservice.models.db.Client;
import com.everwell.registryservice.models.db.Engagement;
import com.everwell.registryservice.models.dto.EngagementDetails;
import com.everwell.registryservice.models.dto.HierarchyResponseData;
import com.everwell.registryservice.models.dto.LanguageMapping;
import com.everwell.registryservice.models.http.requests.UpdateLanguageRequest;
import com.everwell.registryservice.models.http.response.LanguagesResponse;
import com.everwell.registryservice.models.http.response.Response;
import com.everwell.registryservice.models.http.response.ins.LanguageResponse;
import com.everwell.registryservice.repository.EngagementRepository;
import com.everwell.registryservice.service.*;
import com.everwell.registryservice.utils.SentryUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.sql.Time;
import java.util.*;
import java.util.stream.Collectors;


@Service
public class EngagementServiceImpl implements EngagementService {

    @Autowired
    EngagementRepository engagementRepository;

    @Autowired
    HierarchyService hierarchyService;

    @Autowired
    ClientService clientService;

    @Autowired
    INSService insService;

    /**
     * API to fetch Engagement details by Hierarchy ID
     *
     * @param hierarchyId - Engagement for which hierarchy id is expected
     * @return Engagement Details
     */
    public EngagementDetails getEngagementByHierarchy(Long hierarchyId, boolean checkForParentEngagement)
    {
        Client client = clientService.getClientByToken();
        HierarchyResponseData hierarchy = hierarchyService.fetchHierarchyById(hierarchyId, client.getId());
        Engagement engagement = engagementRepository.findFirst1ByHierarchyId(hierarchy.getId());
        if (null == engagement) {
            if (checkForParentEngagement && hierarchy.getLevel() != 1) {
                return getEngagementByHierarchy(hierarchy.getParentId(), true);
            }
            return null;
        }
        return new EngagementDetails(engagement);
    }

    /**
     * API to add new hierarchy in DB
     *
     * @param engagementDetails - id for which engagement is expected
     * @return failed list of engagements
     */
    @Override
    public List<EngagementDetails> addEngagement(List<EngagementDetails> engagementDetails) {
        List<EngagementDetails> failedEntries = new ArrayList<>();
        for (EngagementDetails engagementDetail : engagementDetails) {
            boolean isValid = validateEngagementDetails(engagementDetail);
            if (!isValid) {
                failedEntries.add(engagementDetail);
                continue;
            }
            Engagement engagement = new Engagement (
                engagementDetail.getHierarchyId(), engagementDetail.getDefaultLanguage(),
                engagementDetail.getDefaultTime(), engagementDetail.getLanguages(),
                engagementDetail.getDoseTimings(), engagementDetail.getConsentMandatory(),
                engagementDetail.getNotificationType(), engagementDetail.getDisabledLanguages()
            );
            engagementRepository.save(engagement);
        }
        return failedEntries;
    }

    private boolean validateEngagementDetails(EngagementDetails engagementDetails) {
        try {
            Client client = clientService.getClientByToken();
            // If hierarchy does not exist, an exception will be thrown
            HierarchyResponseData hierarchy = hierarchyService.fetchHierarchyById(engagementDetails.getHierarchyId(), client.getId());

            EngagementDetails existingEngagement = getEngagementByHierarchy(hierarchy.getId(), false);

            if (null != existingEngagement) {
                return false;
            }
            // Validating input time // expected format HH:mm:ss
            Arrays.stream(engagementDetails.getDoseTimings().split(",")).forEach(doseTime -> {
                Time.valueOf(doseTime); // If not valid, it will throw illegalArgumentException
            });
            Time.valueOf(engagementDetails.getDefaultTime());

            List<String> languageIds = new ArrayList<>();
            languageIds.add(engagementDetails.getDefaultLanguage().toString());
            if(null != engagementDetails.getLanguages() && !engagementDetails.getLanguages().isEmpty())
                languageIds.addAll(Arrays.stream(engagementDetails.getLanguages().split(",")).collect(Collectors.toList()));
            if(null != engagementDetails.getDisabledLanguages() && !engagementDetails.getDisabledLanguages().isEmpty())
                languageIds.addAll(Arrays.stream(engagementDetails.getDisabledLanguages().split(",")).collect(Collectors.toList()));
            List<LanguageMapping> validateLanguageIds = CreateLanguageCodeNameMap(languageIds, client.getId(), true);
        }
        catch (Exception e)
        {
            return false;
        }
        return true;
    }

    @Override
    public Boolean updateLanguages(UpdateLanguageRequest updateLanguageRequest){
        Client client = clientService.getClientByToken();
        List<String> languageIds = updateLanguageRequest.getLanguageIds().stream().map(String::valueOf).collect(Collectors.toList());
        if (!CollectionUtils.isEmpty(updateLanguageRequest.getDisabledLanguageIds())){
            languageIds.addAll(updateLanguageRequest.getDisabledLanguageIds().stream().map(String::valueOf).collect(Collectors.toList()));
        }
        List<LanguageMapping> validateLanguageIds = CreateLanguageCodeNameMap(languageIds, client.getId(), true);
        HierarchyResponseData hierarchy = hierarchyService.fetchHierarchyById(updateLanguageRequest.getHierarchyId(), client.getId());
        try {
            Engagement hierarchyDefaultConfig = engagementRepository.findFirst1ByHierarchyId(hierarchy.getId());
            if(hierarchyDefaultConfig != null){
                hierarchyDefaultConfig.setLanguages(updateLanguageRequest.getLanguageIds().stream().map(String::valueOf).collect(Collectors.joining(",")));
                if(updateLanguageRequest.getDisabledLanguageIds() != null) {
                    hierarchyDefaultConfig.setDisabledLanguages(updateLanguageRequest.getDisabledLanguageIds().stream().map(String::valueOf).collect(Collectors.joining(",")));
                }
                engagementRepository.save(hierarchyDefaultConfig);
                return true;
            }
            else{
                EngagementDetails parentEngagement = getEngagementByHierarchy(updateLanguageRequest.getHierarchyId(), true);
                List<EngagementDetails> engagementDetails = new ArrayList<>();
                engagementDetails.add(new EngagementDetails(
                        updateLanguageRequest.getHierarchyId(), updateLanguageRequest.getLanguageIds().get(0),
                        parentEngagement.getDefaultTime(), updateLanguageRequest.getLanguageIds().stream().map(String::valueOf).collect(Collectors.joining(",")),
                        parentEngagement.getDoseTimings(), parentEngagement.getConsentMandatory(),
                        parentEngagement.getNotificationType(), null
                ));
                List<EngagementDetails> result = addEngagement(engagementDetails);
                return CollectionUtils.isEmpty(result);
            }
        }
        catch (Exception e){
            SentryUtils.captureException(e);
            return false;
        }
    }

    private List<LanguageMapping> CreateLanguageCodeNameMap(List<String> languages, Long clientId, Boolean validate){
        List<LanguageMapping> languageCodeNameMap = new ArrayList<>();
        if(languages != null && languages.size() > 0) {
            ResponseEntity<Response<LanguageResponse>> response = insService.getAllLanguages(clientId.toString());
            if(response.getBody() == null || !response.getBody().isSuccess()){
                throw new NotFoundException(response.getBody().getMessage());
            }
            LanguageResponse languageResponse = response.getBody().getData();
            for (String language : languages) {
                LanguageMapping mapping = languageResponse.getLanguageMappings().stream().filter(l -> l.getLanguageId().toString().equals(language)).findFirst().orElse(null);
                if (mapping != null) {
                    languageCodeNameMap.add(mapping);
                }
                if(null == mapping && validate){
                    throw new ValidationException(ValidationStatusEnum.LANGUAGE_ID_NOT_FOUND);
                }
            }
        }
        return languageCodeNameMap;
    }

    @Override
    public LanguagesResponse getLanguages(Long hierarchyId){
        try {
            Client client = clientService.getClientByToken();
            Long clientId = client.getId();
            HierarchyResponseData hierarchy = hierarchyService.fetchHierarchyById(hierarchyId, clientId);
            Engagement hierarchyConfig = engagementRepository.findFirst1ByHierarchyId(hierarchy.getId());
            Engagement deploymentConfig = hierarchy.getLevel() != 1 ? engagementRepository.findFirst1ByHierarchyId(hierarchy.getLevel1Id()) : hierarchyConfig;
            String allLanguages = deploymentConfig.getLanguages();
            String disabledLanguages = null;
            if (hierarchy.getLevel() == 1 && hierarchyConfig.getDisabledLanguages() != null) {
                disabledLanguages = hierarchyConfig.getDisabledLanguages();
                allLanguages = allLanguages + "," + disabledLanguages;
            }
            else {
                if (!StringUtils.isEmpty(allLanguages) && hierarchy.getLevel() >= 2) {
                    List<Long> levelId = Arrays.asList(hierarchy.getLevel1Id(), hierarchy.getLevel2Id(), hierarchy.getLevel3Id(), hierarchy.getLevel4Id(), hierarchy.getLevel5Id(), hierarchy.getLevel6Id());
                    disabledLanguages = engagementRepository.findByHierarchyIdInAndDisabledLanguagesNotNull(levelId).stream().map(Engagement::getDisabledLanguages).collect(Collectors.joining(","));
                }
                allLanguages = engagementRepository.findFirst1ByHierarchyId(hierarchy.getParentId()).getLanguages();
            }
            List<String> availableLanguages = Arrays.stream(allLanguages.split(",")).distinct().collect(Collectors.toList());
            List<String> selectedLanguages = Arrays.stream(hierarchyConfig.getLanguages().split(",")).collect(Collectors.toList());
            if(disabledLanguages != null) {
                List<String> disabled = Arrays.stream(disabledLanguages.split(",")).distinct().collect(Collectors.toList());
                if (hierarchy.getLevel() != 1) {
                    availableLanguages = availableLanguages.stream().filter(language -> !disabled.contains(language)).collect(Collectors.toList());
                }
                selectedLanguages = selectedLanguages.stream().filter(language -> !disabled.contains(language)).collect(Collectors.toList());
            }
            return new LanguagesResponse(
                    CreateLanguageCodeNameMap(availableLanguages, clientId, false),
                    CreateLanguageCodeNameMap(selectedLanguages, clientId, false)
            );
        }
        catch (Exception e){
            SentryUtils.captureException(e);
            return null;
        }
    }
}
