package com.everwell.registryservice.service;

import com.everwell.registryservice.models.dto.EngagementDetails;
import com.everwell.registryservice.models.http.requests.UpdateLanguageRequest;
import com.everwell.registryservice.models.http.response.LanguagesResponse;

import java.util.List;

public interface EngagementService {
    EngagementDetails getEngagementByHierarchy(Long HierarchyId, boolean checkForParentEngagement);
    List<EngagementDetails> addEngagement(List<EngagementDetails> engagementDetails);
    Boolean updateLanguages(UpdateLanguageRequest updateLanguageRequest);
    LanguagesResponse getLanguages(Long hierarchyId);
}
