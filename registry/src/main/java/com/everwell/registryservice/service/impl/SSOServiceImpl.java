package com.everwell.registryservice.service.impl;


import com.everwell.registryservice.constants.SsoRoutes;
import com.everwell.registryservice.constants.ValidationStatusEnum;
import com.everwell.registryservice.exceptions.InternalServerErrorException;
import com.everwell.registryservice.exceptions.ValidationException;
import com.everwell.registryservice.models.dto.ParameterExchange;
import com.everwell.registryservice.models.http.requests.sso.RegistrationRequest;
import com.everwell.registryservice.models.http.response.Response;
import com.everwell.registryservice.models.http.response.sso.LoginResponse;
import com.everwell.registryservice.models.http.response.sso.UserSSOResponse;
import com.everwell.registryservice.service.SSOService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class SSOServiceImpl extends DataGatewayServiceImpl implements SSOService {

    @Override
    public ResponseEntity<Response<LoginResponse>> login(String clientId, Map<String, Object> loginRequest) {
        String endpoint = SsoRoutes.LOGIN.getRoute();
        ParameterExchange exchange = new ParameterExchange(clientId, endpoint, loginRequest, new ParameterizedTypeReference<Response<LoginResponse>>() {
        });
        return postExchange(exchange);
    }

    public UserSSOResponse registration(String clientId, RegistrationRequest registrationRequest) {
        String endpoint = SsoRoutes.REGISTRATION.getRoute();
        ParameterExchange exchange = new ParameterExchange(clientId, endpoint, registrationRequest, new ParameterizedTypeReference<Response<UserSSOResponse>>() {
        });
        ResponseEntity<Response<UserSSOResponse>> ssoResponse = postExchange(exchange);
        if (ssoResponse.getBody() == null || !ssoResponse.getBody().isSuccess() && ssoResponse.getStatusCode().is4xxClientError()) {
            throw new ValidationException(ssoResponse.getBody().getMessage());
        } else if (null == ssoResponse.getBody().getData()) {
            throw new InternalServerErrorException(ValidationStatusEnum.SOMETHING_WENT_WRONG);
        }
        return ssoResponse.getBody().getData();
    }

    public Boolean logout(String clientId, Map<String, Object> logoutRequest) {
        String endpoint = SsoRoutes.LOGOUT.getRoute();
        ParameterExchange exchange = new ParameterExchange(clientId, endpoint, logoutRequest, new ParameterizedTypeReference<Response<Void>>() {
        });
        return postExchange(exchange).getBody().isSuccess();
    }

}
