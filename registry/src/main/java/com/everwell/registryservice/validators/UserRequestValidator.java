package com.everwell.registryservice.validators;

import com.everwell.registryservice.constants.ValidationStatusEnum;
import com.everwell.registryservice.exceptions.ConflictException;
import com.everwell.registryservice.models.db.UserAccess;
import com.everwell.registryservice.models.http.requests.CreateUserRequest;
import com.everwell.registryservice.repository.UserAccessRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Class Type - Validator
 * Purpose - Validate Request received by User Controller
 * Scope - Hierarchy Module
 *
 * @author Ashish Shrivastava
 */
@Component
public class UserRequestValidator {

    @Autowired
    private UserAccessRepository userAccessRepository;

    /**
     * Validator method to validate CreateUserRequest received by Controller
     *
     * @param createUserRequest - Input Create User Request
     */
    public void validateCreateUserRequest(CreateUserRequest createUserRequest) {
        // Validate if username to be created already exists
        UserAccess user = userAccessRepository.findBySsoIdOrUsername(createUserRequest.getSsoId(), createUserRequest.getUsername()).orElse(null);
        if (null != user) {
            throw new ConflictException(ValidationStatusEnum.USER_ALREADY_EXISTS);
        }
    }
}
