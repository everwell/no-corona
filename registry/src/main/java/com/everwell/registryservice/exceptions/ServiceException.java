package com.everwell.registryservice.exceptions;

public class ServiceException extends RuntimeException {

    public ServiceException(String exception) {
        super(exception);
    }
}
