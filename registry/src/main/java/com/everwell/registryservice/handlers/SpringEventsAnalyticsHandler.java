package com.everwell.registryservice.handlers;

import com.everwell.registryservice.binders.EventFlowBinder;
import com.everwell.registryservice.models.dto.EventStreamingAnalyticsDto;
import com.everwell.registryservice.models.dto.GenericEvents;
import com.everwell.registryservice.service.ClientService;
import com.everwell.registryservice.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

@Component
@EnableBinding(EventFlowBinder.class)
public class SpringEventsAnalyticsHandler {
    @Autowired
    private ClientService clientService;
    @Autowired
    EventFlowBinder eventFlowBinder;
    private static final Logger LOGGER = LoggerFactory.getLogger(SpringEventsAnalyticsHandler.class);
    private static String EVENT_FLOW_HEADER = "CLIENT_ID";

    @EventListener
    public void trackEvents(GenericEvents genericEvent) {
        String eventName = genericEvent.getEvent().getEventName();
        if (genericEvent.getData() != null)
            eventName = Utils.asJsonString(genericEvent.getData());

        EventStreamingAnalyticsDto eventStreamingAnalyticsDto = new EventStreamingAnalyticsDto(
                genericEvent.getEvent().getEventCategory(),
                eventName,
                genericEvent.getEvent().getEventAction()
        );
        LOGGER.info("[" + genericEvent.getEvent().getEventCategory() + "] called - " + Utils.asJsonString(eventStreamingAnalyticsDto));
        pushToQueue(eventStreamingAnalyticsDto);
    }
    public void pushToQueue(EventStreamingAnalyticsDto eventStreamingAnalyticsDto) {
        Long eventFlowId = clientService.getClientByToken().getEventFlowId();

        eventFlowBinder.eventOutput().send(MessageBuilder.withPayload(Utils.asJsonString(eventStreamingAnalyticsDto))
                .setHeader(EVENT_FLOW_HEADER, eventFlowId).build());
    }
}