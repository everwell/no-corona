package com.everwell.registryservice.binders;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface INSBinder {
    String INS_SMS_INPUT = "ins-sms-input";

    @Output(INS_SMS_INPUT)
    MessageChannel insSmsOutput();
}
