package com.everwell.registryservice.repository;

import com.everwell.registryservice.models.db.VendorMapping;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VendorRepository extends JpaRepository<VendorMapping, Long> {
    VendorMapping findFirstByDeploymentIdAndTriggerRegistryId(Long deploymentId, Long triggerRegistryId);
}
