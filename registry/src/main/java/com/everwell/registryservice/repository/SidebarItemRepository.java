package com.everwell.registryservice.repository;

import com.everwell.registryservice.models.db.SidebarItem;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SidebarItemRepository extends JpaRepository<SidebarItem, Long> {

}
