package com.everwell.registryservice.repository;

import com.everwell.registryservice.models.db.VendorMapping;
import org.springframework.data.jpa.repository.JpaRepository;


public interface VendorMappingRepository extends JpaRepository<VendorMapping, Long> {

    VendorMapping getVendorMappingByTriggerRegistryId(Long triggerRegistryId);

}