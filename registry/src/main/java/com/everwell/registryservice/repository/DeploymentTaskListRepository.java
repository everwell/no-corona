package com.everwell.registryservice.repository;

import com.everwell.registryservice.models.db.DeploymentTaskList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

import java.util.List;

public interface DeploymentTaskListRepository extends JpaRepository<DeploymentTaskList, Long> {
    List<DeploymentTaskList> getAllByDeploymentIdOrderByDisplayOrderAscIdAsc(Long deploymentId);

    @Query(value = "select t.* from deployment_task_list t where t.task_list_id = :taskListId and t.deployment_id =:deploymentId", nativeQuery = true)
    List<DeploymentTaskList> getDeploymentTaskListByIdAndDeploymentId(Long taskListId, Long deploymentId);

}
