package com.everwell.registryservice.repository;

import com.everwell.registryservice.models.db.AssociationsMaster;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AssociationsMasterRepository extends JpaRepository<AssociationsMaster, Long> {

    Integer countByTypeIn(List<String> types);

    List<AssociationsMaster> findByTypeIn(List<String> types);

    List<AssociationsMaster> findByIdIn(List<Long> ids);

    List<AssociationsMaster> findAll();
}
