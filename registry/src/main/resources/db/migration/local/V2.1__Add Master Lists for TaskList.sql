create table task_list (
  id  bigserial not null,
  name text,
  web_icon text,
  app_icon text,
  description text,
  extra_data text,
  primary key (id)
);

create table filters (
  id  bigserial not null,
  name text,
  primary key (id)
);

create table filter_values (
  id  bigserial not null,
  filter_id bigint,
  value text,
  primary key (id)
);

INSERT INTO task_list (name, web_icon, app_icon, description, extra_data) VALUES ('_end_date_passed', 'fa fa-clock-o', '{md-alarm}', 'tasklist_desc_end_date_passed', '{"name":"End Date Passed Patients","showSummary":"false","defaultSortField":"DAYS_PAST_END_DATE","defaultSortOrder":false,"hideAddPatients":true,"extraFields":["DAYS_PAST_END_DATE"],"disabledFields":["END_DATE_PASSED"],"showCallButton":false,"allowToggleSwitch":false,"showBlankSummary":true,"removePriorityColor":false,"lastDosageTwoDays":false,"lastDosageThreePlusDays":false,"lastDosageOneDay":false}');
INSERT INTO task_list (name, web_icon, app_icon, description, extra_data) VALUES ('_follow_up_call_needed', 'fa fa-phone', '{md-call}', 'tasklist_desc_missed_yesterday_dose', '{"name":"Follow Up Call Needed","showSummary":"false","defaultSortField":"ADHERENCE_7","defaultSortOrder":false,"hideAddPatients":true,"extraFields":[],"disabledFields":["LAST_DOSAGE"],"showCallButton":true,"allowToggleSwitch":false,"showBlankSummary":true,"removePriorityColor":false,"lastDosageTwoDays":false,"lastDosageThreePlusDays":false,"lastDosageOneDay":false}');
INSERT INTO task_list (name, web_icon, app_icon, description, extra_data) VALUES ('_home_visit_required', 'fa fa-home', '{md-home}', 'tasklist_desc_missed_last_two_doses', '{"name":"Home Visit Required","showSummary":false,"defaultSortField":"LAST_DOSAGE","defaultSortOrder":false,"hideAddPatients":true,"extraFields":[],"disabledFields":[],"showCallButton":false,"allowToggleSwitch":false,"showBlankSummary":false,"removePriorityColor":true,"lastDosageTwoDays":false,"lastDosageThreePlusDays":false,"lastDosageOneDay":false}');
INSERT INTO task_list (name, web_icon, app_icon, description, extra_data) VALUES ('_lost_to_follow_up', 'fa fa-group', '{md-group}', 'tasklist_desc_lost_to_follow_up', '{"name":"Lost to follow-up","showSummary":false,"defaultSortField":"LAST_DOSAGE","defaultSortOrder":false,"hideAddPatients":true,"extraFields":[],"disabledFields":[],"showCallButton":false,"allowToggleSwitch":false,"showBlankSummary":false,"removePriorityColor":true,"lastDosageTwoDays":false,"lastDosageThreePlusDays":false,"lastDosageOneDay":false}');
INSERT INTO task_list (name, web_icon, app_icon, description, extra_data) VALUES ('_missed_doses', 'fa fa-phone', '{md-call}', 'tasklist_desc_missed_last_three_doses', '{"name":"Missed Doses","showSummary":false,"defaultSortField":"LAST_DOSAGE","defaultSortOrder":false,"hideAddPatients":true,"extraFields":[],"disabledFields":[],"showCallButton":false,"allowToggleSwitch":false,"showBlankSummary":false,"removePriorityColor":true,"lastDosageTwoDays":false,"lastDosageThreePlusDays":false,"lastDosageOneDay":false}');
INSERT INTO task_list (name, web_icon, app_icon, description, extra_data) VALUES ('_perc_high_attention', 'fa fa-exclamation-circle', '{md-error}', 'tasklist_desc_high_attention', '{"name":"High Attention Patients","showSummary":"false","defaultSortField":"ADHERENCE_30","defaultSortOrder":false,"hideAddPatients":true,"extraFields":[],"disabledFields":["PRIORITY","END_DATE_PASSED"],"showCallButton":false,"allowToggleSwitch":false,"showBlankSummary":true,"removePriorityColor":false,"lastDosageTwoDays":false,"lastDosageThreePlusDays":false,"lastDosageOneDay":false}');
INSERT INTO task_list (name, web_icon, app_icon, description, extra_data) VALUES ('Missed last three days', 'fa fa-phone', '{md-call}', 'tasklist_desc_missed_last_three_doses', '{"name":"Missed last three doses","showSummary":false,"defaultSortField":"LAST_DOSAGE","defaultSortOrder":false,"hideAddPatients":true,"extraFields":[],"disabledFields":[],"showCallButton":true,"allowToggleSwitch":false,"showBlankSummary":false,"removePriorityColor":true,"lastDosageTwoDays":false,"lastDosageThreePlusDays":false,"lastDosageOneDay":false}');
INSERT INTO task_list (name, web_icon, app_icon, description, extra_data) VALUES ('tasklist_dose_not_reported_today', 'fa fa-calendar-minus-o', '{md-event}', 'tasklist_desc_dose_not_reported_today', '{"name":"Dose not reported today","showSummary":false,"defaultSortField":"LAST_DOSAGE","defaultSortOrder":false,"hideAddPatients":true,"extraFields":[],"disabledFields":[],"showCallButton":false,"allowToggleSwitch":false,"showBlankSummary":false,"removePriorityColor":true,"lastDosageTwoDays":false,"lastDosageThreePlusDays":false,"lastDosageOneDay":false}');
INSERT INTO task_list (name, web_icon, app_icon, description, extra_data) VALUES ('tasklist_follow_up_necessary', 'fa fa-phone', '{md-call}', 'tasklist_desc_follow_up', '{"name":"Follow up Necessary","showSummary":"false","defaultSortField":"ADHERENCE_7","defaultSortOrder":false,"hideAddPatients":true,"extraFields":[],"disabledFields":["LAST_DOSAGE"],"showCallButton":true,"allowToggleSwitch":false,"showBlankSummary":true,"removePriorityColor":false,"lastDosageTwoDays":false,"lastDosageThreePlusDays":false,"lastDosageOneDay":false}');
INSERT INTO task_list (name, web_icon, app_icon, description, extra_data) VALUES ('tasklist_follow_up_needed', 'fa fa-phone', '{md-call}', 'tasklist_desc_missed_yesterday_dose', '{"name":"Follow Up Needed","showSummary":"false","defaultSortField":"ADHERENCE_7","defaultSortOrder":false,"hideAddPatients":true,"extraFields":[],"disabledFields":["LAST_DOSAGE"],"showCallButton":true,"allowToggleSwitch":false,"showBlankSummary":true,"removePriorityColor":false,"lastDosageTwoDays":false,"lastDosageThreePlusDays":false,"lastDosageOneDay":false}');
INSERT INTO task_list (name, web_icon, app_icon, description, extra_data) VALUES ('tasklist_lfu', 'fa fa-exclamation-triangle', '{md-error}', 'tasklist_desc_lfu', '{"name":"LFU","showSummary":false,"defaultSortField":"LAST_DOSAGE","defaultSortOrder":false,"hideAddPatients":true,"extraFields":[],"disabledFields":[],"showCallButton":false,"allowToggleSwitch":false,"showBlankSummary":false,"removePriorityColor":true,"lastDosageTwoDays":false,"lastDosageThreePlusDays":false,"lastDosageOneDay":false}');
INSERT INTO task_list (name, web_icon, app_icon, description, extra_data) VALUES ('tasklist_mis_1', 'fa fa-exclamation-triangle', '{md-error}', 'tasklist_desc_mis_1', '{"name":"MIS 1","showSummary":false,"defaultSortField":"LAST_DOSAGE","defaultSortOrder":false,"hideAddPatients":true,"extraFields":[],"disabledFields":[],"showCallButton":false,"allowToggleSwitch":false,"showBlankSummary":false,"removePriorityColor":true,"lastDosageTwoDays":false,"lastDosageThreePlusDays":false,"lastDosageOneDay":false}');
INSERT INTO task_list (name, web_icon, app_icon, description, extra_data) VALUES ('tasklist_mis_2', 'fa fa-exclamation-triangle', '{md-error}', 'tasklist_desc_mis_2', '{"name":"MIS 2","showSummary":false,"defaultSortField":"LAST_DOSAGE","defaultSortOrder":false,"hideAddPatients":true,"extraFields":[],"disabledFields":[],"showCallButton":false,"allowToggleSwitch":false,"showBlankSummary":false,"removePriorityColor":true,"lastDosageTwoDays":false,"lastDosageThreePlusDays":false,"lastDosageOneDay":false}');
INSERT INTO task_list (name, web_icon, app_icon, description, extra_data) VALUES ('tasklist_missed_checkin', 'fa fa-phone', '{md-call}', 'tasklist_desc_missed_ivr_checkin', '{"name":"Check-in Missed","showSummary":"false","defaultSortOrder":false,"hideAddPatients":true,"extraFields":[],"disabledFields":[],"showCallButton":true,"allowToggleSwitch":true,"showBlankSummary":false,"removePriorityColor":false,"lastDosageTwoDays":false,"lastDosageThreePlusDays":false,"lastDosageOneDay":false}');
INSERT INTO task_list (name, web_icon, app_icon, description, extra_data) VALUES ('tasklist_missed_last_dose', 'fa fa-phone', '{md-call}', 'tasklist_desc_missed_yesterday_dose', '{"name":"Missed Last Dose","showSummary":false,"defaultSortField":"LAST_DOSAGE","defaultSortOrder":false,"hideAddPatients":true,"extraFields":[],"disabledFields":[],"showCallButton":false,"allowToggleSwitch":false,"showBlankSummary":false,"removePriorityColor":true,"lastDosageTwoDays":false,"lastDosageThreePlusDays":false,"lastDosageOneDay":false}');
INSERT INTO task_list (name, web_icon, app_icon, description, extra_data) VALUES ('tasklist_missed_last_three_doses', 'fa fa-exclamation-triangle', '{md-error}', 'tasklist_desc_missed_last_three_doses', '{"name":"Missed last three doses","showSummary":false,"defaultSortField":"LAST_DOSAGE","defaultSortOrder":false,"hideAddPatients":true,"extraFields":[],"disabledFields":[],"showCallButton":false,"allowToggleSwitch":false,"showBlankSummary":false,"removePriorityColor":true,"lastDosageTwoDays":false,"lastDosageThreePlusDays":false,"lastDosageOneDay":false}');
INSERT INTO task_list (name, web_icon, app_icon, description, extra_data) VALUES ('tasklist_missed_last_three_plus_doses', 'fa fa-exclamation-triangle', '{md-error}', 'tasklist_desc_missed_last_three_doses', '{"name":"Missed last three doses","showSummary":false,"defaultSortField":"LAST_DOSAGE","defaultSortOrder":false,"hideAddPatients":true,"extraFields":[],"disabledFields":[],"showCallButton":false,"allowToggleSwitch":false,"showBlankSummary":false,"removePriorityColor":true,"lastDosageTwoDays":false,"lastDosageThreePlusDays":false,"lastDosageOneDay":false}');
INSERT INTO task_list (name, web_icon, app_icon, description, extra_data) VALUES ('tasklist_missed_last_two_dose', 'fa fa-home', '{md-home}', 'tasklist_desc_missed_last_two_doses', '{"name":"Missed last two doses","showSummary":false,"defaultSortField":"LAST_DOSAGE","defaultSortOrder":false,"hideAddPatients":true,"extraFields":[],"disabledFields":[],"showCallButton":false,"allowToggleSwitch":false,"showBlankSummary":false,"removePriorityColor":true,"lastDosageTwoDays":false,"lastDosageThreePlusDays":false,"lastDosageOneDay":false}');
INSERT INTO task_list (name, web_icon, app_icon, description, extra_data) VALUES ('tasklist_missed_yesterday_dose', 'fa fa-phone', '{md-call}', 'tasklist_desc_missed_yesterday_dose', '{"name":"Missed yesterdays dose","showSummary":false,"defaultSortField":"LAST_DOSAGE","defaultSortOrder":false,"hideAddPatients":true,"extraFields":[],"disabledFields":[],"showCallButton":false,"allowToggleSwitch":false,"showBlankSummary":false,"removePriorityColor":true,"lastDosageTwoDays":false,"lastDosageThreePlusDays":false,"lastDosageOneDay":true}');
INSERT INTO task_list (name, web_icon, app_icon, description, extra_data) VALUES ('tasklist_negative_checkin', 'fa fa-exclamation-circle', '{md-error}', 'tasklist_desc_negative_ivr_checkin', '{"name":"Check-in Negative","showSummary":"false","defaultSortOrder":false,"hideAddPatients":true,"extraFields":[],"disabledFields":[],"showCallButton":true,"allowToggleSwitch":true,"showBlankSummary":false,"removePriorityColor":false,"lastDosageTwoDays":false,"lastDosageThreePlusDays":false,"lastDosageOneDay":false}');
INSERT INTO task_list (name, web_icon, app_icon, description, extra_data) VALUES ('tasklist_patients_in_crisis', 'fa fa-exclamation-triangle', '{md-error}', 'tasklist_desc_missed_last_three_doses', '{"name":"Patients In Crisis","showSummary":false,"defaultSortField":"LAST_DOSAGE","defaultSortOrder":false,"hideAddPatients":true,"extraFields":[],"disabledFields":[],"showCallButton":false,"allowToggleSwitch":false,"showBlankSummary":false,"removePriorityColor":true,"lastDosageTwoDays":false,"lastDosageThreePlusDays":false,"lastDosageOneDay":false}');
INSERT INTO task_list (name, web_icon, app_icon, description, extra_data) VALUES ('tasklist_refill_due', 'fa fa-exclamation-triangle', '{md-error}', 'tasklist_desc_refill_due', '{"name":"Refill Due","showSummary":false,"defaultSortField":"LAST_DOSAGE","defaultSortOrder":false,"hideAddPatients":true,"extraFields":[],"disabledFields":[],"showCallButton":false,"allowToggleSwitch":false,"showBlankSummary":false,"removePriorityColor":true,"lastDosageTwoDays":false,"lastDosageThreePlusDays":false,"lastDosageOneDay":false}');
INSERT INTO task_list (name, web_icon, app_icon, description, extra_data) VALUES ('tasklist_sputum_test_due', 'fa fa-flask', '{md-colorize}', 'tasklist_desc_sputum_test_due', '{"name":"Sputum Test Due","showSummary":false,"defaultSortOrder":false,"hideAddPatients":true,"extraFields":[],"disabledFields":[],"showCallButton":false,"allowToggleSwitch":false,"showBlankSummary":false,"removePriorityColor":true,"lastDosageTwoDays":false,"lastDosageThreePlusDays":false,"lastDosageOneDay":false}');
INSERT INTO task_list (name, web_icon, app_icon, description, extra_data) VALUES ('tasklist_treatment_outcome_due', 'fa fa-calendar-minus-o', '{md-event}', 'tasklist_desc_treatment_outcome_due', '{"name":"Treatment Outcome Due","showSummary":false,"defaultSortField":"LAST_DOSAGE","defaultSortOrder":false,"hideAddPatients":true,"extraFields":[],"disabledFields":[],"showCallButton":false,"allowToggleSwitch":false,"showBlankSummary":false,"removePriorityColor":true,"lastDosageTwoDays":false,"lastDosageThreePlusDays":false,"lastDosageOneDay":false}');

INSERT INTO filters (name) VALUES ('AttentionRequired');
INSERT INTO filters (name) VALUES ('DidntCallYesterday');
INSERT INTO filters (name) VALUES ('DoseNotReportedToday');
INSERT INTO filters (name) VALUES ('EndDatePassed');
INSERT INTO filters (name) VALUES ('FollowUpNeeded');
INSERT INTO filters (name) VALUES ('LastIvrStatusList');
INSERT INTO filters (name) VALUES ('MaxDaysSinceTreatmentStart');
INSERT INTO filters (name) VALUES ('MinDaysSinceTreatmentStart');
INSERT INTO filters (name) VALUES ('MonitoringMethod');
INSERT INTO filters (name) VALUES ('NumberOfConsecutiveMissedDoses');
INSERT INTO filters (name) VALUES ('NumberOfConsecutiveMissedDosesMax');
INSERT INTO filters (name) VALUES ('NumberOfConsecutiveMissedDosesMin');
INSERT INTO filters (name) VALUES ('OnTreatment');
INSERT INTO filters (name) VALUES ('PositiveDosingDays');
INSERT INTO filters (name) VALUES ('RefillDue');
INSERT INTO filters (name) VALUES ('ScheduleType');

INSERT INTO filter_values (filter_id, value) VALUES ((SELECT id FROM filters WHERE name = 'AttentionRequired'), 'HIGH');
INSERT INTO filter_values (filter_id, value) VALUES ((SELECT id FROM filters WHERE name = 'DidntCallYesterday'), 'True');
INSERT INTO filter_values (filter_id, value) VALUES ((SELECT id FROM filters WHERE name = 'DoseNotReportedToday'), 'True');
INSERT INTO filter_values (filter_id, value) VALUES ((SELECT id FROM filters WHERE name = 'EndDatePassed'), 'True');
INSERT INTO filter_values (filter_id, value) VALUES ((SELECT id FROM filters WHERE name = 'EndDatePassed'), 'False');
INSERT INTO filter_values (filter_id, value) VALUES ((SELECT id FROM filters WHERE name = 'FollowUpNeeded'), 'True');
INSERT INTO filter_values (filter_id, value) VALUES ((SELECT id FROM filters WHERE name = 'LastIvrStatusList'), 'FAILURE,UNCLEAR');
INSERT INTO filter_values (filter_id, value) VALUES ((SELECT id FROM filters WHERE name = 'LastIvrStatusList'), 'NEGATIVE');
INSERT INTO filter_values (filter_id, value) VALUES ((SELECT id FROM filters WHERE name = 'MaxDaysSinceTreatmentStart'), '60');
INSERT INTO filter_values (filter_id, value) VALUES ((SELECT id FROM filters WHERE name = 'MinDaysSinceTreatmentStart'), '178');
INSERT INTO filter_values (filter_id, value) VALUES ((SELECT id FROM filters WHERE name = 'MonitoringMethod'), '99DOTS');
INSERT INTO filter_values (filter_id, value) VALUES ((SELECT id FROM filters WHERE name = 'NumberOfConsecutiveMissedDoses'), '1');
INSERT INTO filter_values (filter_id, value) VALUES ((SELECT id FROM filters WHERE name = 'NumberOfConsecutiveMissedDoses'), '2');
INSERT INTO filter_values (filter_id, value) VALUES ((SELECT id FROM filters WHERE name = 'NumberOfConsecutiveMissedDoses'), '3');
INSERT INTO filter_values (filter_id, value) VALUES ((SELECT id FROM filters WHERE name = 'NumberOfConsecutiveMissedDoses'), '57');
INSERT INTO filter_values (filter_id, value) VALUES ((SELECT id FROM filters WHERE name = 'NumberOfConsecutiveMissedDoses'), '90');
INSERT INTO filter_values (filter_id, value) VALUES ((SELECT id FROM filters WHERE name = 'NumberOfConsecutiveMissedDosesMax'), '60');
INSERT INTO filter_values (filter_id, value) VALUES ((SELECT id FROM filters WHERE name = 'NumberOfConsecutiveMissedDosesMax'), '90');
INSERT INTO filter_values (filter_id, value) VALUES ((SELECT id FROM filters WHERE name = 'NumberOfConsecutiveMissedDosesMin'), '30');
INSERT INTO filter_values (filter_id, value) VALUES ((SELECT id FROM filters WHERE name = 'NumberOfConsecutiveMissedDosesMin'), '60');
INSERT INTO filter_values (filter_id, value) VALUES ((SELECT id FROM filters WHERE name = 'OnTreatment'), 'True');
INSERT INTO filter_values (filter_id, value) VALUES ((SELECT id FROM filters WHERE name = 'PositiveDosingDays'), '168');
INSERT INTO filter_values (filter_id, value) VALUES ((SELECT id FROM filters WHERE name = 'RefillDue'), 'True');
INSERT INTO filter_values (filter_id, value) VALUES ((SELECT id FROM filters WHERE name = 'ScheduleType'), '3HP');
INSERT INTO filter_values (filter_id, value) VALUES ((SELECT id FROM filters WHERE name = 'MinDaysSinceTreatmentStart'), '50');
INSERT INTO filter_values (filter_id, value) VALUES ((SELECT id FROM filters WHERE name = 'DidntCallYesterday'), '1');
INSERT INTO filter_values (filter_id, value) VALUES ((SELECT id FROM filters WHERE name = 'OnTreatment'), '1');


alter table deployment_task_list add task_list_id bigint;

alter table deployment_task_list drop column name;
alter table deployment_task_list drop column web_icon;
alter table deployment_task_list drop column app_icon;
alter table deployment_task_list drop column description;
alter table deployment_task_list drop column extra_data;

alter table task_list_filter_map add filter_value_id bigint;

alter table task_list_filter_map drop column filter_name;
alter table task_list_filter_map drop column value;

