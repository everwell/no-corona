drop table if exists hierarchy cascade;
drop table if exists hierarchy_config cascade;
drop table if exists hierarchy_config_map cascade;
drop table if exists hierarchy_associations cascade;
drop table if exists user_access cascade;
drop table if exists user_access_hierarchy_map cascade;
drop table if exists config cascade;
drop table if exists associations_master cascade;

CREATE TABLE IF NOT EXISTS associations_master
(
    id bigserial NOT NULL,
    type varchar(255),
    CONSTRAINT associations_master_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS config
(
    id bigserial NOT NULL,
    created_at timestamp,
    default_value varchar(255),
    name varchar(255),
    type varchar(255),
    CONSTRAINT config_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS hierarchy
(
    id bigserial NOT NULL,
    active boolean NOT NULL,
    client_id bigint,
    code varchar(255),
    created_at timestamp,
    deployment_id bigint,
    end_date timestamp,
    has_children boolean NOT NULL,
    level bigint,
    level_1_code varchar(255),
    level_1_id bigint,
    level_1_name varchar(255),
    level_1_type varchar(255),
    level_2_code varchar(255),
    level_2_id bigint,
    level_2_name varchar(255),
    level_2_type varchar(255),
    level_3_code varchar(255),
    level_3_id bigint,
    level_3_name varchar(255),
    level_3_type varchar(255),
    level_4_code varchar(255),
    level_4_id bigint,
    level_4_name varchar(255),
    level_4_type varchar(255),
    level_5_code varchar(255),
    level_5_id bigint,
    level_5_name varchar(255),
    level_5_type varchar(255),
    level_6_code varchar(255),
    level_6_id bigint,
    level_6_name varchar(255),
    level_6_type varchar(255),
    merge_status varchar(255),
    name varchar(255),
    parent_id bigint,
    start_date timestamp,
    type varchar(255),
    updated_at timestamp,
    CONSTRAINT hierarchy_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS hierarchy_associations
(
    id bigserial NOT NULL,
    association_id bigint,
    hierarchy_id bigint,
    value varchar(255),
    CONSTRAINT hierarchy_associations_pkey PRIMARY KEY (id),
	CONSTRAINT hierarchy_associations_fk_hierarchy_id
      FOREIGN KEY(hierarchy_id)
	  REFERENCES hierarchy(id),
	CONSTRAINT hierarchy_associations_fk_association_id
      FOREIGN KEY(association_id)
	  REFERENCES associations_master(id)
);

CREATE TABLE IF NOT EXISTS hierarchy_config_map
(
    id bigserial NOT NULL,
    active boolean NOT NULL,
    config_mapping_id bigint,
    created_at timestamp without time zone,
    hierarchy_id bigint,
    value varchar(255),
    CONSTRAINT hierarchy_config_map_pkey PRIMARY KEY (id),
	CONSTRAINT hierarchy_config_map_fk_hierarchy_id
      FOREIGN KEY(hierarchy_id)
	  REFERENCES hierarchy(id),
	CONSTRAINT hierarchy_config_map_fk_config_mapping_id
      FOREIGN KEY(config_mapping_id)
	  REFERENCES config(id)
);

CREATE TABLE IF NOT EXISTS user_access
(
    id bigserial NOT NULL,
    active boolean NOT NULL,
    hierarchy_id bigint,
    selective_hierarchy_mapping boolean,
    sso_id bigint NOT NULL,
    username varchar(255),
    CONSTRAINT user_access_pkey PRIMARY KEY (id),
	CONSTRAINT user_access_fk_hierarchy_id
      FOREIGN KEY(hierarchy_id)
	  REFERENCES hierarchy(id)
);

CREATE TABLE IF NOT EXISTS user_access_hierarchy_map
(
    id bigserial NOT NULL,
    created_at timestamp,
    hierarchy_id bigint,
    last_updated_by bigint,
    relation varchar(255),
    updated_at timestamp,
    user_id bigint NOT NULL,
    CONSTRAINT user_access_hierarchy_map_pkey PRIMARY KEY (id),
	CONSTRAINT user_access_hierarchy_map_fk_hierarchy_mapping
      FOREIGN KEY(hierarchy_id)
	  REFERENCES hierarchy(id),
	CONSTRAINT user_access_hierarchy_map_fk_userid
      FOREIGN KEY(user_id)
	  REFERENCES user_access(id)
);
