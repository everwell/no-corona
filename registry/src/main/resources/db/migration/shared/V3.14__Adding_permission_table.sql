create table if not exists sidebar_permission (
    id bigserial not null,
    deployment_id bigint,
    designation varchar(255) not null,
    sidebar_id bigserial not null,
    is_active boolean NOT NULL DEFAULT true
);

insert into sidebar_permission (id, deployment_id, designation, sidebar_id)
select (select COALESCE(MAX(id), 0) + 1 + 1 from sidebar_permission), null, 'default', (select id from sidebar_item where name = 'overview' limit 1)
where not exists
(select * from sidebar_permission where deployment_id is null and designation = 'default' and sidebar_id = (select id from sidebar_item where name = 'overview' limit 1) );

insert into sidebar_permission (id, deployment_id, designation, sidebar_id)
select (select max(id) + 1 from sidebar_permission), null, 'default', (select id from sidebar_item where name = 'hierarchyManagement' limit 1)
where not exists
(select * from sidebar_permission where deployment_id is null and designation = 'default' and sidebar_id = (select id from sidebar_item where name = 'hierarchyManagement' limit 1) );

insert into sidebar_permission (id, deployment_id, designation, sidebar_id)
select (select max(id) + 1 from sidebar_permission), null, 'default', (select id from sidebar_item where name = '_task_lists' limit 1)
where not exists
(select * from sidebar_permission where deployment_id is null and designation = 'default' and sidebar_id = (select id from sidebar_item where name = '_task_lists' limit 1) );

insert into sidebar_permission (id, deployment_id, designation, sidebar_id)
select (select max(id) + 1 from sidebar_permission), null, 'default', (select id from sidebar_item where name = 'add_dispensation' limit 1)
where not exists
(select * from sidebar_permission where deployment_id is null and designation = 'default' and sidebar_id = (select id from sidebar_item where name = 'add_dispensation' limit 1) );

insert into sidebar_permission (id, deployment_id, designation, sidebar_id)
select (select max(id) + 1 from sidebar_permission), null, 'default', (select id from sidebar_item where name = 'evrimed_metrics' limit 1)
where not exists
(select * from sidebar_permission where deployment_id is null and designation = 'default' and sidebar_id = (select id from sidebar_item where name = 'evrimed_metrics' limit 1) );

insert into sidebar_permission (id, deployment_id, designation, sidebar_id)
select (select max(id) + 1 from sidebar_permission), null, 'default', (select id from sidebar_item where name = 'patient_management' limit 1)
where not exists
(select * from sidebar_permission where deployment_id is null and designation = 'default' and sidebar_id = (select id from sidebar_item where name = 'patient_management' limit 1) );

insert into sidebar_permission (id, deployment_id, designation, sidebar_id)
select (select max(id) + 1 from sidebar_permission), null, 'default', (select id from sidebar_item where name = '_staff_details' limit 1)
where not exists
(select * from sidebar_permission where deployment_id is null and designation = 'default' and sidebar_id = (select id from sidebar_item where name = '_staff_details' limit 1) );

insert into sidebar_permission (id, deployment_id, designation, sidebar_id)
select (select max(id) + 1 from sidebar_permission), null, 'default', (select id from sidebar_item where name = 'reports' limit 1)
where not exists
(select * from sidebar_permission where deployment_id is null and designation = 'default' and sidebar_id = (select id from sidebar_item where name = 'reports' limit 1) );

insert into sidebar_permission (id, deployment_id, designation, sidebar_id)
select (select max(id) + 1 from sidebar_permission), null, 'default', (select id from sidebar_item where name = 'home_fragment_vot' limit 1)
where not exists
(select * from sidebar_permission where deployment_id is null and designation = 'default' and sidebar_id = (select id from sidebar_item where name = 'home_fragment_vot' limit 1) );

insert into sidebar_permission (id, deployment_id, designation, sidebar_id)
select (select max(id) + 1 from sidebar_permission), null, 'default', (select id from sidebar_item where name = '_add_patient' limit 1)
where not exists
(select * from sidebar_permission where deployment_id is null and designation = 'default' and sidebar_id = (select id from sidebar_item where name = '_add_patient' limit 1) );

insert into sidebar_permission (id, deployment_id, designation, sidebar_id)
select (select max(id) + 1 from sidebar_permission), null, 'default', (select id from sidebar_item where name = '_add_medication_template' limit 1)
where not exists
(select * from sidebar_permission where deployment_id is null and designation = 'default' and sidebar_id = (select id from sidebar_item where name = '_add_medication_template' limit 1) );