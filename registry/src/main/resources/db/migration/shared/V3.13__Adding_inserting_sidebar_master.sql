create table if not exists sidebar_item (
    id bigserial not null,
    name varchar(255),
    icon varchar(255),
    link varchar(255),
    relative_path boolean NOT NULL
);

insert into sidebar_item(name, icon, link, relative_path) values('overview', 'tachometer-alt', '/dashboard/overview', true);
insert into sidebar_item(name, icon, link, relative_path) values('hierarchyManagement', 'tree-nodes', '/dashboard/hierarchymanagement', true);
insert into sidebar_item(name, icon, link, relative_path) values('_task_lists', 'list', '/dashboard/taskLists', true);
insert into sidebar_item(name, icon, link, relative_path) values('add_dispensation', 'tablet', '/dashboard/Dispensation', true);
insert into sidebar_item(name, icon, link, relative_path) values('evrimed_metrics', 'evrimed', '/dashboard/evrimed', true);
insert into sidebar_item(name, icon, link, relative_path) values('patient_management', 'briefcase-medical', '/dashboard/UnifiedPatient', true);
insert into sidebar_item(name, icon, link, relative_path) values('_staff_details', 'staff-user', '/dashboard/StaffDetails', true);
insert into sidebar_item(name, icon, link, relative_path) values('reports', 'bar-graph', '/dashboard/Reports', true);
insert into sidebar_item(name, icon, link, relative_path) values('home_fragment_vot', 'play-button', '/dashboard/reviewvot', true);
insert into sidebar_item(name, icon, link, relative_path) values('_add_patient', 'plus', '/dashboard/enrollment', true);
insert into sidebar_item(name, icon, link, relative_path) values('_add_medication_template', 'file-medical', '/dashboard/dispensation/template', true);