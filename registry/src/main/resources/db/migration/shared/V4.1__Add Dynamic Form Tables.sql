create table if not exists form
(
    id                      bigserial not null,
    form_label              varchar(100),
    form_name               varchar(100),
    save_endpoint           varchar(255),
    save_text               varchar(255),
    primary key (id)
);

create table if not exists field
(
    id                      bigserial not null,
    field_name              varchar(100),
    label                   varchar(100),
    component               varchar(100),
    field_options           text,
    disabled                boolean DEFAULT false,
    visible                 boolean DEFAULT true,
    required                boolean DEFAULT false,
    default_value           varchar(255),
    validation_list         varchar(100),
    row_number              bigint,
    column_number           bigint,
    remote_url              varchar(255),
    config                  text,
    type                    varchar(255),
    primary key (id)
);

create table if not exists form_part
(
    id                      bigserial not null,
    part_name               varchar(100),
    part_label              varchar(100),
    part_type               varchar(100),
    position                bigint,
    rows                    bigint,
    columns                 bigint,
    allow_row_open          boolean DEFAULT false,
    allow_row_delete        boolean DEFAULT false,
    list_item_title         varchar(100),
    item_description_field  varchar(100),
    row_delete_text         varchar(100),
    primary key (id)
);

create table if not exists form_items_map
(
    id                      bigserial not null,
    deployment_id           bigint,
    form_name               varchar(100),
    item_type               varchar(50),
    item_id                 bigint,
    parent_type             varchar(50),
    parent_id               bigint,
    primary key (id)
);

create table if not exists field_customizations
(
    id                      bigserial not null,
    field_id                bigint,
    attribute_name          varchar(100),
    attribute_value         text,
    primary key (id)
);

create table if not exists field_dependency
(
    id                      bigserial not null,
    type                    varchar(50),
    form_part_id            bigint,
    field_id                bigint,
    lookups                 text,
    property_and_values     text,
    primary key (id)
);

create index if not exists form_form_name
on form (form_name);

create index if not exists form_items_map_deployment_id_form_name
on form_items_map (deployment_id, form_name);