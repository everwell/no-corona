create table if not exists trigger
(
    id                  bigserial not null,
    client_id           bigint,
    hierarchy_id        bigint,
    trigger_id          bigint,
    default_template_id bigint,
    template_ids        text,
    event_name          text,
    function_name       text,
    cron_time           text,
    time_related        boolean,
    notification_type   text,
    mandatory           boolean,
    active              boolean,
    module              text,
    primary key (id)
);

create table if not exists engagement
(
    id                 bigserial not null,
    hierarchy_id       bigint,
    default_lang       bigint,
    default_time       text,
    languages          text,
    dose_timings       text,
    consent_mandatory  boolean,
    disabled_languages text,
    notification_type  text,
    primary key (id)
);

create table if not exists vendor_mapping
(
    id                  bigserial not null,
    deployment_id       bigint,
    vendor_id           bigint,
    trigger_registry_id bigint,
    primary key (id)
);