package com.everwell.registryservice.mappers;

import com.everwell.registryservice.models.db.Hierarchy;
import com.everwell.registryservice.models.dto.HierarchyResponseData;
import com.everwell.registryservice.models.dto.HierarchyUpdateData;
import com.everwell.registryservice.models.dto.MinHierarchyDetails;
import com.everwell.registryservice.models.http.response.HierarchyResponse;
import com.everwell.registryservice.models.http.response.HierarchySummaryResponseData;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class HierarchyMapperTest {

    private HierarchyMapper hierarchyMapper;

    @Before
    public void before() {
        hierarchyMapper = new HierarchyMapperImpl();
    }

    @Test
    public void testHierarchyResponseToMinHierarchyDetails_NullHierarchy() {
        HierarchyResponse hierarchyResponse = new HierarchyResponse();
        hierarchyResponse.setAssociations(Map.of("association1", "value"));
        MinHierarchyDetails hierarchyDetails = hierarchyMapper.hierarchyResponseToMinHierarchyDetails(hierarchyResponse);
        assertNotNull(hierarchyDetails);
    }

    @Test
    public void testHierarchyResponseToMinHierarchyDetails_Null() {
        MinHierarchyDetails hierarchyDetails = hierarchyMapper.hierarchyResponseToMinHierarchyDetails(null);
        assertNull(hierarchyDetails);
    }

    @Test
    public void hierarchyResponseListToMinHierarchyDetailsList_Null() {
        List<MinHierarchyDetails> hierarchyDetails = hierarchyMapper.hierarchyResponseListToMinHierarchyDetailsList(null);
        assertNull(hierarchyDetails);
    }

    @Test
    public void hierarchyResponseDataToHierarchySummaryResponseData_Null() {
        HierarchySummaryResponseData hierarchyDetails = hierarchyMapper.hierarchyResponseDataToHierarchySummaryResponseData(null);
        assertNull(hierarchyDetails);
    }

    @Test
    public void userUpdateHierarchyRequestToHierarchyUpdateData_Null() {
        HierarchyUpdateData hierarchyDetails = hierarchyMapper.userUpdateHierarchyRequestToHierarchyUpdateData(null);
        assertNull(hierarchyDetails);
    }

    @Test
    public void hierarchyToHierarchySummaryResponseData_Null() {
        HierarchySummaryResponseData hierarchyDetails = hierarchyMapper.hierarchyToHierarchySummaryResponseData(null);
        assertNull(hierarchyDetails);
    }

    @Test
    public void hierarchyToHierarchyResponseData_Null() {
        HierarchyResponseData hierarchyDetails = hierarchyMapper.hierarchyToHierarchyResponseData(null);
        assertNull(hierarchyDetails);
    }

    @Test
    public void hierarchyRequestDataToHierarchy_Null() {
        Hierarchy hierarchyDetails = hierarchyMapper.hierarchyRequestDataToHierarchy(null);
        assertNull(hierarchyDetails);
    }
}
