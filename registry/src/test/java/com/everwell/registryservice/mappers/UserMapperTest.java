package com.everwell.registryservice.mappers;

import com.everwell.registryservice.models.db.UserAccess;
import com.everwell.registryservice.models.dto.UserDetails;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertNull;

public class UserMapperTest {

    private UserMapper userMapper;

    @Before
    public void before() {
        userMapper = new UserMapperImpl();
    }

    @Test
    public void createUserRequestToUserAccess_Null() {
        UserAccess userDetails = userMapper.createUserRequestToUserAccess(null);
        assertNull(userDetails);
    }

    @Test
    public void userAccessToUserDetails_Null() {
        UserDetails userDetails = userMapper.userAccessToUserDetails(null);
        assertNull(userDetails);
    }
}
