package com.everwell.registryservice.controller;

import com.everwell.registryservice.BaseTest;
import com.everwell.registryservice.constants.FieldValidationMessages;
import com.everwell.registryservice.exceptions.CustomExceptionHandler;
import com.everwell.registryservice.models.db.Config;
import com.everwell.registryservice.models.dto.HierarchyResponseData;
import com.everwell.registryservice.models.http.requests.AddConfigRequest;
import com.everwell.registryservice.models.http.requests.ConfigHierarchyMappingRequest;
import com.everwell.registryservice.models.http.requests.GenerateConfigHierarchyMappingRequest;
import com.everwell.registryservice.models.http.requests.GetConfigHierarchyMappingsRequest;
import com.everwell.registryservice.models.http.response.ConfigHierarchyMappingRecordResponse;
import com.everwell.registryservice.models.http.requests.GetConfigHierarchyMappingsByIdRequest;
import com.everwell.registryservice.models.http.response.ConfigHierarchyMappingBaseResponse;
import com.everwell.registryservice.models.http.response.ConfigResponse;
import com.everwell.registryservice.models.http.response.Response;
import com.everwell.registryservice.service.ConfigService;
import com.everwell.registryservice.service.HierarchyService;
import com.everwell.registryservice.utils.Utils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class ConfigControllerTest extends BaseTest {

    private final String configURI = "/v1/config";
    private final String configHierarchyMappingURI = "/v1/config/hierarchy";
    private final String getConfigsForHierarchyURI = "/v1/config/hierarchy/mappings";
    private final String configHierarchyMappingListURI = "/v1/config/hierarchy/mappingsList";
    private final Long testConfigId = 1L;
    private final String testConfigName = "testConfig";
    private final String testConfigValue = "testValue";
    private final String testConfigType = "testType";
    private MockMvc mockMvc;
    @InjectMocks
    private ConfigController configController;
    @Mock
    private ConfigService configService;
    @Mock
    private HierarchyService hierarchyService;
    @Mock
    private BaseController baseController;

    @Before
    public void setup() {
        mockClient();
        mockMvc = MockMvcBuilders.standaloneSetup(configController)
                .setControllerAdvice(new CustomExceptionHandler())
                .build();
    }

    @Test
    public void addConfig_InvalidRequestMissingConfigName_Error() throws Exception {
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .post(configURI)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(getInvalidAddConfigRequestMissingConfigName()))
                )
                .andExpect(status().isBadRequest())
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertEquals(FieldValidationMessages.CONFIG_NAME_MANDATORY, response.getMessage());
                });
    }

    @Test
    public void addConfig_InvalidRequestMissingConfigValue_Error() throws Exception {
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .post(configURI)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(getInvalidAddConfigRequestMissingConfigValue()))
                )
                .andExpect(status().isBadRequest())
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertEquals(FieldValidationMessages.CONFIG_VALUE_MANDATORY, response.getMessage());
                });
    }

    @Test
    public void addConfig_InvalidRequestMissingConfigType_Error() throws Exception {
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .post(configURI)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(getInvalidAddConfigRequestMissingConfigType()))
                )
                .andExpect(status().isBadRequest())
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertEquals(FieldValidationMessages.CONFIG_TYPE_MANDATORY, response.getMessage());
                });
    }

    @Test
    public void addNewConfig_ValidConfig_Success() throws Exception {
        when(configService.addNewConfig(any(AddConfigRequest.class))).thenReturn(getValidConfigDetails());
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .post(configURI)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(getValidAddConfigRequest()))
                )
                .andExpect(status().isCreated())
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertTrue(response.isSuccess());
                    assertNotNull(response.getData());
                    ConfigResponse configResponse = Utils.convertObject(Utils.asJsonString(response.getData()), ConfigResponse.class);
                    assertEquals(testConfigName, configResponse.getName());
                    assertEquals(testConfigType, configResponse.getType());
                    assertEquals(testConfigValue, configResponse.getDefaultValue());
                });
    }

    @Test
    public void getConfigByName_ValidName_Success() throws Exception {
        when(configService.fetchConfigDetails(testConfigName)).thenReturn(getValidConfigDetails());
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .get(configURI + "/" + testConfigName)
                )
                .andExpect(status().isOk())
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertTrue(response.isSuccess());
                    assertNotNull(response.getData());
                    ConfigResponse configResponseList = Utils.convertObject(Utils.asJsonString(response.getData()), ConfigResponse.class);
                    assertEquals(testConfigName, configResponseList.getName());
                });
    }

    @Test
    public void getAllConfigs_ConfigExist_Success() throws Exception {
        when(configService.getAllConfigs()).thenReturn(Collections.nCopies(2, getValidConfigDetails()));
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .get(configURI)
                )
                .andExpect(status().isOk())
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertTrue(response.isSuccess());
                    assertNotNull(response.getData());
                    List<ConfigResponse> configResponseList = Utils.convertObject(Utils.asJsonString(response.getData()), List.class);
                    assertEquals(2, configResponseList.size());
                });
    }

    @Test
    public void deleteConfig_DeletionUnsuccessful_Failure() throws Exception {
        when(configService.deleteConfig(testConfigName)).thenReturn(false);
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .delete(configURI + "?name=" + testConfigName)
                )
                .andExpect(status().isOk())
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertFalse(response.isSuccess());
                });
    }

    @Test
    public void deleteConfig_DeletionSuccessful_success() throws Exception {
        when(configService.deleteConfig(testConfigName)).thenReturn(true);
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .delete(configURI + "?name=" + testConfigName)
                )
                .andExpect(status().isOk())
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertTrue(response.isSuccess());
                });
    }

    @Test
    public void addNewHierarchyConfig_InvalidRequestMissingConfigName_Error() throws Exception {
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .post(configHierarchyMappingURI)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(getInvalidAddHierarchyConfigRequestMissingConfigName()))
                )
                .andExpect(status().isBadRequest())
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertEquals(FieldValidationMessages.CONFIG_NAME_MANDATORY, response.getMessage());
                });
    }

    @Test
    public void addNewHierarchyConfig_InvalidRequestMissingHierarchyId_Error() throws Exception {
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .post(configHierarchyMappingURI)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(getInvalidAddHierarchyConfigRequestMissingHierarchyId()))
                )
                .andExpect(status().isBadRequest())
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertEquals(FieldValidationMessages.HIERARCHY_ID_MANDATORY, response.getMessage());
                });
    }

    @Test
    public void addNewHierarchyConfig_InvalidRequestMissingConfigValue_Error() throws Exception {
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .post(configHierarchyMappingURI)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(getInvalidAddHierarchyConfigRequestMissingConfigValue()))
                )
                .andExpect(status().isBadRequest())
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertEquals(FieldValidationMessages.CONFIG_VALUE_MANDATORY, response.getMessage());
                });
    }

    @Test
    public void addNewHierarchyConfig_ValidRequest_Success() throws Exception {
        when(hierarchyService.fetchHierarchyById(testConfigId, clientId_Nikshay)).thenReturn(getValidHierarchyResponseData());
        when(configService.addConfigHierarchyMapping(any(GenerateConfigHierarchyMappingRequest.class), any(Long.class))).thenReturn(getValidHierarchyConfigMappingResponse());
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .post(configHierarchyMappingURI)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(getValidAddHierarchyConfigRequest()))
                )
                .andExpect(status().isCreated())
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertTrue(response.isSuccess());
                    assertNotNull(response.getData());
                    ConfigHierarchyMappingRecordResponse configResponse = Utils.convertObject(Utils.asJsonString(response.getData()), ConfigHierarchyMappingRecordResponse.class);
                    assertNotNull(configResponse);
                    assertEquals(testConfigName, configResponse.getConfigName());
                });
    }

    @Test
    public void deleteHierarchyConfig_InvalidRequestMissingHierarchyId_Error() throws Exception {
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .delete(configHierarchyMappingURI)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(getInvalidDeleteHierarchyConfigRequestMissingHierarchyId()))
                )
                .andExpect(status().isBadRequest())
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertEquals(FieldValidationMessages.HIERARCHY_ID_MANDATORY, response.getMessage());
                });
        ;
    }

    @Test
    public void deleteHierarchyConfig_InvalidRequestMissingConfigName_Error() throws Exception {
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .delete(configHierarchyMappingURI)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(getInvalidDeleteHierarchyConfigRequestMissingConfigName()))
                )
                .andExpect(status().isBadRequest())
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertEquals(FieldValidationMessages.CONFIG_NAME_MANDATORY, response.getMessage());
                });
        ;
    }

    @Test
    public void deleteHierarchyConfig_DeletionUnsuccessful_Failure() throws Exception {
        when(hierarchyService.fetchHierarchyById(testConfigId, clientId_Nikshay)).thenReturn(getValidHierarchyResponseData());
        when(configService.deleteConfigHierarchyMapping(any(String.class), any(Long.class))).thenReturn(false);
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .delete(configHierarchyMappingURI)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(getValidAddHierarchyConfigRequest()))
                )
                .andExpect(status().isOk())
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertFalse(response.isSuccess());
                });
    }

    @Test
    public void deleteHierarchyConfig_DeletionSuccessful_success() throws Exception {
        when(hierarchyService.fetchHierarchyById(testConfigId, clientId_Nikshay)).thenReturn(getValidHierarchyResponseData());
        when(configService.deleteConfigHierarchyMapping(any(String.class), any(Long.class))).thenReturn(true);
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .delete(configHierarchyMappingURI)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(getValidAddHierarchyConfigRequest()))
                )
                .andExpect(status().isOk())
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertTrue(response.isSuccess());
                });
    }

    @Test
    public void updateHierarchyConfig_InvalidRequestMissingConfigName_Error() throws Exception {
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .put(configHierarchyMappingURI)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(getInvalidAddHierarchyConfigRequestMissingConfigName()))
                )
                .andExpect(status().isBadRequest())
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertEquals(FieldValidationMessages.CONFIG_NAME_MANDATORY, response.getMessage());
                });
    }

    @Test
    public void updateHierarchyConfig_InvalidRequestMissingHierarchyId_Error() throws Exception {
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .put(configHierarchyMappingURI)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(getInvalidAddHierarchyConfigRequestMissingHierarchyId()))
                )
                .andExpect(status().isBadRequest())
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertEquals(FieldValidationMessages.HIERARCHY_ID_MANDATORY, response.getMessage());
                });
    }

    @Test
    public void updateHierarchyConfig_InvalidRequestMissingConfigValue_Error() throws Exception {
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .put(configHierarchyMappingURI)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(getInvalidAddHierarchyConfigRequestMissingConfigValue()))
                )
                .andExpect(status().isBadRequest())
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertEquals(FieldValidationMessages.CONFIG_VALUE_MANDATORY, response.getMessage());
                });
    }

    @Test
    public void updateHierarchyConfig_ValidRequest_Success() throws Exception {
        when(hierarchyService.fetchHierarchyById(testConfigId, clientId_Nikshay)).thenReturn(getValidHierarchyResponseData());
        when(configService.updateConfigHierarchyMapping(any(GenerateConfigHierarchyMappingRequest.class), any(Long.class))).thenReturn(getValidHierarchyConfigMappingResponse());
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .put(configHierarchyMappingURI)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(getValidAddHierarchyConfigRequest()))
                )
                .andExpect(status().isOk())
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertTrue(response.isSuccess());
                    assertNotNull(response.getData());
                    ConfigHierarchyMappingRecordResponse configResponse = Utils.convertObject(Utils.asJsonString(response.getData()), ConfigHierarchyMappingRecordResponse.class);
                    assertNotNull(configResponse);
                    assertEquals(testConfigName, configResponse.getConfigName());
                });
    }

    @Test
    public void getConfigsForHierarchies_invalidRequestMissingData_Error() throws Exception {
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .post(getConfigsForHierarchyURI)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(getInvalidConfigHierarchiesMappingsRequest()))
                )
                .andExpect(status().isBadRequest())
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertEquals(FieldValidationMessages.REQUEST_MISSING_DATA, response.getMessage());
                });
    }

    @Test
    public void getConfigsForHierarchies_validRequest_Success() throws Exception {
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .post(getConfigsForHierarchyURI)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(getValidConfigHierarchiesMappingsRequest()))
                )
                .andExpect(status().isOk())
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertNotNull(response.getData());
                });
    }

    @Test
    public void getConfigHierarchyMappingsById_InvalidRequestMissingMappingIds_Error() throws Exception {
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .post(configHierarchyMappingListURI)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(new GetConfigHierarchyMappingsByIdRequest()))
                )
                .andExpect(status().isBadRequest())
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertEquals(FieldValidationMessages.CONFIG_MAPPING_ID_MANDATORY, response.getMessage());
                });
    }

    @Test
    public void getConfigHierarchyMappingsById_validRequest_Success() throws Exception {
        when(configService.fetchConfigHierarchyMappingsById(any(List.class))).thenReturn(getValidConfigHierarchyMappingsBaseResponseList());
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .post(configHierarchyMappingListURI)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(getValidGetConfigHierarchyMappingsByIdRequest()))
                )
                .andExpect(status().isOk())
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertNotNull(response.getData());
                });
    }


    private Config getValidConfigDetails() {
        return new Config(testConfigId, testConfigName, testConfigValue, testConfigType, LocalDateTime.now(), false);
    }

    private AddConfigRequest getValidAddConfigRequest() {
        return new AddConfigRequest(testConfigName, testConfigValue, testConfigType);
    }

    private AddConfigRequest getInvalidAddConfigRequestMissingConfigName() {
        return new AddConfigRequest(null, testConfigValue, testConfigType);
    }

    private AddConfigRequest getInvalidAddConfigRequestMissingConfigValue() {
        return new AddConfigRequest(testConfigName, null, testConfigType);
    }

    private AddConfigRequest getInvalidAddConfigRequestMissingConfigType() {
        return new AddConfigRequest(testConfigName, testConfigValue, null);
    }

    private HierarchyResponseData getValidHierarchyResponseData() {
        HierarchyResponseData hierarchyResponseData = new HierarchyResponseData();
        hierarchyResponseData.setId(testConfigId);
        return hierarchyResponseData;
    }

    private GenerateConfigHierarchyMappingRequest getValidAddHierarchyConfigRequest() {
        return new GenerateConfigHierarchyMappingRequest(testConfigId, testConfigName, testConfigType);
    }

    private GenerateConfigHierarchyMappingRequest getInvalidAddHierarchyConfigRequestMissingHierarchyId() {
        return new GenerateConfigHierarchyMappingRequest(null, testConfigName, testConfigType);
    }

    private GenerateConfigHierarchyMappingRequest getInvalidAddHierarchyConfigRequestMissingConfigName() {
        return new GenerateConfigHierarchyMappingRequest(testConfigId, null, testConfigType);
    }

    private GenerateConfigHierarchyMappingRequest getInvalidAddHierarchyConfigRequestMissingConfigValue() {
        return new GenerateConfigHierarchyMappingRequest(testConfigId, testConfigName, null);
    }

    private ConfigHierarchyMappingRequest getInvalidDeleteHierarchyConfigRequestMissingHierarchyId() {
        return new ConfigHierarchyMappingRequest(null, testConfigName);
    }

    private ConfigHierarchyMappingRequest getInvalidDeleteHierarchyConfigRequestMissingConfigName() {
        return new ConfigHierarchyMappingRequest(testConfigId, null);
    }

    private ConfigHierarchyMappingRecordResponse getValidHierarchyConfigMappingResponse() {
        return new ConfigHierarchyMappingRecordResponse(testConfigId, testConfigName, testConfigValue, true, false, null);
    }

    private GetConfigHierarchyMappingsRequest getInvalidConfigHierarchiesMappingsRequest() {
        return new GetConfigHierarchyMappingsRequest();
    }

    private GetConfigHierarchyMappingsRequest getValidConfigHierarchiesMappingsRequest() {
        GetConfigHierarchyMappingsRequest getConfigHierarchyMappingsRequest = getInvalidConfigHierarchiesMappingsRequest();
        Map<Long, List<String>> hierarchies = new HashMap<>();
        hierarchies.put(testConfigId, Collections.singletonList(testConfigName));
        getConfigHierarchyMappingsRequest.setHierarchies(hierarchies);
        return getConfigHierarchyMappingsRequest;
    }

    private GetConfigHierarchyMappingsByIdRequest getValidGetConfigHierarchyMappingsByIdRequest() {
        List<Long> mappingsIds = new ArrayList<Long>(Arrays.asList(1L, 2L, 3L));
        return new GetConfigHierarchyMappingsByIdRequest(mappingsIds);
    }

    private List<ConfigHierarchyMappingBaseResponse> getValidConfigHierarchyMappingsBaseResponseList(){
        return Collections.singletonList(new ConfigHierarchyMappingBaseResponse());
    }

}
