package com.everwell.registryservice.controller;

import com.everwell.registryservice.BaseTest;
import com.everwell.registryservice.constants.Constants;
import com.everwell.registryservice.exceptions.CustomExceptionHandler;
import com.everwell.registryservice.models.db.Deployment;
import com.everwell.registryservice.models.dto.DiseaseTemplate;
import com.everwell.registryservice.models.dto.GenericEvents;
import com.everwell.registryservice.models.http.requests.CreateDeploymentRequest;
import com.everwell.registryservice.models.http.response.*;
import com.everwell.registryservice.service.DeploymentService;
import com.everwell.registryservice.service.DiseaseService;
import com.everwell.registryservice.utils.RedisUtils;
import com.everwell.registryservice.utils.Utils;
import com.everwell.registryservice.models.db.DeploymentLanguageMap;
import com.everwell.registryservice.models.db.Language;
import com.everwell.registryservice.service.LanguageService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.*;
import java.util.stream.Collectors;

import static org.hamcrest.Matchers.anything;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class DeploymentControllerTest extends BaseTest
{
    private MockMvc mockMvc;

    @Mock
    private DeploymentService deploymentService;

    @Mock
    private LanguageService languageService;

    @Mock
    private DiseaseService diseaseService;

    @InjectMocks
    private DeploymentController deploymentController;

    RedisUtils redisUtils;

    @Mock
    RedisTemplate<String, Object> redisTemplate;

    @Mock
    private ValueOperations<String, Object> valueOperations;

    @Mock
    private ApplicationEventPublisher applicationEventPublisher;

    @Before
    public void setup() {
        mockClient();
        mockMvc = MockMvcBuilders.standaloneSetup(deploymentController)
                .setControllerAdvice(new CustomExceptionHandler())
                .build();

        redisUtils = new RedisUtils(redisTemplate);
        when(redisTemplate.opsForValue()).thenReturn(valueOperations);
    }

    @Test
    public void testGetByCode_Success() throws Exception {
        Deployment deployment = new Deployment(
            1L,
            "IND",
            123,
            "India Standard Time",
            "IST",
            "Asia/Kolkata",
            1L,
            null,
            "",
            "India",
            "-",
            "https://help.url",
            "10:00 AM"
        );
        String uri = "/v1/deployments/code/" + deployment.getCode();

        when(deploymentService.getByCode(deployment.getCode())).thenReturn(deployment);
        String cacheKey = Constants.DEPLOYMENT_BY_CODE_CACHE_PREFIX + deployment.getCode() + Constants.CLIENT_ID_CACHE_CONFIG + getClient().getId();
        when(redisUtils.hasKey(cacheKey)).thenReturn(false);
        when(diseaseService.getDiseaseByDeployment(any(), any())).thenReturn(Arrays.asList());

        Language language = new Language(1L, "English", "en" , "English");
        Language language2 = new Language(2L, "Hindi", "hn" , "िन्दी (Hindi)");

        when(languageService.getAll()).thenReturn(Arrays.asList(
            language,
            language2
        ));
        when(languageService.getMappings(deployment.getId())).thenReturn(Arrays.asList(
                new DeploymentLanguageMap(1L, deployment.getId(), language.getId(), null, null),
                new DeploymentLanguageMap(2L, deployment.getId(), language2.getId(), null, null)
        ));
        doNothing().when(applicationEventPublisher).publishEvent(any());
        mockMvc.
                perform(
                    MockMvcRequestBuilders
                        .get(uri)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("true"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.code").value(deployment.getCode()));

        Mockito.verify(applicationEventPublisher, Mockito.times(1)).publishEvent(any(GenericEvents.class));
    }

    @Test
    public void testGetByCode_Success_FromCache() throws Exception {
        Deployment deployment = new Deployment(
                1L,
                "IND",
                123,
                "India Standard Time",
                "IST",
                "Asia/Kolkata",
                1L,
                null,
                "",
                "India",
                "-",
                "https://help.url",
                "10:00 AM"
        );
        String uri = "/v1/deployments/code/" + deployment.getCode();

        when(deploymentService.getByCode(deployment.getCode())).thenReturn(deployment);
        String cacheKey = Constants.DEPLOYMENT_BY_CODE_CACHE_PREFIX + deployment.getCode() + Constants.CLIENT_ID_CACHE_CONFIG + getClient().getId();
        when(redisUtils.hasKey(cacheKey)).thenReturn(true);

        Language language = new Language(1L, "English", "en" , "English");
        Language language2 = new Language(2L, "Hindi", "hn" , "िन्दी (Hindi)");

        DeploymentResponse deploymentResponse = new DeploymentResponse(deployment);
        deploymentResponse.setDefaultLanguage(language);
        deploymentResponse.setAllowedLanguages(Arrays.asList(
                language,
                language2
        ));

        when(valueOperations.get(cacheKey)).thenReturn(Utils.asJsonString(deploymentResponse));
        doNothing().when(applicationEventPublisher).publishEvent(any());
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("true"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.code").value(deployment.getCode()));
        Mockito.verify(applicationEventPublisher, Mockito.times(1)).publishEvent(any(GenericEvents.class));
    }

    @Test
    public void testGetByCode_DeserializationError() throws Exception {
        Deployment deployment = new Deployment(
                1L,
                "IND",
                123,
                "India Standard Time",
                "IST",
                "Asia/Kolkata",
                1L,
                null,
                "",
                "India",
                "-",
                "https://help.url",
                "10:00 AM"
        );
        String uri = "/v1/deployments/code/" + deployment.getCode();

        when(deploymentService.getByCode(deployment.getCode())).thenReturn(deployment);
        String cacheKey = Constants.DEPLOYMENT_BY_CODE_CACHE_PREFIX + deployment.getCode() + Constants.CLIENT_ID_CACHE_CONFIG + getClient().getId();
        when(redisUtils.hasKey(cacheKey)).thenReturn(true);

        when(valueOperations.get(cacheKey)).thenReturn("{\"a\": \"A\"}");

        Language language = new Language(1L, "English", "en" , "English");
        Language language2 = new Language(2L, "Hindi", "hn" , "िन्दी (Hindi)");

        when(languageService.getAll()).thenReturn(Arrays.asList(
                language,
                language2
        ));
        when(languageService.getMappings(deployment.getId())).thenReturn(Arrays.asList(
                new DeploymentLanguageMap(1L, deployment.getId(), language.getId(), null, null),
                new DeploymentLanguageMap(2L, deployment.getId(), language2.getId(), null, null)
        ));
        when(diseaseService.getDiseaseByDeployment(any(), any())).thenReturn(Arrays.asList());
        doNothing().when(applicationEventPublisher).publishEvent(any());
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .header("x-client-id", 1)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("true"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.code").value(deployment.getCode()));
    }


    @Test
    public void testGetByCode_DefaultLanguageNotFound() throws Exception {
        Deployment deployment = new Deployment(
                1L,
                "IND",
                123,
                "India Standard Time",
                "IST",
                "Asia/Kolkata",
                3L,
                null,
                "",
                "India",
                "-",
                "https://help.url",
                "10:00 AM"
        );
        String uri = "/v1/deployments/code/" + deployment.getCode();

        when(deploymentService.getByCode(deployment.getCode())).thenReturn(deployment);

        Language language = new Language(1L, "English", "en" , "English");
        Language language2 = new Language(2L, "Hindi", "hn" , "िन्दी (Hindi)");

        when(languageService.getAll()).thenReturn(Arrays.asList(
                language,
                language2
        ));
        when(languageService.getMappings(deployment.getId())).thenReturn(Arrays.asList(
                new DeploymentLanguageMap(1L, deployment.getId(), language.getId(), null, null),
                new DeploymentLanguageMap(2L, deployment.getId(), language2.getId(), null, null)
        ));
        doNothing().when(applicationEventPublisher).publishEvent(any());
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isNotFound())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("false"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Default language not found"));
    }

    @Test
    public void testGetByCode_LanguageNotFound() throws Exception {
        Deployment deployment = new Deployment(
                1L,
                "IND",
                123,
                "India Standard Time",
                "IST",
                "Asia/Kolkata",
                1L,
                null,
                "",
                "India",
                "-",
                "https://help.url",
                "10:00 AM"
        );
        String uri = "/v1/deployments/code/" + deployment.getCode();

        when(deploymentService.getByCode(deployment.getCode())).thenReturn(deployment);

        Language language = new Language(1L, "English", "en" , "English");
        Language language2 = new Language(2L, "Hindi", "hn" , "िन्दी (Hindi)");

        when(languageService.getAll()).thenReturn(Collections.singletonList(
                language
        ));
        when(languageService.getMappings(deployment.getId())).thenReturn(Arrays.asList(
                new DeploymentLanguageMap(1L, deployment.getId(), language.getId(), null, null),
                new DeploymentLanguageMap(2L, deployment.getId(), language2.getId(), null, null)
        ));
        doNothing().when(applicationEventPublisher).publishEvent(any());
        mockMvc.
            perform(
                    MockMvcRequestBuilders
                            .get(uri)
                            .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isNotFound())
            .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("false"))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Language not found"));
    }

    @Test
    public void testGetById_Success() throws Exception {
        Deployment deployment = new Deployment(
            1L,
            "IND",
            123,
            "India Standard Time",
            "IST",
            "Asia/Kolkata",
            1L,
            null,
            "",
            "India",
            "-",
            "https://help.url",
            "10:00 AM"
        );
        String uri = "/v1/deployments/id/" + deployment.getId();

        when(deploymentService.get(deployment.getId())).thenReturn(deployment);
        String cacheKey = Constants.DEPLOYMENT_BY_ID_CACHE_PREFIX + deployment.getId();
        when(redisUtils.hasKey(cacheKey)).thenReturn(false);

        Language language = new Language(1L, "English", "en" , "English");
        Language language2 = new Language(2L, "Hindi", "hn" , "िन्दी (Hindi)");

        when(languageService.getAll()).thenReturn(Arrays.asList(
                language,
                language2
        ));
        when(languageService.getMappings(deployment.getId())).thenReturn(Arrays.asList(
                new DeploymentLanguageMap(1L, deployment.getId(), language.getId(), null, null),
                new DeploymentLanguageMap(2L, deployment.getId(), language2.getId(), null, null)
        ));
        when(diseaseService.getDiseaseByDeployment(any(), any())).thenReturn(Arrays.asList());
        doNothing().when(applicationEventPublisher).publishEvent(any());
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("true"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.code").value(deployment.getCode()));

        Mockito.verify(applicationEventPublisher, Mockito.times(1)).publishEvent(any(GenericEvents.class));
    }

    @Test
    public void testGetById_Success_FromCache() throws Exception {
        Deployment deployment = new Deployment(
                1L,
                "IND",
                123,
                "India Standard Time",
                "IST",
                "Asia/Kolkata",
                1L,
                null,
                "",
                "India",
                "-",
                "https://help.url",
                "10:00 AM"
        );
        String uri = "/v1/deployments/id/" + deployment.getId();

        when(deploymentService.get(deployment.getId())).thenReturn(deployment);
        String cacheKey = Constants.DEPLOYMENT_BY_ID_CACHE_PREFIX + deployment.getId();
        when(redisUtils.hasKey(cacheKey)).thenReturn(true);

        Language language = new Language(1L, "English", "en" , "English");
        Language language2 = new Language(2L, "Hindi", "hn" , "िन्दी (Hindi)");

        DeploymentResponse deploymentResponse = new DeploymentResponse(deployment);
        deploymentResponse.setDefaultLanguage(language);
        deploymentResponse.setAllowedLanguages(Arrays.asList(
                language,
                language2
        ));

        when(valueOperations.get(cacheKey)).thenReturn(Utils.asJsonString(deploymentResponse));

        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .header("x-client-id", 1)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("true"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.code").value(deployment.getCode()));

        Mockito.verify(applicationEventPublisher, Mockito.times(1)).publishEvent(any(GenericEvents.class));
    }

    @Test
    public void testGetById_DeserializationError() throws Exception {
        Deployment deployment = new Deployment(
                1L,
                "IND",
                123,
                "India Standard Time",
                "IST",
                "Asia/Kolkata",
                1L,
                null,
                "",
                "India",
                "-",
                "https://help.url",
                "10:00 AM"
        );
        String uri = "/v1/deployments/id/" + deployment.getId();

        when(deploymentService.get(deployment.getId())).thenReturn(deployment);
        String cacheKey = Constants.DEPLOYMENT_BY_ID_CACHE_PREFIX + deployment.getId();
        when(redisUtils.hasKey(cacheKey)).thenReturn(true);

        when(valueOperations.get(cacheKey)).thenReturn("{\"a\": \"A\"}");

        Language language = new Language(1L, "English", "en" , "English");
        Language language2 = new Language(2L, "Hindi", "hn" , "िन्दी (Hindi)");

        when(languageService.getAll()).thenReturn(Arrays.asList(
                language,
                language2
        ));
        when(languageService.getMappings(deployment.getId())).thenReturn(Arrays.asList(
                new DeploymentLanguageMap(1L, deployment.getId(), language.getId(), null, null),
                new DeploymentLanguageMap(2L, deployment.getId(), language2.getId(), null, null)
        ));
        when(diseaseService.getDiseaseByDeployment(any(), any())).thenReturn(Arrays.asList());
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("true"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.code").value(deployment.getCode()));
    }

    @Test
    public void testGetAll_Success() throws Exception {
        List<Deployment> deployments = new ArrayList<>(Arrays.asList(
                new Deployment(
                        1L,
                        "IND",
                        123,
                        "India Standard Time",
                        "IST",
                        "Asia/Kolkata",
                        1L,
                        null,
                        "",
                        "India",
                        "-",
                        "https://help.url",
                        "10:00 AM"
                ),
                new Deployment(
                        2L,
                        "AUS",
                        456,
                        "Australia Standard Time",
                        "AST",
                        "Australia/Sydney",
                        1L,
                        null,
                        "",
                        "Australia",
                        "-",
                        "https://help.url",
                        "10:00 AM"
                )
        ));
        String uri = "/v1/deployments";

        when(deploymentService.getAll()).thenReturn(deployments);

        Language language = new Language(1L, "English", "en" , "English");
        Language language2 = new Language(2L, "Hindi", "hn" , "िन्दी (Hindi)");

        when(languageService.getAll()).thenReturn(Arrays.asList(
                language,
                language2
        ));

        when(languageService.getMappings(deployments.get(0).getId())).thenReturn(Arrays.asList(
                new DeploymentLanguageMap(1L, deployments.get(0).getId(), language.getId(), null, null),
                new DeploymentLanguageMap(2L, deployments.get(0).getId(), language2.getId(), null, null)
        ));
        when(languageService.getMappings(deployments.get(1).getId())).thenReturn(Collections.singletonList(
                new DeploymentLanguageMap(1L, deployments.get(1).getId(), language.getId(), null, null)
        ));

        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("true"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data[0].code").value(deployments.get(0).getCode()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data[1].code").value(deployments.get(1).getCode()));

        Mockito.verify(applicationEventPublisher, Mockito.times(1)).publishEvent(any(GenericEvents.class));
    }

    @Test
    public void testGetById_SuccessNoLanguages() throws Exception {
        Deployment deployment = new Deployment(
                1L,
                "IND",
                123,
                "India Standard Time",
                "IST",
                "Asia/Kolkata",
                1L,
                null,
                "",
                "India",
                "-",
                "https://help.url",
                "10:00 AM"
        );
        String uri = "/v1/deployments/id/" + deployment.getId();

        when(deploymentService.get(deployment.getId())).thenReturn(deployment);

        Language language = new Language(1L, "English", "en" , "English");
        Language language2 = new Language(2L, "Hindi", "hn" , "िन्दी (Hindi)");

        when(languageService.getAll()).thenReturn(Arrays.asList(
                language,
                language2
        ));

        when(languageService.getMappings(deployment.getId())).thenReturn(new ArrayList<>());

        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("true"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.code").value(deployment.getCode()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.allowedLanguages", hasSize(0)));

        Mockito.verify(applicationEventPublisher, Mockito.times(1)).publishEvent(any(GenericEvents.class));
    }

    @Test
    public void testCreate_Success() throws Exception {
        CreateDeploymentRequest request = new CreateDeploymentRequest(
        "ABC",
        "Country Name",
        null,
        "Default Time Zone",
        null,
        null,
        null,
        null,
        null,
        null,
        1L,
        null,
        Collections.singletonList(1L),
        new ArrayList<>()
        );
        String uri = "/v1/deployments";

        when(deploymentService.create(any())).thenReturn(new Deployment(request));
        Language language = new Language(1L, "English", "en", "English");

        when(languageService.getAll()).thenReturn(Collections.singletonList(
                language
        ));
        when(diseaseService.getDiseaseByDeployment(any(), any())).thenReturn(Arrays.asList());

        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .content(Utils.asJsonString(request))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("true"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.code").value(request.getCode()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.countryName").value(request.getCountryName()));

        Mockito.verify(applicationEventPublisher, Mockito.times(1)).publishEvent(any(GenericEvents.class));
    }

    @Test
    public void testCreate_Success_with_diseaseId() throws Exception {
        CreateDeploymentRequest request = new CreateDeploymentRequest(
                "ABC",
                "Country Name",
                null,
                "Default Time Zone",
                null,
                null,
                null,
                null,
                null,
                null,
                1L,
                null,
                Collections.singletonList(1L),
                Collections.singletonList(1L)
        );
        String uri = "/v1/deployments";

        when(deploymentService.create(any())).thenReturn(new Deployment(request));
        Language language = new Language(1L, "English", "en" , "English");

        when(languageService.getAll()).thenReturn(Collections.singletonList(
                language
        ));
        when(diseaseService.verifyDiseaseIds(any(), any())).thenReturn(true);
        when(diseaseService.getDiseaseByDeployment(any(), any())).thenReturn(Arrays.asList(new DiseaseTemplate(1L, "TB1")));

        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .content(Utils.asJsonString(request))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("true"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.code").value(request.getCode()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.countryName").value(request.getCountryName()));

        Mockito.verify(applicationEventPublisher, Mockito.times(1)).publishEvent(any(GenericEvents.class));
    }

    @Test
    public void testCreate_DiseaseNotFound() throws Exception {
        CreateDeploymentRequest request = new CreateDeploymentRequest(
                "ABC",
                "Country Name",
                null,
                "Default Time Zone",
                null,
                null,
                null,
                null,
                null,
                null,
                1L,
                null,
                Collections.singletonList(1L),
                Collections.singletonList(1L)
        );
        String uri = "/v1/deployments";

        when(deploymentService.create(any())).thenReturn(new Deployment(request));
        Language language = new Language(1L, "English", "en", "English");

        when(languageService.getAll()).thenReturn(Collections.singletonList(
                language
        ));
        when(diseaseService.verifyDiseaseIds(any(), any())).thenReturn(false);
        when(diseaseService.getDiseaseByDeployment(any(), any())).thenReturn(Arrays.asList());

        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .content(Utils.asJsonString(request))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isNotFound())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("false"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Disease not found"));
    }

    @Test
    public void testCreate_DefaultLanguageNotFound() throws Exception {
        CreateDeploymentRequest request = new CreateDeploymentRequest(
                "ABC",
                "Country Name",
                null,
                "Default Time Zone",
                null,
                null,
                null,
                null,
                null,
                null,
                2L,
                null,
                new ArrayList<>(),
                new ArrayList<>()
        );
        String uri = "/v1/deployments";

        when(deploymentService.create(any())).thenReturn(new Deployment(request));

        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .content(Utils.asJsonString(request))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isNotFound())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("false"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Default language not found"));
    }

    @Test
    public void testCreate_LanguageNotFound() throws Exception {
        CreateDeploymentRequest request = new CreateDeploymentRequest(
                "ABC",
                "Country Name",
                null,
                "Default Time Zone",
                null,
                null,
                null,
                null,
                null,
                null,
                1L,
                null,
                Collections.singletonList(2L),
                new ArrayList<>()
        );
        String uri = "/v1/deployments";

        when(deploymentService.create(any())).thenReturn(new Deployment(request));

        Language language = new Language(1L, "English", "en" , "English");

        when(languageService.getAll()).thenReturn(Collections.singletonList(
                language
        ));

        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .content(Utils.asJsonString(request))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isNotFound())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("false"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Language not found"));
    }

    @Test
    public void testProjectList() throws Exception {
        List<Deployment> deployments = new ArrayList<>(Arrays.asList(
                new Deployment(
                        1L,
                        "IND",
                        123,
                        "India Standard Time",
                        "IST",
                        "Asia/Kolkata",
                        1L,
                        null,
                        "",
                        "India",
                        "-",
                        "https://help.url",
                        "10:00 AM"
                ),
                new Deployment(
                        2L,
                        "AUS",
                        456,
                        "Australia Standard Time",
                        "AST",
                        "Australia/Sydney",
                        1L,
                        null,
                        "",
                        "Australia",
                        "-",
                        "https://help.url",
                        "10:00 AM"
                )
        ));
        String uri = "/v1/projectList";

        String videoServerUrl = "https://videoServerUrl.com";
        String baseUrl = "https://baseUrl.com";

        when(deploymentService.projectList()).thenReturn(deployments.stream().map(deployment -> new ProjectListEntry(deployment, videoServerUrl, baseUrl)).collect(Collectors.toList()));

        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.Success").value("true"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.Data[0].CountryName").value(deployments.get(0).getCountryName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.Data[0].ProjectName").value(deployments.get(0).getProjectName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.Data[0].AskForHelpUrl").value(deployments.get(0).getAskForHelpUrl()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.Data[0].BaseUrl").value(baseUrl))
                .andExpect(MockMvcResultMatchers.jsonPath("$.Data[0].VideoServerUrl").value(videoServerUrl))
                .andExpect(MockMvcResultMatchers.jsonPath("$.Data[1].CountryName").value(deployments.get(1).getCountryName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.Data[1].ProjectName").value(deployments.get(1).getProjectName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.Data[1].AskForHelpUrl").value(deployments.get(1).getAskForHelpUrl()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.Data[1].BaseUrl").value(baseUrl))
                .andExpect(MockMvcResultMatchers.jsonPath("$.Data[1].VideoServerUrl").value(videoServerUrl));
    }

    @Test
    public void testCreate_EmptyCode() throws Exception {
        CreateDeploymentRequest request = new CreateDeploymentRequest(
                null,
                "Country Name",
                null,
                "Default Time Zone",
                null,
                null,
                null,
                null,
                null,
                null,
                1L,
                null,
                Collections.singletonList(1L),
                new ArrayList<>()
        );
        String uri = "/v1/deployments";
        doNothing().when(applicationEventPublisher).publishEvent(any());
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .content(Utils.asJsonString(request))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("false"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Code cannot be empty"));
    }

    @Test
    public void testCreate_InvalidCountry() throws Exception {
        CreateDeploymentRequest request = new CreateDeploymentRequest(
                "ABC",
                null,
                null,
                "Default Time Zone",
                null,
                null,
                null,
                null,
                null,
                null,
                1L,
                null,
                Collections.singletonList(1L),
                new ArrayList<>()
        );
        String uri = "/v1/deployments";
        doNothing().when(applicationEventPublisher).publishEvent(any());
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .content(Utils.asJsonString(request))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("false"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Country name cannot be empty"));
    }

    @Test
    public void testCreate_InvalidTimezone() throws Exception {
        CreateDeploymentRequest request = new CreateDeploymentRequest(
                "ABC",
                "Country",
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                1L,
                null,
                Collections.singletonList(1L),
                new ArrayList<>()
        );
        String uri = "/v1/deployments";
        doNothing().when(applicationEventPublisher).publishEvent(any());
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .content(Utils.asJsonString(request))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("false"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Time zone cannot be empty"));
    }

    @Test
    public void testCreate_InvalidDefaultLanguage() throws Exception {
        CreateDeploymentRequest request = new CreateDeploymentRequest(
                "ABC",
                "Country",
                null,
                "India Standard Time",
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                Collections.singletonList(1L),
                new ArrayList<>()
        );
        String uri = "/v1/deployments";
        doNothing().when(applicationEventPublisher).publishEvent(any());
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .content(Utils.asJsonString(request))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("false"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Default Language cannot be empty"));
    }

    @Test
    public void testCreate_InvalidProjectWithAskForHelpUrl() throws Exception {
        CreateDeploymentRequest request = new CreateDeploymentRequest(
                "ABC",
                "Country",
                null,
                "India Standard Time",
                null,
                null,
                null,
                null,
                "https://ask.for.help",
                null,
                1L,
                null,
                Collections.singletonList(1L),
                new ArrayList<>()
        );
        String uri = "/v1/deployments";
        doNothing().when(applicationEventPublisher).publishEvent(any());
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .content(Utils.asJsonString(request))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("false"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Invalid request"));
    }

    @Test
    public void testCreate_InvalidLanguageIds() throws Exception {
        CreateDeploymentRequest request = new CreateDeploymentRequest(
                "ABC",
                "Country",
                null,
                "India Standard Time",
                null,
                null,
                null,
                null,
                "https://ask.for.help",
                null,
                1L,
                null,
                Collections.singletonList(1L),
                Arrays.asList(1L, 2L)
        );
        String uri = "/v1/deployments";

        when(diseaseService.verifyDiseaseIds(any(), any())).thenReturn(false);
        doNothing().when(applicationEventPublisher).publishEvent(any());

        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .content(Utils.asJsonString(request))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("false"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Invalid request"));
    }

    @Test
    public void testPrefix_Success() throws Exception {
        List<PrefixResponse> prefix = new ArrayList<>(Arrays.asList(
                new PrefixResponse(
                        "IND",
                        "India (+123)"
                ),
                new PrefixResponse(
                        "AUS",
                        "Australia (+456)"
                )
        ));
        String uri = "/v1/prefix";

        when(deploymentService.getPrefix()).thenReturn(prefix);

        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andDo(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertTrue(response.isSuccess());
                    assertNotNull(response.getData());
                    List<PrefixResponse> prefixResponse = Utils.convertObject(Utils.asJsonString(response.getData()), List.class);
                    assertNotNull(prefixResponse);
                    assertEquals(prefixResponse.size(), 2);
                });
    }
}
