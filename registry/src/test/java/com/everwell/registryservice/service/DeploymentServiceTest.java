package com.everwell.registryservice.service;

import com.everwell.registryservice.BaseTest;
import com.everwell.registryservice.exceptions.ConflictException;
import com.everwell.registryservice.exceptions.IllegalArgumentException;
import com.everwell.registryservice.exceptions.NotFoundException;
import com.everwell.registryservice.models.db.Deployment;
import com.everwell.registryservice.models.http.requests.CreateDeploymentRequest;
import com.everwell.registryservice.models.http.response.PrefixResponse;
import com.everwell.registryservice.models.http.response.ProjectListEntry;
import com.everwell.registryservice.repository.DeploymentDiseaseRepository;
import com.everwell.registryservice.repository.DeploymentLanguageRepository;
import com.everwell.registryservice.repository.DeploymentRepository;
import com.everwell.registryservice.service.impl.DeploymentServiceImpl;
import org.junit.Test;
import org.mockito.*;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

public class DeploymentServiceTest extends BaseTest {
    @Spy
    @InjectMocks
    private DeploymentServiceImpl deploymentService;

    @Mock
    private DeploymentRepository deploymentRepository;

    @Mock
    private DeploymentLanguageRepository deploymentLanguageRepository;

    @Mock
    private DeploymentDiseaseRepository deploymentDiseaseRepository;

    @Test
    public void testGetByCode_Success() {
        Deployment deployment = new Deployment(
                1L,
                "IND",
                123,
                "India Standard Time",
                "IST",
                "Asia/Kolkata",
                1L,
                null,
                "",
                "India",
                "-",
                "https://help.url",
                "10:00 AM"
        );

        when(deploymentRepository.getDeploymentByCode(deployment.getCode())).thenReturn(deployment);

        Deployment response = deploymentService.getByCode(deployment.getCode());

        assertEquals(deployment.getCode(), response.getCode());
        assertEquals(deployment.getId(), response.getId());
    }

    @Test(expected = NotFoundException.class)
    public void testGetByCode_NotFound() {
        String deploymentCode = "IND";
        when(deploymentRepository.getDeploymentByCode(deploymentCode)).thenReturn(null);

        deploymentService.getByCode(deploymentCode);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetByCode_EmptyCode() {
        deploymentService.getByCode("");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetByCode_NullCode() {
        deploymentService.getByCode(null);
    }

    @Test
    public void testGetById_Success() {
        Deployment deployment = new Deployment(
                1L,
                "IND",
                123,
                "India Standard Time",
                "IST",
                "Asia/Kolkata",
                1L,
                null,
                "",
                "India",
                "-",
                "https://help.url",
                "10:00 AM"
        );

        when(deploymentRepository.getDeploymentById(deployment.getId())).thenReturn(deployment);

        Deployment response = deploymentService.get(deployment.getId());
        assertEquals(deployment.getCode(), response.getCode());
        assertEquals(deployment.getId(), response.getId());
    }

    @Test(expected = NotFoundException.class)
    public void testGetById_NotFound() {
        Long deploymentId = 1L;
        when(deploymentRepository.getDeploymentById(deploymentId)).thenReturn(null);

        deploymentService.get(deploymentId);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetById_InvalidId() {
        deploymentService.get(0L);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetById_NullId() {
        deploymentService.get(null);
    }

    @Test
    public void testGetAll_Success() {
        List<Deployment> deployments = new ArrayList<>(Arrays.asList(
                new Deployment(
                        1L,
                        "IND",
                        123,
                        "India Standard Time",
                        "IST",
                        "Asia/Kolkata",
                        1L,
                        null,
                        "",
                        "India",
                        "-",
                        "https://help.url",
                        "10:00 AM"
                ),
                new Deployment(
                        2L,
                        "AUS",
                        456,
                        "Australia Standard Time",
                        "AST",
                        "Australia/Sydney",
                        1L,
                        null,
                        "",
                        "Australia",
                        "-",
                        "https://help.url",
                        "10:00 AM"
                )
        ));

        when(deploymentRepository.findAll()).thenReturn(deployments);

        List<Deployment> response = deploymentService.getAll();
        assertEquals(deployments.get(0).getCode(), response.get(0).getCode());
        assertEquals(deployments.get(0).getId(), response.get(0).getId());
        assertEquals(deployments.get(1).getCode(), response.get(1).getCode());
        assertEquals(deployments.get(1).getId(), response.get(1).getId());
    }

    @Test
    public void testCreateDeployment_Success() {
        CreateDeploymentRequest request = new CreateDeploymentRequest(
            "ABC",
            "Country Name",
            null,
            "Default Time Zone",
            null,
            null,
            null,
            null,
            null,
            null,
            1L,
            null,
            Collections.singletonList(1L),
            Collections.singletonList(1L)
        );

        when(deploymentRepository.existsAllByCode(request.getCode())).thenReturn(false);
        when(deploymentRepository.save(any())).thenReturn(new Deployment(request));
        when(deploymentLanguageRepository.saveAll(any())).thenReturn(new ArrayList<>());
        when(deploymentDiseaseRepository.saveAll(any())).thenReturn(new ArrayList<>());

        Deployment response = deploymentService.create(request);
        assertEquals(request.getCode(), response.getCode());
        assertEquals(request.getCountryName(), response.getCountryName());
        assertEquals(request.getTimeZone(), response.getTimeZone());
    }

    @Test(expected = ConflictException.class)
    public void testCreateDeployment_AlreadyExists() {
        CreateDeploymentRequest request = new CreateDeploymentRequest(
            "ABC",
            "Country Name",
            null,
            "Default Time Zone",
            null,
            null,
            null,
            null,
            null,
            null,
            1L,
            null,
            new ArrayList<>(),
            new ArrayList<>()
        );

        when(deploymentRepository.existsAllByCode(request.getCode())).thenReturn(true);

        deploymentService.create(request);
    }

    @Test(expected = ConflictException.class)
    public void testCreateDeployment_AlreadyExistsCountryProjectCombination() {
        CreateDeploymentRequest request = new CreateDeploymentRequest(
                "ABC",
                "Country Name",
                null,
                "Default Time Zone",
                null,
                null,
                null,
                "Project Name",
                null,
                null,
                1L,
                null,
                new ArrayList<>(),
                new ArrayList<>()
        );

        when(deploymentRepository.existsAllByCode(request.getCode())).thenReturn(false);
        when(deploymentRepository.existsAllByCountryNameAndProjectName(request.getCountryName(), request.getProjectName())).thenReturn(true);

        deploymentService.create(request);
    }

    @Test
    public void testProjectList() {
        List<Deployment> deployments = new ArrayList<>(Arrays.asList(
                new Deployment(
                        1L,
                        "IND",
                        123,
                        "India Standard Time",
                        "IST",
                        "Asia/Kolkata",
                        1L,
                        null,
                        "",
                        "India",
                        "-",
                        "https://help.url",
                        "10:00 AM"
                ),
                new Deployment(
                        2L,
                        "AUS",
                        456,
                        "Australia Standard Time",
                        "AST",
                        "Australia/Sydney",
                        1L,
                        null,
                        "",
                        "Australia",
                        "-",
                        "https://help.url",
                        "10:00 AM"
                )
        ));

        when(deploymentRepository.getAllByCountryNameNotNullAndProjectNameNotNull()).thenReturn(deployments);

        String baseUrl = "https://baseUrl.com";
        String videoServerUrl = "https://videoServerUrl.com";
        ReflectionTestUtils.setField(deploymentService, "clientBaseUrl", baseUrl);
        ReflectionTestUtils.setField(deploymentService, "videoServerUrl", videoServerUrl);

        List<ProjectListEntry> response = deploymentService.projectList();
        assertEquals(deployments.get(0).getCountryName(), response.get(0).getCountryName());
        assertEquals(deployments.get(0).getProjectName(), response.get(0).getProjectName());
        assertEquals(deployments.get(0).getAskForHelpUrl(), response.get(0).getAskForHelpUrl());
        assertEquals(baseUrl, response.get(0).getBaseUrl());
        assertEquals(videoServerUrl, response.get(0).getVideoServerUrl());

        assertEquals(deployments.get(1).getCountryName(), response.get(1).getCountryName());
        assertEquals(deployments.get(1).getProjectName(), response.get(1).getProjectName());
        assertEquals(deployments.get(1).getAskForHelpUrl(), response.get(1).getAskForHelpUrl());
        assertEquals(baseUrl, response.get(1).getBaseUrl());
        assertEquals(videoServerUrl, response.get(1).getVideoServerUrl());
    }

    @Test
    public void testPrefix() {
        List<Deployment> deployments = new ArrayList<>(Arrays.asList(
                new Deployment(
                        1L,
                        "IND",
                        123,
                        "India Standard Time",
                        "IST",
                        "Asia/Kolkata",
                        1L,
                        null,
                        "",
                        "India",
                        "-",
                        "https://help.url",
                        "10:00 AM"
                ),
                new Deployment(
                        2L,
                        "AUS",
                        456,
                        "Australia Standard Time",
                        "AST",
                        "Australia/Sydney",
                        1L,
                        null,
                        "",
                        "Australia",
                        "-",
                        "https://help.url",
                        "10:00 AM"
                )
        ));

        when(deploymentRepository.findAll()).thenReturn(deployments);

        List<PrefixResponse> response = deploymentService.getPrefix();
        assertEquals(deployments.get(0).getCode(), response.get(0).getKey());
        assertEquals(String.format("%s (+%d)",deployments.get(0).getCountryName(), deployments.get(0).getPhonePrefix()), response.get(0).getValue());
        assertEquals(deployments.get(1).getCode(), response.get(1).getKey());
        assertEquals(String.format("%s (+%d)",deployments.get(1).getCountryName(), deployments.get(1).getPhonePrefix()), response.get(1).getValue());

    }
}
