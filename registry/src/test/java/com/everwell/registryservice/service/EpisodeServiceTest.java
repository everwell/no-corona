package com.everwell.registryservice.service;

import com.everwell.registryservice.BaseTest;
import com.everwell.registryservice.constants.EpisodeRoutes;
import com.everwell.registryservice.exceptions.InternalServerErrorException;
import com.everwell.registryservice.exceptions.UnauthorizedException;
import com.everwell.registryservice.models.db.Client;
import com.everwell.registryservice.models.dto.DiseaseTemplate;
import com.everwell.registryservice.models.http.requests.episode.DiseaseTemplateRequest;
import com.everwell.registryservice.models.http.requests.episode.EpisodeHeaderSearchRequest;
import com.everwell.registryservice.models.http.requests.episode.EpisodeSearchRequest;
import com.everwell.registryservice.models.http.response.ClientResponse;
import com.everwell.registryservice.models.http.response.Response;
import com.everwell.registryservice.models.http.response.episode.HeaderSearchResponse;
import com.everwell.registryservice.service.impl.EpisodeServiceImpl;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;

import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.*;
import org.springframework.http.*;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

public class EpisodeServiceTest extends BaseTest
{
    @Spy
    @InjectMocks
    private EpisodeServiceImpl episodeService;

    @Mock
    RestTemplate restTemplate;

    @Mock
    public static ClientService clientService;

    @Before
    public void setUp() throws IOException {
        ReflectionTestUtils.setField(episodeService, "episodeServiceURL", "localhost");
        when(clientService.getClientByTokenOrDefault()).thenReturn(new Client(1L,"test","test",null,1L, "www.google.com", ssoUrl, null ));
        episodeService.init();
    }

    @Test
    public void authorization_test_success() {
        Client testClient = new Client(1L,"test","test",null,1L, "www.google.com", ssoUrl, null);
        when(clientService.getClientByToken()).thenReturn(testClient);

        ResponseEntity res = new ResponseEntity
                (new Response<ClientResponse>
                    (new ClientResponse(new Client(), "AUTH_TOKEN", 1L, "www.google.com"), ""),
                        HttpStatus.OK);

        Mockito.when(restTemplate.exchange(eq("localhost" + EpisodeRoutes.GET_TOKEN.getPath()), eq(HttpMethod.GET), ArgumentMatchers.any(), (Class<Object>) any()))
                .thenReturn(res);

        episodeService.authenticate();
        assertEquals(episodeService.AUTH_TOKEN, "AUTH_TOKEN");
    }

    @Test(expected = UnauthorizedException.class)
    public void authorization_test_failure() {
        Client testClient = new Client(1L,"test","test",null,1L, "www.google.com", ssoUrl, null);
        when(clientService.getClientByToken()).thenReturn(testClient);

        ResponseEntity res = new ResponseEntity(null, HttpStatus.OK);

        Mockito.when(restTemplate.exchange(eq("localhost" + EpisodeRoutes.GET_TOKEN.getPath()), eq(HttpMethod.GET), ArgumentMatchers.any(), (Class<Object>) any()))
                .thenReturn(res);

        episodeService.authenticate();
    }

    @Test
    public void getAllDiseases_test_success() {
        DiseaseTemplateRequest testResponse = new DiseaseTemplateRequest(Arrays.asList(new DiseaseTemplate(1L, "TB1")));

        ResponseEntity res = new ResponseEntity(new Response<DiseaseTemplateRequest>(testResponse, ""), HttpStatus.OK);

        Mockito.when(restTemplate.exchange(eq("localhost" + EpisodeRoutes.GET_ALL_DISEASES.getPath()), eq(HttpMethod.GET), ArgumentMatchers.any(), (Class<Object>) any()))
                .thenReturn(res);
        when(clientService.getClientByToken()).thenReturn(new Client(1L,"test","test",null ));
        DiseaseTemplateRequest response = episodeService.getAllDiseases();
        assertEquals(response.getDiseases().size(), 1);
    }

    @Test
    public void getAllDiseases_test() {
        DiseaseTemplateRequest testResponse = new DiseaseTemplateRequest(Arrays.asList(new DiseaseTemplate(1L, "TB1")));

        ResponseEntity res = new ResponseEntity(new Response<DiseaseTemplateRequest>(false, testResponse, ""), HttpStatus.OK);

        Mockito.when(restTemplate.exchange(eq("localhost" + EpisodeRoutes.GET_ALL_DISEASES.getPath()), eq(HttpMethod.GET), ArgumentMatchers.any(), (Class<Object>) any()))
                .thenReturn(res);
        when(clientService.getClientByToken()).thenReturn(new Client(1L,"test","test",null ));
        DiseaseTemplateRequest response = episodeService.getAllDiseases();
        assertEquals(response.getDiseases().size(), 1);
    }

    @Test(expected = UnauthorizedException.class)
    public void getAllDiseases_test_failure_authentication_failure() {
        DiseaseTemplateRequest testResponse = new DiseaseTemplateRequest(Arrays.asList(new DiseaseTemplate(1L, "TB1")));

        ResponseEntity res = new ResponseEntity(null, HttpStatus.OK);

        Mockito.when(restTemplate.exchange(eq("localhost" + EpisodeRoutes.GET_ALL_DISEASES.getPath()), eq(HttpMethod.GET), ArgumentMatchers.any(), (Class<Object>) any()))
                .thenReturn(null);

        DiseaseTemplateRequest response = episodeService.getAllDiseases();
        assertEquals(response.getDiseases().size(), 1);
    }

    @Test(expected = NullPointerException.class)
    public void getAllDiseases_test_failure_authentication_success() {
        DiseaseTemplateRequest testResponse = new DiseaseTemplateRequest(Arrays.asList(new DiseaseTemplate(1L, "TB1")));

        ResponseEntity res = new ResponseEntity(null, HttpStatus.OK);

        Mockito.when(restTemplate.exchange(eq("localhost" + EpisodeRoutes.GET_ALL_DISEASES.getPath()), eq(HttpMethod.GET), ArgumentMatchers.any(), (Class<Object>) any()))
                .thenReturn(null);

        Client testClient = new Client(1L,"test","test",null,1L, "www.google.com", ssoUrl, null);
        when(clientService.getClientByToken()).thenReturn(testClient);

        ResponseEntity authenticationRes = new ResponseEntity
                (new Response<ClientResponse>
                        (new ClientResponse(new Client(), "AUTH_TOKEN", 1L, "www.google.com"), ""),
                        HttpStatus.OK);

        Mockito.when(restTemplate.exchange(eq("localhost" + EpisodeRoutes.GET_TOKEN.getPath()), eq(HttpMethod.GET), ArgumentMatchers.any(), (Class<Object>) any()))
                .thenReturn(authenticationRes);

        DiseaseTemplateRequest response = episodeService.getAllDiseases();
        assertEquals(response.getDiseases().size(), 1);
    }

    @Test(expected = NullPointerException.class)
    public void getAllDiseasesTestAuthenticationSuccessResponseNull() {
        DiseaseTemplateRequest testResponse = new DiseaseTemplateRequest(Arrays.asList(new DiseaseTemplate(1L, "TB1")));

        ResponseEntity res = new ResponseEntity(null, HttpStatus.OK);

        Mockito.when(restTemplate.exchange(eq("localhost" + EpisodeRoutes.GET_ALL_DISEASES.getPath()), eq(HttpMethod.GET), ArgumentMatchers.any(), (Class<Object>) any()))
                .thenReturn(null);

        Client testClient = new Client(2L,"test","test",null,1L, "www.google.com", ssoUrl, null);
        when(clientService.getClientByToken()).thenReturn(testClient);

        ResponseEntity authenticationRes = new ResponseEntity
                (new Response<ClientResponse>
                        (new ClientResponse(new Client(), "AUTH_TOKEN", 1L, "www.google.com"), ""),
                        HttpStatus.OK);

        Mockito.when(restTemplate.exchange(eq("localhost" + EpisodeRoutes.GET_TOKEN.getPath()), eq(HttpMethod.GET), ArgumentMatchers.any(), (Class<Object>) any()))
                .thenReturn(authenticationRes);

        DiseaseTemplateRequest response = episodeService.getAllDiseases();
        assertEquals(response.getDiseases().size(), 1);
    }

    @Test
    public void headerSearch_success() {
        EpisodeHeaderSearchRequest episodeSearchRequest = new EpisodeHeaderSearchRequest();

        ResponseEntity res = new ResponseEntity(new Response<HeaderSearchResponse>
                (new HeaderSearchResponse(1L, new ArrayList<>()), ""),
                HttpStatus.OK);

        Mockito.when(restTemplate.exchange(eq("localhost" + EpisodeRoutes.POST_HEADER_SEARCH.getPath()), eq(HttpMethod.POST), ArgumentMatchers.any(), (Class<Object>) any()))
                .thenReturn(res);

        HeaderSearchResponse response = episodeService.headerSearch(episodeSearchRequest);
        assertNotNull(response);
        assertEquals(1L, response.getTotalHits().longValue());
    }

    @Test(expected = InternalServerErrorException.class)
    public void headerSearch_ISEFailure() {
        EpisodeHeaderSearchRequest episodeSearchRequest = new EpisodeHeaderSearchRequest();

        Mockito.when(restTemplate.exchange(eq("localhost" + EpisodeRoutes.POST_HEADER_SEARCH.getPath()), eq(HttpMethod.POST), ArgumentMatchers.any(), (Class<Object>) any()))
                .thenThrow(new InternalServerErrorException("error in episode api"));

        HeaderSearchResponse response = episodeService.headerSearch(episodeSearchRequest);
    }

    @Test
    public void headerSearch_Unauthorized() {
        EpisodeHeaderSearchRequest episodeSearchRequest = new EpisodeHeaderSearchRequest();

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Authorization", "Bearer " + "AUTH_TOKEN1");
        httpHeaders.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON.toString());
        httpHeaders.add("X-CLIENT-ID", "1");
        HttpEntity authenticatedEntity = new HttpEntity<>(episodeSearchRequest, httpHeaders);

        ResponseEntity res = new ResponseEntity(new Response<HeaderSearchResponse>
                (new HeaderSearchResponse(1L, new ArrayList<>()), ""),
                HttpStatus.OK);

        Mockito.when(restTemplate.exchange(eq("localhost" + EpisodeRoutes.POST_HEADER_SEARCH.getPath()), eq(HttpMethod.POST), ArgumentMatchers.any(), (Class<Object>) any()))
                .thenThrow(new HttpClientErrorException(HttpStatus.UNAUTHORIZED));
        Mockito.when(restTemplate.exchange(eq("localhost" + EpisodeRoutes.POST_HEADER_SEARCH.getPath()), eq(HttpMethod.POST), eq(authenticatedEntity), (Class<Object>) any()))
                .thenReturn(res);

        Client testClient = new Client(1L,"test","test",null,1L, "www.google.com", ssoUrl, null);
        when(clientService.getClientByToken()).thenReturn(testClient);

        ResponseEntity authenticationRes = new ResponseEntity
                (new Response<ClientResponse>
                        (new ClientResponse(new Client(), "AUTH_TOKEN1", 1L, "www.google.com"), ""),
                        HttpStatus.OK);

        Mockito.when(restTemplate.exchange(eq("localhost" + EpisodeRoutes.GET_TOKEN.getPath()), eq(HttpMethod.GET), ArgumentMatchers.any(), (Class<Object>) any()))
                .thenReturn(authenticationRes);

        HeaderSearchResponse response = episodeService.headerSearch(episodeSearchRequest);
        Mockito.verify(episodeService, Mockito.times(1)).authenticate();
        assertNotNull(response);
        assertEquals(1L, response.getTotalHits().longValue());
    }

}
