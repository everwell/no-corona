package com.everwell.registryservice.service;


import com.everwell.registryservice.BaseTest;
import com.everwell.registryservice.exceptions.NotFoundException;
import com.everwell.registryservice.models.db.DeploymentLanguageMap;
import com.everwell.registryservice.models.db.Language;
import com.everwell.registryservice.repository.DeploymentLanguageRepository;
import com.everwell.registryservice.repository.LanguageRepository;
import com.everwell.registryservice.service.impl.LanguageServiceImpl;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

public class LanguageServiceTest extends BaseTest {

    @Spy
    @InjectMocks
    private LanguageServiceImpl languageService;

    @Mock
    private LanguageRepository languageRepository;

    @Mock
    private DeploymentLanguageRepository deploymentLanguageRepository;

    @Test
    public void testGetAll()
    {
        List<Language> languages = new ArrayList<>(Arrays.asList(
                new Language(1L, "English", "en", "English"),
                new Language(2L, "Hindi", "hi", "hi")
        ));

        when(languageRepository.findAll()).thenReturn(languages);

        List<Language> response = languageService.getAll();
        assertEquals(response.size(), languages.size());
        assertEquals(response.get(0).getId(), languages.get(0).getId());
        assertEquals(response.get(1).getId(), languages.get(1).getId());
    }

    @Test
    public void testGet_Success()
    {
        Language language = new Language(1L, "English", "en", "English");

        when(languageRepository.findById(language.getId())).thenReturn(java.util.Optional.of(language));
        Language response = languageService.get(language.getId());

        assertEquals(language.getName(), response.getName());
        assertEquals(language.getTranslationKey(), response.getTranslationKey());
    }

    @Test(expected = NotFoundException.class)
    public void testGet_NotFound()
    {
        Long languageId = 1L;
        when(languageRepository.findById(languageId)).thenReturn(Optional.empty());
        Language response = languageService.get(languageId);
    }

    @Test
    public void testGetMappings()
    {
        Long deploymentId = 1L;
        List<DeploymentLanguageMap> maps = Arrays.asList(
            new DeploymentLanguageMap(1L, deploymentId, 1L, null, null),
            new DeploymentLanguageMap(2L, deploymentId, 2L, null, null)
        );

        when(deploymentLanguageRepository.getAllByDeploymentIdAndStoppedAtIsNull(deploymentId)).thenReturn(maps);
        List<DeploymentLanguageMap> response = languageService.getMappings(deploymentId);
        assertEquals(maps.size(), response.size());
        assertEquals(maps.get(0).getId(), response.get(0).getId());
        assertEquals(maps.get(0).getLanguageId(), response.get(0).getLanguageId());
        assertEquals(deploymentId, response.get(0).getDeploymentId());
        assertEquals(maps.get(1).getId(), response.get(1).getId());
        assertEquals(maps.get(1).getLanguageId(), response.get(1).getLanguageId());
        assertEquals(deploymentId, response.get(1).getDeploymentId());
    }
}
