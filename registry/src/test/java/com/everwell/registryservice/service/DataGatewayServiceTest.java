package com.everwell.registryservice.service;

import com.everwell.registryservice.BaseTest;
import com.everwell.registryservice.constants.Constants;
import com.everwell.registryservice.exceptions.ValidationException;
import com.everwell.registryservice.models.dto.GenericExchange;
import com.everwell.registryservice.models.dto.ParameterExchange;
import com.everwell.registryservice.models.http.response.Response;
import com.everwell.registryservice.models.http.response.ins.TemplateResponse;
import com.everwell.registryservice.service.impl.DataGatewayServiceImpl;
import com.everwell.registryservice.utils.Utils;
import com.fasterxml.jackson.core.type.TypeReference;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.*;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

public class DataGatewayServiceTest extends BaseTest {

    @Spy
    @InjectMocks
    DataGatewayServiceImpl dataGatewayService;

    @Mock
    RestTemplate restTemplate;

    private static String clientId = "1";

    @Before
    public void init() {
        ReflectionTestUtils.setField(dataGatewayService, "dataGatewayURL", "http://test:8080");
        ReflectionTestUtils.setField(dataGatewayService, "username", "test");
        ReflectionTestUtils.setField(dataGatewayService, "password", "test");
    }

    @Test
    public void test_authenticate() throws IOException {
        Mockito.when(restTemplate.exchange(anyString(), any(HttpMethod.class), any(), ArgumentMatchers.<Class<Map<String, Object>>>any()))
                .thenReturn(new ResponseEntity<Map<String, Object>>(getAuthenticateResponse(), HttpStatus.OK));

        dataGatewayService.authenticate();
        Mockito.verify(restTemplate, Mockito.times(1)).exchange(anyString(), any(HttpMethod.class), any(), ArgumentMatchers.<Class<Map<String, Object>>>any());
    }

    @Test
    public void test_headers() {
        HttpHeaders headers = dataGatewayService.headers(clientId);

        Assert.assertTrue(headers.containsKey("Authorization"));
        Assert.assertTrue(headers.containsKey(Constants.ACCEPT));
        Assert.assertTrue(headers.containsKey(Constants.CONTENT_TYPE));
        Assert.assertTrue(headers.containsKey(Constants.CLIENT_ID));
    }

    @Test
    public void testExchangeSuccess() {
        TemplateResponse templateResponse = new TemplateResponse(1L, "test", "test", "test", "test");
        ResponseEntity<Response<TemplateResponse>> response = new ResponseEntity<>(new Response<>(true, templateResponse), HttpStatus.OK);
        GenericExchange exchange = new GenericExchange(clientId, "test", HttpMethod.POST, new ParameterizedTypeReference<Response<String>>() {
        }, null);

        when(restTemplate.exchange(anyString(), any(HttpMethod.class), any(), any(ParameterizedTypeReference.class)))
                .thenReturn(response);

        ResponseEntity<Response<TemplateResponse>> apiResponse = dataGatewayService.genericExchange(exchange);

        Mockito.verify(dataGatewayService, Mockito.times(0)).authenticate();
        Assert.assertTrue(apiResponse.getBody().isSuccess());
    }

    @Test
    public void testExchangeAuthError() {
        HttpClientErrorException forbiddenException = HttpClientErrorException.Forbidden.create(HttpStatus.FORBIDDEN, null, null, null, null);
        GenericExchange exchange = new GenericExchange(clientId, "test", HttpMethod.POST, new ParameterizedTypeReference<Response<String>>() {
        }, null);

        when(restTemplate.exchange(anyString(), any(HttpMethod.class), any(), any(ParameterizedTypeReference.class)))
                .thenThrow(forbiddenException);
        doNothing().when(dataGatewayService).authenticate();

        dataGatewayService.genericExchange(exchange);

        Mockito.verify(dataGatewayService, Mockito.times(4)).authenticate();
    }

    @Test
    public void testExchangeCustomError() {
        HttpClientErrorException exception = HttpClientErrorException.BadRequest.create(HttpStatus.BAD_REQUEST, "Bad Request", null, "{\"success\":\"false\"}".getBytes(StandardCharsets.UTF_8), null);
        GenericExchange exchange = new GenericExchange(clientId, "test", HttpMethod.POST, new ParameterizedTypeReference<Response<String>>() {
        }, null);

        when(restTemplate.exchange(anyString(), any(HttpMethod.class), any(), any(ParameterizedTypeReference.class)))
                .thenThrow(exception);

        ResponseEntity<Response<TemplateResponse>> apiResponse = dataGatewayService.genericExchange(exchange);

        Assert.assertFalse(apiResponse.getBody().isSuccess());
    }

    @Test(expected = ValidationException.class)
    public void testExchangeIOError() {
        HttpClientErrorException exception = HttpClientErrorException.BadRequest.create(HttpStatus.BAD_REQUEST, "Bad Request", null, null, null);
        GenericExchange exchange = new GenericExchange(clientId, "test", HttpMethod.POST, new ParameterizedTypeReference<Response<String>>() {
        }, null);

        when(restTemplate.exchange(anyString(), any(HttpMethod.class), any(), any(ParameterizedTypeReference.class)))
                .thenThrow(exception);

        dataGatewayService.genericExchange(exchange);
    }

    @Test
    public void testPostExchange() {
        TemplateResponse templateResponse = new TemplateResponse(1L, "test", "test", "test", "test");
        ResponseEntity<Response<TemplateResponse>> response = new ResponseEntity<>(new Response<>(true, templateResponse), HttpStatus.OK);
        ParameterExchange exchange = new ParameterExchange(clientId, "test", "test", new ParameterizedTypeReference<Response<TemplateResponse>>() {
        });

        when(restTemplate.exchange(anyString(), any(HttpMethod.class), any(), any(ParameterizedTypeReference.class)))
                .thenReturn(response);

        ResponseEntity<Response<TemplateResponse>> apiResponse = dataGatewayService.postExchange(exchange);

        Mockito.verify(dataGatewayService, Mockito.times(0)).authenticate();
        Assert.assertTrue(apiResponse.getBody().isSuccess());
    }

    @Test
    public void testPutExchange() {
        TemplateResponse templateResponse = new TemplateResponse(1L, "test", "test", "test", "test");
        ResponseEntity<Response<TemplateResponse>> response = new ResponseEntity<>(new Response<>(true, templateResponse), HttpStatus.OK);
        ParameterExchange exchange = new ParameterExchange(clientId, "test", "test", new ParameterizedTypeReference<Response<TemplateResponse>>() {
        });

        when(restTemplate.exchange(anyString(), any(HttpMethod.class), any(), any(ParameterizedTypeReference.class)))
                .thenReturn(response);

        ResponseEntity<Response<TemplateResponse>> apiResponse = dataGatewayService.putExchange(exchange);

        Mockito.verify(dataGatewayService, Mockito.times(0)).authenticate();
        Assert.assertTrue(apiResponse.getBody().isSuccess());
    }

    @Test
    public void testGetExchange() {
        Map<String, Object> queryParams = new HashMap<>();
        queryParams.put("test", "test@123");
        TemplateResponse templateResponse = new TemplateResponse(1L, "test", "test", "test", "test");
        ResponseEntity<Response<TemplateResponse>> response = new ResponseEntity<>(new Response<>(true, templateResponse), HttpStatus.OK);
        ParameterExchange exchange = new ParameterExchange(clientId, "test", new ParameterizedTypeReference<Response<TemplateResponse>>() {
        }, queryParams, 1L);

        when(restTemplate.exchange(anyString(), any(HttpMethod.class), any(), any(ParameterizedTypeReference.class)))
                .thenReturn(response);

        ResponseEntity<Response<TemplateResponse>> apiResponseNoQuery = dataGatewayService.getExchange(exchange);

        Mockito.verify(dataGatewayService, Mockito.times(0)).authenticate();
        Assert.assertTrue(apiResponseNoQuery.getBody().isSuccess());

        exchange.setPathVariable(null);
        ResponseEntity<Response<TemplateResponse>> apiResponseNoPath = dataGatewayService.getExchange(exchange);

        Mockito.verify(dataGatewayService, Mockito.times(0)).authenticate();
        Assert.assertTrue(apiResponseNoPath.getBody().isSuccess());
    }

    @Test
    public void testDeleteExchange() {
        Map<String, Object> queryParams = new HashMap<>();
        queryParams.put("test", "test@123");
        TemplateResponse templateResponse = new TemplateResponse(1L, "test", "test", "test", "test");
        ResponseEntity<Response<TemplateResponse>> response = new ResponseEntity<>(new Response<>(true, templateResponse), HttpStatus.OK);
        ParameterExchange exchange = new ParameterExchange(clientId, "test", new ParameterizedTypeReference<Response<TemplateResponse>>() {
        }, 1L);

        when(restTemplate.exchange(anyString(), any(HttpMethod.class), any(), any(ParameterizedTypeReference.class)))
                .thenReturn(response);

        ResponseEntity<Response<TemplateResponse>> apiResponsePath = dataGatewayService.deleteExchange(exchange);

        Mockito.verify(dataGatewayService, Mockito.times(0)).authenticate();
        Assert.assertTrue(apiResponsePath.getBody().isSuccess());

        exchange.setPathVariable(null);
        ResponseEntity<Response<TemplateResponse>> apiResponseNoPath = dataGatewayService.deleteExchange(exchange);

        Mockito.verify(dataGatewayService, Mockito.times(0)).authenticate();
        Assert.assertTrue(apiResponseNoPath.getBody().isSuccess());
    }

    private Map<String, Object> getAuthenticateResponse() throws IOException {
        Map<String, Object> map = new HashMap<>();
        String jsonData = "{\"data\":\"eteaiuraaaxarrarrtieacffr\", \"code\": \"200 OK\",\"message\": \"Successful Response.\"}";
        return Utils.jsonToObject(jsonData, new TypeReference<Map<String, Object>>() {
        });
    }

}
