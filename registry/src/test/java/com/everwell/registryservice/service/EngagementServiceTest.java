package com.everwell.registryservice.service;

import com.everwell.registryservice.BaseTest;
import com.everwell.registryservice.exceptions.NotFoundException;
import com.everwell.registryservice.exceptions.ValidationException;
import com.everwell.registryservice.models.db.Engagement;
import com.everwell.registryservice.models.dto.EngagementDetails;
import com.everwell.registryservice.models.dto.HierarchyResponseData;
import com.everwell.registryservice.models.dto.LanguageMapping;
import com.everwell.registryservice.models.http.requests.UpdateLanguageRequest;
import com.everwell.registryservice.models.http.response.LanguagesResponse;
import com.everwell.registryservice.models.http.response.Response;
import com.everwell.registryservice.models.http.response.ins.LanguageResponse;
import com.everwell.registryservice.repository.EngagementRepository;
import com.everwell.registryservice.service.impl.EngagementServiceImpl;
import com.everwell.registryservice.service.impl.HierarchyServiceImpl;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;


public class EngagementServiceTest extends BaseTest {
    @Spy
    @InjectMocks
    EngagementServiceImpl engagementService;

    @Mock
    EngagementRepository engagementRepository;

    @Mock
    HierarchyServiceImpl hierarchyService;

    @Mock
    ClientService clientService;

    @Mock
    INSService insService;

    @Test(expected = ValidationException.class)
    public void getEngagement_hierarchyNotFound()
    {
        long hierarchyId = 1L;
        when(clientService.getClientByToken()).thenReturn(getClient());
        when(hierarchyService.fetchHierarchyById(eq(hierarchyId), any())).thenThrow(new ValidationException(""));
        engagementService.getEngagementByHierarchy(hierarchyId, false);
    }

    @Test
    public void getEngagement_engagementNotFound()
    {
        long hierarchyId = 1L;
        when(clientService.getClientByToken()).thenReturn(getClient());
        HierarchyResponseData hierarchy = new HierarchyResponseData();
        when(hierarchyService.fetchHierarchyById(eq(hierarchyId), any())).thenReturn(hierarchy);
        when(engagementRepository.findFirst1ByHierarchyId(any())).thenReturn(null);
        EngagementDetails output = engagementService.getEngagementByHierarchy(hierarchyId, false);

        assertEquals(null, output);
    }

    @Test
    public void getEngagement_Success()
    {
        long hierarchyId = 1L;
        HierarchyResponseData hierarchy = new HierarchyResponseData();
        Engagement engagement = new Engagement(1L, 1L, "", "", "", true, "", "");
        when(clientService.getClientByToken()).thenReturn(getClient());
        when(hierarchyService.fetchHierarchyById(eq(hierarchyId), any())).thenReturn(hierarchy);
        when(engagementRepository.findFirst1ByHierarchyId(any())).thenReturn(engagement);
        EngagementDetails engagementDetails = engagementService.getEngagementByHierarchy(hierarchyId, false);

        assertEquals(engagement.getHierarchyId(), engagementDetails.getHierarchyId());
    }

    @Test
    public void getEngagement_Parent_Success()
    {
        long hierarchyId = 1L;
        long hierarchyId2 = 2L;
        HierarchyResponseData hierarchy = new HierarchyResponseData();
        hierarchy.setId(hierarchyId);;
        hierarchy.setParentId(hierarchyId2);
        hierarchy.setLevel(hierarchyId2);
        HierarchyResponseData hierarchy2 = new HierarchyResponseData();
        hierarchy2.setId(hierarchyId2);
        when(clientService.getClientByToken()).thenReturn(getClient());
        Engagement engagement = new Engagement(hierarchyId, hierarchyId, "", "", "", true, "", "");
        when(hierarchyService.fetchHierarchyById(eq(hierarchyId), any())).thenReturn(hierarchy);
        when(hierarchyService.fetchHierarchyById(eq(hierarchyId2), any())).thenReturn(hierarchy2);
        when(engagementRepository.findFirst1ByHierarchyId(eq(hierarchyId))).thenReturn(null);
        when(engagementRepository.findFirst1ByHierarchyId(eq(hierarchyId2))).thenReturn(engagement);
        EngagementDetails engagementDetails = engagementService.getEngagementByHierarchy(hierarchyId, true);

        assertEquals(engagement.getHierarchyId(), engagementDetails.getHierarchyId());
    }

    @Test
    public void addEngagement_success()
    {
        EngagementDetails engagementDetails = new EngagementDetails
                (1L, 1L, "10:00:00", "", "10:00:00", true,
                        "", "");

        HierarchyResponseData hierarchy = new HierarchyResponseData();
        LanguageResponse languageResponse = getLanguageResponse();
        ResponseEntity<Response<LanguageResponse>> insResponse = new ResponseEntity<>(new Response<>(true, languageResponse), HttpStatus.OK);
        when(clientService.getClientByToken()).thenReturn(getClient());
        when(hierarchyService.fetchHierarchyById(any(), any())).thenReturn(hierarchy);
        when(insService.getAllLanguages(any())).thenReturn(insResponse);
        List<EngagementDetails> response = engagementService.addEngagement(Collections.singletonList(engagementDetails));
        assertEquals(response.size(), 0);
    }

    @Test
    public void addEngagement_hierarchyNotFound()
    {
        EngagementDetails engagementDetails = new EngagementDetails
                (1L, 1L, "10:00:00", "", "10:00:00", true,
                        "", "");

        when(clientService.getClientByToken()).thenReturn(getClient());
        when(hierarchyService.fetchHierarchyById(any(), any())).thenThrow(new ValidationException(""));
        List<EngagementDetails> response = engagementService.addEngagement(Collections.singletonList(engagementDetails));
        assertEquals(response.size(), 1);
    }

    @Test
    public void addEngagement_invalidDefaultTime()
    {
        EngagementDetails engagementDetails = new EngagementDetails
                (1L, 1L, "10:00:", "", "10:00:00", true,
                        "", "");

        HierarchyResponseData hierarchy = new HierarchyResponseData();
        when(clientService.getClientByToken()).thenReturn(getClient());
        when(hierarchyService.fetchHierarchyById(any(), any())).thenReturn(hierarchy);
        List<EngagementDetails> response = engagementService.addEngagement(Collections.singletonList(engagementDetails));
        assertEquals(response.size(), 1);
    }

    @Test
    public void addEngagement_invalidDoseTime()
    {
        EngagementDetails engagementDetails = new EngagementDetails
                (1L, 1L, "10:00:00", "", "10:00", true,
                        "", "");

        HierarchyResponseData hierarchy = new HierarchyResponseData();

        when(clientService.getClientByToken()).thenReturn(getClient());

        when(hierarchyService.fetchHierarchyById(any(), any())).thenReturn(hierarchy);
        List<EngagementDetails> response = engagementService.addEngagement(Collections.singletonList(engagementDetails));
        assertEquals(response.size(), 1);
    }

    @Test
    public void addEngagement_engagementAlreadyExist()
    {
        Engagement engagement = new Engagement(1L, 1L, "10:00:00", "", "10:00", true,
                "", "");

        EngagementDetails engagementDetails = new EngagementDetails(engagement);
        HierarchyResponseData hierarchy = new HierarchyResponseData();
        when(clientService.getClientByToken()).thenReturn(getClient());

        when(hierarchyService.fetchHierarchyById(any(), any())).thenReturn(hierarchy);
        when(engagementRepository.findFirst1ByHierarchyId(any())).thenReturn(engagement);
        List<EngagementDetails> response = engagementService.addEngagement(Collections.singletonList(engagementDetails));
        assertEquals(response.size(), 1);
    }
    @Test
    public void addEngagement_getEngagementException()
    {
        Engagement engagement = new Engagement(1L, 1L, "10:00:00", "", "10:00:00", true,
                "", "");

        EngagementDetails engagementDetails = new EngagementDetails(engagement);
        HierarchyResponseData hierarchy = new HierarchyResponseData();
        LanguageResponse languageResponse = getLanguageResponse();
        ResponseEntity<Response<LanguageResponse>> insResponse = new ResponseEntity<>(new Response<>(true, languageResponse), HttpStatus.OK);
        when(clientService.getClientByToken()).thenReturn(getClient());
        when(hierarchyService.fetchHierarchyById(any(), any())).thenReturn(hierarchy);
        when(engagementRepository.findFirst1ByHierarchyId(any())).thenReturn(null);
        when(insService.getAllLanguages(any())).thenReturn(insResponse);
        List<EngagementDetails> response = engagementService.addEngagement(Collections.singletonList(engagementDetails));
        assertEquals(response.size(), 0);
    }

    @Test
    public void addEngagement_invalidLanguageId()
    {
        Engagement engagement = new Engagement(1L, 5L, "10:00:00", null, "10:00:00", true,
                "", "1");

        EngagementDetails engagementDetails = new EngagementDetails(engagement);
        HierarchyResponseData hierarchy = new HierarchyResponseData();
        LanguageResponse languageResponse = getLanguageResponse();
        ResponseEntity<Response<LanguageResponse>> insResponse = new ResponseEntity<>(new Response<>(true, languageResponse), HttpStatus.OK);
        when(clientService.getClientByToken()).thenReturn(getClient());
        when(hierarchyService.fetchHierarchyById(any(), any())).thenReturn(hierarchy);
        when(engagementRepository.findFirst1ByHierarchyId(any())).thenReturn(null);
        when(insService.getAllLanguages(any())).thenReturn(insResponse);
        List<EngagementDetails> response = engagementService.addEngagement(Collections.singletonList(engagementDetails));
        assertEquals(1, response.size());
    }

    @Test
    public void updateLanguagesSuccess(){
        Engagement engagement = getEngagements().get(0);
        UpdateLanguageRequest updateLanguageRequest = getUpdateLanguageRequest();
        HierarchyResponseData hierarchy = new HierarchyResponseData();
        LanguageResponse languageResponse = getLanguageResponse();
        ResponseEntity<Response<LanguageResponse>> insResponse = new ResponseEntity<>(new Response<>(true, languageResponse), HttpStatus.OK);
        when(clientService.getClientByToken()).thenReturn(getClient());
        when(hierarchyService.fetchHierarchyById(any(), any())).thenReturn(hierarchy);
        when(engagementRepository.findFirst1ByHierarchyId(any())).thenReturn(engagement);
        when(insService.getAllLanguages(any())).thenReturn(insResponse);
        Boolean response = engagementService.updateLanguages(updateLanguageRequest);
        assertTrue(response);
    }

    @Test
    public void updateLanguagesSuccessDisabledLanguageIdNull(){
        Engagement engagement = getEngagements().get(0);
        UpdateLanguageRequest updateLanguageRequest = getUpdateLanguageRequestEmpty();
        HierarchyResponseData hierarchy = new HierarchyResponseData();
        LanguageResponse languageResponse = getLanguageResponse();
        ResponseEntity<Response<LanguageResponse>> insResponse = new ResponseEntity<>(new Response<>(true, languageResponse), HttpStatus.OK);
        when(clientService.getClientByToken()).thenReturn(getClient());
        when(hierarchyService.fetchHierarchyById(any(), any())).thenReturn(hierarchy);
        when(engagementRepository.findFirst1ByHierarchyId(any())).thenReturn(engagement);
        when(insService.getAllLanguages(any())).thenReturn(insResponse);
        Boolean response = engagementService.updateLanguages(updateLanguageRequest);
        assertTrue(response);
    }

    @Test
    public void updateLanguagesSuccessEngagementNull(){
        HierarchyResponseData hierarchy = getHierarchyWithParent().get(0);
        HierarchyResponseData hierarchy2 = getHierarchyWithParent().get(1);
        UpdateLanguageRequest updateLanguageRequest = getUpdateLanguageRequest();

        LanguageResponse languageResponse = getLanguageResponse();
        ResponseEntity<Response<LanguageResponse>> insResponse = new ResponseEntity<>(new Response<>(true, languageResponse), HttpStatus.OK);

        when(clientService.getClientByToken()).thenReturn(getClient());
        Engagement engagement = getEngagements().get(0);
        when(hierarchyService.fetchHierarchyById(eq(hierarchy.getId()), any())).thenReturn(hierarchy);
        when(hierarchyService.fetchHierarchyById(eq(hierarchy2.getId()), any())).thenReturn(hierarchy2);
        when(engagementRepository.findFirst1ByHierarchyId(eq(hierarchy.getId()))).thenReturn(null);
        when(engagementRepository.findFirst1ByHierarchyId(eq(hierarchy2.getId()))).thenReturn(engagement);
        when(insService.getAllLanguages(any())).thenReturn(insResponse);
        Boolean response = engagementService.updateLanguages(updateLanguageRequest);
        assertTrue(response);
    }

    @Test(expected = ValidationException.class)
    public void updateLanguageHierarchyNotFound(){
        long hierarchyId = 1L;
        UpdateLanguageRequest updateLanguageRequest = getUpdateLanguageRequest();
        LanguageResponse languageResponse = getLanguageResponse();
        ResponseEntity<Response<LanguageResponse>> insResponse = new ResponseEntity<>(new Response<>(true, languageResponse), HttpStatus.OK);

        when(clientService.getClientByToken()).thenReturn(getClient());
        when(hierarchyService.fetchHierarchyById(eq(hierarchyId), any())).thenThrow(new ValidationException(""));
        when(insService.getAllLanguages(any())).thenReturn(insResponse);
        Boolean response = engagementService.updateLanguages(updateLanguageRequest);
    }

    @Test
    public void updateLanguageNoEngagementFound(){
        HierarchyResponseData hierarchy = new HierarchyResponseData();
        UpdateLanguageRequest updateLanguageRequest = getUpdateLanguageRequest();
        LanguageResponse languageResponse = getLanguageResponse();
        ResponseEntity<Response<LanguageResponse>> insResponse = new ResponseEntity<>(new Response<>(true, languageResponse), HttpStatus.OK);

        when(clientService.getClientByToken()).thenReturn(getClient());
        when(hierarchyService.fetchHierarchyById(any(), any())).thenReturn(hierarchy);
        when(engagementRepository.findFirst1ByHierarchyId(any())).thenReturn(null);
        when(insService.getAllLanguages(any())).thenReturn(insResponse);
        Boolean response = engagementService.updateLanguages(updateLanguageRequest);
        assertFalse(response);
    }

    @Test(expected = NotFoundException.class)
    public void updateLanguageTest_LanguageResponseBodyNull(){
        Engagement engagement = getEngagements().get(0);
        UpdateLanguageRequest updateLanguageRequest = getUpdateLanguageRequest();
        HierarchyResponseData hierarchy = new HierarchyResponseData();
        LanguageResponse languageResponse = getLanguageResponse();
        ResponseEntity<Response<LanguageResponse>> insResponse = new ResponseEntity<>(new Response<>(null), HttpStatus.OK);
        when(clientService.getClientByToken()).thenReturn(getClient());
        when(hierarchyService.fetchHierarchyById(any(), any())).thenReturn(hierarchy);
        when(engagementRepository.findFirst1ByHierarchyId(any())).thenReturn(engagement);
        when(insService.getAllLanguages(any())).thenReturn(insResponse);
        Boolean response = engagementService.updateLanguages(updateLanguageRequest);
    }

    @Test(expected = NotFoundException.class)
    public void updateLanguageTest_LanguageSuccessFalse(){
        Engagement engagement = getEngagements().get(0);
        UpdateLanguageRequest updateLanguageRequest = getUpdateLanguageRequest();
        HierarchyResponseData hierarchy = new HierarchyResponseData();
        LanguageResponse languageResponse = getLanguageResponse();
        ResponseEntity<Response<LanguageResponse>> insResponse = new ResponseEntity<>(new Response<>(false,null, "Language Ids does not exist."), HttpStatus.BAD_REQUEST);
        when(clientService.getClientByToken()).thenReturn(getClient());
        when(hierarchyService.fetchHierarchyById(any(), any())).thenReturn(hierarchy);
        when(engagementRepository.findFirst1ByHierarchyId(any())).thenReturn(engagement);
        when(insService.getAllLanguages(any())).thenReturn(insResponse);
        Boolean response = engagementService.updateLanguages(updateLanguageRequest);
    }

    @Test(expected = ValidationException.class)
    public void updateLanguagesInvalidLanguageId(){
        Engagement engagement = getEngagements().get(0);
        UpdateLanguageRequest updateLanguageRequest = getUpdateLanguageRequestInvalid();
        HierarchyResponseData hierarchy = new HierarchyResponseData();
        LanguageResponse languageResponse = getLanguageResponse();
        ResponseEntity<Response<LanguageResponse>> insResponse = new ResponseEntity<>(new Response<>(true, languageResponse), HttpStatus.OK);
        when(clientService.getClientByToken()).thenReturn(getClient());
        when(hierarchyService.fetchHierarchyById(any(), any())).thenReturn(hierarchy);
        when(engagementRepository.findFirst1ByHierarchyId(any())).thenReturn(engagement);
        when(insService.getAllLanguages(any())).thenReturn(insResponse);
        Boolean response = engagementService.updateLanguages(updateLanguageRequest);
    }

    @Test
    public void getLanguagesTestSuccessLevel1Hierarchy(){
        HierarchyResponseData hierarchy = getHierarchyLevel1();
        Engagement engagement = getEngagements().get(0);
        LanguageResponse languageResponse = getLanguageResponse();
        ResponseEntity<Response<LanguageResponse>> insResponse = new ResponseEntity<>(new Response<>(true, languageResponse), HttpStatus.OK);

        when(clientService.getClientByToken()).thenReturn(getClient());
        when(hierarchyService.fetchHierarchyById(any(), any())).thenReturn(hierarchy);
        when(engagementRepository.findFirst1ByHierarchyId(hierarchy.getId())).thenReturn(engagement);
        when(insService.getAllLanguages(any())).thenReturn(insResponse);

        LanguagesResponse response = engagementService.getLanguages(hierarchy.getId());
        assertEquals(4, response.getAvailableLanguages().size());
        assertEquals(3, response.getSelectedLanguages().size());
        assertTrue(response.getAvailableLanguages().contains(languageResponse.getLanguageMappings().get(0)));
        assertTrue(response.getAvailableLanguages().contains(languageResponse.getLanguageMappings().get(1)));
        assertTrue(response.getAvailableLanguages().contains(languageResponse.getLanguageMappings().get(2)));
        assertTrue(response.getAvailableLanguages().contains(languageResponse.getLanguageMappings().get(3)));
        assertTrue(response.getSelectedLanguages().contains(languageResponse.getLanguageMappings().get(0)));
        assertTrue(response.getSelectedLanguages().contains(languageResponse.getLanguageMappings().get(2)));
        assertTrue(response.getSelectedLanguages().contains(languageResponse.getLanguageMappings().get(3)));

    }

    @Test
    public void getLanguagesTestSuccess(){
        HierarchyResponseData hierarchy = getHierarchyLevel2();
        Engagement engagement = getEngagements().get(0);
        Engagement engagement2 = getEngagements().get(1);
        List<Engagement> engagementList = new ArrayList<>();
        engagementList.add(engagement2);
        LanguageResponse languageResponse = getLanguageResponse();
        ResponseEntity<Response<LanguageResponse>> insResponse = new ResponseEntity<>(new Response<>(true, languageResponse), HttpStatus.OK);


        when(clientService.getClientByToken()).thenReturn(getClient());
        when(hierarchyService.fetchHierarchyById(any(), any())).thenReturn(hierarchy);
        when(engagementRepository.findFirst1ByHierarchyId(hierarchy.getId())).thenReturn(engagement);
        when(engagementRepository.findFirst1ByHierarchyId(hierarchy.getLevel1Id())).thenReturn(engagement2);
        when(engagementRepository.findByHierarchyIdInAndDisabledLanguagesNotNull(any())).thenReturn(engagementList);
        when(insService.getAllLanguages(any())).thenReturn(insResponse);

        LanguagesResponse response = engagementService.getLanguages(hierarchy.getId());
        assertEquals(3, response.getAvailableLanguages().size());
        assertEquals(3, response.getSelectedLanguages().size());
        assertTrue(response.getAvailableLanguages().contains(languageResponse.getLanguageMappings().get(0)));
        assertTrue(response.getAvailableLanguages().contains(languageResponse.getLanguageMappings().get(1)));
        assertTrue(response.getAvailableLanguages().contains(languageResponse.getLanguageMappings().get(2)));
        assertTrue(response.getSelectedLanguages().contains(languageResponse.getLanguageMappings().get(0)));
        assertTrue(response.getSelectedLanguages().contains(languageResponse.getLanguageMappings().get(1)));
        assertTrue(response.getSelectedLanguages().contains(languageResponse.getLanguageMappings().get(2)));
    }

    @Test
    public void getLanguagesTestHierarchyNotFound(){
        long hierarchyId = 1L;
        when(clientService.getClientByToken()).thenReturn(getClient());
        when(hierarchyService.fetchHierarchyById(eq(hierarchyId), any())).thenThrow(new ValidationException(""));
        LanguagesResponse output = engagementService.getLanguages(hierarchyId);
        assertNull(output);
    }

    @Test
    public void getLanguageEngagementNotFound()
    {
        HierarchyResponseData hierarchy = getHierarchyLevel2();
        long hierarchyId = hierarchy.getId();
        when(clientService.getClientByToken()).thenReturn(getClient());

        when(hierarchyService.fetchHierarchyById(eq(hierarchyId), any())).thenReturn(hierarchy);
        when(engagementRepository.findFirst1ByHierarchyId(any())).thenReturn(null);
        LanguagesResponse output = engagementService.getLanguages(hierarchyId);

        assertNull(output);
    }

    private HierarchyResponseData getHierarchyLevel2(){
        HierarchyResponseData hierarchy = new HierarchyResponseData();
        hierarchy.setId(1L);
        hierarchy.setLevel(2L);
        hierarchy.setLevel1Id(2L);
        hierarchy.setParentId(1L);
        return hierarchy;
    }

    private HierarchyResponseData getHierarchyLevel1(){
        HierarchyResponseData hierarchy = new HierarchyResponseData();
        hierarchy.setId(1L);
        hierarchy.setLevel(1L);
        return hierarchy;
    }

    private List<HierarchyResponseData> getHierarchyWithParent(){
        HierarchyResponseData hierarchy = new HierarchyResponseData();
        hierarchy.setId(1L);;
        hierarchy.setParentId(2L);
        hierarchy.setLevel(2L);
        HierarchyResponseData hierarchy2 = new HierarchyResponseData();
        hierarchy2.setId(2L);
        List<HierarchyResponseData> response = Arrays.asList(hierarchy,hierarchy2);
        return response;
    }

    private List<Engagement> getEngagements(){
        Engagement engagement = new Engagement(1L, 1L, "10:00:00", "1,2,3,4", "10:00:00", true,
                "", "2");
        Engagement engagement2 = new Engagement(2L, 1L, "10:00:00", "3", "10:00:00", true,
                "", "4");
        List<Engagement> engagementList = Arrays.asList(engagement,engagement2);
        return  engagementList;
    }

    private UpdateLanguageRequest getUpdateLanguageRequest(){
        List<Long> languageIds = Arrays.asList(1L,2L,3L);
        List<Long> disabledLanguageIds = Arrays.asList(3L,4L);
        UpdateLanguageRequest updateLanguageRequest = new UpdateLanguageRequest(1L, languageIds, disabledLanguageIds);
        return updateLanguageRequest;
    }

    private UpdateLanguageRequest getUpdateLanguageRequestInvalid(){
        List<Long> languageIds = Arrays.asList(1L,2L,30L);
        List<Long> disabledLanguageIds = Arrays.asList(3L,40L);
        UpdateLanguageRequest updateLanguageRequest = new UpdateLanguageRequest(1L, languageIds, disabledLanguageIds);
        return updateLanguageRequest;
    }

    private UpdateLanguageRequest getUpdateLanguageRequestEmpty(){
        List<Long> languageIds = Arrays.asList(1L,2L,3L);
        List<Long> disabledLanguageIds = new ArrayList<>();
        UpdateLanguageRequest updateLanguageRequest = new UpdateLanguageRequest(1L, languageIds, disabledLanguageIds);
        return updateLanguageRequest;
    }

    private LanguageResponse getLanguageResponse(){
        LanguageMapping englishMap = new LanguageMapping(1L, "English");
        LanguageMapping amharicMap = new LanguageMapping(2L, "Amharic");
        LanguageMapping bengaliMap = new LanguageMapping(3L, "Bengali");
        LanguageMapping kirghizMap = new LanguageMapping(4L, "Kirghiz");
        LanguageResponse languageResponse = new LanguageResponse(Arrays.asList(englishMap, amharicMap, bengaliMap, kirghizMap));
        return languageResponse;
    }
}
