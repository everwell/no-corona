package com.everwell.registryservice.service;

import com.everwell.registryservice.BaseTest;
import com.everwell.registryservice.constants.ValidationStatusEnum;
import com.everwell.registryservice.exceptions.ConflictException;
import com.everwell.registryservice.exceptions.NotFoundException;
import com.everwell.registryservice.exceptions.ValidationException;
import com.everwell.registryservice.models.db.Deployment;
import com.everwell.registryservice.models.db.Hierarchy;
import com.everwell.registryservice.models.dto.*;
import com.everwell.registryservice.models.http.requests.UserUpdateHierarchyRequest;
import com.everwell.registryservice.models.http.response.HierarchyResponse;
import com.everwell.registryservice.models.http.response.HierarchySummaryResponseData;
import com.everwell.registryservice.repository.HierarchyRepository;
import com.everwell.registryservice.service.impl.HierarchyServiceImpl;
import com.everwell.registryservice.validators.HierarchyRequestValidator;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;

import java.util.*;
import java.util.stream.Collectors;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

public class HierarchyServiceTest extends BaseTest {
    private final Long testHierarchyId = 34L;
    private final Long testDeploymentId = 36L;
    private final String testHierarchyName = "testHierarchyName";
    private final String testHierarchyCode = "testHierarchyCode";
    private final String testHierarchyType = "testHierarchyType";
    @Spy
    @InjectMocks
    private HierarchyServiceImpl hierarchyService;
    @Mock
    private HierarchyRequestValidator hierarchyRequestValidator;
    @Mock
    private HierarchyRepository hierarchyRepository;

    @Test
    public void addNewHierarchy_validLevel1HierarchyRequestWithoutParentId() {
        HierarchyResponseData hierarchyResponseData = hierarchyService.addNewHierarchy(getValidLevel1HierarchyRequestData(), clientId_Nikshay, testDeploymentId);
        assertTrue(hierarchyResponseData.isActive());
        assertEquals(testHierarchyName, hierarchyResponseData.getName());
        assertEquals(testHierarchyCode, hierarchyResponseData.getCode());
        assertEquals(Optional.of(1L), Optional.of(hierarchyResponseData.getLevel()));
        assertEquals(testHierarchyType, hierarchyResponseData.getType());
        assertEquals(clientId_Nikshay, hierarchyResponseData.getClientId());
        assertEquals(testDeploymentId.toString(), hierarchyResponseData.getDeploymentId());
    }

    @Test
    public void addNewHierarchy_invalidHierarchyRequestParentLevelLessThanChild_Error() {
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, 4L)).thenReturn(Optional.of(getLevel5Hierarchy()));
        ValidationException validationException = assertThrows(ValidationException.class, () -> hierarchyService.addNewHierarchy(getValidLevel5HierarchyRequestData(), clientId_Nikshay, testDeploymentId));
        assertEquals(ValidationStatusEnum.CHILD_HIERARCHY_LEVEL_GREATER_THAN_PARENT.getMessage(), validationException.getMessage());
    }

    @Test
    public void addNewHierarchy_invalidHierarchyRequestParentLevel6_Error() {
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, 5L)).thenReturn(Optional.of(getInvalidParentHierarchy()));
        ValidationException validationException = assertThrows(ValidationException.class, () -> hierarchyService.addNewHierarchy(getValidLevel6HierarchyRequestData(), clientId_Nikshay, testDeploymentId));
        assertEquals(ValidationStatusEnum.CANNOT_CREATE_CHILD_HIERARCHY.getMessage(), validationException.getMessage());
    }

    @Test
    public void addNewHierarchy_invalidLevel1HierarchyRequest_ExistingLevel1HierarchyUnderDeploymentCode_Error() {
        when(hierarchyRepository.findByClientIdAndDeploymentIdAndLevelAndActiveTrue(clientId_Nikshay, testDeploymentId, 1L)).thenReturn(getValidLevel1ParentHierarchy());
        ConflictException conflictException = assertThrows(ConflictException.class, () -> hierarchyService.addNewHierarchy(getValidLevel1HierarchyRequestData(), clientId_Nikshay, testDeploymentId));
        assertEquals(ValidationStatusEnum.LEVEL_1_HIERARCHY_EXISTS_FOR_DEPLOYMENT.getMessage(), conflictException.getMessage());
    }

    @Test
    public void addNewHierarchy_validHierarchyRequestWithLevel1Parent() {
        Hierarchy validLevel1ParentHierarchy = getValidLevel1ParentHierarchy();
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, 1L)).thenReturn(Optional.of(validLevel1ParentHierarchy));
        HierarchyResponseData hierarchyResponseData = hierarchyService.addNewHierarchy(getValidLevel2HierarchyRequestData(), clientId_Nikshay, testDeploymentId);
        assertTrue(hierarchyResponseData.isActive());
        assertEquals(testHierarchyName, hierarchyResponseData.getName());
        assertEquals(testHierarchyCode, hierarchyResponseData.getCode());
        assertEquals(Optional.of(2L), Optional.of(hierarchyResponseData.getLevel()));
        assertEquals(testHierarchyType, hierarchyResponseData.getType());
        assertEquals(Optional.of(1L), Optional.of(hierarchyResponseData.getLevel1Id()));
        assertEquals(testDeploymentId.toString(), hierarchyResponseData.getDeploymentId());
        assertNull(hierarchyResponseData.getLevel2Id());
    }

    @Test
    public void addNewHierarchy_validHierarchyRequestWithLevel2Parent() {
        Hierarchy validLevel2ParentHierarchy = getValidLevel2ParentHierarchy();
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, 2L)).thenReturn(Optional.of(validLevel2ParentHierarchy));
        HierarchyResponseData hierarchyResponseData = hierarchyService.addNewHierarchy(getValidLevel3HierarchyRequestData(), clientId_Nikshay, testDeploymentId);
        assertTrue(hierarchyResponseData.isActive());
        assertEquals(testHierarchyName, hierarchyResponseData.getName());
        assertEquals(testHierarchyCode, hierarchyResponseData.getCode());
        assertEquals(Optional.of(3L), Optional.of(hierarchyResponseData.getLevel()));
        assertEquals(testHierarchyType, hierarchyResponseData.getType());
        assertEquals(Optional.of(1L), Optional.of(hierarchyResponseData.getLevel1Id()));
        assertEquals(Optional.of(2L), Optional.of(hierarchyResponseData.getLevel2Id()));
        assertEquals(testDeploymentId.toString(), hierarchyResponseData.getDeploymentId());
        assertNull(hierarchyResponseData.getLevel3Id());
    }

    @Test
    public void addNewHierarchy_validHierarchyRequestWithLevel3Parent() {
        Hierarchy validLevel3ParentHierarchy = getValidLevel3ParentHierarchy();
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, 3L)).thenReturn(Optional.of(validLevel3ParentHierarchy));
        HierarchyResponseData hierarchyResponseData = hierarchyService.addNewHierarchy(getValidLevel4HierarchyRequestData(), clientId_Nikshay, testDeploymentId);
        assertTrue(hierarchyResponseData.isActive());
        assertEquals(testHierarchyName, hierarchyResponseData.getName());
        assertEquals(testHierarchyCode, hierarchyResponseData.getCode());
        assertEquals(Optional.of(4L), Optional.of(hierarchyResponseData.getLevel()));
        assertEquals(testHierarchyType, hierarchyResponseData.getType());
        assertEquals(Optional.of(1L), Optional.of(hierarchyResponseData.getLevel1Id()));
        assertEquals(Optional.of(2L), Optional.of(hierarchyResponseData.getLevel2Id()));
        assertEquals(Optional.of(3L), Optional.of(hierarchyResponseData.getLevel3Id()));
        assertEquals(testDeploymentId.toString(), hierarchyResponseData.getDeploymentId());
        assertNull(hierarchyResponseData.getLevel4Id());
    }

    @Test
    public void addNewHierarchy_validHierarchyRequestWithLevel4Parent() {
        Hierarchy validLevel4ParentHierarchy = getValidLevel4ParentHierarchy();
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, 4L)).thenReturn(Optional.of(validLevel4ParentHierarchy));
        HierarchyResponseData hierarchyResponseData = hierarchyService.addNewHierarchy(getValidLevel5HierarchyRequestData(), clientId_Nikshay, testDeploymentId);
        assertTrue(hierarchyResponseData.isActive());
        assertEquals(testHierarchyName, hierarchyResponseData.getName());
        assertEquals(testHierarchyCode, hierarchyResponseData.getCode());
        assertEquals(Optional.of(5L), Optional.of(hierarchyResponseData.getLevel()));
        assertEquals(testHierarchyType, hierarchyResponseData.getType());
        assertEquals(Optional.of(1L), Optional.of(hierarchyResponseData.getLevel1Id()));
        assertEquals(Optional.of(2L), Optional.of(hierarchyResponseData.getLevel2Id()));
        assertEquals(Optional.of(3L), Optional.of(hierarchyResponseData.getLevel3Id()));
        assertEquals(Optional.of(4L), Optional.of(hierarchyResponseData.getLevel4Id()));
        assertEquals(testDeploymentId.toString(), hierarchyResponseData.getDeploymentId());
        assertNull(hierarchyResponseData.getLevel5Id());
    }

    @Test
    public void addNewHierarchy_validHierarchyRequestWithLevel5Parent() {
        Hierarchy validLevel5ParentHierarchy = getValidLevel5ParentHierarchy();
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, 5L)).thenReturn(Optional.of(validLevel5ParentHierarchy));
        HierarchyResponseData hierarchyResponseData = hierarchyService.addNewHierarchy(getValidLevel6HierarchyRequestData(), clientId_Nikshay, testDeploymentId);
        assertTrue(hierarchyResponseData.isActive());
        assertEquals(testHierarchyName, hierarchyResponseData.getName());
        assertEquals(testHierarchyCode, hierarchyResponseData.getCode());
        assertEquals(Optional.of(6L), Optional.of(hierarchyResponseData.getLevel()));
        assertEquals(testHierarchyType, hierarchyResponseData.getType());
        assertEquals(Optional.of(1L), Optional.of(hierarchyResponseData.getLevel1Id()));
        assertEquals(Optional.of(2L), Optional.of(hierarchyResponseData.getLevel2Id()));
        assertEquals(Optional.of(3L), Optional.of(hierarchyResponseData.getLevel3Id()));
        assertEquals(Optional.of(4L), Optional.of(hierarchyResponseData.getLevel4Id()));
        assertEquals(Optional.of(5L), Optional.of(hierarchyResponseData.getLevel5Id()));
        assertEquals(testDeploymentId.toString(), hierarchyResponseData.getDeploymentId());
        assertNull(hierarchyResponseData.getLevel6Id());
    }

    @Test
    public void fetchHierarchyById_hierarchyNotFound_Error() {
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, testHierarchyId)).thenReturn(Optional.empty());
        NotFoundException notFoundException = assertThrows(NotFoundException.class, () -> hierarchyService.fetchHierarchyById(testHierarchyId, clientId_Nikshay));
        assertEquals(ValidationStatusEnum.HIERARCHY_NOT_FOUND.getMessage(), notFoundException.getMessage());
    }

    @Test
    public void fetchHierarchyById_hierarchyFound_Success() {
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, testHierarchyId)).thenReturn(Optional.of(new Hierarchy()));
        assertNotNull(hierarchyService.fetchHierarchyById(testHierarchyId, clientId_Nikshay));
    }

    @Test
    public void fetchHierarchyByIdWithAncestors_noAncestors() {
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, testHierarchyId)).thenReturn(Optional.of(getValidHierarchyDataWithoutAncestors()));
        List<HierarchyResponse> hierarchyResponseList = hierarchyService.fetchHierarchyByIdWithAncestors(testHierarchyId, clientId_Nikshay);
        assertEquals(1, hierarchyResponseList.size());
    }

    @Test
    public void fetchHierarchyByIdWithAncestors_ancestorsFound() {
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, testHierarchyId)).thenReturn(Optional.of(getValidHierarchyDataWithAncestors()));
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, 1L)).thenReturn(Optional.of(getValidHierarchyDataWithoutAncestors()));
        List<HierarchyResponse> hierarchyResponseList = hierarchyService.fetchHierarchyByIdWithAncestors(testHierarchyId, clientId_Nikshay);
        assertEquals(2, hierarchyResponseList.size());
    }

    @Test
    public void fetchHierarchyByIdWithDescendants_noDescendants() {
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, testHierarchyId)).thenReturn(Optional.of(getValidLevel6ParentHierarchy()));
        when(hierarchyRepository.findByClientIdAndParentId(clientId_Nikshay, testHierarchyId)).thenReturn(Collections.emptyList());
        List<HierarchyResponse> hierarchyResponseList = hierarchyService.fetchHierarchyByIdWithDescendants(testHierarchyId, false, clientId_Nikshay);
        assertEquals(1, hierarchyResponseList.size());
    }

    @Test
    public void fetchHierarchyByIdWithDescendants_descendantsFound_immediateChildrenOnly() {
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, testHierarchyId)).thenReturn(Optional.of(getValidHierarchyDataForDescendants()));
        when(hierarchyRepository.findByClientIdAndParentId(clientId_Nikshay, testHierarchyId)).thenReturn(Collections.singletonList(getLevel5Hierarchy()));
        List<HierarchyResponse> hierarchyResponseList = hierarchyService.fetchHierarchyByIdWithDescendants(testHierarchyId, false, clientId_Nikshay);
        assertEquals(2, hierarchyResponseList.size());
    }

    @Test
    public void fetchHierarchyByIdWithDescendants_descendantsFound_fullTree() {
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, testHierarchyId)).thenReturn(Optional.of(getValidHierarchyDataForDescendants()));
        List<Hierarchy> validDescendantsTree = getValidDescendantsTree();
        when(hierarchyRepository.findAllDescendants(2L, testHierarchyId)).thenReturn(Optional.of(validDescendantsTree));
        List<HierarchyResponse> hierarchyResponseList = hierarchyService.fetchHierarchyByIdWithDescendants(testHierarchyId, true, clientId_Nikshay);
        assertEquals(validDescendantsTree.size() + 1, hierarchyResponseList.size());
    }

    @Test
    public void updateHierarchyTestOnlyName() {
        Hierarchy baseHierarchy = getLevel6Hierarchy();
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, testHierarchyId)).thenReturn(Optional.of(baseHierarchy));
        HierarchyUpdateData hierarchyUpdateData = getHierarchyUpdateDataOnlyName();
        HierarchyResponseData hierarchyResponseData = hierarchyService.updateHierarchy(hierarchyUpdateData, clientId_Nikshay);
        assertEquals(hierarchyUpdateData.getName(), hierarchyResponseData.getName());
        assertEquals(baseHierarchy.getId(), hierarchyResponseData.getId());
        assertEquals(baseHierarchy.isActive(), hierarchyResponseData.isActive());
        assertEquals(baseHierarchy.getCode(), hierarchyResponseData.getCode());
        assertEquals(baseHierarchy.getParentId(), hierarchyResponseData.getParentId());
    }

    @Test
    public void updateHierarchyTestOnlyNameOfAncestor() {
        Hierarchy baseHierarchy = getValidLevel1ParentHierarchy();
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, testHierarchyId)).thenReturn(Optional.of(baseHierarchy));
        when(hierarchyRepository.findAllDescendants(baseHierarchy.getLevel(), baseHierarchy.getId())).thenReturn(Optional.of(getValidDescendantsTree()));
        HierarchyUpdateData hierarchyUpdateData = getHierarchyUpdateDataOnlyName();
        HierarchyResponseData hierarchyResponseData = hierarchyService.updateHierarchy(hierarchyUpdateData, clientId_Nikshay);
        assertEquals(hierarchyUpdateData.getName(), hierarchyResponseData.getName());
        assertEquals(baseHierarchy.getId(), hierarchyResponseData.getId());
        assertEquals(baseHierarchy.isActive(), hierarchyResponseData.isActive());
        assertEquals(baseHierarchy.getCode(), hierarchyResponseData.getCode());
        assertEquals(baseHierarchy.getParentId(), hierarchyResponseData.getParentId());
    }

    @Test
    public void updateHierarchyTestLevel2NameOfAncestor() {
        Hierarchy baseHierarchy = getValidLevel2ParentHierarchy();
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, testHierarchyId)).thenReturn(Optional.of(baseHierarchy));
        when(hierarchyRepository.findAllDescendants(baseHierarchy.getLevel(), baseHierarchy.getId())).thenReturn(Optional.of(getValidDescendantsTree()));
        HierarchyUpdateData hierarchyUpdateData = getHierarchyUpdateDataOnlyName();
        HierarchyResponseData hierarchyResponseData = hierarchyService.updateHierarchy(hierarchyUpdateData, clientId_Nikshay);
        assertEquals(hierarchyUpdateData.getName(), hierarchyResponseData.getName());
    }

    @Test
    public void updateHierarchyTestLevel3NameOfAncestor() {
        Hierarchy baseHierarchy = getValidLevel3ParentHierarchy();
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, testHierarchyId)).thenReturn(Optional.of(baseHierarchy));
        when(hierarchyRepository.findAllDescendants(baseHierarchy.getLevel(), baseHierarchy.getId())).thenReturn(Optional.of(getValidDescendantsTree()));
        HierarchyUpdateData hierarchyUpdateData = getHierarchyUpdateDataOnlyName();
        HierarchyResponseData hierarchyResponseData = hierarchyService.updateHierarchy(hierarchyUpdateData, clientId_Nikshay);
        assertEquals(hierarchyUpdateData.getName(), hierarchyResponseData.getName());
    }

    @Test
    public void updateHierarchyTestLevel4NameOfAncestor() {
        Hierarchy baseHierarchy = getValidLevel4ParentHierarchy();
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, testHierarchyId)).thenReturn(Optional.of(baseHierarchy));
        when(hierarchyRepository.findAllDescendants(baseHierarchy.getLevel(), baseHierarchy.getId())).thenReturn(Optional.of(getValidDescendantsTree()));
        HierarchyUpdateData hierarchyUpdateData = getHierarchyUpdateDataOnlyName();
        HierarchyResponseData hierarchyResponseData = hierarchyService.updateHierarchy(hierarchyUpdateData, clientId_Nikshay);
        assertEquals(hierarchyUpdateData.getName(), hierarchyResponseData.getName());
    }

    @Test
    public void updateHierarchyTestLevel5NameOfAncestor() {
        Hierarchy baseHierarchy = getValidLevel5ParentHierarchy();
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, testHierarchyId)).thenReturn(Optional.of(baseHierarchy));
        when(hierarchyRepository.findAllDescendants(baseHierarchy.getLevel(), baseHierarchy.getId())).thenReturn(Optional.of(getValidDescendantsTree()));
        HierarchyUpdateData hierarchyUpdateData = getHierarchyUpdateDataOnlyName();
        HierarchyResponseData hierarchyResponseData = hierarchyService.updateHierarchy(hierarchyUpdateData, clientId_Nikshay);
        assertEquals(hierarchyUpdateData.getName(), hierarchyResponseData.getName());
    }

    @Test
    public void updateHierarchyTestFullData() {
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, testHierarchyId)).thenReturn(Optional.of(getLevel6Hierarchy()));
        Hierarchy parentHierarchy = getValidLevel5ParentHierarchy();
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, 35L)).thenReturn(Optional.of(parentHierarchy));
        HierarchyUpdateData hierarchyUpdateData = getHierarchyUpdateData();
        HierarchyResponseData hierarchyResponseData = hierarchyService.updateHierarchy(hierarchyUpdateData, clientId_Nikshay);
        assertEquals(hierarchyUpdateData.getName(), hierarchyResponseData.getName());
        assertEquals(hierarchyUpdateData.getHierarchyId(), hierarchyResponseData.getId());
        assertEquals(hierarchyUpdateData.getActive(), hierarchyResponseData.isActive());
        assertEquals(hierarchyUpdateData.getCode(), hierarchyResponseData.getCode());
        assertEquals(hierarchyUpdateData.getParentId(), hierarchyResponseData.getParentId());
    }

    @Test
    public void updateHierarchyTestFullDataExceptName() {
        Hierarchy inputHierarchy = getLevel6Hierarchy();
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, testHierarchyId)).thenReturn(Optional.of(inputHierarchy));
        Hierarchy parentHierarchy = getValidLevel5ParentHierarchy();
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, 35L)).thenReturn(Optional.of(parentHierarchy));
        HierarchyUpdateData hierarchyUpdateData = getHierarchyUpdateDataExceptName();
        HierarchyResponseData hierarchyResponseData = hierarchyService.updateHierarchy(hierarchyUpdateData, clientId_Nikshay);
        assertEquals(inputHierarchy.getName(), hierarchyResponseData.getName());
        assertEquals(hierarchyUpdateData.getHierarchyId(), hierarchyResponseData.getId());
        assertEquals(hierarchyUpdateData.getActive(), hierarchyResponseData.isActive());
        assertEquals(hierarchyUpdateData.getCode(), hierarchyResponseData.getCode());
        assertEquals(hierarchyUpdateData.getParentId(), hierarchyResponseData.getParentId());
    }

    @Test
    public void deleteHierarchy_inactiveHierarchy_Error() {
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, testHierarchyId)).thenReturn(Optional.of(getInactiveHierarchy()));
        NotFoundException notFoundException = assertThrows(NotFoundException.class, () -> hierarchyService.deleteHierarchy(testHierarchyId, clientId_Nikshay));
        assertEquals(ValidationStatusEnum.HIERARCHY_NOT_ACTIVE.getMessage(), notFoundException.getMessage());
    }

    @Test
    public void deleteHierarchy_activeHierarchy_Success() {
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, testHierarchyId)).thenReturn(Optional.of(getActiveHierarchy()));
        assertTrue(hierarchyService.deleteHierarchy(testHierarchyId, clientId_Nikshay));
    }

    @Test
    public void fetchAllHierarchiesTest_HierarchyNotFound() {
        List<Hierarchy> hierarchyList = getValidDescendantsTree();
        when(hierarchyRepository.findByIdInAndClientIdAndActiveTrue(any(List.class), any(Long.class))).thenReturn(hierarchyList);
        NotFoundException notFoundException = assertThrows(NotFoundException.class, () -> hierarchyService.fetchAllHierarchies(Collections.singletonList(1L), clientId_Nikshay));
        assertEquals(ValidationStatusEnum.HIERARCHY_NOT_FOUND.getMessage(), notFoundException.getMessage());
    }

    @Test
    public void fetchAllHierarchiesTest() {
        List<Hierarchy> hierarchyList = getValidDescendantsTree();
        List<Long> hierarchyIdList = hierarchyList.stream().map(Hierarchy::getId).collect(Collectors.toList());
        when(hierarchyRepository.findByIdInAndClientIdAndActiveTrue(any(List.class), any(Long.class))).thenReturn(hierarchyList);
        List<HierarchyResponse> hierarchyResponseList = hierarchyService.fetchAllHierarchies(hierarchyIdList, clientId_Nikshay);
        assertEquals(hierarchyIdList.size(), hierarchyResponseList.size());
    }

    @Test
    public void fetchAllHierarchiesLineagesTest() {
        List<Long> hierarchyIdList = getHierarchyIdList(6L);
        List<Hierarchy> hierarchyList = getValidHierarchiesList();
        when(hierarchyRepository.findByIdInAndClientIdAndActiveTrue(any(List.class), any(Long.class))).thenReturn(hierarchyList);
        List<HierarchyLineage> hierarchyLineages = hierarchyService.fetchAllHierarchiesLineages(hierarchyIdList, clientId_Nikshay);
        assertEquals(hierarchyIdList.size(), hierarchyLineages.size());
        hierarchyLineages.forEach(hierarchyLineage -> {
            assertNotNull(hierarchyLineage.getId());
            assertNotNull(hierarchyLineage.getAncestralLevelMap());
            assertTrue(hierarchyLineage.getAncestralLevelMap().size() > 0);
        });
    }

    @Test(expected = NotFoundException.class)
    public void fetchHierarchySummary_HierarchyNotFoundFailure() {
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, testHierarchyId)).thenReturn(Optional.empty());
        hierarchyService.fetchHierarchySummaryById(testHierarchyId, clientId_Nikshay);
    }

    @Test
    public void fetchHierarchySummary_NullParentIdSuccess() {
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, 1L)).thenReturn(Optional.of(getValidLevel1ParentHierarchy()));
        when(hierarchyRepository.countByClientIdAndParentId(clientId_Nikshay, 1L)).thenReturn(20L);
        HierarchySummaryResponseData summaryResponseData = hierarchyService.fetchHierarchySummaryById(1L, clientId_Nikshay);
        assertNotNull(summaryResponseData);
        assertNotNull(summaryResponseData.getId());
        assertEquals(20L, summaryResponseData.getChildren().longValue());
        assertEquals(0L, summaryResponseData.getSiblings().longValue());
    }

    @Test
    public void fetchHierarchySummary_NotNullParentIdSuccess() {
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, 2L)).thenReturn(Optional.of(getValidLevel2ParentHierarchy()));
        when(hierarchyRepository.countByClientIdAndParentId(clientId_Nikshay, 2L)).thenReturn(10L);
        when(hierarchyRepository.countByClientIdAndParentId(clientId_Nikshay,1L)).thenReturn(20L);
        HierarchySummaryResponseData summaryResponseData = hierarchyService.fetchHierarchySummaryById(2L, clientId_Nikshay);
        assertNotNull(summaryResponseData);
        assertNotNull(summaryResponseData.getId());
        assertEquals(10L, summaryResponseData.getChildren().longValue());
        assertEquals(19L, summaryResponseData.getSiblings().longValue());
    }

    @Test
    public void fetchHierarchySummary_NoChildrenSuccess() {
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, testHierarchyId)).thenReturn(Optional.of(getValidLevel5ParentHierarchy()));
        when(hierarchyRepository.countByClientIdAndParentId(clientId_Nikshay, 5L)).thenReturn(10L);
        when(hierarchyRepository.countByClientIdAndParentId(clientId_Nikshay,4L)).thenReturn(5L);
        HierarchySummaryResponseData summaryResponseData = hierarchyService.fetchHierarchySummaryById(testHierarchyId, clientId_Nikshay);
        assertNotNull(summaryResponseData);
        assertNotNull(summaryResponseData.getId());
        assertEquals(0L, summaryResponseData.getChildren().longValue());
        assertEquals(4L, summaryResponseData.getSiblings().longValue());
    }

    @Test
    public void validateStrictDescendant_true() {
        boolean strictDescendant = hierarchyService.validateIsDescendant(testHierarchyId, clientId_Nikshay, testHierarchyId);
        assertTrue(strictDescendant);
    }

    @Test
    public void validateDescendant_RootAtLevel1_ChildAtLevel2_true() {
        Long rootHierarchyId = 1L;
        Long suspectedChildHierarchyId = 2L;
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, rootHierarchyId)).thenReturn(Optional.of(getValidLevel1ParentHierarchy()));
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, suspectedChildHierarchyId)).thenReturn(Optional.of(getValidLevel2ParentHierarchy()));
        boolean strictDescendant = hierarchyService.validateIsDescendant(rootHierarchyId, clientId_Nikshay, suspectedChildHierarchyId);
        assertTrue(strictDescendant);
    }

    @Test
    public void validateDescendant_RootAtLevel1_ChildAtLevel3_true() {
        Long rootHierarchyId = 1L;
        Long suspectedChildHierarchyId = 3L;
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, rootHierarchyId)).thenReturn(Optional.of(getValidLevel1ParentHierarchy()));
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, suspectedChildHierarchyId)).thenReturn(Optional.of(getValidLevel3ParentHierarchy()));
        boolean strictDescendant = hierarchyService.validateIsDescendant(rootHierarchyId, clientId_Nikshay, suspectedChildHierarchyId);
        assertTrue(strictDescendant);
    }

    @Test
    public void validateDescendant_RootAtLevel1_ChildAtLevel4_true() {
        Long rootHierarchyId = 1L;
        Long suspectedChildHierarchyId = 4L;
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, rootHierarchyId)).thenReturn(Optional.of(getValidLevel1ParentHierarchy()));
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, suspectedChildHierarchyId)).thenReturn(Optional.of(getValidLevel4ParentHierarchy()));
        boolean strictDescendant = hierarchyService.validateIsDescendant(rootHierarchyId, clientId_Nikshay, suspectedChildHierarchyId);
        assertTrue(strictDescendant);
    }

    @Test
    public void validateDescendant_RootAtLevel1_ChildAtLevel5_true() {
        Long rootHierarchyId = 1L;
        Long suspectedChildHierarchyId = 5L;
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, rootHierarchyId)).thenReturn(Optional.of(getValidLevel1ParentHierarchy()));
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, suspectedChildHierarchyId)).thenReturn(Optional.of(getValidLevel5ParentHierarchy()));
        boolean strictDescendant = hierarchyService.validateIsDescendant(rootHierarchyId, clientId_Nikshay, suspectedChildHierarchyId);
        assertTrue(strictDescendant);
    }

    @Test
    public void validateDescendant_RootAtLevel2_ChildAtLevel1_false() {
        Long rootHierarchyId = 2L;
        Long suspectedChildHierarchyId = 1L;
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, rootHierarchyId)).thenReturn(Optional.of(getValidLevel2ParentHierarchy()));
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, suspectedChildHierarchyId)).thenReturn(Optional.of(getValidLevel1ParentHierarchy()));
        boolean strictDescendant = hierarchyService.validateIsDescendant(rootHierarchyId, clientId_Nikshay, suspectedChildHierarchyId);
        assertFalse(strictDescendant);
    }

    @Test
    public void validateDescendant_RootAtLevel2_ChildAtLevel3_true() {
        Long rootHierarchyId = 2L;
        Long suspectedChildHierarchyId = 3L;
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, rootHierarchyId)).thenReturn(Optional.of(getValidLevel2ParentHierarchy()));
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, suspectedChildHierarchyId)).thenReturn(Optional.of(getValidLevel3ParentHierarchy()));
        boolean strictDescendant = hierarchyService.validateIsDescendant(rootHierarchyId, clientId_Nikshay, suspectedChildHierarchyId);
        assertTrue(strictDescendant);
    }

    @Test
    public void validateDescendant_RootAtLevel2_ChildAtLevel4_true() {
        Long rootHierarchyId = 2L;
        Long suspectedChildHierarchyId = 4L;
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, rootHierarchyId)).thenReturn(Optional.of(getValidLevel2ParentHierarchy()));
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, suspectedChildHierarchyId)).thenReturn(Optional.of(getValidLevel4ParentHierarchy()));
        boolean strictDescendant = hierarchyService.validateIsDescendant(rootHierarchyId, clientId_Nikshay, suspectedChildHierarchyId);
        assertTrue(strictDescendant);
    }

    @Test
    public void validateDescendant_RootAtLevel2_ChildAtLevel5_true() {
        Long rootHierarchyId = 2L;
        Long suspectedChildHierarchyId = 5L;
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, rootHierarchyId)).thenReturn(Optional.of(getValidLevel2ParentHierarchy()));
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, suspectedChildHierarchyId)).thenReturn(Optional.of(getValidLevel5ParentHierarchy()));
        boolean strictDescendant = hierarchyService.validateIsDescendant(rootHierarchyId, clientId_Nikshay, suspectedChildHierarchyId);
        assertTrue(strictDescendant);
    }

    @Test
    public void validateDescendant_RootAtLevel3_ChildAtLevel1_false() {
        Long rootHierarchyId = 3L;
        Long suspectedChildHierarchyId = 1L;
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, rootHierarchyId)).thenReturn(Optional.of(getValidLevel3ParentHierarchy()));
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, suspectedChildHierarchyId)).thenReturn(Optional.of(getValidLevel1ParentHierarchy()));
        boolean strictDescendant = hierarchyService.validateIsDescendant(rootHierarchyId, clientId_Nikshay, suspectedChildHierarchyId);
        assertFalse(strictDescendant);
    }

    @Test
    public void validateDescendant_RootAtLevel3_ChildAtLevel2_false() {
        Long rootHierarchyId = 3L;
        Long suspectedChildHierarchyId = 2L;
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, rootHierarchyId)).thenReturn(Optional.of(getValidLevel3ParentHierarchy()));
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, suspectedChildHierarchyId)).thenReturn(Optional.of(getValidLevel2ParentHierarchy()));
        boolean strictDescendant = hierarchyService.validateIsDescendant(rootHierarchyId, clientId_Nikshay, suspectedChildHierarchyId);
        assertFalse(strictDescendant);
    }

    @Test
    public void validateDescendant_RootAtLevel3_ChildAtLevel4_true() {
        Long rootHierarchyId = 3L;
        Long suspectedChildHierarchyId = 4L;
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, rootHierarchyId)).thenReturn(Optional.of(getValidLevel3ParentHierarchy()));
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, suspectedChildHierarchyId)).thenReturn(Optional.of(getValidLevel4ParentHierarchy()));
        boolean strictDescendant = hierarchyService.validateIsDescendant(rootHierarchyId, clientId_Nikshay, suspectedChildHierarchyId);
        assertTrue(strictDescendant);
    }

    @Test
    public void validateDescendant_RootAtLevel3_ChildAtLevel5_true() {
        Long rootHierarchyId = 3L;
        Long suspectedChildHierarchyId = 5L;
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, rootHierarchyId)).thenReturn(Optional.of(getValidLevel3ParentHierarchy()));
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, suspectedChildHierarchyId)).thenReturn(Optional.of(getValidLevel5ParentHierarchy()));
        boolean strictDescendant = hierarchyService.validateIsDescendant(rootHierarchyId, clientId_Nikshay, suspectedChildHierarchyId);
        assertTrue(strictDescendant);
    }

    @Test
    public void validateDescendant_RootAtLevel4_ChildAtLevel1_false() {
        Long rootHierarchyId = 4L;
        Long suspectedChildHierarchyId = 1L;
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, rootHierarchyId)).thenReturn(Optional.of(getValidLevel4ParentHierarchy()));
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, suspectedChildHierarchyId)).thenReturn(Optional.of(getValidLevel1ParentHierarchy()));
        boolean strictDescendant = hierarchyService.validateIsDescendant(rootHierarchyId, clientId_Nikshay, suspectedChildHierarchyId);
        assertFalse(strictDescendant);
    }

    @Test
    public void validateDescendant_RootAtLevel4_ChildAtLevel2_false() {
        Long rootHierarchyId = 4L;
        Long suspectedChildHierarchyId = 2L;
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, rootHierarchyId)).thenReturn(Optional.of(getValidLevel4ParentHierarchy()));
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, suspectedChildHierarchyId)).thenReturn(Optional.of(getValidLevel2ParentHierarchy()));
        boolean strictDescendant = hierarchyService.validateIsDescendant(rootHierarchyId, clientId_Nikshay, suspectedChildHierarchyId);
        assertFalse(strictDescendant);
    }

    @Test
    public void validateDescendant_RootAtLevel4_ChildAtLevel3_false() {
        Long rootHierarchyId = 4L;
        Long suspectedChildHierarchyId = 3L;
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, rootHierarchyId)).thenReturn(Optional.of(getValidLevel4ParentHierarchy()));
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, suspectedChildHierarchyId)).thenReturn(Optional.of(getValidLevel3ParentHierarchy()));
        boolean strictDescendant = hierarchyService.validateIsDescendant(rootHierarchyId, clientId_Nikshay, suspectedChildHierarchyId);
        assertFalse(strictDescendant);
    }

    @Test
    public void validateDescendant_RootAtLevel4_ChildAtLevel5_true() {
        Long rootHierarchyId = 4L;
        Long suspectedChildHierarchyId = 5L;
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, rootHierarchyId)).thenReturn(Optional.of(getValidLevel4ParentHierarchy()));
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, suspectedChildHierarchyId)).thenReturn(Optional.of(getValidLevel5ParentHierarchy()));
        boolean strictDescendant = hierarchyService.validateIsDescendant(rootHierarchyId, clientId_Nikshay, suspectedChildHierarchyId);
        assertTrue(strictDescendant);
    }

    @Test
    public void validateDescendant_RootAtLevel5_ChildAtLevel1_false() {
        Long rootHierarchyId = 5L;
        Long suspectedChildHierarchyId = 1L;
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, rootHierarchyId)).thenReturn(Optional.of(getValidLevel5ParentHierarchy()));
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, suspectedChildHierarchyId)).thenReturn(Optional.of(getValidLevel1ParentHierarchy()));
        boolean strictDescendant = hierarchyService.validateIsDescendant(rootHierarchyId, clientId_Nikshay, suspectedChildHierarchyId);
        assertFalse(strictDescendant);
    }

    @Test
    public void validateDescendant_RootAtLevel5_ChildAtLevel2_false() {
        Long rootHierarchyId = 5L;
        Long suspectedChildHierarchyId = 2L;
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, rootHierarchyId)).thenReturn(Optional.of(getValidLevel5ParentHierarchy()));
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, suspectedChildHierarchyId)).thenReturn(Optional.of(getValidLevel2ParentHierarchy()));
        boolean strictDescendant = hierarchyService.validateIsDescendant(rootHierarchyId, clientId_Nikshay, suspectedChildHierarchyId);
        assertFalse(strictDescendant);
    }

    @Test
    public void validateDescendant_RootAtLevel5_ChildAtLevel3_false() {
        Long rootHierarchyId = 5L;
        Long suspectedChildHierarchyId = 3L;
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, rootHierarchyId)).thenReturn(Optional.of(getValidLevel5ParentHierarchy()));
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, suspectedChildHierarchyId)).thenReturn(Optional.of(getValidLevel3ParentHierarchy()));
        boolean strictDescendant = hierarchyService.validateIsDescendant(rootHierarchyId, clientId_Nikshay, suspectedChildHierarchyId);
        assertFalse(strictDescendant);
    }

    @Test
    public void validateDescendant_RootAtLevel5_ChildAtLevel4_false() {
        Long rootHierarchyId = 5L;
        Long suspectedChildHierarchyId = 4L;
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, rootHierarchyId)).thenReturn(Optional.of(getValidLevel5ParentHierarchy()));
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, suspectedChildHierarchyId)).thenReturn(Optional.of(getValidLevel4ParentHierarchy()));
        boolean strictDescendant = hierarchyService.validateIsDescendant(rootHierarchyId, clientId_Nikshay, suspectedChildHierarchyId);
        assertFalse(strictDescendant);
    }

    @Test
    public void validateDescendant_RootAtLevel5_ChildAtLevel6_true() {
        Long rootHierarchyId = 5L;
        Long suspectedChildHierarchyId = 6L;
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, rootHierarchyId)).thenReturn(Optional.of(getValidLevel5ParentHierarchy()));
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, suspectedChildHierarchyId)).thenReturn(Optional.of(getValidLevel6ParentHierarchy()));
        boolean strictDescendant = hierarchyService.validateIsDescendant(rootHierarchyId, clientId_Nikshay, suspectedChildHierarchyId);
        assertTrue(strictDescendant);
    }

    @Test
    public void validateDescendant_RootAtLevel6_ChildAtLevel1_false() {
        Long rootHierarchyId = 6L;
        Long suspectedChildHierarchyId = 1L;
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, rootHierarchyId)).thenReturn(Optional.of(getValidLevel6ParentHierarchy()));
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, suspectedChildHierarchyId)).thenReturn(Optional.of(getValidLevel1ParentHierarchy()));
        boolean strictDescendant = hierarchyService.validateIsDescendant(rootHierarchyId, clientId_Nikshay, suspectedChildHierarchyId);
        assertFalse(strictDescendant);
    }

    @Test
    public void validateDescendant_RootAtLevel6_ChildAtLevel2_false() {
        Long rootHierarchyId = 6L;
        Long suspectedChildHierarchyId = 2L;
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, rootHierarchyId)).thenReturn(Optional.of(getValidLevel6ParentHierarchy()));
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, suspectedChildHierarchyId)).thenReturn(Optional.of(getValidLevel2ParentHierarchy()));
        boolean strictDescendant = hierarchyService.validateIsDescendant(rootHierarchyId, clientId_Nikshay, suspectedChildHierarchyId);
        assertFalse(strictDescendant);
    }

    @Test
    public void validateDescendant_RootAtLevel6_ChildAtLevel3_false() {
        Long rootHierarchyId = 6L;
        Long suspectedChildHierarchyId = 3L;
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, rootHierarchyId)).thenReturn(Optional.of(getValidLevel6ParentHierarchy()));
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, suspectedChildHierarchyId)).thenReturn(Optional.of(getValidLevel3ParentHierarchy()));
        boolean strictDescendant = hierarchyService.validateIsDescendant(rootHierarchyId, clientId_Nikshay, suspectedChildHierarchyId);
        assertFalse(strictDescendant);
    }

    @Test
    public void validateDescendant_RootAtLevel6_ChildAtLevel4_false() {
        Long rootHierarchyId = 6L;
        Long suspectedChildHierarchyId = 4L;
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, rootHierarchyId)).thenReturn(Optional.of(getValidLevel6ParentHierarchy()));
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, suspectedChildHierarchyId)).thenReturn(Optional.of(getValidLevel4ParentHierarchy()));
        boolean strictDescendant = hierarchyService.validateIsDescendant(rootHierarchyId, clientId_Nikshay, suspectedChildHierarchyId);
        assertFalse(strictDescendant);
    }

    @Test
    public void validateDescendant_RootAtLevel6_ChildAtLevel5_false() {
        Long rootHierarchyId = 6L;
        Long suspectedChildHierarchyId = 5L;
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, rootHierarchyId)).thenReturn(Optional.of(getValidLevel6ParentHierarchy()));
        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, suspectedChildHierarchyId)).thenReturn(Optional.of(getValidLevel5ParentHierarchy()));
        boolean strictDescendant = hierarchyService.validateIsDescendant(rootHierarchyId, clientId_Nikshay, suspectedChildHierarchyId);
        assertFalse(strictDescendant);
    }

    @Test
    public void userUpdateHierarchy_Success() {
        UserUpdateHierarchyRequest hierarchyRequest = new UserUpdateHierarchyRequest();
        hierarchyRequest.setHierarchyId(testHierarchyId);
        hierarchyRequest.setActive(true);
        hierarchyRequest.setName("newHierarchyName");
        hierarchyRequest.setCode("newCode");

        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, testHierarchyId)).thenReturn(Optional.of(getLevel6Hierarchy()));
        when(hierarchyRepository.save(any())).thenReturn(null);

        HierarchySummaryResponseData responseData = hierarchyService.userUpdateHierarchy(hierarchyRequest, clientId_Nikshay);
        assertEquals("newHierarchyName", responseData.getName());
        assertEquals("newCode", responseData.getCode());
    }

    @Test(expected = NotFoundException.class)
    public void userUpdateHierarchy_HierarchyNotFound_Failure() {
        UserUpdateHierarchyRequest hierarchyRequest = new UserUpdateHierarchyRequest();
        hierarchyRequest.setHierarchyId(testHierarchyId);
        hierarchyRequest.setActive(true);
        hierarchyRequest.setName("newHierarchyName");
        hierarchyRequest.setCode("newCode");

        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, testHierarchyId)).thenThrow(new NotFoundException(ValidationStatusEnum.HIERARCHY_NOT_FOUND));

        hierarchyService.userUpdateHierarchy(hierarchyRequest, clientId_Nikshay);
    }

    @Test(expected = NotFoundException.class)
    public void addHierarchyOptionsToUppFilter_HierarchyNotFound_Failure() {
        UnifiedListFilterResponse toBeFilledResponse = new UnifiedListFilterResponse();

        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, testHierarchyId)).thenThrow(new NotFoundException(ValidationStatusEnum.HIERARCHY_NOT_FOUND));

        hierarchyService.addHierarchyOptionsToUppFilter(testHierarchyId, clientId_Nikshay, toBeFilledResponse);
    }

    @Test
    public void addHierarchyOptionsToUppFilter_level2_Success() {
        UnifiedListFilterResponse toBeFilledResponse = new UnifiedListFilterResponse();

        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, testHierarchyId)).thenReturn(Optional.of(getValidHierarchyDataForDescendants()));
        when(hierarchyRepository.findAllDescendants(any(), any())).thenReturn(Optional.of(getValidDescendantsTree()));

        UnifiedListFilterResponse response = hierarchyService.addHierarchyOptionsToUppFilter(testHierarchyId, clientId_Nikshay, toBeFilledResponse);
        assertEquals(4, response.getHierarchyOptions().size());
    }

    @Test
    public void addHierarchyOptionsToUppFilter_level6_Success() {
        UnifiedListFilterResponse toBeFilledResponse = new UnifiedListFilterResponse();

        when(hierarchyRepository.findByClientIdAndIdAndActiveTrue(clientId_Nikshay, testHierarchyId)).thenReturn(Optional.of(getValidLevel6ParentHierarchy()));
        when(hierarchyRepository.findAllDescendants(any(), any())).thenReturn(Optional.of(new ArrayList<>()));

        UnifiedListFilterResponse response = hierarchyService.addHierarchyOptionsToUppFilter(testHierarchyId, clientId_Nikshay, toBeFilledResponse);
        assertEquals(6, response.getHierarchyOptions().size());
    }

    private HierarchyRequestData getValidLevel1HierarchyRequestData() {
        HierarchyRequestData hierarchyRequestData = new HierarchyRequestData();
        hierarchyRequestData.setName(testHierarchyName);
        hierarchyRequestData.setCode(testHierarchyCode);
        hierarchyRequestData.setLevel(1L);
        hierarchyRequestData.setType(testHierarchyType);
        return hierarchyRequestData;
    }

    private HierarchyRequestData getValidLevel2HierarchyRequestData() {
        HierarchyRequestData hierarchyRequestData = getValidLevel1HierarchyRequestData();
        hierarchyRequestData.setLevel(2L);
        hierarchyRequestData.setParentId(1L);
        return hierarchyRequestData;
    }

    private HierarchyRequestData getValidLevel3HierarchyRequestData() {
        HierarchyRequestData hierarchyRequestData = getValidLevel1HierarchyRequestData();
        hierarchyRequestData.setLevel(3L);
        hierarchyRequestData.setParentId(2L);
        return hierarchyRequestData;
    }

    private HierarchyRequestData getValidLevel4HierarchyRequestData() {
        HierarchyRequestData hierarchyRequestData = getValidLevel1HierarchyRequestData();
        hierarchyRequestData.setLevel(4L);
        hierarchyRequestData.setParentId(3L);
        return hierarchyRequestData;
    }

    private HierarchyRequestData getValidLevel5HierarchyRequestData() {
        HierarchyRequestData hierarchyRequestData = getValidLevel1HierarchyRequestData();
        hierarchyRequestData.setLevel(5L);
        hierarchyRequestData.setParentId(4L);
        return hierarchyRequestData;
    }

    private HierarchyRequestData getValidLevel6HierarchyRequestData() {
        HierarchyRequestData hierarchyRequestData = getValidLevel1HierarchyRequestData();
        hierarchyRequestData.setLevel(6L);
        hierarchyRequestData.setParentId(5L);
        return hierarchyRequestData;
    }

    private Hierarchy getValidLevel1ParentHierarchy() {
        Hierarchy parentHierarchy = new Hierarchy();
        parentHierarchy.setId(1L);
        parentHierarchy.setName(testHierarchyName + "parent");
        parentHierarchy.setCode(testHierarchyCode);
        parentHierarchy.setLevel(1L);
        parentHierarchy.setType(testHierarchyType);
        parentHierarchy.setDeploymentId(testDeploymentId);
        parentHierarchy.setHasChildren(true);
        return parentHierarchy;
    }

    private Hierarchy getValidLevel2ParentHierarchy() {
        Hierarchy parentHierarchy = getValidLevel1ParentHierarchy();
        parentHierarchy.setId(2L);
        parentHierarchy.setLevel(2L);
        parentHierarchy.setLevel1Id(1L);
        parentHierarchy.setParentId(1L);
        parentHierarchy.setLevel1Code(testHierarchyCode);
        parentHierarchy.setLevel1Name(testHierarchyName + 1L);
        parentHierarchy.setDeploymentId(testDeploymentId);
        parentHierarchy.setHasChildren(true);
        return parentHierarchy;
    }

    private Hierarchy getValidLevel3ParentHierarchy() {
        Hierarchy parentHierarchy = getValidLevel2ParentHierarchy();
        parentHierarchy.setId(3L);
        parentHierarchy.setLevel(3L);
        parentHierarchy.setLevel2Id(2L);
        parentHierarchy.setLevel2Name(testHierarchyName + 2L);
        parentHierarchy.setDeploymentId(testDeploymentId);
        return parentHierarchy;
    }

    private Hierarchy getValidLevel4ParentHierarchy() {
        Hierarchy parentHierarchy = getValidLevel3ParentHierarchy();
        parentHierarchy.setId(4L);
        parentHierarchy.setLevel(4L);
        parentHierarchy.setLevel3Id(3L);
        parentHierarchy.setLevel3Name(testHierarchyName + 3L);
        parentHierarchy.setDeploymentId(testDeploymentId);
        return parentHierarchy;
    }

    private Hierarchy getValidLevel5ParentHierarchy() {
        Hierarchy parentHierarchy = getValidLevel4ParentHierarchy();
        parentHierarchy.setId(5L);
        parentHierarchy.setLevel(5L);
        parentHierarchy.setLevel4Id(4L);
        parentHierarchy.setParentId(4L);
        parentHierarchy.setLevel4Name(testHierarchyName + 4L);
        parentHierarchy.setDeploymentId(testDeploymentId);
        parentHierarchy.setHasChildren(false);
        return parentHierarchy;
    }

    private Hierarchy getValidLevel6ParentHierarchy() {
        Hierarchy parentHierarchy = getValidLevel5ParentHierarchy();
        parentHierarchy.setId(6L);
        parentHierarchy.setLevel(6L);
        parentHierarchy.setLevel5Id(5L);
        parentHierarchy.setLevel5Name(testHierarchyName + 5L);
        return parentHierarchy;
    }

    private Hierarchy getLevel5Hierarchy() {
        Hierarchy parentHierarchy = new Hierarchy();
        parentHierarchy.setId(5L);
        parentHierarchy.setName(testHierarchyName + "parent");
        parentHierarchy.setCode(testHierarchyCode);
        parentHierarchy.setLevel(5L);
        return parentHierarchy;
    }

    private Hierarchy getLevel6Hierarchy() {
        Hierarchy parentHierarchy = new Hierarchy();
        parentHierarchy.setId(testHierarchyId);
        parentHierarchy.setName(testHierarchyName + "parent");
        parentHierarchy.setCode(testHierarchyCode);
        parentHierarchy.setLevel(6L);
        return parentHierarchy;
    }

    private Hierarchy getInvalidParentHierarchy() {
        Hierarchy parentHierarchy = new Hierarchy();
        parentHierarchy.setId(5L);
        parentHierarchy.setName(testHierarchyName + "parent");
        parentHierarchy.setCode(testHierarchyCode);
        parentHierarchy.setLevel(6L);
        return parentHierarchy;
    }

    private Hierarchy getValidHierarchyDataWithoutAncestors() {
        Hierarchy hierarchyResponseData = new Hierarchy();
        hierarchyResponseData.setName(testHierarchyName);
        hierarchyResponseData.setCode(testHierarchyCode);
        hierarchyResponseData.setLevel(1L);
        hierarchyResponseData.setType(testHierarchyType);
        return hierarchyResponseData;
    }

    private Hierarchy getValidHierarchyDataWithAncestors() {
        Hierarchy hierarchyResponseData = new Hierarchy();
        hierarchyResponseData.setName(testHierarchyName);
        hierarchyResponseData.setCode(testHierarchyCode);
        hierarchyResponseData.setLevel(2L);
        hierarchyResponseData.setType(testHierarchyType);
        hierarchyResponseData.setParentId(1L);
        return hierarchyResponseData;
    }

    private Hierarchy getValidHierarchyDataForDescendants() {
        Hierarchy hierarchyResponseData = new Hierarchy();
        hierarchyResponseData.setName(testHierarchyName);
        hierarchyResponseData.setCode(testHierarchyCode);
        hierarchyResponseData.setLevel(2L);
        hierarchyResponseData.setType(testHierarchyType);
        hierarchyResponseData.setId(1L);
        hierarchyResponseData.setLevel1Name(testHierarchyName+"1");
        hierarchyResponseData.setLevel1Code(testHierarchyCode+"1");
        hierarchyResponseData.setLevel1Type(testHierarchyType+"1");
        return hierarchyResponseData;
    }

    private List<Hierarchy> getValidDescendantsTree() {
        List<Hierarchy> descendantHierarchiesList = new ArrayList<>();
        descendantHierarchiesList.add(getLevel5Hierarchy());
        descendantHierarchiesList.add(getInvalidParentHierarchy());
        descendantHierarchiesList.add(getValidLevel5ParentHierarchy());
        return descendantHierarchiesList;
    }

    private List<Hierarchy> getValidHierarchiesList() {
        List<Hierarchy> hierarchiesList = new ArrayList<>();
        hierarchiesList.add(getValidLevel1ParentHierarchy());
        hierarchiesList.add(getValidLevel2ParentHierarchy());
        hierarchiesList.add(getValidLevel3ParentHierarchy());
        hierarchiesList.add(getValidLevel4ParentHierarchy());
        hierarchiesList.add(getValidLevel5ParentHierarchy());
        hierarchiesList.add(getValidLevel6ParentHierarchy());
        return hierarchiesList;
    }


    private Hierarchy getInactiveHierarchy() {
        Hierarchy inactiveHierarchy = getLevel5Hierarchy();
        inactiveHierarchy.setActive(false);
        return inactiveHierarchy;
    }

    private Hierarchy getActiveHierarchy() {
        Hierarchy inactiveHierarchy = getLevel5Hierarchy();
        inactiveHierarchy.setActive(true);
        return inactiveHierarchy;
    }

    public HierarchyUpdateData getHierarchyUpdateDataOnlyName() {
        HierarchyUpdateData hierarchyUpdateData = new HierarchyUpdateData();
        hierarchyUpdateData.setHierarchyId(testHierarchyId);
        hierarchyUpdateData.setName(testHierarchyName + "Update");
        return hierarchyUpdateData;
    }

    private HierarchyUpdateData getHierarchyUpdateData() {
        HierarchyUpdateData hierarchyUpdateData = new HierarchyUpdateData();
        hierarchyUpdateData.setHierarchyId(testHierarchyId);
        hierarchyUpdateData.setActive(true);
        hierarchyUpdateData.setName(testHierarchyName + "Update");
        hierarchyUpdateData.setParentId(35L);
        hierarchyUpdateData.setCode(testHierarchyCode + "Update");
        return hierarchyUpdateData;
    }

    private HierarchyUpdateData getHierarchyUpdateDataExceptName() {
        HierarchyUpdateData hierarchyUpdateData = new HierarchyUpdateData();
        hierarchyUpdateData.setHierarchyId(testHierarchyId);
        hierarchyUpdateData.setActive(true);
        hierarchyUpdateData.setParentId(35L);
        hierarchyUpdateData.setCode(testHierarchyCode + "Update");
        return hierarchyUpdateData;
    }

    private List<Long> getHierarchyIdList(long size) {
        List<Long> hierarchyIdList = new ArrayList<>();
        for (long i = 1; i <= size; i++) {
            hierarchyIdList.add(i);
        }
        return hierarchyIdList;
    }

}
