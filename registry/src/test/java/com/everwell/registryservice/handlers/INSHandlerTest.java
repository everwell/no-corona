package com.everwell.registryservice.handlers;

import com.everwell.registryservice.BaseTest;
import com.everwell.registryservice.binders.INSBinder;
import com.everwell.registryservice.enums.RMQQueues;
import com.everwell.registryservice.exceptions.HandlerException;
import com.everwell.registryservice.models.dto.GenericEvent;
import com.everwell.registryservice.models.http.requests.ins.SmsDetailsRequest;
import com.everwell.registryservice.models.dto.UserSmsDetails;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.messaging.MessageChannel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;

public class INSHandlerTest extends BaseTest {

    @Spy
    @InjectMocks
    INSHandler insHandler;

    @Mock
    INSBinder insBinder;

    @Mock
    MessageChannel insSmsOutput;

    @Test
    public void sendSmsToINSTest() {
        Long clientId = 1L;
        Map<String, String> parameters = new HashMap<>();
        parameters.put("test", "test");
        UserSmsDetails userDetail = new UserSmsDetails(1L, parameters, "1");
        List<UserSmsDetails> userDetails = new ArrayList<>();
        userDetails.add(userDetail);
        List<Long> templateIds = new ArrayList<>();
        templateIds.add(1L);
        SmsDetailsRequest smsDetail = new SmsDetailsRequest(userDetails, 1L, 1L, 1L, templateIds, true, true, true, "test");
        GenericEvent<SmsDetailsRequest> event = new GenericEvent<>(smsDetail, RMQQueues.Q_INS_PROCESS_SMS.getQueue());
        List<GenericEvent<SmsDetailsRequest>> smsDetails = new ArrayList<>();
        smsDetails.add(event);

        Mockito.when(insBinder.insSmsOutput()).thenReturn(insSmsOutput);

        insHandler.sendSmsToINS(smsDetails, clientId);

        Mockito.verify(insSmsOutput, Mockito.times(1)).send(any());
    }

    @Test(expected = HandlerException.class)
    public void sendSmsToINSTestException() {
        Long clientId = 1L;
        Map<String, String> parameters = new HashMap<>();
        parameters.put("test", "test");
        UserSmsDetails userDetail = new UserSmsDetails(1L, parameters, "1");
        List<UserSmsDetails> userDetails = new ArrayList<>();
        userDetails.add(userDetail);
        List<Long> templateIds = new ArrayList<>();
        templateIds.add(1L);
        SmsDetailsRequest smsDetail = new SmsDetailsRequest(userDetails, 1L, 1L, 1L, templateIds, true, true, true, "test");
        GenericEvent<SmsDetailsRequest> event = new GenericEvent<>(smsDetail, RMQQueues.Q_INS_PROCESS_SMS.getQueue());
        List<GenericEvent<SmsDetailsRequest>> smsDetails = new ArrayList<>();
        smsDetails.add(event);

        Mockito.when(insBinder.insSmsOutput()).thenReturn(null);

        insHandler.sendSmsToINS(smsDetails, clientId);
    }

}
