package com.everwell.registryservice;

import com.everwell.registryservice.constants.AuthConstants;
import com.everwell.registryservice.constants.Constants;
import com.everwell.registryservice.exceptions.NotFoundException;
import com.everwell.registryservice.filters.JwtFilters;
import com.everwell.registryservice.models.db.Client;
import com.everwell.registryservice.models.db.UserAccess;
import com.everwell.registryservice.service.ClientService;
import com.everwell.registryservice.service.UserService;
import com.everwell.registryservice.utils.JwtUtils;
import com.everwell.registryservice.utils.OutputStreamUtils;
import com.everwell.registryservice.utils.Utils;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.MalformedJwtException;
import javax.servlet.http.Cookie;

import io.jsonwebtoken.impl.DefaultClaims;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.util.ReflectionTestUtils;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.util.*;

import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

public class JwtFiltersTest extends BaseTest {

    @InjectMocks
    JwtFilters jwtFilter;

    @Mock
    HttpServletRequest httpServletRequest;

    @Mock
    HttpServletResponse httpServletResponse;

    @Mock
    FilterChain filterChain;

    @Mock
    OutputStreamUtils outputStreamUtils;

    @Spy
    JwtUtils jwtUtils;

    @Mock
    ClientService clientService;

    @Mock
    UserService userService;

    Constants constants;

    private static final Long dummy_client_id = 1L;
    private static final Long dummy_user_id = 123L;
    private static final String dummy_user = "dummy_user";
    private static final String password = "dummy_password";
    private String jwtToken;
    private String ssoToken = "eyJhbGciOiJSUzI1NiJ9.eyJqdGkiOiI1Mzg1MiIsImlhdCI6MTY2MjAzNzQyMSwic3ViIjoiZGVtb2NvdW50cnlsb2dpbiIsImlzcyI6ImV2ZXJ3ZWxsIiwicGFzc3dvcmRSZXNldE9uIjoiMTY2ODkyODc1Njg5NyIsInNzb1VzZXJJZCI6IjYiLCJleHAiOjE2NjI2NDIyMjF9.MHMIch-zagIoej43p7ErOfQUQAeTrdeMKS6AMynEqyDlLnUw7a8IlH-UuBUik5MAR1N8afnjOQaWYEfwpxqzZ4eacPZkbhQfElvENAgSSPgj4QOsJF148IfxK7H1glozY5a5XszRYe0xKsVD6mdoDkf_YOp0q9DsxmXaPz3zejxN5LptjT1VRjkwWyIxC9LXCC_0XeL4HQxQkGIHBOJnk9MdUHUNZNTG5L3jtxA3gNNgRCdY9sT9rV7rYYZr82sw2poz8BBTHdG1II_Hx2iu1Ltp5kY_djkaGAmUCiuuEAKZ6ury__CcatUkTUqSyLXszNDworUQFOiPPWNGh-ilZA";
    private String ssoKey = "SSOAuthorizationLocal";

    private static final String UNSUPPORTED_KEY_TYPE = "Key bytes can only be specified for HMAC signatures. Please specify a PublicKey or PrivateKey instance.";

    @Before
    public void setUp() {
        jwtUtils.setSecretKey("abcd");
        jwtUtils.setSsoPublicKey("MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAgHAl0ifXDAhN28NvkNLAUc+ZECr5IiFY2iv/RSVV7m7lb/hosA7YAWhq7VbqjztkMPRzUzFyPZooIG3hdgu6uho4gqPfACmk5O6uotU+zHQ1ADzclKjfGfb3uaCU53w379Ajf26Ic6hy9OwOnSZYw9BZ2xF3EUdQS7QjtvpiW50Ok14qcQbRdiJia+yWpl4dwyexGACkKAPEI1+6SFhq7xkTFytsBHvdeIeiEw3dchqGOAKj5m7lVt3BJZhnmnn3dr67BQf41mL2L2atFJl9MARJu7XyFgyScX4iPnXM7ySJrpJrAcGOvjFJdnOTq1s6zWnewLNaJl//+mgvRG+CawIDAQAB");
        //Initializing so that the constructor of Constants is also covered
        constants = new Constants();
        jwtToken = jwtUtils.generateToken(String.valueOf(dummy_client_id));
        Authentication authentication = Mockito.mock(Authentication.class);
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);
    }

    @Test
    public void testDoFilterInternalSuccess() throws ServletException, IOException {

        Client dummy_client = new Client(1L, dummy_user, password, Utils.getCurrentDate());
        String authorizationHeader = "Bearer "+jwtToken;

        when(httpServletRequest.getHeader(anyString())).thenReturn(authorizationHeader);
        when(clientService.getClient(any())).thenReturn(dummy_client);
        doNothing().when(filterChain).doFilter(any(), any());

        jwtFilter.doFilterInternal(httpServletRequest, httpServletResponse, filterChain);

        Mockito.verify(filterChain, Mockito.times(1)).doFilter(any(), any());
        Mockito.verify(SecurityContextHolder.getContext(), Mockito.times(1)).setAuthentication(any());
    }

    @Test
    public void testDoFilterInternalExpiredToken() throws ServletException, IOException, ParseException {

        Client dummy_client = new Client(1L, dummy_user, password, Utils.getCurrentDate());
        String authorizationHeader = "Bearer "+jwtToken;
        Date oldExpiration = Utils.convertStringToDate("2019-09-11 13:00:00");
        doReturn(oldExpiration).when(jwtUtils).getExpirationDateFromToken(anyString());
        when(httpServletRequest.getHeader(anyString())).thenReturn(authorizationHeader);
        when(clientService.getClient(any())).thenReturn(dummy_client);
        doNothing().when(filterChain).doFilter(any(), any());

        jwtFilter.doFilterInternal(httpServletRequest, httpServletResponse, filterChain);
        Mockito.verify(filterChain, Mockito.times(1)).doFilter(any(), any());
        Mockito.verify(SecurityContextHolder.getContext(), Mockito.times(0)).setAuthentication(any());
    }

    @Test
    public void testDoFilterInternalIncorrectJWT() throws ServletException, IOException {

        Client dummy_client = new Client(1L, dummy_user, password, Utils.getCurrentDate());
        String wrongJWT = jwtToken.substring(0,jwtToken.length() - 2) + "xy";
        String authorizationHeader = "Bearer "+wrongJWT;

        when(httpServletRequest.getHeader(anyString())).thenReturn(authorizationHeader);
        when(clientService.getClient(any())).thenReturn(dummy_client);
        doNothing().when(filterChain).doFilter(eq(httpServletRequest), eq(httpServletResponse));
        doNothing().when(outputStreamUtils).writeCustomResponseToOutputStream(any(), any(), any());

        jwtFilter.doFilterInternal(httpServletRequest, httpServletResponse, filterChain);

        Mockito.verify(filterChain, Mockito.times(0)).doFilter(eq(httpServletRequest), eq(httpServletResponse));
        Mockito.verify(SecurityContextHolder.getContext(), Mockito.times(0)).setAuthentication(any());
    }

    @Test
    public void testDoFilterInternalMalformedJWT() throws ServletException, IOException {

        Client dummy_client = new Client(1L, dummy_user, password, Utils.getCurrentDate());
        String malformedJWT = "qwertyuo";
        String authorizationHeader = "Bearer "+malformedJWT;

        when(httpServletRequest.getHeader(anyString())).thenReturn(authorizationHeader);
        when(clientService.getClient(any())).thenReturn(dummy_client);
        doNothing().when(filterChain).doFilter(any(), any());
        doNothing().when(outputStreamUtils).writeCustomResponseToOutputStream(any(), any(), any());

        jwtFilter.doFilterInternal(httpServletRequest, httpServletResponse, filterChain);

        Mockito.verify(filterChain, Mockito.times(0)).doFilter(any(), any());
        Mockito.verify(outputStreamUtils, Mockito.times(1)).writeCustomResponseToOutputStream(any(), any(), any());
    }

    @Test
    public void testDoFilterInternalUnsupportedJWT() throws ServletException, IOException {

        Client dummy_client = new Client(1L, dummy_user, password, Utils.getCurrentDate());
        String unsupportedES256Token = "eyJhbGciOiJFUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWUsImlhdCI6MTUxNjIzOTAyMn0.tyh-VfuzIxCyGYDlkBA7DfyjrqmSHu6pQ2hoZuFqUSLPNY2N0mpHb3nk5K17HWP_3cYHBw7AhHale5wky6-sVA";
        String authorizationHeader = "Bearer "+unsupportedES256Token;

        when(httpServletRequest.getHeader(anyString())).thenReturn(authorizationHeader);
        when(clientService.getClient(any())).thenReturn(dummy_client);
        doNothing().when(filterChain).doFilter(any(), any());
        doNothing().when(outputStreamUtils).writeCustomResponseToOutputStream(any(), any(), any());

        jwtFilter.doFilterInternal(httpServletRequest, httpServletResponse, filterChain);

        Mockito.verify(filterChain, Mockito.times(0)).doFilter(any(), any());
        Mockito.verify(outputStreamUtils, Mockito.times(1)).writeCustomResponseToOutputStream(any(), any(), any());
    }

    @Test
    public void testDoFilterException() throws ServletException, IOException {

        String authorizationHeader = "Bearer " + jwtToken;

        when(httpServletRequest.getHeader(anyString())).thenReturn(authorizationHeader);
        when(clientService.getClient(any())).thenThrow(new RuntimeException());

        doNothing().when(filterChain).doFilter(any(), any());
        doNothing().when(outputStreamUtils).writeCustomResponseToOutputStream(any(), any(), any());

        jwtFilter.doFilterInternal(httpServletRequest, httpServletResponse, filterChain);

        Mockito.verify(filterChain, Mockito.times(0)).doFilter(any(), any());
        Mockito.verify(outputStreamUtils, Mockito.times(1)).writeCustomResponseToOutputStream(any(), any(), any());
    }

    @Test
    public void testShouldNotFilterTrue() {

        when(httpServletRequest.getRequestURI()).thenReturn(AuthConstants.AUTH_WHITELIST[0]);
        assertTrue(jwtFilter.shouldNotFilter(httpServletRequest));
    }

    @Test
    public void testDoFilterInternal_MalformedJWTException() throws ServletException, IOException {

        Cookie cookie = new Cookie(ssoKey, ssoToken);
        Cookie[] cookies = { cookie };
        ReflectionTestUtils.setField(jwtFilter, "ssoAuthCookieName", ssoKey);

        when(httpServletRequest.getCookies()).thenReturn(cookies);
        doThrow(new MalformedJwtException("not imp exception")).when(jwtUtils).getParsedClaims(eq(ssoToken));

        doNothing().when(filterChain).doFilter(any(), any());
        doNothing().when(outputStreamUtils).writeCustomResponseToOutputStream(any(), any(), any());

        jwtFilter.doFilterInternal(httpServletRequest, httpServletResponse, filterChain);

        Mockito.verify(filterChain, Mockito.times(0)).doFilter(any(), any());
        Mockito.verify(outputStreamUtils, Mockito.times(1)).writeCustomResponseToOutputStream(any(), any(), any());
        Mockito.verify(SecurityContextHolder.getContext(), Mockito.times(0)).setAuthentication(any());
    }

    @Test
    public void testDoFilterInternalIllegalArgumentException() throws ServletException, IOException {

        Cookie cookie = new Cookie(ssoKey, ssoToken);
        Cookie[] cookies = { cookie };
        ReflectionTestUtils.setField(jwtFilter, "ssoAuthCookieName", ssoKey);

        when(httpServletRequest.getCookies()).thenReturn(cookies);
        doThrow(new IllegalArgumentException("not imp exception")).when(jwtUtils).getParsedClaims(eq(ssoToken));

        doNothing().when(filterChain).doFilter(any(), any());
        doNothing().when(outputStreamUtils).writeCustomResponseToOutputStream(any(), any(), any());

        jwtFilter.doFilterInternal(httpServletRequest, httpServletResponse, filterChain);

        Mockito.verify(filterChain, Mockito.times(0)).doFilter(any(), any());
        Mockito.verify(outputStreamUtils, Mockito.times(1)).writeCustomResponseToOutputStream(any(), any(), any());
        Mockito.verify(SecurityContextHolder.getContext(), Mockito.times(0)).setAuthentication(any());
    }

    @Test
    public void testDoFilterInternalFailure_UserNotFound() throws ServletException, IOException {

        Cookie cookie = new Cookie(ssoKey, ssoToken);
        Cookie[] cookies = { cookie };
        ReflectionTestUtils.setField(jwtFilter, "ssoAuthCookieName", ssoKey);

        Map<String, Object> claimsMap = new HashMap<>();
        claimsMap.put(AuthConstants.SSO_AUTH_USER_ID, dummy_user_id);
        Claims claims = new DefaultClaims(claimsMap);
        claims.setIssuer(AuthConstants.SSO_AUTH_ISSUER);

        when(httpServletRequest.getCookies()).thenReturn(cookies);
        doReturn(claims).when(jwtUtils).getParsedClaims(eq(ssoToken));
        when(userService.fetchUserData(eq(dummy_user_id))).thenThrow(new NotFoundException("user not found"));

        doNothing().when(filterChain).doFilter(any(), any());
        doNothing().when(outputStreamUtils).writeCustomResponseToOutputStream(any(), any(), any());

        jwtFilter.doFilterInternal(httpServletRequest, httpServletResponse, filterChain);

        Mockito.verify(filterChain, Mockito.times(0)).doFilter(any(), any());
        Mockito.verify(outputStreamUtils, Mockito.times(1)).writeCustomResponseToOutputStream(any(), any(), any());
        Mockito.verify(SecurityContextHolder.getContext(), Mockito.times(0)).setAuthentication(any());
    }

    @Test
    public void testDoFilterInternalFailure_ClientNotFound() throws ServletException, IOException {

        Cookie cookie = new Cookie(ssoKey, ssoToken);
        Cookie[] cookies = { cookie };
        ReflectionTestUtils.setField(jwtFilter, "ssoAuthCookieName", ssoKey);

        Map<String, Object> claimsMap = new HashMap<>();
        claimsMap.put(AuthConstants.SSO_AUTH_USER_ID, dummy_user_id);
        Claims claims = new DefaultClaims(claimsMap);
        claims.setIssuer(AuthConstants.SSO_AUTH_ISSUER);

        when(httpServletRequest.getCookies()).thenReturn(cookies);
        doReturn(claims).when(jwtUtils).getParsedClaims(eq(ssoToken));
        when(userService.fetchUserData(eq(dummy_user_id))).thenReturn(getValidUserAccess());
        when(clientService.getClient(eq(dummy_client_id))).thenThrow(new NotFoundException("client not found"));

        doNothing().when(filterChain).doFilter(any(), any());
        doNothing().when(outputStreamUtils).writeCustomResponseToOutputStream(any(), any(), any());

        jwtFilter.doFilterInternal(httpServletRequest, httpServletResponse, filterChain);

        Mockito.verify(filterChain, Mockito.times(0)).doFilter(any(), any());
        Mockito.verify(outputStreamUtils, Mockito.times(1)).writeCustomResponseToOutputStream(any(), any(), any());
        Mockito.verify(SecurityContextHolder.getContext(), Mockito.times(0)).setAuthentication(any());
    }

    @Test
    public void testDoFilterInternal_fallbackToken_Success() throws ServletException, IOException {

        Client dummy_client = new Client(1L, dummy_user, password, Utils.getCurrentDate());
        String authorizationHeader = "Bearer "+jwtToken;

        Map<String, Object> claimsMap = new HashMap<>();
        Claims claims = new DefaultClaims(claimsMap);
        claims.setSubject(dummy_client_id.toString());

        when(httpServletRequest.getCookies()).thenReturn(null);
        when(httpServletRequest.getHeader(anyString())).thenReturn(authorizationHeader);
        when(clientService.getClient(any())).thenReturn(dummy_client);
        doNothing().when(filterChain).doFilter(any(), any());

        jwtFilter.doFilterInternal(httpServletRequest, httpServletResponse, filterChain);

        Mockito.verify(filterChain, Mockito.times(1)).doFilter(any(), any());
        Mockito.verify(SecurityContextHolder.getContext(), Mockito.times(1)).setAuthentication(any());
    }

    @Test
    public void testDoFilterInternal_fallbackToken_NotFoundException() throws ServletException, IOException {

        String authorizationHeader = "Bearer "+jwtToken;

        Map<String, Object> claimsMap = new HashMap<>();
        Claims claims = new DefaultClaims(claimsMap);
        claims.setSubject(dummy_client_id.toString());

        when(httpServletRequest.getCookies()).thenReturn(null);
        when(httpServletRequest.getHeader(anyString())).thenReturn(authorizationHeader);
        doReturn(claims).when(jwtUtils).getParsedClaims(eq(authorizationHeader));
        when(clientService.getClient(any())).thenThrow(new NotFoundException("client not found"));
        doNothing().when(filterChain).doFilter(any(), any());

        jwtFilter.doFilterInternal(httpServletRequest, httpServletResponse, filterChain);

        Mockito.verify(filterChain, Mockito.times(0)).doFilter(any(), any());
        Mockito.verify(outputStreamUtils, Mockito.times(1)).writeCustomResponseToOutputStream(any(), any(), any());
        Mockito.verify(SecurityContextHolder.getContext(), Mockito.times(0)).setAuthentication(any());
    }

    private UserAccess getValidUserAccess() {
        UserAccess userAccess = new UserAccess();
        userAccess.setClientId(dummy_client_id);
        return userAccess;
    }
}
