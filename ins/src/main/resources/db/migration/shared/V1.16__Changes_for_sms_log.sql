ALTER TABLE ins_sms_logs ALTER COLUMN message TYPE text;

CREATE INDEX IF NOT EXISTS ins_sms_logs_phone_number_index ON ins_sms_logs (phone_number);