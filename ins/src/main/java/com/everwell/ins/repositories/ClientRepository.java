package com.everwell.ins.repositories;

import com.everwell.ins.models.db.Client;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClientRepository extends JpaRepository<Client, Long> {

    Client findByName(String name);

}
