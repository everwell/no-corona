package com.everwell.ins.repositories;

import com.everwell.ins.models.db.PushNotificationTemplate;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PushTemplateRepository extends JpaRepository<PushNotificationTemplate,Long> {
}
