package com.everwell.ins.repositories;

import com.everwell.ins.models.db.Language;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface LanguageRepository extends JpaRepository<Language, Long> {

    List<Language> findAllByIdIn(List<Long> languageIds);
}
