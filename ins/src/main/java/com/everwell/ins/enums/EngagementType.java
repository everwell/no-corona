package com.everwell.ins.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum EngagementType {
    SMS(1L),
    PUSH_NOTIFICATION(2L),
    EMAIL(3L);

    private final Long id;
}
