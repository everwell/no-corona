package com.everwell.ins.enums;

public enum AnalyticEventCategoryEnum {
    OUTBOUNDSMS,
    RECONCILIATION,
    PUSHNOTIFICATION,
    EMAIL
}
