package com.everwell.ins.enums;

public enum AnalyticEventActionEnum {
    VENDOR_ID,
    TEMPLATE_ID,
    TRIGGER_ID,
    VENDOR
}
