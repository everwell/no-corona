package com.everwell.ins.services;

import com.everwell.ins.models.db.EpisodeNotification;
import com.everwell.ins.models.http.requests.*;
import com.everwell.ins.models.http.responses.EpisodeNotificationResponse;
import com.everwell.ins.models.http.responses.EpisodeUnreadNotificationResponse;

import java.util.List;

public interface EpisodeNotificationService {

    void saveNotificationBulk(List<EpisodeNotification> episodeNotificationList);

    List<EpisodeNotificationResponse> getNotificationForEpisodes(EpisodeNotificationSearchRequest episodeNotificationSearchRequest, Long clientId);

    void updateNotification(UpdateEpisodeNotificationRequest episodeNotificationRequest);

    void deleteNotification(DeleteNotificationRequest deleteNotificationRequest);

    void updateNotificationBulk(UpdateEpisodeNotificationBulkRequest episodeNotificationRequest, Long clientId);
    
    EpisodeUnreadNotificationResponse getUnreadNotificationForEpisodes(EpisodeUnreadNotificationSearchRequest episodeNotificationSearchRequest, Long clientId);
    
    void addNotificationLog(NotificationLogRequest notificationLogRequest);
}
