package com.everwell.ins.services.impl;

import com.everwell.ins.exceptions.NotFoundException;
import com.everwell.ins.exceptions.ValidationException;
import com.everwell.ins.models.db.Client;
import com.everwell.ins.models.http.requests.RegisterClientRequest;
import com.everwell.ins.models.http.responses.ClientResponse;
import com.everwell.ins.repositories.ClientRepository;
import com.everwell.ins.services.ClientService;
import com.everwell.ins.utils.JwtUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class ClientServiceImpl implements ClientService {

    private static Logger LOGGER = LoggerFactory.getLogger(ClientServiceImpl.class);

    @Autowired
    ClientRepository clientRepository;

    @Override
    public ClientResponse registerClient(RegisterClientRequest clientRequest) {
        Client existingClient = clientRepository.findByName(clientRequest.getName());
        if (null != existingClient) {
            LOGGER.error("[registerClient] User already exists");
            throw new ValidationException("User already exists");
        }
        String encodedPassword = new BCryptPasswordEncoder().encode(clientRequest.getPassword());
        Client newClient = new Client(clientRequest.getName(), encodedPassword);
        clientRepository.save(newClient);
        return new ClientResponse(newClient);
    }

    @Override
    public ClientResponse getClientWithToken(Long id) {
        Client client = getClient(id);
        String tokenSecret = client.getPassword() + client.getName() + id;
        String jwtToken = JwtUtils.generateToken(String.valueOf(id), tokenSecret);
        return new ClientResponse(client, jwtToken, JwtUtils.getExpirationDateFromToken(jwtToken, tokenSecret).getTime());
    }

    @Override
    public Client getClient(Long id) {
        if (null == id) {
            LOGGER.error("[getClient] id cannot be null");
            throw new ValidationException("id cannot be null");
        }
        Client client = clientRepository.findById(id).orElse(null);
        if (null == client) {
            LOGGER.error("[getClient] No client found with id:" + id);
            throw new NotFoundException("No client found with id:" + id);
        }
        return client;
    }

    @Override
    public Client getCurrentClient()
    {
        return (Client) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }

}
