package com.everwell.ins.services.impl;

import com.everwell.ins.exceptions.ValidationException;
import com.everwell.ins.models.http.requests.StatisticsRequest;
import com.everwell.ins.repositories.SmsLogsRepository;
import com.everwell.ins.services.StatisticsService;
import com.everwell.ins.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.Date;

@Service
public class StatisticsServiceImpl implements StatisticsService {

    @Autowired
    SmsLogsRepository smsLogsRepository;

    @Override
    public Long getNotificationsCount(StatisticsRequest statisticsRequest) {
        Date from;
        Date to;
        try {
            from = Utils.convertStringToDate(statisticsRequest.getFrom(), "dd-MM-yyyy HH:mm:ss");
            to = Utils.convertStringToDate(statisticsRequest.getTo(), "dd-MM-yyyy HH:mm:ss");
        } catch (ParseException e) {
            throw new ValidationException("Could not parse. Correct date format is dd-MM-yyyy HH:mm:ss");
        }
        if(statisticsRequest.getTemplateId() != null && statisticsRequest.getTriggerId() != null)
            return smsLogsRepository.getSmsLogsForNotificationStatsWithTemplateAndTriggerId(statisticsRequest.getTriggerId(),
                statisticsRequest.getTemplateId(), statisticsRequest.getVendorId(), from, to);
        else if(statisticsRequest.getTemplateId() != null){
            return smsLogsRepository.getSmsLogsForNotificationStatsWithTemplateId(statisticsRequest.getTemplateId(),
                    statisticsRequest.getVendorId(), from, to);
        }
        else if(statisticsRequest.getTriggerId() != null){
            return smsLogsRepository.getSmsLogsForNotificationStatsWithTriggerId(statisticsRequest.getTriggerId(),
                    statisticsRequest.getVendorId(), from, to);
        }
        else{
            return smsLogsRepository.getSmsLogsForNotificationStats(statisticsRequest.getVendorId(), from, to);
        }
    }

}
