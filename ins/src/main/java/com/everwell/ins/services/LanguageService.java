package com.everwell.ins.services;

import com.everwell.ins.models.db.Language;
import com.everwell.ins.models.dto.LanguageMapping;

import java.util.List;

public interface LanguageService {
    Language getLanguage(Long languageId);

    List<LanguageMapping> getLanguageMappings(List<Long> languageIds);

    List<LanguageMapping> getAllLanguageMappings();
}
