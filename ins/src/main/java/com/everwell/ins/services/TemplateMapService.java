package com.everwell.ins.services;

import com.everwell.ins.models.db.TemplateMap;

public interface TemplateMapService {
    TemplateMap getTemplateMap(Long templateId);
}
