package com.everwell.ins.vendors;

import com.everwell.ins.constants.Constants;
import com.everwell.ins.enums.Language;
import com.everwell.ins.enums.SmsGateway;
import com.everwell.ins.exceptions.ValidationException;
import com.everwell.ins.exceptions.VendorException;
import com.everwell.ins.models.dto.SmsDto;
import com.everwell.ins.models.dto.vendorConfigs.PlivoConfigDto;
import com.everwell.ins.models.dto.vendorCreds.AuthIdAuthTokenDto;
import com.everwell.ins.models.dto.VendorResponseDto;
import com.plivo.api.Plivo;
import com.plivo.api.models.message.Message;
import com.plivo.api.models.message.MessageCreateResponse;
import com.everwell.ins.exceptions.NotImplementedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.net.URL;

@Component
@Scope(value = "prototype", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class SmsPlivoHandler extends SmsHandler {

    private static Logger LOGGER = LoggerFactory.getLogger(SmsPlivoHandler.class);

    private PlivoConfigDto config;

    private AuthIdAuthTokenDto creds;

    @Value("${server.port}")
    private String serverPort;

    @PostConstruct
    public void setBaseUrl() {
        this.baseUrl = "http://" + baseUrl + ":" + serverPort;
    }

    @Override
    public void setVendorParams(Long vendorId) {
        config = vendorService.getVendorConfig(vendorId, PlivoConfigDto.class);
        creds = vendorService.getVendorCredentials(vendorId, AuthIdAuthTokenDto.class);
    }

    @Override
    public SmsGateway getVendorGatewayEnumerator() {
        return SmsGateway.PLIVO;
    }

    @Override
    public String getLanguageMapping(Language language) {
        throw new NotImplementedException("Unimplemented method");
    }

    @Override
    public Integer getBatchSize(Boolean isMessageCommon) {
        return (isMessageCommon) ? Integer.parseInt(config.getBulkSmsLimit()) : Constants.BATCH_SIZE_UNIQUE_SMS;
    }

    @Override
    public VendorResponseDto vendorCall(List<SmsDto> persons, Language language, Long templateId) {
        if (null == persons) {
            LOGGER.error("[vendorCall] The person list is empty");
            throw new ValidationException("The person list is empty");
        }
        VendorResponseDto responseDto = new VendorResponseDto();
        String phone = persons.stream().map(SmsDto::getPhone)
                .collect(Collectors.joining("<"));
        String message = persons.get(0).getMessage();
        try {
            LocalDateTime start = LocalDateTime.now();
            Plivo.init(creds.getAuthId(), creds.getAuthToken());
            MessageCreateResponse response = Message.creator(creds.getSrcNumber(),
                            Collections.singletonList(phone), message)
                    .url(new URL(baseUrl + config.getReconciliation()))
                    .create();

            responseDto.setApiResponse(response.getApiId());
            responseDto.setMessageId(response.getMessageUuid().get(0));
            Long elapsed = Duration.between(start, LocalDateTime.now()).toMillis();
            LOGGER.info("[vendorCall] :  Sent SMS " + message + " to " + phone + " took " + elapsed.toString() + "ms, response = " + responseDto.getApiResponse());
        } catch (Exception e) {
            String errorMessage = "Plivo unable to send SMS to " + phone + ", exception: " + e;
            LOGGER.error("[vendorCall] " + errorMessage);
            throw new VendorException(errorMessage, fetchExtraData(getVendorGatewayEnumerator().toString(), phone, message, templateId));
        }
        return responseDto;
    }
}

