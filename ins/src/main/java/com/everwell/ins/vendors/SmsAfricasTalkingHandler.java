package com.everwell.ins.vendors;

import com.africastalking.AfricasTalking;
import com.africastalking.SmsService;
import com.africastalking.sms.Recipient;
import com.everwell.ins.constants.Constants;
import com.everwell.ins.enums.Language;
import com.everwell.ins.enums.SmsGateway;
import com.everwell.ins.exceptions.ValidationException;
import com.everwell.ins.exceptions.VendorException;
import com.everwell.ins.models.dto.SmsDto;
import com.everwell.ins.models.dto.vendorConfigs.AfricasTalkingConfigDto;
import com.everwell.ins.models.dto.vendorCreds.ApiKeyFromDto;
import com.everwell.ins.models.dto.VendorResponseDto;
import com.everwell.ins.exceptions.NotImplementedException;
import com.everwell.ins.utils.SentryUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Component
@Scope(value = "prototype", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class SmsAfricasTalkingHandler extends SmsHandler {

    private static Logger LOGGER = LoggerFactory.getLogger(SmsAfricasTalkingHandler.class);

    private AfricasTalkingConfigDto config;

    private ApiKeyFromDto creds;

    @Override
    public void setVendorParams(Long vendorId) {
        config = vendorService.getVendorConfig(vendorId, AfricasTalkingConfigDto.class);
        creds = vendorService.getVendorCredentials(vendorId, ApiKeyFromDto.class);
    }

    @Override
    public SmsGateway getVendorGatewayEnumerator() {
        return SmsGateway.AFRICASTALKING;
    }

    @Override
    public String getLanguageMapping(Language language) {
        throw new NotImplementedException("Unimplemented method");
    }

    @Override
    public Integer getBatchSize(Boolean isMessageCommon) {
        return (isMessageCommon && null != config.getBulkSmsLimit()) ? Integer.parseInt(config.getBulkSmsLimit()) : Constants.BATCH_SIZE_UNIQUE_SMS;
    }

    @Override
    public VendorResponseDto vendorCall(List<SmsDto> persons, Language language, Long templateId) {
        if (null == persons) {
            LOGGER.error("[vendorCall] The person list is empty");
            throw new ValidationException("The person list is empty");
        }
        VendorResponseDto responseDto = new VendorResponseDto();
        List<String> phones = new ArrayList<>();
        persons.forEach(p -> {
            phones.add(p.getPhone());
        });
        String message = persons.get(0).getMessage();
        try {
            LocalDateTime start = LocalDateTime.now();
            AfricasTalking.initialize(creds.getUserName(), creds.getApiKey());
            SmsService sms = AfricasTalking.getService(AfricasTalking.SERVICE_SMS);
            LOGGER.info("Africa Talking Vendor Call: username = " + creds.getUserName() +
                    ", from = " + creds.getFrom());
            List<Recipient> response = sms.send(message, creds.getFrom(), phones.toArray(new String[0]), true);
            if (response == null || response.size() == 0) {
                // Connection failed at Africa Talking's end #7332
                responseDto.setApiResponse("Connection with telcos failed");
                responseDto.setMessageId("0");
                SentryUtils.captureException(new Exception("Connection with telcos failed for: " + phones + " , templateId = " + templateId + " . Timestamp = " + LocalDateTime.now()));
            } else {
                responseDto.setApiResponse(response.get(0).status);
                responseDto.setMessageId(response.get(0).messageId);
            }
            Long elapsed = Duration.between(start, LocalDateTime.now()).toMillis();
            LOGGER.info("[VendorCall] :  Sent SMS " + message + " to " + phones + " took " + elapsed.toString() + "ms, response = " + responseDto.getApiResponse());
        } catch (Exception e) {
            String errorMessage = "AfricasTalking unable to send SMS to " + phones + ", exception: " + e;
            LOGGER.error("[vendorCall] " + errorMessage);
            //throw new VendorException(errorMessage, fetchExtraData(getVendorGatewayEnumerator().toString(), phones.toString(), message, templateId));
        }
        return responseDto;
    }
}

