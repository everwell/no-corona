package com.everwell.ins.vendors;

import com.everwell.ins.constants.Constants;
import com.everwell.ins.enums.Language;
import com.everwell.ins.enums.SmsGateway;
import com.everwell.ins.exceptions.ValidationException;
import com.everwell.ins.exceptions.VendorException;
import com.everwell.ins.models.dto.SmsDto;
import com.everwell.ins.models.dto.VendorResponseDto;
import com.everwell.ins.models.dto.vendorConfigs.SmsPohConfigDto;
import com.everwell.ins.models.dto.vendorCreds.ApiKeySecretDto;
import com.everwell.ins.models.http.responses.SmsPohResponse;
import com.everwell.ins.utils.HttpUtils;
import com.everwell.ins.utils.Utils;
import com.google.gson.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
@Scope(value = "prototype", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class SmsPohHandler extends SmsHandler{

    private static Logger LOGGER = LoggerFactory.getLogger(SmsPohHandler.class);
    private ApiKeySecretDto creds;
    private SmsPohConfigDto config;

    @Override
    public SmsGateway getVendorGatewayEnumerator() {
        return SmsGateway.SMSPOH;
    }

    @Override
    public void setVendorParams(Long vendorId) {
        config = vendorService.getVendorConfig(vendorId, SmsPohConfigDto.class);
        creds = vendorService.getVendorCredentials(vendorId, ApiKeySecretDto.class);
    }

    @Override
    public String getLanguageMapping(Language language) {
        return null;
    }

    @Override
    public Integer getBatchSize(Boolean isMessageCommon) {
        return (isMessageCommon && null != config.getBulkSmsLimit()) ? Integer.parseInt(config.getBulkSmsLimit()) : Constants.BATCH_SIZE_UNIQUE_SMS;
    }

    public VendorResponseDto vendorCall(List<SmsDto> persons, Language language, Long templateId) {
        if (null == persons) {
            LOGGER.error("[vendorCall] The person list is empty");
            throw new ValidationException("The person list is empty");
        }
        VendorResponseDto responseDto = new VendorResponseDto();
        String phone = persons.stream().map(SmsDto::getPhone)
                .collect(Collectors.joining(","));
        String message = persons.get(0).getMessage();
        try {
            Map<String, String> headers = new HashMap<>();
            headers.put("Authorization", "Bearer " + creds.getApiKey());
            JsonObject requestBody = new JsonObject();
            requestBody.addProperty("message", message);
            requestBody.addProperty("to", phone);
            requestBody.addProperty("sender", "CareConnect");
            LocalDateTime start = LocalDateTime.now();
            String response = HttpUtils.postJsonRequest(creds.getUrl(), requestBody.toString(), headers);
            Long elapsed = Duration.between(start, LocalDateTime.now()).toMillis();
            LOGGER.info("[VendorCall] :  Sent SMS " + message + " to " + phone + " took " + elapsed + "ms, response = " + responseDto.getApiResponse());
            SmsPohResponse responseObject = Utils.jsonToObject(response, SmsPohResponse.class);
            responseDto.setApiResponse(response);
            responseDto.setMessageId(Objects.toString(responseObject.getData().getMessages().get(0).getId(), null));
        } catch (Exception e) {
            String errorMessage = "SmsPoh unable to send SMS to " + phone + ", exception: " + e;
            LOGGER.error("[vendorCall] " + errorMessage);
            throw new VendorException(errorMessage, fetchExtraData(getVendorGatewayEnumerator().toString(), phone, message, null));
        }
        return responseDto;
    }

}
