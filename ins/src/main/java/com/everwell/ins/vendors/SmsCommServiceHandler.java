package com.everwell.ins.vendors;

import com.everwell.ins.enums.Language;
import com.everwell.ins.enums.SmsGateway;
import com.everwell.ins.exceptions.NotImplementedException;
import com.everwell.ins.exceptions.ValidationException;
import com.everwell.ins.exceptions.VendorException;
import com.everwell.ins.models.dto.SmsDto;
import com.everwell.ins.models.dto.VendorResponseDto;
import com.everwell.ins.models.dto.vendorConfigs.CommServiceConfigDto;
import com.everwell.ins.models.dto.vendorCreds.ClientIdUrlDto;
import com.everwell.ins.models.http.requests.CommServiceRequest;
import com.everwell.ins.utils.HttpUtils;
import com.everwell.ins.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Component
@Scope(value = "prototype", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class SmsCommServiceHandler extends SmsHandler {

    private static Logger LOGGER = LoggerFactory.getLogger(SmsCommServiceHandler.class);

    private CommServiceConfigDto config;

    private ClientIdUrlDto creds;

    @Override
    public void setVendorParams(Long vendorId) {
        config = vendorService.getVendorConfig(vendorId, CommServiceConfigDto.class);
        creds = vendorService.getVendorCredentials(vendorId, ClientIdUrlDto.class);
    }

    @Override
    public SmsGateway getVendorGatewayEnumerator() {
        return SmsGateway.COMMSERVICE;
    }

    @Override
    public String getLanguageMapping(Language language) {
        throw new NotImplementedException("Unimplemented method");
    }

    @Override
    public VendorResponseDto vendorCall(List<SmsDto> persons, Language language, Long templateId) {
        if (null == persons) {
            LOGGER.error("[vendorCall] The person list is empty");
            throw new ValidationException("The person list is empty");
        }
        VendorResponseDto responseDto = new VendorResponseDto();
        String message = persons.get(0).getMessage();
        String phone = persons.stream().map(SmsDto::getPhone)
                .collect(Collectors.joining(","));
        Boolean isMessageCommon = false;
        try {
            List<SmsDto> personList = new ArrayList<>();
            persons.forEach(p -> {
                personList.add(new SmsDto(p.getId(), p.getPhone(), p.getMessage()));
            });
            CommServiceRequest request = new CommServiceRequest(message, config.getDeploymentCode(), isMessageCommon,
                    config.getSmsGateway(), personList);
            Map<String, String> headers = new HashMap<String, String>() {{
                put("Comm-Service-Client-Id", creds.getClientId());
            }};
            LocalDateTime start = LocalDateTime.now();
            String respString = HttpUtils.postJsonRequest(creds.getUrl(), Utils.asJsonString(request), headers);
            responseDto.setApiResponse(respString);
            Long elapsed = Duration.between(start, LocalDateTime.now()).toMillis();
            LOGGER.info("[VendorCall] :  Sent SMS " + message + " to " + phone + " took " + elapsed.toString() + "ms, response = " + responseDto.getApiResponse());
        } catch (Exception e) {
            String errorMessage = "CommService unable to send SMS to " + phone + ", exception: " + e;
            LOGGER.error("[vendorCall] " + errorMessage);
            throw new VendorException(errorMessage, fetchExtraData(getVendorGatewayEnumerator().toString(), phone, message, templateId));

        }
        return responseDto;
    }

}
