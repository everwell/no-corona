package com.everwell.ins.vendors;

import com.everwell.ins.binders.INSEventBinder;
import com.everwell.ins.constants.Constants;
import com.everwell.ins.constants.RabbitMQConstants;
import com.everwell.ins.models.dto.EventStreamingDto;
import com.everwell.ins.models.http.requests.DeactivateDeviceIdsRequest;
import com.everwell.ins.services.RabbitMQPublisherService;
import com.everwell.ins.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.event.EventListener;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class SpringEventHandler {
    @Autowired
    RabbitMQPublisherService rabbitMQPublisherService;

    private static Logger LOGGER = LoggerFactory.getLogger(SpringEventHandler.class);

    @EventListener
    public void deactivateDeviceIdEvent(DeactivateDeviceIdsRequest deactivateDeviceIdsRequest) {
        LOGGER.info("Deactivate Device ID Event called" + deactivateDeviceIdsRequest.toString());
        EventStreamingDto<DeactivateDeviceIdsRequest> event = new EventStreamingDto<>(RabbitMQConstants.INVALID_DEVICE_ID, deactivateDeviceIdsRequest);
        Map<String, Object> headers = new HashMap<>();
        headers.put(Constants.RMQ_HEADER_CLIENT_ID, deactivateDeviceIdsRequest.getClientId());
        rabbitMQPublisherService.send(Utils.asJsonString(event),RabbitMQConstants.INVALID_DEVICE_ID ,RabbitMQConstants.HEADERS_EXCHANGE,headers);
    }
}
