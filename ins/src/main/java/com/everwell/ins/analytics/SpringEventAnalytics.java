package com.everwell.ins.analytics;

import com.everwell.ins.binders.INSEventBinder;
import com.everwell.ins.enums.AnalyticEventActionEnum;
import com.everwell.ins.enums.AnalyticEventCategoryEnum;
import com.everwell.ins.models.db.Client;
import com.everwell.ins.models.db.Trigger;
import com.everwell.ins.models.db.Vendor;
import com.everwell.ins.models.dto.EventStreamingAnalyticsDto;
import com.everwell.ins.models.http.requests.EmailRequest;
import com.everwell.ins.models.http.requests.PushNotificationRequest;
import com.everwell.ins.models.http.requests.SmsRequest;
import com.everwell.ins.repositories.ClientRepository;
import com.everwell.ins.services.ClientService;
import com.everwell.ins.services.TriggerService;
import com.everwell.ins.services.VendorService;
import com.everwell.ins.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.event.EventListener;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestHeader;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.*;

@Component
@EnableBinding(INSEventBinder.class)
public class SpringEventAnalytics {

    private static Logger LOGGER = LoggerFactory.getLogger(SpringEventAnalytics.class);

    @Autowired
    protected VendorService vendorService;

    @Autowired
    INSEventBinder insEventBinder;

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    protected TriggerService triggerService;

    @Autowired
    protected ClientService clientService;

    @Value("${ins.default.client_id}")
    Long defaultClientId;

    private Map<Long, Long> clientIdTriggerIdMap = new HashMap<>();

    @PostConstruct
    public void init() {
        for(Client client : clientRepository.findAll()) {
            clientIdTriggerIdMap.put(client.getId(), client.getEventFlowId());
        }
    }

    public List<EventStreamingAnalyticsDto>  getSMStreamingAnalyticsDTo(SmsRequest smsRequest)
    {
        Vendor vendor = vendorService.getVendor(smsRequest.getVendorId());
        String vendorName = vendor.getGateway().concat("_").concat(vendor.getId().toString());
        Trigger trigger = triggerService.getTrigger(smsRequest.getTriggerId());
        String triggerName = trigger != null && trigger.getTriggerName() != null ? trigger.getTriggerName() : smsRequest.getTriggerId().toString();
        String templateName = smsRequest.getTemplateId().toString();
        List<EventStreamingAnalyticsDto> eventStreamingAnalyticsDto = new ArrayList<>();
        smsRequest.getPersonList().forEach(p -> {
            eventStreamingAnalyticsDto.add(new EventStreamingAnalyticsDto(AnalyticEventCategoryEnum.OUTBOUNDSMS.toString(), AnalyticEventActionEnum.VENDOR_ID.toString(), vendorName));
            eventStreamingAnalyticsDto.add(new EventStreamingAnalyticsDto(AnalyticEventCategoryEnum.OUTBOUNDSMS.toString(), AnalyticEventActionEnum.TEMPLATE_ID.toString(), templateName));
            eventStreamingAnalyticsDto.add(new EventStreamingAnalyticsDto(AnalyticEventCategoryEnum.OUTBOUNDSMS.toString(), AnalyticEventActionEnum.TRIGGER_ID.toString(), triggerName));
        });
        return eventStreamingAnalyticsDto;
    }

    @EventListener
    public void trackSmsEvent(SmsRequest smsRequest) {
        List<EventStreamingAnalyticsDto> eventStreamingAnalyticsDto=getSMStreamingAnalyticsDTo(smsRequest);
        Long id = smsRequest.getClientId();
        LOGGER.info("[trackSMSEvent] called - " + eventStreamingAnalyticsDto.toString());
        eventStreamingAnalyticsDto.forEach( event-> {
                insEventBinder.eventOutput().send(MessageBuilder.withPayload(Utils.asJsonString(event)).setHeader("CLIENT_ID",clientIdTriggerIdMap.get(id)).build());
        }
        );
    }

    @EventListener
    public void trackReconciliationEvent(String eventName) throws IOException {
        EventStreamingAnalyticsDto eventStreamingAnalyticsDto=new EventStreamingAnalyticsDto(AnalyticEventCategoryEnum.RECONCILIATION.toString(), AnalyticEventActionEnum.VENDOR.toString(), eventName);
        LOGGER.info("[trackReconciliationEvent] called - " + eventStreamingAnalyticsDto.toString());
        insEventBinder.eventOutput().send(MessageBuilder.withPayload(Utils.asJsonString(eventStreamingAnalyticsDto))
            .setHeader("CLIENT_ID", clientIdTriggerIdMap.get(defaultClientId))
            .build());
    }

    public List<EventStreamingAnalyticsDto>  getPNStreamingAnalyticsDTo(PushNotificationRequest pushNotificationRequest)
    {
        Vendor vendor = vendorService.getVendor(pushNotificationRequest.getVendorId());
        String vendorName = vendor.getGateway().concat("_").concat(vendor.getId().toString());
        String triggerName = pushNotificationRequest.getTriggerId().toString();
        String templateName = pushNotificationRequest.getTemplateId().toString();
        List<EventStreamingAnalyticsDto> eventStreamingAnalyticsDto = new ArrayList<>();
        pushNotificationRequest.getPersonList().forEach(p -> {
            eventStreamingAnalyticsDto.add(new EventStreamingAnalyticsDto(AnalyticEventCategoryEnum.PUSHNOTIFICATION.toString(), AnalyticEventActionEnum.VENDOR_ID.toString(), vendorName));
            eventStreamingAnalyticsDto.add(new EventStreamingAnalyticsDto(AnalyticEventCategoryEnum.PUSHNOTIFICATION.toString(), AnalyticEventActionEnum.TEMPLATE_ID.toString(), templateName));
            eventStreamingAnalyticsDto.add(new EventStreamingAnalyticsDto(AnalyticEventCategoryEnum.PUSHNOTIFICATION.toString(), AnalyticEventActionEnum.TRIGGER_ID.toString(), triggerName));
        });
        return eventStreamingAnalyticsDto;
    }

    @EventListener
    public void trackPNEvent(PushNotificationRequest pushNotificationRequest) {
        List<EventStreamingAnalyticsDto> eventStreamingAnalyticsDto= getPNStreamingAnalyticsDTo(pushNotificationRequest);
        Long id = null == pushNotificationRequest.getClientId() ?  defaultClientId : pushNotificationRequest.getClientId();
        LOGGER.info("[trackPNEvent] called - " + eventStreamingAnalyticsDto.toString());
        eventStreamingAnalyticsDto.forEach( event-> {
                    insEventBinder.eventOutput().send(MessageBuilder.withPayload(Utils.asJsonString(event)).setHeader("CLIENT_ID",clientIdTriggerIdMap.get(id)).build());
                }
        );
    }

    public List<EventStreamingAnalyticsDto>  getEmailStreamingAnalyticsDTo(EmailRequest emailRequest)
    {
        Vendor vendor = vendorService.getVendor(emailRequest.getVendorId());
        String vendorName = vendor.getGateway().concat("_").concat(vendor.getId().toString());
        String triggerName = emailRequest.getTriggerId().toString();
        String templateName = emailRequest.getTemplateId().toString();
        List<EventStreamingAnalyticsDto> eventStreamingAnalyticsDto = new ArrayList<>();
        emailRequest.getRecipientList().forEach(p -> {
            eventStreamingAnalyticsDto.add(new EventStreamingAnalyticsDto(AnalyticEventCategoryEnum.EMAIL.toString(), AnalyticEventActionEnum.VENDOR_ID.toString(), vendorName));
            eventStreamingAnalyticsDto.add(new EventStreamingAnalyticsDto(AnalyticEventCategoryEnum.EMAIL.toString(), AnalyticEventActionEnum.TEMPLATE_ID.toString(), templateName));
            eventStreamingAnalyticsDto.add(new EventStreamingAnalyticsDto(AnalyticEventCategoryEnum.EMAIL.toString(), AnalyticEventActionEnum.TRIGGER_ID.toString(), triggerName));
        });
        return eventStreamingAnalyticsDto;
    }

    @EventListener
    public void trackEmailEvent(EmailRequest emailRequest) {
        List<EventStreamingAnalyticsDto> eventStreamingAnalyticsDto=getEmailStreamingAnalyticsDTo(emailRequest);
        Long id = emailRequest.getClientId();
        LOGGER.info("[trackEmailEvent] called - " + eventStreamingAnalyticsDto.toString());
        eventStreamingAnalyticsDto.forEach( event-> {
                insEventBinder.eventOutput().send(MessageBuilder.withPayload(Utils.asJsonString(event)).setHeader("CLIENT_ID",clientIdTriggerIdMap.get(id)).build());
            }
        );
    }
}
