package com.everwell.ins.controllers;


import com.everwell.ins.models.db.Template;
import com.everwell.ins.models.http.requests.TemplateRequest;
import com.everwell.ins.models.http.responses.Response;
import com.everwell.ins.models.http.responses.TemplateResponse;
import com.everwell.ins.services.LanguageService;
import com.everwell.ins.services.TemplateService;
import com.everwell.ins.services.TypeService;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class TemplateController {

    private static Logger LOGGER = LoggerFactory.getLogger(TemplateController.class);

    @Autowired
    private TemplateService templateService;

    @Autowired
    private TypeService typeService;

    @Autowired
    private LanguageService languageService;

    @ApiOperation(
            value = "Save Templates",
            notes = "Accept template content, parameters along with language and type Ids to save in database"
    )
    @PostMapping(value = "/v1/template")
    public ResponseEntity<Response<Long>> saveTemplate(@RequestBody TemplateRequest templateRequest) {
        LOGGER.info("[saveTemplate] template request accepted");
        templateRequest.validate(templateRequest);
        Long templateId = templateService.saveTemplate(templateRequest);
        Response<Long> response = new Response<>(true, templateId);
        return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
    }

    @ApiOperation(
            value = "Get All Templates",
            notes = "Shows the template related information, for all templates"
    )
    @GetMapping(value = "/v1/template")
    public ResponseEntity<Response<List<TemplateResponse>>> getAllTemplates() {
        LOGGER.info("[getAllTemplate] get template request accepted");
        List<Template> templates = templateService.getAllTemplates();
        List<TemplateResponse> templateResponse = templates
                .stream()
                .map(m -> new TemplateResponse(m.getId(),
                        m.getContent(),
                        m.getParameters(),
                        typeService.getType(m.getTypeId()).getType(),
                        languageService.getLanguage(m.getLanguageId()).getLanguage()))
                .collect(Collectors.toList());
        Response<List<TemplateResponse>> response = new Response<>(true, templateResponse);
        LOGGER.info("[getAllTemplate] response generated: " + response);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation(
            value = "Get Template",
            notes = "Shows the template related information, for particular template Id"
    )
    @GetMapping(value = "/v1/template/{id}")
    public ResponseEntity<Response<TemplateResponse>> getTemplate(@PathVariable Long id) {
        LOGGER.info("[getTemplate] get template request accepted");
        Template template = templateService.getTemplate(id);
        TemplateResponse templateResponse = new TemplateResponse(template.getId(),
                template.getContent(),
                template.getParameters(),
                typeService.getType(template.getTypeId()).getType(),
                languageService.getLanguage(template.getLanguageId()).getLanguage());
        Response<TemplateResponse> response = new Response<>(true, templateResponse);
        LOGGER.info("[getTemplate] response generated: " + response);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

}
