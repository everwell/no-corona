package com.everwell.ins.controllers;

import com.everwell.ins.exceptions.ValidationException;
import com.everwell.ins.models.dto.TypeMapping;
import com.everwell.ins.models.http.requests.TypeMappingRequest;
import com.everwell.ins.models.http.responses.Response;
import com.everwell.ins.models.http.responses.TypeResponse;
import com.everwell.ins.services.TypeService;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class TypeController {

    private static Logger LOGGER = LoggerFactory.getLogger(TypeController.class);

    @Autowired
    private TypeService typeService;

    @ApiOperation(
            value = "Get Type Mapping",
            notes = "Fetch type Id mappings"
    )
    @PostMapping(value = "/v1/type")
    public ResponseEntity<Response<TypeResponse>> getTypeMappings(@RequestBody TypeMappingRequest typeMappingRequest) {
        LOGGER.info("[getTypeMappings] get type mapping request accepted");
        typeMappingRequest.validate(typeMappingRequest);
        List<TypeMapping> typeList = typeService.getTypeMappings(typeMappingRequest.getTypeIds());
        Response<TypeResponse> response = new Response<>(true, new TypeResponse(typeList));
        LOGGER.info("[getTypeMappings] response generated: " + response);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping(value = "/v1/allTypes")
    public ResponseEntity<Response<TypeResponse>> getAllTypeMappings() {
        LOGGER.info("[getAllTypeMappings] get type mapping request accepted");
        List<TypeMapping> typeList = typeService.getAllTypeMappings();
        Response<TypeResponse> response = new Response<>(true, new TypeResponse(typeList));
        LOGGER.info("[getAllTypeMappings] response generated: " + response);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
