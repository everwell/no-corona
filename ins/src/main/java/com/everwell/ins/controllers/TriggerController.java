package com.everwell.ins.controllers;


import com.everwell.ins.exceptions.NotFoundException;
import com.everwell.ins.models.db.Trigger;
import com.everwell.ins.models.http.responses.Response;
import com.everwell.ins.models.http.responses.TriggerResponse;
import com.everwell.ins.services.TriggerService;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class TriggerController {

    private static Logger LOGGER = LoggerFactory.getLogger(TriggerController.class);

    @Autowired
    private TriggerService triggerService;

    @ApiOperation(
            value = "Get All Triggers",
            notes = "Shows the trigger related information, for all triggers"
    )
    @GetMapping(value = "/v1/trigger")
    public ResponseEntity<Response<List<TriggerResponse>>> getAllTriggers() {
        LOGGER.info("[getAllTriggers] get triggers request accepted");
        List<Trigger> triggers = triggerService.getAllTriggers();
        List<TriggerResponse> triggerResponse = triggers
                .stream()
                .map(m -> new TriggerResponse(m.getId(),
                        m.getTriggerName(),
                        m.getTrigger(),
                        m.getParameters(),
                        m.getTemplateId()))
                .collect(Collectors.toList());
        Response<List<TriggerResponse>> response = new Response<>(true, triggerResponse);
        LOGGER.info("[getAllTriggers] response generated: " + response);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation(
            value = "Get Trigger",
            notes = "Shows the trigger related information, for particular trigger Id"
    )
    @GetMapping(value = "/v1/trigger/{id}")
    public ResponseEntity<Response<TriggerResponse>> getTrigger(@PathVariable Long id) {
        LOGGER.info("[getTrigger] get trigger request accepted");
        Trigger trigger = triggerService.getTrigger(id);
        if (null == trigger) {
            throw new NotFoundException("Trigger with id " + id + " not found!");
        }
        TriggerResponse triggerResponse = new TriggerResponse(trigger.getId(),
                trigger.getTriggerName(),
                trigger.getTrigger(),
                trigger.getParameters(),
                trigger.getTemplateId());
        Response<TriggerResponse> response = new Response<>(true, triggerResponse);
        LOGGER.info("[getTrigger] response generated: " + response);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

}
