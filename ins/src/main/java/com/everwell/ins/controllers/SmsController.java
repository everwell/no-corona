package com.everwell.ins.controllers;


import com.everwell.ins.models.dto.SmsDetailedRequestDto;
import com.everwell.ins.models.dto.SmsRequestDto;
import com.everwell.ins.models.dto.SmsTemplateDto;
import com.everwell.ins.models.http.requests.SmsDetailedRequest;
import com.everwell.ins.models.http.requests.SmsTemplateRequest;
import com.everwell.ins.models.http.responses.Response;
import com.everwell.ins.models.http.requests.SmsRequest;
import com.everwell.ins.services.*;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

import static com.everwell.ins.enums.EngagementType.SMS;

@RestController
public class SmsController {

    private static Logger LOGGER = LoggerFactory.getLogger(SmsController.class);

    @Autowired
    private SmsService smsService;

    @Autowired
    private TemplateService templateService;

    @Autowired
    private TriggerService triggerService;

    @Autowired
    private EngagementService engagementService;

    @Autowired
    private ClientService clientService;

    @ApiOperation(
            value = "Send Bulk SMS",
            notes = "Accept phones numbers along with messages and sends out via sms gateway based on vendorId"
    )
    @PostMapping(value = "/v1/sms/bulk")
    public ResponseEntity<Response<String>> sendBulk(@RequestBody SmsRequest smsRequest) {
        LOGGER.info("[sendBulk] sms request accepted");
        smsRequest.validate(smsRequest);
        smsRequest.setClientId(clientService.getCurrentClient().getId());
        smsService.sendSms(smsRequest);
        return new ResponseEntity<>(new Response<>(null, "sms request accepted"), HttpStatus.ACCEPTED);
    }

    @ApiOperation(
            value = "Send Bulk SMS with Template Ids",
            notes = "Accept template Ids along with dynamic parameters and sends out sms"
    )
    @PostMapping(value = "/v2/sms/bulk")
    public ResponseEntity<Response<SmsTemplateRequest>> sendBulkWithTemplateId(@RequestBody SmsTemplateRequest smsTemplateRequest) {
        LOGGER.info("[sendBulkAlongWithTemplateIds] sms request accepted");
        smsTemplateRequest.validate(smsTemplateRequest);
        if (null == smsTemplateRequest.getTemplateId())
            smsTemplateRequest.setTemplateId(triggerService.getTrigger(smsTemplateRequest.getTriggerId()).getTemplateId());
        smsTemplateRequest.setClientId(clientService.getCurrentClient().getId());
        SmsRequestDto smsRequestDto = templateService.createSms(smsTemplateRequest);
        if (!CollectionUtils.isEmpty(smsRequestDto.getSmsRequest().getPersonList())) {
            if (!smsTemplateRequest.getIsMandatory())
                engagementService.modifyList(smsRequestDto, SMS.getId());
            smsService.sendSms(smsRequestDto.getSmsRequest());
        }
        SmsTemplateRequest response = null;
        String message = null;
        boolean success = true;
        HttpStatus status = HttpStatus.ACCEPTED;
        if (CollectionUtils.isEmpty(smsRequestDto.getFailedSmsRequest())) {
            message = "sms request accepted";
        } else if (smsRequestDto.getFailedSmsRequest().size() == smsTemplateRequest.getPersonList().size()) {
            response = smsTemplateRequest;
            success = false;
            message = "sms request failed";
            status = HttpStatus.BAD_REQUEST;
        } else {
            response = new SmsTemplateRequest(smsTemplateRequest.getTemplateId(), smsRequestDto.getFailedSmsRequest(), smsTemplateRequest.getVendorId(), smsTemplateRequest.getTriggerId());
            message = "sms request partially failed";
        }
        Response<SmsTemplateRequest> finalResponse = new Response<>(success, response, message);
        return new ResponseEntity<>(finalResponse, status);
    }

    @ApiOperation(
            value = "Send Bulk SMS with template and trigger Id",
            notes = "Accept template Id, trigger Id along with dynamic parameters and sends out sms based on engagement config"
    )
    @PostMapping(value = "/v3/sms/bulk")
    public ResponseEntity<Response<List<SmsTemplateDto>>> sendBulkSms(@RequestBody SmsDetailedRequest smsDetailedRequest) {
        LOGGER.info("[sendBulkSms] sms request accepted");
        if (null == smsDetailedRequest.getClientId()) {
            smsDetailedRequest.setClientId(clientService.getCurrentClient().getId());
        }
        smsDetailedRequest.validate(smsDetailedRequest, String.valueOf(smsDetailedRequest.getClientId()));
        engagementService.filterSmsList(smsDetailedRequest, SMS.getId());
        SmsDetailedRequestDto smsDetailedRequestDto = null;
        if (!CollectionUtils.isEmpty(smsDetailedRequest.getPersonList())) {
            smsDetailedRequestDto = templateService.constructSms(smsDetailedRequest);
            smsService.sendBulkSms(smsDetailedRequestDto.getSmsRequests());
        }
        List<SmsTemplateDto> response = new ArrayList<>();
        String message = null;
        boolean success = true;
        HttpStatus status = HttpStatus.ACCEPTED;
        if (null == smsDetailedRequestDto || CollectionUtils.isEmpty(smsDetailedRequestDto.getFailedSmsRequest())) {
            message = "sms request accepted";
        } else if (smsDetailedRequestDto.getFailedSmsRequest().size() == smsDetailedRequest.getPersonList().size()) {
            response = smsDetailedRequest.getPersonList();
            success = false;
            message = "sms request failed";
            status = HttpStatus.BAD_REQUEST;
        } else {
            response = smsDetailedRequestDto.getFailedSmsRequest();
            message = "sms request partially failed";
        }
        Response<List<SmsTemplateDto>> finalResponse = new Response<>(success, response, message);
        return new ResponseEntity<>(finalResponse, status);
    }

}
