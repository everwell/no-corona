package com.everwell.ins.constants;

public class RabbitMQConstants {
    public static String DLX_EXCHANGE = "ex.ins.dlx";
    public static String WAIT_EXCHANGE = "ex.ins.wait";
    public static String RETRY_COUNT_HEADER = "x-retry-count";
    public static int RMQ_MAX_RETRY_COUNT = 3;
    public static String INVALID_DEVICE_ID = "q.invalid_device_id";
    public static String EX_DIRECT = "direct";
    public static String HEADERS_EXCHANGE = "ex.ins.headers";
    public static String DELAYED_EXCHANGE = "ex.ins.delayed";
    public static String DELAY_HEADER = "x-delay";
}
