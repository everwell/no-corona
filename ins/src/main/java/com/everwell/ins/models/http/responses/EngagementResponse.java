package com.everwell.ins.models.http.responses;

import com.everwell.ins.models.db.Engagement;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.util.Date;

@AllArgsConstructor
@ToString
@Getter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class EngagementResponse {
    String entityId;
    Boolean consent;
    Date time;
    Long typeId;
    String type;
    String language;
    Long languageId;

    public EngagementResponse(Engagement engagement, String type, String language) {
        this.entityId = engagement.getEntityId();
        this.consent = engagement.getConsent();
        this.time = engagement.getTime();
        this.languageId = engagement.getLanguageId();
        this.typeId = engagement.getTypeId();
        this.type = type;
        this.language = language;
    }
}
