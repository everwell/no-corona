package com.everwell.ins.models.http.responses;

import com.everwell.ins.models.dto.TypeMapping;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

import java.util.List;

@AllArgsConstructor
@ToString
@Getter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TypeResponse {
    List<TypeMapping> typeMappings;
}
