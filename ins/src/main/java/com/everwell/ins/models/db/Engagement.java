package com.everwell.ins.models.db;

import com.everwell.ins.exceptions.ValidationException;
import com.everwell.ins.models.http.requests.EngagementRequest;
import com.everwell.ins.utils.Utils;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.lang.reflect.Field;
import java.sql.Time;
import java.text.ParseException;
import java.util.Date;

@Entity
@Table(name = "ins_engagement")
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class Engagement {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Setter
    private Long id;
    @Column(name = "entity_id")
    private String entityId;
    @Column(name = "consent")
    private Boolean consent;
    @Column(name = "time")
    @Temporal(TemporalType.TIME)
    private Date time;
    @Column(name = "language_id")
    private Long languageId;
    @Column(name = "type_id")
    private Long typeId;

    public Engagement(String entityId, Boolean consent, Date time, Long languageId, Long typeId) {
        this.entityId = entityId;
        this.consent = consent;
        this.time = time;
        this.languageId = languageId;
        this.typeId = typeId;
    }

    public Engagement update(Engagement other) {
        Class klass = getClass();
        for (Field field : klass.getDeclaredFields()) {
            try {
                if (field.get(other) != null) {
                    field.set(this, field.get(other));
                }
            } catch (IllegalAccessException e) {
            }
        }
        return this;
    }

    public Engagement(EngagementRequest engagementRequest) {
        this.entityId = engagementRequest.getEntityId();
        this.consent = engagementRequest.getConsent();
        if (null != engagementRequest.getTimeOfDay()) {
            try {
                this.time = Utils.convertStringToDate(engagementRequest.getTimeOfDay());
            } catch (ParseException e) {
                throw new ValidationException("unable to parse date");
            }
        }
        this.languageId = engagementRequest.getLanguageId();
        this.typeId = engagementRequest.getTypeId();
    }
}
