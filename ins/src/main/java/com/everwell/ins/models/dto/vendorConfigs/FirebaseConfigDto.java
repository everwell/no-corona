package com.everwell.ins.models.dto.vendorConfigs;

import lombok.*;

@Data
public class FirebaseConfigDto extends VendorConfigsBase{
    String bulkPNLimit;
}
