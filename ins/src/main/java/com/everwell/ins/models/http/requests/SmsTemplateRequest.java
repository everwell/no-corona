package com.everwell.ins.models.http.requests;

import com.everwell.ins.exceptions.ValidationException;
import com.everwell.ins.models.dto.SmsTemplateDto;
import lombok.*;
import org.springframework.util.CollectionUtils;

import java.util.List;

@NoArgsConstructor
@Getter
@Setter
@ToString
public class SmsTemplateRequest {
    Long templateId;
    List<SmsTemplateDto> personList;
    Long vendorId;
    Long triggerId = 0L;
    Boolean isMandatory = true;
    Boolean defaultConsent = true;
    Long clientId;

    public SmsTemplateRequest(Long templateId, List<SmsTemplateDto> personList, Long vendorId, Long triggerId) {
        this.personList = personList;
        this.vendorId = vendorId;
        this.templateId = templateId;
        this.triggerId = triggerId != null ? triggerId : 0L;
    }

    public SmsTemplateRequest(Long templateId, List<SmsTemplateDto> personList, Long vendorId, Long triggerId, Boolean isMandatory, Boolean defaultConsent) {
        this.personList = personList;
        this.vendorId = vendorId;
        this.templateId = templateId;
        this.triggerId = triggerId != null ? triggerId : 0L;
        this.isMandatory = isMandatory != null ? isMandatory : true;
        this.defaultConsent = defaultConsent != null ? defaultConsent : true;
    }

    public void validate(SmsTemplateRequest smsTemplateRequest) {
        if (CollectionUtils.isEmpty(smsTemplateRequest.getPersonList())) {
            throw new ValidationException("personList should not be empty");
        }
        if (null == smsTemplateRequest.getVendorId()) {
            throw new ValidationException("vendor id is required");
        }
        if (null == smsTemplateRequest.getTemplateId() && smsTemplateRequest.getTriggerId() == 0) {
            throw new ValidationException("template id is required");
        }
    }
}
