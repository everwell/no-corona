package com.everwell.ins.models.dto.vendorConfigs;

import lombok.*;

@Data
public class NICConfigDto extends VendorConfigsBase {
    String bulkSmsLimit;
    String language;
    String unicodeLanguage;
    String dlrType;
}
