package com.everwell.ins.models.http.requests;

import com.everwell.ins.exceptions.ValidationException;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@NoArgsConstructor
public class DeleteNotificationRequest {
    Long notificationId;
    Map<String, Object> notificationExtraKeys;

    public void validate() {
        if (notificationId == null && notificationExtraKeys == null) {
            throw new ValidationException("Id and extra data cannot be empty!");
        }
    }
}
