package com.everwell.ins.models.dto.vendorConfigs;

import lombok.Data;

@Data
public class InterraConfigDto extends VendorConfigsBase {
    String bulkSmsLimit;
}
