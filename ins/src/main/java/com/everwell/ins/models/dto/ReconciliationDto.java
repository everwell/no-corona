package com.everwell.ins.models.dto;

import lombok.*;
import com.everwell.ins.exceptions.ValidationException;
import org.springframework.util.CollectionUtils;

import java.io.Serializable;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class ReconciliationDto implements Serializable {
    List<Long> vendorIds;
    String messageId;
    String status;
    String to;
    Boolean retry = true;

    public ReconciliationDto(List<Long> vendorIds, String messageId, String status, String to) {
        this.vendorIds = vendorIds;
        this.messageId = messageId;
        this.status = status;
        this.to = to;
    }
    public void validate(ReconciliationDto reconciliationDto) {
        if (CollectionUtils.isEmpty(reconciliationDto.getVendorIds())) {
            throw new ValidationException("Vendor list should not be empty");
        }
        if (null == reconciliationDto.getTo()) {
            throw new ValidationException("Phone number is required");
        }
        if (null == reconciliationDto.getMessageId()) {
            throw new ValidationException("Message Id is required");
        }
        if (null == reconciliationDto.getStatus()) {
            throw new ValidationException("Status is required");
        }
    }

}
