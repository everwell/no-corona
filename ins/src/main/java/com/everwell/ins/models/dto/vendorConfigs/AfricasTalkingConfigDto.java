package com.everwell.ins.models.dto.vendorConfigs;

import lombok.*;

@Data
public class AfricasTalkingConfigDto extends VendorConfigsBase {
    String bulkSmsLimit;
    String language;
    String unicodeLanguage;
}
