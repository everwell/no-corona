package com.everwell.ins.models.dto.vendorCreds;

import lombok.Data;

@Data
public class ClientIdUrlDto extends VendorCredsBase {
    String clientId;
    String url;
}
