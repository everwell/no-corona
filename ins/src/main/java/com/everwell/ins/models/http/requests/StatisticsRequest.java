package com.everwell.ins.models.http.requests;

import com.everwell.ins.exceptions.ValidationException;
import lombok.*;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class StatisticsRequest {
    Long triggerId;
    Long templateId;
    Long vendorId;
    String from;
    String to;

    public void validate(StatisticsRequest statisticsRequest) {
        if (null == statisticsRequest.getVendorId()) {
            throw new ValidationException("vendor id is required");
        }
        if (null == statisticsRequest.getFrom()) {
            throw new ValidationException("from range filter is required");
        }
        if (null == statisticsRequest.getTo()) {
            throw new ValidationException("to range filter is required");
        }
    }
}
