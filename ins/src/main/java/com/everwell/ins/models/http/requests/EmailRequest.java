package com.everwell.ins.models.http.requests;

import com.everwell.ins.enums.Language;
import com.everwell.ins.exceptions.ValidationException;
import com.everwell.ins.models.dto.EmailAttachmentDto;
import com.everwell.ins.models.dto.EmailTemplateDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.regex.Pattern;

@NoArgsConstructor
@Getter
@Setter
@ToString
public class EmailRequest {
    String body;
    String subject;
    Boolean isEmailCommon;
    List<EmailTemplateDto> recipientList;
    EmailAttachmentDto attachmentDto;
    String sender;
    Long vendorId;
    Language language = Language.NON_UNICODE;
    Long templateId;
    Long triggerId;
    Long clientId = 1L;

    public EmailRequest(Long vendorId, Long triggerId, Long templateId, String sender, List<EmailTemplateDto> recipientList)
    {
        this.vendorId = vendorId;
        this.templateId = templateId;
        this.triggerId = triggerId;
        this.sender = sender;
        this.recipientList = recipientList;
    }

    public EmailRequest(Long vendorId, Long triggerId, Long templateId, String sender, List<EmailTemplateDto> recipientList, EmailAttachmentDto attachmentDto)
    {
        this.vendorId = vendorId;
        this.templateId = templateId;
        this.triggerId = triggerId;
        this.sender = sender;
        this.recipientList = recipientList;
        this.attachmentDto = attachmentDto;
    }

    public static boolean emailPatternMatches(String emailAddress) {
        String regexPattern = "^(?=.{1,64}@)[A-Za-z0-9_-]+(\\.[A-Za-z0-9_-]+)*@" + "[^-][A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$";
        return Pattern.compile(regexPattern)
            .matcher(emailAddress)
            .matches();
    }

    public void validate(EmailRequest emailRequest) {
        if (CollectionUtils.isEmpty(emailRequest.getRecipientList())) {
            throw new ValidationException("personList should not be empty");
        }

        if (null == emailRequest.getVendorId()) {
            throw new ValidationException("vendor id is required");
        }

        if (!StringUtils.hasText(emailRequest.getSender()) || !emailPatternMatches(emailRequest.getSender())) {
            throw new ValidationException("Please enter a valid sender" + emailRequest.getSender());
        }

        if (null == emailRequest.getTriggerId()) {
            throw new ValidationException("Trigger id is required");
        }

        if (null == emailRequest.getTemplateId()) {
            throw new ValidationException("Template id is required");
        }

        if(null != emailRequest.getAttachmentDto())
        {
            if(!StringUtils.hasText(emailRequest.getAttachmentDto().getAttachmentName()))
                throw new ValidationException("Attachment name cannot be null");
            if(!StringUtils.hasText(emailRequest.getAttachmentDto().getAttachmentType()))
                throw new ValidationException("Attachment Type cannot be null");
            if(!StringUtils.hasText(emailRequest.getAttachmentDto().getAttachmentContent()))
                throw new ValidationException("Attachment content cannot be null");
        }

        emailRequest.getRecipientList().forEach(r -> {
            if (!StringUtils.hasText(r.getRecipient()) || !emailPatternMatches(r.getRecipient()))
                throw new ValidationException("Recipient is invalid" + r.getRecipient());
        });

    }
}
