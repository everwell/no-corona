package com.everwell.ins.models.dto;

import com.everwell.ins.models.http.requests.PushNotificationRequest;
import lombok.AllArgsConstructor;
import lombok.Getter;


import java.io.Serializable;
import java.util.List;

@AllArgsConstructor
@Getter
public class PushNotificationRequestDto implements Serializable {
    PushNotificationRequest pushNotificationRequest;
    List<PushNotificationTemplateDto> failedPNRequests;
}

