package com.everwell.ins.models.http.responses;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

@AllArgsConstructor
@ToString
@Getter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TriggerResponse {
    Long triggerId;
    String name;
    String trigger;
    String parameters;
    Long templateId;
}
