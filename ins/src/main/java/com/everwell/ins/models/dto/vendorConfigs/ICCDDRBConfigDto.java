package com.everwell.ins.models.dto.vendorConfigs;

import lombok.*;

@Data
public class ICCDDRBConfigDto extends VendorConfigsBase {
    String bulkSmsLimit;
}
