package com.everwell.ins.models.http.requests;

import com.everwell.ins.exceptions.ValidationException;
import lombok.*;
import org.springframework.util.CollectionUtils;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@ToString
public class EngagementBulkRequest {
    List<EngagementRequest> engagementRequestList;

    public void validate() {
        if (CollectionUtils.isEmpty(engagementRequestList)) {
            throw new ValidationException("Engagement request list cannot be null");
        }

        engagementRequestList.forEach(e -> e.validate(e));
    }
}
