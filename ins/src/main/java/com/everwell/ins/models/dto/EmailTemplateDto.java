package com.everwell.ins.models.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Map;

@NoArgsConstructor
@Getter
@Setter
@ToString
public class EmailTemplateDto implements Serializable {
    String body;
    String subject;
    Map<String,String> bodyParameters;
    Map<String,String> subjectParameters;
    String recipient;
    Long languageId;

    public EmailTemplateDto(String recipient, String body, String subject)
    {
        this.recipient = recipient;
        this.subject = subject;
        this.body = body;
    }

    public EmailTemplateDto(String recipient, Map<String,String> bodyParameters, Map<String,String> subjectParameters)
    {
        this.recipient = recipient;
        this.bodyParameters = bodyParameters;
        this.subjectParameters = subjectParameters;
    }

}
