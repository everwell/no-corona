package com.everwell.ins.models.http.responses;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class SmsPohMessagesResponse {
    private Long id;
    @JsonProperty(value = "create_at")
    private String createAt;
    @JsonProperty(value = "message_to")
    private String messageTo;
    @JsonProperty(value = "message_text")
    private String messageText;
    private String operator;
    private String sender;
    @JsonProperty(value = "is_delivered")
    private String isDelivered;
    @JsonProperty(value = "is_queuing")
    private String isQueuing;
    private String test;
    @JsonProperty(value = "num_parts")
    private Integer numParts;
    private Long credit;
}
