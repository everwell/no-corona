package com.everwell.ins.models.http.requests;

import com.everwell.ins.enums.Language;
import com.everwell.ins.exceptions.ValidationException;
import com.everwell.ins.models.dto.SmsDto;
import lombok.*;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.List;

@NoArgsConstructor
@Getter
@Setter
@ToString
public class SmsRequest {
    String message;
    Boolean isMessageCommon;
    List<SmsDto> personList;
    Long vendorId;
    Language language = Language.NON_UNICODE;
    Long templateId = 0L;
    Long triggerId = 0L;
    Boolean defaultConsent = true;
    Long clientId;

    public SmsRequest(String message, Boolean isMessageCommon, List<SmsDto> personList, Long vendorId, Long templateId, Long triggerId, Boolean defaultConsent) {
        this.message = message;
        this.isMessageCommon = isMessageCommon;
        this.personList = personList;
        this.vendorId = vendorId;
        this.templateId = templateId != null ? templateId : 0L;
        this.triggerId = triggerId != null ? triggerId : 0L;
        this.defaultConsent = defaultConsent != null ? defaultConsent : true;
    }

    public void validate(SmsRequest smsRequest) {
        if (CollectionUtils.isEmpty(smsRequest.getPersonList())) {
            throw new ValidationException("personList should not be empty");
        }
        if (null == smsRequest.getVendorId()) {
            throw new ValidationException("vendor id is required");
        }
        if (null == smsRequest.getIsMessageCommon()) {
            throw new ValidationException("is Message Common required");
        }
        if (smsRequest.getIsMessageCommon()) {
            if (StringUtils.isEmpty(smsRequest.getMessage())) {
                throw new ValidationException("message is required");
            }
        }
    }
}
