package tests.controllers;

import com.everwell.ins.controllers.TypeController;
import com.everwell.ins.exceptions.CustomExceptionHandler;
import com.everwell.ins.models.dto.TypeMapping;
import com.everwell.ins.models.http.requests.TypeMappingRequest;
import com.everwell.ins.models.http.responses.TypeResponse;
import com.everwell.ins.services.TypeService;
import com.everwell.ins.utils.Utils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import tests.BaseTest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class TypeControllerTests extends BaseTest {

    private MockMvc mockMvc;

    @Mock
    private TypeService typeService;

    @InjectMocks
    private TypeController typeController;

    private static String sms = "Sms";
    private static String ivr = "Ivr";
    private static final String MESSAGE_TYPE_IDS_REQUIRED = "type ids are required";

    protected List<TypeMapping> typeMappings() {
        List<TypeMapping> typeMapping = new ArrayList<>();
        typeMapping.add(new TypeMapping(1L, sms));
        typeMapping.add(new TypeMapping(2L, ivr));
        return typeMapping;
    }

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(typeController)
                .setControllerAdvice(new CustomExceptionHandler())
                .build();
    }

    @Test
    public void testGetTypeMappings() throws Exception {
        String uri = "/v1/type";
        List<Long> typeIds = new ArrayList<>();
        typeIds.add(1L);
        typeIds.add(2L);
        TypeMappingRequest typeMappingRequest = new TypeMappingRequest(typeIds);

        when(typeService.getTypeMappings(any())).thenReturn(typeMappings());

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(typeMappingRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.typeMappings[0].type").value(sms))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.typeMappings[1].type").value(ivr));
        ;
        verify(typeService, Mockito.times(1)).getTypeMappings(any());
    }

    @Test
    public void testGetTypeMappingsWithoutTypeIds() throws Exception {
        String uri = "/v1/type";
        List<Long> typeIds = new ArrayList<>();
        TypeMappingRequest typeMappingRequest = new TypeMappingRequest(typeIds);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(typeMappingRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(MESSAGE_TYPE_IDS_REQUIRED));
    }

    @Test
    public void testGetAllTypeMappings() throws Exception {
        String uri = "/v1/allTypes";

        when(typeService.getAllTypeMappings()).thenReturn(typeMappings());

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.typeMappings[0].type").value(sms))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.typeMappings[1].type").value(ivr));
        ;
        verify(typeService, Mockito.times(1)).getAllTypeMappings();
    }
}
