package tests.vendors;

import com.everwell.ins.exceptions.ValidationException;
import com.everwell.ins.models.dto.EmailAttachmentDto;
import com.everwell.ins.models.dto.EmailTemplateDto;
import com.everwell.ins.models.dto.SendgridResponseDto;
import com.everwell.ins.models.dto.vendorConfigs.SendgridConfigDto;
import com.everwell.ins.models.dto.vendorCreds.ApiKeyDto;
import com.everwell.ins.models.http.requests.EmailRequest;
import com.everwell.ins.repositories.EmailLogsRepository;
import com.everwell.ins.services.VendorService;
import com.everwell.ins.utils.SentryUtils;
import com.everwell.ins.vendors.SendgridHandler;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;
import org.junit.Test;
import org.junit.platform.commons.logging.LoggerFactory;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;


@PrepareForTest({SendGrid.class, SentryUtils.class, LoggerFactory.class})
@PowerMockIgnore({"javax.xml.*", "com.sun.org.apache.xerces.*", "org.slf4j.*", "org.xml.*","com.ibm.*","javax.net.ssl.*"})
@RunWith(PowerMockRunner.class)
public class SendgridHandlerTest {

    @InjectMocks
    private SendgridHandler sendgridHandler;

    @Mock
    private SendgridConfigDto config;

    @Mock
    protected VendorService vendorService;

    @Mock
    private ApiKeyDto creds;
    

    String mockCred = "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";

    @Test
    public void testSetVendorParams() {
        Long vendorId = 1L;
        when(vendorService.getVendorConfig(any(), any())).thenReturn(null);
        when(vendorService.getVendorCredentials(any(), any())).thenReturn(null);

        sendgridHandler.setVendorParams(vendorId);
        verify(vendorService, Mockito.times(1)).getVendorConfig(any(), any());
        verify(vendorService, Mockito.times(1)).getVendorCredentials(any(), any());
    }

    @Test
    public void testGetBatchSize()
    {
        Integer size = 5;
        when(config.getBulkEmailLimit()).thenReturn("5");

        Integer response =  sendgridHandler.getBatchSize(true);
        assertEquals(size,response);

        size = 1;
        Integer response2 =  sendgridHandler.getBatchSize(false);
        assertEquals(size,response2);
    }

    @Test
    public void testSendEmail() throws Exception {
        List<EmailTemplateDto> emailTemplateDtos = new ArrayList<>();
        emailTemplateDtos.add(new EmailTemplateDto("test@everwell.org", "body", "subject"));
        EmailAttachmentDto attachmentDto = new EmailAttachmentDto("Attachment Content","testAttachment",".csv");
        EmailRequest emailRequest = new EmailRequest(1L, 1L, 1L, "test@everwell.org", emailTemplateDtos, attachmentDto);

        mockStatic(SendGrid.class);

        when(creds.getApiKey()).thenReturn(mockCred);

        ReflectionTestUtils.setField(sendgridHandler, "serverPort", "");
        sendgridHandler.setBaseUrl();


       SendgridResponseDto responseDto = sendgridHandler.sendgridCall(emailTemplateDtos,emailRequest);
       assertEquals("202", responseDto.getSendgridResponse());
    }


    @Test(expected = ValidationException.class)
    public void testSendEmailNoPersonList() throws Exception {
        List<EmailTemplateDto> emailTemplateDtos = null;
        sendgridHandler.sendgridCall(emailTemplateDtos,null);
    }

}
