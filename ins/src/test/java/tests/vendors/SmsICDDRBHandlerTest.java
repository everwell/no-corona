package tests.vendors;


import com.everwell.ins.enums.Language;
import com.everwell.ins.exceptions.NotImplementedException;
import com.everwell.ins.exceptions.ValidationException;
import com.everwell.ins.exceptions.VendorException;
import com.everwell.ins.models.dto.SmsDto;
import com.everwell.ins.models.dto.VendorResponseDto;
import com.everwell.ins.models.dto.vendorCreds.UserKeyServerUrlDto;
import org.asynchttpclient.*;
import org.asynchttpclient.netty.NettyResponseFuture;
import com.everwell.ins.vendors.SmsICDDRBHandler;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;

import java.util.List;
import java.util.concurrent.ExecutionException;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

@PowerMockIgnore({"javax.xml.*", "com.sun.org.apache.xerces.*", "org.slf4j.*", "org.xml.*", "com.ibm.*"})
public class SmsICDDRBHandlerTest extends SmsHandlerTest {

    @InjectMocks
    private SmsICDDRBHandler smsICCDDRBHandler;

    @Mock
    private UserKeyServerUrlDto creds;

    @Test(expected = NotImplementedException.class)
    public void testGetLanguageMapping() {
        smsICCDDRBHandler.getLanguageMapping(Language.NON_UNICODE);
    }

    @Test
    public void testSetVendorParams() {
        Long vendorId = 1L;
        when(vendorService.getVendorConfig(any(), any())).thenReturn(null);
        when(vendorService.getVendorCredentials(any(), any())).thenReturn(null);

        smsICCDDRBHandler.setVendorParams(vendorId);
        verify(vendorService, Mockito.times(1)).getVendorConfig(any(), any());
        verify(vendorService, Mockito.times(1)).getVendorCredentials(any(), any());
    }

    @PrepareForTest({AsyncHttpClient.class, NettyResponseFuture.class, Dsl.class})
    @Test
    public void testVendorCall() throws InterruptedException, ExecutionException {
        List<SmsDto> persons = getSmsDtos();
        mockStatic(Dsl.class);
        DefaultAsyncHttpClient client = mock(DefaultAsyncHttpClient.class);
        Response response = mock(Response.class);
        NettyResponseFuture<Response> future = mock(NettyResponseFuture.class);

        when(creds.getUrl()).thenReturn(testUrl);
        when(Dsl.asyncHttpClient()).thenReturn(client);
        when(client.executeRequest(any(Request.class))).thenReturn(future);
        when(future.get()).thenReturn(response);
        when(response.getStatusCode()).thenReturn(200);
        when(response.getResponseBody()).thenReturn(testApiResponse);

        VendorResponseDto expectedDto = new VendorResponseDto();
        expectedDto.setApiResponse(testApiResponse);
        VendorResponseDto responseDto = smsICCDDRBHandler.vendorCall(persons, Language.NON_UNICODE, null);
        assertEquals(expectedDto.getMessageId(), responseDto.getMessageId());
        assertEquals(expectedDto.getApiResponse(), responseDto.getApiResponse());
    }

    @PrepareForTest({AsyncHttpClient.class, NettyResponseFuture.class, Dsl.class})
    @Test(expected = VendorException.class)
    public void testVendorCallException() throws InterruptedException, ExecutionException {
        List<SmsDto> persons = getSmsDtos();
        mockStatic(Dsl.class);
        DefaultAsyncHttpClient client = mock(DefaultAsyncHttpClient.class);
        NettyResponseFuture<Response> future = mock(NettyResponseFuture.class);

        when(creds.getUrl()).thenReturn(testUrl);
        when(Dsl.asyncHttpClient()).thenReturn(client);
        when(client.executeRequest(any(Request.class))).thenReturn(future);
        when(future.get()).thenThrow(InterruptedException.class);

        smsICCDDRBHandler.vendorCall(persons, Language.NON_UNICODE, null);
    }

    @Test(expected = ValidationException.class)
    public void testVendorCallNullException() {
        List<SmsDto> persons = null;
        smsICCDDRBHandler.vendorCall(persons, Language.NON_UNICODE, null);
    }

}
