package tests.vendors;

import com.everwell.ins.models.db.EmailLogs;
import com.everwell.ins.models.dto.EmailTemplateDto;
import com.everwell.ins.models.dto.SendgridResponseDto;
import com.everwell.ins.models.http.requests.EmailRequest;
import com.everwell.ins.repositories.EmailLogsRepository;
import com.everwell.ins.services.VendorService;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.test.util.ReflectionTestUtils;
import tests.BaseTest;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

public class EmailHandlerTest extends BaseTest {

    @InjectMocks
    private MockEmailHandler emailHandler;

    @Mock
    protected VendorService vendorService;

    @Mock
    protected EmailLogsRepository emailLogsRepository;

    @Mock
    private ApplicationEventPublisher applicationEventPublisher;

    protected String testSendgridResponse = "202";

    @Test
    public void testCreateLogs() {
        List<EmailTemplateDto> emailTemplateDtos = new ArrayList<>();
        emailTemplateDtos.add(new EmailTemplateDto("recipient1", "body", "subject"));
        emailTemplateDtos.add(new EmailTemplateDto("recipient2", "body", "subject"));
        EmailRequest emailRequest = new EmailRequest(1L, 1L, 1L, "sender", emailTemplateDtos);

        SendgridResponseDto responseDto = new SendgridResponseDto(testSendgridResponse);
        ReflectionTestUtils.setField(emailHandler, "isMock", true);

        List<EmailLogs> expectedEmailLogCollection = new ArrayList<>();
        expectedEmailLogCollection.add(new EmailLogs(1L,1L,1L,"recipient1","sender","body","subject","TEST",LocalDateTime.now()));
        expectedEmailLogCollection.add(new EmailLogs(1L,1L,1L,"recipient2","sender","body","subject","TEST",LocalDateTime.now()));
        List<EmailLogs> responseEmailLogCollection = emailHandler.createLogs(emailTemplateDtos, responseDto, emailRequest);

        for (int i = 0; i < 2; i++) {
            assertEquals(responseEmailLogCollection.get(i).getVendorId(), expectedEmailLogCollection.get(i).getVendorId());
            assertEquals(responseEmailLogCollection.get(i).getRecipient(), expectedEmailLogCollection.get(i).getRecipient());
            assertEquals(responseEmailLogCollection.get(i).getSendgridResponse(), expectedEmailLogCollection.get(i).getSendgridResponse());
        }
    }

    @Test
    public void testCreateLogsIsNotMock() {
        List<EmailTemplateDto> emailTemplateDtos = new ArrayList<>();
        emailTemplateDtos.add(new EmailTemplateDto("recipient1", "body", "subject"));
        emailTemplateDtos.add(new EmailTemplateDto("recipient2", "body", "subject"));
        EmailRequest emailRequest = new EmailRequest(1L, 1L, 1L, "sender", emailTemplateDtos);
        SendgridResponseDto responseDto = new SendgridResponseDto(testSendgridResponse);
        ReflectionTestUtils.setField(emailHandler, "isMock", false);

        List<EmailLogs> expectedEmailLogCollection = new ArrayList<>();
        expectedEmailLogCollection.add(new EmailLogs(1L,1L,1L,"recipient1","sender","body","subject","202",LocalDateTime.now()));
        expectedEmailLogCollection.add(new EmailLogs(1L,1L,1L,"recipient2","sender","body","subject","202",LocalDateTime.now()));
        List<EmailLogs> responseEmailLogCollection = emailHandler.createLogs(emailTemplateDtos, responseDto, emailRequest);

        for (int i = 0; i < 2; i++) {
            assertEquals(responseEmailLogCollection.get(i).getVendorId(), expectedEmailLogCollection.get(i).getVendorId());
            assertEquals(responseEmailLogCollection.get(i).getRecipient(), expectedEmailLogCollection.get(i).getRecipient());
            assertEquals(responseEmailLogCollection.get(i).getSendgridResponse(), expectedEmailLogCollection.get(i).getSendgridResponse());
        }
    }

    @Test
    public void testProcessEmail() throws IOException, ExecutionException, InterruptedException {
        List<EmailTemplateDto> emailTemplateDtos = new ArrayList<>();
        emailTemplateDtos.add(new EmailTemplateDto("recipient1", "body", "subject"));
        emailTemplateDtos.add(new EmailTemplateDto("recipient2", "body", "subject"));
        EmailRequest emailRequest = new EmailRequest(1L, 1L, 1L, "sender", emailTemplateDtos);
        ReflectionTestUtils.setField(emailHandler, "isMock", true);
        List<EmailLogs> emailLogCollection = new ArrayList<>();
        emailLogCollection.add(new EmailLogs(1L,1L,1L,"recipient1","sender","body","subject","TEST",LocalDateTime.now()));
        emailLogCollection.add(new EmailLogs(1L,1L,1L,"recipient2","sender","body","subject","TEST",LocalDateTime.now()));
        emailHandler = Mockito.spy(emailHandler);

        doReturn(emailLogCollection).when(emailHandler).createLogs(eq(emailTemplateDtos), any(), eq(emailRequest));

        CompletableFuture<List<EmailLogs>> expectedCompletableFuture = CompletableFuture.completedFuture(emailLogCollection);
        CompletableFuture<List<EmailLogs>> actualCompletableFuture = emailHandler.processEmail(emailTemplateDtos, emailRequest);
        assertEquals(expectedCompletableFuture.get(), actualCompletableFuture.get());
    }

    @Test
    public void testProcessEmailIsNotMock() throws IOException, ExecutionException, InterruptedException {
        List<EmailTemplateDto> emailTemplateDtos = new ArrayList<>();
        emailTemplateDtos.add(new EmailTemplateDto("recipient1", "body", "subject"));
        emailTemplateDtos.add(new EmailTemplateDto("recipient2", "body", "subject"));
        EmailRequest emailRequest = new EmailRequest(1L, 1L, 1L, "sender", emailTemplateDtos);
        ReflectionTestUtils.setField(emailHandler, "isMock", false);
        SendgridResponseDto responseDto = new SendgridResponseDto(testSendgridResponse);
        List<EmailLogs> emailLogCollection = new ArrayList<>();
        emailLogCollection.add(new EmailLogs(1L,1L,1L,"recipient1","sender","body","subject","202",LocalDateTime.now()));
        emailLogCollection.add(new EmailLogs(1L,1L,1L,"recipient2","sender","body","subject","202",LocalDateTime.now()));
        emailHandler = Mockito.spy(emailHandler);

        doReturn(emailLogCollection).when(emailHandler).createLogs(eq(emailTemplateDtos), any(), eq(emailRequest));
        doReturn(responseDto).when(emailHandler).sendgridCall(emailTemplateDtos,emailRequest);

        CompletableFuture<List<EmailLogs>> expectedCompletableFuture = CompletableFuture.completedFuture(emailLogCollection);
        CompletableFuture<List<EmailLogs>> actualCompletableFuture = emailHandler.processEmail(emailTemplateDtos, emailRequest);
        assertEquals(expectedCompletableFuture.get(), actualCompletableFuture.get());
    }

    @Test
    public void testSendEmail() throws IOException, ExecutionException, InterruptedException {
        List<EmailTemplateDto> emailTemplateDtos = new ArrayList<>();
        emailTemplateDtos.add(new EmailTemplateDto("recipient1", "body", "subject"));
        emailTemplateDtos.add(new EmailTemplateDto("recipient2", "body", "subject"));
        EmailRequest emailRequest = new EmailRequest(1L, 1L, 1L, "sender", emailTemplateDtos);
        emailRequest.setIsEmailCommon(true);
        emailRequest.setSubject("subject");
        emailRequest.setBody("body");
        ReflectionTestUtils.setField(emailHandler, "isMock", true);
        SendgridResponseDto responseDto = new SendgridResponseDto(testSendgridResponse);
        List<EmailLogs> emailLogList = new ArrayList<>();
        emailLogList.add(new EmailLogs(1L,1L,1L,"recipient1","sender","body","subject","202",LocalDateTime.now()));
        CompletableFuture<List<EmailLogs>> completableFuture = CompletableFuture.completedFuture(emailLogList);

        List<EmailLogs> emailLogsCollection = new ArrayList<>();
        for(int i = 0; i < 2; i++) {
            emailLogsCollection.addAll(completableFuture.get());
        }

        emailHandler = Mockito.spy(emailHandler);

        doNothing().when(emailHandler).setVendorParams(1L);
        doReturn(completableFuture).when(emailHandler).processEmail(any(),any());

        emailHandler.sendEmail(emailRequest);
        verify(emailLogsRepository, Mockito.times(1)).saveAll(emailLogsCollection);


    }
}
