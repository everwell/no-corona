package tests.vendors;


import com.everwell.ins.enums.Language;
import com.everwell.ins.exceptions.ValidationException;
import com.everwell.ins.exceptions.VendorException;
import com.everwell.ins.models.dto.SmsDto;
import com.everwell.ins.models.dto.VendorResponseDto;
import com.everwell.ins.models.dto.vendorConfigs.TwilioConfigDto;
import com.everwell.ins.models.dto.vendorCreds.AuthTokenFromDto;
import com.everwell.ins.vendors.SmsTwilioHandler;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.rest.api.v2010.account.MessageCreator;
import com.twilio.type.PhoneNumber;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.IOException;
import java.net.URI;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

@PrepareForTest({Twilio.class, Message.class})
public class SmsTwilioHandlerTest extends SmsHandlerTest {

    @InjectMocks
    private SmsTwilioHandler smsTwilioHandler;

    @Mock
    private TwilioConfigDto config;

    @Mock
    private AuthTokenFromDto creds;

    @Mock
    Message response;

    @Mock
    MessageCreator mockMessageCreator;

    @Test
    public void testSetVendorParams() {
        Long vendorId = 1L;
        when(vendorService.getVendorConfig(any(), any())).thenReturn(null);
        when(vendorService.getVendorCredentials(any(), any())).thenReturn(null);

        smsTwilioHandler.setVendorParams(vendorId);
        verify(vendorService, Mockito.times(1)).getVendorConfig(any(), any());
        verify(vendorService, Mockito.times(1)).getVendorCredentials(any(), any());
    }

    @Test
    public void testGetBatchSize() {
        Integer size = 1;

        Integer batchSize1 = smsTwilioHandler.getBatchSize(true);
        assertEquals(size, batchSize1);

        Integer batchSize2 = smsTwilioHandler.getBatchSize(false);
        assertEquals(size, batchSize2);
    }

    @Test
    public void testGetLanguageMapping() {
        String english = "english";
        String unicode = "unicode";
        when(config.getLanguage()).thenReturn(english);
        when(config.getUnicodeLanguage()).thenReturn(unicode);

        String language1 = smsTwilioHandler.getLanguageMapping(Language.NON_UNICODE);
        assertEquals(english, language1);

        String language2 = smsTwilioHandler.getLanguageMapping(Language.UNICODE);
        assertEquals(unicode, language2);
    }

    @Test
    public void testVendorCall() {
        List<SmsDto> persons = getSmsDtos();
        mockStatic(Twilio.class);
        mockStatic(Message.class);

        when(Message.creator(any(PhoneNumber.class), any(PhoneNumber.class), anyString())).thenReturn(mockMessageCreator);
        when(mockMessageCreator.setStatusCallback(any(URI.class))).thenReturn(mockMessageCreator);
        when(mockMessageCreator.setSmartEncoded(any())).thenReturn(mockMessageCreator);
        when(mockMessageCreator.create()).thenReturn(response);
        when(response.getAccountSid()).thenReturn(testApiResponse);
        when(response.getSid()).thenReturn(testMessageId);
        when(config.getReconciliation()).thenReturn("");

        ReflectionTestUtils.setField(smsTwilioHandler, "serverPort", "");
        smsTwilioHandler.setBaseUrl();
        VendorResponseDto expectedDto = new VendorResponseDto(testApiResponse, testMessageId);
        VendorResponseDto responseDto = smsTwilioHandler.vendorCall(persons, Language.NON_UNICODE, null);
        assertEquals(expectedDto.getMessageId(), responseDto.getMessageId());
        assertEquals(expectedDto.getApiResponse(), responseDto.getApiResponse());
    }

    @Test(expected = ValidationException.class)
    public void testVendorCallNullException() {
        List<SmsDto> persons = null;
        smsTwilioHandler.vendorCall(persons, Language.NON_UNICODE, null);
    }

    @Test(expected = VendorException.class)
    public void testVendorCallException() throws IOException {
        List<SmsDto> persons = getSmsDtos();
        mockStatic(Twilio.class);
        mockStatic(Message.class);

        when(Message.creator(any(PhoneNumber.class), any(PhoneNumber.class), anyString())).thenReturn(mockMessageCreator);
        when(mockMessageCreator.setStatusCallback(any(URI.class))).thenReturn(mockMessageCreator);
        when(mockMessageCreator.setSmartEncoded(any())).thenReturn(mockMessageCreator);
        when(mockMessageCreator.create()).thenReturn(null);

        smsTwilioHandler.vendorCall(persons, Language.NON_UNICODE, null);
    }
}
