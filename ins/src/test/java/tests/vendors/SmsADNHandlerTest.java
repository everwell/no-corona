package tests.vendors;

import com.everwell.ins.enums.Language;
import com.everwell.ins.enums.SmsGateway;
import com.everwell.ins.exceptions.ValidationException;
import com.everwell.ins.exceptions.VendorException;
import com.everwell.ins.models.dto.SmsDto;
import com.everwell.ins.models.dto.VendorResponseDto;
import com.everwell.ins.models.dto.vendorConfigs.ADNConfigDto;
import com.everwell.ins.models.dto.vendorCreds.ApiKeySecretDto;
import com.everwell.ins.vendors.SmsADNHandler;
import org.asynchttpclient.*;
import org.asynchttpclient.netty.NettyResponseFuture;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;

import java.util.List;
import java.util.concurrent.ExecutionException;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.mock;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

@PowerMockIgnore({"javax.xml.*", "com.sun.org.apache.xerces.*", "org.slf4j.*", "org.xml.*", "com.ibm.*"})
public class SmsADNHandlerTest extends SmsHandlerTest{

    @InjectMocks
    private SmsADNHandler smsADNHandler;

    @Mock
    ApiKeySecretDto creds;

    @Mock
    ADNConfigDto config;

    String testResponse = "{\"sms_uid\":\"123\"}";

    @Test
    public void testGetLanguageMapping() {
        String response = smsADNHandler.getLanguageMapping(Language.NON_UNICODE);
        Assert.assertNull(response);
    }

    @Test
    public void testGetBatchSize() {
        Integer size = 5;
        when(config.getBulkSmsLimit()).thenReturn("5");

        Integer batchSize1 = smsADNHandler.getBatchSize(true);
        assertEquals(size, batchSize1);

        size = 1;
        Integer batchSize2 = smsADNHandler.getBatchSize(false);
        assertEquals(size, batchSize2);
    }

    @Test
    public void testVendorGateway() {
       SmsGateway gateway = smsADNHandler.getVendorGatewayEnumerator();
       Assert.assertEquals(SmsGateway.ADN.toString(), gateway.toString());
    }

    @Test
    public void testSetVendorParams() {
        Long vendorId = 1L;
        when(vendorService.getVendorConfig(any(), any())).thenReturn(null);
        when(vendorService.getVendorCredentials(any(), any())).thenReturn(null);

        smsADNHandler.setVendorParams(vendorId);
        verify(vendorService, Mockito.times(1)).getVendorConfig(any(), any());
        verify(vendorService, Mockito.times(1)).getVendorCredentials(any(), any());
    }

    @Test(expected =ValidationException.class)
    public void testVendorCallNullException() {
        List<SmsDto> persons = null;
        smsADNHandler.vendorCall(persons, Language.NON_UNICODE, null);
    }

    @PrepareForTest({AsyncHttpClient.class, NettyResponseFuture.class, Dsl.class})
    @Test
    public void testVendorCall() throws InterruptedException, ExecutionException {
        List<SmsDto> persons = getSmsDtos();
        mockStatic(Dsl.class);
        DefaultAsyncHttpClient client = mock(DefaultAsyncHttpClient.class);
        Response response = mock(Response.class);
        NettyResponseFuture<Response> future = mock(NettyResponseFuture.class);

        when(creds.getUrl()).thenReturn(testUrl);
        when(Dsl.asyncHttpClient()).thenReturn(client);
        when(client.executeRequest(any(Request.class))).thenReturn(future);
        when(future.get()).thenReturn(response);
        when(response.getStatusCode()).thenReturn(200);
        when(response.getResponseBody()).thenReturn(testResponse);

        VendorResponseDto expectedDto = new VendorResponseDto();
        expectedDto.setApiResponse(testResponse);
        expectedDto.setMessageId("123");
        VendorResponseDto responseDto = smsADNHandler.vendorCall(persons, Language.NON_UNICODE, null);
        assertEquals(expectedDto.getMessageId(), responseDto.getMessageId());
        assertEquals(expectedDto.getApiResponse(), responseDto.getApiResponse());
    }

    @PrepareForTest({AsyncHttpClient.class, NettyResponseFuture.class, Dsl.class})
    @Test(expected = VendorException.class)
    public void testVendorCallException() throws InterruptedException, ExecutionException {
        List<SmsDto> persons = getSmsDtos();
        mockStatic(Dsl.class);
        DefaultAsyncHttpClient client = mock(DefaultAsyncHttpClient.class);
        NettyResponseFuture<Response> future = mock(NettyResponseFuture.class);

        when(creds.getUrl()).thenReturn(testUrl);
        when(Dsl.asyncHttpClient()).thenReturn(client);
        when(client.executeRequest(any(Request.class))).thenReturn(future);
        when(future.get()).thenThrow(InterruptedException.class);

        smsADNHandler.vendorCall(persons, Language.NON_UNICODE, null);
    }

}
