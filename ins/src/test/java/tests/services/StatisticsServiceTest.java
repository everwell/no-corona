package tests.services;

import com.everwell.ins.exceptions.ValidationException;
import com.everwell.ins.models.http.requests.StatisticsRequest;
import com.everwell.ins.repositories.SmsLogsRepository;
import com.everwell.ins.services.StatisticsService;
import com.everwell.ins.services.impl.StatisticsServiceImpl;
import com.everwell.ins.utils.Utils;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import tests.BaseTest;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class StatisticsServiceTest extends BaseTest {

    @Spy
    @InjectMocks
    StatisticsServiceImpl statisticsService;

    @Mock
    SmsLogsRepository smsLogsRepository;

    private static Long triggerId = 1L;
    private static Long templateId = 1L;
    private static Long vendorId = 1L;
    private static String from = "01-01-2020 23:15:45";
    private static String to = "02-01-2020 23:15:45";

    @Test
    public void testGetNotificationsCountWithTemplateAndTriggerId() {
        Long count = 1L;
        StatisticsRequest statisticsRequest = new StatisticsRequest(triggerId, templateId, vendorId, from, to);

        when(smsLogsRepository.getSmsLogsForNotificationStatsWithTemplateAndTriggerId(any(), any(), any(), any(), any())).thenReturn(count);
        assertEquals(count, statisticsService.getNotificationsCount(statisticsRequest));
    }

    @Test
    public void testGetNotificationsCountWithTemplateId() {
        Long count = 2L;
        StatisticsRequest statisticsRequest = new StatisticsRequest(null, templateId, vendorId, from, to);

        when(smsLogsRepository.getSmsLogsForNotificationStatsWithTemplateId(any(), any(), any(), any())).thenReturn(count);
        assertEquals(count, statisticsService.getNotificationsCount(statisticsRequest));
    }

    @Test
    public void testGetNotificationsCountWithTriggerId() {
        Long count = 3L;
        StatisticsRequest statisticsRequest = new StatisticsRequest(triggerId, null, vendorId, from, to);

        when(smsLogsRepository.getSmsLogsForNotificationStatsWithTriggerId(any(), any(), any(), any())).thenReturn(count);
        assertEquals(count, statisticsService.getNotificationsCount(statisticsRequest));
    }

    @Test
    public void testGetNotificationsCount() {
        Long count = 4L;
        StatisticsRequest statisticsRequest = new StatisticsRequest(null, null, vendorId, from, to);

        when(smsLogsRepository.getSmsLogsForNotificationStats(any(), any(), any())).thenReturn(count);
        assertEquals(count, statisticsService.getNotificationsCount(statisticsRequest));
    }

    @Test(expected = ValidationException.class)
    public void testIncorrectDateFormat() {
        StatisticsRequest statisticsRequest = new StatisticsRequest(null, null, vendorId, "2012-04-23", to);
        statisticsService.getNotificationsCount(statisticsRequest);
    }
}
