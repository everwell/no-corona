package tests.services;


import com.everwell.ins.exceptions.ConflictException;
import com.everwell.ins.exceptions.NotFoundException;
import com.everwell.ins.exceptions.ValidationException;
import com.everwell.ins.models.db.Engagement;
import com.everwell.ins.models.db.TemplateMap;
import com.everwell.ins.models.dto.SmsDto;
import com.everwell.ins.models.dto.SmsRequestDto;
import com.everwell.ins.models.dto.SmsTemplateDto;
import com.everwell.ins.models.http.requests.EngagementRequest;
import com.everwell.ins.models.http.requests.SmsDetailedRequest;
import com.everwell.ins.models.http.requests.SmsRequest;
import com.everwell.ins.repositories.EngagementRepository;
import com.everwell.ins.repositories.TemplateMapRepository;
import com.everwell.ins.services.impl.EngagementServiceImpl;
import com.everwell.ins.services.impl.TemplateMapServiceImpl;
import com.everwell.ins.utils.Utils;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import tests.BaseTest;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TemplateMapServiceTest extends BaseTest {

    @Spy
    @InjectMocks
    private TemplateMapServiceImpl templateMapServiceImpl;

    @Mock
    private TemplateMapRepository templateMapRepository;

    private static Long templateId = 1L;
    private static String vendorTemplateId = "1L";

    @Test
    public void testGetTemplateMap() {
        TemplateMap templateMap = new TemplateMap(templateId, vendorTemplateId);
        when(templateMapRepository.findByTemplateId(any())).thenReturn(templateMap);

        templateMapServiceImpl.getTemplateMap(templateId);
        verify(templateMapRepository, Mockito.times(1)).findByTemplateId(any());
    }

    @Test(expected = NotFoundException.class)
    public void testGetTemplateMapNotFound() {
        when(templateMapRepository.findByTemplateId(any())).thenReturn(null);
        templateMapServiceImpl.getTemplateMap(templateId);
    }

}
