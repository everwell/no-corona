package tests.services;


import com.everwell.ins.exceptions.NotFoundException;
import com.everwell.ins.models.db.Language;
import com.everwell.ins.models.dto.LanguageMapping;
import com.everwell.ins.models.http.responses.LanguageResponse;
import com.everwell.ins.models.http.responses.TypeResponse;
import com.everwell.ins.repositories.LanguageRepository;
import com.everwell.ins.services.impl.LanguageServiceImpl;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import tests.BaseTest;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class LanguageServiceTest extends BaseTest {

    @Spy
    @InjectMocks
    private LanguageServiceImpl languageService;

    @Mock
    private LanguageRepository languageRepository;

    @Test
    public void testGetLanguage() {
        Long languageId = 1L;
        String english = "ENGLISH";
        Language language = new Language(languageId, english);
        when(languageRepository.findById(any())).thenReturn(Optional.of(language));

        Language response = languageService.getLanguage(languageId);
        assertEquals(language, response);
        verify(languageRepository, Mockito.times(1)).findById(any());
    }

    @Test(expected = NotFoundException.class)
    public void testGetLanguageMappingNotFound() {
        Long languageId = 1L;
        when(languageRepository.findById(any())).thenReturn(Optional.empty());
        languageService.getLanguage(languageId);
    }

    @Test
    public void testGetLanguageMappings() {
        List<LanguageMapping> languageMapping = new ArrayList<>();
        languageMapping.add(new LanguageMapping(1L, "English"));
        languageMapping.add(new LanguageMapping(2L, "Bangla"));
        List<Long> languageIds = new ArrayList<>();
        languageIds.add(1L);
        languageIds.add(2L);
        List<Language> languages = new ArrayList<>();
        languages.add(new Language(1L, "English"));
        languages.add(new Language(2L, "Bangla"));
        when(languageRepository.findAllByIdIn(any())).thenReturn(languages);

        List<LanguageMapping> response = languageService.getLanguageMappings(languageIds);
        assertEquals(languageMapping.get(0).getLanguage(), response.get(0).getLanguage());
        verify(languageRepository, Mockito.times(1)).findAllByIdIn(any());
    }

    @Test
    public void testGetAllLanguageMappings() {
        List<LanguageMapping> languageMapping = new ArrayList<>();
        languageMapping.add(new LanguageMapping(1L, "English"));
        languageMapping.add(new LanguageMapping(2L, "Bangla"));
        List<Language> languages = new ArrayList<>();
        languages.add(new Language(1L, "English"));
        languages.add(new Language(2L, "Bangla"));
        when(languageRepository.findAll()).thenReturn(languages);

        List<LanguageMapping> response = languageService.getAllLanguageMappings();
        assertEquals(languageMapping.get(0).getLanguage(), response.get(0).getLanguage());
        verify(languageRepository, Mockito.times(1)).findAll();
    }
}
