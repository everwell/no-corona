package tests.services;


import com.everwell.ins.exceptions.NotFoundException;
import com.everwell.ins.models.db.Type;
import com.everwell.ins.models.dto.TypeMapping;
import com.everwell.ins.models.http.responses.TypeResponse;
import com.everwell.ins.repositories.TypeRepository;
import com.everwell.ins.services.impl.TypeServiceImpl;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import tests.BaseTest;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TypeServiceTest extends BaseTest {

    @Spy
    @InjectMocks
    private TypeServiceImpl typeService;

    @Mock
    private TypeRepository typeRepository;

    @Test
    public void testGetType() {
        Long typeId = 1L;
        String sms = "SMS";
        Type type = new Type(typeId, sms);
        when(typeRepository.findById(any())).thenReturn(Optional.of(type));

        Type response = typeService.getType(typeId);
        assertEquals(type, response);
        verify(typeRepository, Mockito.times(1)).findById(any());
    }

    @Test(expected = NotFoundException.class)
    public void testGetTypeMappingNotFound() {
        Long typeId = 1L;
        when(typeRepository.findById(any())).thenReturn(Optional.empty());
        typeService.getType(typeId);
    }

    @Test
    public void testGetTypeMappings() {
        List<TypeMapping> typeMapping = new ArrayList<>();
        typeMapping.add(new TypeMapping(1L, "Sms"));
        typeMapping.add(new TypeMapping(2L, "Ivr"));
        List<Long> typeIds = new ArrayList<>();
        typeIds.add(1L);
        typeIds.add(2L);
        List<Type> types = new ArrayList<>();
        types.add(new Type(1L, "Sms"));
        types.add(new Type(2L, "Ivr"));
        when(typeRepository.findAllByIdIn(any())).thenReturn(types);

        List<TypeMapping> response = typeService.getTypeMappings(typeIds);
        assertEquals(typeMapping.get(0).getType(), response.get(0).getType());
        verify(typeRepository, Mockito.times(1)).findAllByIdIn(any());
    }

    @Test
    public void testGetAllTypeMappings() {
        List<TypeMapping> typeMapping = new ArrayList<>();
        typeMapping.add(new TypeMapping(1L, "Sms"));
        typeMapping.add(new TypeMapping(2L, "Ivr"));
        List<Type> types = new ArrayList<>();
        types.add(new Type(1L, "Sms"));
        types.add(new Type(2L, "Ivr"));
        when(typeRepository.findAll()).thenReturn(types);

        List<TypeMapping> response = typeService.getAllTypeMappings();
        assertEquals(typeMapping.get(0).getType(), response.get(0).getType());
        verify(typeRepository, Mockito.times(1)).findAll();
    }
}
