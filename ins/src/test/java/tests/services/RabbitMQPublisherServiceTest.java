package tests.services;


import com.everwell.ins.models.dto.EventStreamingDto;
import com.everwell.ins.services.impl.RabbitMQPublisherServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import tests.BaseTest;

import static org.assertj.core.api.Assertions.assertThatCode;

public class RabbitMQPublisherServiceTest extends BaseTest {

    @Spy
    @InjectMocks
    private RabbitMQPublisherServiceImpl rabbitMQPublisherService;

    private RabbitTemplate rabbitTemplateMock;

    String test = "Test";
    EventStreamingDto<String> message = new EventStreamingDto<>(test, test);

    @Before
    public void setUp() {
        rabbitTemplateMock = Mockito.mock(RabbitTemplate.class);
    }

    @Test
    public void publish() {
        assertThatCode(() -> rabbitTemplateMock.convertAndSend(test, message))
                .doesNotThrowAnyException();
    }
}
