package tests.services;


import com.everwell.ins.enums.Language;
import com.everwell.ins.exceptions.NotFoundException;
import com.everwell.ins.models.db.EmailTemplate;
import com.everwell.ins.models.db.PushNotificationTemplate;
import com.everwell.ins.models.db.Template;
import com.everwell.ins.models.dto.*;
import com.everwell.ins.models.http.requests.*;
import com.everwell.ins.repositories.EmailTemplateRepository;
import com.everwell.ins.repositories.PushTemplateRepository;
import com.everwell.ins.repositories.TemplateRepository;
import com.everwell.ins.services.impl.TemplateServiceImpl;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import tests.BaseTest;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TemplateServiceTest extends BaseTest {

    @Spy
    @InjectMocks
    private TemplateServiceImpl templateService;

    @Mock
    private TemplateRepository templateRepository;

    @Mock
    private PushTemplateRepository pushTemplateRepository;

    @Mock
    private EmailTemplateRepository emailTemplateRepository;

    private static Long id = 1L;
    private static Long typeId = 1L;
    private static Long languageId = 1L;
    private static Long vendorId = 1L;
    private static String entityId = "1";
    private static String content = "Content";
    private static String phone = "9705197814";
    private static String body = "body";
    private static String subject = "subject";

    @Test
    public void testSaveTemplate() {
        TemplateRequest request = new TemplateRequest(content, languageId, typeId, null, false);
        Template template = new Template(content, null, languageId, typeId);
        when(templateRepository.save(any())).thenReturn(template);

        templateService.saveTemplate(request);
        verify(templateRepository, Mockito.times(1)).save(any());
    }

    @Test
    public void testGetTemplate() {
        Template template = new Template(content, null, languageId, typeId);
        when(templateRepository.findById(any())).thenReturn(Optional.of(template));

        templateService.getTemplate(id);
        verify(templateRepository, Mockito.times(1)).findById(any());
    }

    @Test
    public void testGetPNTemplate() {
        PushNotificationTemplate template = new PushNotificationTemplate(content, null, null,languageId, 2L,"test",null,2L);
        when(pushTemplateRepository.findById(any())).thenReturn(Optional.of(template));

        templateService.getPNTemplate(id);
        verify(pushTemplateRepository, Mockito.times(1)).findById(any());
    }

    @Test
    public void testGetEmailTemplate() {
        EmailTemplate template = new EmailTemplate(subject,body,null,null,languageId,typeId);
        when(emailTemplateRepository.findById(1L)).thenReturn(Optional.of(template));

        templateService.getEmailTemplate(id);
        verify(emailTemplateRepository, Mockito.times(1)).findById(any());
    }

    @Test(expected = NotFoundException.class)
    public void testGetTemplateNotFound() {
        when(templateRepository.findById(any())).thenReturn(Optional.empty());
        templateService.getTemplate(id);
    }

    @Test(expected = NotFoundException.class)
    public void testGetPNTemplateNotFound() {
        when(pushTemplateRepository.findById(any())).thenReturn(Optional.empty());
        templateService.getPNTemplate(id);
    }

    @Test(expected = NotFoundException.class)
    public void testGetEmailTemplateNotFound() {
        when(emailTemplateRepository.findById(1L)).thenReturn(Optional.empty());
        templateService.getEmailTemplate(id);
    }

    @Test
    public void testGetAllTemplates() {
        Template template = new Template(content, null, languageId, typeId);
        List<Template> templateList = new ArrayList<>();
        templateList.add(template);
        when(templateRepository.findAll()).thenReturn(templateList);

        templateService.getAllTemplates();
        verify(templateRepository, Mockito.times(1)).findAll();
    }

    @Test(expected = NotFoundException.class)
    public void testGetAllTemplatesNotFound() {
        when(templateRepository.findAll()).thenReturn(null);
        templateService.getAllTemplates();
    }

    @Test
    public void testCreateSmsWithoutParameters() {
        List<SmsTemplateDto> smsTemplateDto = new ArrayList<>();
        smsTemplateDto.add(new SmsTemplateDto(entityId, phone, null));
        SmsTemplateRequest smsTemplateRequest = new SmsTemplateRequest(id, smsTemplateDto, vendorId, null);
        Template template = new Template(content, null, languageId, typeId);
        when(templateRepository.findById(any())).thenReturn(Optional.of(template));

        SmsRequestDto response = templateService.createSms(smsTemplateRequest);
        assertEquals(response.getSmsRequest().getMessage(), content);
        assertEquals(response.getSmsRequest().getIsMessageCommon(), true);
        assertEquals(response.getSmsRequest().getVendorId(), vendorId);
        assertEquals(response.getSmsRequest().getTemplateId(), id);
        assertEquals(response.getSmsRequest().getLanguage(), Language.NON_UNICODE);
        assertEquals(response.getSmsRequest().getPersonList().get(0).getMessage(), content);
        assertEquals(response.getSmsRequest().getPersonList().get(0).getPhone(), smsTemplateRequest.getPersonList().get(0).getPhone());
        assertEquals(response.getSmsRequest().getPersonList().get(0).getId(), smsTemplateRequest.getPersonList().get(0).getId());
        assertTrue(response.getFailedSmsRequest().isEmpty());
    }

    @Test
    public void testCreateSmsWithParameters() {
        String parameters = "Phone,Name";
        String messageContent = "Name : %(Name) ; Phone : %(Phone)";
        String id1 = "1";
        String id2 = "2";
        String id3 = "3";
        Map<String, String> entityParametersMap1 = new HashMap<>();
        entityParametersMap1.put("Phone", "1");
        entityParametersMap1.put("Name", "Test");
        String finalMessage = "Name : Test ; Phone : 1";
        Map<String, String> entityParametersMap2 = new HashMap<>();
        entityParametersMap2.put("Phone", "2");
        List<SmsTemplateDto> smsTemplateDto = new ArrayList<>();
        smsTemplateDto.add(new SmsTemplateDto(id1, phone, entityParametersMap1));
        smsTemplateDto.add(new SmsTemplateDto(id2, phone, entityParametersMap2));
        smsTemplateDto.add(new SmsTemplateDto(id3, phone, entityParametersMap2));
        SmsTemplateRequest smsTemplateRequest = new SmsTemplateRequest(id, smsTemplateDto, vendorId, null);
        Template template = new Template(messageContent, parameters, languageId, typeId);
        when(templateRepository.findById(any())).thenReturn(Optional.of(template));

        SmsRequestDto response = templateService.createSms(smsTemplateRequest);
        assertNull(response.getSmsRequest().getMessage());
        assertEquals(response.getSmsRequest().getIsMessageCommon(), false);
        assertEquals(response.getSmsRequest().getVendorId(), vendorId);
        assertEquals(response.getSmsRequest().getTemplateId(), id);
        assertEquals(response.getSmsRequest().getLanguage(), Language.NON_UNICODE);
        assertEquals(response.getSmsRequest().getPersonList().get(0).getMessage(), finalMessage);
        assertEquals(response.getSmsRequest().getPersonList().get(0).getPhone(), phone);
        assertEquals(response.getSmsRequest().getPersonList().get(0).getId(), id1);

        assertEquals(2, response.getFailedSmsRequest().size());
        assertEquals(id2, response.getFailedSmsRequest().get(0).getId());
        assertEquals(phone, response.getFailedSmsRequest().get(0).getPhone());
        assertEquals(entityParametersMap2, response.getFailedSmsRequest().get(0).getParameters());
    }

    @Test
    public void testConstructSms() {
        String parameters = "Phone,Name";
        String messageContent = "Name : %(Name) ; Phone : %(Phone)";
        Map<String, String> entityParametersMap1 = new HashMap<>();
        entityParametersMap1.put("Phone", "1");
        entityParametersMap1.put("Name", "Test");
        Map<String, String> entityParametersMap2 = new HashMap<>();
        entityParametersMap2.put("Phone", "2");
        List<SmsTemplateDto> smsTemplateDto = new ArrayList<>();
        smsTemplateDto.add(new SmsTemplateDto("1", phone, entityParametersMap1, 1L));
        smsTemplateDto.add(new SmsTemplateDto("2", phone, entityParametersMap2, 1L));
        smsTemplateDto.add(new SmsTemplateDto("3", phone, null, 1L));
        smsTemplateDto.add(new SmsTemplateDto("4", phone, entityParametersMap1, 2L));
        smsTemplateDto.add(new SmsTemplateDto("5", phone, entityParametersMap2, 2L));
        smsTemplateDto.add(new SmsTemplateDto("6", phone, null, 2L));
        smsTemplateDto.add(new SmsTemplateDto("7", phone, entityParametersMap1, null));
        smsTemplateDto.add(new SmsTemplateDto("8", phone, entityParametersMap2, null));
        smsTemplateDto.add(new SmsTemplateDto("9", phone, null, null));
        List<Long> templateIds = new ArrayList<>();
        templateIds.add(2L);
        SmsDetailedRequest smsDetailedRequest = new SmsDetailedRequest(id, templateIds, smsTemplateDto, id, id, true, true, true, null, null, null, 29L);
        Template template1 = new Template(messageContent, parameters, 1L, typeId);
        Template template2 = new Template(content, null, 2L, typeId);

        when(templateRepository.findById(1L)).thenReturn(Optional.of(template1));
        when(templateRepository.findById(2L)).thenReturn(Optional.of(template2));

        SmsDetailedRequestDto response = templateService.constructSms(smsDetailedRequest);
        assertEquals(2, response.getSmsRequests().size());
        assertEquals(2, response.getSmsRequests().get(0).getPersonList().size());
        assertEquals(3, response.getSmsRequests().get(1).getPersonList().size());
        assertEquals(4, response.getFailedSmsRequest().size());
    }

    @Test
    public void testConstructSmsUnicode() {
        String messageContent = "हिंदी";
        Long unicodeLanguageId = 1L;
        List<SmsTemplateDto> smsTemplateDto = new ArrayList<>();
        smsTemplateDto.add(new SmsTemplateDto(entityId, phone, null, unicodeLanguageId));

        SmsDetailedRequest smsDetailedRequest = new SmsDetailedRequest(id, null, smsTemplateDto, id, id, true, true, true, null, null, null, 29L);
        Template template = new Template(messageContent, null, unicodeLanguageId, typeId, true);

        when(templateRepository.findById(unicodeLanguageId)).thenReturn(Optional.of(template));

        SmsDetailedRequestDto response = templateService.constructSms(smsDetailedRequest);
        assertEquals(1, response.getSmsRequests().size());
        assertEquals(1, response.getSmsRequests().get(0).getPersonList().size());
        assertEquals(Language.UNICODE, response.getSmsRequests().get(0).getLanguage());
    }

    @Test
    public void testCreateSmsUnicode() {
        List<SmsTemplateDto> smsTemplateDto = new ArrayList<>();
        String unicodeContent = "हिंदी";
        Long unicodeLanguageId = 2L;
        smsTemplateDto.add(new SmsTemplateDto(entityId, phone, null));
        SmsTemplateRequest smsTemplateRequest = new SmsTemplateRequest(id, smsTemplateDto, vendorId, null);
        Template template = new Template(unicodeContent, null, unicodeLanguageId, typeId, true);
        when(templateRepository.findById(any())).thenReturn(Optional.of(template));

        SmsRequestDto response = templateService.createSms(smsTemplateRequest);
        assertEquals(response.getSmsRequest().getMessage(), unicodeContent);
        assertEquals(response.getSmsRequest().getIsMessageCommon(), true);
        assertEquals(response.getSmsRequest().getVendorId(), vendorId);
        assertEquals(response.getSmsRequest().getTemplateId(), id);
        assertEquals(response.getSmsRequest().getLanguage(), Language.UNICODE);
        assertEquals(response.getSmsRequest().getPersonList().get(0).getMessage(), unicodeContent);
        assertEquals(response.getSmsRequest().getPersonList().get(0).getPhone(), smsTemplateRequest.getPersonList().get(0).getPhone());
        assertEquals(response.getSmsRequest().getPersonList().get(0).getId(), smsTemplateRequest.getPersonList().get(0).getId());
        assertTrue(response.getFailedSmsRequest().isEmpty());
    }

    @Test
    public void testConstructNotificationWithParameters() {
        String messageContent = "Name : %(Name) ; Phone : %(Phone)";
        String headingContent = "Name : %(Name) ; Phone : %(Phone)";
        String contentParameters = "Phone,Name";
        String headingParameters = "Phone,Name";
        PushNotificationTemplate template = new PushNotificationTemplate(messageContent,contentParameters,headingParameters,1L,2L,headingContent,null,2L);
        Map<String,String> contentParametersMap = new HashMap<>();
        contentParametersMap.put("Phone","1");
        contentParametersMap.put("Name","Test");
        Map<String,String> headingParametersMap = new HashMap<>();
        headingParametersMap.put("Phone","1");
        headingParametersMap.put("Name","Test");
        Map<String,String> headingParametersFailMap = new HashMap<>();


        List<PushNotificationTemplateDto> pushNotificationTemplateDtos = new ArrayList<>();
        pushNotificationTemplateDtos.add(new PushNotificationTemplateDto("1","1",headingParametersMap,contentParametersMap));
        pushNotificationTemplateDtos.add(new PushNotificationTemplateDto("2","2",headingParametersMap,contentParametersMap));
        pushNotificationTemplateDtos.add(new PushNotificationTemplateDto("3","3",headingParametersFailMap,contentParametersMap));

        PushNotificationTemplate template1 = new PushNotificationTemplate(messageContent, contentParameters,headingParameters ,1L, 2L,headingContent,null,2L);

        when(pushTemplateRepository.findById(1L)).thenReturn(Optional.of(template1));

        PushNotificationRequest pushNotificationRequest = new PushNotificationRequest(pushNotificationTemplateDtos,1L,1L,1L);
        PushNotificationRequestDto response = templateService.constructNotification(pushNotificationRequest);

        assertEquals("Name : Test ; Phone : 1",response.getPushNotificationRequest().getPersonList().get(0).getMessage());
        assertEquals("Name : Test ; Phone : 1",response.getPushNotificationRequest().getPersonList().get(0).getHeading());
        assertEquals(2,response.getPushNotificationRequest().getPersonList().size());
        assertEquals(1,response.getFailedPNRequests().size());
    }

    @Test
    public void testConstructNotificationWithOutParameters() {
        String messageContent = "Name and Phone";
        String headingContent = "Name and Phone";
        PushNotificationTemplate template = new PushNotificationTemplate(messageContent,null,null,1L,2L,headingContent,null,1L);

        List<PushNotificationTemplateDto> pushNotificationTemplateDtos = new ArrayList<>();
        pushNotificationTemplateDtos.add(new PushNotificationTemplateDto("1","1",(Map<String, String>) null,(Map<String,String>) null));
        when(pushTemplateRepository.findById(1L)).thenReturn(Optional.of(template));

        PushNotificationRequest pushNotificationRequest = new PushNotificationRequest(pushNotificationTemplateDtos,1L,1L,1L);
        PushNotificationRequestDto response = templateService.constructNotification(pushNotificationRequest);

        assertEquals("Name and Phone",response.getPushNotificationRequest().getPersonList().get(0).getMessage());
        assertEquals("Name and Phone",response.getPushNotificationRequest().getPersonList().get(0).getHeading());
        assertEquals(1,response.getPushNotificationRequest().getPersonList().size());

    }

    @Test
    public void testConstructNotificationWithContentParameters() {
        String messageContent = "Name : %(Name) ; Phone : %(Phone)";
        String headingContent = "Name and Phone";
        String contentParameters = "Phone,Name";
        PushNotificationTemplate template = new PushNotificationTemplate(messageContent,contentParameters,null,1L,2L,headingContent,null,1L);
        Map<String,String> contentParametersMap = new HashMap<>();
        contentParametersMap.put("Phone","1");
        contentParametersMap.put("Name","Test");
        Map<String,String> headingParametersMap = new HashMap<>();

        List<PushNotificationTemplateDto> pushNotificationTemplateDtos = new ArrayList<>();
        pushNotificationTemplateDtos.add(new PushNotificationTemplateDto("1","1",contentParametersMap,null));
        pushNotificationTemplateDtos.add(new PushNotificationTemplateDto("2","2",contentParametersMap,null));
        pushNotificationTemplateDtos.add(new PushNotificationTemplateDto("3","3",contentParametersMap,null));
        pushNotificationTemplateDtos.add(new PushNotificationTemplateDto("3","3", (Map<String, String>) null,null));

        PushNotificationTemplate template1 = new PushNotificationTemplate(messageContent, contentParameters,null ,1L, 2L,headingContent,null,2L);

        when(pushTemplateRepository.findById(1L)).thenReturn(Optional.of(template1));

        PushNotificationRequest pushNotificationRequest = new PushNotificationRequest(pushNotificationTemplateDtos,1L,1L,1L);
        PushNotificationRequestDto response = templateService.constructNotification(pushNotificationRequest);

        assertEquals("Name : Test ; Phone : 1",response.getPushNotificationRequest().getPersonList().get(0).getMessage());
        assertEquals("Name and Phone",response.getPushNotificationRequest().getPersonList().get(0).getHeading());
        assertEquals(3,response.getPushNotificationRequest().getPersonList().size());
        assertEquals(1,response.getFailedPNRequests().size());
    }

    @Test
    public void testConstructNotificationWithHeadingParameters() {
        String headingContent = "Name : %(Name) ; Phone : %(Phone)";
        String messageContent = "Name and Phone";
        String headingParameters = "Phone,Name";
        Map<String,String> headingParametersMap = new HashMap<>();
        headingParametersMap.put("Phone","1");
        headingParametersMap.put("Name","Test");

        List<PushNotificationTemplateDto> pushNotificationTemplateDtos = new ArrayList<>();
        pushNotificationTemplateDtos.add(new PushNotificationTemplateDto("1","1",null,headingParametersMap));
        pushNotificationTemplateDtos.add(new PushNotificationTemplateDto("2","2",null,headingParametersMap));
        pushNotificationTemplateDtos.add(new PushNotificationTemplateDto("3","3",null,headingParametersMap));
        pushNotificationTemplateDtos.add(new PushNotificationTemplateDto("3","3", (Map<String, String>) null,null));


        PushNotificationTemplate template1 = new PushNotificationTemplate(messageContent,null,headingParameters ,1L, 2L,headingContent,null,1L);

        when(pushTemplateRepository.findById(1L)).thenReturn(Optional.of(template1));

        PushNotificationRequest pushNotificationRequest = new PushNotificationRequest(pushNotificationTemplateDtos,1L,1L,1L);
        PushNotificationRequestDto response = templateService.constructNotification(pushNotificationRequest);

        assertEquals("Name : Test ; Phone : 1",response.getPushNotificationRequest().getPersonList().get(0).getHeading());
        assertEquals("Name and Phone",response.getPushNotificationRequest().getPersonList().get(0).getMessage());
        assertEquals(3,response.getPushNotificationRequest().getPersonList().size());
        assertEquals(1,response.getFailedPNRequests().size());

    }

    @Test
    public void testConstructEmailWithParameters() {
        String bodyContent = "Name : %(Name) ; Phone : %(Phone)";
        String subjectContent = "Name : %(Name) ; Phone : %(Phone)";
        String bodyParameters = "Phone,Name";
        String subjectParameters = "Phone,Name";
        EmailTemplate template = new EmailTemplate(bodyContent,subjectContent,bodyParameters,subjectParameters,1L,3L);
       
        Map<String,String> bodyParametersMap = new HashMap<>();
        bodyParametersMap.put("Phone","1");
        bodyParametersMap.put("Name","Test");
        Map<String,String> subjectParametersMap = new HashMap<>();
        subjectParametersMap.put("Phone","1");
        subjectParametersMap.put("Name","Test");
        Map<String,String> subjectParametersFailMap = new HashMap<>();


        List<EmailTemplateDto> emailTemplateDtos = new ArrayList<>();
        emailTemplateDtos.add(new EmailTemplateDto("1",subjectParametersMap,bodyParametersMap));
        emailTemplateDtos.add(new EmailTemplateDto("2",subjectParametersMap,bodyParametersMap));
        emailTemplateDtos.add(new EmailTemplateDto("3",subjectParametersFailMap,bodyParametersMap));

        EmailTemplate template1 = new EmailTemplate(bodyContent,subjectContent,bodyParameters,subjectParameters,1L,3L);

        when(emailTemplateRepository.findById(1L)).thenReturn(Optional.of(template1));

        EmailRequest emailRequest = new EmailRequest(1L,1L,1L,"sender",emailTemplateDtos);
        EmailRequestDto response = templateService.constructEmail(emailRequest);

        assertEquals("Name : Test ; Phone : 1",response.getEmailRequest().getRecipientList().get(0).getBody());
        assertEquals("Name : Test ; Phone : 1",response.getEmailRequest().getRecipientList().get(0).getSubject());
        assertEquals(2,response.getEmailRequest().getRecipientList().size());
        assertEquals(1,response.getFailedEmailRequests().size());
    }

    @Test
    public void testConstructEmailWithOutParameters() {
        String bodyContent = "Name and Phone";
        String subjectContent = "Name and Phone";
        EmailTemplate template = new EmailTemplate(bodyContent,subjectContent,null,null,1L,3L);

        List<EmailTemplateDto> emailTemplateDtos = new ArrayList<>();
        emailTemplateDtos.add(new EmailTemplateDto("1",(Map<String, String>) null,(Map<String,String>) null));
        when(emailTemplateRepository.findById(1L)).thenReturn(Optional.of(template));

        EmailRequest emailRequest = new EmailRequest(1L,1L,1L,"sender",emailTemplateDtos);
        EmailRequestDto response = templateService.constructEmail(emailRequest);

        assertEquals("Name and Phone",response.getEmailRequest().getRecipientList().get(0).getBody());
        assertEquals("Name and Phone",response.getEmailRequest().getRecipientList().get(0).getSubject());
        assertEquals(1,response.getEmailRequest().getRecipientList().size());

    }

    @Test
    public void testConstructEmailWithBodyParameters() {
        String bodyContent = "Name : %(Name) ; Phone : %(Phone)";
        String subjectContent = "Name and Phone";
        String bodyParameters = "Phone,Name";
        EmailTemplate template = new EmailTemplate(bodyContent,subjectContent,bodyParameters,null,1L,3L);
        Map<String,String> bodyParametersMap = new HashMap<>();
        bodyParametersMap.put("Phone","1");
        bodyParametersMap.put("Name","Test");
        Map<String,String> subjectParametersMap = new HashMap<>();

        List<EmailTemplateDto> emailTemplateDtos = new ArrayList<>();
        emailTemplateDtos.add(new EmailTemplateDto("1",bodyParametersMap,null));
        emailTemplateDtos.add(new EmailTemplateDto("2",bodyParametersMap,null));
        emailTemplateDtos.add(new EmailTemplateDto("3",bodyParametersMap,null));
        emailTemplateDtos.add(new EmailTemplateDto("3", (Map<String, String>) null,null));

        EmailTemplate template1 = new EmailTemplate(bodyContent,subjectContent ,bodyParameters,null ,1L, 3L);

        when(emailTemplateRepository.findById(1L)).thenReturn(Optional.of(template1));

        EmailRequest emailRequest = new EmailRequest(1L,1L,1L,"sender",emailTemplateDtos);
        EmailRequestDto response = templateService.constructEmail(emailRequest);

        assertEquals("Name : Test ; Phone : 1",response.getEmailRequest().getRecipientList().get(0).getBody());
        assertEquals("Name and Phone",response.getEmailRequest().getRecipientList().get(0).getSubject());
        assertEquals(3,response.getEmailRequest().getRecipientList().size());
        assertEquals(1,response.getFailedEmailRequests().size());
    }

    @Test
    public void testConstructEmailWithSubjectParameters() {
        String subjectContent = "Name : %(Name) ; Phone : %(Phone)";
        String bodyContent = "Name and Phone";
        String subjectParameters = "Phone,Name";
        Map<String,String> subjectParametersMap = new HashMap<>();
        subjectParametersMap.put("Phone","1");
        subjectParametersMap.put("Name","Test");

        List<EmailTemplateDto> emailTemplateDtos = new ArrayList<>();
        emailTemplateDtos.add(new EmailTemplateDto("1",null,subjectParametersMap));
        emailTemplateDtos.add(new EmailTemplateDto("2",null,subjectParametersMap));
        emailTemplateDtos.add(new EmailTemplateDto("3",null,subjectParametersMap));
        emailTemplateDtos.add(new EmailTemplateDto("3", (Map<String, String>) null,null));


        EmailTemplate template1 = new EmailTemplate(bodyContent,subjectContent,null,subjectParameters ,1L, 3L);

        when(emailTemplateRepository.findById(1L)).thenReturn(Optional.of(template1));

        EmailRequest emailRequest = new EmailRequest(1L,1L,1L,"sender",emailTemplateDtos);
        EmailRequestDto response = templateService.constructEmail(emailRequest);

        assertEquals("Name : Test ; Phone : 1",response.getEmailRequest().getRecipientList().get(0).getSubject());
        assertEquals("Name and Phone",response.getEmailRequest().getRecipientList().get(0).getBody());
        assertEquals(3,response.getEmailRequest().getRecipientList().size());
        assertEquals(1,response.getFailedEmailRequests().size());

    }


}
